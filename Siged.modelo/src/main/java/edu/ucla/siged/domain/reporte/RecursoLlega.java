package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;

@Component
public class RecursoLlega{
	
	private Integer totalPorRecurso;
	
	private Integer totalDonacion;
	
	private String nombreRecurso;
	
	private Double porcentaje;
	
	

	public RecursoLlega() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public RecursoLlega(Integer totalPorRecurso, Integer totalDonacion,
			String nombreRecurso, Double porcentaje) {
		super();
		this.totalPorRecurso = totalPorRecurso;
		this.totalDonacion = totalDonacion;
		this.nombreRecurso = nombreRecurso;
		this.porcentaje = porcentaje;
	}


	public Integer getTotalPorRecurso() {
		return totalPorRecurso;
	}

	public void setTotalPorRecurso(Integer totalPorRecurso) {
		this.totalPorRecurso = totalPorRecurso;
	}

	public Integer getTotalDonacion() {
		return totalDonacion;
	}

	public void setTotalDonacion(Integer totalDonacion) {
		this.totalDonacion = totalDonacion;
	}

	public String getNombreRecurso() {
		return nombreRecurso;
	}

	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	

	
}
