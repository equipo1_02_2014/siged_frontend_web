package edu.ucla.siged.domain.gestioneventos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Component
@Entity 
@Table(name="recurso")
public class Recurso implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="nombrerecurso",length = 45 ,nullable=false)
	private String nombreRecurso;
	@Column(name="descripcion",length = 50 ,nullable=false)
	private String descripcion;
	@Column(name="estatus" ,nullable=false)
	private Short estatus;
	
	public Recurso() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Recurso(Integer id, String nombreRecurso, String descripcion,
			Short estatus) {
		super();
		this.id = id;
		this.nombreRecurso = nombreRecurso;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}
	
	public Recurso(String nombreRecurso, String descripcion, Short estatus) {
		super();
		this.nombreRecurso = nombreRecurso;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombreRecurso() {
		return nombreRecurso;
	}
	
	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Short getEstatus() {
		return estatus;
	}
	
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
}