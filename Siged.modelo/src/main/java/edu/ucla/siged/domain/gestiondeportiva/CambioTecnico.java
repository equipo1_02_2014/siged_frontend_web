package edu.ucla.siged.domain.gestiondeportiva;

// Generated 26/10/2014 04:01:31 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.stereotype.Component;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

/**
 * CambioTecnico
 */
@Component
@Entity 
@Table(name="cambiotecnico")
public class CambioTecnico implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	
	@ManyToOne
	@JoinColumn(name="idactividadpractica")
	private ActividadPractica actividadPractica;
	
	@ManyToOne
	@JoinColumn(name = "idequipotecnico", referencedColumnName = "id")
	private EquipoTecnico equipoTecnico;
	
	
	@ManyToOne
	@JoinColumn(name = "idtecnico", nullable = false)
	private Tecnico tecnicoSuplente;


	public CambioTecnico() {
		super();
		// TODO Auto-generated constructor stub
	}


	public CambioTecnico(Integer id, ActividadPractica actividadPractica,
			EquipoTecnico equipoTecnico, Tecnico tecnicoSuplente) {
		super();
		this.id = id;
		this.actividadPractica = actividadPractica;
		this.equipoTecnico = equipoTecnico;
		this.tecnicoSuplente = tecnicoSuplente;
	}


	public CambioTecnico(ActividadPractica actividadPractica,
			EquipoTecnico equipoTecnico, Tecnico tecnicoSuplente) {
		super();
		this.actividadPractica = actividadPractica;
		this.equipoTecnico = equipoTecnico;
		this.tecnicoSuplente = tecnicoSuplente;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public ActividadPractica getActividadPractica() {
		return actividadPractica;
	}


	public void setActividadPractica(ActividadPractica actividadPractica) {
		this.actividadPractica = actividadPractica;
	}


	public EquipoTecnico getEquipoTecnico() {
		return equipoTecnico;
	}


	public void setEquipoTecnico(EquipoTecnico equipoTecnico) {
		this.equipoTecnico = equipoTecnico;
	}


	public Tecnico getTecnicoSuplente() {
		return tecnicoSuplente;
	}


	public void setTecnicoSuplente(Tecnico tecnicoSuplente) {
		this.tecnicoSuplente = tecnicoSuplente;
	}
		
}
