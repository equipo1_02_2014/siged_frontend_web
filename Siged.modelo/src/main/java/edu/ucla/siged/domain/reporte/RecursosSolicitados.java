package edu.ucla.siged.domain.reporte;


public class RecursosSolicitados {
	
    private String nombreRecurso;
    private Integer cantidad;
	private Integer total;
	private Double porcentaje;
	
	
	
	public RecursosSolicitados(String nombreRecurso, Integer cantidad,
			Integer total, Double porcentaje) {
		super();
		this.nombreRecurso = nombreRecurso;
		this.cantidad = cantidad;
		this.total = total;
		this.porcentaje = porcentaje;
		
		
	}
	
	
	public RecursosSolicitados() {
		super();
	}



	public String getNombreRecurso() {
		return nombreRecurso;
	}

	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}



	public Integer getCantidad() {
		return cantidad;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	public Double getPorcentaje()
	{
		return  porcentaje;
	}

	public void setPorcentaje(Double porcentaje)
	{
		this.porcentaje = porcentaje;
	}

//	public int compareTo(RecursosSolicitados obj) {
//		System.out.println("Resultado ext. " + this.total  +  " Resultado Obj Ext. "+ obj.getTotal());
//		return this.total.compareTo(obj.getTotal());
//	}



	
	
	
}

