package edu.ucla.siged.domain.reporte;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Asistencia implements Comparable<Asistencia> {


	private Integer id;
	
	private Date fecha;
	
	private Integer asistencia;
	
	private String nombre;
	
	private Integer cupos;
	
	private Double porcentaje;
	

	public Asistencia() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Asistencia(Integer id, Date fecha,
			Integer asistencia) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.asistencia = asistencia;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getAsistencia() {
		return asistencia;
	}

	public void setAsistencia(Integer asistencia) {
		this.asistencia = asistencia;
	}


	public int compareTo(Asistencia obj) {
		return this.asistencia.compareTo(obj.getAsistencia());
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Integer getCupos() {
		return cupos;
	}


	public void setCupos(Integer cupos) {
		this.cupos = cupos;
	}


	public Double getPorcentaje() {
		return porcentaje;
	}


	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	
	
	
	
}
