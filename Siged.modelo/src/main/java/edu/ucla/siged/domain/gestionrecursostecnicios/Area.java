package edu.ucla.siged.domain.gestionrecursostecnicios;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="area")
public class Area implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="nombre", nullable=false, length=45)
	private String nombre;
	@Column(name="ubicacion", nullable=false, length=150)
	private String ubicacion;
	@Column(name="estatus", nullable=false)
	private Short estatus;
	@ManyToOne
	@JoinColumn(name="idtipoarea", nullable= false)
	private TipoArea tipoArea;
	
	@JsonBackReference
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="idarea")
	private Set<HorarioArea> horariosArea = new HashSet<HorarioArea>();

	public Area() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Area(String nombre, String ubicacion, Short estatus,
			TipoArea tipoArea, Set<HorarioArea> horariosArea) {
		super();
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.estatus = estatus;
		this.tipoArea = tipoArea;
		this.horariosArea = horariosArea;
	}

	public Area(Integer id, String nombre, String ubicacion, Short estatus,
			TipoArea tipoArea, Set<HorarioArea> horariosArea) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.ubicacion = ubicacion;
		this.estatus = estatus;
		this.tipoArea = tipoArea;
		this.horariosArea = horariosArea;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public TipoArea getTipoArea() {
		return tipoArea;
	}

	public void setTipoArea(TipoArea tipoArea) {
		this.tipoArea = tipoArea;
	}

	public Set<HorarioArea> getHorariosArea() {
		return horariosArea;
	}

	public void setHorariosArea(Set<HorarioArea> horariosArea) {
		this.horariosArea = horariosArea;
	}
	
/////////////////////////////////////////////*******************agregado******************************	
	@JsonBackReference
	public void addHorariosArea(HorarioArea horariosArea){
		getHorariosArea().add(horariosArea);
	}
	
}
