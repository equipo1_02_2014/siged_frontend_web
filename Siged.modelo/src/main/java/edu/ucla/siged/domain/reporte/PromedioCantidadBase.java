package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;

@Component
public class PromedioCantidadBase implements Comparable<PromedioCantidadBase> {


	private String descripcion;
	
	private Integer cantidadConsiderada;
	
	private Integer cantidadTotal;
	
	private Double promedio;
	
	private Double porcentaje;
	
	
	public PromedioCantidadBase(String descripcion,
			Integer cantidadConsiderada, Integer cantidadTotal, Double promedio) {
		super();
		this.descripcion = descripcion;
		this.cantidadConsiderada = cantidadConsiderada;
		this.cantidadTotal = cantidadTotal;
		this.promedio = promedio;
	}
	
	public PromedioCantidadBase() {
		super();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getCantidadConsiderada() {
		return cantidadConsiderada;
	}

	public void setCantidadConsiderada(Integer cantidadConsiderada) {
		this.cantidadConsiderada = cantidadConsiderada;
	}

	public Integer getCantidadTotal() {
		return cantidadTotal;
	}

	public void setCantidadTotal(Integer cantidadTotal) {
		this.cantidadTotal = cantidadTotal;
	}

	public Double getPromedio() {
		return promedio;
	}

	public void setPromedio(Double promedio) {
		this.promedio = promedio;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}
	
	public int compareTo(PromedioCantidadBase obj) {
		return this.promedio.compareTo(obj.getPromedio());
	}

	
	
}
