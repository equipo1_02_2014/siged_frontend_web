package edu.ucla.siged.domain.utilidades;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.beans.support.SortDefinition;

public class Paginacion {
	
	public Paginacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static List<Object> paginar(Object clase, List list , SortDefinition sort, Integer page, Integer limit){
		
		PagedListHolder<Object> pagedList = new PagedListHolder<Object>(list , sort);
		pagedList.resort();
        pagedList.setPageSize(limit);
        pagedList.setPage(page);
        
		return pagedList.getPageList();
		
	}
	
	@SuppressWarnings("unchecked")
	public static List<Object> paginar_para_set(Object clase, Set set , SortDefinition sort, Integer page, Integer limit){
		
		List<Object> list = new ArrayList<Object>(set);
		PagedListHolder<Object> pagedList = new PagedListHolder<Object>(list , sort);
		pagedList.resort();
        pagedList.setPageSize(limit);
        pagedList.setPage(page);
        
		return pagedList.getPageList();
		
	}

}
