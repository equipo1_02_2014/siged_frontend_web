package edu.ucla.siged.domain.utilidades;

// Generated 26/10/2014 04:01:31 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * Organizacion generated by hbm2java
 */
@Component
@Entity
@Table(name="organizacion")
public class Organizacion implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name ="nombre", nullable=false)
	private String nombre;
	@Column(name ="direccion", nullable=false)
	private String direccion;
	@Column(name ="telefono", nullable=false)
	private String telefono;
	@Column(name ="email")
	private String email;
	@Column(name ="vision")
	private String vision;
	@Column(name ="mision")
	private String mision;
	@Column(name ="horarioatencion")
	private String horarioAtencion;
	@Column(name ="directiva")
	private String directiva;
	@Column(name ="rif", nullable=false)
	private String rif;

	public Organizacion() {
	}

	public Organizacion(Integer id, String nombre, String direccion,
			String telefono, String email, String vision, String mision,
			String horarioAtencion, String directiva, String rif) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.email = email;
		this.vision = vision;
		this.mision = mision;
		this.horarioAtencion = horarioAtencion;
		this.directiva = directiva;
		this.rif = rif;
	}



	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "direccion", nullable = false)
	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVision() {
		return this.vision;
	}

	public void setVision(String vision) {
		this.vision = vision;
	}

	public String getMision() {
		return this.mision;
	}

	public void setMision(String mision) {
		this.mision = mision;
	}

	public String getHorarioAtencion() {
		return this.horarioAtencion;
	}

	public void setHorarioAtencion(String horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}

	public String getDirectiva() {
		return this.directiva;
	}

	public void setDirectiva(String directiva) {
		this.directiva = directiva;
	}

	public String getRif() {
		return this.rif;
	}

	public void setRif(String rif) {
		this.rif = rif;
	}

}
