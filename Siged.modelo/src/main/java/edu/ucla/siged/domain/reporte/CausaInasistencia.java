package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;

@Component
public class CausaInasistencia implements Comparable<CausaInasistencia> {


	private String nombre;
	private Integer cantidad;
	private Integer total;
	private Double porcentaje;
	

	public CausaInasistencia() {
		super();
	}

	
	public int compareTo(CausaInasistencia obj) {
		return this.porcentaje.compareTo(obj.getPorcentaje());
	}


	public Double getPorcentaje() {
		return porcentaje;
	}


	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}


	public Integer getTotal() {
		return total;
	}


	public void setTotal(Integer total) {
		this.total = total;
	}


	public Integer getCantidad() {
		return cantidad;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
