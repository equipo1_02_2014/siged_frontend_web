package edu.ucla.siged.domain.gestioneventos;

// Generated 26/10/2014 04:01:31 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.stereotype.Component;

/**
 * Pregunta generated by hbm2java
 */
@Component
@Entity 
@Table(name="pregunta")
public class Pregunta implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="descripcion",length = 150 ,nullable=false)
	private String descripcion;
	
	@ManyToOne
	@JoinColumn(name = "idtipopregunta", nullable = true)
	private TipoPregunta tipoPregunta;
	
	@JsonBackReference
	@OneToMany(cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
	@JoinColumn(name="idpregunta")
	private Set<OpcionPregunta> opcionPreguntas;

	public Pregunta() {
		super();
		descripcion="";
		tipoPregunta=null;
		opcionPreguntas= new HashSet<OpcionPregunta>();
		// TODO Auto-generated constructor stub
	}
	
	public Pregunta(Integer id, String descripcion, TipoPregunta tipoPregunta,
			Set<OpcionPregunta> opcionPreguntas) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.tipoPregunta = tipoPregunta;
		this.opcionPreguntas = opcionPreguntas;
	}

	public Pregunta(String descripcion, TipoPregunta tipoPregunta,
			Set<OpcionPregunta> opcionPreguntas) {
		super();
		this.descripcion = descripcion;
		this.tipoPregunta = tipoPregunta;
		this.opcionPreguntas = opcionPreguntas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public TipoPregunta getTipoPregunta() {
		return tipoPregunta;
	}

	public void setTipoPregunta(TipoPregunta tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}

	public Set<OpcionPregunta> getOpcionPreguntas() {
		return opcionPreguntas;
	}

	public void setOpcionPreguntas(Set<OpcionPregunta> opcionPreguntas) {
		this.opcionPreguntas = opcionPreguntas;
	}
	
	public void agregarOpcion(OpcionPregunta opcion) {
		// TODO Auto-generated method stub
		getOpcionPreguntas().add(opcion);
	}
	
	public void eliminarOpcion(OpcionPregunta opcion){
		getOpcionPreguntas().remove(opcion);
	}
	
}
