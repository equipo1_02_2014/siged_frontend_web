package edu.ucla.siged.domain.reporte;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Entidad {
	@Id
	private Integer id;
	@Column(name="nombrefisico")
	private String nombreFisico;
	@Column(name="nombreamostrar")
	private String nombreAMostrar;
	@OneToMany
	@JoinColumn(name="identidad")
	private Set<Atributo> atributos;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombreFisico() {
		return nombreFisico;
	}
	public void setNombreFisico(String nombreFisico) {
		this.nombreFisico = nombreFisico;
	}
	public String getNombreAMostrar() {
		return nombreAMostrar;
	}
	public void setNombreAMostrar(String nombreAMostrar) {
		this.nombreAMostrar = nombreAMostrar;
	}
	public Set<Atributo> getAtributos() {
		return atributos;
	}
	public void setAtributos(Set<Atributo> atributos) {
		this.atributos = atributos;
	}
	
	
	
}
