package edu.ucla.siged.domain.gestionrecursostecnicios;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name ="horarioarea")
public class HorarioArea implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Temporal(TemporalType.TIME)
	@Column(name="horainicio",nullable=false)
	private Date horaInicio;
	@Temporal(TemporalType.TIME)
	@Column(name="horafin",nullable=false)
	private Date horaFin;
	@Column(name="dia", nullable=false)
	private Integer dia;
	@Column(name="observacion", nullable=true, length=150)
	private String observacion; 
	@Column(name="disponible", nullable=true)
	private Boolean disponible;
	//Agregado por Jesus
	@ManyToOne
	@JoinColumn(name="idarea",nullable=true)
	private Area area;
	
	public HorarioArea() {
		super();
		// TODO Auto-generated constructor stub
	}
	public HorarioArea(Date horaInicio, Date horaFin, Integer dia,
			String observacion, Boolean disponible) {
		super();
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.dia = dia;
		this.observacion = observacion;
		this.disponible = disponible;
	}
	public HorarioArea(Integer id, Date horaInicio, Date horaFin,
			Integer dia, String observacion, Boolean disponible) {
		super();
		this.id = id;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.dia = dia;
		this.observacion = observacion;
		this.disponible = disponible;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getHoraInicio() {
		return horaInicio;
	}
	
	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	public Date getHoraFin() {
		return horaFin;
	}
	
	public void setHoraFin(Date horaFin) {
		this.horaFin = horaFin;
	}
	
	public Integer getDia() {
		return dia;
	}
	
	public void setDia(Integer dia) {
		this.dia = dia;
	}
	
	public String getObservacion() {
		return observacion;
	}
	
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public Boolean getDisponible() {
		return disponible;
	}
	
	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}
	
	//Metodos agregados por Jesus
	public Area getArea() {
		return area;
	}
	
	public void setArea(Area area) {
		this.area = area;
	}

}