package edu.ucla.siged.domain.utilidades;

// Generated 26/10/2014 04:01:31 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * Estadocivil generated by hbm2java
 */
@Component
@Entity 
@Table(name="estadocivil")
public class EstadoCivil implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="descripcion", length=45, nullable=false)
	private String descripcion;

	public EstadoCivil() {
	}

	public EstadoCivil(String descripcion) {
	super();
		this.descripcion = descripcion;
    }

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
