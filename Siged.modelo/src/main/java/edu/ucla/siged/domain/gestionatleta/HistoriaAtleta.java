package edu.ucla.siged.domain.gestionatleta;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="historiaatleta")
public class HistoriaAtleta implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="fecha", nullable = false)
	private Date fecha;
	@Column(name="estatus")
	private Short estatus;
	@ManyToOne
	@JoinColumn(name = "idestatusatleta", nullable = false)
	private EstatusAtleta estatusAtleta;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "idatleta", nullable = true)
	private Atleta atleta;
	

	public HistoriaAtleta() {
	}

	public HistoriaAtleta(Date fecha, Short estatus, EstatusAtleta estatusAtleta) {
		super();
		this.fecha = fecha;
		this.estatus = estatus;
		this.estatusAtleta = estatusAtleta;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public EstatusAtleta getEstatusAtleta() {
		return estatusAtleta;
	}

	public void setEstatusAtleta(EstatusAtleta estatusAtleta) {
		this.estatusAtleta = estatusAtleta;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Atleta getAtleta() {
		return atleta;
	}

	public void setAtleta(Atleta atleta) {
		this.atleta = atleta;
	}
	
}