package edu.ucla.siged.domain.utilidades;

import edu.ucla.siged.domain.gestioneventos.Encuesta;

public class EncuestaWrapper {
	
	private Boolean respondida;
	private Encuesta encuesta;
	
	public EncuestaWrapper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EncuestaWrapper(Boolean respondida, Encuesta encuesta) {
		super();
		this.respondida = respondida;
		this.encuesta = encuesta;
	}

	public Boolean getRespondida() {
		return respondida;
	}

	public void setRespondida(Boolean respondida) {
		this.respondida = respondida;
	}

	public Encuesta getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

}
