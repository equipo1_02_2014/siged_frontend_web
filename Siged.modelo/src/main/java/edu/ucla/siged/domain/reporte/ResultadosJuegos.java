package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;

@Component
public class ResultadosJuegos {
	
	private String competencia;
	private String equipo;
	private String fecha;
	private String resultado;
	private String contrincante;
	private String categoria;
	private String rango;
	
	public ResultadosJuegos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCompetencia() {
		return competencia;
	}

	public void setCompetencia(String competencia) {
		this.competencia = competencia;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getContrincante() {
		return contrincante;
	}

	public void setContrincante(String contrincante) {
		this.contrincante = contrincante;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getRango() {
		return rango;
	}

	public void setRango(String rango) {
		this.rango = rango;
	}

}
