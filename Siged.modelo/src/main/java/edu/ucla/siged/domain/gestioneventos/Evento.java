package edu.ucla.siged.domain.gestioneventos;

// Generated 26/10/2014 04:01:31 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.stereotype.Component;

/**
 * Evento generated by hbm2java
 */
@Component
@Entity
@Table(name="evento")
public class Evento implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Temporal(TemporalType.DATE)
	@Column(name="fecha", nullable = false)
	private Date fecha;
	@Column(name="direccion", length = 250,nullable = true)
	private String direccion;
	@Column(name="observacion", length = 250,nullable = true)
	private String observacion;
	@Column(name="nombre", length = 150,nullable = false)
	private String nombre;
	@Temporal(TemporalType.TIME)
	@Column(name="hora", length = 15,nullable = false)
	private Date hora;
	@Column(name="responsable", length = 150,nullable = true)
	private String responsable;
	@Column(name="contactotelefono", length = 15,nullable = true)
	private String contactoTelefono;
	@Column(name="contactocorreo", length = 150,nullable = true)
	private String contactoCorreo;
	@Column(name="patrocinantes", length = 500,nullable = true)
	private String patrocinantes;
	@Temporal(TemporalType.DATE)
	@Column(name="fecharegistro", nullable = false)
	private Date fechaRegistro;
	@Column(name="resultadosesperados", nullable = true)
	private Long resultadosEsperados;
	@Column(name="resultadosobtenidos", nullable = true)
	private Long resultadosObtenidos;
	
	@JsonBackReference
	@OneToOne(optional=true, fetch=FetchType.LAZY,mappedBy="evento", cascade= CascadeType.ALL )
	//@JoinColumn(name="iddonacion")
	private Donacion donacion;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name="idevento")
	private Set<AdjuntoEvento> adjuntoEventos =  new HashSet<AdjuntoEvento>();

//	@JsonBackReference
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "evento", cascade = CascadeType.ALL, orphanRemoval = true)
//	private Set<AdjuntoEvento> adjuntoEventos =  new HashSet<AdjuntoEvento>();

	public Evento() {
		super();
	}

	public Evento(Date fecha, String direccion, String observacion,
			String nombre, Date hora, String responsable,
			String contactoTelefono, String contactoCorreo,
			String patrocinantes, Date fechaRegistro, Long resultadosEsperados,
			Long resultadosObtenidos, Donacion donacion,
			Set<AdjuntoEvento> adjuntoEventos) {
		super();
		this.fecha = fecha;
		this.direccion = direccion;
		this.observacion = observacion;
		this.nombre = nombre;
		this.hora = hora;
		this.responsable = responsable;
		this.contactoTelefono = contactoTelefono;
		this.contactoCorreo = contactoCorreo;
		this.patrocinantes = patrocinantes;
		this.fechaRegistro = fechaRegistro;
		this.resultadosEsperados = resultadosEsperados;
		this.resultadosObtenidos = resultadosObtenidos;
		this.donacion = donacion;
		this.adjuntoEventos = adjuntoEventos;
	}

	public Evento(Integer id, Date fecha, String direccion, String observacion,
			String nombre, Date hora, String responsable,
			String contactoTelefono, String contactoCorreo,
			String patrocinantes, Date fechaRegistro, Long resultadosEsperados,
			Long resultadosObtenidos, Donacion donacion,
			Set<AdjuntoEvento> adjuntoEventos) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.direccion = direccion;
		this.observacion = observacion;
		this.nombre = nombre;
		this.hora = hora;
		this.responsable = responsable;
		this.contactoTelefono = contactoTelefono;
		this.contactoCorreo = contactoCorreo;
		this.patrocinantes = patrocinantes;
		this.fechaRegistro = fechaRegistro;
		this.resultadosEsperados = resultadosEsperados;
		this.resultadosObtenidos = resultadosObtenidos;
		this.donacion = donacion;
		this.adjuntoEventos = adjuntoEventos;
	}
	
	public Evento(Date fecha, String direccion, String observacion,
			String nombre, Date hora, String responsable,
			String contactoTelefono, String contactoCorreo,
			String patrocinantes, Date fechaRegistro, Long resultadosEsperados,
			Long resultadosObtenidos, /*Donacion donacion,*/
			Set<AdjuntoEvento> adjuntoEventos) {
		super();
		this.fecha = fecha;
		this.direccion = direccion;
		this.observacion = observacion;
		this.nombre = nombre;
		this.hora = hora;
		this.responsable = responsable;
		this.contactoTelefono = contactoTelefono;
		this.contactoCorreo = contactoCorreo;
		this.patrocinantes = patrocinantes;
		this.fechaRegistro = fechaRegistro;
		this.resultadosEsperados = resultadosEsperados;
		this.resultadosObtenidos = resultadosObtenidos;
//		this.donacion = donacion;
		this.adjuntoEventos = adjuntoEventos;
	}

	public Evento(Integer id, Date fecha, String direccion, String observacion,
			String nombre, Date hora, String responsable,
			String contactoTelefono, String contactoCorreo,
			String patrocinantes, Date fechaRegistro, Long resultadosEsperados,
			Long resultadosObtenidos, /*Donacion donacion,*/
			Set<AdjuntoEvento> adjuntoEventos) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.direccion = direccion;
		this.observacion = observacion;
		this.nombre = nombre;
		this.hora = hora;
		this.responsable = responsable;
		this.contactoTelefono = contactoTelefono;
		this.contactoCorreo = contactoCorreo;
		this.patrocinantes = patrocinantes;
		this.fechaRegistro = fechaRegistro;
		this.resultadosEsperados = resultadosEsperados;
		this.resultadosObtenidos = resultadosObtenidos;
//		this.donacion = donacion;
		this.adjuntoEventos = adjuntoEventos;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getContactoTelefono() {
		return contactoTelefono;
	}

	public void setContactoTelefono(String contactoTelefono) {
		this.contactoTelefono = contactoTelefono;
	}

	public String getContactoCorreo() {
		return contactoCorreo;
	}

	public void setContactoCorreo(String contactoCorreo) {
		this.contactoCorreo = contactoCorreo;
	}

	public String getPatrocinantes() {
		return patrocinantes;
	}

	public void setPatrocinantes(String patrocinantes) {
		this.patrocinantes = patrocinantes;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Long getResultadosEsperados() {
		return resultadosEsperados;
	}

	public void setResultadosEsperados(Long resultadosEsperados) {
		this.resultadosEsperados = resultadosEsperados;
	}

	public Long getResultadosObtenidos() {
		return resultadosObtenidos;
	}

	public void setResultadosObtenidos(Long resultadosObtenidos) {
		this.resultadosObtenidos = resultadosObtenidos;
	}

	public Donacion getDonacion() {
		return donacion;
	}

	public void setDonacion(Donacion donacion) {
		this.donacion = donacion;
	}

	public Set<AdjuntoEvento> getAdjuntoEventos() {
		return adjuntoEventos;
	}

	public void setAdjuntoEventos(Set<AdjuntoEvento> adjuntoEventos) {
		this.adjuntoEventos = adjuntoEventos;
	}
}
