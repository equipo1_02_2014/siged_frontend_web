package edu.ucla.siged.domain.gestiondeportiva;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name="actividad")
public class Actividad implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="descripcion", nullable=false,length=255)
	private String descripcion;
	@Column(name="estatus", nullable=false)
	private Short estatus;
	public Actividad() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Actividad(String descripcion, Short estatus) {
		super();
		this.descripcion = descripcion;
		this.estatus = estatus;
	}
	public Actividad(Integer id, String descripcion, Short estatus) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.estatus = estatus;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Short getEstatus() {
		return estatus;
	}
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	
	
	
}
