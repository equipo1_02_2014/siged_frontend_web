package edu.ucla.siged.domain.utilidades;

public class RespuestaServidor {

	private Boolean Ok;
	private String mensaje;
	public RespuestaServidor() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RespuestaServidor(Boolean Ok, String mensaje) {
		super();
		this.Ok = Ok;
		this.mensaje = mensaje;
	}
	public Boolean getOk() {
		return Ok;
	}
	public void setOk(Boolean ok) {
		Ok = ok;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
