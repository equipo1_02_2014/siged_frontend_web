package edu.ucla.siged.domain.reporte;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Reporte {
	private List<String> listaTipos;
	private String tipo;
	private boolean todos;

	
	public Reporte() {
		
	}
	
	
	public void inicializarLista(){
		listaTipos = new ArrayList<String>();
		listaTipos.add("pdf");
		listaTipos.add("xls");
	}
	
	public List<String> getListaTipos() {
		return listaTipos;
	}

	public void setListaTipos(List<String> listaTipos) {
		this.listaTipos = listaTipos;
	}

	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public boolean isTodos() {
		return todos;
	}


	public void setTodos(boolean todos) {
		this.todos = todos;
	}
	
	
    
	
}
