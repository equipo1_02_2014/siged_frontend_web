package edu.ucla.siged.domain.gestiondeportiva;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.stereotype.Component;
import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;

@Component
@Entity
@Table(name="actividadpractica")
public class ActividadPractica implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Temporal(TemporalType.TIME)
	@Column(name="horainicio", nullable=false)
	private Date horaInicio;
	@Temporal(TemporalType.TIME)
	@Column(name="horafin", nullable=false)
	private Date horaFin;
	@Column(name="realizada")
	private Boolean realizada;
	@Column(name="observacion", length=255)
	private String observacion;
	@ManyToOne
	@JoinColumn(name="idpractica")
	private Practica practica;
	@ManyToOne
	@JoinColumn(name="idactividad")
	private Actividad actividad;
	@ManyToOne
	@JoinColumn(name="idarea")
	private Area area;
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="idactividadpractica")
	private Set<CambioTecnico> cambioTecnicos = new HashSet<CambioTecnico>();
	@ManyToOne
	@JoinColumn(name="idcausa")
	private Causa causa;


	public ActividadPractica() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ActividadPractica(Date horaInicio, Date horaFin, Boolean realizada,
			String observacion, Practica practica, Actividad actividad,
			Area area, Set<CambioTecnico> cambioTecnicos) {
		super();
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.realizada = realizada;
		this.observacion = observacion;
		this.practica = practica;
		this.actividad = actividad;
		this.area = area;
		this.cambioTecnicos = cambioTecnicos;
	}

	public ActividadPractica(Integer id, Date horaInicio, Date horaFin,
			Boolean realizada, String observacion, Practica practica,
			Actividad actividad, Area area, Set<CambioTecnico> cambioTecnicos) {
		super();
		this.id = id;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.realizada = realizada;
		this.observacion = observacion;
		this.practica = practica;
		this.actividad = actividad;
		this.area = area;
		this.cambioTecnicos = cambioTecnicos;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Date getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(Date horaFin) {
		this.horaFin = horaFin;
	}

	public Boolean getRealizada() {
		return realizada;
	}

	public void setRealizada(Boolean realizada) {
		this.realizada = realizada;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Practica getPractica() {
		return practica;
	}

	public void setPractica(Practica practica) {
		this.practica = practica;
	}

	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Set<CambioTecnico> getCambioTecnicos() {
		return cambioTecnicos;
	}

	public void setCambioTecnicos(Set<CambioTecnico> cambioTecnicos) {
		this.cambioTecnicos = cambioTecnicos;
	}
	
	public Causa getCausa() {
		return causa;
	}

	public void setCausa(Causa causa) {
		this.causa = causa;
	}

	
}
