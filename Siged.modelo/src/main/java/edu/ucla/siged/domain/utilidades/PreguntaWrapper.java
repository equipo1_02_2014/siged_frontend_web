package edu.ucla.siged.domain.utilidades;

import java.util.Date;
import edu.ucla.siged.domain.gestioneventos.Pregunta;

public class PreguntaWrapper {
	
	private Integer idEncuesta;
	private Pregunta pregunta;
	private Date fecha;
	
	public PreguntaWrapper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PreguntaWrapper(Pregunta pregunta, Date fecha) {
		super();
		this.pregunta = pregunta;
		this.fecha = fecha;
	}

	public PreguntaWrapper(Integer idEncuesta, Pregunta pregunta, Date fecha) {
		super();
		this.idEncuesta = idEncuesta;
		this.pregunta = pregunta;
		this.fecha = fecha;
	}
	
	public Integer getIdEncuesta() {
		return idEncuesta;
	}

	public void setIdEncuesta(Integer idEncuesta) {
		this.idEncuesta = idEncuesta;
	}
	

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
