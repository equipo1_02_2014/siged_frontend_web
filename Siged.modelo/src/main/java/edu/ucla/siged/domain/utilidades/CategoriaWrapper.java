package edu.ucla.siged.domain.utilidades;

public class CategoriaWrapper {
	
	private Integer categoriaId;
	private String Categoria;
	private Integer rangoId;
	private String Rango;
	
	public CategoriaWrapper() {
		super();
	}

	public CategoriaWrapper(Integer categoriaId, String categoria,
			Integer rangoId, String rango) {
		super();
		this.categoriaId = categoriaId;
		Categoria = categoria;
		this.rangoId = rangoId;
		Rango = rango;
	}

	public Integer getCategoriaId() {
		return categoriaId;
	}

	public void setCategoriaId(Integer categoriaId) {
		this.categoriaId = categoriaId;
	}

	public String getCategoria() {
		return Categoria;
	}

	public void setCategoria(String categoria) {
		Categoria = categoria;
	}

	public Integer getRangoId() {
		return rangoId;
	}

	public void setRangoId(Integer rangoId) {
		this.rangoId = rangoId;
	}

	public String getRango() {
		return Rango;
	}

	public void setRango(String rango) {
		Rango = rango;
	}


}
