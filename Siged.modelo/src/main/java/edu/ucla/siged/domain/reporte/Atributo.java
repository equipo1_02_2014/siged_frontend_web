package edu.ucla.siged.domain.reporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Atributo {
	@Id
	private Integer id;
	@Column(name="nombrefisico")
	private String nombreFisico;
	@Column(name="nombreamostrar")
	private String nombreAMostrar;
	private boolean mostrar;
	private Integer tipo; //1:integer, 2:string, 3:fecha
	@ManyToOne
	@JoinColumn(name="identidad")
	private Entidad entidad;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombreFisico() {
		return nombreFisico;
	}
	public void setNombreFisico(String nombreFisico) {
		this.nombreFisico = nombreFisico;
	}
	public String getNombreAMostrar() {
		return nombreAMostrar;
	}
	public void setNombreAMostrar(String nombreLogico) {
		this.nombreAMostrar = nombreLogico;
	}
	
	public boolean isMostrar() {
		return mostrar;
	}
	public void setMostrar(boolean mostrar) {
		this.mostrar = mostrar;
	}
	
	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Entidad getEntidad() {
		return entidad;
	}
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}
	
	
}
