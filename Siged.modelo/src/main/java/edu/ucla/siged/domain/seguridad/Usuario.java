/*Class Usuario
 * 
 * Version 1.0
 * 
 * 06/12/2014
 * 
 * Copyright Equipo 1. Challenge.
 */

package edu.ucla.siged.domain.seguridad;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.stereotype.Component;

import edu.ucla.siged.domain.utilidades.Archivo;

/**
 * Noticia generated by hbm2java
 */
@Component
@Entity 
@Table(name="usuario")
public class Usuario implements java.io.Serializable {

	/**
	 * 
	 */
	@Embedded
	protected Archivo foto = new Archivo();
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="nombre",length = 150 ,nullable=false)
	private String nombre;
	@Column(name="apellido",length = 250 ,nullable=false)
	private String apellido;
	@Temporal(TemporalType.DATE)
	@Column(name="fechaRegistro", nullable=false)
	private Date fechaRegistro;
	@Temporal(TemporalType.DATE)
	@Column(name="fechaNacimiento", nullable=false)
	private Date fechaNacimiento;
	@Column(name="cedula",length = 11 ,nullable=false)
	private String cedula;
	@Column(name="email",length = 255 ,nullable=false)
	private String email;
	@Column(name="password",length = 256 ,nullable=false)
	private String password;
	@Column(name="celular",length = 15 ,nullable=false)
	private String celular;
	@Column(name="telefono",length = 15 ,nullable=true)
	private String telefono;
	@Column(name="nombreusuario",length = 15 ,nullable=false)
	private String nombreUsuario;
	@Column(name="token",length = 255 ,nullable=true)
	private String token;
	@Column(name="macdispositivo",length = 255 ,nullable=true)
	private String macDispositivo;
	@Column(name="estatus" ,nullable=false)
	private Short estatus;
	@Column(name="preguntasecreta",length = 255 ,nullable=true)
	private String preguntaSecreta;
	@Column(name="respuestasecreta",length = 255 ,nullable=true)
	private String respuestaSecreta;

	@JsonBackReference
	@OneToMany(cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
	@JoinColumn(name="idusuario")
	private Set<RolUsuario> roles= new HashSet<RolUsuario>();
	
	

	public Usuario() {
		super();
		roles= new HashSet<RolUsuario>();
		fechaRegistro=new Date();
		nombreUsuario="";
		password="";
		// TODO Auto-generated constructor stub
	}
	
	public Usuario(Integer id, String nombre, String apellido,
			Date fechaRegistro, Date fechaNacimiento, String cedula,
			String email, String password, String celular, String telefono,
			String nombreUsuario, String token, String macDispositivo,
			Short estatus, String preguntaSecreta, String respuestaSecreta,
			Set<RolUsuario> roles, Archivo foto) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaRegistro = fechaRegistro;
		this.fechaNacimiento = fechaNacimiento;
		this.cedula = cedula;
		this.email = email;
		this.password = password;
		this.celular = celular;
		this.telefono = telefono;
		this.nombreUsuario = nombreUsuario;
		this.token = token;
		this.macDispositivo = macDispositivo;
		this.estatus = estatus;
		this.preguntaSecreta = preguntaSecreta;
		this.respuestaSecreta = respuestaSecreta;
		this.roles = roles;
		this.foto = foto;
	}
	
	

	public Usuario(Integer id, String nombre, String apellido,
			Date fechaRegistro, Date fechaNacimiento, String cedula,
			String email, String password, String celular,
			String nombreUsuario, Short estatus, Archivo foto) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaRegistro = fechaRegistro;
		this.fechaNacimiento = fechaNacimiento;
		this.cedula = cedula;
		this.email = email;
		this.password = password;
		this.celular = celular;
		this.nombreUsuario = nombreUsuario;
		this.estatus = estatus;
		this.foto = foto;
		roles= new HashSet<RolUsuario>();
	}

	

	public Usuario(Archivo foto, String nombre, String apellido,
			Date fechaNacimiento, String email, String celular, String telefono) {
		super();
		this.foto = foto;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNacimiento = fechaNacimiento;
		this.email = email;
		this.celular = celular;
		this.telefono = telefono;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMacDispositivo() {
		return macDispositivo;
	}

	public void setMacDispositivo(String macDispositivo) {
		this.macDispositivo = macDispositivo;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public String getPreguntaSecreta() {
		return preguntaSecreta;
	}

	public void setPreguntaSecreta(String preguntaSecreta) {
		this.preguntaSecreta = preguntaSecreta;
	}

	public String getRespuestaSecreta() {
		return respuestaSecreta;
	}

	public void setRespuestaSecreta(String respuestaSecreta) {
		this.respuestaSecreta = respuestaSecreta;
	}

	public Set<RolUsuario> getRoles() {
		return roles;
	}

	public void setRoles(Set<RolUsuario> roles) {
		this.roles = roles;
	}

	public Archivo getFoto() {
		return foto;
	}

	public void setFoto(Archivo foto) {
		this.foto = foto;
	}

	public void agregarRolUsuario(RolUsuario rolUsuario){
		this.roles.add(rolUsuario);
	}
	
	public void eliminarRolUsuario(RolUsuario rolUsuario){
		this.roles.remove(rolUsuario);
	}

	/*public List<RolUsuario> getListaRoles() {
		Object[] rolest=getRoles().toArray();
		RolUsuario rolt= new RolUsuario();
		List<RolUsuario> lista = null;
		for(int i=0; i<rolest.length;i++)
		{
			rolt= (RolUsuario) rolest[i];
			lista.add(rolt);
		}
		return lista;
	}*/
	public boolean isAnonimo(){
		if(this.nombreUsuario.equals(""))
			return true;
		return false;
	}
}
