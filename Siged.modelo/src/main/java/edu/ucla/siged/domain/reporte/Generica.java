package edu.ucla.siged.domain.reporte;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Generica {
	@Id
	private Integer id=1;
	private String campo1;
	private String campo2;
	private String campo3;
	private String campo4;
	

	public Generica() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Generica(String campo1, String campo2, String campo3, String campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2;
		this.campo3 = campo3;
		this.campo4 = campo4;
	}
	
	
	public Generica(String campo1, String campo2, String campo3) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2;
		this.campo3 = campo3;
	}
	
	public Generica(Date campo1, Date campo2) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2.toString();
	}
	
	public Generica(String campo1, String campo2) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2;
	}
	
	public Generica(String campo1) {
		super();
		this.campo1 = campo1;
	}
	
	
	public Generica(String campo1, Short campo2) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
	}
	
	
	public Generica(String campo1, Integer campo2, Integer campo3) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3.toString();
	}
	
	public Generica(String campo1, Integer campo2) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
	
	}
	
	public Generica(Integer campo1, Integer campo2) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2.toString();
	}
	
	//caso competencia
	public Generica(String campo1, Date campo2, Date campo3, String campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3.toString();
		this.campo4 = campo4;
	}

	public Generica(String campo1, Date campo2, Date campo3) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3.toString();
	}

	public Generica(Date campo1, Date campo2, String campo3) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
	}

	
	public Generica(String campo1, Date campo2) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
	}
	
	public Generica(String campo1, Date campo2, String campo3, Date campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
		this.campo4 = campo4.toString();
	}
	
	//caso delegado
	public Generica(String campo1, String campo2, String campo3, Date campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
		this.campo4 = campo4.toString();
	}
	
	
	//para horarios
	public Generica(String campo1, Integer campo2, Date campo3, Date campo4) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2.toString();
		this.campo3 = campo3.toString();
		this.campo4 = campo4.toString();
	}

	public Generica(Integer campo1, Integer campo2, String campo3) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
	}
	
	public Generica(Integer campo1, String campo2, Integer campo3) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2;
		this.campo3 = campo3.toString();
	}
	
	public Generica(String campo1, Integer campo2, String campo3) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
	}
	
	public Generica(Date campo1, Date campo2, String campo3, String campo4) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
		this.campo4 = campo4;
	}
	
	public Generica(Date campo1, String campo2, Date campo3, String campo4) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2;
		this.campo3 = campo3.toString();
		this.campo4 = campo4;
	}
	
	public Generica(String campo1, String campo2, Date campo3, Date campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2;
		this.campo3 = campo3.toString();
		this.campo4 = campo4.toString();
	}
	
	public Generica(Date campo1, String campo2, String campo3, String campo4) {
		super();
		this.campo1 = campo1.toString();
		this.campo2 = campo2;
		this.campo3 = campo3;
		this.campo4 = campo4;
	}
	
	public Generica(String campo1, Date campo2, String campo3, String campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3;
		this.campo4 = campo4;
	}
	
	public Generica(String campo1, String campo2, Date campo3, String campo4) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2;
		this.campo3 = campo3.toString();
		this.campo4 = campo4;
	}
	
	public Generica(String campo1, Date campo2, Integer campo3) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3.toString();
	}
	
	public Generica(String campo1, Integer campo2, Date campo3) {
		super();
		this.campo1 = campo1;
		this.campo2 = campo2.toString();
		this.campo3 = campo3.toString();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCampo1() {
		return campo1;
	}
	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}
	public String getCampo2() {
		return campo2;
	}
	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}
	public String getCampo3() {
		return campo3;
	}
	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}
	public String getCampo4() {
		return campo4;
	}
	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}
	
	

}
