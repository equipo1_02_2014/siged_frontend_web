/*Class Rol
 * 
 * Version 1.0
 * 
 * 06/12/2014
 * 
 * Copyright Equipo 1. Challenge.
 */

package edu.ucla.siged.domain.seguridad;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.springframework.stereotype.Component;

@Component
@Entity 
@Table(name="rol")
public class Rol implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="nombre",length = 150 ,nullable=false)
	private String nombre;
	@Column(name="descripcion",length = 250 ,nullable=false)
	private String descripcion;
	@Temporal(TemporalType.DATE)
	@Column(name="fecharegistro", nullable=false)
	private Date fechaRegistro;
	@Column(name="estatus",nullable=false)
	private Short estatus;
	
	@JsonBackReference
	@OneToMany(cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
	@JoinColumn(name="idrol")
	private Set<ControlAcceso> funcionalidades= new HashSet<ControlAcceso>();
	
	@JsonBackReference
	@OneToMany(cascade = {CascadeType.ALL}, fetch=FetchType.LAZY)
	@JoinColumn(name="idrol")
	private Set<RolUsuario> rolUsuarios;

	public Rol() {
		super();
		// TODO Auto-generated constructor stub
		funcionalidades= new HashSet<ControlAcceso>();
		fechaRegistro=new Date();
	}

	public Rol(Integer id, String nombre, String descripcion,
			Date fechaRegistro, Short estatus,
			Set<ControlAcceso> controlAccesos, Set<RolUsuario> rolUsuarios) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fechaRegistro = fechaRegistro;
		this.estatus = estatus;
		this.funcionalidades = controlAccesos;
		this.rolUsuarios = rolUsuarios;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Set<ControlAcceso> getFuncionalidades() {
		return funcionalidades;
	}

	public void setControlAccesos(Set<ControlAcceso> controlAccesos) {
		this.funcionalidades = controlAccesos;
	}

	public Set<RolUsuario> getRolUsuarios() {
		return rolUsuarios;
	}

	public void setRolUsuarios(Set<RolUsuario> rolUsuarios) {
		this.rolUsuarios = rolUsuarios;
	}

	public void agregarControlAcceso(ControlAcceso funcionalidadRol){
		this.funcionalidades.add(funcionalidadRol);
	}

	public void eliminarFuncionalidadRol(ControlAcceso funcionalidadRol){
		this.funcionalidades.remove(funcionalidadRol);
	}
	
	/*public List<ControlAcceso> getListaFuncionalidades() {
		Object[] funcionalidadest=getFuncionalidades().toArray();
		ControlAcceso funcionalidadt= new ControlAcceso();
		List<ControlAcceso> lista = null;
		for(int i=0; i<funcionalidadest.length;i++)
		{
			funcionalidadt= (ControlAcceso) funcionalidadest[i];
			lista.add(funcionalidadt);
		}
		return lista;
	}*/
}
