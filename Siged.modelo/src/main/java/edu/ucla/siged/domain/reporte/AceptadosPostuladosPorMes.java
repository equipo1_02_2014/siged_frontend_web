package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;


@Component
public class AceptadosPostuladosPorMes implements Comparable<AceptadosPostuladosPorMes> {

	public AceptadosPostuladosPorMes() {
		super();
		// TODO Auto-generated constructor stub
	}



	Integer cantidadaceptados;
	Integer cantidadpostulados;
	String mesaceptados;
	String mespostulados;
	
	

	public AceptadosPostuladosPorMes(Integer cantidadaceptados,
			Integer cantidadpostulados, String mesaceptados, String mespostulados) {
		super();
		this.cantidadaceptados = cantidadaceptados;
		this.cantidadpostulados = cantidadpostulados;
		this.mesaceptados = mesaceptados;
		this.mespostulados = mespostulados;
	}
	
	
	public Integer getCantidadaceptados() {
		return cantidadaceptados;
	}



	public void setCantidadaceptados(int cantidadaceptados) {
		this.cantidadaceptados = cantidadaceptados;
	}



	public Integer getCantidadpostulados() {
		return cantidadpostulados;
	}



	public void setCantidadpostulados(int cantidadpostulados) {
		this.cantidadpostulados = cantidadpostulados;
	}



	public String getMesaceptados() {
		return mesaceptados;
	}



	public void setMesaceptados(String mesaceptados) {
		this.mesaceptados = mesaceptados;
	}



	public String getMespostulados() {
		return mespostulados;
	}



	public void setMespostulados(String mespostulados) {
		this.mespostulados = mespostulados;
	}



	public int compareTo(AceptadosPostuladosPorMes obj) {
		return this.cantidadaceptados.compareTo(obj.getCantidadaceptados());
		
	}



	

}
