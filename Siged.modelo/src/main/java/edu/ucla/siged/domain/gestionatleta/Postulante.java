package edu.ucla.siged.domain.gestionatleta;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.Persona;

@Component
@Entity
@Table(name="postulante")
public class Postulante extends Persona implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name ="asmatico")
	private Boolean asmatico;
	@Column(name ="retardo")
	private Boolean retardo;
	@Column(name ="sordomudo")
	private Boolean sordoMudo;
	@Column(name ="intervencionquirurgica")
	private Boolean intervencionQuirurgica;
	@Column(name ="descripcionintervencion", length = 255)
	private String descripcionIntervencion;
	@Column(name ="observacion", length = 255)
	private String observacion;
	@Temporal(TemporalType.DATE)
	@Column(name="fechapostulacion", nullable= false)
	private Date fechaPostulacion = new Date();
	@Column(name="autorrespuesta", nullable=true)
	private String autorRespuesta;
	@Temporal(TemporalType.DATE)
	@Column(name="fechaRespuesta", nullable=true)
	private Date fechaRespuesta = new Date();
	@Column(name="respuesta", nullable=true)
	private String respuesta;
	@Column(name="aprobacion")
	private Boolean aprobacion;
	@Column(name="estatus", nullable=false)
	private Short estatus;
	@ManyToOne
	@JoinColumn(name="idcausa")
	private Causa causa;
	
	
	public Postulante() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Postulante(String cedula, String nombre, String apellido,
			Date fechaNacimiento, String lugarNacimiento, String direccion,
			String telefono, String email, Integer id, Boolean asmatico, 
			Boolean retardo, Boolean sordoMudo, Boolean intervencionQuirurgica,
			String descripcionIntervencion, String observacion, 
			Date fechaPostulacion, String autorRespuesta, Date fechaRespuesta,
			String respuesta, Boolean aprobacion, Short estatus) {
		super();
		this.id = id;
		this.asmatico = asmatico;
		this.retardo = retardo;
		this.sordoMudo = sordoMudo;
		this.intervencionQuirurgica = intervencionQuirurgica;
		this.descripcionIntervencion = descripcionIntervencion;
		this.observacion = observacion;
		this.fechaPostulacion = fechaPostulacion;
		this.autorRespuesta = autorRespuesta;
		this.fechaRespuesta = fechaRespuesta;
		this.respuesta = respuesta;
		this.aprobacion = aprobacion;
		this.estatus = estatus;
	}
	
	//Agregado ENERO 31 - Constructor basado en Informacion de solicitud (creacion del objeto)
		public Postulante(String cedula, String nombre, String apellido,
				Date fechaNacimiento, String lugarNacimiento, String direccion,
				String telefono, String email, Boolean asmatico, 
				Boolean retardo, Boolean sordoMudo, Boolean intervencionQuirurgica,
				String descripcionIntervencion, String observacion, 
				Date fechaPostulacion, Short estatus) {
			super();
			this.cedula = cedula;
			this.nombre = nombre;
			this.apellido =  apellido;
			this.fechaNacimiento =  fechaNacimiento;
			this.lugarNacimiento = lugarNacimiento;
			this.direccion =  direccion;
			this.telefono = telefono;
			this.email = email;
			this.asmatico = asmatico;
			this.retardo = retardo;
			this.sordoMudo = sordoMudo;
			this.intervencionQuirurgica = intervencionQuirurgica;
			this.descripcionIntervencion = descripcionIntervencion;
			this.observacion = observacion;
			this.fechaPostulacion = fechaPostulacion;
			this.estatus = estatus;
		}
		
		//+Febrero 09
		public Postulante(String cedula, String nombre, String apellido,
				Date fechaNacimiento, String lugarNacimiento, String direccion,
				String telefono, String celular, String email, Archivo foto, Boolean asmatico, 
				Boolean retardo, Boolean sordoMudo, Boolean intervencionQuirurgica,
				String descripcionIntervencion, String observacion, 
				Date fechaPostulacion, Short estatus) {
			super();
			this.cedula = cedula;
			this.nombre = nombre;
			this.apellido =  apellido;
			this.fechaNacimiento =  fechaNacimiento;
			this.lugarNacimiento = lugarNacimiento;
			this.direccion =  direccion;
			this.telefono = telefono;
			this.celular = celular;
			this.email = email;
			this.foto = foto;
			this.asmatico = asmatico;
			this.retardo = retardo;
			this.sordoMudo = sordoMudo;
			this.intervencionQuirurgica = intervencionQuirurgica;
			this.descripcionIntervencion = descripcionIntervencion;
			this.observacion = observacion;
			this.fechaPostulacion = fechaPostulacion;
			this.estatus = estatus;
		}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAsmatico() {
		return asmatico;
	}

	public void setAsmatico(Boolean asmatico) {
		this.asmatico = asmatico;
	}

	public Boolean getRetardo() {
		return retardo;
	}

	public void setRetardo(Boolean retardo) {
		this.retardo = retardo;
	}

	public Boolean getSordoMudo() {
		return sordoMudo;
	}

	public void setSordoMudo(Boolean sordoMudo) {
		this.sordoMudo = sordoMudo;
	}

	public Boolean getIntervencionQuirurgica() {
		return intervencionQuirurgica;
	}

	public void setIntervencionQuirurgica(Boolean intervencionQuirurgica) {
		this.intervencionQuirurgica = intervencionQuirurgica;
	}

	public String getDescripcionIntervencion() {
		return descripcionIntervencion;
	}

	public void setDescripcionIntervencion(String descripcionIntervencion) {
		this.descripcionIntervencion = descripcionIntervencion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getFechaPostulacion() {
		return fechaPostulacion;
	}

	public void setFechaPostulacion(Date fechaPostulacion) {
		this.fechaPostulacion = fechaPostulacion;
	}

	public String getAutorRespuesta() {
		return autorRespuesta;
	}

	public void setAutorRespuesta(String autorRespuesta) {
		this.autorRespuesta = autorRespuesta;
	}

	public Date getFechaRespuesta() {
		return fechaRespuesta;
	}

	public void setFechaRespuesta(Date fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public Boolean getAprobacion() {
		return aprobacion;
	}

	public void setAprobacion(Boolean aprobacion) {
		this.aprobacion = aprobacion;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Causa getCausa() {
		return causa;
	}

	public void setCausa(Causa causa) {
		this.causa = causa;
	}

}