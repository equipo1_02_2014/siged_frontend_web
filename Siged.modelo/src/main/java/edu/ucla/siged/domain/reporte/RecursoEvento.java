package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;

@Component
public class RecursoEvento implements Comparable<RecursoEvento> {

	private String nombre;
	private String correo;
	private String telefono;
	private String direccion;
	private String fecha;
	private String hora;
	private String patrocinantes;
	private String responsable;
	private Integer cantidad;
	private String recurso;
	
	public RecursoEvento() {
		super();
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getPatrocinantes() {
		return patrocinantes;
	}

	public void setPatrocinantes(String patrocinantes) {
		this.patrocinantes = patrocinantes;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public int compareTo(RecursoEvento obj) {
		return this.cantidad.compareTo(obj.getCantidad());
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	
	
	
}
