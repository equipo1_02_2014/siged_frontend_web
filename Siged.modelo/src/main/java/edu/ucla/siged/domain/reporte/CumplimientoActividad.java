package edu.ucla.siged.domain.reporte;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Subselect;

import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

@Entity
@Subselect(value = "")
public class CumplimientoActividad {
	@Id
	Integer id;
	Actividad actividad;
	Tecnico tecnico;
	Equipo equipo;
	String grupo; //actividad, tecnico o equipo
	Long cantidadCumplida;
	Long cantidadTotal;
	

	public CumplimientoActividad(Actividad actividad, String grupo, Long cantidadCumplida,
			Long cantidadTotal) {
		super();
		this.actividad = actividad;
		this.grupo = grupo;
		this.cantidadCumplida = cantidadCumplida;
		this.cantidadTotal = cantidadTotal;
	}


	public CumplimientoActividad(Tecnico tecnico, String grupo,
			Long cantidadCumplida, Long cantidadTotal) {
		super();
		this.tecnico = tecnico;
		this.grupo = grupo;
		this.cantidadCumplida = cantidadCumplida;
		this.cantidadTotal = cantidadTotal;
	}
	

	public CumplimientoActividad(Equipo equipo, String grupo,
			Long cantidadCumplida, Long cantidadTotal) {
		super();
		this.equipo = equipo;
		this.grupo = grupo;
		this.cantidadCumplida = cantidadCumplida;
		this.cantidadTotal = cantidadTotal;
	}


	public CumplimientoActividad() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGrupo() {
		return grupo;
	}


	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}


	public Long getCantidadCumplida() {
		return cantidadCumplida;
	}



	public void setCantidadCumplida(Long cantidadCumplida) {
		this.cantidadCumplida = cantidadCumplida;
	}



	public Long getCantidadTotal() {
		return cantidadTotal;
	}



	public void setCantidadTotal(Long cantidadTotal) {
		this.cantidadTotal = cantidadTotal;
	}
	
	
	

}
