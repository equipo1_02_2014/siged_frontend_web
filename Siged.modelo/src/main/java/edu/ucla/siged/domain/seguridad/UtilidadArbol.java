package edu.ucla.siged.domain.seguridad;

import java.util.HashMap;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class UtilidadArbol extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;
	Window win = null;
	public UtilidadArbol() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Listen("onClick=#tree")
	public void onClickMenu(String rutaArchivoZul, String rutaModal) {
		try {
			final HashMap<String, Object> map = new HashMap<String, Object>();
	  		map.put("rutaModal", rutaModal);
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			if (win != null) {
				win.detach();
			}
			System.out.println("/vistas/"+rutaArchivoZul);
			win = (Window) Executions.createComponents("/vistas/"+rutaArchivoZul,center, null);
			
		} catch (Exception e) {
			Messagebox.show(e.toString());
		}
	}
}
