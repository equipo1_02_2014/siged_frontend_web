package edu.ucla.siged.domain.utilidades;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ucla.siged.domain.seguridad.Usuario;

public class SessionWrapper {
	
	private Usuario usuario;
	private List<String> funcionalidades;
	private List<String> Roles;
	
	public SessionWrapper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SessionWrapper(Usuario usuario, List<String> funcionalidades) {
		super();
		this.usuario = usuario;
		this.funcionalidades = funcionalidades;
	}
	
	public SessionWrapper(Usuario usuario, List<String> funcionalidades,
			List<String> roles) {
		super();
		this.usuario = usuario;
		this.funcionalidades = funcionalidades;
		Roles = roles;
	}
	
	

	public List<String> getRoles() {
		return Roles;
	}

	public void setRoles(List<String> roles) {
		Roles = roles;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<String> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<String> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

}
