package edu.ucla.siged.domain.gestionatleta;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import edu.ucla.siged.domain.gestiondeportiva.AsistenciaAtleta;

/* LEER: el atributo tipo se asignara en duro, es decir, se le hara setTipo(1) si la causa es de inasistencia
 * a practica y setTipo(2) si la causa es de rechazo de postulacion. Nombre es la causa en si, ejemplo: Enfermedad,
 * viaje, estudios, desconocida etc. la descripcion es alguna observacion o detalle de lo que es la causa.
 */
@Entity
public class Causa {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="nombre", length= 50)
	private String nombre;
	@Column(name="descripcion", length=255)
	private String descripcion;
	private Integer tipo;   //1: Causa de practica, 2:causa de no aceptacion de postulacion
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="idcausa")
	private Set<Postulante> postulantes = new HashSet<Postulante>();
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name="idcausa")
	private Set<AsistenciaAtleta> asistenciaAtletas = new HashSet<AsistenciaAtleta>();
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public Set<Postulante> getPostulantes() {
		return postulantes;
	}
	public void setPostulantes(Set<Postulante> postulantes) {
		this.postulantes = postulantes;
	}
	public Set<AsistenciaAtleta> getAsistenciaAtletas() {
		return asistenciaAtletas;
	}
	public void setAsistenciaAtletas(Set<AsistenciaAtleta> asistenciaAtletas) {
		this.asistenciaAtletas = asistenciaAtletas;
	}
}