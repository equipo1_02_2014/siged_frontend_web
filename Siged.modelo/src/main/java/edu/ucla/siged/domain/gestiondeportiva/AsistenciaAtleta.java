package edu.ucla.siged.domain.gestiondeportiva;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.stereotype.Component;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Causa;

@Component
@Entity
@Table(name="asistenciaatleta")
public class AsistenciaAtleta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="asistencia")
	private Boolean asistencia;
	@ManyToOne
	@JoinColumn(name="idatleta")
	private Atleta atleta;
	@ManyToOne
	@JoinColumn(name="idpractica")
	private Practica practica;
	@ManyToOne
	@JoinColumn(name="idcausa")
	private Causa causa;

	public AsistenciaAtleta() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AsistenciaAtleta(Boolean asistencia, Atleta atleta, Practica practica) {
		super();
		this.asistencia = asistencia;
		this.atleta = atleta;
		this.practica = practica;
	}

	public AsistenciaAtleta(Integer id, Boolean asistencia, Atleta atleta,
			Practica practica) {
		super();
		this.id = id;
		this.asistencia = asistencia;
		this.atleta = atleta;
		this.practica = practica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAsistencia() {
		return asistencia;
	}

	public void setAsistencia(Boolean asistencia) {
		this.asistencia = asistencia;
	}

	public Atleta getAtleta() {
		return atleta;
	}

	public void setAtleta(Atleta atleta) {
		this.atleta = atleta;
	}

	public Practica getPractica() {
		return practica;
	}

	public void setPractica(Practica practica) {
		this.practica = practica;
	}

	public Causa getCausa() {
		return causa;
	}

	public void setCausa(Causa causa) {
		this.causa = causa;
	}
	
	
}
