package edu.ucla.siged.domain.reporte;

import org.springframework.stereotype.Component;

@Component
public class FrecuenciaCausa{


	private Integer idCausa;
	
	private Integer cantidadCausa;
	
	private String nombre;
	

	public FrecuenciaCausa() {
		super();
	}
	


	public Integer getIdCausa() {
		return idCausa;
	}



	public void setIdCausa(Integer idCausa) {
		this.idCausa = idCausa;
	}



	public Integer getCantidadCausa() {
		return cantidadCausa;
	}



	public void setCantidadCausa(Integer cantidadCausa) {
		this.cantidadCausa = cantidadCausa;
	}



	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
	
}
