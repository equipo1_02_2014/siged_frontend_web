/*Class Rol Usuario
 * 
 * Version 1.0
 * 
 * 06/12/2014
 * 
 * Copyright Equipo 1. Challenge.
 */

package edu.ucla.siged.domain.seguridad;

import java.util.Date;
import javax.persistence.*;
import org.springframework.stereotype.Component;


@Component
@Entity 
@Table(name="rolusuario")
public class RolUsuario implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Temporal(TemporalType.DATE)
	@Column(name="fecharegistro", nullable=false)
	private Date fechaRegistro;
	@Temporal(TemporalType.DATE)
	@Column(name="fechaeliminacion", nullable=true)
	private Date fechaEliminacion;
	@Column(name="usuarioRegistro",length = 255 ,nullable=true)
	private String usuarioRegistro;
	@Column(name="usuarioeliminacion",length = 255 ,nullable=true)
	private String usuarioEliminacion;
	@Column(name="estatus" ,nullable=false)
	private Short estatus;
	@ManyToOne
	@JoinColumn(name = "idrol", nullable = false)
	private Rol rol;
	@ManyToOne
	@JoinColumn(name = "idusuario", nullable = false)
	private Usuario usuario;

	public RolUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RolUsuario(Integer id, Date fechaRegistro, Date fechaEliminacion,
			String usuarioRegistro, String usuarioEliminacion, Short estatus,
			Rol rol, Usuario usuario) {
		super();
		this.id = id;
		this.fechaRegistro = fechaRegistro;
		this.fechaEliminacion = fechaEliminacion;
		this.usuarioRegistro = usuarioRegistro;
		this.usuarioEliminacion = usuarioEliminacion;
		this.estatus = estatus;
		this.rol = rol;
		this.usuario = usuario;
	}
	
	public RolUsuario(Date fechaRegistro, Short estatus, Rol rol) {
		super();
		this.fechaRegistro = fechaRegistro;
		this.estatus = estatus;
		this.rol = rol;
	}

	public RolUsuario(Date fechaRegistro, Date fechaEliminacion,
			String usuarioRegistro, String usuarioEliminacion, Short estatus,
			Rol rol, Usuario usuario) {
		super();
		
		this.fechaRegistro = fechaRegistro;
		this.fechaEliminacion = fechaEliminacion;
		this.usuarioRegistro = usuarioRegistro;
		this.usuarioEliminacion = usuarioEliminacion;
		this.estatus = estatus;
		this.rol = rol;
		this.usuario = usuario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaEliminacion() {
		return fechaEliminacion;
	}

	public void setFechaEliminacion(Date fechaEliminacion) {
		this.fechaEliminacion = fechaEliminacion;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getUsuarioEliminacion() {
		return usuarioEliminacion;
	}

	public void setUsuarioEliminacion(String usuarioEliminacion) {
		this.usuarioEliminacion = usuarioEliminacion;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
