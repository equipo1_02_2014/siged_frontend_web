package edu.ucla.siged.domain.utilidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class Persona {
	@Column(name="cedula", length= 10)
	protected String cedula;
	@Column(name="nombre", length= 50)
	protected String nombre;
	@Column(name="apellido", length=50)
	protected String apellido;
	@Column(name="telefono",length=15)
	protected String telefono;
	@Column(name="celular", length =15)
	protected String celular;
	@Column(name="direccion", length=150)
	protected String direccion;
	@Temporal(TemporalType.DATE)
	@Column(name="fechanacimiento")
	protected Date fechaNacimiento;
	@Column(name="lugarnacimiento", length=150)
	protected String lugarNacimiento;
	@Column(name="email", length=100)
	protected String email;
	@Embedded
	protected Archivo foto = new Archivo(); 
	
	public Persona() {
		super();
		// TODO Auto-generated constructor stub
	}
	


	public Persona(String cedula, String nombre, String apellido,
			String telefono, String celular, String direccion,
			Date fechaNacimiento, String lugarNacimiento, String email,
			Archivo foto) {
		super();
		this.cedula = cedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.celular = celular;
		this.direccion = direccion;
		this.fechaNacimiento = fechaNacimiento;
		this.lugarNacimiento = lugarNacimiento;
		this.email = email;
		this.foto = foto;
	}



	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Archivo getFoto() {
		return foto;
	}

	public void setFoto(Archivo foto) {
		this.foto = foto;
	}	
	
}