package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import edu.ucla.siged.domain.gestiondeportiva.Actividad;

public interface DaoActividadI extends JpaRepository<Actividad, Integer>{

	@Query("SELECT A FROM Actividad A WHERE estatus=:estatus")
	public List<Actividad> findByEstatus(@Param("estatus") Short estatus);
}