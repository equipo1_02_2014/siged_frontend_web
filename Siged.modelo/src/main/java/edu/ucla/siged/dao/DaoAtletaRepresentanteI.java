package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestionatleta.AtletaRepresentante;

public interface DaoAtletaRepresentanteI extends JpaRepository<AtletaRepresentante, Integer> {

}
