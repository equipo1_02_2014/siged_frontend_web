package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface DaoPracticaI extends JpaRepository<Practica, Integer> {


	public List<Practica> findByEstatusOrderByFechaDesc(Short estatus);
	
	public List<Practica> findByEstatus(Short estatus);

	public List<Practica> findByFecha(Date fecha);
	
	public Practica findByFechaAndEquipo(Date fecha, Equipo equipo);
	
	@Query("SELECT p FROM Practica p WHERE p.equipo.id=?1 AND "
			+ "p.fecha=?2  AND (?3 BETWEEN p.horaInicio AND p.horaFin)")
	public List<Practica> buscarPracticasPorEquipoTiempo(Integer idEquipo,Date fecha,Date hora);
	
	@Query("SELECT p FROM Practica p,ActividadPractica ap WHERE ap.area.id=?1 AND "
			+ "p.fecha=?2 AND (?3 BETWEEN ap.horaInicio AND ap.horaFin) AND "
			+ "p.id=ap.practica.id")
	public List<Practica> buscarPracticasPorAreaTiempo(Integer idArea,Date fecha,Date hora);
	
	@Query("SELECT p FROM Practica p WHERE p.equipo.id=?1 AND "
			+ "p.fecha=?2 AND (?3 BETWEEN p.horaInicio AND p.horaFin) and p.id <>?4")
	public List<Practica> buscarPracticasPorEquipoTiempo(Integer idEquipo,Date fecha,Date hora, Integer idPractica);
	
	@Query("SELECT p FROM Practica p,ActividadPractica ap WHERE ap.area.id=?1 AND "
			+ "p.fecha=?2 AND (?3 BETWEEN ap.horaInicio AND ap.horaFin) AND "
			+ "p.id=ap.practica.id and p.id<>?4")
	public List<Practica> buscarPracticasPorAreaTiempo(Integer idArea,Date fecha,Date hora, Integer idPractica);
	
	@Query("SELECT p FROM Practica p WHERE p.estatus=1 AND "
			+ "p.equipo.id=?1 AND p.fecha >= CURRENT_DATE")
	public List<Practica> buscarPracticaPorEquipo(Integer idequipo);
	//rest
	@Query("Select p from Practica p Where p.fecha BETWEEN ? AND ? ")
	public List<Practica> porRango(Date desde, Date hasta);
	
	@Query("SELECT p FROM Practica p WHERE p.fecha < ?")
	public List<Practica> practicasPasadas(Date fechasistema);
	
	@Query("SELECT p FROM Practica p WHERE p.fecha >= ?")
	public List<Practica> practicasProximas(Date fechasistema);
}
