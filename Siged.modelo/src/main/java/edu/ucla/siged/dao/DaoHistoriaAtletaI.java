package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;

public interface DaoHistoriaAtletaI extends JpaRepository<HistoriaAtleta, Integer> {

	@Query("SELECT HA FROM HistoriaAtleta HA, Atleta A "
			+ "WHERE  A.id=HA.atleta.id "
			+ "AND HA.estatus=1"
			+ "AND A.id=?1")
	public HistoriaAtleta buscarHistoriaAtletaEstatus1(Integer idAtleta);
}
