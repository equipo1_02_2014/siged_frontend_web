package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.gestioneventos.TipoPregunta;

public interface DaoTipoPreguntaI extends JpaRepository<TipoPregunta, Integer> {

}
