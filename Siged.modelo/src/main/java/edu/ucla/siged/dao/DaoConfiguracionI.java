package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.utilidades.Configuracion;

public interface DaoConfiguracionI extends JpaRepository<Configuracion, Integer> {

}
