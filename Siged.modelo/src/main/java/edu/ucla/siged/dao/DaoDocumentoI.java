package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestionatleta.Documento;

public interface DaoDocumentoI extends JpaRepository<Documento, Integer> {

}
