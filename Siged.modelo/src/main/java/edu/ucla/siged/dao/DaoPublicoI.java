package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.seguridad.Rol;

public interface DaoPublicoI extends JpaRepository<Publico, Integer> {

	//List<Publico> findByEncuesta(Encuesta encuesta);
}
