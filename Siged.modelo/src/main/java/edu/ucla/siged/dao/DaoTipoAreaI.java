package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import edu.ucla.siged.domain.gestionrecursostecnicios.TipoArea;

public interface DaoTipoAreaI extends JpaRepository<TipoArea, Integer> {

}
