package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.DelegadoEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface DaoDelegadoEquipoI extends	JpaRepository<DelegadoEquipo, Integer> {

	@Query("SELECT R FROM Representante R, DelegadoEquipo DE, Equipo E WHERE E.id = ?1 AND DE.responsabilidad = ?2 AND DE.equipo.id = E.id AND R.id = DE.representante.id AND DE.estatus = 1")
	Representante obtenerDelegado(Integer id, Short responsabilidad);

    @Query("SELECT R FROM Representante R WHERE R.estatus=1")
	public List<Representante> buscarRepresentantesDisponiblesDao();
	
//    @Query("SELECT R FROM Representante R WHERE R.estatus=1 and R.id NOT IN ("
//    		+ "SELECT R.id FROM Representante R, DelegadoEquipo DE, Equipo E "
//    		+ "WHERE R.estatus=1 and DE.representante.id=R.id and DE.equipo.id=E.id)")
//	public List<Representante> buscarRepresentantesDisponiblesDao();
    
    @Query("SELECT R FROM Representante R, DelegadoEquipo DE, Equipo E WHERE E = ?1 AND DE.equipo.id = E.id AND DE.representante.id = R.id")
    public List<Representante> obtenerDelegadosEquipo(Equipo equipo);
    
    @Query("SELECT DE FROM DelegadoEquipo DE, Equipo E, Representante R WHERE DE.representante=?1 AND DE.equipo=?2 AND DE.equipo.id = E.id AND DE.representante.id = R.id")
    DelegadoEquipo obtenerDelegadoEquipo(Representante delegado, Equipo equipo);
}
