package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import edu.ucla.siged.domain.gestionatleta.Causa;

public interface DaoCausaI extends JpaRepository<Causa, Integer> {
	public  List<Causa> findByTipo(Integer tipo);
	
	@Query("SELECT c FROM Causa c WHERE c.tipo=2")
	public List<Causa> listaCausas();
}
