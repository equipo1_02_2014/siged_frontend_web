package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

public interface DaoTecnicoI extends JpaRepository<Tecnico, Integer> {
	
    @Query("SELECT T FROM Tecnico T WHERE T.estatus=1")
    public List<Tecnico> buscarTecnicosDisponiblesDao();
	
//    @Query("SELECT T FROM Tecnico T WHERE T.estatus=1 and T.id NOT IN ("
//    		+ "SELECT T.id FROM Tecnico T, EquipoTecnico ET, Equipo E "
//    		+ "WHERE T.estatus=1 and ET.tecnico.id=T.id and ET.equipo.id=E.id)")
//    public List<Tecnico> buscarTecnicosDisponiblesDao();
    
    public Tecnico findByCedula(String cedula);

}