package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestiondeportiva.PosicionArbitro;

public interface DaoPosicionArbitroI extends JpaRepository<PosicionArbitro, Integer> {

}
