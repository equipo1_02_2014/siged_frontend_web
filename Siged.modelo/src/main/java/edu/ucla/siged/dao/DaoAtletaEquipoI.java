package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface DaoAtletaEquipoI extends JpaRepository<AtletaEquipo, Integer> {
	List<AtletaEquipo> findByEquipo(Equipo equipo);
	
	@Query("SELECT AE.id FROM AtletaEquipo AE WHERE AE.atleta=?1")
	Integer obtenerIdAtletaEquipo(Atleta atleta);
	
	@Query("SELECT AE FROM AtletaEquipo AE, Equipo E, Atleta A WHERE AE.atleta=?1 AND AE.equipo=?2 AND AE.equipo.id = E.id AND AE.atleta.id = A.id")
	AtletaEquipo obtenerAtletaEquipo(Atleta atleta, Equipo equipo);
	
	@Query("SELECT A FROM Atleta A, AtletaEquipo AE, Equipo E WHERE E = ?1 AND AE.equipo.id = E.id AND AE.atleta.id=A.id")
	public List<Atleta> obtenerAtletasEquipo(Equipo equipo);
}
