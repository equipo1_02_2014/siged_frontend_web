package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestionatleta.Representante;

public interface DaoRepresentanteI extends
		JpaRepository<Representante, Integer> {

	//Agregado por Jesus
    public Representante findByCedula(String cedula);
}
