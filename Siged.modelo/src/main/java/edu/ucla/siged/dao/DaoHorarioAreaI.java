package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;

public interface DaoHorarioAreaI extends JpaRepository<HorarioArea, Integer> {

	/*Metodo agregado por Jesus*/
	
	@Query("SELECT ha FROM Area a,HorarioArea ha WHERE"
			+ " a.id=?1 AND ha.dia=?2 AND ha.disponible=false AND"
			+ " a.estatus=1 AND a.id=ha.area.id AND"
			+ " ((?3 BETWEEN ha.horaInicio AND ha.horaFin) OR"
			+ " (?4 BETWEEN ha.horaInicio AND ha.horaFin))")
	public List<HorarioArea> buscarHorarioAreaOcupada(Integer idarea,Integer dia,Date horaInicio,Date horaFin);

	//agregado por juan
	@Query("select count(a) from HorarioArea a where a.area = ?1 and a.dia = ?2 and a.disponible=false "+
			   "and ((?3 between a.horaInicio and a.horaFin) or (?4 between a.horaInicio and a.horaFin))")
	public long areaOcupada (Area area, int dia, Date horaInicio, Date horaFin);

}
