package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.utilidades.Organizacion;

public interface DaoOrganizacionI extends JpaRepository<Organizacion, Integer> {

}
