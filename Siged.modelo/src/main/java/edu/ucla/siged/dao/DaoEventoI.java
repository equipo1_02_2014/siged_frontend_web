package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.domain.gestioneventos.Evento;

public interface DaoEventoI extends JpaRepository<Evento, Integer> {
	
	public static int TAMANIO_PAGINA=5;
	
	@Query("SELECT e FROM Evento e WHERE e.fecha > CURRENT_DATE "
			+ "OR e.id IN (SELECT g.id FROM Evento g WHERE g.fecha = CURRENT_DATE)")
	public Page<Evento> buscarEventosFuturos(Pageable p);
	
	@Query("SELECT e FROM Evento e WHERE e.fecha > CURRENT_DATE")
	public Page<Evento> buscarEventosProximos(Pageable p);
	
	@Query("SELECT COUNT(e) FROM Evento e WHERE e.fecha > CURRENT_DATE")
	public long buscarTotalEventosProximos();
	
	
	@Query("SELECT e FROM Evento e WHERE e.fecha > CURRENT_DATE "
			+ "OR e.id IN (SELECT g.id FROM Evento g WHERE g.fecha = CURRENT_DATE)")
	public List<Evento> buscarEventosFuturos();
	
	@Query("SELECT e FROM Evento e WHERE e.fecha >= ?")
	public List<Evento> eventosProximos(Date fechasistema);
	
	@Query("SELECT e FROM Evento e WHERE e.fecha < ?")
	public List<Evento> eventosPasados(Date fechasistema);
	
	Evento findById( @Param ("id") Integer id);
	

}

