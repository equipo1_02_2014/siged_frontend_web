package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;

public interface DaoAdjuntoNoticiaI extends JpaRepository<AdjuntoNoticia, Integer> {

}
