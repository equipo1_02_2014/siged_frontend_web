package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestionatleta.Reposo;

public interface DaoReposoI extends JpaRepository<Reposo, Integer> {

}
