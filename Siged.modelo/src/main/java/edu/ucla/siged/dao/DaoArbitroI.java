/** 
 * 	DaoArbitroI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;

public interface DaoArbitroI extends JpaRepository<Arbitro, Integer> {

	public List<Arbitro> findByEstatusTrue();
	public List<Arbitro> findByEstatus(Short estatus);
	@Query("select count(b) from Arbitro b where b.cedula = ?1 and b.id <> ?2")
	public Long countByCedula(String cedula, Integer idArbitro);
	public Arbitro findByCedula(String cedula);
	@Query("select count(b) from ArbitroJuego b where b.arbitro = ?1")
	public Long countByArbitroEnJuego(Arbitro arbitro);
}