package edu.ucla.siged.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import edu.ucla.siged.domain.seguridad.Usuario;

public interface DaoUsuarioI extends JpaRepository<Usuario, Integer> {

	Usuario findByNombreUsuario(@Param ("nomb") String nombreUsuario);

	Usuario findById(@Param ("id") Integer id);
	
	public Page<Usuario> findByEstatus(@Param ("est") Short estatus, Pageable pagina);
	
	@Query("SELECT COUNT(U) FROM Usuario U WHERE U.estatus=?")
	public long countByEstatus(Short estatus);

	Usuario findByCedula(@Param ("ced") String cedula);
	
	//Rest
	Usuario findByEmail(@Param ("email") String email);
	
	@Query("SELECT u FROM Usuario u WHERE u.nombreUsuario=? AND u.password=? AND u.estatus=1")
	public Usuario login(String nombreUsuario, String password );
	
	@Query("SELECT u FROM Usuario u WHERE u.email=?1 AND u.respuestaSecreta=?2 AND u.estatus=1")
	public Usuario findMatchEmailRespuestaSecreta(String email, String respuestaSecreta );
	
	@Query("SELECT u FROM Usuario u WHERE u.nombreUsuario=? AND u.estatus=1")
	public Usuario buscarPorNombreUsuario(String nombreUsuario);

}
