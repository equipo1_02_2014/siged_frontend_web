package edu.ucla.siged.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import edu.ucla.siged.domain.gestioneventos.Sugerencia;

public interface DaoSugerenciaI extends JpaRepository<Sugerencia, Integer> {
	@Query("SELECT s FROM Sugerencia s WHERE s.estatusSugerencia=1")
	public Page<Sugerencia> buscarSugerenciasPendientes(Pageable p);
}