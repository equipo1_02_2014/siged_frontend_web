package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestioneventos.Noticia;

public interface DaoNoticiaI extends JpaRepository<Noticia, Integer> {
	
	@Query("Select n from Noticia n Where n.fecha BETWEEN ? AND ? ")
	public List<Noticia> porRango(Date desde, Date hasta);

}
