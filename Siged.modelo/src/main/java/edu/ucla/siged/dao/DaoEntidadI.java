package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.reporte.Entidad;


public interface DaoEntidadI extends JpaRepository<Entidad, Integer>  {

}
