package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface DaoEquipoI extends JpaRepository<Equipo, Integer> {
	//Version  09/02/2015 buscarEquipoActual
	@Query(
		"SELECT E FROM Equipo E, AtletaEquipo AE, Atleta A "
	   + "WHERE A.id = :idatleta "
	   + "AND E.id=AE.equipo.id "
	   + "AND A.id=AE.atleta.id "
	   + "AND AE.fechaInscripcion="
	   + "(SELECT MAX(AE.fechaInscripcion) FROM E, AE, A "
	   + "WHERE E.id=AE.equipo.id "
	   + "AND A.id=AE.atleta.id "
	   + "AND :idatleta = A.id)")
	public Equipo buscarEquipoActual(@Param("idatleta") Integer idatleta);
	//Version  09/02/2015 buscarEquipoActual2
	@Query(
			"SELECT E FROM Equipo E, AtletaEquipo AE, Atleta A "
		   + "WHERE A.id = :idatleta "
		   + "AND E.id=AE.equipo.id "
		   + "AND A.id=AE.atleta.id "
		   + "AND E.estatus = 1 OR E.fechaCierre < :fecha")
		   
		public Equipo buscarEquipoActual(@Param("idatleta") Integer idatleta, @Param("fecha") Date fecha);
	
	public List<Equipo> findByEstatus(Short estatus);
	
	@Query("SELECT E FROM Equipo E WHERE LOWER(E.nombre) LIKE LOWER(CONCAT(?1, '%'))"
			+ "AND LOWER(E.categoria.nombre) LIKE LOWER(CONCAT(?2, '%'))"
			+ "AND LOWER(E.rango.descripcion) LIKE LOWER(CONCAT(?3, '%'))"
			+ "AND LOWER(E.tipoEquipo.descripcion) LIKE LOWER(CONCAT(?4, '%'))"
			+ "AND E.estatus = ?5")
	public Page<Equipo> filtroEquipo(String nombre, String categoria, String rango, String tipoEquipo, Short estatus, Pageable pagina);
	
	@Query("SELECT COUNT(E) FROM Equipo E WHERE LOWER(E.nombre) LIKE LOWER(CONCAT(?1, '%'))"
			+ "AND LOWER(E.categoria.nombre) LIKE LOWER(CONCAT(?2, '%'))"
			+ "AND LOWER(E.rango.descripcion) LIKE LOWER(CONCAT(?3, '%'))"
			+ "AND LOWER(E.tipoEquipo.descripcion) LIKE LOWER(CONCAT(?4, '%'))"
			+ "AND E.estatus = ?5")
	public long filtroEquipoCantidad(String nombre, String categoria, String rango, String tipoEquipo, Short estatus);
	
	@Query("SELECT e FROM Equipo e,TipoEquipo t WHERE e.estatus=1 AND e.tipoEquipo.id=1 AND e.tipoEquipo.id=t.id")
	public List<Equipo> buscarEquiposRoster();
	
	@Query("SELECT E FROM Equipo E WHERE E.estatus = ?1")
	public Page<Equipo> buscarEquiposTrue(Short estatus, Pageable pagina);
	
	@Query("SELECT COUNT(E) FROM Equipo E WHERE E.estatus = ?1")
	public long cantidadEquiposTrue(Short estatus);
	
	//Para servicios Rest
	@Query("SELECT e FROM Equipo e WHERE e.categoria.id=? and e.rango.id=? and e.tipoEquipo.id=?"
			+ "and e.fechaCreacion between ? and ? AND e.estatus=1")
	public Equipo findByCategoriaRango(Integer idCategoria, Integer idRango, Integer idTipoEquipo, Date inicio, Date fin);
	
	@Query("SELECT E FROM Equipo E WHERE E.estatus = 1")
	public List<Equipo> buscarEquiposTrue();
	
	@Query("SELECT count(E) FROM Equipo E WHERE E.nombre = ?1")
	public long buscarNombre(String nombre);
	
	@Query("SELECT E FROM Equipo E, EquipoTecnico ET, Tecnico T "
			+ "WHERE E.id= ET.equipo.id "
			+ "AND ET.tecnico.id=T.id "
			+ "AND T.id = ?1 "
			+ "AND E.estatus=1")
	public Page<Equipo> filtroEquiposPorTecnico(Integer idTecnico, Pageable pagina);

	@Query("SELECT COUNT(E) FROM Equipo E, EquipoTecnico ET, Tecnico T "
			+ "WHERE E.id= ET.equipo.id "
			+ "AND ET.tecnico.id=T.id "
			+ "AND T.id = ?1 "
			+ "AND E.estatus=1")
	public long totalEquiposPorTecnico(Integer idTecnico);
	
	//Yessika
	public static int TAMANIO_PAGINA=5;
		
		/**
		 * Metodo que devuelve una lista de Equipos Vigentes a la Fecha Actual
		 * 
		 * */
		@Query("SELECT e FROM Equipo e WHERE "
				+ "CURRENT_DATE BETWEEN e.fechaCreacion AND "
				+ "e.fechaCierre")
		public List<Equipo> buscarEquiposVigentes();
		
		
		
		/**
		 * Metodo que devuelve una lista de Competencias Futuras y 
		 * Vigentes a la Fecha Actual
		 * 
		 * */
		@Query("SELECT e FROM Equipo e WHERE "
				+ "CURRENT_DATE <= e.fechaCierre")
		public List<Equipo> buscarEquiposFuturosYVigentes();
		
		
		/**
		 * Metodo que devuelve una lista de Competencias Futuras y 
		 * Vigentes a la Fecha Actual
		 * 
		 * */
		@Query("SELECT e FROM Equipo e WHERE "
				+ "CURRENT_DATE <= e.fechaCierre")
		public Page<Equipo> buscarEquiposFuturosYVigentes(Pageable p);
		
		@Query("SELECT COUNT(E) FROM Equipo E WHERE E.estatus = 1")
		public long totalEquiposActivos();
		
		//Agregado por Jesus 
		@Query("SELECT e FROM Equipo e,TipoEquipo te WHERE e.estatus=1 AND e.tipoEquipo.id=1 AND e.tipoEquipo.id=te.id AND "
				+ "e.id IN "
				+ "(SELECT et.equipo.id FROM EquipoTecnico et WHERE et.estatus=1) GROUP BY e.id")
		public List<Equipo> buscarEquiposRosterConTecnico();
}
