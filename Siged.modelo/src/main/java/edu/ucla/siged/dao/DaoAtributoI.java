package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.reporte.Atributo;
import edu.ucla.siged.domain.reporte.Entidad;

public interface DaoAtributoI extends JpaRepository<Atributo, Integer> {
	
	public List<Atributo> findByEntidad(Entidad entidad);
}
