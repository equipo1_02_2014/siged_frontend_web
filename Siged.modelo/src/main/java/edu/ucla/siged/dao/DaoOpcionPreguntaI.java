package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;

public interface DaoOpcionPreguntaI extends JpaRepository<OpcionPregunta, Integer> {

	/*@Query("SELECT Op FROM OpcionPregunta Op WHERE Op.idpregunta=?")
	List<OpcionPregunta> findByPregunta(Integer idpregunta);
*/
}
