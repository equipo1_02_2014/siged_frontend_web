package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestioneventos.Respuesta;

public interface DaoRespuestaI extends JpaRepository<Respuesta, Integer> {

	/*@Query("Select R from Respuesta R where idopcionpregunta=?")
	List<Respuesta> findByOpcionPregunta(Integer id);*/
	
	//Rest
		@Query("SELECT r FROM Respuesta r WHERE r.usuario.id=?1 AND r.opcionPregunta.id=?2")
		public Respuesta verificarRespuestaEncuesta(Integer idUsuario, Integer idOpcionPregunta);

		@Query("SELECT r FROM Respuesta r WHERE r.opcionPregunta.id=?2")
		public Respuesta verificarRespuestaEncuestaPublica(Integer idOpcionPregunta);
		
		@Query("SELECT r FROM Respuesta r WHERE r.usuario.id=?1 AND r.opcionPregunta.id=?2")
		public Respuesta verificarRespuestaPregunta(Integer idUsuario, Integer idOpcionPregunta);
		
		@Query("SELECT COUNT(r.opcionPregunta.id) FROM Respuesta r WHERE r.opcionPregunta.id=? ")
		public Object contarRespuestasPorOpcionPregunta(Integer idOpcionPregunta);
		


}
