package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;

public interface DaoHorarioPracticaI extends JpaRepository<HorarioPractica, Integer> {

	HorarioPractica findByEquipoAndDia(Equipo equipo, int dia);
	public List<HorarioPractica> findByEquipo(Equipo equipo);
}
