package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;

public interface DaoJuegoI extends JpaRepository<Juego, Integer> {
	
	
	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND c.id=j.competencia.id AND j.equipo.id=?1 AND "
			+ "j.fecha=?2 AND (j.horaInicio BETWEEN ?3 AND ?4)")
	public List<Juego> buscarJuegosPorEquipoTiempo(Integer idEquipo,Date fecha,Date hora,Date horaFinTentativa);
	
	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND c.id=j.competencia.id AND j.area.id=?1 AND j.fecha=?2 AND "
			+ "(j.horaInicio BETWEEN ?3 AND ?4)")
	public List<Juego> buscarJuegosPorAreaTiempo(Integer idArea,Date fecha,Date hora,Date horaFinTentativa);
	
	
	@Query("SELECT a FROM Arbitro a WHERE a.estatus=1 AND a.id NOT IN"
			+ "(SELECT a.id FROM Arbitro a,ArbitroJuego aj,Juego j,Competencia c WHERE c.estatus=1 AND "
			+ "j.fecha=?1 AND (j.horaInicio BETWEEN ?2 AND ?3) AND a.estatus=1 "
			+ "AND aj.arbitro.id=a.id AND aj.juego.id=j.id AND c.id=j.competencia.id)")
	public List<Arbitro> buscarArbitrosDisponibles(Date fecha,Date hora,Date horaFinTentativa);
	
	@Query("SELECT a FROM Anotador a WHERE a.estatus=1 AND a.id NOT IN"
			+ "(SELECT a.id FROM Anotador a,Juego j,Competencia c WHERE c.estatus=1 AND "
			+ "j.fecha=?1 AND (j.horaInicio BETWEEN ?2 AND ?3) AND a.estatus=1 "
			+ "AND a.id=j.anotador.id AND c.id=j.competencia.id)")
	public List<Anotador> buscarAnotadoresDisponibles(Date fecha,Date hora,Date horaFinTentativa);
	
	
	@Query("SELECT j FROM Competencia c,Juego j WHERE c.id=?1 AND "
			+ "j.fecha < CURRENT_DATE AND c.estatus=1 AND c.id=j.competencia.id")
	public List<Juego> buscarJuegosCompetenciaPasados(Integer idCompetencia);
	
	
	@Query("SELECT j FROM Competencia c,Juego j WHERE c.id=?1 AND "
			+ "j.fecha = CURRENT_DATE AND j.horaInicio < CURRENT_TIME AND "
			+ "c.estatus=1 AND c.id=j.competencia.id")
	public List<Juego> buscarJuegosCompetenciaHoyHoraPasada(Integer idCompetencia);
	
	@Query("SELECT aj FROM Juego j,ArbitroJuego aj,PosicionArbitro pa WHERE j.id=?1 AND j.id=aj.juego.id "	                                                                     + "AND aj.posicionArbitro.id=pa.id ORDER BY pa.id")
	public List<ArbitroJuego> obtenerArbitroJuego(Integer idJuego);
	

	@Query("SELECT j FROM Juego j,Arbitro a, ArbitroJuego aj,Competencia c WHERE a.id=?1 AND fecha=?2 AND "
			+ "(j.horaInicio BETWEEN ?3 AND ?4) AND a.estatus=1 AND c.estatus=1 AND c.id=j.competencia.id AND j.id=aj.juego.id AND a.id=aj.arbitro.id")
	public List<Juego> buscarJuegosDeArbitroPorFechaAndHora(Integer idArbitro,Date fecha,Date horaInicio,Date horaFinTentativa);

	
	@Query("SELECT j FROM Anotador a,Juego j,Competencia c WHERE j.fecha=?2 AND " 
			+ "(j.horaInicio BETWEEN ?3 AND ?4) AND "
			+ "a.id=?1 AND a.estatus=1 AND c.estatus=1 AND c.id=j.competencia.id AND a.id=j.anotador.id)")
	public List<Juego> buscarJuegosDeAnotadorPorFechaAndHora(Integer idAnotador,Date fecha,Date horaInicio,Date horaFinTentativa);
	

	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND j.competencia.id=c.id AND j.fecha < CURRENT_DATE "
			+ "OR j.id IN (SELECT g.id FROM Juego g,Competencia e WHERE e.estatus=1 AND g.competencia.id=e.id AND g.fecha = CURRENT_DATE AND g.horaInicio < CURRENT_TIME)")
	public Page<Juego> buscarJuegosPasados(Pageable p);
	

	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND j.competencia.id=c.id AND j.fecha < CURRENT_DATE "
			+ "OR j.id IN (SELECT g.id FROM Juego g,Competencia e WHERE e.estatus=1 AND g.competencia.id=e.id AND g.fecha = CURRENT_DATE AND g.horaInicio < CURRENT_TIME)")
	public List<Juego> buscarJuegosPasados();
	
	@Query("SELECT j FROM Juego j WHERE j.fecha < CURRENT_DATE AND j.resultado=-1 "
			+ "OR j.id IN (SELECT g.id FROM Juego g WHERE  g.fecha = CURRENT_DATE AND g.horaInicio < CURRENT_TIME)")
	public Page<Juego> buscarJuegosPasadosSinResultado(Pageable p);
	
	@Query("SELECT j FROM Juego j WHERE j.fecha < CURRENT_DATE AND j.resultado=-1 "
			+ "OR j.id IN (SELECT g.id FROM Juego g WHERE  g.fecha = CURRENT_DATE AND g.horaInicio < CURRENT_TIME)")
	public List<Juego> buscarJuegosPasadosSinResultado();
	
	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND c.id=j.competencia.id AND j.fecha > CURRENT_DATE "
			+ "OR j.id IN (SELECT g.id FROM Juego g,Competencia e WHERE e.estatus=1 AND g.competencia.id=e.id AND g.fecha = CURRENT_DATE)")
	public Page<Juego> buscarJuegosFuturos(Pageable p);
	
	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND c.id=j.competencia.id AND j.fecha > CURRENT_DATE "
			+ "OR j.id IN (SELECT g.id FROM Juego g,Competencia e WHERE e.estatus=1 AND e.id=g.competencia.id AND g.fecha = CURRENT_DATE)")
	public List<Juego> buscarJuegosFuturos();
	
	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND c.id=j.competencia.id")
	public Page<Juego> buscarTodosCompetenciasActivas(Pageable p);
	
	@Query("SELECT j FROM Juego j,Competencia c WHERE c.estatus=1 AND c.id=j.competencia.id")
	public List<Juego> buscarTodosCompetenciasActivas();
	
	//Solicitado para verificar la existencia de un Area en algun juego
	@Query("SELECT j FROM Juego j WHERE j.area.id=?1")
	public List<Juego> buscarAreaJuego(Integer idarea);
	
	//Agregados para Vistas propias del Usuarios
	
	//Para Representantes que deseean ver Juegos de sus Representados
	@Query("SELECT j FROM Juego j,Equipo e " 
	        +        "WHERE j.equipo.id=e.id AND "
            +              "j.fecha >= CURRENT_DATE AND "
	        +               "e.id IN "
            +   "(SELECT eq.id FROM Equipo eq,Representante r,AtletaRepresentante ar,Atleta a,AtletaEquipo ae "
            +                  "WHERE r.cedula=:cedula AND "
            +                         "r.id=ar.representante.id AND "
            +                         "a.id=ar.atleta.id AND "
            +                         "a.id=ae.atleta.id AND "
            +                         "eq.id=ae.equipo.id AND "
            +                         "eq.estatus=1)")
	public Page<Juego> buscarJuegosPorRepresentante(@Param("cedula") String cedulaRepresentante,Pageable p);
	
	//Para Representantes que deseean ver Juegos de sus Representados
		@Query("SELECT COUNT(j) FROM Juego j,Equipo e " 
		        +        "WHERE j.equipo.id=e.id AND "
	            +              "j.fecha >= CURRENT_DATE AND "
		        +               "e.id IN "
	            +   "(SELECT eq.id FROM Equipo eq,Representante r,AtletaRepresentante ar,Atleta a,AtletaEquipo ae "
	            +                  "WHERE r.cedula=:cedula AND "
	            +                         "r.id=ar.representante.id AND "
	            +                         "a.id=ar.atleta.id AND "
	            +                         "a.id=ae.atleta.id AND "
	            +                         "eq.id=ae.equipo.id AND "
	            +                         "eq.estatus=1)")
		public long obtenerCantidadJuegosPorRepresentante(@Param("cedula") String cedulaRepresentante);
	
	//Para Tecnicos que deseean ver Juegos de los Equipos a los que Entrena
	
	@Query("SELECT j FROM Juego j,Equipo e " 
	        +        "WHERE j.equipo.id=e.id AND "
            +              "j.fecha >= CURRENT_DATE AND "
	        +               "e.id IN "
            +   "(SELECT eq.id FROM Equipo eq,"
            +                          "Tecnico t,"
            +                          "EquipoTecnico et "
            +                     "WHERE t.cedula=:cedula AND "
            +                           "t.id=et.tecnico.id AND "
            +                           "eq.id=et.equipo.id AND "
            +                           "et.estatus=1 AND "
            +                           "t.estatus=1 AND "
            +                           "eq.estatus=1)")
	public Page<Juego> buscarJuegosPorTecnico(@Param("cedula")String cedulaTecnico,Pageable p);
	
	@Query("SELECT COUNT(j) FROM Juego j,Equipo e " 
	        +        "WHERE j.equipo.id=e.id AND "
            +              "j.fecha >= CURRENT_DATE AND "
	        +               "e.id IN "
            +   "(SELECT eq.id FROM Equipo eq,"
            +                          "Tecnico t,"
            +                          "EquipoTecnico et "
            +                     "WHERE t.cedula=:cedula AND "
            +                           "t.id=et.tecnico.id AND "
            +                           "eq.id=et.equipo.id AND "
            +                           "et.estatus=1 AND "
            +                           "t.estatus=1 AND "
            +                           "eq.estatus=1)")
	public long obtenerCantidadJuegosPorTecnico(@Param("cedula")String cedulaTecnico);
	
	
	@Query("SELECT j FROM Juego j,Equipo e " 
	        +        "WHERE j.equipo.id=e.id AND "
            +              "j.fecha >= CURRENT_DATE AND "
	        +               "e.id IN "
            +   "(SELECT eq.id FROM Equipo eq,"
            +                         "Atleta a,"
            +                         "AtletaEquipo ae "
            +                     "WHERE a.cedula=:cedula AND "
            +                           "a.id=ae.atleta.id AND "
            +                           "eq.id=ae.equipo.id AND "
            +                           "eq.estatus=1)")
	public Page<Juego> buscarJuegosPorAtleta(@Param("cedula")String cedulaAtleta,Pageable p);
	

	@Query("SELECT COUNT(j) FROM Juego j,Equipo e " 
	        +        "WHERE j.equipo.id=e.id AND "
            +              "j.fecha >= CURRENT_DATE AND "
	        +               "e.id IN "
            +   "(SELECT eq.id FROM Equipo eq,"
            +                         "Atleta a,"
            +                         "AtletaEquipo ae "
            +                     "WHERE a.cedula=:cedula AND "
            +                           "a.id=ae.atleta.id AND "
            +                           "eq.id=ae.equipo.id AND "
            +                           "eq.estatus=1)")
	public long obtenerCantidadJuegosPorAtleta(@Param("cedula")String cedulaAtleta);
	
	//Rest
			@Query("SELECT j FROM Juego j WHERE j.competencia.id = ? ORDER BY j.fecha")
			public List<Juego> juegosPorCompetencia(Integer idcompetencia );
			
			@Query("SELECT j FROM Juego j WHERE j.competencia.id = ? AND j.fecha<? ORDER BY j.fecha ")
			public List<Juego> juegosPorCompetenciaJugados(Integer idcompetencia,Date fechasistema );
			
			@Query("SELECT j FROM Juego j WHERE j.competencia.id = ? AND j.fecha>=? ORDER BY j.fecha ")
			public List<Juego> juegosPorCompetenciaProximos(Integer idcompetencia,Date fechasistema );
			
			
			
			@Query("SELECT j FROM Juego j WHERE j.equipo.categoria.id =? AND j.fecha<?  ORDER BY j.fecha ASC")
			public List<Juego> juegosPorCategoriaJugados(Integer idcategoria, Date fechasistema );
			
			@Query("SELECT j FROM Juego j WHERE j.equipo.categoria.id =? AND j.fecha>=?  ORDER BY j.fecha ASC")
			public List<Juego> juegosPorCategoriaProximos(Integer idcategoria, Date fechasistema );
			
			
			
			@Query("SELECT j FROM Juego j WHERE j.equipo.categoria.id = ? AND j.competencia.id = ? AND j.fecha<? ORDER BY j.fecha ASC")
			public List<Juego> juegosPorCategoriaCompetenciaJugados(Integer idcategoria, Integer idcompetencia, Date fechasistema );
			
			@Query("SELECT j FROM Juego j WHERE j.equipo.categoria.id = ? AND j.competencia.id = ? AND j.fecha>=? ORDER BY j.fecha ASC")
			public List<Juego> juegosPorCategoriaCompetenciaProximos(Integer idcategoria, Integer idcompetencia, Date fechasistema );
			
			
			@Query("Select j from Juego j Where j.fecha BETWEEN ? AND ? and j.equipo.id=?")
			public List<Juego> porRango(Date desde, Date hasta, Integer idEquipo);
			
			
			@Query("SELECT j FROM Juego j WHERE j.fecha > ? and j.equipo.id=?")
			public List<Juego> juegosPendientes(Date fechasistema, Integer idEquipo);
			
			//Overload Juegos Jugados
			@Query("SELECT j FROM Juego j WHERE j.fecha < ? AND j.resultado!=-1 and j.equipo.id=?")
			public List<Juego> juegosJugados(Date fechasistema, Integer idEquipo);
			
//			@Query("SELECT j FROM Juego j WHERE j.fecha < ? AND j.resultado!=-1 ORDER BY j.fecha, j.horaInicio")
//			public List<Juego> juegosJugados(Date fechasistema);
			
			@Query("SELECT j FROM Juego j WHERE j.fecha < ? ORDER BY j.fecha, j.horaInicio")
			public List<Juego> juegosJugados(Date fechasistema);
			
			@Query("SELECT j FROM Juego j WHERE j.fecha >= ? ORDER BY j.fecha, j.horaInicio")
			public List<Juego> juegosProximos(Date fechasistema);
	
	
	
}