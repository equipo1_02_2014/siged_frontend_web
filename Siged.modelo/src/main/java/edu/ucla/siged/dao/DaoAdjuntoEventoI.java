package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;

public interface DaoAdjuntoEventoI extends JpaRepository<AdjuntoEvento, Integer> {

}
