/** 
 * 	DaoAreaI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;

public interface DaoAreaI extends JpaRepository<Area, Integer> {
	
	public List<Area> findByEstatusTrue();
	public List<Area> findByEstatus(Short estatus);
}