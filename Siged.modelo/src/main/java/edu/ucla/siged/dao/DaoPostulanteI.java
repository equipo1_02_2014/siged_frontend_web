package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.Postulante;

public interface DaoPostulanteI extends JpaRepository<Postulante, Integer> {
	@Query("SELECT p FROM Postulante p WHERE p.estatus=1")
	public Page<Postulante> buscarPostulantesPendientes(Pageable p);
	
	@Query("SELECT p FROM Postulante p WHERE p.estatus=2")
	public Page<Postulante> buscarPostulantesAprobados(Pageable p);

	@Query("SELECT p FROM Postulante p WHERE p.causa.id=?1")
	public List<Postulante> buscarCausaPTrue(Integer id);
	
	//Agregado por Jesus
	@Query("SELECT p FROM Postulante p WHERE p.fechaPostulacion BETWEEN ?1 AND ?2")
	public List<Postulante> buscarPostulantesPorRangoFecha(Date fechaInicio,Date fechaFin);
}