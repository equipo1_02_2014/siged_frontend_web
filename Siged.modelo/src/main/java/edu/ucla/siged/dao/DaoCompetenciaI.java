package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;

public interface DaoCompetenciaI extends JpaRepository<Competencia, Integer> {
	
	public static int TAMANIO_PAGINA=5;

	//Metodo que devuelve una lista de Competencias Vigentes a la Fecha Actual
	@Query("SELECT c FROM Competencia c WHERE "
			+ "c.estatus=1 AND CURRENT_DATE BETWEEN c.fechaInicio AND "
			+ "c.fechaFin")
	public List<Competencia> buscarCompetenciasVigentes();
	
	//Metodo que devuelve una lista de Competencias Futuras y Vigentes a la Fecha Actual
	@Query("SELECT c FROM Competencia c WHERE "
			+ "c.estatus=1 AND CURRENT_DATE <= c.fechaFin")
	public List<Competencia> buscarCompetenciasFuturasYVigentes();
	
	
	//Metodo que devuelve una lista de Competencias Futuras y Vigentes a la Fecha Actual
	@Query("SELECT c FROM Competencia c WHERE "
			+ "c.estatus=1 AND CURRENT_DATE <= c.fechaFin")
	public Page<Competencia> buscarCompetenciasFuturasYVigentes(Pageable p);

	
	//Metodos Agregados por Ysabel Oviedo
	@Query("SELECT E FROM Competencia E WHERE E.estatus = 1")
	public Page<Competencia> buscarCompetenciasActivas(Pageable p);
	
	@Query("SELECT COUNT(E) FROM Competencia E WHERE E.estatus = 1")
	public long totalCompetenciasActivas(); 
	
	//Yessi
	@Query("SELECT c FROM Competencia c,Juego j WHERE c.estatus=1 AND "
			+ "j.equipo.id=?1 AND c.id=j.competencia.id")
	public List<Competencia> buscarCompetenciaPorEquipo(Integer idequipo);
}
