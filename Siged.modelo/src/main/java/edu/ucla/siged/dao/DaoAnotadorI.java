/** 
 * 	DaoAnotadorI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;

public interface DaoAnotadorI extends JpaRepository<Anotador, Integer> {
	
	public List<Anotador> findByEstatusTrue();
	public List<Anotador> findByEstatus(Short estatus);
	@Query("select count(a) from Anotador a where a.cedula = ?1 and a.id <> ?2")
	public Long countByCedula(String cedula, Integer id);
	@Query("select count(a) from Juego a where a.anotador = ?1")
	public Long countByAnotador(Anotador anotador);
	public Anotador findByCedula(String cedula);
}