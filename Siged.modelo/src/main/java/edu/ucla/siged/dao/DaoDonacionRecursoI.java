package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;

public interface DaoDonacionRecursoI extends
		JpaRepository<DonacionRecurso, Integer> {

}
