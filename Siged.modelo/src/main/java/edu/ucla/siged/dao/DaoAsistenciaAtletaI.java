package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestiondeportiva.AsistenciaAtleta;

public interface DaoAsistenciaAtletaI extends JpaRepository<AsistenciaAtleta, Integer> {

	
}
