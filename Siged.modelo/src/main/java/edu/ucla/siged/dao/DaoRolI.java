package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.domain.seguridad.Rol;

public interface DaoRolI extends JpaRepository<Rol, Integer> {

	@Query ("SELECT R FROM Rol R WHERE estatus=:est")
	public List<Rol> findByEstatus(@Param ("est") Short estatus);
	public Rol findByNombre(@Param ("nomb") String nombre);
	
	//Rest
	@Query("SELECT rs.rol FROM RolUsuario rs WHERE rs.usuario.id=? AND rs.estatus=1")
	public List<Rol> obtenerRolesPorUsuario(Integer idUsuario);
	
	
}
