package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego;

public interface DaoArbitroJuegoI extends JpaRepository<ArbitroJuego, Integer> {

}
