package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.Rol;

public interface DaoControlAccesoI extends JpaRepository<ControlAcceso, Integer> {

	List<ControlAcceso> findByFuncionalidad(Funcionalidad funcionalidad);

	List<ControlAcceso> findByRol(Rol rol);
	
	//Rest
	@Query("SELECT ca FROM ControlAcceso ca WHERE ca.rol.id=? AND ca.estatus=1")
	public List<ControlAcceso> obtenerPorRolId(Integer id);

}
