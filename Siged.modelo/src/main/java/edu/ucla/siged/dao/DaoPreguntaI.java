package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;

public interface DaoPreguntaI extends JpaRepository<Pregunta, Integer> {
	
	@Query("SELECT Distinct P, OP, R FROM Pregunta as P, OpcionPregunta as OP, Respuesta as R WHERE P.id=OP.id AND OP.id=R.id") 
	public List<Object[]> getPreguntasConRespuesta();

	/*@Query("SELECT P FROM Pregunta P WHERE P.idencuesta=?")
	public List<Pregunta> findByEncuesta(Integer idencuesta);
*/
}