package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;

public interface DaoRolUsuarioI extends JpaRepository<RolUsuario, Integer> {


	public List<RolUsuario> findByUsuario(Usuario usuario);
	public List<RolUsuario> findByRol(Rol rol);
	
	//Rest
	@Query("SELECT ru FROM RolUsuario ru WHERE ru.usuario.id=? AND ru.estatus=1")
	public RolUsuario buscarPorUsuarioId(Integer idUsuario);
	
	@Query("SELECT ru FROM RolUsuario ru WHERE ru.usuario.id=? AND ru.estatus=1")
	public List<RolUsuario> buscarListaPorUsuarioId(Integer idUsuario);
}
