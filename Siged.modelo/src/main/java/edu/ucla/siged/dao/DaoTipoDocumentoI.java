package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestionatleta.TipoDocumento;

public interface DaoTipoDocumentoI extends JpaRepository<TipoDocumento, Integer> {

}
