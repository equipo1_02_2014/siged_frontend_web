package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;

public interface DaoEstatusAtletaI extends JpaRepository<EstatusAtleta, Integer> {

	@Query("SELECT EA FROM EstatusAtleta EA WHERE EA.id!=0")
	public List<EstatusAtleta> buscarEstatusAtletaNoEliminados();
}
