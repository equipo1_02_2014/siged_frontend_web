package edu.ucla.siged.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class DaoFiltro<T> {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<T> buscarFiltrados(String hql, int tamanoPagina, int numeroPagina){
		
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery(hql); //daoEquipo.findAll();
		query.setMaxResults(tamanoPagina);
		query.setFirstResult(numeroPagina * tamanoPagina);
		List<T> lista =query.list(); //daoEquipo.findAll();
		session.close();
		return lista;
		
	}
	
	public Long totalFiltrados(String hql){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) "+hql); //daoEquipo.findAll();
		long total= (Long) query.uniqueResult(); //daoEquipo.findAll();
		session.close();
		return total;
	}
	
	public List<T> buscarTodos(String hql){
		
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery(hql); //daoEquipo.findAll();
		List<T> lista =query.list(); //daoEquipo.findAll();
		session.close();
		return lista;
		
	}

}
