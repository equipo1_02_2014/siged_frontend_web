package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.utilidades.EstadoCivil;

public interface DaoEstadoCivilI extends JpaRepository<EstadoCivil, Integer> {

}
