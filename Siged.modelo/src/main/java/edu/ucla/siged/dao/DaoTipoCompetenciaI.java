package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;

public interface DaoTipoCompetenciaI extends JpaRepository<TipoCompetencia, Integer> {

}
