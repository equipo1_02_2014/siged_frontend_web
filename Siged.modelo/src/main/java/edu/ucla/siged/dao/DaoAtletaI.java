package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Reposo;

public interface DaoAtletaI extends JpaRepository<Atleta, Integer> {
	
	public Atleta findByCedula(String cedula);
	
	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA "
			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
		    + "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND HA.estatus=1 "
			+ "AND EA.id!=0 "
			+ "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
	public Page<Atleta> filtroAtleta(String cedula, String nombre, String apellido, Pageable pagina);

	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA "
   		    + "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND HA.estatus=1 "
			+ "AND EA.id!=0 ")
    public long FiltroAtletaCantidad(String cedula, String nombre, String apellido);

	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA "
			  + "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			  + "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
			  + "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			  + "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			  + "AND HA.estatus=1 "
			  + "AND EA.id=?4 "
			  + "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
	public Page<Atleta> filtroAtletaMasEstatusAtleta(String cedula, String nombre, String apellido, Integer idEstatusAtleta, Pageable pagina);

	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA "
			  + "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			  + "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
			  + "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			  + "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			  + "AND HA.estatus=1 "
			  + "AND EA.id=?4")
	public long filtroAtletaMasEstatusAtletaCantidad(String cedula, String nombre, String apellido, Integer idEstatusAtleta);

//	1 
	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA "
			  + "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			  + "AND HA.estatus=1 "
			  + "AND EA.id!=0 "
			  + "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
	public Page<Atleta> buscarAtletasEstatusNoEliminados(Pageable pagina);
//  
	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA "
			  + "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			  + "AND HA.estatus=1 "
			  + "AND EA.id!=0 ")
	public long cantidadAtletasEstatusNoEliminado();

//	2 ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO
	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
			+ "AtletaEquipo AE, Equipo E "
			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
		    + "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
			+ "AND HA.estatus=1 "
			+ "AND EA.id!=0 "
			+ "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
	public Page<Atleta> filtroAtleta(String cedula, String nombre, String apellido, String nombreEquipo, Pageable pagina);

//	CANTIDAD DE ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO	
	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
			+ "AtletaEquipo AE, Equipo E "
			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
			+ "AND HA.estatus=1 "
			+ "AND EA.id!=0 ")
    public long FiltroAtletaCantidad(String cedula, String nombre, String apellido, String nombreEquipo);

//	3 ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO Y POR Y ESTATUS ATLETA
	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
			+ "AtletaEquipo AE, Equipo E "  
			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
			+ "AND HA.estatus=1 "
			+ "AND EA.id=?5 "
			+ "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
	public Page<Atleta> filtroAtletaMasEstatusAtleta(String cedula, String nombre, String apellido,String nombreEquipo, Integer idEstatusAtleta, Pageable pagina);

//	CANTIDAD DE ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO Y ESTATUS ATLETA 	
	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
			+ "AtletaEquipo AE, Equipo E "   
			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
			+ "AND HA.estatus=1 "
			+ "AND EA.id=?5")
	public long filtroAtletaMasEstatusAtletaCantidad(String cedula, String nombre, String apellido,String nombreEquipo, Integer idEstatusAtleta);

//	@Query("SELECT r FROM Reposo r, Atleta a WHERE a.id=r.atleta.id AND  a.id=? ")
//	public List<Reposo> getRepososPoAtleta(Integer id);
////	4 ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO Y POR BECADO	
//	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
//			+ "AtletaEquipo AE, Equipo E, "
//			+ "HistoriaAyuda HAY "
//			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
//			+ "AND HAY.atleta.id = A.id "
//			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
//			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
//		    + "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
//			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
//			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
//			+ "AND HA.estatus=1 "
//			+ "AND EA.id!=0 "
//			+ "AND ?5 BETWEEN HAY.fechaInicio AND HAY.fechaFin "
//			+ "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
//	public Page<Atleta> filtroAtletaBecados(String cedula, String nombre, String apellido,String nombreEquipo, Date fecha, Pageable pagina);
//	
////	CANTIDAD DE ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO Y POR BECADO 
//	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
//			+ "AtletaEquipo AE, Equipo E, "
//			+ "HistoriaAyuda HAY "
//			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
//			+ "AND HAY.atleta.id = A.id "
//			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
//			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
//		    + "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
//			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
//			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
//			+ "AND HA.estatus=1 "
//			+ "AND EA.id!=0 "
//			+ "AND ?5 BETWEEN HAY.fechaInicio AND HAY.fechaFin ")
//	public long filtroAtletaBecadosCantidad(String cedula, String nombre, String apellido,String nombreEquipo, Date fecha);
//
////	5 ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO, ESTATUS ATLETA Y POR BECADO
//	@Query("SELECT A FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
//			+ "AtletaEquipo AE, Equipo E, "
//			+ "HistoriaAyuda HAY "  
//			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
//			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
//			+ "AND HAY.atleta.id = A.id "
//			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
//			+ "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
//			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
//			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
//			+ "AND HA.estatus=1 "
//			+ "AND EA.id=?5 "
//			+ "AND ?6 BETWEEN HAY.fechaInicio AND HAY.fechaFin "
//			+ "GROUP BY A.id ORDER BY A.fechaAdmision DESC")
//	public Page<Atleta> filtroAtletaBecadosMasEstatusAtleta(String cedula, String nombre, String apellido,String nombreEquipo, Integer idEstatusAtleta, Date fecha, Pageable pagina);
//
////	CANTIDAD DE ATLETAS FILTRADOS POR CEDULA,NOMBRE,APELLIDO,NOMBRE EQUIPO, ESTATUS ATLETA Y POR BECADO 	
//	@Query("SELECT COUNT(A) FROM Atleta A, HistoriaAtleta HA, EstatusAtleta EA, "
//			+ "AtletaEquipo AE, Equipo E, "
//			+ "HistoriaAyuda HAY "  
//			+ "WHERE A.id=HA.atleta.id AND HA.estatusAtleta.id=EA.id "
//			+ "AND A.id=AE.atleta.id AND AE.equipo.id=E.id "
//			+ "AND HAY.atleta.id = A.id "
//			+ "AND LOWER(A.cedula) LIKE LOWER(CONCAT(?1, '%')) "
//			+ "AND LOWER(A.nombre) LIKE LOWER(CONCAT(?2, '%')) "
//			+ "AND LOWER(A.apellido) LIKE LOWER(CONCAT(?3, '%')) "
//			+ "AND LOWER(E.nombre) LIKE LOWER(CONCAT(?4, '%')) "
//			+ "AND HA.estatus=1 "
//			+ "AND EA.id=?5 "
//			+ "AND ?6 BETWEEN HAY.fechaInicio AND HAY.fechaFin")
//	public long filtroAtletaMasEstatusAtletaCantidad(String cedula, String nombre, String apellido,String nombreEquipo, Integer idEstatusAtleta, Date fecha);

}
