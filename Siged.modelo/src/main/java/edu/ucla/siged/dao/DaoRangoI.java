package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestionequipo.Rango;

public interface DaoRangoI extends JpaRepository<Rango, Integer> {

}
