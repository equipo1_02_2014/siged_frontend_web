package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;

public interface DaoTipoEquipoI extends JpaRepository<TipoEquipo, Integer> {

}
