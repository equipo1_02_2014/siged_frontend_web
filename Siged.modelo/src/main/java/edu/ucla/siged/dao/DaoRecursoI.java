package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.ucla.siged.domain.gestioneventos.Recurso;

public interface DaoRecursoI extends JpaRepository<Recurso, Integer> {
}
