package edu.ucla.siged.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;

public interface DaoActividadPracticaI extends JpaRepository<ActividadPractica,Integer> {
	@Query("SELECT a FROM ActividadPractica a WHERE a.causa.id=?1")
	public List<ActividadPractica> buscarCausaATrue(Integer id);
	
	//Se agrega para Verificar si alguna Area es usada por alguna Actividad
	@Query("SELECT ap FROM ActividadPractica ap WHERE ap.area.id=?1")
	public List<ActividadPractica> buscarAreaEnActividad(Integer idArea);
}
