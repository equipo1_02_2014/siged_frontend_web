package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;
import edu.ucla.siged.domain.gestiondeportiva.CambioTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

public interface DaoCambioTecnicoI extends JpaRepository<CambioTecnico, Integer> {
  
	public List<CambioTecnico> findByActividadPractica(ActividadPractica actividadPractica);
	

	/*@Query("SELECT t FROM Tecnico t,CambioTecnico ct,Practica p,EquipoTecnico eq WHERE "
			+ "eq.responsabilidad=1 AND p.id=?1 AND ct.practica.id=p.id AND "
			+ "ct.tecnicoSuplente.id=t.id AND ct.equipoTecnico.id=eq.id AND p.estatus=1 AND t.estatus=1 AND eq.estatus=1")
	public Tecnico buscarTecnicoPrincipalPorPractica(Integer idPractica);
	
	
	@Query("SELECT t FROM Tecnico t,CambioTecnico ct,Practica p,EquipoTecnico eq WHERE "
			+ "eq.responsabilidad=2 AND p.id=?1 AND ct.practica.id=p.id AND "
			+ "ct.tecnicoSuplente.id=t.id AND ct.equipoTecnico.id=eq.id AND p.estatus=1 AND t.estatus=1 AND eq.estatus=1")
	public Tecnico buscarTecnicoAsistentePorPractica(Integer idPractica);
	
	@Query("SELECT t FROM Tecnico t WHERE t.id NOT IN(SELECT t.id FROM Tecnico t,CambioTecnico ct,Practica p WHERE "
			+ "p.fecha=?1 AND p.horaInicio=?2 AND ct.practica.id=p.id AND "
			+ "ct.tecnicoSuplente.id=t.id AND p.estatus=1 AND t.estatus=1)")
	public List<Tecnico> buscarTecnicosDisponibles(Date fecha,Date hora);
	
	
	@Query("SELECT ct FROM CambioTecnico ct,Practica p,EquipoTecnico eq WHERE "
			+ "p.id=?1 AND eq.responsabilidad=1 AND ct.practica.id=p.id AND "
			+ "ct.equipoTecnico.id=eq.id")
	public CambioTecnico buscarCambioTecnicoPrincipalPorPractica(Integer idPractica);
	
	@Query("SELECT ct FROM CambioTecnico ct,Practica p,EquipoTecnico eq WHERE "
			+ "p.id=?1 AND eq.responsabilidad=2 AND ct.practica.id=p.id AND "
			+ "ct.equipoTecnico.id=eq.id")
	public CambioTecnico buscarCambioTecnicoAsistentePorPractica(Integer idPractica);*/
	
	@Query("SELECT c FROM CambioTecnico c WHERE c.tecnicoSuplente.id=?1")
	public List<CambioTecnico> buscarTecnicoCTrue(Integer id);
	
}
