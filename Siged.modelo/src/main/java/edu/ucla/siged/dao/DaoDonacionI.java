package edu.ucla.siged.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.ucla.siged.domain.gestioneventos.Donacion;

public interface DaoDonacionI extends JpaRepository<Donacion, Integer> {

}
