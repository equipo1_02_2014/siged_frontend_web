package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

public interface DaoEquipoTecnicoI extends JpaRepository<EquipoTecnico, Integer> {

	List<EquipoTecnico> findByEquipoAndEstatus(Equipo equipo, short estatus);
	
	@Query("SELECT E FROM EquipoTecnico E WHERE E.tecnico.id =?1")
	public List<EquipoTecnico> buscarTecnicoETrue(Integer id);
	
	@Query("SELECT ET FROM EquipoTecnico ET WHERE ET.estatus = ?1")
	public Page<EquipoTecnico> buscarEquipoTecnicosTrue(Short estatus, Pageable pagina);
	
    @Query("SELECT T FROM EquipoTecnico ET, Tecnico T, Equipo E WHERE E.id = ?1 and ET.responsabilidad=?2 and ET.equipo.id = E.id  and T.id = ET.tecnico.id  and  T.estatus = 1")
	Tecnico obtenerTecnicoDao(Integer id, Short responsabilidad);

    @Query("SELECT T FROM Tecnico T, EquipoTecnico ET, Equipo E WHERE E = ?1 AND ET.equipo.id = E.id AND ET.tecnico.id = T.id")    
    public List<Tecnico> obtenerTecnicosEquipo(Equipo equipo);

    @Query("SELECT ET FROM EquipoTecnico ET, Equipo E, Tecnico T WHERE ET.tecnico=?1 AND ET.equipo=?2 AND ET.equipo.id = E.id AND ET.tecnico.id = T.id")
    EquipoTecnico obtenerTecnicoEquipo(Tecnico tecnico, Equipo equipo);
}
