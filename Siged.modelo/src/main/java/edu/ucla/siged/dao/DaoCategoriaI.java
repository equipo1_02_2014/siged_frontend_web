package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionequipo.Categoria;

public interface DaoCategoriaI extends JpaRepository<Categoria, Integer> {

	//Agregado Por Jesus
		@Query("SELECT c FROM Categoria c WHERE c.estatus=1 AND ?1 BETWEEN c.edadMinima AND c.edadMaxima")
		public Categoria buscarCategoriaPorEdad(Integer edad);
		
		public List<Categoria> findByEstatus(Short estatus);
}