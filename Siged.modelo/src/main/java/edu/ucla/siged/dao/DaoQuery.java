package edu.ucla.siged.dao;

import java.util.List;

import org.hibernate.Cache;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class DaoQuery {
	@Autowired
	private SessionFactory sessionFactory;
	
	//################### Jesus
	public List ejecutarQueryPromedioCantidad(String hql){
		
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("descripcion",StandardBasicTypes.STRING)
				                                  .addScalar("cantidadconsiderada", StandardBasicTypes.INTEGER)
				                                  .addScalar("cantidadtotal", StandardBasicTypes.INTEGER);
	
		List lista =query.list(); 
		session.close();
		return lista;
		
	}

	//################### johann
	public List ejecutarQueryAsistencia(String hql){
		
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("id", StandardBasicTypes.INTEGER)
                .addScalar("fecha", StandardBasicTypes.DATE)
                .addScalar("asistencia", StandardBasicTypes.INTEGER)
                .addScalar("nombre",StandardBasicTypes.STRING)
				.addScalar("cupostotales",StandardBasicTypes.INTEGER);

		List lista =query.list(); 
		session.close();
		return lista;
		
	}
	
	//################### johann
	public List ejecutarQueryCausaInasistencia(String hql){
		
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("nombre", StandardBasicTypes.STRING)
				                                  .addScalar("cantidad", StandardBasicTypes.INTEGER)
				                                  .addScalar("total",StandardBasicTypes.INTEGER);
		List lista =query.list(); 
		session.close();
		return lista;
		
	}
	
	//################### johann
		public List ejecutarQueryBecadosInasistentes(String hql){
			
			Cache objCache= sessionFactory.getCache();
			objCache.evictDefaultQueryRegion();
			
			Session session = sessionFactory.openSession();
			
			SQLQuery query=session.createSQLQuery(hql).addScalar("nombre", StandardBasicTypes.STRING)
					                                  .addScalar("apellido", StandardBasicTypes.STRING)
					                                  .addScalar("equipo", StandardBasicTypes.STRING)
					                                  .addScalar("fecha",StandardBasicTypes.DATE)
														.addScalar("horaInicio",StandardBasicTypes.DATE)
														.addScalar("horaFin",StandardBasicTypes.DATE)
														.addScalar("causa",StandardBasicTypes.STRING);
			
			List lista =query.list(); 
			session.close();
			return lista;
			
		}
	
	//################### YSA johann
	public List ejecutarQueryRecursosEventos(String hql){
		
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("nombre", StandardBasicTypes.STRING)
				                                  .addScalar("contactocorreo", StandardBasicTypes.STRING)
				                                  .addScalar("contactotelefono", StandardBasicTypes.STRING)
				                                  .addScalar("direccion",StandardBasicTypes.STRING)
													.addScalar("fecha",StandardBasicTypes.DATE)
													.addScalar("hora",StandardBasicTypes.DATE)
													.addScalar("patrocinantes",StandardBasicTypes.STRING)
													.addScalar("responsable",StandardBasicTypes.STRING)
													.addScalar("cantidad",StandardBasicTypes.INTEGER)
													.addScalar("nombrerecurso",StandardBasicTypes.STRING);
		
		List lista =query.list(); 
		session.close();
		return lista;
		
	}
	
	//################### Yessi
	public List ejecutarQueryFrecuenciaCausas(String hql){
		
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("nombre",StandardBasicTypes.STRING)
												  .addScalar("idcausa", StandardBasicTypes.INTEGER)
				                                  .addScalar("cantidadcausas", StandardBasicTypes.INTEGER);
		
		List lista =query.list(); 
		session.close();
		return lista;
		
	}

	
	//################### YSA 
	
	public List ejecutarQueryCantidadRecursos(String restricciones){
	    
		String hql="SELECT tablarecursos.nombrerecurso AS nombrerecurso, tablarecursos.cantidad AS cantidad, tablarecursos.total AS total FROM (SELECT recurso.nombrerecurso AS nombrerecurso,SUM(donacionrecurso.cantidadsolicitada) AS cantidad, (select SUM(donacionrecurso.cantidadsolicitada) FROM donacion, donacionrecurso, recurso "+ 
" WHERE recurso.id=donacionrecurso.idrecurso  AND donacion.id=donacionrecurso.iddonacion AND donacion.tipo=1 "+ restricciones + ") AS total "+
 " FROM recurso,donacionrecurso,donacion WHERE recurso.id=donacionrecurso.idrecurso "+
				" AND donacion.id=donacionrecurso.iddonacion AND donacion.tipo=1 "+ restricciones + "GROUP BY recurso.id) AS tablarecursos";
	
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("nombrerecurso",StandardBasicTypes.STRING)
				                                  .addScalar("cantidad", StandardBasicTypes.INTEGER)
				                                  .addScalar("total", StandardBasicTypes.INTEGER);
	
		List lista =query.list(); 
		session.close();
		return lista;
		
	}

	

	//################################################ AGREGADO POR EMILY SILVA Y GABRIELA PALMAR ########################################
		//Recursos que mas entran a la fundacion
		public List ejecutarQueryRecurso(String restricciones, String idevento){
			
			Cache objCache= sessionFactory.getCache();
			objCache.evictDefaultQueryRegion();
			
			Session session = sessionFactory.openSession();
			
			String sql ="SELECT recurso.nombrerecurso, SUM(donacionrecurso.cantidad) AS totalporrecurso,totalrecursos.total AS "
					+ "totaldonacion FROM recurso, donacionrecurso, donacion, (SELECT SUM(donacionrecurso.cantidad) AS total FROM " 
	                + "donacionrecurso, recurso, donacion WHERE recurso.id=donacionrecurso.idrecurso AND "
	                + "donacion.id=donacionrecurso.iddonacion " +restricciones+ " AND donacion.tipo=0) AS totalrecursos "
	                + "WHERE donacion.tipo=0 AND recurso.id=donacionrecurso.idrecurso " +restricciones+
	                  "AND donacion.id = donacionrecurso.iddonacion GROUP BY recurso.id,totalrecursos.total ";
			
			System.out.println(" $$$$$$$$$$$$$$$$ " + sql);
			
			SQLQuery query=session.createSQLQuery(sql).addScalar("nombrerecurso", StandardBasicTypes.STRING)
					                                  .addScalar("totalporrecurso",StandardBasicTypes.INTEGER)
					                                  .addScalar("totaldonacion",StandardBasicTypes.INTEGER);
			
			List lista =query.list(); 
			session.close();
			return lista;
			
		}
		
	//################################################### HASTA AQUI #################################################################################
		
		
		
		
	//HILDA
		public List ejecutarQueryAceptadosPostulados(String hql){
			
			Cache objCache= sessionFactory.getCache();
			objCache.evictDefaultQueryRegion();
			
			Session session = sessionFactory.openSession();
			
			SQLQuery query=session.createSQLQuery(hql).addScalar("cantidadaceptados", StandardBasicTypes.INTEGER)
					                                  .addScalar("cantidadpostulados", StandardBasicTypes.INTEGER)
					                                  .addScalar("mesaceptados",StandardBasicTypes.STRING)
													  .addScalar("mespostulados",StandardBasicTypes.STRING);
			
			List lista =query.list(); 
			session.close();
			return lista;
			
		}

	//------------------------------------- metodo de Lilianny reporte ResultadosJuegos ---------------------------------------
	
	public List ejecutarQueryResultadosJuegos(String hql){
		
		Cache objCache= sessionFactory.getCache();
		objCache.evictDefaultQueryRegion();
		Session session = sessionFactory.openSession();
		
		SQLQuery query=session.createSQLQuery(hql).addScalar("competencia", StandardBasicTypes.STRING)
												  .addScalar("equipo", StandardBasicTypes.STRING)
												  .addScalar("fecha", StandardBasicTypes.DATE)
												  .addScalar("resultado", StandardBasicTypes.STRING)
												  .addScalar("contrincante", StandardBasicTypes.STRING)
												  .addScalar("categoria", StandardBasicTypes.STRING)
												  .addScalar("rango", StandardBasicTypes.STRING);
		List lista = query.list();
		session.close();
		return lista;
		
	}
		
}