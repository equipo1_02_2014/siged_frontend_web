package edu.ucla.siged.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.HistoriaAyuda;

public interface DaoHistoriaAyudaI extends JpaRepository<HistoriaAyuda, Integer> {
	
	@Query("SELECT COUNT(HAY) FROM HistoriaAyuda HAY, Atleta A "
			+ "WHERE HAY.atleta.id=A.id "
			+ "AND A.id=?1 "
			+ "AND ?2 >= HAY.fechaInicio AND ?2 < HAY.fechaFin ")
	public long buscarBecaPorMes(Integer id, Date date);
}
