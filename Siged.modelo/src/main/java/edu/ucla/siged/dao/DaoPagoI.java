package edu.ucla.siged.dao;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.ucla.siged.domain.gestionatleta.Pago;

public interface DaoPagoI extends JpaRepository<Pago, Integer> {

	@Query("SELECT P FROM Pago P "
			+ "WHERE LOWER(P.numRecibo) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND LOWER(P.atleta.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(P.atleta.apellido) LIKE LOWER(CONCAT(?3, '%')) "
			+ "GROUP BY P.id, P.numRecibo ORDER BY P.numRecibo DESC")
	public Page<Pago> filtroPago(String numRecibo, String nombreAtleta, String apellidoAtleta, Pageable pagina);

	@Query("SELECT COUNT(P) FROM Pago P "
			+ "WHERE LOWER(P.numRecibo) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND LOWER(P.atleta.nombre) LIKE LOWER(CONCAT(?2, '%')) "
			+ "AND LOWER(P.atleta.apellido) LIKE LOWER(CONCAT(?3, '%'))")
	public long filtroPagoCantidad(String numRecibo, String nombreAtleta, String apellidoAtleta);

	@Query("SELECT P FROM Pago P "
			+ "WHERE LOWER(P.numRecibo) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND P.fecha=?2 "
			+ "AND LOWER(P.atleta.nombre) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND LOWER(P.atleta.apellido) LIKE LOWER(CONCAT(?4, '%')) "
			+ "GROUP BY P.id, P.numRecibo ORDER BY P.numRecibo DESC")
	public Page<Pago> filtroPagoFecha(String numRecibo, Date fecha, String nombreAtleta, String apellidoAtleta, Pageable pagina);

	@Query("SELECT COUNT(P) FROM Pago P "
			+ "WHERE LOWER(P.numRecibo) LIKE LOWER(CONCAT(?1, '%')) "
			+ "AND P.fecha=?2 "
			+ "AND LOWER(P.atleta.nombre) LIKE LOWER(CONCAT(?3, '%')) "
			+ "AND LOWER(P.atleta.apellido) LIKE LOWER(CONCAT(?4, '%')) ")
	public long filtroPagoFechaCantidad(String numRecibo, Date fecha, String nombreAtleta, String apellidoAtleta);

	public Pago findByNumRecibo(String numRecibo);
	
	@Query("SELECT COUNT(P) FROM Pago P, Atleta A "
			+ "WHERE A.id=P.atleta.id "
			+ "AND A.id =?1 "
			+ "AND P.mes=?2 "
			+ "AND P.anno=?3")
	public long buscarPagoPorMesYAnno(int id, Integer mes, Long anno);
}

