package edu.ucla.siged.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.domain.seguridad.Funcionalidad;

public interface DaoFuncionalidadI extends JpaRepository<Funcionalidad, Integer> {


	public List<Funcionalidad> findByEstatus(@Param ("est") Short estatus);

	public Funcionalidad findByNombre(@Param ("nomb") String nombre);

	public Funcionalidad findById(@Param ("id") Integer id);

	@Query("SELECT COUNT(F) FROM Funcionalidad F WHERE F.estatus=?")
	public long countByEstatus(Short estatus);
	
	/*@Query("SELECT COUNT(F) FROM Funcionalidad F WHERE F.idPadre=?")
	public long contarHijos(Integer idPadre);*/
	
	@Query("SELECT COUNT(F) FROM Funcionalidad F WHERE F.idPadre=?")
	public long countByIdPadre(Integer idPadre);

	@Query("SELECT F FROM Funcionalidad F WHERE F.vinculo!='' and F.estatus=1")
	public List<Funcionalidad> findOpcionesVinculo();

}
