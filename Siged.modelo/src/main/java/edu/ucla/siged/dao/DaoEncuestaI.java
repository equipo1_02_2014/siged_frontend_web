package edu.ucla.siged.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestioneventos.Encuesta;

public interface DaoEncuestaI extends JpaRepository<Encuesta, Integer> {
	
	Encuesta findByNombre(@Param ("nom") String nombre);

	Encuesta findById( @Param ("id") Integer id);

	/*@Query("SELECT E FROM Encuesta E WHERE E.id in (?)")
	Page<Encuesta> findEncuestasGrupo(String idRestriccion, Pageable pagina );
	
	@Query("SELECT count(E) FROM Encuesta E WHERE E.id in (?)")
	public long countEncuestasGrupo(String idRestriccion);*/
	
	
	/*@Query("SELECT E FROM Encuesta E where E.id not in "
			+ "(select Et.id from Encuesta Et, Publico P where "
			+ "Et.id=P.idencuesta) and E.fechafin<current_date")
	Page<Encuesta> findEncuestasCerradasPublicas( Pageable pagina );
	
	@Query("SELECT count(E) FROM Encuesta E where E.id not in "
			+ "(select Et.id from Encuesta Et, Publico P where "
			+ "Et.id=P.idencuesta) and E.fechafin<current_date ")
	public long countEncuestasCerradasPublicas();*/
	
	//Rest
	@Query("SELECT e FROM Encuesta e WHERE e.fechaInicio<=?1 AND e.fechaFin>=?1 AND estatus=1")
	public List<Encuesta> encuestasAbiertas(Date fechasistema);
	
	
	
}
