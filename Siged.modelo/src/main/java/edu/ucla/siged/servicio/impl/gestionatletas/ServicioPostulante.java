package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.ucla.siged.domain.gestionatleta.Postulante;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoPostulanteI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPostulanteI;

@Service("servicioPostulante")
public class ServicioPostulante implements ServicioPostulanteI {

		@Autowired
		private DaoPostulanteI daoPostulante;	
		@Autowired
		private SessionFactory sessionFactory;

		public void guardar(Postulante postulante) {
			daoPostulante.save(postulante);
		}

		public long totalPostulantes(){
			return daoPostulante.count();
		}
		
		public Postulante buscarPorId(Integer Id) {
			return daoPostulante.findOne(Id);
		}

		public Page<Postulante> buscarTodos(int numeroPagina) {
			PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"fechaPostulacion");
			return daoPostulante.findAll(pagina);
		}
		
		public List<Postulante> buscarPostulantesPendientes(int nroPagina){
	        List<Postulante> listaPostulantesPendientes = new ArrayList<Postulante>();
	        Sort orden= new Sort(Sort.Direction.DESC,"fechaPostulacion");
			Pageable paginable= new PageRequest(nroPagina,ServicioPostulanteI.TAMANO_PAGINA,orden);
			Page<Postulante> pagina = daoPostulante.buscarPostulantesPendientes(paginable);
			return pagina.getContent();
		}
		
		public long totalPostulantesPendientes(){
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("select count(*) from Postulante where estatus=1");
			long totalPendientes = (Long) query.uniqueResult();
			session.close();
			return totalPendientes;
		}
		
		public long totalPostulantesAprobados(){
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("select count(*) from Postulante where estatus=2");
			long totalAprobados = (Long) query.uniqueResult();
			session.close();
			return totalAprobados;
		}
		
		public List<Postulante> buscarPostulantesAprobados(int nroPagina){
	        List<Postulante> listaPostulantesAprobados = new ArrayList<Postulante>();
	        Sort orden= new Sort(Sort.Direction.DESC,"fechaRespuesta");
			Pageable paginable= new PageRequest(nroPagina,ServicioPostulanteI.TAMANO_PAGINA,orden);
			Page<Postulante> pagina = daoPostulante.buscarPostulantesAprobados(paginable);
			return pagina.getContent();
		}
		
		public List<Postulante> buscarPostulantesPorRangoFechaPostulacion(
				Date fechaInicio, Date fechaFin) {
			return daoPostulante.buscarPostulantesPorRangoFecha(fechaInicio, fechaFin);
		}

		public List<Postulante> buscarTodos(){
			return daoPostulante.findAll();
		}
}	