package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.Set;

import edu.ucla.siged.domain.gestionatleta.Documento;

public interface ServicioDocumentoI {

	void eliminarDocumentos(Set<Documento> documentos);
	void eliminarDocumento(Documento documento);
	
}
