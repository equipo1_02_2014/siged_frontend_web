package edu.ucla.siged.servicio.interfaz.gestionequipos;
import java.util.List;

import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.DelegadoEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface ServicioDelegadoEquipoI {

	public void guardar(DelegadoEquipo delegadoEquipo);
	
	public Representante obtenerDelegado(Integer id);
	
	public List<Representante> buscarRepresentantesDisponibles();
	
	public Representante obtenerDelegadoPrincipal(Integer id);
	
	public Representante obtenerDelegadoSuplente(Integer id);
	
	public void revocarDelegadosEquipoCerrado(Equipo equipo);
}