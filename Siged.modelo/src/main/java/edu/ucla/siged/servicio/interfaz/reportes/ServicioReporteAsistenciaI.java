package edu.ucla.siged.servicio.interfaz.reportes;

import java.util.List;

import edu.ucla.siged.domain.reporte.Asistencia;
import edu.ucla.siged.domain.reporte.CausaInasistencia;

public interface ServicioReporteAsistenciaI {

	public List<Asistencia> buscarAsistencia(String restricciones, String asiste);
	
	public List<CausaInasistencia> buscarCausaInasistencia(String restricciones, String becados, String noBecados);
	
}
