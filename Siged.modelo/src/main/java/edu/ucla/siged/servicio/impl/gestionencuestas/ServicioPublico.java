package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;








import edu.ucla.siged.dao.DaoPublicoI;
import edu.ucla.siged.dao.DaoRolI;
import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioPublicoI;

@Service("servicioPublico")
public class ServicioPublico implements ServicioPublicoI {

	@Autowired private DaoPublicoI daoPublico;
	@Autowired private DaoRolI daoRol;
	
	@Autowired private SessionFactory sessionFactory;
	
	public List<Publico> buscarPublicoEncuesta(Encuesta encuesta) {
		
		encuesta.getPublicos().size();
		Object[] publicost=encuesta.getPublicos().toArray();
		Publico publicot= new Publico();
		List<Publico> lista = new ArrayList<Publico>();
		for(int i=0; i<publicost.length;i++)
		{
			publicot= (Publico) publicost[i];
			lista.add(publicot);
		}
		return lista;
	}

	public void eliminarVarios(List<Publico> lista) {
		daoPublico.delete(lista);
	
	}

	public void guardarListaPublico(List<Publico> listaPublicos) {
		try{
		daoPublico.save(listaPublicos);
		}
		catch(Exception e){
			System.out.println("ERROR : "+e.getCause());
		}
	}

}
