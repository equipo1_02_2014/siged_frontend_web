package edu.ucla.siged.servicio.impl.reportes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoQuery;
import edu.ucla.siged.domain.reporte.BecadosInasistentesPracticas;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioBecadosInasistentesPracticasI;

@Service("servicioBecadosInasistentesPracticas")
public class ServicioBecadosInasistentesPracticas implements ServicioBecadosInasistentesPracticasI {

	@Autowired
	private DaoQuery daoQuery;
	
	public List<BecadosInasistentesPracticas> buscarAsistencia(String restricciones) {
		
		String sql = "SELECT a.nombre, a.apellido, e.nombre as equipo, p.fecha, p.horaInicio, p.horaFin,  " +
				" (SELECT c.nombre FROM causa c INNER JOIN asistenciaatleta aa ON c.id = aa.idcausa WHERE aa.idatleta = a.id  and aa.idpractica = p.id) as causa " + 
				" FROM atleta a  " +
				" INNER JOIN atletaequipo ae ON a.id = ae.idatleta " + 
				" INNER JOIN equipo e ON ae.idequipo = e.id  " +
				" INNER JOIN practica p ON p.idequipo = e.id  " +
				" WHERE  " +
				" a.id IN (  " +
				" SELECT aa.idatleta FROM asistenciaatleta aa WHERE aa.idpractica = p.id and asistencia = false and aa.idatleta = a.id " + 
				" and aa.idatleta IN (SELECT ha.idatleta FROM historiaayuda ha WHERE ha.fechafin >= current_date)  " +
				" ) " + restricciones + " ORDER BY a.id, p.fecha desc";
		
		
		System.out.println("---------------------------------------" + System.lineSeparator() + sql);
		
		List consultaResultado= daoQuery.ejecutarQueryBecadosInasistentes(sql);
		List<BecadosInasistentesPracticas> lista= new ArrayList<BecadosInasistentesPracticas>();
		BecadosInasistentesPracticas becadosInasistentesPracticas;		
		
		for (Object resultado:consultaResultado){
			
			Object[] registro= (Object[]) resultado;
			becadosInasistentesPracticas= new BecadosInasistentesPracticas();
			becadosInasistentesPracticas.setNombre(registro[0].toString() + " " + registro[1].toString());
			becadosInasistentesPracticas.setEquipo(registro[2].toString());
			String fecha= new SimpleDateFormat("dd/MM/YYYY").format((Date)registro[3]);
			becadosInasistentesPracticas.setFecha(fecha);
			becadosInasistentesPracticas.setHoraInicio(registro[4].toString());
			becadosInasistentesPracticas.setHoraFin(registro[5].toString());
			becadosInasistentesPracticas.setCausa(registro[6].toString());
			
			lista.add(becadosInasistentesPracticas);
			
		}
		
		//ServicioReporteAsistencia
		
		//Collections.sort(lista);
		/*
		Collections.reverse(lista);
		if (cantidadEquipos < lista.size()){
		    lista= lista.subList(0, cantidadEquipos);
		}*/
		
		return lista;	 
	}

	
}