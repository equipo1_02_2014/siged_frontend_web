package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoRepresentanteI;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;

@Service
public class ServicioRepresentante implements ServicioRepresentanteI {

	private @Autowired DaoRepresentanteI daoRepresentanteI;

	public List<Representante> obtenerListaRepresentante() {
		return null;
	}
	
	//Agregado por Jesus
	public Representante buscarPorCedula(String cedula) {
		return daoRepresentanteI.findByCedula(cedula);
	}

}
