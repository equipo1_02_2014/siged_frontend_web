package edu.ucla.siged.servicio.impl.seguridad;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoControlAccesoI;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioControlAccesoI;



@Service("servicioControlAcceso")
public class ServicioControlAcceso implements ServicioControlAccesoI{

	@Autowired private DaoControlAccesoI daoControlAcceso;
	@Autowired private SessionFactory sessionFactory;
	
	public List<ControlAcceso> buscarTodos() {
		return daoControlAcceso.findAll();
	
	}
	
	public void eliminar(ControlAcceso funcionalidadRolSeleccionado) {
		// TODO Auto-generated method stub
		funcionalidadRolSeleccionado.setEstatus((short) 0);
			daoControlAcceso.save(funcionalidadRolSeleccionado);
		
		
	}
	public void eliminarVarios2(Set<ControlAcceso> funcionalidadesRol) {
		// TODO Auto-generated method stub
		Object[] listaFuncionalidadesRol = funcionalidadesRol.toArray();
		List<ControlAcceso> funcionalidadesEliminadas =null;
		ControlAcceso funcionalidadRolTemp = null;
		for (int i=0;i<funcionalidadesRol.size();i++){
			funcionalidadRolTemp=(ControlAcceso) listaFuncionalidadesRol[i];
			funcionalidadRolTemp.setEstatus(Short.parseShort("0"));
			funcionalidadesEliminadas.add(funcionalidadRolTemp);
		}
			
			
		daoControlAcceso.save(funcionalidadesEliminadas);
	}
	
	public void eliminarVarios(List<ControlAcceso> funcionalidadesRol) {
		// TODO Auto-generated method stub
		/*Object[] listaFuncionalidadesRol = funcionalidadesRol.toArray();
		List<ControlAcceso> funcionalidadesEliminadas =null;
		ControlAcceso funcionalidadRolTemp = null;
		for (int i=0;i<funcionalidadesRol.size();i++){
			funcionalidadRolTemp=(ControlAcceso) listaFuncionalidadesRol[i];
			funcionalidadRolTemp.setEstatus(Short.parseShort("0"));
			funcionalidadesEliminadas.add(funcionalidadRolTemp);
		}*/
			
			
		daoControlAcceso.delete(funcionalidadesRol);
	}
	
	public void guardarListaFuncionalidadRol(List<ControlAcceso> funcionalidades) {
		daoControlAcceso.save(funcionalidades);
		
	}
	
	public void guardarListaFuncionalidadesRol(List<ControlAcceso> funcionalidades) {
		daoControlAcceso.save(funcionalidades);
	}

	public List<ControlAcceso> getFuncionalidadesRol(Rol rol) {   	
    	return daoControlAcceso.findByRol(rol);
	}
	
	
}