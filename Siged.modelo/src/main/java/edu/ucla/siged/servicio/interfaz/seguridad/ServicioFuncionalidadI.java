package edu.ucla.siged.servicio.interfaz.seguridad;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.seguridad.Funcionalidad;

public interface ServicioFuncionalidadI {
	
	public static final int TAMANO_PAGINA = 10;
	public List<Funcionalidad> buscarTodos();
	public void guardar(Funcionalidad funcionalidad);
	public void eliminar(Funcionalidad funcionalidad);
	public void eliminarVarios(List<Funcionalidad> funcionalidades);
	public Page<Funcionalidad> buscarTodos(int numeroPagina);
	public List<Funcionalidad> buscarFiltrado(String hql,int numeroPagina);
	public long totalFuncionalidades();
	public long totalFuncionalidadesFiltradas(String hql);
	public List<Funcionalidad> buscarActivos();
	public Funcionalidad buscarByNombre(String nombre);
	public Funcionalidad buscarById(Integer id);
	public Short esHijo(Funcionalidad funcionalidad);
	public List<Funcionalidad> buscarFuncionalidadesHojasActivas();
	
}