package edu.ucla.siged.servicio.impl.reportes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoFiltro;
import edu.ucla.siged.dao.DaoQuery;
import edu.ucla.siged.domain.reporte.AceptadosPostuladosPorMes;
import edu.ucla.siged.domain.reporte.CumplimientoActividad;
import edu.ucla.siged.domain.reporte.FrecuenciaCausa;
import edu.ucla.siged.domain.reporte.PromedioCantidadBase;
import edu.ucla.siged.domain.reporte.RecursoEvento;
import edu.ucla.siged.domain.reporte.RecursoLlega;
import edu.ucla.siged.domain.reporte.ResultadosJuegos;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;

@Service("servicioReporteEstadistico")
public class ServicioReporteEstadistico implements ServicioReporteEstadisticoI {

	@Autowired
	private DaoQuery daoQuery;
	@Autowired
	DaoFiltro daoFiltro;
	
	//jesus
		public List<PromedioCantidadBase> buscarEquiposMayorPromedioJuegosGanados(String restricciones,int cantidadEquipos) {

			String sql="SELECT tablajuegosganados.nombre AS descripcion,tablajuegosganados.ganados AS cantidadconsiderada,tablajuegosjugados.jugados AS cantidadtotal FROM " + 
		            "(SELECT equipo.id AS idganados,equipo.nombre AS nombre,COUNT(equipo.id) AS ganados FROM equipo,juego " +
				             "WHERE juego.resultado=1 " + restricciones +" AND equipo.id=juego.idequipo " +
				             "GROUP BY equipo.id) AS tablajuegosganados," +
				    "(SELECT equipo.id AS idjugados,COUNT(equipo.id) AS jugados FROM equipo,juego " + 
				             "WHERE equipo.id=juego.idequipo AND juego.resultado!=0 " + restricciones  + " GROUP by equipo.id) AS tablajuegosjugados " + 
				     "WHERE tablajuegosganados.idganados=tablajuegosjugados.idjugados";
			
			System.out.println("---------------------------------------" + System.lineSeparator() + sql);
			
			List consultaResultado= daoQuery.ejecutarQueryPromedioCantidad(sql);
			List<PromedioCantidadBase> lista= new ArrayList<PromedioCantidadBase>();
			PromedioCantidadBase objPromedioCantidadBase;
			
			
			for (Object resultado:consultaResultado){
				
				Object[] registro= (Object[]) resultado;
				
				objPromedioCantidadBase= new PromedioCantidadBase();
				
				objPromedioCantidadBase.setDescripcion(registro[0].toString().substring(9, registro[0].toString().length()));
				
				objPromedioCantidadBase.setCantidadConsiderada((Integer)registro[1]);
				
				objPromedioCantidadBase.setCantidadTotal((Integer)registro[2]);
				
				objPromedioCantidadBase.setPromedio((objPromedioCantidadBase.getCantidadConsiderada() * 1.0 / objPromedioCantidadBase.getCantidadTotal()));
				
				lista.add(objPromedioCantidadBase);
			}
			
			//ServicioReporteAsistencia
			
			Collections.sort(lista);
			
			Collections.reverse(lista);
			if (cantidadEquipos < lista.size()){
			    lista= lista.subList(0, cantidadEquipos);
			}
			return lista;	 
		}
		
		
		//johann
		public List<RecursoEvento> buscarRecursosEventos(String restricciones,int cantidadEquipos) {

			String sql="SELECT e.nombre, e.contactocorreo, e.contactotelefono, e.direccion, e.fecha, e.hora, " +
					" e.patrocinantes, e.responsable, dr.cantidad, r.nombrerecurso " +  
					" FROM evento e  " +
					" INNER JOIN donacion d ON e.id = d.idevento " + 
					" INNER JOIN donacionrecurso dr ON d.id = dr.iddonacion " + 
					" INNER JOIN recurso r ON dr.idrecurso = r.id WHERE d.estatus = 4 " + restricciones;
			
			System.out.println("---------------------------------------" + System.lineSeparator() + sql);
			
			List consultaResultado= daoQuery.ejecutarQueryRecursosEventos(sql);
			List<RecursoEvento> lista= new ArrayList<RecursoEvento>();
			RecursoEvento objRecursoEvento;
			
			
			for (Object resultado:consultaResultado){
				
				Object[] registro= (Object[]) resultado;
				objRecursoEvento= new RecursoEvento();
				objRecursoEvento.setNombre(registro[0].toString());
				objRecursoEvento.setCorreo(registro[1].toString());
				objRecursoEvento.setTelefono(registro[2].toString());
				objRecursoEvento.setDireccion(registro[3].toString());
				String fecha= new SimpleDateFormat("dd/MM/YYYY").format((Date)registro[4]);
				objRecursoEvento.setFecha(fecha);
				objRecursoEvento.setHora(registro[5].toString());
				objRecursoEvento.setPatrocinantes(registro[6].toString());
				objRecursoEvento.setResponsable(registro[7].toString());
				objRecursoEvento.setCantidad((Integer)registro[8]);
				objRecursoEvento.setRecurso(registro[9].toString());
				lista.add(objRecursoEvento);
				
			}
					
			
			Collections.sort(lista);
			
			return lista;	 
			
		}
		
		//yessi
		public List<FrecuenciaCausa> buscarFrecuenciaCausa(String restricciones,int cantidadCausas){

			String sql = "SELECT distinct c.nombre, p.idCausa, (SELECT count(p1.*) cantidadCausas FROM Postulante as p1 WHERE p1.idcausa = c.id and c.tipo=2 "+ restricciones + ") as cantidadCausas FROM Causa as c, Postulante p WHERE p.idcausa = c.id and c.tipo=2";
			
			List consultaResultado= daoQuery.ejecutarQueryFrecuenciaCausas(sql);
			List<FrecuenciaCausa> listaCausa= new ArrayList<FrecuenciaCausa>();
			FrecuenciaCausa objCausa;
			
			
			for (Object resultado:consultaResultado){
				
				Object[] registro= (Object[]) resultado;
				objCausa= new FrecuenciaCausa();
				objCausa.setIdCausa((Integer)registro[1]);
				objCausa.setCantidadCausa((Integer)registro[2]);
				objCausa.setNombre(registro[0].toString());
				
				listaCausa.add(objCausa);
			}
			
			return listaCausa;	 
		}
		
		
		

		//################################################ AGREGADO POR EMILY SILVA Y GABRIELA PALMAR ########################################
		//agregado para recurso
		public List<RecursoLlega> buscarRecursos(String restricciones, String idevento ) {

			List consultaResultado= daoQuery.ejecutarQueryRecurso(restricciones, idevento);
			List<RecursoLlega> lista= new ArrayList<RecursoLlega>();
			RecursoLlega recursoLlega;		
			
			for (Object resultado:consultaResultado){
				
				Object[] registro= (Object[]) resultado;
				recursoLlega= new RecursoLlega();
				
				recursoLlega.setNombreRecurso(registro[0].toString());
				recursoLlega.setTotalPorRecurso((Integer)registro[1]);
				recursoLlega.setTotalDonacion((Integer)registro[2]);
				
				Double porcentaje= (recursoLlega.getTotalPorRecurso()*100)/(recursoLlega.getTotalDonacion()*1.0);
				
		        recursoLlega.setPorcentaje(Math.round((porcentaje)*Math.pow(10, 2))/Math.pow(10, 2));
				
				lista.add(recursoLlega);
			}
			return lista;	 
		}
		
	//################################################ HASTA AQUI #########################################################################

	//hilda
	public List<AceptadosPostuladosPorMes> buscarAceptadosPostulados(String restricciones) {
			
			String sql ="select coalesce(aceptados.numaceptados,0) as cantidadaceptados, coalesce(postulados.numpostulados,0) as cantidadpostulados, coalesce(postulados.mespostulados,'00') as mespostulados, coalesce(aceptados.mesaceptados,'00') as mesaceptados  "
					+ "from (select count(*) as numaceptados,  to_char(fecharespuesta, 'MM') as mesaceptados from postulante where estatus=2 group by to_char(fecharespuesta, 'MM')) as aceptados "
					+ "full outer join"
					+ "(select count(*) as numpostulados, to_char(fechapostulacion, 'MM') as mespostulados from postulante "+restricciones +" group by to_char(fechapostulacion, 'MM')) as postulados "
					+ "on postulados.mespostulados=aceptados.mesaceptados  " ;
			
			
			System.out.println("---------------------------------------" + System.lineSeparator() + sql);
			
			List consultaResultado= daoQuery.ejecutarQueryAceptadosPostulados(sql);
			List<AceptadosPostuladosPorMes> lista= new ArrayList<AceptadosPostuladosPorMes>();
			AceptadosPostuladosPorMes aceptadosPostulados;		
			
			for (Object resultado:consultaResultado){
				
				Object[] registro= (Object[]) resultado;
				aceptadosPostulados= new AceptadosPostuladosPorMes();
				aceptadosPostulados.setCantidadaceptados((Integer)registro[0]);
				aceptadosPostulados.setCantidadpostulados((Integer)registro[1]);			
				aceptadosPostulados.setMespostulados((registro[2].toString()));
				aceptadosPostulados.setMesaceptados((registro[3].toString()));
				
				
				//asistencia.setNombre(registro[3].toString().substring(9, registro[3].toString().length())  + " " + registro[1].toString());
				
				lista.add(aceptadosPostulados);
			}
			
			//ServicioReporteAsistencia
			
			Collections.sort(lista);
			/*
			Collections.reverse(lista);
			if (cantidadEquipos < lista.size()){
			    lista= lista.subList(0, cantidadEquipos);
			}*/
			
			return lista;	 
		}

		//------------------------------------- metodo de Lilianny reporte ResultadosJuegos ---------------------------------------
		
		public List<ResultadosJuegos> listarResultadosJuegos (String restricciones){
			
			String sql="SELECT c.nombre AS competencia, e.nombre AS equipo , j.fecha, j.resultado, "
					+ "j.equipocontrario AS contrincante, cat.nombre AS categoria, r.descripcion AS rango "
					+ "FROM juego AS j INNER JOIN equipo AS e ON j.idequipo = e.id "
					+ "INNER JOIN competencia AS c ON j.idcompetencia = c.id "
					+ "INNER JOIN tipocompetencia AS tip ON c.idtipocompetencia = tip.id "
					+ "INNER JOIN categoria AS cat ON e.idcategoria = cat.id "
					+ "INNER JOIN rango AS r ON e.idrango = r.id WHERE j.resultado <> -1 " + restricciones;
			
			List consultaResultado= daoQuery.ejecutarQueryResultadosJuegos(sql);
			List<ResultadosJuegos> lista = new ArrayList<ResultadosJuegos>();
			ResultadosJuegos objResultadosJuegos;
			
			for (Object resultado:consultaResultado){
				
				Object[] registro= (Object[]) resultado;
				objResultadosJuegos = new ResultadosJuegos();
				objResultadosJuegos.setCompetencia(registro[0].toString());
				objResultadosJuegos.setEquipo(registro[1].toString());
				String fecha = new SimpleDateFormat("dd/MM/YYYY").format((Date)registro[2]);
				objResultadosJuegos.setFecha(fecha);
				objResultadosJuegos.setResultado(registro[3].toString());
				objResultadosJuegos.setContrincante(registro[4].toString());
				objResultadosJuegos.setCategoria(registro[5].toString());
				objResultadosJuegos.setRango(registro[6].toString());
				lista.add(objResultadosJuegos);
		
			}
			
			return lista;
		}
	
	public List<CumplimientoActividad> listaCumplimiento(String restricciones, int tipo) {
		String hql ="";
		String campoPrincipal="";
		String restriccionSecundaria="";
		String restriccionesSubquery=restricciones;
		restriccionesSubquery=restriccionesSubquery.replaceAll("e.categoria.nombre", "eq.categoria.nombre");
		restriccionesSubquery=restriccionesSubquery.replaceAll("e.rango.descripcion", "eq.rango.descripcion");
		
		switch(tipo){
		case 1: {campoPrincipal ="a,a.descripcion";
				restriccionSecundaria ="ap.actividad=a";}
		break;
		case 2: {campoPrincipal = "t,concat(t.nombre,' ',t.apellido)";
				restriccionSecundaria ="et.tecnico=t";
				restriccionesSubquery=restriccionesSubquery.replaceAll("a.descripcion", "ap.actividad.descripcion");
				}
		break;
		default: {campoPrincipal = "e,e.nombre";
				 restriccionSecundaria ="eq=e";
				 restriccionesSubquery=restriccionesSubquery.replaceAll("a.descripcion", "ap.actividad.descripcion");
					}
		}
		
		hql = " select new CumplimientoActividad ("+campoPrincipal+ "," 
			  + "(select count(ap) from EquipoTecnico et, Equipo eq, Practica p, ActividadPractica ap "
			  + "    where et.equipo = eq and eq = p.equipo and p = ap.practica and "+restriccionSecundaria+" and ap.realizada=true and "+restriccionesSubquery+"),"
			  + "(select count(ap)  from EquipoTecnico et, Equipo eq, Practica p, ActividadPractica ap "
			  + "    where et.equipo = eq and eq = p.equipo and p = ap.practica and "+restriccionSecundaria+" and "+restriccionesSubquery+"))"
				   
			  +" from Tecnico t, EquipoTecnico et, Equipo e, Practica p, ActividadPractica ap, Actividad a "
			  +" where t=et.tecnico and et.equipo = e and e = p.equipo and p = ap.practica and ap.actividad=a and "+restricciones
			  +" group by 1,2 ";
		
		System.out.println(hql);
		
		 
		
		return daoFiltro.buscarTodos(hql);
	}
}