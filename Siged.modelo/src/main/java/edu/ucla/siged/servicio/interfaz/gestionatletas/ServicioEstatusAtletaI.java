package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.List;

import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;

public interface ServicioEstatusAtletaI {

	public List<EstatusAtleta> listaEstatusAtleta();
	public EstatusAtleta obtenerEstatus();
	public EstatusAtleta buscarEstatusAtleta(Integer id);
	public EstatusAtleta obtenerEstatusSinAsignar(); // necesario para retirar un atleta de un equipo - Luis
	public List<EstatusAtleta> listaEstatusAtletaNoEliminado();
}
