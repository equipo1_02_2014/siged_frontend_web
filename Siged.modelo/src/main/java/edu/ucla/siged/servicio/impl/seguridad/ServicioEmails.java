package edu.ucla.siged.servicio.impl.seguridad;

import java.security.GeneralSecurityException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.stereotype.Service;
import com.sun.mail.util.MailSSLSocketFactory;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioEmailsI;

@Service
public class ServicioEmails implements ServicioEmailsI {

	private final Properties properties = new Properties();
	private Session session;
	
	final String username = "siged.fls@gmail.com";
	final String password = "siged_fls";
	
	private void loadProperties() {
		MailSSLSocketFactory sf = null;
		
		try {
			sf = new MailSSLSocketFactory();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		
		sf.setTrustAllHosts(true);
		
		properties.put("mail.smtp.ssl.socketFactory", sf);
		properties.put("mail.smtp.auth", true);
		properties.put("mail.smtp.starttls.enable", true);
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.debug", "true");
		properties.put("mail.smtp.mail.sender", username);
		session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username,password);
					}
				});
	}
	

	public void enviarEmailRecuperarContrasena(String email,  String password) {
		loadProperties();
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setSubject("--- Recuperar Contrasena :: SIGED ---");
			message.setText("Ud. ha accedido al sistema de recuperacion de contrasenas de SIGED."
					+ "\n\n Su contrasena de acceso a SIGED es: " +password);
			Transport.send(message);
		} catch (MessagingException me) {
			throw new RuntimeException(me);
		}
	}
	
	public void enviarEmailUsuarioNuevo(String email, String nombreUsuario, String password) {
		loadProperties();
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress((String) properties.get("mail.smtp.mail.sender")));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setSubject("--- Bienvenido(a) a SIGED ---");
			message.setText("Se le ha otorgado con exito un USUARIO y CONTRASENA de acceso al Sistema." 
					+"\n\n Ud podra hacer uso de nuestras funcionalidades, ingresando con la siguiente informacion."
					+ "\n\n USUARIO    : " + nombreUsuario
					+ "\n\n CONTRASENA : " + password
					+ "\n\n Esperamos que el uso de SIGED sea placentero para Ud ");
			Transport.send(message);
		} catch (MessagingException me) {
			throw new RuntimeException(me);
		}
	}
	
	public void enviarEmailSolicitudPostulacion(String email, String nombre){
		loadProperties();
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress((String) properties.getProperty("mail.smtp.mail.sender")));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setSubject("||- POSTULACIÓN RECIBIDA -||- SIGED -||- FUNDACIÓN LUIS SOJO -||");
			message.setContent("<p><span style='font-size:16px;'><strong>Se&ntilde;or(a) : "+nombre+"</strong></span></p>"
					+"<p style='text-align: center;'>Hemos recibido satisfactoriamente su solicitud de inscripci&oacute;n.</p>"
					+"<p style='text-align: center;'>Est&eacute; atento que le responderemos a la brevedad posible por &eacute;ste medio.</p>"
					+"<p>&nbsp;</p><p><span style='color:#ff3300;'><span style='font-family: georgia,serif;'><em><strong>"
					+"<span style='background-color: rgb(255, 255, 255);'>Fundaci&oacute;n Luis Sojo. Ven a so&ntilde;ar, a jugar, "
					+"a construir un Pa&iacute;s.</span></strong></em></span></span></p>","text/html");
			Transport.send(message);
		}catch(MessagingException me){
			me.printStackTrace();
			throw new RuntimeException(me);
		}
	}
	
	public void enviarEmailAprobarPostulante(String email, String nombre){
		loadProperties();
		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress((String) properties.getProperty("mail.smtp.mail.sender")));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setSubject("||- POSTULACIÓN RECIBIDA -||- SIGED -||- FUNDACIÓN LUIS SOJO -||");
			message.setContent("<p><span style='font-size:16px;'><strong>Se&ntilde;or(a) :</strong></span></p>"
					+"<p style='text-align: center;'>La Fundaci&oacute;n Luis Sojo se complace en informarle que su Postulaci&oacute;n ha sido aprobada.</p>"
					+"<p style='text-align: center;'>Favor dirigirse a las instalaciones de la Fundaci&oacute;n en el siguiente horario:</p>"
					+"<p style='text-align: center;'>Lunes a Viernes. De 2:00pm a 6:00pm</p><p style='margin-left: 40px;'>Con los siguientes documentos:</p>"
					+"<ul><li>Partida de Nacimiento del Atleta</li><li>2 Fotos tipo carnet del atleta</li><li>Historial M&eacute;dico del Atleta</li>"
					+"<li>Planilla de inscripci&oacute;n de la Escuela o Colegio</li><li>C&eacute;dula del Representante Legal del Atleta</li>"
					+"<li>Carpeta Marr&oacute;n tama&ntilde;o oficio</li></ul><p>&nbsp;</p><p><span style='color:#ff3300;'>"
					+"<span style='font-family: georgia,serif;'><em><strong><span style='background-color: rgb(255, 255, 255);'>"
					+"Fundaci&oacute;n Luis Sojo. Ven a so&ntilde;ar, a jugar, a construir un Pa&iacute;s.</span></strong></em></span></span></p>","text/html");
			Transport.send(message);
		}catch(MessagingException me){
			me.printStackTrace();
			throw new RuntimeException(me);
		}
	}
}