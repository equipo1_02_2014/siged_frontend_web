package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.Date;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoHistoriaAyudaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAyudaI;

@Service("servicioHistoriaAyuda")
public class ServicioHistoriaAyuda implements ServicioHistoriaAyudaI {

	private @Autowired DaoHistoriaAyudaI daoHistoriaAyuda;

	public boolean buscarBecaPorAtletaYFecha(Integer id, Date fecha) {
		// TODO Auto-generated method stub
		boolean bool = false;
		long entero = daoHistoriaAyuda.buscarBecaPorMes(id, fecha);
		if (entero != 0) {
			bool = true;
		}
		return bool;

	}

}
