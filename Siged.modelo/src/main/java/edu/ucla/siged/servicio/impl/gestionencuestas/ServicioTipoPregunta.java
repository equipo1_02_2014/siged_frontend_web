package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoTipoPreguntaI;
import edu.ucla.siged.domain.gestioneventos.TipoPregunta;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioTipoPreguntaI;

@Service("servicioTipoPregunta")
public class ServicioTipoPregunta implements ServicioTipoPreguntaI{
	@Autowired
	private DaoTipoPreguntaI daoTipoPregunta;
	@Autowired
	private SessionFactory sessionFactory;
	
	public void guardar(TipoPregunta tipoPregunta) {
		// TODO Auto-generated method stub
		
		if (tipoPregunta.getId()==null){
			//anotador.setEstatus(true);
			daoTipoPregunta.save(tipoPregunta);
		}
		else{
			tipoPregunta.setId(tipoPregunta.getId());
			daoTipoPregunta.save(tipoPregunta);
		}
	}
	
	
	public Page<TipoPregunta> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoTipoPregunta.findAll(pagina);
	}
	
	public TipoPregunta buscarPorId(Integer Id) {
		// TODO Auto-generated method stub
		return daoTipoPregunta.findOne(Id);
	}
	
	public List<TipoPregunta> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from TipoPregunta where "+hql); //daoTipoPregunta.findAll();
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<TipoPregunta> lista =query.list(); //daoTipoPregunta.findAll();
		session.close();
		return lista;

		}

	
	// TIPO PREGUNTA NO MANEJA ESTATUS
	//public void eliminar(TipoPregunta tipoPregunta) {
		// TODO Auto-generated method stub
		//tipoPregunta.setEstatus(Short.parseShort("0"));
		//anotador.setEstatus(null);
		//anotador.setEstatus(0);
		//daoAnotador.save(anotador);
	//}
	//public void eliminarVarios(List<Anotador> anotadores) {
		// TODO Auto-generated method stub
		//for (int i=0;i<anotadores.size();i++)
			//anotadores.get(i).setEstatus(Short.parseShort("0"));
			//anotadores.get(i).setEstatus(0);
		//daoAnotador.save(anotadores);
	//}
	
	public long totalTipoPreguntas() {
		// TODO Auto-generated method stub
		return daoTipoPregunta.count();
	}


	public long totalTipoPreguntasFiltradas(String hql) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from TipoPregunta where "+hql); //daoTipoPregunta.findAll();
		long total= (Long) query.uniqueResult(); //daoTipoPregunta.findAll();
		session.close();
		return total;

	}
	
	
}