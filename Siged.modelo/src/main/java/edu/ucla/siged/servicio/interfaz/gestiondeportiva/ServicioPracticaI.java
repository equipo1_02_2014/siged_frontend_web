
package edu.ucla.siged.servicio.interfaz.gestiondeportiva;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;


public interface ServicioPracticaI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(Practica practica, int tipoOperacion);
	public Page<Practica> buscarTodos(int numeroPagina);
	public Practica buscarPorId(Integer Id);
	public Practica buscarPorFechaYEquipo(Date fecha, Equipo equipo);
	public List<Practica> buscarFiltrado(String hql,int numeroPagina);
	public void  eliminar(Practica practica);
	public void eliminarVarios(List<Practica> practicas);
	public long totalPracticas();
	public long totalPracticasFiltradas(String hql);
	public List<Equipo> buscarTodosEquipo(); //ojo
	public HorarioPractica obtenerHorarioPracticaEquipo(Equipo equipo, Date fecha); //ojo
	public List<Area> buscarTodosArea(); //ojo
	public List<Actividad> buscarTodosActividad(); //ojo
	public List<EquipoTecnico> buscarEquipoTecnico(Equipo equipo); //ojo
	public boolean isHorarioAreaDisponible(Area area, int dia, Date horaInicio, Date horaFin);
	public List<Causa> buscarCausasInasistencia();
	public List<Tecnico> buscarTecnicos();
	public List<ActividadPractica> traerActividadesAnteriores(Date fecha, Practica practicaActual);  
	public boolean isEquipoJuegoFechaHora(Equipo equipo,Date fecha,Date hora);
    public boolean isAreaOcupadaJuegoFechaHora(Area area, Date fecha, Date hora);
    public boolean isEquipoPracticaFechaHora(Equipo equipo,Date fecha,Date hora);
	public boolean isAreaOcupadaFechaHora(Area area,Date fecha,Date hora);
	public boolean isEquipoPracticaFechaHora(Equipo equipo,Date fecha,Date hora, Integer idPractica);
	public boolean isAreaOcupadaFechaHora(Area area,Date fecha,Date hora, Integer idPractica);
	public boolean existePracticaPorEquipo(Equipo equipo);
}