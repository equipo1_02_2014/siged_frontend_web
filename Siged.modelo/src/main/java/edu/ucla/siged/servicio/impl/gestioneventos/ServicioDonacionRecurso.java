package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoDonacionRecursoI;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioDonacionRecursoI;

@Service("servicioDonacionRecurso")
public class ServicioDonacionRecurso implements ServicioDonacionRecursoI {

	@Autowired
	private DaoDonacionRecursoI daoDonacionRecursoI;

	public void Guardar(DonacionRecurso dr) {
		daoDonacionRecursoI.save(dr);
	}

	public void Eliminar(Integer id) {
		daoDonacionRecursoI.delete(id);
	}
	
	public void EliminarVarios(Set<DonacionRecurso> drs) {
		daoDonacionRecursoI.delete(drs);
	}

	public List<DonacionRecurso> ListarDonacionRecursos() {
		List<DonacionRecurso> listaDonacionRecursos = daoDonacionRecursoI.findAll();
		return listaDonacionRecursos;
	}

}
