package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.List;

import edu.ucla.siged.domain.gestionatleta.TipoDocumento;

public interface ServicioTipoDocumentoI {
	
	List<TipoDocumento> listaDocumentos();
	TipoDocumento tipoDocumento(Integer id);
}
