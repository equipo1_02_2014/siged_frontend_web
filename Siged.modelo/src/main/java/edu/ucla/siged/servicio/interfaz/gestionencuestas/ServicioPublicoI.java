package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.seguridad.Rol;

public interface ServicioPublicoI {

	List<Publico> buscarPublicoEncuesta(Encuesta encuesta);

	void eliminarVarios(List<Publico> lista);

	void guardarListaPublico(List<Publico> listaPublicos);

}
