package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestioneventos.TipoPregunta;

public interface ServicioTipoPreguntaI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(TipoPregunta tipoPregunta);
	public Page<TipoPregunta> buscarTodos(int numeroPagina);
	public TipoPregunta buscarPorId(Integer Id);
	public List<TipoPregunta> buscarFiltrado(String hql,int numeroPagina);
	public long totalTipoPreguntas();
	public long totalTipoPreguntasFiltradas(String hql);
	
}