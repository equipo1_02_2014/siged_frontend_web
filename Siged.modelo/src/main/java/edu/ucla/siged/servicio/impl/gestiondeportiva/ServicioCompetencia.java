package edu.ucla.siged.servicio.impl.gestiondeportiva;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoCompetenciaI;
import edu.ucla.siged.dao.DaoTipoCompetenciaI;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;


@Service("servicioCompetencia")
public class ServicioCompetencia implements ServicioCompetenciaI {

	@Autowired
	private DaoCompetenciaI daoCompetencia;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private DaoTipoCompetenciaI daoTipoCompetencia;

	public void editar(Competencia competencia) {
		// TODO Auto-generated method stub
	}

	public List<Competencia> buscarTodos() {
		return daoCompetencia.findAll();
	}

	public List<Competencia> buscarCompetenciasVigentes() {
		return daoCompetencia.buscarCompetenciasVigentes();
	}

	@Transactional
	public Set<Juego> buscarJuegosPorCompetencia(Competencia competencia) {
	    Set<Juego> juegos=null;
		if (competencia!=null){
			competencia.getJuegos().size();
			juegos= competencia.getJuegos();
		}
		return juegos;
	}

	public List<Competencia> buscarCompetenciasFuturasYVigentes() {
		return daoCompetencia.buscarCompetenciasFuturasYVigentes();
	}

	public List<Competencia> buscarCompetenciasFuturasYVigentes(int nroPagina) {
		Sort orden= new Sort(Sort.Direction.ASC,"fechaInicio");
		Pageable paginable= new PageRequest(nroPagina,this.getTamanioPagina(),orden);
		Page<Competencia> pagina= daoCompetencia.buscarCompetenciasFuturasYVigentes(paginable);
		return pagina.getContent();
	}

	public long obtenerCantidadCompetenciasFuturasYVigentes(){
		List lista= this.buscarCompetenciasFuturasYVigentes();
		long cantidadRegistros= 0;
		if (lista!=null){
			cantidadRegistros= lista.size();
		}
		return cantidadRegistros;
	}
	
	public int getTamanioPagina(){
		return DaoCompetenciaI.TAMANIO_PAGINA;
	}
	
	//Metodos de Ysabel	
	public void guardar(Competencia competencia){
		if ((Integer)competencia.getId() == null){
		competencia.setEstatus(Short.parseShort("1")) ;
			daoCompetencia.save(competencia);
		}
		else{
			competencia.setId(competencia.getId());
			daoCompetencia.save(competencia);
		}
	}
	
	public Page<Competencia> buscarTodos(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina,this.getTamanioPagina(), Sort.Direction.ASC,"id");
		return daoCompetencia.findAll(pagina);
	}
	
	public Competencia buscarPorId(Integer Id) {
		return daoCompetencia.findOne(Id);
	}
	
	public List<Competencia> buscarFiltrado(String hql, int numeroPagina){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Competencia where "+hql); 
		query.setMaxResults(this.getTamanioPagina());
		query.setFirstResult(numeroPagina * this.getTamanioPagina());
		List<Competencia> lista =query.list(); 
		session.close();
		return lista;
	}
	
	public long totalCompetencias(){
		return daoCompetencia.count();
	}
	
	public long totalCompetenciasFiltrados(String hql){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Competencia where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
	
	public List<TipoCompetencia> buscarTodosTipoCompetencia(){
		return daoTipoCompetencia.findAll();
	}
	
	public void Desactivar(Competencia competencia) {
		competencia.setEstatus(Short.parseShort("0"));
		daoCompetencia.save(competencia);
	}
	public Page<Competencia> buscarCompetenciasActivas(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina,this.getTamanioPagina(), Sort.Direction.ASC,"id");
		return daoCompetencia.buscarCompetenciasActivas(pagina);
	}
	
	public long totalCompetenciasActivas(){
		return daoCompetencia.totalCompetenciasActivas();
	}
	
	//Yessi
	public boolean existeCompetenciaPorEquipo(Equipo equipo){
		List<Competencia> lista= daoCompetencia.buscarCompetenciaPorEquipo(equipo.getId());
		return !lista.isEmpty();
	}
	
}