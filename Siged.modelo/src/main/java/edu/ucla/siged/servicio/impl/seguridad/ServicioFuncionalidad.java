package edu.ucla.siged.servicio.impl.seguridad;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoFuncionalidadI;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;


@Service("servicioFuncionalidad")
public class ServicioFuncionalidad implements ServicioFuncionalidadI{

	@Autowired private DaoFuncionalidadI daoFuncionalidad;
	@Autowired private SessionFactory sessionFactory;
	
	public List<Funcionalidad> buscarTodos() {
		return daoFuncionalidad.findAll();
	}

	public void guardar(Funcionalidad funcionalidad) {
		// TODO Auto-generated method stub
		daoFuncionalidad.save(funcionalidad);
	}
	
	public void eliminarLogico(Funcionalidad funcionalidad) {
		// TODO Auto-generated method stub
		funcionalidad.setEstatus((short) 0);
		daoFuncionalidad.save(funcionalidad);
	}
	
	public void eliminar(Funcionalidad funcionalidad) {
		// TODO Auto-generated method stub
		daoFuncionalidad.delete(funcionalidad);
	}
	
	public void eliminarVarios(List<Funcionalidad> funcionalidad) {
		// TODO Auto-generated method stub
		daoFuncionalidad.delete(funcionalidad);
	}
	
	public void eliminarVariosLogico(List<Funcionalidad> funcionalidades) {
		// TODO Auto-generated method stub
		for (int i=0;i<funcionalidades.size();i++)
			funcionalidades.get(i).setEstatus(Short.parseShort("0"));
			
		daoFuncionalidad.save(funcionalidades);
	}
	
	public Page<Funcionalidad> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoFuncionalidad.findAll(pagina);
	}
	
	public List<Funcionalidad> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Funcionalidad where "+hql); 
		System.out.println("from Funcionalidad where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Funcionalidad> lista =query.list(); 
		session.close();
		return lista;
		
		
		}
	public List<Funcionalidad> buscarActivos() {
		return daoFuncionalidad.findByEstatus((short)1);
	
	}

	public long totalFuncionalidades() {
		// TODO Auto-generated method stub
		return daoFuncionalidad.count();
	}

	public long totalFuncionalidadesFiltradas(String hql) {
		
			// TODO Auto-generated method stub
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("select count(*) from Funcionalidad where "+hql); 
			long total= (Long) query.uniqueResult(); 
			session.close();
			return total;

		

	}

	public Funcionalidad buscarByNombre(String nombre) {
		// TODO Auto-generated method stub
		return daoFuncionalidad.findByNombre(nombre);
	}
	
	public Funcionalidad buscarById(Integer id){
		Funcionalidad func= daoFuncionalidad.findById(id);
		/*if(func!=null)
		func.setHijo(this.esHijo(func));
		*/return func;
	}

	public Short esHijo(Funcionalidad funcionalidad) {
		long count=daoFuncionalidad.countByIdPadre(funcionalidad.getId());
		if(count==0)
		return 1;
		return 0;
	}

	public List<Funcionalidad> buscarFuncionalidadesHojasActivas() {
		
		return 	daoFuncionalidad.findOpcionesVinculo();
	}


	
}
