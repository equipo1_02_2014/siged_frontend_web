package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.List;

import edu.ucla.siged.domain.utilidades.EstadoCivil;

public interface ServicioEstadoCivilI {

	List<EstadoCivil> obtenerListaEstadoCivil();
}
