package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.List;
import java.util.Set;






import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Documento;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.domain.gestionatleta.Reposo;
import edu.ucla.siged.domain.gestionequipo.Categoria;

public interface ServicioAtletaI {

	public static final int TAMANO_PAGINA = 9;
	public List<Atleta> obtenerListaAtleta();
	public List<Atleta> obtenerListaAtleta(int numeroPagina);//Para la paginacion
	public List<Atleta> obtenerListaAtleta(int numeroPagina, String cedula, String nombre, String apellido, String equipoActual,EstatusAtleta estatusAtleta);	
	public long totalAtletasFiltrados(String cedula, String nombre, String apellido, String equipoActual, EstatusAtleta estatusAtleta);
	public long totalAtletas();
	public void guardarAtleta(Atleta atleta, Set<Reposo> reposos, Set<Documento> documentos);
	public void guardarAtletaConEstatus(Atleta atleta, Set<Reposo> reposos, Set<Documento> documentos);
	public List<Atleta> obtenerAtletasValidos(Categoria categoria);
	public void editarEstatus(Atleta atleta);
	public void eliminarAtletaLogicamente(Atleta atleta);
	public void eliminarAtletasLogicamente(List<Atleta> atletas);
	public Atleta buscarAtletaPorCedula(String cedula);
	public Atleta buscarAtletaPorID(Integer id);
	
	// 29-01-15 por  Henri
	public List<Atleta> ListarAtletasSinBeca(int numeroPagina, int tamanoPagina);
	public long AtletasSinBeca();
	public void Guardar(Atleta a);
	
	public void estatusAsignarAtletaEquipo(Atleta atleta);
	public void estatusRetirarAtletaEquipo(Atleta atleta);

}

