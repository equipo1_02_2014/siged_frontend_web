package edu.ucla.siged.servicio.interfaz.gestiondeportiva;

import edu.ucla.siged.domain.gestionrecursostecnicios.Area;

public interface ServicioActividadPracticaI {

	public boolean existeAreaEnActividadPractica(Area area);
}
