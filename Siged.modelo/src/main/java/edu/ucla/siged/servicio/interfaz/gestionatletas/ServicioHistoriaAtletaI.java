package edu.ucla.siged.servicio.interfaz.gestionatletas;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;

public interface ServicioHistoriaAtletaI {

	public HistoriaAtleta buscarUltimaHistoriaAtleta(Atleta atleta);
}
