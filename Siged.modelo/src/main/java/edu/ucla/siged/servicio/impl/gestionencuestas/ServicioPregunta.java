package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoPreguntaI;
import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioPreguntaI;

@Service("servicioPregunta")
public class ServicioPregunta implements ServicioPreguntaI{
	//private static final int TAMANO_PAGINA = 0;
	@Autowired
	private DaoPreguntaI daoPregunta;
	@Autowired
	private SessionFactory sessionFactory;
	
	public void guardar(Pregunta pregunta) {
		// TODO Auto-generated method stub
		
		if (pregunta.getId()==null){
			//anotador.setEstatus(true);
			daoPregunta.save(pregunta);
		}
		else{
			pregunta.setId(pregunta.getId());
			daoPregunta.save(pregunta);
		}
	}
	
	
	public Page<Pregunta> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoPregunta.findAll(pagina);
	}
	
	public Pregunta buscarPorId(Integer Id) {
		// TODO Auto-generated method stub
		return daoPregunta.findOne(Id);
	}
	
	public List<Pregunta> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Pregunta where "+hql); //daoPregunta.findAll();
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Pregunta> lista =query.list(); //daoPregunta.findAll();
		session.close();
		return lista;

		}

	
	public long totalPreguntas() {
		// TODO Auto-generated method stub
		return daoPregunta.count();
	}


	public long totalPreguntasFiltradas(String hql) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Pregunta where "+hql); //daoPregunta.findAll();
		long total= (Long) query.uniqueResult(); //daoPregunta.findAll();
		session.close();
		return total;

	}
	
	public Pregunta buscarPrimeraPregunta(Encuesta encuesta)
	{
		/*List<Pregunta> preguntas= daoPregunta.findByEncuesta(encuesta.getId());
		if(preguntas.size()>0)
			return preguntas.get(0);
		else*/
			return null;
	}


	
	
}