/** 
 * 	ServicioAreaI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;

public interface ServicioAreaI {

	public static final int TAMANO_PAGINA = 5;
	public void guardar(Area area);
	public Page<Area> buscarTodos(int numeroPagina);
	public List<Area> buscarTodos();
	public Area buscarPorId(Integer Id);
	public List<Area> buscarFiltrado(String hql,int numeroPagina);
	public void  eliminar(Area area);
	public int eliminarVarios(List<Area> area);
	public long totalArea();
	public long totalAreaFiltrados(String hql);
	public boolean buscarArea(Area area);
}