/** 
 * 	ServicioAnotadorI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos;

import java.util.List;
import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;

public interface ServicioAnotadorI {
	public static final int TAMANO_PAGINA = 5;
	public void guardar(Anotador anotador);
	public Page<Anotador> buscarTodos(int numeroPagina);
	public Anotador buscarPorId(Integer Id);
	public List<Anotador> buscarFiltrado(String hql,int numeroPagina);
	public void  eliminar(Anotador anotador);
	public int eliminarVarios(List<Anotador> anotadores);
	public long totalAnotadores();
	public long totalAnotadoresFiltrados(String hql);
	public boolean existeAnotador(String cedula, Integer idAnotador); 
	public boolean existeEnJuego(Anotador anotador);
	public Anotador buscarAnotadorPorCedula(String cedula);
}