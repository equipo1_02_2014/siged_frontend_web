package edu.ucla.siged.servicio.impl.gestiondeportiva;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.domain.gestiondeportiva.CambioTecnico;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCambioTecnicoI;
import edu.ucla.siged.dao.DaoCambioTecnicoI;

@Service("servicioCambioTecnico")
public class ServicioCambioTecnico implements ServicioCambioTecnicoI {
//	
//	@Autowired
//	private DaoCambioTecnicoI daoCambioTecnico;
//	
//	public void guardar(CambioTecnico cambioTecnico) {
//		daoCambioTecnico.save(cambioTecnico);
//
//	}
//
//	public void editar(CambioTecnico cambioTecnico) {
//		 daoCambioTecnico.save(cambioTecnico);
//	}
//
//	public void eliminar(CambioTecnico cambioTecnico) {
//		
//
//	}
//
//	public List<CambioTecnico> buscarPorPractica(Practica practica) {
//		return daoCambioTecnico.findByPractica(practica);
//	}
//
//	public boolean isExistCambioTecnicoPorPractica(Practica practica) {
//		List<CambioTecnico> lista=daoCambioTecnico.findByPractica(practica);
//	    return !lista.isEmpty();
//	}
//
//	public Tecnico buscarTecnicoPrincipalPorPractica(Practica practica) {
//		return daoCambioTecnico.buscarTecnicoPrincipalPorPractica(practica.getId());
//	}
//
//	public Tecnico buscarTecnicoAsistentePorPractica(Practica practica) {
//		return daoCambioTecnico.buscarTecnicoAsistentePorPractica(practica.getId());
//	}
//
//	public List<Tecnico> buscarTecnicosDisponibles(Date fecha, Date hora) {
//		return daoCambioTecnico.buscarTecnicosDisponibles(fecha, hora);
//	}
//
//	public List<CambioTecnico> buscarTodos() {
//		return daoCambioTecnico.findAll();
//	}
//
//	public CambioTecnico buscarCambioTecnicoPrincipalPorPractica(
//			Practica practica) {
//		return daoCambioTecnico.buscarCambioTecnicoPrincipalPorPractica(practica.getId());
//	}
//
//	public CambioTecnico buscarCambioTecnicoAsistentePorPractica(
//			Practica practica) {
//		return daoCambioTecnico.buscarCambioTecnicoAsistentePorPractica(practica.getId());
//	}

}
