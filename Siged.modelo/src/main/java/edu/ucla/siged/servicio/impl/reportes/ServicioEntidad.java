package edu.ucla.siged.servicio.impl.reportes;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoAtributoI;
import edu.ucla.siged.dao.DaoEntidadI;
import edu.ucla.siged.domain.reporte.Atributo;
import edu.ucla.siged.domain.reporte.Entidad;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioEntidadI;

@Service("servicioEntidad")
public class ServicioEntidad implements ServicioEntidadI{
	@Autowired
	DaoEntidadI daoEntidad;
	@Autowired
	DaoAtributoI daoAtributo;
	
	public List<Entidad> buscarTodas() {
		return daoEntidad.findAll();
	}
	
	public List<Atributo> buscarAtributos(Entidad entidad) {
		return daoAtributo.findByEntidad(entidad);
	}
	
}
