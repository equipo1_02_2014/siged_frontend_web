package edu.ucla.siged.servicio.interfaz.seguridad;

import java.util.List;
import java.util.Set;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;

public interface ServicioUsuarioI {
	
	public static final int TAMANO_PAGINA = 5;
	public List<Usuario> buscarTodos();
	public void guardar(Usuario usuario);
	public void eliminar(Usuario usuario);
	public void eliminarVarios(List<Usuario> usuarios);
	public Page<Usuario> buscarTodos(int numeroPagina);
	public List<Usuario> buscarFiltrado(String hql,int numeroPagina);
	public long totalUsuarios();
	public long totalUsuariosFiltrados(String hql);
	public Set<RolUsuario> getRolesUsuario(Usuario usuarioSeleccionado);
	public Usuario buscarByNombreUsuario(String nombreUsuario);
	public Usuario findById(Integer id);
	public Usuario buscarPersonaByCedula(String string);
	public void guardarCedulaUsuario(String cedula, String cedulaNueva);
	public Usuario buscarPorCedula(String cedula);
}