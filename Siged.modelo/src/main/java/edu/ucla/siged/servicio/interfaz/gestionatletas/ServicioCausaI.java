package edu.ucla.siged.servicio.interfaz.gestionatletas;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import edu.ucla.siged.domain.gestionatleta.Causa;


public interface ServicioCausaI {
	

	
	public static final int TAMANO_PAGINA = 4;
	public void guardar(Causa causa);
	public Page<Causa> buscarTodos(int numeroPagina);
	public Causa buscarPorId(Integer Id);
	public List<Causa> buscarFiltrado(String hql,int numeroPagina);
	public void  eliminar(Causa causa);
	public void eliminarVarios(List<Causa> causa);
	public boolean verificarEliminar(Causa causa);
	public long totalCausas();
	public long totalCausasFiltrados(String hql);
	public List<Causa> buscarTodos();
	public List<Causa> buscarTodas(Integer tipo);
	public List<Causa> listaCausasPost();

}
