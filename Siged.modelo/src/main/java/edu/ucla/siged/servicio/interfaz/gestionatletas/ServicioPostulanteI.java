package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionatleta.Postulante;

public interface ServicioPostulanteI {
		public static final int TAMANO_PAGINA = 10;
		public Page<Postulante> buscarTodos(int paginaActual);
		public void guardar(Postulante postulante);
		public Postulante buscarPorId(Integer Id);
		public long totalPostulantes();
		List<Postulante> buscarPostulantesPendientes(int nroPagina);
		List<Postulante> buscarPostulantesAprobados(int nroPagina);
		public long totalPostulantesPendientes();
		public long totalPostulantesAprobados();
		//Agregado por Jesus
		public List<Postulante> buscarPostulantesPorRangoFechaPostulacion(Date fechaInicio,Date fechaFin);
		public List<Postulante> buscarTodos();
}