package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;

import edu.ucla.siged.domain.gestioneventos.Donacion;

public interface ServicioDonacionI {
	
	public static final int TAMANO_PAGINA = 4;
	
	public void Guardar(Donacion d);
	
	public void Eliminar(Integer id);
	
	public List<Donacion> ListarDonaciones(int numeroPagina);
	
	public List<Donacion> ListarDonacionesNE(int numeroPagina);//Donaciones que no viene de un evento
	
	public List<Donacion> buscarFiltrado(String hql,int numeroPagina);
	
	public long totalDonaciones();
	
	public long totalDonacionesFiltrados(String hql);
	
	public void EliminarVarios(List<Donacion> donaciones);

}
