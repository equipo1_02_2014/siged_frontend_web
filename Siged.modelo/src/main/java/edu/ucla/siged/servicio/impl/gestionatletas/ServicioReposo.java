package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoReposoI;
import edu.ucla.siged.domain.gestionatleta.Reposo;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioReposoI;
@Service
public class ServicioReposo implements ServicioReposoI {

	private @Autowired DaoReposoI daoReposo;
	public List<Reposo> listaReposos() {
		return daoReposo.findAll();
	}
	
	public void eliminarReposos(Set<Reposo> reposos) {
		daoReposo.delete(reposos);
	}
	
	
}
