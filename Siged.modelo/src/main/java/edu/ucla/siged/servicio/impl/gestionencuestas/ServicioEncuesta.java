package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import edu.ucla.siged.dao.DaoEncuestaI;
import edu.ucla.siged.dao.DaoOpcionPreguntaI;
import edu.ucla.siged.dao.DaoPreguntaI;
import edu.ucla.siged.dao.DaoRespuestaI;
import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.gestioneventos.Respuesta;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioEncuestaI;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("servicioEncuesta")
public class ServicioEncuesta implements ServicioEncuestaI{
	
	@Autowired private DaoEncuestaI daoEncuestaI;
	@Autowired private DaoPreguntaI daoPreguntaI;
	@Autowired private DaoOpcionPreguntaI daoOpcionesI;
	@Autowired private DaoRespuestaI daoRespuestaI;
	@Autowired private SessionFactory sessionFactory;
	
	public ServicioEncuesta(){
	}

	public void guardar(Encuesta encuesta){
		try{
		daoEncuestaI.save(encuesta);
		}
		catch(Exception e)
		{
			System.out.println("Error al guardar: "+e.getCause());
		}
	}
	
	@Transactional
	public void eliminar(Encuesta encuesta) throws Exception {
		
		encuesta.getPreguntas().size();
		for(Pregunta p: encuesta.getPreguntas())
			p.getOpcionPreguntas().size();
		encuesta.getPublicos().size();
		
		try{
			daoEncuestaI.delete(encuesta);
		}
		catch(Exception e){
			System.out.println("Error:"+e.getCause());
			throw new Exception("No se pudo eliminar la encuesta");
		}
		
		
			/*List<Pregunta> list= daoPreguntaI.findByEncuesta(encuesta.getId());
			
			Session session = sessionFactory.openSession(); 
			
			//session.beginTransaction();
			try{
			Query q;
			for (Pregunta pregunta : list) {
				List<OpcionPregunta> listOpc= daoOpcionesI.findByPregunta(pregunta.getId());
				for(OpcionPregunta op: listOpc){
					List<Respuesta> listResp= daoRespuestaI.findByOpcionPregunta(op.getId());
					
					for(Respuesta r:listResp){
					q=session.createQuery("delete from Respuesta where id="+r.getId()); 	
					q.executeUpdate();	
					}
					
					q=session.createQuery("delete from Opcion where id="+op.getId()); 	
					q.executeUpdate();
				}
									
				q=session.createQuery("delete from Pregunta where id="+pregunta.getId()); 	
				q.executeUpdate();
			}
			
			Query query = session.createQuery("delete from Encuesta where id="+encuesta.getId());
			int deletedCount = query.executeUpdate();
			session.getTransaction().commit();
			}
			catch(Exception e){
				session.getTransaction().rollback();	
				System.out.println("Error:"+e.getCause());
				throw new Exception("No se pudo eliminar la encuesta");
			}
			
			session.close();*/
		
	}
	
	public void eliminarVarios(List<Encuesta> encuestas) throws Exception {
		for( Encuesta encuesta : encuestas){
			try {
				this.eliminar(encuesta);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new Exception("No se pudo completar la operaci�n");
			}
		}
	}
	
	public List<Encuesta> buscarTodos(int numeroPagina) {
		// TODO Auto-generated method stub
			PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
			List<Encuesta> list= daoEncuestaI.findAll(pagina).getContent();
					
			for(Encuesta encuesta : list){
				encuesta.getPreguntas().size();
				for(Pregunta p: encuesta.getPreguntas())
					p.getOpcionPreguntas().size();
				
				encuesta.getPublicos().size();
				
			}
			return list;
		}
		
	public void actualizar(Encuesta encuesta){
		daoEncuestaI.save(encuesta);
	}


	public long totalEncuestas() {
		// TODO Auto-generated method stub
		return daoEncuestaI.count();
	}

	public List<Encuesta> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		System.out.println("buscarFiltrado 152");
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Encuesta where "+hql); 
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		System.out.println("buscar filtrado " + query);
		List<Encuesta> lista =query.list(); 
		for(Encuesta encuesta : lista){
			encuesta.getPreguntas().size();
			for(Pregunta p: encuesta.getPreguntas())
				p.getOpcionPreguntas().size();
			
			encuesta.getPublicos().size();
			
		}
		session.close();
		return lista;
		
		
		}

	public long totalEncuestasFiltradas(String hql) {
		
			// TODO Auto-generated method stub
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("select count(*) from Encuesta where "+hql); 
			System.out.println("total encuestas 177 ->"+query);
			long total= (Long) query.uniqueResult(); 
			
			session.close();
			return total;

		

	}

	public Encuesta buscarEncuestaByNombre(String nombre) {
		
		return daoEncuestaI.findByNombre(nombre);
	}

	 //Encuestas Publicas
	public List<Encuesta> listarEncuestasPublicas(){
		Session session = sessionFactory.openSession(); 
		List results=session.createSQLQuery("SELECT encuesta.id FROM encuesta where encuesta.id "
				+ "not in (select encuesta.id from encuesta, publico "
				+ "where encuesta.id=publico.idencuesta and publico.responder=1) "
				+ " and encuesta.fechafin>=current_date and encuesta.fechainicio<=current_date").list();
		Encuesta enc=new Encuesta();
		List<Integer> lista=new ArrayList<Integer>();
		List<Encuesta> encuestas= new ArrayList<Encuesta>();
		for (Iterator iter = results.iterator(); iter.hasNext();) {
		     //Object[] objects = (Object[]) iter.next();
		     Integer id = (Integer) iter.next();
		     enc=daoEncuestaI.findById(id);
		     for(Pregunta p: enc.getPreguntas())
					p.getOpcionPreguntas().size();
				enc.getPublicos().size();
		     lista.add(id);
		     encuestas.add(enc);
		     System.out.println("Encuesta Id: " + id);
		  }
		session.close();
		return encuestas;
	}
	
	//Encuestas Publicas y para determinados roles
	public List<Encuesta> listarEncuestasPublicasParaRoles(List<Rol> roles){
		String rolRestriccion="";
		for (int i = 0; i < roles.size(); i++){
			rolRestriccion+=roles.get(i).getId();
			if(i+1<roles.size())
			rolRestriccion+=",";
		}
		System.out.println(rolRestriccion);
		Session session = sessionFactory.openSession(); 
		String q="SELECT distinct encuesta.id FROM public.encuesta left "
				+ "join publico on encuesta.id=publico.idencuesta where (publico.idencuesta is null or"
				+ " publico.responder!=1) "
				+ " and encuesta.fechafin>=current_date and encuesta.fechainicio<=current_date";
		if(roles.size()>0)
		q+=" or publico.idrol in ("+rolRestriccion+") ";
		System.out.println(q);
		List results=session.createSQLQuery(q).list();
		Encuesta enc=new Encuesta();
		List<Integer> lista=new ArrayList<Integer>();
		List<Encuesta> encuestas= new ArrayList<Encuesta>();
		for (Iterator iter = results.iterator(); iter.hasNext();) {
		     //Object[] objects = (Object[]) iter.next();
		     Integer id = (Integer) iter.next();
		     enc=daoEncuestaI.findById(id);
		     for(Pregunta p: enc.getPreguntas())
					p.getOpcionPreguntas().size();
				enc.getPublicos().size();
		     lista.add(id);
		     encuestas.add(enc);
		     System.out.println("Encuesta Id: " + id);
		  }
		session.close();
		return encuestas;
	}

	// Encuestas abiertas publicas y determinadas para ciertos roles no respondidas por un usuario 
	public List<Encuesta> listarEncuestasNoRespondidasUsuario(List<Rol> roles, Integer idusuario){
		String rolRestriccion="";
		for (int i = 0; i < roles.size(); i++){
			rolRestriccion+=roles.get(i).getId();
			if(i+1<roles.size())
			rolRestriccion+=",";
		}
		System.out.println(rolRestriccion);
		Session session = sessionFactory.openSession(); 
		String q="SELECT distinct encuesta.id FROM " + 
				  "encuesta left join publico on encuesta.id=publico.idencuesta " +
				  "where (publico.idrol in ("+rolRestriccion+")  or publico.idencuesta is null) and encuesta.id not in " +
				  "( SELECT "+ 
				  "encuesta.id " +
					" FROM " +
					"  pregunta, "+ 
					"  usuario, "+
					"  encuesta, "+
					"  respuesta, " +
					"  opcionpregunta "+ 
					"WHERE "+
					"pregunta.idencuesta=encuesta.id and "+
					" pregunta.id = opcionpregunta.idpregunta AND " +
					"  usuario.id = respuesta.idusuario AND " +
					"  opcionpregunta.id = respuesta.idopcionpregunta AND " +
					" usuario.id='"+ idusuario + "') and encuesta.fechafin>=current_date and encuesta.fechainicio<=current_date";
		System.out.println(q);
		List results=session.createSQLQuery(q).list();
		Encuesta enc=new Encuesta();
		List<Integer> lista=new ArrayList<Integer>();
		List<Encuesta> encuestas= new ArrayList<Encuesta>();
		for (Iterator iter = results.iterator(); iter.hasNext();) {
		     //Object[] objects = (Object[]) iter.next();
		     Integer id = (Integer) iter.next();
		     enc=daoEncuestaI.findById(id);
		     for(Pregunta p: enc.getPreguntas())
					p.getOpcionPreguntas().size();
				enc.getPublicos().size();
		     lista.add(id);
		     encuestas.add(enc);
		     System.out.println("Encuesta Id: " + id);
		  }
		session.close();
		return encuestas;
	}
	
	//lista de encuestas abiertas que pueden ser respondidas para un usuario de acuerdo a sus roles  (con permiso de responderlas)
	public List<Encuesta> listarEncuestasUsuarioParaVotar(List<Rol> roles, Integer idusuario) {
		List<Encuesta> lista= listarEncuestasNoRespondidasUsuario(roles,idusuario);
		List<Encuesta> listaResponder=new ArrayList<Encuesta>();
		boolean agregada=false;
		for (Encuesta encuesta : lista) {
			encuesta.getPublicos().size();
			System.out.println("SIZE  PUBLICOS"+encuesta.getPublicos().size());
			List<Publico> publicos =getListaPublico(encuesta);
			if(publicos.size()>0){
			for (Publico p : publicos) {
				for (Rol rol : roles) {
					if(rol.getId()==p.getRol().getId() && p.getResponder()==1 )
					{listaResponder.add(encuesta);
					 agregada=true;
					 break;
					}
				}
				if(agregada==true)
					break;
			}
			}
			agregada=false;
			
		}
		return lista;
	}
		
		
	public List<Publico> getListaPublico(Encuesta encuesta) {
		encuesta.getPublicos().size();
		Set<Publico> publicos=encuesta.getPublicos();
		Object[] publicost=publicos.toArray();
		Publico publicot= new Publico();
		List<Publico> lista = new ArrayList<Publico>();
		for(int i=0; i<publicost.length;i++)
		{
			publicot= (Publico) publicost[i];
			lista.add(publicot);
		}
		System.out.println("PASO CARGA PUBLICO SIZe"+ lista.size());
		return lista;
	}
	
	//Retorna una lista de floats en la cual en la posicion 0 est�n los votos de la opci�n y en la posicion 1
	// est� el porc que representan esos votos de acuerdo al total de votos de la pregunta
	public List<String> getResultadoOpcionPregunta(Integer idOpcion) {
		Session session = sessionFactory.openSession(); 
			
		List results=session.createSQLQuery("select opcionid, votos, total, preguntaid, "
				+ " (select p.idencuesta from pregunta as p where p.id=preguntaid) as idencuesta, "
				+ "  (CASE WHEN total>0 then round((cast(votos as numeric)/cast(total as numeric))*100,2) ELSE 0 END) as porc from (SELECT "
				+ "  opcionpregunta.id as opcionid,   count(respuesta.idopcionpregunta) as votos"
				+ " FROM opcionpregunta left join respuesta on opcionpregunta.id = respuesta.idopcionpregunta"
				+ " group by opcionpregunta.id) as t, (SELECT count(respuesta.id) as total, pregunta.id as "
				+ " preguntaid FROM opcionpregunta, respuesta, pregunta where "
				+ " opcionpregunta.id = respuesta.idopcionpregunta and pregunta.id=opcionpregunta.idpregunta"
				+ " group by pregunta.id) as o where (select w.idpregunta from "
				+ " opcionpregunta as w where t.opcionid=w.id)=o.preguntaid and opcionid="+idOpcion.toString()
				+ " order by idencuesta ASC").list();
	
		List<String> lista=new ArrayList<String>();
		for (Iterator iter = results.iterator(); iter.hasNext();) {
		     Object[] objects = (Object[]) iter.next();
		     String var = objects[1].toString(); //cantidad de votos
		     lista.add(var);
		     var = objects[5].toString(); //porcentaje
		     lista.add(var);
		     System.out.println("opcion: " + idOpcion +" votos: "+lista.get(0) + " Porc: "+ lista.get(1));
		  }
		session.close();
		return lista;	
	}
	
	public String getTotalVotosPregunta(Integer idPregunta) {
		Session session = sessionFactory.openSession(); 
		List results=session.createSQLQuery("SELECT count(respuesta.id) as total, pregunta.id as preguntaid"
				+ " FROM opcionpregunta, respuesta, pregunta where opcionpregunta.id = respuesta.idopcionpregunta "
				+ " and pregunta.id=opcionpregunta.idpregunta and pregunta.id="+ idPregunta.toString()
				+ "  group by pregunta.id").list();
		
		String var= "0";
		for (Iterator iter = results.iterator(); iter.hasNext();) {
		     Object[] objects = (Object[]) iter.next();
		     var =  objects[0].toString(); //cantidad de votos de la pregunta
		     System.out.println("pregunta: " + idPregunta +" votos: "+ var );
		  }
		session.close();
		return var;	
	}
	
	
	// Encuestas publicas y determinadas para ciertos roles que estan cerradas (fecha fin<HOY)
		public List<Encuesta> listarEncuestasCerradasParaRoles(List<Rol> roles){
			System.out.println("listarEncuestasCerradasParaRoles 389");
			
			String rolRestriccion="";
			for (int i = 0; i < roles.size(); i++){
				rolRestriccion+=roles.get(i).getId();
				if(i+1<roles.size())
				rolRestriccion+=",";
			}
			System.out.println(rolRestriccion);
			Session session = sessionFactory.openSession(); 
			String q="SELECT distinct encuesta.id FROM encuesta left join publico on "
					+ " encuesta.id=publico.idencuesta where (publico.idrol in ("+rolRestriccion+") or "
					+ " publico.idencuesta is null) and encuesta.fechafin<current_date";
			
			System.out.println(q);
			List results=session.createSQLQuery(q).list();
			Encuesta enc=new Encuesta();
			List<Integer> lista=new ArrayList<Integer>();
			List<Encuesta> encuestas= new ArrayList<Encuesta>();
			for (Iterator iter = results.iterator(); iter.hasNext();) {
			     //Object[] objects = (Object[]) iter.next();
			     Integer id = (Integer) iter.next();
			     enc=daoEncuestaI.findById(id);
			     for(Pregunta p: enc.getPreguntas())
						p.getOpcionPreguntas().size();
					enc.getPublicos().size();
			     lista.add(id);
			     encuestas.add(enc);
			     System.out.println("Encuesta Id: " + id);
			  }
			session.close();
			return encuestas;
		}
		
		//lista de id de las encuestas cerradas cuyos resultados pueden ser vistos para un usuario de acuerdo a sus "roles"  (con permiso de ver resultados)
		public List<Integer> listarIdEncuestasCerradasParaVerResultados(List<Rol> roles) {
			System.out.println("listarIdEncuestasCerradasParaVerResultados 423");
			List<Encuesta> lista= listarEncuestasCerradasParaRoles(roles);
			List<Encuesta> listaResultados=new ArrayList<Encuesta>();
			List<Integer> listaIdEncuesta= new ArrayList<Integer>();
			boolean agregada=false;
			for (Encuesta encuesta : lista) {
				encuesta.getPublicos().size();
				encuesta.getPreguntas().size();
			
				List<Publico> publicos =getListaPublico(encuesta);
				if(publicos.size()>0){
				for (Publico p : publicos) {
					for (Rol rol : roles) {
						if(rol.getId()==p.getRol().getId() && p.getVerResultados()==1 )
						{listaResultados.add(encuesta);
						 listaIdEncuesta.add(encuesta.getId());  
						 agregada=true;
						 break;
						}
					}
					if(agregada==true)
						break;
				}
				}
				agregada=false;
				
			}
			return listaIdEncuesta;
		}
		
		//Retorna la lista paginada de encuestas cerradas las cuales son publicas o aquellas que son privadas
		// pero se les permite ver los resultados a los roles (parametro)
		
		public List<Encuesta> listarEncuestasCerradasParaVerResultados(List<Rol> roles, int numeroPagina) {
			System.out.println("listarEncuestasCerradasParaVerResultados 459");
			List<Integer> listaIds= listarIdEncuestasCerradasParaVerResultados(roles);
			String idRestriccion="";
			for (int i = 0; i < listaIds.size(); i++){
				idRestriccion+=listaIds.get(i);
				if(i+1<listaIds.size())
				idRestriccion+=",";
			}
			if(listaIds.size()>0){
			List<Encuesta> list= buscarFiltrado("id in ("+idRestriccion+") order by id desc", numeroPagina);
			for(Encuesta encuesta : list){
				encuesta.getPreguntas().size();
				for(Pregunta p: encuesta.getPreguntas())
					p.getOpcionPreguntas().size();
				
				encuesta.getPublicos().size();
				
			}
			return list;
			}
			else
			return null;
		}
		
		public long totalEncuestasCerradasParaVerResultados(List<Rol> roles) {
			// TODO Auto-generated method stub
			List<Integer> listaIds= listarIdEncuestasCerradasParaVerResultados(roles);
			String idRestriccion="";
			for (int i = 0; i < listaIds.size(); i++){
				idRestriccion+=listaIds.get(i);
				if(i+1<listaIds.size())
				idRestriccion+=",";
			}
			if(listaIds.size()>0)
				return totalEncuestasFiltradas("id in ("+idRestriccion+")");
			else
				return 0;
		}
		
		
		 //Ids Encuestas Publicas Cerradas
		public List<Integer> listarIdEncuestasPublicasCerradas(){
			Session session = sessionFactory.openSession(); 
			List results=session.createSQLQuery("SELECT encuesta.id FROM encuesta where encuesta.id "
					+ "not in (select encuesta.id from encuesta, publico "
					+ "where encuesta.id=publico.idencuesta) "
					+ " and encuesta.fechafin<current_date").list();
			Encuesta enc=new Encuesta();
			List<Integer> lista=new ArrayList<Integer>();
			List<Encuesta> encuestas= new ArrayList<Encuesta>();
			for (Iterator iter = results.iterator(); iter.hasNext();) {
			     //Object[] objects = (Object[]) iter.next();
			     Integer id = (Integer) iter.next();
			     enc=daoEncuestaI.findById(id);
			     for(Pregunta p: enc.getPreguntas())
						p.getOpcionPreguntas().size();
					enc.getPublicos().size();
			     lista.add(id);
			     encuestas.add(enc);
			     System.out.println("Encuesta Id: " + id);
			  }
			session.close();
			return lista;
		}
		
	
	//Retorna la lista paginada de encuestas cerradas las cuales son publicas	
		public List<Encuesta> listarEncuestasCerradasPublicasPaginada(int numeroPagina) {
			// TODO Auto-generated method stub
				List<Integer> listaIds= listarIdEncuestasPublicasCerradas();
						
				String idRestriccion="";
				for (int i = 0; i < listaIds.size(); i++){
					idRestriccion+=listaIds.get(i);
					if(i+1<listaIds.size())
					idRestriccion+=",";
				}
				if(listaIds.size()>0){
				List<Encuesta> list= buscarFiltrado("id in ("+idRestriccion+") order by id desc", numeroPagina);
				for(Encuesta encuesta : list){
					encuesta.getPreguntas().size();
					for(Pregunta p: encuesta.getPreguntas())
						p.getOpcionPreguntas().size();
					
					encuesta.getPublicos().size();
					
				}
				return list;
				}
				else
					return null;

			}
		
		public long totalEncuestasCerradasPublicas() {
			// TODO Auto-generated method stub
			List<Integer> listaIds= listarIdEncuestasPublicasCerradas();
			String idRestriccion="";
			for (int i = 0; i < listaIds.size(); i++){
				idRestriccion+=listaIds.get(i);
				if(i+1<listaIds.size())
				idRestriccion+=",";
			}
			if(listaIds.size()>0)
				return totalEncuestasFiltradas("id in ("+idRestriccion+")");
			else
				return 0;
		}


		
	
}