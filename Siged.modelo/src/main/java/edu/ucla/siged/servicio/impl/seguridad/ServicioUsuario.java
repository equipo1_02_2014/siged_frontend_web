package edu.ucla.siged.servicio.impl.seguridad;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoArbitroI;
import edu.ucla.siged.dao.DaoAtletaI;
import edu.ucla.siged.dao.DaoRepresentanteI;
import edu.ucla.siged.dao.DaoRolUsuarioI;
import edu.ucla.siged.dao.DaoTecnicoI;
import edu.ucla.siged.dao.DaoUsuarioI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;


@Service("servicioUsuario")

public class ServicioUsuario implements ServicioUsuarioI{
	
	@Autowired private DaoUsuarioI daoUsuario;
	@Autowired private DaoRolUsuarioI daoRolUsuario;
	
	@Autowired private DaoAtletaI daoAtleta;
	@Autowired private DaoTecnicoI daoTecnico;
	@Autowired private DaoArbitroI daoArbitro;
	@Autowired private DaoRepresentanteI daoRepresentante;
	@Autowired private SessionFactory sessionFactory;
	
	public List<Usuario> buscarTodos() {
		return daoUsuario.findAll();
	}

	public List<Usuario> buscarTodosEstatus(short i, int numeroPagina) {
		Pageable pagina = new PageRequest(numeroPagina, TAMANO_PAGINA);
		return daoUsuario.findByEstatus(i, pagina).getContent();
	}
	public void guardar(Usuario usuario) {
		// TODO Auto-generated method stub
		daoUsuario.save(usuario);
	}
	
	public void eliminar(Usuario usuario) {
		// TODO Auto-generated method stub
		usuario.setEstatus((short) 0);
		daoUsuario.save(usuario);
	}

	public void eliminarVarios(List<Usuario> usuarios) {
		// TODO Auto-generated method stub
		for (int i=0;i<usuarios.size();i++)
			usuarios.get(i).setEstatus(Short.parseShort("0"));
			
		daoUsuario.save(usuarios);
	}
	
	public void activar(Usuario usuario) {
		// TODO Auto-generated method stub
		usuario.setEstatus((short) 1);
		daoUsuario.save(usuario);
	}

	public void activarVarios(List<Usuario> usuarios) {
		// TODO Auto-generated method stub
		for (int i=0;i<usuarios.size();i++)
			usuarios.get(i).setEstatus(Short.parseShort("1"));
			
		daoUsuario.save(usuarios);
	}
	@Transactional
	public void eliminarFisico(Usuario usuario) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("buscando roles"+"/usuer:"+usuario.getId());
		List<RolUsuario> list=daoRolUsuario.findByUsuario(usuario);
		Session session = sessionFactory.openSession(); 
		session.beginTransaction();
		try{
		Query q;
		for (RolUsuario rolUsuario : list) {
			System.out.println("Eliminando"+rolUsuario.getId()+"/usuer:"+rolUsuario.getUsuario().getId());
			//session.delete(rolUsuario);
			q=session.createQuery("delete from RolUsuario where id="+rolUsuario.getId()); 	
			q.executeUpdate();
		}
		//session.createQuery("delete from Usuario where id="+usuario.getId()); 
		System.out.println("Eliminando uuario:"+usuario.getId());
		Query query = session.createQuery("delete from Usuario where id="+usuario.getId());
		int deletedCount = query.executeUpdate();
		//usuario=(Usuario) session.load(Usuario.class, usuario.getId());
		//session.delete(usuario);
		System.out.println(session.toString());
		session.getTransaction().commit();
		System.out.println("Todo va biennnnnnnnnnnnnnnnnn");
		}
		catch(Exception e){
			session.getTransaction().rollback();	
			System.out.println("Error:"+e.getCause());
			throw new Exception("No se pudo eliminar el rol");
		}
		
		
		session.close();
		
	}
	
	public void eliminarVariosFisico(List<Usuario> listaUsuarios) throws Exception {
		Session session = sessionFactory.openSession(); 
		session.beginTransaction();
		try{
		Query q;
		for (Usuario usuario : listaUsuarios) {
			List<RolUsuario> list=daoRolUsuario.findByUsuario(usuario);
			for (RolUsuario rolUsuario : list) {
				//session.delete(rolUsuario);
				q=session.createQuery("delete from RolUsuario where id="+rolUsuario.getId()); 	
				q.executeUpdate();
			}
			Query query = session.createQuery("delete from Usuario where id="+usuario.getId());
			int deletedCount = query.executeUpdate();
			//session.delete(usuario);
			session.getTransaction().commit();
		}
		}
		catch(Exception e){
			session.getTransaction().rollback();
			throw new Exception("No se pudo eliminar el rol");
		}
		
		session.close();
	}
	
	public Page<Usuario> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoUsuario.findAll(pagina);
	}
	
	public List<Usuario> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
	
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Usuario where "+hql); 
		System.out.println("from Usuario where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Usuario> lista =query.list(); 
		session.close();
		return lista;
		
		
		}

	public long totalUsuarios() {
		// TODO Auto-generated method stub
		return daoUsuario.count();
	}
	
	public long totalUsuariosEstatus(short i) {
		// TODO Auto-generated method stub
		return daoUsuario.countByEstatus( i);
	}

	public long totalUsuariosFiltrados(String hql) {
		
			// TODO Auto-generated method stub
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("select count(*) from Usuario where "+hql); 
			long total= (Long) query.uniqueResult(); 
			session.close();
			return total;

		

	}
	
	public Set<RolUsuario> getRolesUsuario(Usuario usuarioSeleccionado) {
		Hibernate.initialize(usuarioSeleccionado.getRoles());
    	//usuarioSeleccionado.getRoles().size();
    	for(RolUsuario rolu: usuarioSeleccionado.getRoles()){
			
			Hibernate.initialize(rolu.getRol());
		}
    	
    	return usuarioSeleccionado.getRoles();
	}
	
	public List<RolUsuario> getRolesActivosUsuario(Usuario usuario) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from RolUsuario where idUsuario='"+usuario.getId()+"' and estatus='"+"1'"); 
		List<RolUsuario> lista =query.list(); 
		session.close();
		return lista;
	}
	
	public Usuario buscarByNombreUsuario(String nombreUsuario)
	{
		return daoUsuario.findByNombreUsuario(nombreUsuario);
	}
	
	public Usuario findById(Integer id){
		return daoUsuario.findById(id);
	}

	public Usuario buscarPersonaByCedula(String cedula) {
		Atleta atleta=new Atleta();
		Tecnico tecnico= new Tecnico();
		Arbitro arbitro= new Arbitro();
		Representante representante= new Representante();
		Usuario usuario=new Usuario();
		usuario=daoUsuario.findByCedula(cedula);
		if(usuario!=null)
			return usuario;
		else{
			
			atleta=daoAtleta.findByCedula(cedula);
			if(atleta!=null)
			{
				usuario=new Usuario(atleta.getFoto(), atleta.getNombre(), atleta.getApellido(),
									atleta.getFechaNacimiento(), atleta.getEmail(), atleta.getCelular(),
									atleta.getTelefono());
				usuario.setFechaRegistro(new Date());
				return usuario;
			}
			else{
				representante=daoRepresentante.findByCedula(cedula);
				if(representante!=null){
					usuario=new Usuario(representante.getFoto(), representante.getNombre(), representante.getApellido(),
							representante.getFechaNacimiento(), representante.getEmail(), representante.getCelular(),
							representante.getTelefono());
					usuario.setFechaRegistro(new Date());
					return usuario;
				}
				else{
					tecnico=daoTecnico.findByCedula(cedula);
					if(tecnico!=null){
						usuario=new Usuario(tecnico.getFoto(), tecnico.getNombre(), tecnico.getApellido(),
								tecnico.getFechaNacimiento(), tecnico.getEmail(), tecnico.getCelular(),
								tecnico.getTelefono());
						usuario.setFechaRegistro(new Date());
						return usuario;
					}
					else
					{
						arbitro=daoArbitro.findByCedula(cedula);
						if(arbitro!=null){
							usuario=new Usuario(arbitro.getFoto(), arbitro.getNombre(), arbitro.getApellido(),
									arbitro.getFechaNacimiento(), arbitro.getEmail(), arbitro.getCelular(),
									arbitro.getTelefono());
							usuario.setFechaRegistro(new Date());
							return usuario;
						}
						else
							return null;
						
					}
				}
			}
			
		}
	}

	public void guardarCedulaUsuario(String cedula, String cedulaNueva) {
		Usuario usuario=daoUsuario.findByCedula(cedula);
		usuario.setCedula(cedulaNueva);
		daoUsuario.save(usuario);
	}

	public Usuario buscarPorCedula(String cedula) {
		// TODO Auto-generated method stub
		return daoUsuario.findByCedula(cedula);
	}
	
	
		
	
}