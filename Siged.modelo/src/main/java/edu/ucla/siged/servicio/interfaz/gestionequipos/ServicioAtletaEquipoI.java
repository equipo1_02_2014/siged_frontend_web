package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface ServicioAtletaEquipoI {
	
	List<AtletaEquipo> obtenerListaAtletaEquipo( Equipo equipo);
	public void guardarAtletaEquipo(AtletaEquipo atletaEquipo);
	public void eliminarAtletaEquipo(Atleta atleta); // Necesario para retirar un atleta del equipo - Luis
	public void revocarAtletasEquipoCerrado(Equipo equipo); // Necesario para retirar todos los atletas de un equipo al cerrarlo - Luis & Yessica
}