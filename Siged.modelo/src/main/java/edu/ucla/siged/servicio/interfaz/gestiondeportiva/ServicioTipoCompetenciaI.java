package edu.ucla.siged.servicio.interfaz.gestiondeportiva;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;

public interface ServicioTipoCompetenciaI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(TipoCompetencia tipoCompetencia);
	public Page<TipoCompetencia> buscarTodos(int numeroPagina);
	public TipoCompetencia buscarPorId(Integer Id);
	public List<TipoCompetencia> buscarFiltrado(String hql,int numeroPagina);
	public void  actualizar(TipoCompetencia tipoCompetencia);
	public void actualizarVarios(List<TipoCompetencia> tipoCompetencias);
	public void eliminar(TipoCompetencia tipoCompetencia);
	public void eliminarVarios(List<TipoCompetencia> tipoCompetencias);
	public long totalTipoCompetencia();
	public long totalTipoCompetenciasFiltradas(String hql);
}