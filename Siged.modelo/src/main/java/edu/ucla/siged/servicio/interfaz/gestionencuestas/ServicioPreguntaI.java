package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;

public interface ServicioPreguntaI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(Pregunta pregunta);
	public Page<Pregunta> buscarTodos(int numeroPagina);
	public Pregunta buscarPorId(Integer Id);
	public List<Pregunta> buscarFiltrado(String hql,int numeroPagina);
	public long totalPreguntas();
	public long totalPreguntasFiltradas(String hql);
	public Pregunta buscarPrimeraPregunta(Encuesta encuesta);
	
}