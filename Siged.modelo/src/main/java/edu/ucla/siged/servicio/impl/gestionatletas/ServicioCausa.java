package edu.ucla.siged.servicio.impl.gestionatletas;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoActividadPracticaI;
import edu.ucla.siged.dao.DaoCausaI;
import edu.ucla.siged.dao.DaoPostulanteI;
import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioCausaI;

@Service("servicioCausa")
public class ServicioCausa implements ServicioCausaI {
	
	@Autowired
	private DaoCausaI daoCausa;
	
	@Autowired
	private DaoPostulanteI daoPostulante;
	@Autowired
	private DaoActividadPracticaI daoActividadPractica;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void guardar(Causa causa) {
		if (causa.getId() ==null){
			daoCausa.save(causa);
		}
		else{
			causa.setId(causa.getId());
			daoCausa.save(causa);
		}
	}
	
	public Page<Causa> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoCausa.findAll(pagina);
	}
	
	public Causa buscarPorId(Integer Id) {
		return daoCausa.findOne(Id);
	}
	
	public List<Causa> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Causa where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Causa> lista =query.list();
		session.close();
		return lista;
		}

	@Transactional
	public void eliminar(Causa causa) {
		daoCausa.delete(causa);
		
	}
	
	@Transactional
	public void eliminarVarios(List<Causa> causas) {
		for (int i=0;i<causas.size();i++)
			
		daoCausa.delete(causas);
	}
	
	public boolean verificarEliminar(Causa causa)
	{
		
			boolean ocupado;
		
		if (daoPostulante.buscarCausaPTrue(causa.getId()).isEmpty())
		{
			if (daoActividadPractica.buscarCausaATrue(causa.getId()).isEmpty())
			{ 
				return  ocupado=false;
				}
			else{
				return  ocupado=true;
			}
		}
		else {
			return  ocupado=true;
		}
			
			//return false;
	}
	
	public long totalCausas() {
		return daoCausa.count();
	}

	public long totalCausasFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Causa where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}

	public List<Causa> buscarTodos() {
		// TODO Auto-generated method stub
		return daoCausa.findAll();
	}
	
	public List<Causa> buscarTodas(Integer tipo) {
		// TODO Auto-generated method stub
		return daoCausa.findByTipo(tipo);
	}

	public List<Causa> listaCausasPost(){
		return daoCausa.listaCausas();
	}
	

}
