package edu.ucla.siged.servicio.interfaz.gestiondeportiva;

import java.util.Collection;

import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;
import java.util.Set;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.PosicionArbitro;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

public interface ServicioJuegoI {
    
public static int MINUTOS_DURACION_JUEGO=59;
	
	public static int TAMANIO_PAGINA=5;
	
	public void guardar(Juego juego);
	
    public void editar(Juego juego);
    
    public List<Juego> buscarTodos();
    
    public List<Juego> buscarTodos(int nroPagina);
    
    public List<Juego> buscarTodosCompetenciasActivas(int nroPagina);
    
    public Set<ArbitroJuego> obtenerArbitrosJuego(Juego juego);
    
    public List<ArbitroJuego> obtenerListaArbitrosJuego(Juego juego);
    
    public long obtenerCantidadJuegos();
    
    public long obtenerCantidadJuegosCompetenciasActivas();
    
    public int obtenerTamanioPagina();
    
    public boolean isEquipoJuegoFechaHora(Equipo equipo,Date fecha,Date hora);
    
    public boolean isAreaOcupadaFechaHora(Area area, Date fecha, Date hora);
    
    public List<Area> buscarAreasJuego();
    
    public List<Anotador> buscarAnotadoresDisponibles(Date fecha,Date hora);
    
    public List<Arbitro> buscarArbitrosDisponibles(Date fecha,Date hora);
    
    public List<PosicionArbitro> buscarPosicionesArbitros();
    
    public List<Juego> buscarJuegosCompetenciaTerminados(Competencia competencia);
    
    public List<Juego> buscarJuegosPasados(int nroPagina);
    
    public long obtenerCantidadJuegosPasados();
    
    public List<Juego> buscarJuegosPasadosSinResultado(int nroPagina);
    
    public long obtenerCantidadJuegosPasadosSinResultado();
    
    public List<Juego> buscarFiltrado(String hql, int numeroPagina);
    
    public List<Juego> buscarFiltradoJuegosPasados(String hql, int numeroPagina);
    
    public long obtenerTotalFiltrados(String hql);
    
    public List<Juego> buscarJuegosDeAnotadorPorFechaAndHora(Anotador anotador,Date fecha,Date horaInicio);
    
    public List<Juego> buscarJuegosDeArbitroPorFechaAndHora(Arbitro arbitro,Date fecha,Date horaInicio);
    
    public List<Juego> buscarJuegosDeAreaPorFechaAndHora(Area area,Date fecha,Date horaInicio);
    
    public List<Juego> buscarJuegosDeEquipoPorFechaAndHora(Equipo equipo,Date fecha,Date horaInicio);
    
    public boolean existeOtroJuegoDeEquipoPorFechaYHora(Juego juego,Equipo equipo,Date fecha,Date horaInicio);
    
    public boolean existeOtroJuegoDeAreaPorFechaYHora(Juego juego,Area area,Date fecha,Date horaInicio);
    
    public boolean existeArbitrosRepetidos(Collection<ArbitroJuego> arbitros);
    
    public boolean existeOtroJuegoDeArbitroPorFechaYHora(Juego juego,Arbitro arbitro,Date fecha,Date horaInicio);
    
    public boolean existeOtroJuegoDeAnotadorPorFechaYHora(Juego juego,Anotador anotador,Date fecha,Date horaInicio);
    
     public Page<Juego>  buscarJuegosFuturos(int numeroPagina);
    
    public int obtenerCantidadJuegosFuturos();
    
    public boolean isAreaOcupadaHorario(Area area, Date fecha,Date horaInicio);
    
  //Solicitado para verificar la existencia de un Area en algun juego
   public boolean existeAreaEnJuego(Area area);
   
  //Se agregan para soporte de acciones propias del usuario
   
   public List<Juego> buscarJuegosPorRepresentante(int nroPagina,Representante representante);
   
   public long obtenerCantidadJuegosPorRepresentante(Representante representante);
   
   public List<Juego> buscarJuegosPorTecnico(int nroPagina,Tecnico tecnico);
   
   public long obtenerCantidadJuegosPorTecnico(Tecnico tecnico);
   
   public List<Juego> buscarJuegosPorAtleta(int numeroPagina,Atleta atleta);
   
   public long obtenerCantidadJuegosPorAtleta(Atleta atleta);
}

