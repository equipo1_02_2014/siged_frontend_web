package edu.ucla.siged.servicio.interfaz.seguridad;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.seguridad.Rol;

public interface ServicioRolI {
	
	public static final int TAMANO_PAGINA = 5;
	public List<Rol> buscarTodos();
	public List<Rol> buscarActivos();
	public void guardar(Rol rol);
	public void eliminar(Rol rol) throws Exception;
	public void eliminarVarios(List<Rol> roles) throws Exception;
	public Page<Rol> buscarTodos(int numeroPagina);
	public List<Rol> buscarFiltrado(String hql,int numeroPagina);
	public long totalRoles();
	public long totalRolesFiltrados(String hql);
	public Rol buscarRolByNombre(String nombre);
	
}