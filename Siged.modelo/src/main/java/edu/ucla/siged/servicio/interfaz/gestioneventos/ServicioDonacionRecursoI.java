package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;
import java.util.Set;

import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;

public interface ServicioDonacionRecursoI {
	
public void Guardar(DonacionRecurso dr);
	
	public void Eliminar(Integer id);
	
	public void EliminarVarios(Set<DonacionRecurso> drs);
	
	public List<DonacionRecurso> ListarDonacionRecursos();

}
