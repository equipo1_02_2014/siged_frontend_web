package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoParentescoI;
import edu.ucla.siged.domain.gestionatleta.Parentesco;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioParentescoI;

@Service
public class ServicioParentesco implements ServicioParentescoI {

	private @Autowired DaoParentescoI daoParentesco;
	
	public List<Parentesco> obtenerListaParentesco() {
		return daoParentesco.findAll();
	}
	
}
