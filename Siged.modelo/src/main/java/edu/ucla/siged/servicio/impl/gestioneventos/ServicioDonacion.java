package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoDonacionI;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioDonacionI;

@Service("servicioDonacion")
public class ServicioDonacion implements ServicioDonacionI {

	@Autowired
	private DaoDonacionI donacionDao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void Guardar(Donacion d) {
		donacionDao.save(d);

	}

	public void Eliminar(Integer id) {
		donacionDao.delete(id);

	}

	public List<Donacion> ListarDonaciones(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		
		List<Donacion> donaciones = donacionDao.findAll(pagina).getContent();
		for(Donacion donacion:donaciones){
			donacion.getRecursosDonados().size();
		}
		return donaciones;
	}
	
	//Donaciones que no viene de un evento
		public List<Donacion> ListarDonacionesNE(int numeroPagina) {
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("from Donacion where tipo in (0) order by fecha desc");
			query.setMaxResults(TAMANO_PAGINA);
			query.setFirstResult(numeroPagina * TAMANO_PAGINA);
			List<Donacion> donaciones =query.list();
			for(Donacion donacion:donaciones){
				donacion.getRecursosDonados().size();
			}
			session.close();
			return donaciones;
		}
	
	public List<Donacion> buscarFiltrado(String hql,int numeroPagina){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Donacion where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Donacion> donaciones =query.list();
		for(Donacion donacion:donaciones){
			donacion.getRecursosDonados().size();
		}
		session.close();
		return donaciones;
	}
	
	public long totalDonaciones(){
		return donacionDao.count();
	}
	
	public long totalDonacionesFiltrados(String hql){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Donacion where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}

	public void EliminarVarios(List<Donacion> donaciones) {
		donacionDao.delete(donaciones);
	}

}
