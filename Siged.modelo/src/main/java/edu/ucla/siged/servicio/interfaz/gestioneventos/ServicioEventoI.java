package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestioneventos.Evento;

public interface ServicioEventoI {
	
	public static int TAMANIO_PAGINA = 5;
	public void Guardar(Evento evento);
	public void Actualizar(Evento evento);
	public void Eliminar(Evento evento);
	public void EliminarVarios(List<Evento> eventos);
	public List<Evento> ListarEventos();
	public long totalEventos();
	public long totalEventosFiltrados(String hql);
	public List<Evento> buscarFiltrado(String hql,int numeroPagina);
	public Page<Evento>  buscarEventosFuturos(int numeroPagina);
	public int obtenerCantidadEventosFuturos();
	public int obtenerTamanioPagina();
}