package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;

public interface ServicioEquipoHorarioI {
	
	
	public List<HorarioPractica> getHorariosEquipo(Equipo equipo);
	public void guardarListaHorarioEquipo(List<HorarioPractica> horarioPractica);
	public void eliminar(HorarioPractica horarioPractica);
	
	
}