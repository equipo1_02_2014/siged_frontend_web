package edu.ucla.siged.servicio.impl.gestionatletas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoHistoriaAtletaI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAtletaI;

@Service
public class ServicioHistoriaAtleta implements ServicioHistoriaAtletaI {

	private @Autowired DaoHistoriaAtletaI daoHistoriaAtleta;
	
	public HistoriaAtleta buscarUltimaHistoriaAtleta(Atleta atleta){
		return daoHistoriaAtleta.buscarHistoriaAtletaEstatus1(atleta.getId());
	}
}
