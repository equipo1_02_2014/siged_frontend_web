package edu.ucla.siged.servicio.impl.gestiondeportiva;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoTipoCompetenciaI;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioTipoCompetenciaI;

@Service("servicioTipoCompetencia")
public class ServicioTipoCompetencia implements ServicioTipoCompetenciaI{
	@Autowired
	private DaoTipoCompetenciaI daoTipoCompetencia;
	@Autowired
	private SessionFactory sessionFactory;
	
	public void guardar(TipoCompetencia tipoCompetencia) {
		if (tipoCompetencia.getId()==null){
			daoTipoCompetencia.save(tipoCompetencia);
		}
		else{
			tipoCompetencia.setId(tipoCompetencia.getId());
			daoTipoCompetencia.save(tipoCompetencia);
		}
	}
	
	public Page<TipoCompetencia> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoTipoCompetencia.findAll(pagina);
	}
	
	public TipoCompetencia buscarPorId(Integer Id) {
		return daoTipoCompetencia.findOne(Id);
	}
	
	public List<TipoCompetencia> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from TipoCompetencia where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<TipoCompetencia> lista =query.list();
		session.close();
		return lista;
		}

	public void actualizar(TipoCompetencia tipoCompetencia) {
		tipoCompetencia.setEstatus(Short.parseShort("0"));
		daoTipoCompetencia.save(tipoCompetencia);
	}
	
	public void actualizarVarios(List<TipoCompetencia> tipoCompetencias) {
		
		if(tipoCompetencias!=null)
		{
		for (int i=0;i<tipoCompetencias.size();i++)
			tipoCompetencias.get(i).setEstatus(Short.parseShort("0"));
		    daoTipoCompetencia.save(tipoCompetencias);
		}   
	}
	
	public void eliminar(TipoCompetencia tipoCompetencia){
		daoTipoCompetencia.delete(tipoCompetencia);
	}
	
	public void eliminarVarios(List<TipoCompetencia> tipoCompetencias){
		daoTipoCompetencia.delete(tipoCompetencias);
	}
	
	public long totalTipoCompetencia() {
		return daoTipoCompetencia.count();
	}

    public long totalTipoCompetenciasFiltradas(String hql) {
		Session session = sessionFactory.openSession();
		Query query=session.createQuery("select count(*) from TipoCompetencia where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}

}