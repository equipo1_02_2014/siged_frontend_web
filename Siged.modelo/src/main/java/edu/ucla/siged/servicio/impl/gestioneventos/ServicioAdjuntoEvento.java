package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoAdjuntoEventoI;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioAdjuntoEventoI;

@Service("servicioAdjuntoEvento")
public class ServicioAdjuntoEvento implements ServicioAdjuntoEventoI {

	@Autowired
	private DaoAdjuntoEventoI daoAdjuntoEventoI;

	public void Guardar(AdjuntoEvento adjuntoEvento) {
		daoAdjuntoEventoI.save(adjuntoEvento);
	}

	public void Actualizar(AdjuntoEvento adjuntoEvento) {
		daoAdjuntoEventoI.save(adjuntoEvento);
	}

	public void Eliminar(Integer id) {
		daoAdjuntoEventoI.delete(id);
	}

	public void EliminarVarios(Set<AdjuntoEvento> adjuntosEvento) {
		daoAdjuntoEventoI.delete(adjuntosEvento);
	}
	
	public List<AdjuntoEvento> ListarAdjuntosEvento() {
		List<AdjuntoEvento> adjuntosEvento = daoAdjuntoEventoI.findAll();
		return adjuntosEvento;
	}

}
