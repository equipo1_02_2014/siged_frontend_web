/** 
 * 	ServicioArbitroI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;

public interface ServicioArbitroI {
	public static final int TAMANO_PAGINA = 5;
	public void guardar(Arbitro arbitro);
	public Page<Arbitro> buscarTodos(int numeroPagina);
	public Arbitro buscarPorId(Integer Id);
	public List<Arbitro> buscarFiltrado(String hql,int numeroPagina);
	public void  eliminar(Arbitro arbitro);
	public int eliminarVarios(List<Arbitro> arbitros);
	public long totalArbitros();
	public long totalArbitrosFiltrados(String hql);	
	public boolean existeArbitro(String cedula, Integer idArbitro);
	public boolean existeEnJuego(Arbitro arbitro);
}