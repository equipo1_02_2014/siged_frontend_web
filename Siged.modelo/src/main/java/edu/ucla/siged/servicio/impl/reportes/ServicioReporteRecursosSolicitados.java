package edu.ucla.siged.servicio.impl.reportes;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoQuery;
import edu.ucla.siged.domain.reporte.RecursosSolicitados;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteRecursosSolicitadosI;

@Service("servicioReporteRecursosSolicitados")

public class ServicioReporteRecursosSolicitados implements ServicioReporteRecursosSolicitadosI {
@Autowired
private DaoQuery daoRecursosSolicitados;
	
	public List<RecursosSolicitados> buscarRecursosSolicitados(String restricciones) {
		
		//System.out.println("---------------------------------------" + System.lineSeparator() + sql);
		List consultaResultado= daoRecursosSolicitados.ejecutarQueryCantidadRecursos(restricciones);
		List<RecursosSolicitados> lista= new ArrayList<RecursosSolicitados>();
		RecursosSolicitados objRecursosSolicitados;		
		
		for (Object resultado:consultaResultado){
			
			Object[] registro= (Object[]) resultado;
			
			objRecursosSolicitados= new RecursosSolicitados();
			
			objRecursosSolicitados.setNombreRecurso(registro[0].toString());
			
			objRecursosSolicitados.setCantidad((Integer)registro[1]);
			
			objRecursosSolicitados.setTotal((Integer)registro[2]);
			
			
		double porcentaje= (( objRecursosSolicitados.getCantidad()*100)/(objRecursosSolicitados.getTotal()*1.0));
			//System.out.println("-----------------"+porcentaje+"------------------------------");
			// double rep = Math.round(porcentaje*100)/100;
			 //double port=Math.round(( objRecursosSolicitados.getCantidad()*100)/(objRecursosSolicitados.getTotal())*100.00)/100;
			DecimalFormat df = new DecimalFormat("#.##");
			String porcentajeAux = df.format(porcentaje);
			double porcentajeFormateado;
			try {
				porcentajeFormateado = df.parse(porcentajeAux).doubleValue();

			
		objRecursosSolicitados.setPorcentaje(porcentajeFormateado) ;
			
			lista.add(objRecursosSolicitados);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		for(RecursosSolicitados recurso:lista){
			System.out.println("Recurso: " + recurso.getNombreRecurso()  + " Cantidad:" + recurso.getCantidad() + "Total:" + recurso.getTotal());
		}
		System.out.println("-----------------------------------------------");
		
		//Collections.sort(lista);
		
		//Collections.reverse(lista);
		
		return lista;	 
	}

	


	

}
