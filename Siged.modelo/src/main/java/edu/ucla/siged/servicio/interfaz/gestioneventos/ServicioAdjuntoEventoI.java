package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;
import java.util.Set;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;

public interface ServicioAdjuntoEventoI {

	public void Guardar(AdjuntoEvento adjuntoEvento);
	public void Actualizar(AdjuntoEvento adjuntoEvento);
	public void Eliminar(Integer id);
	public void EliminarVarios(Set<AdjuntoEvento> adjuntosEvento);
	public List<AdjuntoEvento> ListarAdjuntosEvento();
}