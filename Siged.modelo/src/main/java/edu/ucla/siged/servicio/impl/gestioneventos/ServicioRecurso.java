/** 
 * 	ServicioRecurso
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoRecursoI;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioRecursoI;

//Declaración de la clase ServicioRecurso con sus atributos:
@Service("servicioRecurso")
public class ServicioRecurso implements ServicioRecursoI{
	@Autowired
	private DaoRecursoI daoRecurso;
	@Autowired
	private SessionFactory sessionFactory;
	
	//Implementación del método para guardar un registro:
	public void guardar(Recurso recurso) {
		
		if (recurso.getId()==null){
			daoRecurso.save(recurso);
		}
		
		else{
			recurso.setId(recurso.getId());
			daoRecurso.save(recurso);
		    }
	}
	
	//Implementación del método para buscar todos los registros:
	public Page<Recurso> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoRecurso.findAll(pagina);
	}
	
	//Implementación del método para buscar un registro por Id:
	public Recurso buscarPorId(Integer Id) {
		return daoRecurso.findOne(Id);
	}
	
	//Implementación del método para buscar los registros por filtrado y crear una lista:
	public List<Recurso> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Recurso where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Recurso> lista =query.list();
		session.close();
		return lista;
		}

	//Implementación del método para eliminar un registro lógicamente:
	public void actualizar(Recurso recurso) {
		recurso.setEstatus(Short.parseShort("0"));
		daoRecurso.save(recurso);
	}
	
	//Implementación del método para eliminar varios registros lógicamente:
	public void actualizarVarios(List<Recurso> recursos) {
		for (int i=0;i<recursos.size();i++)
			recursos.get(i).setEstatus(Short.parseShort("0"));
		daoRecurso.save(recursos);
	}
	
	//Implementación del método para eliminar un registro físicamente:
	public void eliminar(Recurso recurso){
		daoRecurso.delete(recurso);
	}
	
	//Implementación del método para eliminar varios registros físicamente:
	public void eliminarVarios(List<Recurso> recursos){
		daoRecurso.delete(recursos);
	}
	
	//Implementación del método para retornar la cantidad de registros totales:
	public long totalRecursos() {
		return daoRecurso.count();
	}
	
	//Implementación del método para retornar la cantidad de registros totales filtrados:
	public long totalRecursosFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Recurso where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
	
	//Implementación del método para retornar una lista con todos los registros existentes:
	public List<Recurso> ListarRecursos(){
		return daoRecurso.findAll();
	}
}