/** 
 * 	ServicioRango
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.List;
import edu.ucla.siged.domain.gestionequipo.Rango;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoRangoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;

@Service("servicioRango")
public class ServicioRango implements ServicioRangoI{
	@Autowired
	private DaoRangoI daoRango;
	@Autowired
	private SessionFactory sessionFactory;

	public void guardar(Rango rango) {
		if (rango.getId()==null){
			daoRango.save(rango);
		}
		else{
			rango.setId(rango.getId());
			daoRango.save(rango);
		    }
	}
	
	public Page<Rango> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoRango.findAll(pagina);
	}
		
	public Rango buscarPorId(Integer Id) {
		return daoRango.findOne(Id);
	}

	public List<Rango> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Rango where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Rango> lista = query.list();
		session.close();
		return lista;
		}
	
	public void actualizar(Rango rango) {
		rango.setEstatus(Short.parseShort("0"));
		daoRango.save(rango);
	}

	public void actualizarVarios(List<Rango> rangos) {
		for (int i=0;i<rangos.size();i++)
			rangos.get(i).setEstatus(Short.parseShort("0"));
		daoRango.save(rangos);
	}

	public void eliminar(Rango rango){
		daoRango.delete(rango);
	}
	
	public void eliminarVarios(List<Rango> rangos){
		daoRango.delete(rangos);
	}
	
	public long totalRangos() {
		return daoRango.count();
	}

	public List<Rango> buscarTodos() {
		return daoRango.findAll();
	}
	
	public long totalRangosFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Rango where "+hql);
		long total = (Long) query.uniqueResult();
		session.close();
		return total;
	}

}