package edu.ucla.siged.servicio.impl.gestiondeportiva;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;
import edu.ucla.siged.domain.gestiondeportiva.AsistenciaAtleta;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoActividadI;
import edu.ucla.siged.dao.DaoActividadPracticaI;
import edu.ucla.siged.dao.DaoAreaI;
import edu.ucla.siged.dao.DaoAsistenciaAtletaI;
import edu.ucla.siged.dao.DaoEquipoTecnicoI;
import edu.ucla.siged.dao.DaoFiltro;
import edu.ucla.siged.dao.DaoHorarioPracticaI;
import edu.ucla.siged.dao.DaoPracticaI;
import edu.ucla.siged.dao.DaoTecnicoI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioCausaI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioHorarioAreaI;

@Service ("servicioPractica")
public class ServicioPractica implements ServicioPracticaI {
	@Autowired
	DaoPracticaI daoPractica;
	@Autowired
	ServicioEquipoI servicioEquipo;
	@Autowired
	DaoHorarioPracticaI daoHorarioPractica; //se debe sustituir por servicio
	@Autowired
	DaoAreaI daoArea; //se debe sustituir por servicio
	@Autowired
	DaoActividadI daoActividad; //se debe sustituir por servicio
	@Autowired
	DaoEquipoTecnicoI daoEquipoTecnico; //se debe sustituir por servicio
	@Autowired
	DaoActividadPracticaI daoActividadPractica; //se debe sustituir por servicio
	@Autowired
	DaoTecnicoI daoTecnico; //se debe sustituir por servicio
	@Autowired
	ServicioAtletaEquipoI servicioAtletaEquipo;
	@Autowired
	ServicioJuegoI servicioJuego;
	@Autowired
	ServicioHorarioAreaI servicioHorarioArea;
	@Autowired
	ServicioCausaI servicioCausa;
	@Autowired
	DaoAsistenciaAtletaI daoAsistenciaAtleta; //se debe sustituir por servicio
	@Autowired
	DaoFiltro<Practica> daoFiltroPractica;
	
	@Transactional
	public void guardar(Practica practica, int tipoOperacion) {
		if (tipoOperacion==1){
		   List<AtletaEquipo> listaAtletaEquipo = servicioAtletaEquipo.obtenerListaAtletaEquipo(practica.getEquipo());
		   for(int i=0;i<listaAtletaEquipo.size();i++){
		      practica.getAsistenciaAtletas().add(new AsistenciaAtleta(true,listaAtletaEquipo.get(i).getAtleta(),practica));
		   }
		}else if (tipoOperacion==3){
			practica.setEstatus((short) 1);
		}
		daoPractica.save(practica);
		
	}

	public Page<Practica> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.DESC,"fecha");
		return daoPractica.findAll(pagina);
	}

	public Practica buscarPorId(Integer Id) {
		Practica laPractica = daoPractica.findOne(Id);
		laPractica.getActividadesPractica().size();
		return laPractica;
	}
	
	public Practica buscarPorFechaYEquipo(Date fecha, Equipo equipo) {
		return daoPractica.findByFechaAndEquipo(fecha, equipo);
	}

	public List<Practica> buscarFiltrado(String hql, int numeroPagina) {
		hql="from Practica where"+hql+" order by fecha desc";
		return daoFiltroPractica.buscarFiltrados(hql, TAMANO_PAGINA, numeroPagina);
	}

	public void eliminar(Practica practica) {
		daoPractica.delete(practica);
		
	}

	public void eliminarVarios(List<Practica> practicas) {
		daoPractica.delete(practicas);
		
	}

	public long totalPracticas() {
		// TODO Auto-generated method stub
		return daoPractica.count();
	}

	public long totalPracticasFiltradas(String hql) {
		hql="from Practica where "+hql;
		return daoFiltroPractica.totalFiltrados(hql);
	}

	public List<Equipo> buscarTodosEquipo() {
		// debe listar los actuales y activos
		return servicioEquipo.buscarTodosTrue();
	}
	
	private static int diaDeLaSemana(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);		
	}

	public HorarioPractica obtenerHorarioPracticaEquipo(Equipo equipo, Date fecha) {
		// dado un equipo y un dia, traer el horario de practica de ese equipo en ese dia.
		return daoHorarioPractica.findByEquipoAndDia(equipo, diaDeLaSemana(fecha));
	}

	public List<Area> buscarTodosArea() {
		// TODO Auto-generated method stub
		return daoArea.findAll();
	}

	public List<Actividad> buscarTodosActividad() {
		// TODO Auto-generated method stub
		return daoActividad.findAll();
	}

	public List<EquipoTecnico> buscarEquipoTecnico(Equipo equipo) {
		// hay que pedirlo ordenado por responsabilidad
		return daoEquipoTecnico.findByEquipoAndEstatus(equipo, (short) 1);
	}
	
	public boolean isEquipoJuegoFechaHora(Equipo equipo, Date fecha, Date hora) {
		
		return servicioJuego.isEquipoJuegoFechaHora(equipo, fecha, hora);
	}

	public boolean isAreaOcupadaJuegoFechaHora(Area area, Date fecha, Date hora) {
		
		return servicioJuego.isAreaOcupadaFechaHora(area, fecha, hora);
	}
	
	public List<Causa> buscarCausasInasistencia() {
		// TODO Auto-generated method stub
		return servicioCausa.buscarTodas(1);
	}

	public List<Tecnico> buscarTecnicos() {
		return daoTecnico.findAll();
	}
	
	public List<ActividadPractica> traerActividadesAnteriores(Date fecha, Practica practicaActual) {
		Practica practica = daoPractica.findByFechaAndEquipo(fecha, practicaActual.getEquipo());
		List<ActividadPractica> actividadesPractica = new ArrayList<ActividadPractica>(practica.getActividadesPractica());
		for(int i = 0; i< actividadesPractica.size();i++){
			actividadesPractica.get(i).setPractica(practicaActual);
			actividadesPractica.get(i).setId(null);
		}
		
		return actividadesPractica;
	}

	
	//metodos de Jesus

   	
	public boolean isEquipoPracticaFechaHora(Equipo equipo, Date fecha,
			Date hora) {
		
		boolean existe=false;
		
		List<Practica> practicas= daoPractica.buscarPracticasPorEquipoTiempo(equipo.getId(), fecha, hora);
		
		if (practicas!=null && !practicas.isEmpty()){
			existe=true;
		}
		
		return existe;
	}

	public boolean isAreaOcupadaFechaHora(Area area, Date fecha, Date hora) {
		
		boolean existe=false;
		
		List<Practica> practicas= daoPractica.buscarPracticasPorAreaTiempo(area.getId(), fecha, hora);
		
		if (practicas!=null && !practicas.isEmpty()){
			existe=true;
		}
		
		return existe;
	}
	
	public boolean isEquipoPracticaFechaHora(Equipo equipo, Date fecha,Date hora, Integer idPractica) {
		
		boolean existe=false;
		
		if (idPractica == null){
			idPractica = 0;
		}
		
		List<Practica> practicas= daoPractica.buscarPracticasPorEquipoTiempo(equipo.getId(), fecha, hora, idPractica);
		
		if (practicas!=null && !practicas.isEmpty()){
			existe=true;
		}
		
		return existe;
	}

	public boolean isAreaOcupadaFechaHora(Area area, Date fecha, Date hora, Integer idPractica) {
		
		boolean existe=false;
		if (idPractica == null){
			idPractica = 0;
		}
		List<Practica> practicas= daoPractica.buscarPracticasPorAreaTiempo(area.getId(), fecha, hora, idPractica);
		
		if (practicas!=null && !practicas.isEmpty()){
			existe=true;
		}
		
		return existe;
	}

	public boolean isHorarioAreaDisponible(Area area, int dia, Date horaInicio,Date horaFin) {
		//0:ocupada, 1:desocupada
		return servicioHorarioArea.isHorarioDisponible(area, dia, horaInicio, horaFin);
	}

	public boolean existePracticaPorEquipo(Equipo equipo){
		List<Practica> lista= daoPractica.buscarPracticaPorEquipo(equipo.getId());
		return !lista.isEmpty();
	}
	
	
}

