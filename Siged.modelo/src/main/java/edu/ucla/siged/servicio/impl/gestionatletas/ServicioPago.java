package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoPagoI;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPagoI;

@Service
@Transactional
public class ServicioPago implements ServicioPagoI {

	private @Autowired DaoPagoI daoPagoI;
	public List<Pago> obtenerListaPago(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.DESC, "numRecibo","fecha");
		List<Pago> pagos = daoPagoI.findAll(pagina).getContent();
		return pagos;
	}
	public long totalPago(){
		return daoPagoI.count();
	}
	
	public List<Pago> obtenerListaPago(int numeroPagina,Date fechaPago, String numeroRecibo, String nombreAtleta, String apellidoAtleta){
		List<Pago> pagos = new ArrayList<Pago>();
		if (fechaPago != null){
			Pageable pagina = new PageRequest(numeroPagina, TAMANO_PAGINA);
			pagos = daoPagoI.filtroPagoFecha(numeroRecibo, fechaPago, nombreAtleta, apellidoAtleta, pagina).getContent();
		}else if (numeroRecibo != "" || nombreAtleta!="" || apellidoAtleta!=""){
			Pageable pagina = new PageRequest(numeroPagina, TAMANO_PAGINA);
			pagos = daoPagoI.filtroPago(numeroRecibo, nombreAtleta, apellidoAtleta, pagina).getContent();
		}else{
			PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.DESC, "numRecibo");
			pagos = daoPagoI.findAll(pagina).getContent();
		}
			
		
		return pagos;
	}
	public long totalpago(int numeroPagina,Date fechaPago, String numeroRecibo, String nombreAtleta, String apellidoAtleta){
		long total;
		if (fechaPago != null){
			total = daoPagoI.filtroPagoFechaCantidad(numeroRecibo, fechaPago, nombreAtleta, apellidoAtleta);
		}else if (numeroRecibo != "" || nombreAtleta!="" || apellidoAtleta!=""){
			total = daoPagoI.filtroPagoCantidad(numeroRecibo, nombreAtleta, apellidoAtleta);
		}else{
			total = daoPagoI.count();
		}
		return total;
	}
	public void eliminarPago(Pago pago) {
		daoPagoI.delete(pago);
	}
	public void eliminarPagos(List<Pago> pagos) {
		// TODO Auto-generated method stub
		daoPagoI.delete(pagos);
	}
	
	public void guardarPago(Pago pago){
		daoPagoI.save(pago);
	}
	public boolean buscarPago(String numRecibo) {
		// TODO Auto-generated method stub
		boolean bool = false;
		if(daoPagoI.findByNumRecibo(numRecibo)!=null){
			bool = true;
		}
		return bool;
	}
	public boolean buscarPagoPorMesYAnno(int id, Integer mes, Long anno) {
		// TODO Auto-generated method stub
		if(daoPagoI.buscarPagoPorMesYAnno(id, mes, anno)!=0){
			return true;
		}else{
			return false;
		}
		
	}
	
	
}
