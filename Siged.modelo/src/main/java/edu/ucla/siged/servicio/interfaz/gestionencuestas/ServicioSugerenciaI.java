package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;

import edu.ucla.siged.domain.gestioneventos.Sugerencia;

public interface ServicioSugerenciaI {
	public static final int TAMANO_PAGINA = 5;
	public void guardar(Sugerencia sugerencia);
	public void  eliminar(Sugerencia sugerencia);
	public List<Sugerencia> listarSugerencia();
	public List<Sugerencia> buscarTodos();
	List<Sugerencia> buscarSugerenciaPendiente(int nroPagina);
	public long totalSugerencias();
	public long totalSugerenciasPendientes();

}