package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;

public interface ServicioEquipoTecnicoI {
	
	public static final int TAMANIO_PAGINA = 9;
	
	public void guardarEquipo( Equipo equipo );
	public List<Equipo> buscarTodos();
	public void eliminarEquipo( Equipo equipo );
	public void eliminarEquipos( List<Equipo> equipos );
	public List<Atleta> obtenerAtletasPorEquipo(Equipo equipo);
	
	public Page<EquipoTecnico>buscarEquipoTecnicosTrue(int numeroPagina);
	public Tecnico obtenerTecnicoPrincipal(Integer id);
	public Tecnico obtenerTecnicoAsistente(Integer id);
	public void guardarEquipoTecnico(EquipoTecnico equipoTecnico);
	public List<Tecnico> buscarTecnicosDisponibles();	
	public void revocarTecnicosEquipoCerrado(Equipo equipo);
//	public EquipoTecnico buscarTecnicoEquipo(Equipo equipo);
}
