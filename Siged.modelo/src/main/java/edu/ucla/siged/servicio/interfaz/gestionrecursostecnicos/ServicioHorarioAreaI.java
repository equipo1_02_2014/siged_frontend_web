/** 
 * 	ServicioHorarioAreaI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos;

import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;

public interface ServicioHorarioAreaI {
	
	public static final int TAMANO_PAGINA = 5;
	public void guardar(HorarioArea hrstarea);
	public Page<HorarioArea> buscarTodos(int numeroPagina);
	public HorarioArea buscarPorId(Integer Id);
	public void  eliminar(HorarioArea hrstarea);
	public void eliminarVarios(List<HorarioArea> HorarioArea);
	public long totalHorarioArea();
	public boolean isHorarioDisponible(Area area, int dia, Date horaInicio, Date horaFin);
	public List<HorarioArea> buscarHorarioAreaOcupadas(Area area,Date dia,Date horaInicial,Date horaFinal);
}