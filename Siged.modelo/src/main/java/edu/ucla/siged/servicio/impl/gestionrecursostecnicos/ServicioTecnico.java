package edu.ucla.siged.servicio.impl.gestionrecursostecnicos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoCambioTecnicoI;
import edu.ucla.siged.dao.DaoEquipoTecnicoI;
import edu.ucla.siged.dao.DaoEstadoCivilI;
import edu.ucla.siged.dao.DaoTecnicoI;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.utilidades.EstadoCivil;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;

@Service("servicioTecnico")
public class ServicioTecnico implements ServicioTecnicoI {

	@Autowired
	private DaoTecnicoI daoTecnico;
	@Autowired
	private DaoEstadoCivilI daoEstadoCivil;
	@Autowired
	private DaoEquipoTecnicoI daoEquipoTecnico;
	@Autowired
	private DaoCambioTecnicoI daoCambioTecnico;
	@Autowired
	private SessionFactory sessionFactory;

	public void guardar(Tecnico tecnico) {
		if (tecnico.getId() == null) {
			daoTecnico.save(tecnico);
		} else {
			tecnico.setId(tecnico.getId());
			daoTecnico.save(tecnico);
		}
	}

	public Page<Tecnico> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA,
				Sort.Direction.ASC, "id");
		return daoTecnico.findAll(pagina);
	}

	public Tecnico buscarPorId(Integer Id) {
		return daoTecnico.findOne(Id);
	}

	public List<Tecnico> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Tecnico where " + hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Tecnico> lista = query.list();
		session.close();
		return lista;
	}

	@Transactional
	public void eliminar(Tecnico tecnico) {
		daoTecnico.delete(tecnico);
	}

	@Transactional
	public void eliminarVarios(List<Tecnico> tecnicos) {
		for (int i = 0; i < tecnicos.size(); i++)
			daoTecnico.delete(tecnicos);
	}

	public boolean verificarEliminar(Tecnico tecnico) {
		boolean ocupado;
		if (daoEquipoTecnico.buscarTecnicoETrue(tecnico.getId()).isEmpty()) {
			if (daoCambioTecnico.buscarTecnicoCTrue(tecnico.getId()).isEmpty()) {
				return ocupado = false;
			} else {
				return ocupado = true;
			}
		} else {
			return ocupado = true;
		}
	}

	public long totalTecnicos() {
		return daoTecnico.count();
	}

	public long totalTecnicosFiltrados(String hql) {
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("select count(*) from Tecnico where "
				+ hql);
		long total = (Long) query.uniqueResult();
		session.close();
		return total;
	}

	public List<EstadoCivil> buscarEstadoCivilTecnico() {
		return daoEstadoCivil.findAll();
	}

	public Tecnico buscarTecnicoPorCedula(String cedula) {
		return daoTecnico.findByCedula(cedula);
	}
	
	//Agregado por Jesus
	public Tecnico buscarPorCedula(String cedula) {
		return daoTecnico.findByCedula(cedula);
	}

	public List<Tecnico> buscarTodos() {
		// TODO Auto-generated method stub
		return daoTecnico.findAll();
	}

}