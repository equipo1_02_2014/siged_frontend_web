package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoEventoI;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioEventoI;

@Service("servicioEvento")
public class ServicioEvento implements ServicioEventoI{

	@Autowired
	private DaoEventoI daoEventoI;
	@Autowired
	private SessionFactory sessionFactory;
	
	public ServicioEvento() {}
	
	public void Guardar(Evento evento) {
		if(evento.getId()==null)
			daoEventoI.save(evento);
		else{
			evento.setId(evento.getId());
			daoEventoI.save(evento);
		}
			
	}

	public void Actualizar(Evento evento) {
		daoEventoI.save(evento);
	}

	public void Eliminar(Evento evento) {
		daoEventoI.delete(evento);
	}
	
	public void EliminarVarios(List<Evento> eventos){
		daoEventoI.delete(eventos);
	}

	public List<Evento> ListarEventos() {
		List<Evento> listaEventos = daoEventoI.findAll();
		for(Evento evento :listaEventos) {
			evento.getAdjuntoEventos().size();
		}
		return listaEventos;
	}
	
	public List<Evento> buscarTodos(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA, Sort.Direction.DESC,"fecha");
		List<Evento> listaEventos = daoEventoI.findAll(pagina).getContent();
		for(Evento evento : listaEventos){
			evento.getAdjuntoEventos().size();
		}
		return listaEventos;
	}
	public List<Evento> buscarTodosProximos(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA, Sort.Direction.DESC,"fecha");
		List<Evento> listaEventos = daoEventoI.buscarEventosProximos(pagina).getContent();
		for(Evento evento : listaEventos){
			evento.getAdjuntoEventos().size();
		}
		return listaEventos;
	}
	public long totalEventosProximos(){
		return daoEventoI.buscarTotalEventosProximos();
	}
	
	public long totalEventos(){
		return daoEventoI.count();
	}
	
	public long totalEventosFiltrados(String hql){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Evento where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
	
	public List<Evento> buscarFiltrado(String hql,int numeroPagina){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Evento where "+hql);
		query.setMaxResults(TAMANIO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANIO_PAGINA);
		List<Evento> lista = query.list();
		for(Evento evento : lista){
			evento.getAdjuntoEventos().size();
		}
		session.close();
		return lista;
	}
	
	public int obtenerTamanioPagina() {
		return ServicioEventoI.TAMANIO_PAGINA;
	}
	
	public Page<Evento> buscarEventosFuturos(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina,this.obtenerTamanioPagina(), Sort.Direction.ASC,"fecha");
		return daoEventoI.buscarEventosFuturos(pagina);
	}
	
	public int obtenerCantidadEventosFuturos(){
		List<Evento> eventos = daoEventoI.buscarEventosFuturos();
		return eventos.size();
	}
	
	//********************************************************
		//      inicio para las donaciones por eventos
		//********************************************************
		public List<Evento> EventosRealizadosSinDonacion(int numeroPagina, int tamano_pg){
			Session session = sessionFactory.openSession(); 
			Query query=session.createSQLQuery("select E.id from evento E left join donacion D on E.id=D.idevento where  D.idevento is null and E.fecha < current_date ORDER BY E.fecha, E.hora DESC");
			query.setMaxResults(tamano_pg);
			query.setFirstResult(numeroPagina * tamano_pg);
			List results = query.list();
			List<Evento> lista = new ArrayList<Evento>();
			for (Iterator iter = results.iterator(); iter.hasNext();) {
				Integer id = (Integer) iter.next();
				lista.add(daoEventoI.findById(id));
			}
			session.close();
			return lista;
		}
		
		public List<Evento> buscarEventosRealizadosSinDonacion(String hql, int numeroPagina, int tamano_pg){
			Session session = sessionFactory.openSession(); 
			Query query=session.createSQLQuery("select E.id from evento E left join donacion D on E.id=D.idevento where  D.idevento is null and E.fecha < current_date and "+hql+" ORDER BY E.fecha, E.hora DESC");
			query.setMaxResults(tamano_pg);
			query.setFirstResult(numeroPagina * tamano_pg);
			List results = query.list();
			List<Evento> lista = new ArrayList<Evento>();
			for (Iterator iter = results.iterator(); iter.hasNext();) {
				Integer id = (Integer) iter.next();
				lista.add(daoEventoI.findById(id));
			}
			session.close();
			return lista;
		}
		
		public long TotalEventosRealizadosSinDonacion(){
			Session session = sessionFactory.openSession(); 
			List results=session.createSQLQuery("select E.id from evento E left join donacion D on E.id=D.idevento where  D.idevento is null and E.fecha < current_date").list();
			List<Evento> lista = new ArrayList<Evento>();
			for (Iterator iter = results.iterator(); iter.hasNext();) {
				Integer id = (Integer) iter.next();
				lista.add(daoEventoI.findById(id));
			}
			session.close();
			return lista.size();
		}
		
		public long totalEventosRealizadosSinDonacion(String hql){
			Session session = sessionFactory.openSession(); 
			List results=session.createSQLQuery("select E.id from evento E left join donacion D on E.id=D.idevento where  D.idevento is null and E.fecha < current_date and "+hql).list();
			List<Evento> lista = new ArrayList<Evento>();
			for (Iterator iter = results.iterator(); iter.hasNext();) {
				Integer id = (Integer) iter.next();
				lista.add(daoEventoI.findById(id));
			}
			session.close();
			return lista.size();
		}
		//********************************************************
		//      fin para las donaciones por eventos
		//********************************************************
		
	
}
