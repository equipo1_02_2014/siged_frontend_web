/** 
 * 	ServicioTipoArea
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.impl.gestionrecursostecnicos;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoTipoAreaI;
import edu.ucla.siged.domain.gestionrecursostecnicios.TipoArea;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTipoAreaI;

@Service("servicioTipoArea")
public class ServicioTipoArea implements ServicioTipoAreaI {
	
	private @Autowired DaoTipoAreaI  daoTipoArea;

	public List<TipoArea> obtenerListaTipoArea() {
		return daoTipoArea.findAll();
	}
}