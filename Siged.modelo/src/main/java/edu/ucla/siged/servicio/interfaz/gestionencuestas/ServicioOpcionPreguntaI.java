package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;

public interface ServicioOpcionPreguntaI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(OpcionPregunta opcionPregunta);
	public Page<OpcionPregunta> buscarTodos(int numeroPagina);
	public OpcionPregunta buscarPorId(Integer Id);
	public List<OpcionPregunta> buscarFiltrado(String hql,int numeroPagina);
	public long totalOpcionesPreguntas();
	public long totalOpcionesPreguntasFiltradas(String hql);
	public List<OpcionPregunta> buscarOpciones(Pregunta pregunta);
	public void eliminarVarios(List<OpcionPregunta> listaOpcionesEliminadas);
	public void eliminar(OpcionPregunta opcionPregunta);
	
}