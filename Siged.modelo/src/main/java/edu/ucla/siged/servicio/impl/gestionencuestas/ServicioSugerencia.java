package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoSugerenciaI;
import edu.ucla.siged.domain.gestioneventos.Sugerencia;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioSugerenciaI;


@Service("servicioSugerencia")
public class ServicioSugerencia implements ServicioSugerenciaI {

	@Autowired 
	private DaoSugerenciaI daoSugerencia;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Sugerencia> listarSugerencia() { 
		List<Sugerencia> listasugerencias = daoSugerencia.findAll();
		return listasugerencias;
	}
	
	public void guardar(Sugerencia sugerencia) {
		daoSugerencia.save(sugerencia);

	}

	public void eliminar(Sugerencia sugerencia) {
		daoSugerencia.delete(sugerencia);
	}
	
	public void eliminarSugerencias(List<Sugerencia> sugerencias) {
		for(Sugerencia sugerencia:sugerencias){
			daoSugerencia.delete(sugerencia);
		}
	}

	public List<Sugerencia> buscarTodos() {
		return daoSugerencia.findAll();
	}
	
	public List<Sugerencia> buscarSugerenciaPendiente(int nroPagina){
        List<Sugerencia> listaSugerenciasPendientes = new ArrayList<Sugerencia>();
        Sort orden= new Sort(Sort.Direction.DESC,"fechaEmision");
		Pageable paginable= new PageRequest(nroPagina,ServicioSugerenciaI.TAMANO_PAGINA,orden);
		Page<Sugerencia> pagina = daoSugerencia.buscarSugerenciasPendientes(paginable);
		return pagina.getContent();
	}
	
	public long totalSugerencias(){
		return daoSugerencia.count();
	}
	
	public long totalSugerenciasPendientes(){
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("select count(*) from Sugerencia where estatusSugerencia=1");
		long totalPendientes = (Long) query.uniqueResult();
		session.close();
		return totalPendientes;
	}

}