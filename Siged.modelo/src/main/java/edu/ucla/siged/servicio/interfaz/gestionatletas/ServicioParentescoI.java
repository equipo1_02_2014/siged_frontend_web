package edu.ucla.siged.servicio.interfaz.gestionatletas;

import edu.ucla.siged.domain.gestionatleta.Parentesco;

import java.util.List;

public interface ServicioParentescoI {

	List<Parentesco> obtenerListaParentesco();
}
