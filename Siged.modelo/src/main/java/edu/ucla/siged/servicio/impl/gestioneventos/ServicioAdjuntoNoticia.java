package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoAdjuntoNoticiaI;
import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioAdjuntoNoticiaI;

@Service("servicioAdjuntoNoticia")
public class ServicioAdjuntoNoticia implements ServicioAdjuntoNoticiaI {

	@Autowired
	private DaoAdjuntoNoticiaI daoAdjuntoNoticiaI;

	public void Guardar(AdjuntoNoticia an) {
		daoAdjuntoNoticiaI.save(an);
	}

	public void Actualizar(AdjuntoNoticia an) {
		daoAdjuntoNoticiaI.save(an);
	}

	public void Eliminar(Integer id) {
		daoAdjuntoNoticiaI.delete(id);
	}

	public void EliminarVarios(Set<AdjuntoNoticia> adjuntos) {
		daoAdjuntoNoticiaI.delete(adjuntos);
	}

	public List<AdjuntoNoticia> ListarAdjuntosNoticia() {
		List<AdjuntoNoticia> adjuntos = daoAdjuntoNoticiaI.findAll();
		return adjuntos;
	}

}
