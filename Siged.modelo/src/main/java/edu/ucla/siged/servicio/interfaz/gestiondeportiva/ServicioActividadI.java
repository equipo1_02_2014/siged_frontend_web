package edu.ucla.siged.servicio.interfaz.gestiondeportiva;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestiondeportiva.Actividad;

public interface ServicioActividadI {
	
	public List<Actividad> listarActividad();
	public void guardar(Actividad actividad);
	public void eliminar(Actividad actividad);
	public void eliminarVarios(List<Actividad> actividades);
	public Page<Actividad> buscarTodos(int numeroPagina);
	public Actividad buscarPorId(Integer Id);
	public List<Actividad> buscarTodos();
	public List<Actividad> buscarActivos();
	public List<Actividad> buscarFiltrado(String hql, int numeroPagina);
	public long totalActividadesFiltrados(String hql);
	public static final int TAMANO_PAGINA = 4;
	public long totalActividades();
}