package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoEstatusAtletaI;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstatusAtletaI;

@Service
public class ServicioEstatusAtleta implements ServicioEstatusAtletaI {
	
	private @Autowired DaoEstatusAtletaI daoEstatusAtletaI; 
	
	public List<EstatusAtleta> listaEstatusAtleta(){
		return daoEstatusAtletaI.findAll();
	}
	
	public EstatusAtleta obtenerEstatus(){
		return daoEstatusAtletaI.findOne(1);
	}
	
	public EstatusAtleta buscarEstatusAtleta(Integer id){
		return daoEstatusAtletaI.findOne(id);
	}

	public EstatusAtleta obtenerEstatusSinAsignar() {
		return daoEstatusAtletaI.findOne(2);
	}

	public List<EstatusAtleta> listaEstatusAtletaNoEliminado() {
		// TODO Auto-generated method stub
		return daoEstatusAtletaI.buscarEstatusAtletaNoEliminados();
	}
	
}
