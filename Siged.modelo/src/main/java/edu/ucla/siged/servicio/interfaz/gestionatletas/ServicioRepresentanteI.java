package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.List;
import edu.ucla.siged.domain.gestionatleta.Representante;

public interface ServicioRepresentanteI {

	public static final int TAMANO_PAGINA = 9;
	public List<Representante> obtenerListaRepresentante();
	
	//Agregado por Jesus
	public Representante buscarPorCedula(String cedula);
}

