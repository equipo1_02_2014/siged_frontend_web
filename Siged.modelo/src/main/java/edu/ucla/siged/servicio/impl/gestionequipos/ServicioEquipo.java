package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoCategoriaI;
import edu.ucla.siged.dao.DaoCompetenciaI;
import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.dao.DaoFiltro;
import edu.ucla.siged.dao.DaoRangoI;
import edu.ucla.siged.dao.DaoTipoEquipoI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;

@Service("servicioEquipo")
public class ServicioEquipo implements ServicioEquipoI {
	
	@Autowired 
	private DaoEquipoI daoEquipo;
	@Autowired
	private DaoTipoEquipoI daoTipoEquipo;
	@Autowired
	private DaoRangoI daoRango;
	@Autowired
	private DaoCategoriaI daoCategoria;
	@Autowired
	private DaoCompetenciaI daoCompetencia;
	@Autowired 
	ServicioCompetenciaI servicioCompetencia;
	@Autowired 
	ServicioPracticaI servicioPractica;
	@Autowired 
	ServicioAtletaEquipoI servicioAtletaEquipoI;
	@Autowired 
	DaoFiltro<Equipo> daoFiltroEquipo;
	private SessionFactory sessionFactory;

	public void guardarEquipo( Equipo equipo ) { 
		daoEquipo.save( equipo ); 
	}

	public List<Equipo> buscarTodos() {	
		return daoEquipo.findAll(); 
	}
	
	public List<Equipo> buscarTodosTrue() {	
		return daoEquipo.findByEstatus(Short.parseShort("1")); 
	}
	
	public void eliminarEquipo( Equipo equipo ) {
		equipo.setEstatus( Short.parseShort("0"));		
		daoEquipo.save(equipo);
	}
	
	public void eliminarEquipos( List<Equipo> equipos ){
		for(Equipo equipo:equipos){
			equipo.setEstatus( Short.parseShort("0"));
		}
		daoEquipo.save( equipos );
	}
	
	public List<AtletaEquipo> atletaPorEquipo(Equipo equipo){
		return servicioAtletaEquipoI.obtenerListaAtletaEquipo( equipo );
	}
	
	public List<Atleta> obtenerAtletasPorEquipo(Equipo equipo) {
		List<AtletaEquipo> set= servicioAtletaEquipoI.obtenerListaAtletaEquipo(equipo);
		
		List<Atleta> listaAtleta= new ArrayList<Atleta>();
		
		for (AtletaEquipo aq: set){
			listaAtleta.add(aq.getAtleta());
		}		
		return listaAtleta;
	}
	
	public List<Equipo> buscarEquipos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA, Sort.Direction.DESC, "fechaCreacion" ); 
		List<Equipo> equipos = daoEquipo.findAll(pagina).getContent();
		return equipos;
	}
	
	public List<Equipo> obtenerListaEquipos(int numeroPagina, String nombre, String categoria, String rango, String tipoEquipo) {
		List<Equipo> resultado = new LinkedList<Equipo>();
		if( !nombre.equalsIgnoreCase("") || !categoria.equalsIgnoreCase("") || !rango.equalsIgnoreCase("") || !tipoEquipo.equalsIgnoreCase("") ){
			resultado = equiposFiltrados(numeroPagina, nombre, categoria, rango, tipoEquipo, Short.parseShort("1"));
		}else{
			resultado = buscarEquipos(numeroPagina);
		}
		return resultado;
	}
	
	public List<Equipo> equiposFiltrados(int numeroPagina, String nombre, String categoria, String rango, String tipoEquipo, Short estatus){
		Pageable pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA);
		return daoEquipo.filtroEquipo(nombre, categoria, rango, tipoEquipo, estatus, pagina).getContent();		
	}

	public Page<Equipo> buscarTodos(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA, Sort.Direction.DESC, "fechaCreacion" ); 
		return daoEquipo.findAll(pagina);
	}

	public Page<Equipo> buscarEquiposTrue(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA, Sort.Direction.DESC, "fechaCreacion"); 
		return daoEquipo.buscarEquiposTrue(Short.parseShort("1"), pagina);
	}
	
	public List<Equipo> buscarEquiposTrue(){
		return daoEquipo.buscarEquiposTrue();
	}
	
	public long totalEquipos(){ 
		return daoEquipo.cantidadEquiposTrue(Short.parseShort("1"));

	}
	
	public long totalEquiposFiltrados(String hql){
		hql = "from Equipo where "+hql;
		return daoFiltroEquipo.totalFiltrados(hql);
	}

	public List<Equipo> buscarEquiposRoster() {
		List<Equipo> equipos= daoEquipo.buscarEquiposRoster();
		return equipos;
	}

	public List<Equipo> buscarFiltrado(String hql, int numeroPagina) {
		hql="from Equipo where "+hql+" order by fechaCreacion DESC";				
		return daoFiltroEquipo.buscarFiltrados(hql, TAMANIO_PAGINA, numeroPagina);
	}
	
	public boolean buscarNombreEquipo(Equipo equipo){
		long nombresIguales;
		nombresIguales = daoEquipo.buscarNombre(equipo.getNombre());
		if(nombresIguales >= 1){
			return true;
		}else{
			return false;
		}
	}
	
	public List<Equipo> buscarEquiposPorTecnico(Integer idTecnico, int numeroPagina, int tamanoPagina){
		Pageable pagina = new PageRequest(numeroPagina, tamanoPagina, Sort.Direction.DESC, "fechaCreacion", "id");
		return daoEquipo.filtroEquiposPorTecnico(idTecnico, pagina).getContent();
	}
	
	public long totalEquipoPorTecnico(Integer idTecnico){
		return daoEquipo.totalEquiposPorTecnico(idTecnico);
	}
	
	public boolean verificarCerrar(Equipo equipo)
	{
		boolean ocupado;
		if (servicioCompetencia.existeCompetenciaPorEquipo(equipo) || 
			servicioPractica.existePracticaPorEquipo(equipo)) {
			  ocupado=true;
		}
		else{   
			 ocupado=false;
		}
		return ocupado;
	}
	
	public List<Equipo> buscarEquiposVigentes() {
		
		return daoEquipo.buscarEquiposVigentes();
		
	}
	
	public List<Equipo> buscarEquiposFuturosYVigentes() {
		return daoEquipo.buscarEquiposFuturosYVigentes();
	}

	public List<Equipo> buscarEquiposFuturosYVigentes(int nroPagina) {
		Sort orden= new Sort(Sort.Direction.ASC,"fechaCreacion");
		
		Pageable paginable= new PageRequest(nroPagina, TAMANIO_PAGINA, orden);
		
		Page<Equipo> pagina= daoEquipo.buscarEquiposFuturosYVigentes(paginable);
		
		return pagina.getContent();
	}
	
	public void Desactivar(Equipo equipo) {
		equipo.setEstatus(Short.parseShort("0"));
		daoEquipo.save(equipo);
	}
	
	public long totalEquiposActivos(){
		return daoEquipo.totalEquiposActivos();
	}

	public List<TipoEquipo> buscarTodosTipoEquipo(){
		return daoTipoEquipo.findAll();
	}	
	
	public List<Categoria> buscarTodasCategoria(){
		Sort orden= new Sort(Sort.Direction.ASC,"id");
		return daoCategoria.findAll(orden);
	}	
	
	public List<Rango> buscarTodosRango(){
		return daoRango.findAll();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public List<Equipo> buscarEquiposRosterConTecnico() {
		List<Equipo> equipos= daoEquipo.buscarEquiposRosterConTecnico();
		
		return equipos;
	}
	
	public List<Equipo> buscarFiltradoPorTecnico(String hql, int numeroPagina){
		hql="SELECT e " + hql + " ORDER BY e.fechaCreacion DESC";
		return daoFiltroEquipo.buscarFiltrados(hql, TAMANIO_PAGINA, numeroPagina);
	}

	public long totalEquiposFiltradosPorTecnico(String hql){ return daoFiltroEquipo.totalFiltrados(hql); }
		
	public List<Equipo> buscarFiltradoPorDelegado(String hql, int numeroPagina){
		hql="SELECT e " + hql + " ORDER BY e.fechaCreacion DESC";
		return daoFiltroEquipo.buscarFiltrados(hql, TAMANIO_PAGINA, numeroPagina);
	}
	
	public long totalEquiposFiltradosPorDelegado(String hql){ return daoFiltroEquipo.totalFiltrados(hql); }

}