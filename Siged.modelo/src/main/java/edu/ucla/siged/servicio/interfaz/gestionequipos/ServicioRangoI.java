/** 
 * 	ServicioRangoI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestionequipo.Rango;

public interface ServicioRangoI {
	public static final int TAMANO_PAGINA = 5;
	public Page<Rango> buscarTodos(int numeroPagina);
	public Rango buscarPorId(Integer Id);
	public List<Rango> buscarFiltrado(String hql,int numeroPagina);
	public void actualizar(Rango rango);
	public void actualizarVarios(List<Rango> rangos);
	public void eliminarVarios(List<Rango> rangos);
	public long totalRangos();
	public long totalRangosFiltrados(String hql);
	List<Rango> buscarTodos();
	public void guardar(Rango rango);
	public void eliminar(Rango rango);
}