/** 
 * 	ServicioArea
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.impl.gestionrecursostecnicos;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.ucla.siged.dao.DaoAreaI;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.servicio.impl.gestiondeportiva.ServicioActividadPractica;
import edu.ucla.siged.servicio.impl.gestiondeportiva.ServicioJuego;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAreaI;

@Service("servicioArea")
public class ServicioArea implements ServicioAreaI {
	
		@Autowired
		private DaoAreaI daoArea;
		@Autowired
		private ServicioJuego servicioJuego;
		@Autowired
		private ServicioActividadPractica servicioActividadPractica;
		@Autowired
		private SessionFactory sessionFactory;
		
		@Transactional
		public void guardar(Area area) {
			area.setEstatus(Short.parseShort("1"));
			if (area.getId()==null){
				area.setEstatus((short) 1);
				daoArea.save(area);
			}
			else{
				area.setId(area.getId());
				daoArea.save(area);
			}
		}

		public void eliminar(Area area) {
			daoArea.delete(area);
		}
				
		public Page<Area> buscarTodos(int numeroPagina) {
			PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
			return daoArea.findAll(pagina);
		}
		
		@Transactional
		public Area buscarPorId(Integer Id) {
			Area elArea = daoArea.findOne(Id);
			elArea.getHorariosArea().size();
			return elArea;
		}
		
		public int eliminarVarios(List<Area> area) {
			int cont=0;
			for (int i=0;i<area.size();i++){
				if ((!servicioJuego.existeAreaEnJuego(area.get(i))) &&
						(!servicioActividadPractica.existeAreaEnActividadPractica(area.get(i)))){
					daoArea.delete(area.get(i));
					cont++;
				}
			}
			return cont;
		}
	
		public long totalArea() {
			return daoArea.count();
		}
		
		public long totalAreaFiltrados(String hql) {
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("select count(*) from Area where "+hql); //daoEquipo.findAll();
			long total= (Long) query.uniqueResult(); //daoEquipo.findAll();
			session.close();
			return total;
		}

		
		public List<Area> buscarFiltrado(String hql, int numeroPagina) {
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("from Area where "+hql);
			query.setMaxResults(TAMANO_PAGINA);
			query.setFirstResult(numeroPagina * TAMANO_PAGINA);
			List<Area> lista =query.list();
			session.close();
			return lista;

		}

		public boolean buscarArea(Area area){
			boolean esDeJuego=servicioJuego.existeAreaEnJuego(area);
			boolean esDeActividad=servicioActividadPractica.existeAreaEnActividadPractica(area);
			if ((!esDeJuego) && (!esDeActividad))
				return true;
			else
				return false;
		}

		public List<Area> buscarTodos() {
			return null;
		}
}