package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoEstadoCivilI;
import edu.ucla.siged.domain.utilidades.EstadoCivil;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstadoCivilI;

@Service
public class ServicioEstadoCivil implements ServicioEstadoCivilI {

	private @Autowired DaoEstadoCivilI daoEstadoCivil;
	
	public List<EstadoCivil> obtenerListaEstadoCivil() {
		// TODO Auto-generated method stub
		return daoEstadoCivil.findAll();
	}
	
}
