package edu.ucla.siged.servicio.interfaz.reportes;
import java.util.List;

import edu.ucla.siged.domain.reporte.RecursosSolicitados;

public interface ServicioReporteRecursosSolicitadosI {
	
	public List<RecursosSolicitados> buscarRecursosSolicitados(String restricciones);
	
	

}
