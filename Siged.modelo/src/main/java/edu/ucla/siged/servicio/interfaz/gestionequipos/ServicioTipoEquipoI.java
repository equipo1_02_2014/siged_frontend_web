package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;

public interface ServicioTipoEquipoI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(TipoEquipo tipoequipo);
	public Page<TipoEquipo> buscarTodos(int numeroPagina);
	public TipoEquipo buscarPorId(Integer Id);
	public List<TipoEquipo> buscarFiltrado(String hql,int numeroPagina);
	public void eliminarTipoEquipo(TipoEquipo tipoequipo);
	public void eliminarTipoEquipos( List<TipoEquipo> tipoequipos );
	public long totalTipoEquipos();
	public long totalTipoEquiposFiltrados(String hql);
	public List<TipoEquipo> buscarTodos();

}