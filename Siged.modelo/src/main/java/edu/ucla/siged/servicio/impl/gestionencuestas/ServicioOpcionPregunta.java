package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoOpcionPreguntaI;
import edu.ucla.siged.dao.DaoRespuestaI;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioOpcionPreguntaI;

@Service("servicioOpcionPregunta")

public class ServicioOpcionPregunta implements ServicioOpcionPreguntaI{
	@Autowired
	private DaoOpcionPreguntaI daoOpcionPregunta;
	@Autowired
	private DaoRespuestaI daoRespuesta;

	@Autowired
	private SessionFactory sessionFactory;
	
	public void guardar(OpcionPregunta opcionPregunta) {
		// TODO Auto-generated method stub
		
		if (opcionPregunta.getId()==null){
			//anotador.setEstatus(true);
			daoOpcionPregunta.save(opcionPregunta);
		}
		else{
			opcionPregunta.setId(opcionPregunta.getId());
			daoOpcionPregunta.save(opcionPregunta);
		}
	}
	
	
	public Page<OpcionPregunta> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoOpcionPregunta.findAll(pagina);
	}
	
	public OpcionPregunta buscarPorId(Integer Id) {
		// TODO Auto-generated method stub
		return daoOpcionPregunta.findOne(Id);
	}
	
	public List<OpcionPregunta> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from OpcionPregunta where "+hql); //daoEquipo.findAll();
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<OpcionPregunta> lista =query.list(); //daoEquipo.findAll();
		session.close();
		return lista;

		}

	
	//public void eliminar(OpcionPregunta opcionPregunta) {
		// TODO Auto-generated method stub
		//opcionPregunta.setEstatus(Short.parseShort("0"));
		//anotador.setEstatus(null);
		//anotador.setEstatus(0);
		//daoAnotador.save(anotador);
	//}
	//public void eliminarVarios(List<Anotador> anotadores) {
		// TODO Auto-generated method stub
		//for (int i=0;i<anotadores.size();i++)
			//anotadores.get(i).setEstatus(Short.parseShort("0"));
			//anotadores.get(i).setEstatus(0);
		//daoAnotador.save(anotadores);
	//}
	
	
	public long totalOpcionesPreguntas() {
		// TODO Auto-generated method stub
		return daoOpcionPregunta.count();
	}


	public long totalOpcionesPreguntasFiltradas(String hql) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from OpcionPregunta where "+hql); //daoEquipo.findAll();
		long total= (Long) query.uniqueResult(); //daoEquipo.findAll();
		session.close();
		return total;

	}


	public List<OpcionPregunta> buscarOpciones(Pregunta pregunta) {
		
		 pregunta.getOpcionPreguntas().size();
				Object[] opcionest=pregunta.getOpcionPreguntas().toArray();
				OpcionPregunta opciont= new OpcionPregunta();
				List<OpcionPregunta> lista = new ArrayList<OpcionPregunta>();
				for(int i=0; i<opcionest.length;i++)
				{
					opciont= (OpcionPregunta) opcionest[i];
					lista.add(opciont);
				}
				return lista;
			
	}

@Transactional
	public void eliminarVarios(List<OpcionPregunta> listaOpcionesEliminadas) {
		
			for (OpcionPregunta opcionPregunta : listaOpcionesEliminadas) {
				opcionPregunta.getRespuestas().size();
				daoRespuesta.delete(opcionPregunta.getRespuestas());
				opcionPregunta.getRespuestas().size();
			}
		daoOpcionPregunta.delete(listaOpcionesEliminadas);
		
		
		
	}


public void eliminar(OpcionPregunta opcionPregunta) {
	// TODO Auto-generated method stub
	daoOpcionPregunta.delete(opcionPregunta);
	
}
	
	
}