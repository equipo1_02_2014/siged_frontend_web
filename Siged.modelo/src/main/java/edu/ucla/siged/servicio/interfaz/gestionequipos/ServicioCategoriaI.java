package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.utilidades.Persona;

public interface ServicioCategoriaI {
	public static final int TAMANO_PAGINA = 5;
	public void guardar(Categoria categoria);
	public Page<Categoria> buscarTodos(int numeroPagina);
	public Categoria buscarPorId(Integer Id);
	public List<Categoria> buscarFiltrado(String hql,int numeroPagina);
	public void eliminarCategoria(Categoria categoria);
	public void eliminarCategorias( List<Categoria> categorias );
	public long totalCategorias();
	public long totalCategoriasFiltradas(String hql);
	public List<Categoria> buscarTodos();
	//Agregado Por Jesus
	public Categoria obtenerCategoria(Persona persona);
	public List<Categoria> buscarCategoriasActivas();
}