/** 
 * 	ServicioHorarioArea
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.impl.gestionrecursostecnicos;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoHorarioAreaI;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioHorarioAreaI;

@Service("servicioHorarioArea")//Agregado por Jesus
public class ServicioHorarioArea implements ServicioHorarioAreaI{
	
	@Autowired
	private DaoHorarioAreaI daoHorarioArea;
	
	public void guardar(HorarioArea horarioArea) {		
		if (horarioArea.getId()==null){
			daoHorarioArea.save(horarioArea);
		}
		else{
			horarioArea.setId(horarioArea.getId());
			daoHorarioArea.save(horarioArea);
		}
	}
	
	public Page<HorarioArea> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoHorarioArea.findAll(pagina);
	}
	
	public HorarioArea buscarPorId(Integer Id) {
		return daoHorarioArea.findOne(Id);
	}

	public void eliminar(HorarioArea horarioArea) {
		daoHorarioArea.save(horarioArea);
	}

	public long totalArea() {
		return daoHorarioArea.count();
	}

	public void eliminarVarios(List<HorarioArea> HorarioArea) {		
	}

	public long totalHorarioArea() {
		return 0;
	}
	
	public List<HorarioArea> buscarHorarioAreaOcupadas(Area area,Date dia,Date horaInicial,Date horaFinal){
		Calendar fechaInicio= Calendar.getInstance();
		fechaInicio.setTime(dia);
		int diaSemana= fechaInicio.get(Calendar.DAY_OF_WEEK);
		List<HorarioArea> lista= daoHorarioArea.buscarHorarioAreaOcupada(area.getId(), diaSemana, horaInicial, horaFinal);
		return lista;
	
	}

	public boolean isHorarioDisponible(Area area, int dia, Date horaInicio,Date horaFin) {
		if (daoHorarioArea.areaOcupada(area, dia, horaInicio, horaFin)>0){
			return true;
		}else{
			return false;
		}
	}
}