package edu.ucla.siged.servicio.impl.seguridad;

import java.util.Date;
import java.util.List;
import java.util.Set;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoRolUsuarioI;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolUsuarioI;


@Service("servicioRolUsuario")
public class ServicioRolUsuario implements ServicioRolUsuarioI{

	@Autowired private DaoRolUsuarioI daoRolUsuario;
	@Autowired private SessionFactory sessionFactory;
	
	public List<RolUsuario> buscarTodos(Usuario usuario) {
		/*Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from RolUsuario where Usuario"=); 
		System.out.println("from Funcionalidad where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Funcionalidad> lista =query.list(); 
		session.close();
		return lista;*/
		
		
		return daoRolUsuario.findAll();
	
	}

	public List<RolUsuario> buscarTodos() {
		return daoRolUsuario.findAll();
	
	}
	
	public void eliminar(RolUsuario rolUsuarioSeleccionado) {
		// TODO Auto-generated method stub
			rolUsuarioSeleccionado.setFechaEliminacion(new Date());
			rolUsuarioSeleccionado.setEstatus((short) 0);
			daoRolUsuario.save(rolUsuarioSeleccionado);
		
		
	}
	public void eliminarVarios(Set<RolUsuario> rolesUsuario) {
		// TODO Auto-generated method stub
		Object[] listaRolesUsuario = rolesUsuario.toArray();
		List<RolUsuario> rolesEliminados =null;
		RolUsuario rolUsuarioTemp = null;
		for (int i=0;i<rolesUsuario.size();i++){
			rolUsuarioTemp=(RolUsuario) listaRolesUsuario[i];
			rolUsuarioTemp.setEstatus(Short.parseShort("0"));
			rolesEliminados.add(rolUsuarioTemp);
		}
			
			
		daoRolUsuario.save(rolesEliminados);
	}
	
	public void guardarListaRolUsuario(List<RolUsuario> roles) {
		daoRolUsuario.save(roles);
	}

	public List<RolUsuario> getRolesUsuario(Usuario usuario) {   	
    	return daoRolUsuario.findByUsuario(usuario);
	}
	public List<RolUsuario> getRolesUsuarioByRol(Rol rol) {
		return daoRolUsuario.findByRol(rol);
	}
}