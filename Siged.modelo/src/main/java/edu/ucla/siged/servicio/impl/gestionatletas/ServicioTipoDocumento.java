package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoTipoDocumentoI;
import edu.ucla.siged.domain.gestionatleta.TipoDocumento;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioTipoDocumentoI;

@Service
public class ServicioTipoDocumento implements ServicioTipoDocumentoI {

	private @Autowired DaoTipoDocumentoI daoTipoDocumento;
	
	public List<TipoDocumento> listaDocumentos() {
		// TODO Auto-generated method stub
		
		return daoTipoDocumento.findAll();
	}

	public TipoDocumento tipoDocumento(Integer id) {
		// TODO Auto-generated method stub
		return daoTipoDocumento.findOne(id);
	}
	
}
