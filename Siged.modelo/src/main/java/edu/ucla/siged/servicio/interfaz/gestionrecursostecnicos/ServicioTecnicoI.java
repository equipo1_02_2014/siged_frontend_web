package edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.utilidades.EstadoCivil;

public interface ServicioTecnicoI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(Tecnico tecnico);
	public Page<Tecnico> buscarTodos(int numeroPagina);
	public List<Tecnico> buscarTodos();
	public Tecnico buscarPorId(Integer Id);
	public List<Tecnico> buscarFiltrado(String hql,int numeroPagina);
	public void  eliminar(Tecnico tecnico);
	public void eliminarVarios(List<Tecnico> tecnico);
	public boolean verificarEliminar(Tecnico tecnico);
	public long totalTecnicos();
	public long totalTecnicosFiltrados(String hql);
	public List<EstadoCivil> buscarEstadoCivilTecnico();
	public Tecnico buscarTecnicoPorCedula(String cedula);
	
	//Agregado por Jesus
	public Tecnico buscarPorCedula(String cedula);
}