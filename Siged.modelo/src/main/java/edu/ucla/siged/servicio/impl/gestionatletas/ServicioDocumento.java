package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoDocumentoI;
import edu.ucla.siged.domain.gestionatleta.Documento;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioDocumentoI;

@Service
public class ServicioDocumento implements ServicioDocumentoI {

	private @Autowired DaoDocumentoI daoDocumento;
	
	public void eliminarDocumentos(Set<Documento> documentos) {
		daoDocumento.delete(documentos);
	}

	public void eliminarDocumento(Documento documento) {
		daoDocumento.delete(documento);
	}

}
