package edu.ucla.siged.servicio.impl.reportes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoQuery;
import edu.ucla.siged.domain.reporte.Asistencia;
import edu.ucla.siged.domain.reporte.CausaInasistencia;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteAsistenciaI;

@Service("servicioReporteAsistencia")
public class ServicioReporteAsistencia implements ServicioReporteAsistenciaI {

	@Autowired
	private DaoQuery daoQuery;
	
	public List<Asistencia> buscarAsistencia(String restricciones, String asiste) {
		
		String sql ="SELECT p.id, p.fecha, " +
 " (SELECT count(*) cantidadAsistentes FROM AsistenciaAtleta WHERE asistencia = " + asiste + " and idPractica = p.id) as asistencia, e.nombre, " +
 " (SELECT count(ae.id) as inscritos FROM atletaequipo ae WHERE ae.idequipo = e.id) as cupostotales  " + 
 " FROM Practica p  INNER JOIN equipo e ON p.idequipo = e.id WHERE 1=1 " + restricciones;
		
		
		System.out.println("---------------------------------------" + System.lineSeparator() + sql);
		
		List consultaResultado= daoQuery.ejecutarQueryAsistencia(sql);
		List<Asistencia> lista= new ArrayList<Asistencia>();
		Asistencia asistencia;		
		
		for (Object resultado:consultaResultado){
			
			Object[] registro= (Object[]) resultado;
			asistencia= new Asistencia();
			asistencia.setId((Integer)registro[0]);
			asistencia.setFecha((Date)registro[1]);
			asistencia.setAsistencia((Integer)registro[2]);
			asistencia.setNombre(registro[3].toString() + " " + registro[1].toString());
			asistencia.setCupos((Integer)registro[4]);
			Double porcentaje = ((double)asistencia.getAsistencia() / (double)asistencia.getCupos()) * 100;
			double roundOff = Math.round(porcentaje*100)/100;
			asistencia.setPorcentaje(roundOff);
			lista.add(asistencia);
		}
		
		Collections.sort(lista);
		
		return lista;	 
	}
	
	
	
public List<CausaInasistencia> buscarCausaInasistencia(String restricciones, String becados, String noBecados) {

		
		String sql = "SELECT distinct(c.nombre) as nombre,  " +
				" (SELECT count(c2.id) as can FROM asistenciaatleta aa2 INNER JOIN causa c2 ON aa2.idcausa = c2.id " + 
				" INNER JOIN practica p1 ON aa2.idpractica = p1.id INNER JOIN equipo e1 ON p1.idequipo = e1.id  " +
				" WHERE c2.id = c.id AND aa2.asistencia = false " + restricciones + becados + ") as cantidad, " +
				" (SELECT count(c2.id) as can FROM asistenciaatleta aa2 INNER JOIN causa c2 ON aa2.idcausa = c2.id " + 
				" INNER JOIN practica p1 ON aa2.idpractica = p1.id INNER JOIN equipo e1 ON p1.idequipo = e1.id  " +
				" WHERE aa2.asistencia = false  " + restricciones + becados + ") as total " +
				" FROM causa c INNER JOIN asistenciaatleta aa ON aa.idcausa = c.id  ";
		
		
		System.out.println("---------------------------------------" + System.lineSeparator() + sql);
		
		List consultaResultado= daoQuery.ejecutarQueryCausaInasistencia(sql);
		List<CausaInasistencia> lista= new ArrayList<CausaInasistencia>();
		CausaInasistencia causaInasistencia;		
		
		for (Object resultado:consultaResultado){
			
			Object[] registro= (Object[]) resultado;
			causaInasistencia= new CausaInasistencia();
			causaInasistencia.setNombre(registro[0].toString());
			causaInasistencia.setCantidad((Integer)registro[1]);
			causaInasistencia.setTotal((Integer)registro[2]);
			Double porcentaje = ((double)causaInasistencia.getCantidad() / (double)causaInasistencia.getTotal()) * 100.00;
			Double porcentaje2D = Math.round(porcentaje*100.00)/100.00;
			causaInasistencia.setPorcentaje(porcentaje2D);
			lista.add(causaInasistencia);
			
		}
		
		Collections.sort(lista);
		
		return lista;	 
		
	}

	
}