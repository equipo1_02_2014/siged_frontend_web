package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestioneventos.Respuesta;

public interface ServicioRespuestaI {
	public static final int TAMANO_PAGINA = 4;
	public void guardar(Respuesta respuesta);
	public Page<Respuesta> buscarTodos(int numeroPagina);
	public Respuesta buscarPorId(Integer Id);
	public List<Respuesta> buscarFiltrado(String hql,int numeroPagina);
	public long totalRespuestas();
	public long totalRespuestasFiltradas(String hql);
	
}