package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoDelegadoEquipoI;
import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.DelegadoEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioDelegadoEquipoI;

@Service("servicioDelegadoEquipo")
public class ServicioDelegadoEquipo implements ServicioDelegadoEquipoI{

	@Autowired
	private DaoEquipoI daoEquipo;
	
	@Autowired
	private DaoDelegadoEquipoI daoDelegadoEquipo;		

	public void guardar(DelegadoEquipo delegadoEquipo) {
		daoDelegadoEquipo.save(delegadoEquipo);
		}
	public List<Equipo> buscarTodos() {	
		return daoEquipo.findByEstatus(Short.parseShort("1")); 
		}
	public Representante obtenerDelegado(Integer id) {
		Representante delegado = daoDelegadoEquipo.obtenerDelegado(id, Short.parseShort("1"));
		if(delegado == null){
			return null;
		}else{
			return delegado;
		}
	}
	
	public List<Representante> buscarRepresentantesDisponibles(){
		return daoDelegadoEquipo.buscarRepresentantesDisponiblesDao();
	}
	
	public Representante obtenerDelegadoPrincipal(Integer id) {
		Representante representante = daoDelegadoEquipo.obtenerDelegado(id, Short.parseShort("1"));
		if(representante != null){
			return representante;
		}else{
			return null;
		}
	}
	
	public Representante obtenerDelegadoSuplente(Integer id) {
		Representante representante = daoDelegadoEquipo.obtenerDelegado(id, Short.parseShort("2"));
		if(representante != null){
			return representante;
		}else{
			return null;
		}
	}

	public void revocarDelegadosEquipoCerrado(Equipo equipo){
		List <Representante> listaDelegadoEquipo = new ArrayList<Representante>();
		
		listaDelegadoEquipo = daoDelegadoEquipo.obtenerDelegadosEquipo(equipo);
		
		for(Representante delegado : listaDelegadoEquipo){
			DelegadoEquipo delegadoEquipo = daoDelegadoEquipo.obtenerDelegadoEquipo(delegado, equipo);
			daoDelegadoEquipo.delete(delegadoEquipo);
		}
	}
	
}
