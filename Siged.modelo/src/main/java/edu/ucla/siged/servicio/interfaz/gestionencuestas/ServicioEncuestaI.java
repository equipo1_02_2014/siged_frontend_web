package edu.ucla.siged.servicio.interfaz.gestionencuestas;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.seguridad.Rol;

public  interface ServicioEncuestaI {
	public static final int TAMANO_PAGINA = 10;
	
    public void guardar(Encuesta encuesta);
	public void eliminar(Encuesta encuesta) throws Exception;
	public void eliminarVarios(List<Encuesta> encuesta) throws Exception;
	public List<Encuesta> buscarTodos(int i);
	public long totalEncuestas();
	public List<Encuesta> buscarFiltrado(String jpql, int paginaActual);
	public long totalEncuestasFiltradas(String hql);
	public Encuesta buscarEncuestaByNombre(String nombre);
	public List<Encuesta> listarEncuestasPublicas();
	public List<Encuesta> listarEncuestasPublicasParaRoles(List<Rol> roles);
	public List<Encuesta> listarEncuestasNoRespondidasUsuario(List<Rol> roles,
			Integer id);
	public List<Encuesta> listarEncuestasUsuarioParaVotar(List<Rol> roles,
			Integer id);
	public List<Integer> listarIdEncuestasCerradasParaVerResultados(
			List<Rol> roles);
	public List<Encuesta> listarEncuestasCerradasPublicasPaginada(int i);
	public long totalEncuestasCerradasPublicas();
	public long totalEncuestasCerradasParaVerResultados(List<Rol> roles);
	public List<Encuesta> listarEncuestasCerradasParaVerResultados(
			List<Rol> roles, int i);
	public List<String> getResultadoOpcionPregunta(Integer idOpcion);
	public String getTotalVotosPregunta(Integer idPregunta);
		
}