package edu.ucla.siged.servicio.impl.gestiondeportiva;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoActividadI;
import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioActividadI;

@Service("servicioActividad")
public class ServicioActividad implements ServicioActividadI {

	@Autowired 
	private DaoActividadI daoActividad;
	@Autowired
	private SessionFactory sessionFactory;

	public List<Actividad> listarActividad() { 
          List<Actividad> listaActividad = daoActividad.findAll();
          return listaActividad;
	}
	
	public void guardar(Actividad actividad) {
		if(actividad.getId()==null){
			daoActividad.save(actividad);
		}
		else{
			actividad.setId(actividad.getId());
			daoActividad.save(actividad);
		}
	}

	public void eliminar(Actividad actividad) {
		actividad.setEstatus( Short.parseShort("0"));	
		daoActividad.delete(actividad);	
	}
	
	public void eliminarVarios(List<Actividad> actividades) {
		for(Actividad actividad:actividades){
			actividad.setEstatus(Short.parseShort("0"));
		}
		daoActividad.delete(actividades);
	}
	
	public Page<Actividad> buscarTodos(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoActividad.findAll(pagina);
	}
	
	public Actividad buscarPorId(Integer Id){
		return daoActividad.findOne(Id);
	}
		
	public List<Actividad> buscarTodos() {
		return daoActividad.findAll();
	}
	
	public List<Actividad> buscarActivos(){
		return daoActividad.findByEstatus(Short.parseShort("1"));
	}
	
	public List<Actividad> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Actividad where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Actividad> lista =query.list();
		session.close();
		return lista;
	}

	public long totalActividadesFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Actividad where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
	
	public long totalActividades() {
		return daoActividad.count();
	}

}