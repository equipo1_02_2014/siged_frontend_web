package edu.ucla.siged.servicio.interfaz.reportes;

import java.util.List;

import edu.ucla.siged.domain.reporte.Atributo;
import edu.ucla.siged.domain.reporte.Entidad;

public interface ServicioEntidadI {
	public List<Entidad> buscarTodas();
	public List<Atributo> buscarAtributos(Entidad entidad);
	
}
