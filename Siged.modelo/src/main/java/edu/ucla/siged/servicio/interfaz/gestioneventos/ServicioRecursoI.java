/** 
 * 	ServicioRecursoI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;
import org.springframework.data.domain.Page;
import edu.ucla.siged.domain.gestioneventos.Recurso;

public interface ServicioRecursoI {
	public static final int TAMANO_PAGINA = 5;
	public void guardar(Recurso recurso); //M�todo para guardar un registro.
	public Page<Recurso> buscarTodos(int numeroPagina); //M�todo para buscar todos los registros.
	public Recurso buscarPorId(Integer Id); //M�todo para buscar un registro por Id.
	public List<Recurso> buscarFiltrado(String hql,int numeroPagina); //M�todo para buscar los registros por filtrado y crear una lista.
	public void  actualizar(Recurso recurso); //M�todo para eliminar un registro l�gicamente.
	public void actualizarVarios(List<Recurso> recursos); //M�todo para eliminar varios registros l�gicamente.
	public void eliminar(Recurso recurso); //M�todo para eliminar un registro f�sicamente.
	public void eliminarVarios(List<Recurso> recursos); //M�todo para eliminar varios registros f�sicamente.
	public long totalRecursos(); //M�todo para retornar la cantidad de registros totales.
	public long totalRecursosFiltrados(String hql); //M�todo para retornar la cantidad de registros totales filtrados.
	public List<Recurso> ListarRecursos(); //M�todo para retornar una lista con todos los registros existentes.
}