package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.Date;

public interface ServicioHistoriaAyudaI {
	
	public boolean buscarBecaPorAtletaYFecha(Integer id, Date fecha );

}
