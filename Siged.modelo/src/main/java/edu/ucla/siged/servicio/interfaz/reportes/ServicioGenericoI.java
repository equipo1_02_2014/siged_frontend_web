package edu.ucla.siged.servicio.interfaz.reportes;

import java.util.List;

import edu.ucla.siged.domain.reporte.Generica;

public interface ServicioGenericoI {
	public List <Generica> buscarTodos(String hql);

}
