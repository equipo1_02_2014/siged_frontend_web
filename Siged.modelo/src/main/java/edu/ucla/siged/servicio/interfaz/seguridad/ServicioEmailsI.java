package edu.ucla.siged.servicio.interfaz.seguridad;

public interface ServicioEmailsI {
		
	  void enviarEmailRecuperarContrasena(String email, String password);
	  void enviarEmailUsuarioNuevo(String email,String nombreUsuario, String password);
	  void enviarEmailSolicitudPostulacion(String email, String nombre);
	  void enviarEmailAprobarPostulante(String email, String nombre);
	  //void enviarEmailPostulante(String email, String password);
	  //void enviarEmailPostulanteAprobado(String email, String password);
	  //void sendNormalEmail(String correo, String titulo, String contenido);

}