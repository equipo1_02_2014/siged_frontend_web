package edu.ucla.siged.servicio.impl.reportes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoFiltro;
import edu.ucla.siged.domain.reporte.Generica;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioGenericoI;

@Service("servicioGenerico")
public class ServicioGenerico implements ServicioGenericoI {
	@Autowired
	DaoFiltro<Generica> daoFiltro;
	
	public List <Generica> buscarTodos(String hql){
		return daoFiltro.buscarTodos(hql);
	}
	
}