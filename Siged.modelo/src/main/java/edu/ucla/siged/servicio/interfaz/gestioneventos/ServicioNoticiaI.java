package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;
import edu.ucla.siged.domain.gestioneventos.Noticia;

public interface ServicioNoticiaI {
	
	public static final int TAMANO_PAGINA = 5;
	public void Guardar(Noticia n);	
	public void Actualizar(Noticia n);	
	public void Eliminar(Integer id);	
	public void EliminarVarios(List<Noticia> noticias);	
	public List<Noticia> ListarNoticias();	
	public List<Noticia> buscarTodas(int numeroPagina);
	public long totalNoticias();	
	public long totalNoticiasFiltradas(String hql);
	public List<Noticia> buscarFiltrado(String hql,int numeroPagina);
	public List<Noticia> buscarTodas(int numeroPagina, int tamano_pg);

}
