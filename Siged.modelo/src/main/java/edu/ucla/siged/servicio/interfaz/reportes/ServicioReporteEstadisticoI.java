package edu.ucla.siged.servicio.interfaz.reportes;

import java.util.List;

import edu.ucla.siged.domain.reporte.AceptadosPostuladosPorMes;
import edu.ucla.siged.domain.reporte.CumplimientoActividad;
import edu.ucla.siged.domain.reporte.FrecuenciaCausa;
import edu.ucla.siged.domain.reporte.PromedioCantidadBase;
import edu.ucla.siged.domain.reporte.RecursoEvento;
import edu.ucla.siged.domain.reporte.RecursoLlega;
import edu.ucla.siged.domain.reporte.ResultadosJuegos;

public interface ServicioReporteEstadisticoI {

	public List<PromedioCantidadBase> buscarEquiposMayorPromedioJuegosGanados(String restricciones,int cantidadEquipos);
	public List<RecursoEvento> buscarRecursosEventos(String restricciones,int cantidadEquipos);
	public List<FrecuenciaCausa> buscarFrecuenciaCausa(String restricciones,int cantidadCausas);
	//################################################ AGREGADO POR EMILY SILVA Y GABRIELA PALMAR ########################################
	public List<RecursoLlega> buscarRecursos(String restricciones, String evento);
	//################################################ HASTA AQUI ########################################
	//HILDA
	public List<AceptadosPostuladosPorMes> buscarAceptadosPostulados(String restricciones);
	//------------------------------------- metodo de Lilianny reporte ResultadosJuegos ---------------------------------------
	public List<ResultadosJuegos> listarResultadosJuegos(String restricciones);
	public List<CumplimientoActividad> listaCumplimiento(String restricciones, int tipo);
	
}
