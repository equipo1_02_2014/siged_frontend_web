/** 
 * 	ServicioTipoAreaI
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos;

import java.util.List;
import edu.ucla.siged.domain.gestionrecursostecnicios.TipoArea;

public interface ServicioTipoAreaI {
	List<TipoArea> obtenerListaTipoArea();
}