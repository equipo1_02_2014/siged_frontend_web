package edu.ucla.siged.servicio.impl.gestiondeportiva;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoAreaI;
import edu.ucla.siged.dao.DaoFiltro;
import edu.ucla.siged.dao.DaoJuegoI;
import edu.ucla.siged.dao.DaoPosicionArbitroI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.PosicionArbitro;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioHorarioAreaI;

@Service("servicioJuego")
public class ServicioJuego implements ServicioJuegoI {
	
	@Autowired
	private DaoJuegoI daoJuego;
	
	@Autowired
	private DaoPosicionArbitroI daoPosicionArbitro;
	
	//Cambiar por Servicios cuando este listo
	@Autowired
	private DaoAreaI daoArea; 
	
	@Autowired
	private DaoFiltro<Juego> daoFiltro;
	
	@Autowired
	private ServicioHorarioAreaI servicioHorarioArea;
	

	@Transactional(readOnly=false)
	public void guardar(Juego juego) {
		
		daoJuego.save(juego);
		
	}

	@Transactional(readOnly=false)
	public void editar(Juego juego) {
		
		daoJuego.save(juego);
		
	}

	public List<Juego> buscarTodos() {
		
		return daoJuego.findAll();
		
	}

	/**
	 * Este Metodo Devuelve true en caso de que el Equipo ya tenga un Juego
	 * el dia y hora pasadas por parametros. A la hora se le suma 1 hora mas
	 * para tener holgura
	 * */
	public boolean isEquipoJuegoFechaHora(Equipo equipo,Date fecha, Date hora) {
		
        Calendar fechaInicioJuego= Calendar.getInstance();
		
		Calendar fechaFinalJuego= Calendar.getInstance();
		
	    String anio= new SimpleDateFormat("yyyy").format(fecha);
		
		String mes= new SimpleDateFormat("M").format(fecha);
		
		String dia= new SimpleDateFormat("d").format(fecha);
		
		String hour = new SimpleDateFormat("H").format(hora);
		
		String minute= new SimpleDateFormat("m").format(hora);
		
		fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                 Integer.parseInt(dia), Integer.parseInt(hour), 
			                 Integer.parseInt(minute),0);
	
	    fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
		                    Integer.parseInt(dia), Integer.parseInt(hour), 
		                    Integer.parseInt(minute),0);
	    
	    fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
	    
	    fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		List<Juego> juegos= daoJuego.buscarJuegosPorEquipoTiempo(equipo.getId(), fecha,fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		boolean existe= false;
		
		if (juegos!=null && !juegos.isEmpty()){
		
			existe=true;
			
		}
		
		return existe;
		
		
	}

	public boolean isAreaOcupadaFechaHora(Area area, Date fecha, Date hora) {
        
        Calendar fechaInicioJuego= Calendar.getInstance();
		
		Calendar fechaFinalJuego= Calendar.getInstance();
		
	    String anio= new SimpleDateFormat("yyyy").format(fecha);
		
		String mes= new SimpleDateFormat("M").format(fecha);
		
		String dia= new SimpleDateFormat("d").format(fecha);
		
		String hour = new SimpleDateFormat("H").format(hora);
		
		String minute= new SimpleDateFormat("m").format(hora);
		
		fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                 Integer.parseInt(dia), Integer.parseInt(hour), 
			                 Integer.parseInt(minute),0);
	
	    fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
		                    Integer.parseInt(dia), Integer.parseInt(hour), 
		                    Integer.parseInt(minute),0);
	    
	    fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
	    
	    fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		List<Juego> juegos= daoJuego.buscarJuegosPorAreaTiempo(area.getId(), fecha,fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		boolean existe= false;
		
		if (juegos!=null && !juegos.isEmpty()){
		
			existe=true;
			
		}
		
		return existe;
	}

	public List<Area> buscarAreasJuego() {
		List<Area> areas= daoArea.findByEstatus(Short.parseShort("1"));
		List<Area> areasJuego= new ArrayList<Area>();
		
		if (areas!=null){
		    
		    for(Area a:areas){
		    	
		    	if(!a.getNombre().equalsIgnoreCase("TUNEL")){
		    		areasJuego.add(a);
		    	}
		    }
		    
		}
		
		return areasJuego;
	}

	
	public List<Anotador> buscarAnotadoresDisponibles(Date fecha,Date hora) {
			
		Calendar fechaInicioJuego= Calendar.getInstance();
		
		Calendar fechaFinalJuego= Calendar.getInstance();
		
	    String anio= new SimpleDateFormat("yyyy").format(fecha);
		
		String mes= new SimpleDateFormat("M").format(fecha);
		
		String dia= new SimpleDateFormat("d").format(fecha);
		
		String hour = new SimpleDateFormat("H").format(hora);
		
		String minute= new SimpleDateFormat("m").format(hora);
		
		fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                 Integer.parseInt(dia), Integer.parseInt(hour), 
			                 Integer.parseInt(minute),0);
	
	    fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
		                    Integer.parseInt(dia), Integer.parseInt(hour), 
		                    Integer.parseInt(minute),0);
	    
	    fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
	    
	    fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		
		List<Anotador> lista= daoJuego.buscarAnotadoresDisponibles(fecha,fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		return lista;
	}

	public List<Arbitro> buscarArbitrosDisponibles(Date fecha,Date hora) {
		
	    Calendar fechaInicioJuego= Calendar.getInstance();
		
		Calendar fechaFinalJuego= Calendar.getInstance();
		
	    String anio= new SimpleDateFormat("yyyy").format(fecha);
		
		String mes= new SimpleDateFormat("M").format(fecha);
		
		String dia= new SimpleDateFormat("d").format(fecha);
		
		String hour = new SimpleDateFormat("H").format(hora);
		
		String minute= new SimpleDateFormat("m").format(hora);
		
		fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                 Integer.parseInt(dia), Integer.parseInt(hour), 
			                 Integer.parseInt(minute),0);
	
	    fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
		                    Integer.parseInt(dia), Integer.parseInt(hour), 
		                    Integer.parseInt(minute),0);
	    
	    fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
	    
	    fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
	    
	    List<Arbitro> lista= daoJuego.buscarArbitrosDisponibles(fecha,fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
	    
			
		return lista;
	}

	public List<PosicionArbitro> buscarPosicionesArbitros() {
		List<PosicionArbitro> posiciones= daoPosicionArbitro.findAll();
		
		return posiciones;
	}

	public List<Juego> buscarJuegosCompetenciaTerminados(Competencia competencia) {
		List<Juego> listaJuegos= new ArrayList<Juego>();
		
		if (competencia!=null){
		
			listaJuegos = daoJuego.buscarJuegosCompetenciaPasados(competencia.getId());
		   
			List<Juego> listaJuegosHoraInicioPasada= daoJuego.buscarJuegosCompetenciaHoyHoraPasada(competencia.getId());
			
			listaJuegos.addAll(listaJuegosHoraInicioPasada);
		   
		}
		return listaJuegos;
	}
	
	public List<Juego> buscarJuegosPasados(int nroPagina){
        List<Juego> listaJuegos= new ArrayList<Juego>();
            
        Sort orden= new Sort(Sort.Direction.DESC,"fecha");
		
		Pageable paginable= new PageRequest(nroPagina,ServicioJuegoI.TAMANIO_PAGINA,orden);
	
		
		Page<Juego> pagina = daoJuego.buscarJuegosPasados(paginable);
		   
			
		return pagina.getContent();
	}
	
	public List<Juego> buscarJuegosPasadosSinResultado(int nroPagina){
        List<Juego> listaJuegos= new ArrayList<Juego>();
            
        Sort orden= new Sort(Sort.Direction.DESC,"fecha");
		
		Pageable paginable= new PageRequest(nroPagina,ServicioJuegoI.TAMANIO_PAGINA,orden);
	
		
		Page<Juego> pagina = daoJuego.buscarJuegosPasadosSinResultado(paginable);
		   
			
		return pagina.getContent();
	}
	
	

	public List<Juego> buscarTodos(int nroPagina) {
			Sort orden= new Sort(Sort.Direction.DESC,"fecha");
			
			Pageable paginable= new PageRequest(nroPagina,ServicioJuegoI.TAMANIO_PAGINA,orden);
			
			Page<Juego> pagina= daoJuego.findAll(paginable);
			
			return pagina.getContent();
		
	}

	public long obtenerCantidadJuegos() {
		return daoJuego.count();
	}

	public int obtenerTamanioPagina() {
		return ServicioJuegoI.TAMANIO_PAGINA;
	}
	
	public List<Juego> buscarFiltradoJuegosPasados(String hql, int numeroPagina){
		
		String filtroJuegosPasados= " AND j.id IN (SELECT j.id FROM Juego j WHERE j.fecha < CURRENT_DATE "
				                    + "OR j.id IN (SELECT g.id FROM Juego g WHERE g.fecha = CURRENT_DATE AND g.horaInicio < CURRENT_TIME))";
		
        hql="SELECT j " + hql + filtroJuegosPasados + " ORDER BY j.fecha DESC";
		
		return daoFiltro.buscarFiltrados(hql, ServicioJuegoI.TAMANIO_PAGINA, numeroPagina);
	}
	
	public long obtenerCantidadJuegosPasados(){
		
		List<Juego> juegos= daoJuego.buscarJuegosPasados();
		
		return juegos.size();
	}
	
    public long obtenerCantidadJuegosPasadosSinResultado(){
		
		List<Juego> juegos= daoJuego.buscarJuegosPasadosSinResultado();
		
		return juegos.size();
	}

	public List<Juego> buscarFiltrado(String hql, int numeroPagina) {
		
		hql="SELECT j " + hql + " ORDER BY j.fecha DESC";
		
		return daoFiltro.buscarFiltrados(hql, ServicioJuegoI.TAMANIO_PAGINA, numeroPagina);
	}

	public long obtenerTotalFiltrados(String hql) {
		return daoFiltro.totalFiltrados(hql);
	}


	public Set<ArbitroJuego> obtenerArbitrosJuego(Juego juego) {
		
		List<ArbitroJuego> listaArbitroJuego= daoJuego.obtenerArbitroJuego(juego.getId());
		
		Set<ArbitroJuego> arbitrosJuego= new HashSet<ArbitroJuego>(listaArbitroJuego);
		
		
		return arbitrosJuego;
	}

	public List<ArbitroJuego> obtenerListaArbitrosJuego(Juego juego){
		List<ArbitroJuego> listaArbitroJuego= daoJuego.obtenerArbitroJuego(juego.getId());
		
		return listaArbitroJuego;
	}
	
	 public List<Juego> buscarJuegosDeEquipoPorFechaAndHora(Equipo equipo,Date fecha,Date horaInicio){
		 Calendar fechaInicioJuego= Calendar.getInstance();
			
		 Calendar fechaFinalJuego= Calendar.getInstance();
			
		 String anio= new SimpleDateFormat("yyyy").format(fecha);
			
	     String mes= new SimpleDateFormat("M").format(fecha);
			
		 String dia= new SimpleDateFormat("d").format(fecha);
			
		 String hour = new SimpleDateFormat("H").format(horaInicio);
			
		 String minute= new SimpleDateFormat("m").format(horaInicio);
			
		 fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
				                 Integer.parseInt(dia), Integer.parseInt(hour), 
				                 Integer.parseInt(minute),0);
		
		 fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                    Integer.parseInt(dia), Integer.parseInt(hour), 
			                    Integer.parseInt(minute),0);
		    
		 fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
		    
		 fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		 List<Juego> juegos= daoJuego.buscarJuegosPorEquipoTiempo(equipo.getId(),fechaInicioJuego.getTime() , 
				                                                  fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		return juegos;
	 }
	    
	 public boolean existeOtroJuegoDeEquipoPorFechaYHora(Juego juego,Equipo equipo,Date fecha,Date horaInicio){
		    
		 List<Juego> juegos= this.buscarJuegosDeEquipoPorFechaAndHora(equipo,fecha,horaInicio);
			
			boolean existe= false;
			
			for (Juego juegoIterador:juegos){
				
				if (!juegoIterador.getId().equals(juego.getId())){
					existe= true;
				}
				
			}
			
			return existe;
	 }
	
	
	public List<Juego> buscarJuegosDeAnotadorPorFechaAndHora(Anotador anotador,
			Date fecha, Date horaInicio) {
		
		 Calendar fechaInicioJuego= Calendar.getInstance();
			
		 Calendar fechaFinalJuego= Calendar.getInstance();
			
		 String anio= new SimpleDateFormat("yyyy").format(fecha);
			
	     String mes= new SimpleDateFormat("M").format(fecha);
			
		 String dia= new SimpleDateFormat("d").format(fecha);
			
		 String hour = new SimpleDateFormat("H").format(horaInicio);
			
		 String minute= new SimpleDateFormat("m").format(horaInicio);
			
		 fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
				                 Integer.parseInt(dia), Integer.parseInt(hour), 
				                 Integer.parseInt(minute),0);
		
		 fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                    Integer.parseInt(dia), Integer.parseInt(hour), 
			                    Integer.parseInt(minute),0);
		    
		 fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
		    
		 fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		List<Juego> juegos= daoJuego.buscarJuegosDeAnotadorPorFechaAndHora(anotador.getId(),fechaInicioJuego.getTime() , 
				                                                           fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		return juegos;
	}
	
	public List<Juego> buscarJuegosDeArbitroPorFechaAndHora(Arbitro arbitro,Date fecha,Date horaInicio){
		 Calendar fechaInicioJuego= Calendar.getInstance();
			
		 Calendar fechaFinalJuego= Calendar.getInstance();
			
		 String anio= new SimpleDateFormat("yyyy").format(fecha);
			
	     String mes= new SimpleDateFormat("M").format(fecha);
			
		 String dia= new SimpleDateFormat("d").format(fecha);
			
		 String hour = new SimpleDateFormat("H").format(horaInicio);
			
		 String minute= new SimpleDateFormat("m").format(horaInicio);
			
		 fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
				                 Integer.parseInt(dia), Integer.parseInt(hour), 
				                 Integer.parseInt(minute),0);
		
		 fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                    Integer.parseInt(dia), Integer.parseInt(hour), 
			                    Integer.parseInt(minute),0);
		    
		 fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
		    
		 fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		List<Juego> juegos= daoJuego.buscarJuegosDeArbitroPorFechaAndHora(arbitro.getId(), fechaInicioJuego.getTime(), 
				                                                          fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		return juegos;
	}


	public boolean existeOtroJuegoDeAnotadorPorFechaYHora(Juego juego,
			Anotador anotador, Date fecha, Date horaInicio) {
		
		List<Juego> juegos= this.buscarJuegosDeAnotadorPorFechaAndHora(anotador, fecha, horaInicio);
		
		boolean existe= false;
		
		for (Juego juegoIterador:juegos){
			
			if (!juegoIterador.getId().equals(juego.getId())){
				existe= true;
			}
			
		}
		
		return existe;
	}

	public boolean existeOtroJuegoDeArbitroPorFechaYHora(Juego juego,
			Arbitro arbitro, Date fecha, Date horaInicio) {
		
		List<Juego> juegos= this.buscarJuegosDeArbitroPorFechaAndHora(arbitro, fecha, horaInicio);
		
		boolean existe= false;
		
        for (Juego juegoIterador:juegos){
			
			if (!juegoIterador.getId().equals(juego.getId())){
				existe= true;
			}
			
		}
		
		return existe;
	}
	
	

	public boolean existeArbitrosRepetidos(Collection<ArbitroJuego> arbitros) {
		
		//Comparator<ArbitroJuego> comparator= new ComparatorArbitro();
		TreeSet<ArbitroJuego> arbitrosJuego= new TreeSet<ArbitroJuego>(new ComparatorArbitro<ArbitroJuego>());
		boolean existenArbitrosRepetidos=false;
		
		arbitrosJuego.addAll(arbitros);
		
		existenArbitrosRepetidos= (arbitrosJuego.size() != arbitros.size());
		
		return existenArbitrosRepetidos;
	}
	



	public List<Juego> buscarJuegosDeAreaPorFechaAndHora(Area area, Date fecha,
			Date horaInicio) {
		Calendar fechaInicioJuego= Calendar.getInstance();
		
		 Calendar fechaFinalJuego= Calendar.getInstance();
			
		 String anio= new SimpleDateFormat("yyyy").format(fecha);
			
	     String mes= new SimpleDateFormat("M").format(fecha);
			
		 String dia= new SimpleDateFormat("d").format(fecha);
			
		 String hour = new SimpleDateFormat("H").format(horaInicio);
			
		 String minute= new SimpleDateFormat("m").format(horaInicio);
			
		 fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
				                 Integer.parseInt(dia), Integer.parseInt(hour), 
				                 Integer.parseInt(minute),0);
		
		 fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                    Integer.parseInt(dia), Integer.parseInt(hour), 
			                    Integer.parseInt(minute),0);
		    
		 fechaInicioJuego.add(Calendar.MINUTE, -(ServicioJuegoI.MINUTOS_DURACION_JUEGO));
		    
		 fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		List<Juego> juegos= daoJuego.buscarJuegosPorAreaTiempo(area.getId(), fechaInicioJuego.getTime(), 
				                                                          fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		return juegos;
	}

	public boolean existeOtroJuegoDeAreaPorFechaYHora(Juego juego, Area area,
			Date fecha, Date horaInicio) {
	
		List<Juego> juegos= this.buscarJuegosDeAreaPorFechaAndHora(area, fecha, horaInicio);
		
        boolean existe= false;
		
        for (Juego juegoIterador:juegos){
			
			if (!juegoIterador.getId().equals(juego.getId())){
				existe= true;
			}
			
		}
		
		return existe;
	}
	
	
	
	/** Comparator para ArbitroJuego 
	 * Valida que no haya Arbitros Iguales
	 * ***********************************************/
	
	public static class ComparatorArbitro<ArbitroJuego> implements Comparator<ArbitroJuego>{
        
		public int compare(ArbitroJuego arbj1, ArbitroJuego arbj2) {
			
			return ((edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego) arbj1).getArbitro().getId().compareTo(((edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego) arbj2).getArbitro().getId());
			
		}

		public static <T, U extends Comparable<? super U>> Comparator<T> comparing(
				Function<? super T, ? extends U> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T, U> Comparator<T> comparing(
				Function<? super T, ? extends U> arg0,
				Comparator<? super U> arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T> Comparator<T> comparingDouble(
				ToDoubleFunction<? super T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T> Comparator<T> comparingInt(
				ToIntFunction<? super T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T> Comparator<T> comparingLong(
				ToLongFunction<? super T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T extends Comparable<? super T>> Comparator<T> naturalOrder() {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T> Comparator<T> nullsFirst(Comparator<? super T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T> Comparator<T> nullsLast(Comparator<? super T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public static <T extends Comparable<? super T>> Comparator<T> reverseOrder() {
			// TODO Auto-generated method stub
			return null;
		}

		public Comparator<ArbitroJuego> reversed() {
			// TODO Auto-generated method stub
			return null;
		}

		public Comparator<ArbitroJuego> thenComparing(
				Comparator<? super ArbitroJuego> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public <U extends Comparable<? super U>> Comparator<ArbitroJuego> thenComparing(
				Function<? super ArbitroJuego, ? extends U> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public <U> Comparator<ArbitroJuego> thenComparing(
				Function<? super ArbitroJuego, ? extends U> arg0,
				Comparator<? super U> arg1) {
			// TODO Auto-generated method stub
			return null;
		}

		public Comparator<ArbitroJuego> thenComparingDouble(
				ToDoubleFunction<? super ArbitroJuego> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public Comparator<ArbitroJuego> thenComparingInt(
				ToIntFunction<? super ArbitroJuego> arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public Comparator<ArbitroJuego> thenComparingLong(
				ToLongFunction<? super ArbitroJuego> arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}

	public boolean isAreaOcupadaHorario(Area area,Date fecha,Date horaInicio){
		
		 Calendar fechaInicioJuego= Calendar.getInstance();
		
		 Calendar fechaFinalJuego= Calendar.getInstance();
			
		 String anio= new SimpleDateFormat("yyyy").format(fecha);
			
	     String mes= new SimpleDateFormat("M").format(fecha);
			
		 String dia= new SimpleDateFormat("d").format(fecha);
			
		 String hour = new SimpleDateFormat("H").format(horaInicio);
			
		 String minute= new SimpleDateFormat("m").format(horaInicio);
			
		 fechaInicioJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
				                 Integer.parseInt(dia), Integer.parseInt(hour), 
				                 Integer.parseInt(minute),0);
		
		 fechaFinalJuego.set(Integer.parseInt(anio), Integer.parseInt(mes)-1, 
			                    Integer.parseInt(dia), Integer.parseInt(hour), 
			                    Integer.parseInt(minute),0);
		    
		 fechaFinalJuego.add(Calendar.MINUTE, ServicioJuegoI.MINUTOS_DURACION_JUEGO);
		
		 
		List<HorarioArea> lista= servicioHorarioArea.buscarHorarioAreaOcupadas(area, fechaInicioJuego.getTime(),fechaInicioJuego.getTime(), fechaFinalJuego.getTime());
		
		System.out.println("----------------------");
		for (HorarioArea ha:lista){
			System.out.println(ha.getArea().getNombre());
		}
		System.out.println("----------------------");
		
		
		
		
		return !lista.isEmpty();
	}


	
	/***********************************************************************************/
	
	public Page<Juego> buscarJuegosFuturos(int numeroPagina)
	{
		PageRequest pagina = new PageRequest(numeroPagina,this.obtenerTamanioPagina(), Sort.Direction.ASC,"fecha");
		return daoJuego.buscarJuegosFuturos(pagina);
	}
	
	public int obtenerCantidadJuegosFuturos(){
		
		List<Juego> juegos= daoJuego.buscarJuegosFuturos();
		
		return juegos.size();
	}

	public List<Juego> buscarTodosCompetenciasActivas(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina,this.obtenerTamanioPagina(), Sort.Direction.ASC,"fecha");
		return daoJuego.buscarTodosCompetenciasActivas(pagina).getContent();
	}

	public long obtenerCantidadJuegosCompetenciasActivas() {
        List<Juego> juegos= daoJuego.buscarTodosCompetenciasActivas();
		
		return juegos.size();
	}

	//Solicitado para verificar la existencia de un Area en algun juego
    public boolean existeAreaEnJuego(Area area){
    	List<Juego> lista= daoJuego.buscarAreaJuego(area.getId());
    	
    	return !lista.isEmpty();
    }
    
  //Se agregan para soporte de acciones propias del usuario
  	public List<Juego> buscarJuegosPorRepresentante(int numeroPagina,
  			Representante representante) {
  		PageRequest pagina = new PageRequest(numeroPagina,this.obtenerTamanioPagina(), Sort.Direction.ASC,"fecha");
  		
  		List<Juego> lista= daoJuego.buscarJuegosPorRepresentante(representante.getCedula(),pagina).getContent();
  		
  		return lista;
  	}

  	public long obtenerCantidadJuegosPorRepresentante(Representante representante) {
  		return daoJuego.obtenerCantidadJuegosPorRepresentante(representante.getCedula());
  	}

  	public List<Juego> buscarJuegosPorTecnico(int numeroPagina, Tecnico tecnico) {
          PageRequest pagina = new PageRequest(numeroPagina,this.obtenerTamanioPagina(), Sort.Direction.ASC,"fecha");
  		
  		List<Juego> lista= daoJuego.buscarJuegosPorTecnico(tecnico.getCedula(),pagina).getContent();
  		
  		return lista;
  	}

  	public long obtenerCantidadJuegosPorTecnico(Tecnico tecnico) {
  		
  		return daoJuego.obtenerCantidadJuegosPorTecnico(tecnico.getCedula());
  	}

  	public List<Juego> buscarJuegosPorAtleta(int numeroPagina, Atleta atleta) {
          PageRequest pagina = new PageRequest(numeroPagina,this.obtenerTamanioPagina(), Sort.Direction.ASC,"fecha");
  		
  		List<Juego> lista= daoJuego.buscarJuegosPorAtleta(atleta.getCedula(),pagina).getContent();
  		
  		return lista;
  	}

  	public long obtenerCantidadJuegosPorAtleta(Atleta atleta) {
  		return daoJuego.obtenerCantidadJuegosPorAtleta(atleta.getCedula());
  	}
  	
}
