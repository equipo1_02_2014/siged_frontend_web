package edu.ucla.siged.servicio.impl.gestiondeportiva;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoActividadI;
import edu.ucla.siged.dao.DaoActividadPracticaI;
import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioActividadPracticaI;

@Service("servicioActividadPractica")
public class ServicioActividadPractica implements ServicioActividadPracticaI {

	@Autowired 
	private DaoActividadPracticaI daoActividadPractica;
	
	public boolean existeAreaEnActividadPractica(Area area) {
		List<ActividadPractica> lista= daoActividadPractica.buscarAreaEnActividad(area.getId());
		
		return !lista.isEmpty();
	}

}
