package edu.ucla.siged.servicio.impl.gestionencuestas;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoRespuestaI;
import edu.ucla.siged.domain.gestioneventos.Respuesta;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioRespuestaI;

@Service("servicioRespuesta")
public class ServicioRespuesta implements ServicioRespuestaI{
	@Autowired
	private DaoRespuestaI daoRespuesta;
	@Autowired
	private SessionFactory sessionFactory;
	
	public void guardar(Respuesta respuesta) {
		// TODO Auto-generated method stub

			daoRespuesta.save(respuesta);
	}
	
	
	public Page<Respuesta> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoRespuesta.findAll(pagina);
	}
	
	public Respuesta buscarPorId(Integer Id) {
		// TODO Auto-generated method stub
		return daoRespuesta.findOne(Id);
	}
	
	public List<Respuesta> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Respuesta where "+hql); //daoEquipo.findAll();
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Respuesta> lista =query.list(); //daoEquipo.findAll();
		session.close();
		return lista;

		}

	
	//public void eliminar(Respuesta respuesta) {
		// TODO Auto-generated method stub
		//7respuesta.setEstatus(Short.parseShort("0"));
		//anotador.setEstatus(null);
		//anotador.setEstatus(0);
		//daoAnotador.save(anotador);
	//}
	//public void eliminarVarios(List<Anotador> anotadores) {
		// TODO Auto-generated method stub
		//for (int i=0;i<anotadores.size();i++)
			//anotadores.get(i).setEstatus(Short.parseShort("0"));
			//anotadores.get(i).setEstatus(0);
		//daoAnotador.save(anotadores);
	//}
	
	public long totalRespuestas() {
		// TODO Auto-generated method stub
		return daoRespuesta.count();
	}


	public long totalRespuestasFiltradas(String hql) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Respuesta where "+hql); //daoEquipo.findAll();
		long total= (Long) query.uniqueResult(); //daoEquipo.findAll();
		session.close();
		return total;

	}
	
	
}