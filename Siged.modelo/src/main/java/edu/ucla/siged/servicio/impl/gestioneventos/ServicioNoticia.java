package edu.ucla.siged.servicio.impl.gestioneventos;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoNoticiaI;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioNoticiaI;

@Service("servicioNoticia")
public class ServicioNoticia implements ServicioNoticiaI{
	
	@Autowired
	private DaoNoticiaI daoNoticiaI;
	@Autowired
	private SessionFactory sessionFactory;//nuevo 09-12-14
	
	public ServicioNoticia(){}
	
	public void Guardar(Noticia n){
		daoNoticiaI.save(n);
	}
	
	public void Actualizar(Noticia n){
		daoNoticiaI.save(n);
	}
	
	public void Eliminar(Integer id){
		daoNoticiaI.delete(id);
	}
	
	public void EliminarVarios(List<Noticia> noticias) {
		daoNoticiaI.delete(noticias);
	}
	
	public List<Noticia> ListarNoticias(){
		List<Noticia> listanoticias = daoNoticiaI.findAll();
		for(Noticia noticia : listanoticias){
			noticia.getAdjuntoNoticias().size();
		}
		for(Noticia noticia : listanoticias){
			noticia.getPublicos().size();
		}
		return listanoticias;
	}
	
	//nuevo 09-12-14
	public List<Noticia> buscarTodas(int numeroPagina){
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.DESC,"fecha");
		List<Noticia> listanoticias = daoNoticiaI.findAll(pagina).getContent();
		for(Noticia noticia : listanoticias){
			noticia.getAdjuntoNoticias().size();
		}
		for(Noticia noticia : listanoticias){
			noticia.getPublicos().size();
		}
		return listanoticias;
		
	}
		
	//nuevo 09-12-14
	public long totalNoticias(){
		return daoNoticiaI.count();
	}
		
	//nuevo 09-12-14
	public long totalNoticiasFiltradas(String hql){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Noticia where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
		
	//nuevo 09-12-14
	public List<Noticia> buscarFiltrado(String hql,int numeroPagina){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Noticia where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Noticia> lista = query.list();
		for(Noticia noticia : lista){
			noticia.getAdjuntoNoticias().size();
		}
		for(Noticia noticia : lista){
			noticia.getPublicos().size();
		}
		session.close();
		return lista;
	}
	
	//nuevo 27-01-15
	public List<Noticia> buscarTodas(int numeroPagina, int tamano_pg){
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Noticia ORDER BY fecha, hora DESC");
		query.setMaxResults(tamano_pg);
		query.setFirstResult(numeroPagina * tamano_pg);
		List<Noticia> lista = query.list();
		for(Noticia noticia : lista){
			noticia.getAdjuntoNoticias().size();
		}
		for(Noticia noticia : lista){
			noticia.getPublicos().size();
		}
		session.close();
		return lista;
		
	}
	
	public List<Noticia> buscarTodasPorRol(List<Rol> roles, int numeroPagina, int tamano_pg){
		String rolRestriccion="";
		for (int i = 0; i < roles.size(); i++){
			rolRestriccion+=roles.get(i).getId();
			if(i+1<roles.size())
			rolRestriccion+=",";
		}
		
		String q="select N from Noticia N left join N.publicos P where (P is null or P.rol.id in ("+rolRestriccion+")) ORDER BY fecha, hora DESC";
		
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery(q);
		query.setMaxResults(tamano_pg);
		query.setFirstResult(numeroPagina * tamano_pg);
		List<Noticia> lista = query.list();
		for(Noticia noticia : lista){
			noticia.getAdjuntoNoticias().size();
		}
		for(Noticia noticia : lista){
			noticia.getPublicos().size();
		}
		session.close();
		return lista;
		
	}
	
	public long TodasPorRol(List<Rol> roles){
		String rolRestriccion="";
		for (int i = 0; i < roles.size(); i++){
			rolRestriccion+=roles.get(i).getId();
			if(i+1<roles.size())
			rolRestriccion+=",";
		}
		
		String q="select count(N) from Noticia N left join N.publicos P where (P is null or P.rol.id in ("+rolRestriccion+"))";
		
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery(q);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
	/*************/
	
	public List<Noticia> buscarTodasPublicas(int numeroPagina, int tamano_pg){
		
		String q="select N from Noticia N left join N.publicos P where (P is null) ORDER BY fecha, hora DESC";
		
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery(q);
		query.setMaxResults(tamano_pg);
		query.setFirstResult(numeroPagina * tamano_pg);
		List<Noticia> lista = query.list();
		for(Noticia noticia : lista){
			noticia.getAdjuntoNoticias().size();
		}
		for(Noticia noticia : lista){
			noticia.getPublicos().size();
		}
		session.close();
		return lista;
		
	}
	
	public long TodasPublicas(){
		
		String q="select count(N) from Noticia N left join N.publicos P where (P is null)";
		
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery(q);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
		
}
