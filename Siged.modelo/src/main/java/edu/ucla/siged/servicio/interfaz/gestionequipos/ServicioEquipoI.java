package edu.ucla.siged.servicio.interfaz.gestionequipos;

import java.util.List;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;

public interface ServicioEquipoI {

	public static final int TAMANIO_PAGINA = 8;
	public void guardarEquipo( Equipo equipo );
	public List<Equipo> buscarTodos();
	public List<Equipo> buscarTodosTrue();
	public void eliminarEquipo( Equipo equipo );
	public void eliminarEquipos( List<Equipo> equipos );
	public List<AtletaEquipo> atletaPorEquipo(Equipo equipo);
	public List<Atleta> obtenerAtletasPorEquipo(Equipo equipo);
	public List<Equipo> buscarEquipos(int numeroPagina);
	public List<Equipo> obtenerListaEquipos(int numeroPagina, String nombre, String categoria, String rango, String tipoEquipo);
	public Page<Equipo> buscarTodos(int numeroPagina);	
	public long totalEquipos();
	public long totalEquiposFiltrados(String hql);
	public List<Equipo> buscarEquiposRoster();
	public Page<Equipo> buscarEquiposTrue(int numeroPagina);
	public List<Equipo> buscarFiltrado(String hql,int numeroPagina);
	public List<Equipo> buscarEquiposTrue();
	public boolean buscarNombreEquipo(Equipo equipo);
	public List<Equipo> buscarEquiposPorTecnico(Integer idTecnico, int numeroPagina, int tamanoPagina);
	public long totalEquipoPorTecnico(Integer idTecnico);
	List<TipoEquipo> buscarTodosTipoEquipo();
	List<Categoria> buscarTodasCategoria();
	List<Rango> buscarTodosRango();
	public List<Equipo> buscarEquiposVigentes();
    public List<Equipo> buscarEquiposFuturosYVigentes();
    public List<Equipo> buscarEquiposFuturosYVigentes(int nroPagina);
	public long totalEquiposActivos();
	public void Desactivar(Equipo equipo);
	public boolean verificarCerrar(Equipo equipo);
	
	public long totalEquiposFiltradosPorTecnico(String hql);
	public List<Equipo> buscarFiltradoPorTecnico(String hql, int numeroPagina);
	public long totalEquiposFiltradosPorDelegado(String hql);
	public List<Equipo> buscarFiltradoPorDelegado(String hql, int numeroPagina);
	
	//Agregado por Jesus
	public List<Equipo> buscarEquiposRosterConTecnico();

}