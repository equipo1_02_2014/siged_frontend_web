package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.List;
import java.util.Set;

import edu.ucla.siged.domain.gestionatleta.Reposo;

public interface ServicioReposoI {
	
	List<Reposo> listaReposos();
	void eliminarReposos(Set<Reposo> reposos);

}
