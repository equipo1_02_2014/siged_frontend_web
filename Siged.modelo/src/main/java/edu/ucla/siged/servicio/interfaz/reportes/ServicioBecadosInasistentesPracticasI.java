package edu.ucla.siged.servicio.interfaz.reportes;

import java.util.List;

import edu.ucla.siged.domain.reporte.BecadosInasistentesPracticas;

public interface ServicioBecadosInasistentesPracticasI {

	public List<BecadosInasistentesPracticas> buscarAsistencia(String restricciones);
	
}
