package edu.ucla.siged.servicio.interfaz.seguridad;

import java.util.List;
import java.util.Set;

import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;

public interface ServicioRolUsuarioI {
	
	public static final int TAMANO_PAGINA = 4;
	public List<RolUsuario> buscarTodos();
	public void eliminar(RolUsuario rolUsuarioSeleccionado);
	public void eliminarVarios(Set<RolUsuario> rolesUsuario);
	public void guardarListaRolUsuario(List<RolUsuario> roles);
	public List<RolUsuario> getRolesUsuario(Usuario usuario);
	public List<RolUsuario> getRolesUsuarioByRol(Rol rol);

}