package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoHorarioPracticaI;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoHorarioI;


@Service("servicioEquipoHorario")
public class ServicioEquipoHorario implements ServicioEquipoHorarioI{

	@Autowired private DaoHorarioPracticaI daoHorarioPractica;
	@Autowired private SessionFactory sessionFactory;
		
	public List<HorarioPractica> getHorariosEquipo(Equipo equipo) {
		return daoHorarioPractica.findByEquipo(equipo);
	}

	public void guardarListaHorarioEquipo(List<HorarioPractica> horarioPractica) {
		for(HorarioPractica horario:horarioPractica){
			daoHorarioPractica.save(horario);
		}		
	}

	public void eliminar(HorarioPractica horarioPractica) {
		daoHorarioPractica.delete(horarioPractica);
	}

}