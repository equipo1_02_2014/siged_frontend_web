package edu.ucla.siged.servicio.impl.gestionrecursostecnicos;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoAnotadorI;
import edu.ucla.siged.dao.DaoJuegoI;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAnotadorI;

@Service("servicioAnotador")
public class ServicioAnotador implements ServicioAnotadorI{
	@Autowired
	private DaoAnotadorI daoAnotador;
	@Autowired
	private SessionFactory sessionFactory;
	@Transactional
	public void guardar(Anotador anotador) {
		
		
		if (anotador.getId()==null){
			anotador.setEstatus((short) 1);
			daoAnotador.save(anotador);
		}
		else{
			anotador.setId(anotador.getId());
			daoAnotador.save(anotador);
		}
	}
	
	public Page<Anotador> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoAnotador.findAll(pagina);
	}
	
	public Anotador buscarPorId(Integer Id) {
		return daoAnotador.findOne(Id);
	}
	
	public List<Anotador> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Anotador where "+hql); 
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Anotador> lista =query.list(); 
		session.close();
		return lista;

	}
	
	public void eliminar(Anotador anotador) {
		daoAnotador.delete(anotador);
	}
	
	public int eliminarVarios(List<Anotador> anotadores) {
		int cont=0;
		for (int i=0;i<anotadores.size();i++){
			if (!existeEnJuego(anotadores.get(i))){
				daoAnotador.delete(anotadores.get(i));
				cont++;
			}
		}
		return cont;
	}
	
	public long totalAnotadores() {
		return daoAnotador.count();
	}

	public long totalAnotadoresFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Anotador where "+hql); 
		long total= (Long) query.uniqueResult(); 
		session.close();
		return total;

	}

	public boolean existeAnotador(String cedula, Integer idAnotador) {
		if (idAnotador == null){
			idAnotador=0;
		}
		Long cuantos = daoAnotador.countByCedula(cedula, idAnotador);
		if (cuantos > 0){
		   return true;
		}
		else {
			return false;
		}
	}

	public boolean existeEnJuego(Anotador anotador) {
		// TODO Auto-generated method stub
		Long cuantosAnotadores=daoAnotador.countByAnotador(anotador);
		if (cuantosAnotadores > 0){
			   return true;
			}
			else {
				return false;
			}
	}

	public Anotador buscarAnotadorPorCedula(String cedula) {
		// TODO Auto-generated method stub
		return daoAnotador.findByCedula(cedula);
	}
}