package edu.ucla.siged.servicio.impl.gestionatletas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoAtletaI;
import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.dao.DaoRepresentanteI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.AtletaRepresentante;
import edu.ucla.siged.domain.gestionatleta.Documento;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAyuda;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.domain.gestionatleta.Reposo;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioDocumentoI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstatusAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioReposoI;

@Service
@Controller
@Transactional
public class ServicioAtleta implements ServicioAtletaI {

	private @Autowired DaoAtletaI daoAtletaI;
	private @Autowired DaoEquipoI daoEquipoI;
	private @Autowired DaoRepresentanteI daoRepresentante;
	private @Autowired ServicioReposoI servicioReposo;
	private @Autowired ServicioDocumentoI servicioDocumento;
	private @Autowired ServicioEstatusAtletaI servicioEstatusAtleta;
	private @Autowired ServicioHistoriaAtletaI servicioHistoriaAtleta;
	
	@Autowired  // 29-01-15 por  Henri
	private SessionFactory sessionFactory;

	@SuppressWarnings("deprecation")
	public List<Atleta> obtenerListaAtleta(int numeroPagina) {
//		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA,
//				Sort.Direction.DESC, "fechaAdmision", "id", "cedula");
		Pageable pagina = new PageRequest(numeroPagina, TAMANO_PAGINA);
		List<Atleta> atletas = daoAtletaI.buscarAtletasEstatusNoEliminados(pagina).getContent();
		for (Atleta atleta : atletas) {

			Date fechaMax = new Date(0);
			for (HistoriaAtleta historiaAtleta : atleta.getHistoriaAtleta()) {
				if (historiaAtleta.getFecha().after(fechaMax)) {
					fechaMax = historiaAtleta.getFecha();
					atleta.setEstatusActual(historiaAtleta.getEstatusAtleta());
				}
			}

			for (AtletaRepresentante atletaRepresentante : atleta
					.getAtletaRepresentantes()) {
				// Inicializo cada representante y parentezco de cada atleta
				// representante
				Hibernate.initialize(atletaRepresentante.getRepresentante());
				Hibernate.initialize(atletaRepresentante.getRepresentante()
						.getEstadoCivil());
				Hibernate.initialize(atletaRepresentante.getParentesco());
			}

			for (Reposo reposo : atleta.getReposos()) {
				// Inicializamos Cada Documento de los Reposos
				Hibernate.initialize(reposo.getDocumento());
			}

			atleta.getDocumentos().size();

			// Retornar Equipo Actual
			// OJO ESTO DEBE ESTAR EN UN SERVICIO
			// PUEDE SER CAMBIADO POR UN FOR
			atleta.setEquipoActual(daoEquipoI.buscarEquipoActual(atleta.getId(), new Date()));
			

			// Determina si esta Becado o no
			Iterator<HistoriaAyuda> historiaAyudaIterator = atleta
					.getHistoriaAyudas().iterator();
			Date fechaHoy = new Date();
			long cantidadDeMesesBecados=0;
			while (historiaAyudaIterator.hasNext()) {
				HistoriaAyuda historiaAyuda = historiaAyudaIterator.next();
				if (historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear()){
					if(historiaAyuda.getFechaInicio().getMonth()<=fechaHoy.getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if(fechaHoy.getYear()==historiaAyuda.getFechaFin().getYear()) {
					if(fechaHoy.getMonth()<=historiaAyuda.getFechaFin().getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if(historiaAyuda.getFechaInicio().getYear()==historiaAyuda.getFechaFin().getYear()){
					if(historiaAyuda.getFechaInicio().getMonth()<=fechaHoy.getMonth() && fechaHoy.getMonth()<=historiaAyuda.getFechaFin().getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if (historiaAyuda.getFechaInicio().getYear() < fechaHoy.getYear() && fechaHoy.getYear() < historiaAyuda.getFechaFin().getYear()) {
					atleta.setBecado("BECADO");
				}
				
				if((fechaHoy.getYear()==historiaAyuda.getFechaFin().getYear() && fechaHoy.getMonth()==historiaAyuda.getFechaFin().getMonth()) || 
					 (historiaAyuda.getFechaFin().getYear()<fechaHoy.getYear()) || 
					 (historiaAyuda.getFechaFin().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaFin().getMonth()<fechaHoy.getMonth())){
					if(historiaAyuda.getFechaInicio().getYear()==historiaAyuda.getFechaFin().getYear()){
						cantidadDeMesesBecados += (historiaAyuda.getFechaFin().getMonth()+2) - (historiaAyuda.getFechaInicio().getMonth()+1);
					}else 
						if((historiaAyuda.getFechaFin().getYear()-historiaAyuda.getFechaInicio().getYear())==1){
						cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1)) +
												  (historiaAyuda.getFechaFin().getMonth()+1);
					}
					else{
						cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1)) +
												  (((historiaAyuda.getFechaFin().getYear()-historiaAyuda.getFechaInicio().getYear())-1)*12) +
												  (historiaAyuda.getFechaFin().getMonth()+1);
					}
				}else if((historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaInicio().getMonth()==fechaHoy.getMonth())||
					  (historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaInicio().getMonth()<fechaHoy.getMonth()) ||
					  (historiaAyuda.getFechaInicio().getYear()<fechaHoy.getYear())){
							cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1))+(((fechaHoy.getYear()-historiaAyuda.getFechaInicio().getYear())-1)*12)+(fechaHoy.getMonth()+1);
				}
				

				
			}
			
			
                long cantidadDeMesesPagos = 0;                                                                                                                                                                                                                                            				Iterator<Pago> pagoIterator = atleta.getPagos().iterator();
				while (pagoIterator.hasNext()) {
					Pago pago = pagoIterator.next();
					if(pago.getAnno()==fechaHoy.getYear()+1900){
						if (pago.getMes()<=fechaHoy.getMonth() + 1) {
							cantidadDeMesesPagos++;
						}
					}else if(pago.getAnno()<fechaHoy.getYear()+1900){
						cantidadDeMesesPagos++;
					}
					
				}
			
			
			
			
			long cantidadDeMeseMorosos = 0;
			if(atleta.getFechaAdmision().getYear()==fechaHoy.getYear()){
				cantidadDeMeseMorosos += (fechaHoy.getMonth()+2) - (atleta.getFechaAdmision().getMonth()+1) - cantidadDeMesesBecados - cantidadDeMesesPagos;
			}else 
				if((fechaHoy.getYear()-atleta.getFechaAdmision().getYear())==1){
				cantidadDeMeseMorosos += ((13-(atleta.getFechaAdmision().getMonth()+1)) +
										 (fechaHoy.getMonth()+1)) -
										 cantidadDeMesesBecados -
										 cantidadDeMesesPagos;
			}else{
				cantidadDeMeseMorosos += ((13-(atleta.getFechaAdmision().getMonth()+1)) +
										 (((fechaHoy.getYear()-atleta.getFechaAdmision().getYear())-1)*12) +
										 (fechaHoy.getMonth()+1)) -
										 cantidadDeMesesBecados -
										 cantidadDeMesesPagos;
			}
			if(cantidadDeMeseMorosos==0){
				atleta.setSolvente("SOLVENTE");
			}else{
				atleta.setSolvente("MOROSO");
			}

			
/*
			// Determina si esta solvente o no
			if (!atleta.getBecado().equalsIgnoreCase("BECADO")) {
				Iterator<Pago> pagoIterator = atleta.getPagos().iterator();
				while (pagoIterator.hasNext()) {
					Pago pago = pagoIterator.next();
					if (fechaHoy.getMonth() + 1 == pago.getMes()) {
						atleta.setSolvente("SOLVENTE");
					} else if (fechaHoy.getMonth() + 1 > pago.getMes()) {
						atleta.setSolvente("MOROSO");
					}
				}
			}*/
			
		}
		return atletas;
	}

	@SuppressWarnings("deprecation")
	public List<Atleta> obtenerListaAtleta(int numeroPagina, String cedula,String nombre, String apellido, String equipoActual, EstatusAtleta estatusAtleta) {

		List<Atleta> atletas = new ArrayList<Atleta>();
		

		if (!cedula.equalsIgnoreCase("") || !nombre.equalsIgnoreCase("") || !apellido.equalsIgnoreCase("") || !equipoActual.equalsIgnoreCase("")) {
			if(estatusAtleta!=null){
				atletas = atletasEstatusAtletaFiltrados(numeroPagina, cedula, nombre, apellido, equipoActual,estatusAtleta);
			}else{
				atletas = atletasfiltrados(numeroPagina, cedula, nombre, apellido, equipoActual);
			}
		} else if(estatusAtleta!=null){
			atletas = atletasEstatusAtletaFiltrados(numeroPagina, cedula, nombre, apellido, equipoActual,estatusAtleta);		
		} else {
			atletas = obtenerListaAtleta(numeroPagina);
		}
		for (Atleta atleta : atletas) {

			Date fechaMax = new Date(0);
			for (HistoriaAtleta historiaAtleta : atleta.getHistoriaAtleta()) {
				if (historiaAtleta.getFecha().after(fechaMax)) {
					fechaMax = historiaAtleta.getFecha();
					atleta.setEstatusActual(historiaAtleta.getEstatusAtleta());
				}
			}

			for (AtletaRepresentante atletaRepresentante : atleta
					.getAtletaRepresentantes()) {
				// Inicializo cada representante y parentezco de cada atleta
				// representante
				Hibernate.initialize(atletaRepresentante.getRepresentante());
				Hibernate.initialize(atletaRepresentante.getRepresentante()
						.getEstadoCivil());
				Hibernate.initialize(atletaRepresentante.getParentesco());
			}

			for (Reposo reposo : atleta.getReposos()) {
				// Inicializamos Cada Documento de los Reposos
				Hibernate.initialize(reposo.getDocumento());
			}

			atleta.getDocumentos().size();

			// Retornar Equipo Actual
			// OJO ESTO DEBE ESTAR EN UN SERVICIO
			// PUEDE SER CAMBIADO POR UN FOR
			atleta.setEquipoActual(daoEquipoI.buscarEquipoActual(atleta.getId(), new Date()));


			// Determina si esta Becado o no
			Iterator<HistoriaAyuda> historiaAyudaIterator = atleta
					.getHistoriaAyudas().iterator();
			Date fechaHoy = new Date();
			long cantidadDeMesesBecados=0;
			while (historiaAyudaIterator.hasNext()) {
				HistoriaAyuda historiaAyuda = historiaAyudaIterator.next();
				if (historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear()){
					if(historiaAyuda.getFechaInicio().getMonth()<=fechaHoy.getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if(fechaHoy.getYear()==historiaAyuda.getFechaFin().getYear()) {
					if(fechaHoy.getMonth()<=historiaAyuda.getFechaFin().getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if(historiaAyuda.getFechaInicio().getYear()==historiaAyuda.getFechaFin().getYear()){
					if(historiaAyuda.getFechaInicio().getMonth()<=fechaHoy.getMonth() && fechaHoy.getMonth()<=historiaAyuda.getFechaFin().getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if (historiaAyuda.getFechaInicio().getYear() < fechaHoy.getYear() && fechaHoy.getYear() < historiaAyuda.getFechaFin().getYear()) {
					atleta.setBecado("BECADO");
				}
				
				if((fechaHoy.getYear()==historiaAyuda.getFechaFin().getYear() && fechaHoy.getMonth()==historiaAyuda.getFechaFin().getMonth()) || 
					 (historiaAyuda.getFechaFin().getYear()<fechaHoy.getYear()) || 
					 (historiaAyuda.getFechaFin().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaFin().getMonth()<fechaHoy.getMonth())){
					if(historiaAyuda.getFechaInicio().getYear()==historiaAyuda.getFechaFin().getYear()){
						cantidadDeMesesBecados += (historiaAyuda.getFechaFin().getMonth()+2) - (historiaAyuda.getFechaInicio().getMonth()+1);
					}else 
						if((historiaAyuda.getFechaFin().getYear()-historiaAyuda.getFechaInicio().getYear())==1){
						cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1)) +
												  (historiaAyuda.getFechaFin().getMonth()+1);
					}
					else{
						cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1)) +
												  (((historiaAyuda.getFechaFin().getYear()-historiaAyuda.getFechaInicio().getYear())-1)*12) +
												  (historiaAyuda.getFechaFin().getMonth()+1);
					}
				}else if((historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaInicio().getMonth()==fechaHoy.getMonth())||
					  (historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaInicio().getMonth()<fechaHoy.getMonth()) ||
					  (historiaAyuda.getFechaInicio().getYear()<fechaHoy.getYear())){
							cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1))+(((fechaHoy.getYear()-historiaAyuda.getFechaInicio().getYear())-1)*12)+(fechaHoy.getMonth() +1);
				}
				

				
			}
			
			
                long cantidadDeMesesPagos = 0;                                                                                                                                                                                                                                            				Iterator<Pago> pagoIterator = atleta.getPagos().iterator();
				while (pagoIterator.hasNext()) {
					Pago pago = pagoIterator.next();
					if(pago.getAnno()==fechaHoy.getYear()+1900){
						if (pago.getMes()<=fechaHoy.getMonth() + 1) {
							cantidadDeMesesPagos++;
						}
					}else if(pago.getAnno()<fechaHoy.getYear()+1900){
						cantidadDeMesesPagos++;
					}
					
				}
			
			
			
			
			long cantidadDeMeseMorosos = 0;
			if(atleta.getFechaAdmision().getYear()==fechaHoy.getYear()){
				cantidadDeMeseMorosos += (fechaHoy.getMonth()+2) - (atleta.getFechaAdmision().getMonth()+1) - cantidadDeMesesBecados - cantidadDeMesesPagos;
			}else 
				if((fechaHoy.getYear()-atleta.getFechaAdmision().getYear())==1){
				cantidadDeMeseMorosos += ((13-(atleta.getFechaAdmision().getMonth()+1)) +
										 (fechaHoy.getMonth()+1)) -
										 cantidadDeMesesBecados -
										 cantidadDeMesesPagos;
			}else{
				cantidadDeMeseMorosos += ((13-(atleta.getFechaAdmision().getMonth()+1)) +
										 (((fechaHoy.getYear()-atleta.getFechaAdmision().getYear())-1)*12) +
										 (fechaHoy.getMonth()+1)) -
										 cantidadDeMesesBecados -
										 cantidadDeMesesPagos;
			}
			if(cantidadDeMeseMorosos==0){
				atleta.setSolvente("SOLVENTE");
			}else{
				atleta.setSolvente("MOROSO");
			}		
		}

		return atletas;
	}
	@Rollback
	public void guardarAtletaConEstatus(Atleta atleta, Set<Reposo> reposos,
			Set<Documento> documentos) {

		for (AtletaRepresentante atletaRepresentante : atleta
				.getAtletaRepresentantes()) {
			daoRepresentante.save(atletaRepresentante.getRepresentante());
		}
		if (reposos.size() != 0) {
			servicioReposo.eliminarReposos(reposos);
		}
		if (documentos.size() != 0) {
			servicioDocumento.eliminarDocumentos(documentos);
		}
		if (atleta.getHistoriaAtleta().size() != 1) {
			HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);
			for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){
				if(historiaAtleta.getId()==historiaAtletaActual.getId()){
					historiaAtleta.setEstatus((short) 0);
				}
			}
		}
		
		daoAtletaI.save(atleta);
	}
	@Rollback
	public void guardarAtleta(Atleta atleta, Set<Reposo> reposos,
			Set<Documento> documentos) {

		for (AtletaRepresentante atletaRepresentante : atleta
				.getAtletaRepresentantes()) {
			daoRepresentante.save(atletaRepresentante.getRepresentante());

		}
		if (reposos.size() != 0) {
			servicioReposo.eliminarReposos(reposos);
		}
		if (documentos.size() != 0) {
			servicioDocumento.eliminarDocumentos(documentos);
		}
		
		daoAtletaI.save(atleta);
	}

	// m�todo requerido por la gesti�n de Equipo
	public List<Atleta> obtenerAtletasValidos(Categoria categoria) {
		Calendar calendarActual = Calendar.getInstance();
		Calendar calendarFechaNacimiento = Calendar.getInstance();
		int edadMinima = categoria.getEdadMinima();
		int edadMaxima = categoria.getEdadMaxima();
		List<Atleta> atletas = daoAtletaI.findAll();
		List<Atleta> atletasSinAsignar = new ArrayList<Atleta>(atletas);
		for (Atleta atleta : atletas) {
			atleta.getHistoriaAtleta().size();
			Date fechaMax = new Date(0);
			for (HistoriaAtleta historiaAtleta : atleta.getHistoriaAtleta()) {
				if (historiaAtleta.getFecha().after(fechaMax)) {
					fechaMax = historiaAtleta.getFecha();
					atleta.setEstatusActual(historiaAtleta.getEstatusAtleta());
				}
			}
			if (atleta.getEstatusActual().getId() != 2) {
				atletasSinAsignar.remove(atleta);
			}
		}

		List<Atleta> atletasValidos = new ArrayList<Atleta>(atletasSinAsignar);

		for (Atleta atleta : atletasSinAsignar) {
			calendarFechaNacimiento.setTime(atleta.getFechaNacimiento());
			int edadAtleta = calendarActual.get(Calendar.YEAR)
					- calendarFechaNacimiento.get(Calendar.YEAR);

			if (calendarActual.get(Calendar.MONTH) < calendarFechaNacimiento
					.get(Calendar.MONTH)) {
				--edadAtleta;
			} else if (calendarActual.get(Calendar.MONTH) == calendarFechaNacimiento
					.get(Calendar.MONTH)) {
				if (calendarActual.get(Calendar.DAY_OF_MONTH) < calendarFechaNacimiento
						.get(Calendar.DAY_OF_MONTH)) {
					--edadAtleta;
				}
			}

			if (edadAtleta < edadMinima || edadAtleta > edadMaxima) {
				atletasValidos.remove(atleta);
			}
		}
		return atletasValidos;
	}

	// cambiar el estatus del atleta a activo (que ya esta asignado a un
	// equipos)
	public void editarEstatus(Atleta atleta) {
		HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);		
		for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){
			if(historiaAtleta.getId()==historiaAtletaActual.getId()){
				historiaAtleta.setEstatus((short) 0);
			}
		}
		daoAtletaI.save(atleta);
	}
	
	
	public long totalAtletas() {
		// TODO Auto-generated method stub
		//return daoAtletaI.count();
		return daoAtletaI.cantidadAtletasEstatusNoEliminado();
	}

	public void eliminarAtletaLogicamente(Atleta atleta) {
		HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);
		for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){
			if(historiaAtleta.getId()==historiaAtletaActual.getId()){
				historiaAtleta.setEstatus((short) 0);
			}
		}
		HistoriaAtleta historiaAtleta = new HistoriaAtleta(new Date(),
				(short) 1, servicioEstatusAtleta.buscarEstatusAtleta(0));
		atleta.addHistoriasAtletla(historiaAtleta);
		daoAtletaI.save(atleta);
	}

	public void eliminarAtletasLogicamente(List<Atleta> atletas) {
		for (Atleta atleta : atletas) {
			HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);
			for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){
				if(historiaAtleta.getId()==historiaAtletaActual.getId())
					historiaAtleta.setEstatus((short) 0);
			}
		}
		daoAtletaI.save(atletas);

	}

	public List<Atleta> atletasfiltrados(int numeroPagina, String cedula,
			String nombre, String apellido, String equipoActual) {

		Pageable pagina = new PageRequest(numeroPagina, TAMANO_PAGINA);
		if(!equipoActual.equalsIgnoreCase("")){
		return daoAtletaI.filtroAtleta(cedula, nombre, apellido, equipoActual, pagina)
				.getContent();
		}else{
			return daoAtletaI.filtroAtleta(cedula, nombre, apellido, pagina).getContent();
		}
			
	}

	public List<Atleta> atletasEstatusAtletaFiltrados(int numeroPagina,
			String cedula, String nombre, String apellido, String equipoActual,
			EstatusAtleta estatusAtleta) {
		Pageable pagina = new PageRequest(numeroPagina, TAMANO_PAGINA);
		
		if(!equipoActual.equalsIgnoreCase("")){
			return daoAtletaI.filtroAtletaMasEstatusAtleta(cedula, nombre,
					apellido, equipoActual ,estatusAtleta.getId(), pagina).getContent();
		}else{
			return daoAtletaI.filtroAtletaMasEstatusAtleta(cedula, nombre, apellido, estatusAtleta.getId(), pagina).getContent();
		}
		 
	}

	public long totalAtletasFiltrados(String cedula, String nombre,
			String apellido, String equipoActual, EstatusAtleta estatusAtleta) {
		long total = 0;
		if (!cedula.equalsIgnoreCase("") || !nombre.equalsIgnoreCase("") || !apellido.equalsIgnoreCase("")) {
			if (estatusAtleta != null) {
				if(!equipoActual.equalsIgnoreCase("")){
					total = daoAtletaI.filtroAtletaMasEstatusAtletaCantidad(cedula,	nombre, apellido, equipoActual,estatusAtleta.getId());
				}else{
					total = daoAtletaI.filtroAtletaMasEstatusAtletaCantidad(cedula, nombre, apellido, estatusAtleta.getId());
				}
				
			}else{
				if(!equipoActual.equalsIgnoreCase("")){
					total = daoAtletaI.FiltroAtletaCantidad(cedula, nombre, apellido, equipoActual);
				}else{
					total = daoAtletaI.FiltroAtletaCantidad(cedula, nombre, apellido);
				}
				
			}
		} else if(estatusAtleta!=null){
			if(!equipoActual.equalsIgnoreCase("")){
				total = daoAtletaI.filtroAtletaMasEstatusAtletaCantidad(cedula,	nombre, apellido, equipoActual,estatusAtleta.getId());
			}else{
				total = daoAtletaI.filtroAtletaMasEstatusAtletaCantidad(cedula, nombre, apellido, estatusAtleta.getId());
			}
			
		} else {
			total = totalAtletas();
		}

		return total;
	}
	

	@SuppressWarnings("deprecation")
	public List<Atleta> obtenerListaAtleta() {
		List<Atleta> atletas = daoAtletaI.findAll();
		for (Atleta atleta : atletas) {

			Date fechaMax = new Date(0);
			for (HistoriaAtleta historiaAtleta : atleta.getHistoriaAtleta()) {
				if (historiaAtleta.getFecha().after(fechaMax)) {
					fechaMax = historiaAtleta.getFecha();
					atleta.setEstatusActual(historiaAtleta.getEstatusAtleta());
				}
			}

			for (AtletaRepresentante atletaRepresentante : atleta
					.getAtletaRepresentantes()) {
				// Inicializo cada representante y parentezco de cada atleta
				// representante
				Hibernate.initialize(atletaRepresentante.getRepresentante());
				Hibernate.initialize(atletaRepresentante.getRepresentante()
						.getEstadoCivil());
				Hibernate.initialize(atletaRepresentante.getParentesco());
			}

			for (Reposo reposo : atleta.getReposos()) {
				// Inicializamos Cada Documento de los Reposos
				Hibernate.initialize(reposo.getDocumento());
			}

			atleta.getDocumentos().size();

			// Retornar Equipo Actual
			// OJO ESTO DEBE ESTAR EN UN SERVICIO
			// PUEDE SER CAMBIADO POR UN FOR
			atleta.setEquipoActual(daoEquipoI.buscarEquipoActual(atleta.getId(), new Date()));


			// Determina si esta Becado o no
			Iterator<HistoriaAyuda> historiaAyudaIterator = atleta
					.getHistoriaAyudas().iterator();
			Date fechaHoy = new Date();
			long cantidadDeMesesBecados=0;
			while (historiaAyudaIterator.hasNext()) {
				HistoriaAyuda historiaAyuda = historiaAyudaIterator.next();
				if (historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear()){
					if(historiaAyuda.getFechaInicio().getMonth()<=fechaHoy.getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if(fechaHoy.getYear()==historiaAyuda.getFechaFin().getYear()) {
					if(fechaHoy.getMonth()<=historiaAyuda.getFechaFin().getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if(historiaAyuda.getFechaInicio().getYear()==historiaAyuda.getFechaFin().getYear()){
					if(historiaAyuda.getFechaInicio().getMonth()<=fechaHoy.getMonth() && fechaHoy.getMonth()<=historiaAyuda.getFechaFin().getMonth()){
						atleta.setBecado("BECADO");
					}
				}else if (historiaAyuda.getFechaInicio().getYear() < fechaHoy.getYear() && fechaHoy.getYear() < historiaAyuda.getFechaFin().getYear()) {
					atleta.setBecado("BECADO");
				}
				
				if((fechaHoy.getYear()==historiaAyuda.getFechaFin().getYear() && fechaHoy.getMonth()==historiaAyuda.getFechaFin().getMonth()) || 
					 (historiaAyuda.getFechaFin().getYear()<fechaHoy.getYear()) || 
					 (historiaAyuda.getFechaFin().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaFin().getMonth()<fechaHoy.getMonth())){
					if(historiaAyuda.getFechaInicio().getYear()==historiaAyuda.getFechaFin().getYear()){
						cantidadDeMesesBecados += (historiaAyuda.getFechaFin().getMonth()+2) - (historiaAyuda.getFechaInicio().getMonth()+1);
					}else 
						if((historiaAyuda.getFechaFin().getYear()-historiaAyuda.getFechaInicio().getYear())==1){
						cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1)) +
												  (historiaAyuda.getFechaFin().getMonth()+1);
					}
					else{
						cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1)) +
												  (((historiaAyuda.getFechaFin().getYear()-historiaAyuda.getFechaInicio().getYear())-1)*12) +
												  (historiaAyuda.getFechaFin().getMonth()+1);
					}
				}else if((historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaInicio().getMonth()==fechaHoy.getMonth())||
					  (historiaAyuda.getFechaInicio().getYear()==fechaHoy.getYear() && historiaAyuda.getFechaInicio().getMonth()<fechaHoy.getMonth()) ||
					  (historiaAyuda.getFechaInicio().getYear()<fechaHoy.getYear())){
							cantidadDeMesesBecados += (13-(historiaAyuda.getFechaInicio().getMonth()+1))+(((fechaHoy.getYear()-historiaAyuda.getFechaInicio().getYear())-1)*12)+(fechaHoy.getMonth()+1);
				}
				

				
			}
			
			
                long cantidadDeMesesPagos = 0;                                                                                                                                                                                                                                            				Iterator<Pago> pagoIterator = atleta.getPagos().iterator();
				while (pagoIterator.hasNext()) {
					Pago pago = pagoIterator.next();
					if(pago.getAnno()==fechaHoy.getYear()+1900){
						if (pago.getMes()<=fechaHoy.getMonth() + 1) {
							cantidadDeMesesPagos++;
						}
					}else if(pago.getAnno()<fechaHoy.getYear()+1900){
						cantidadDeMesesPagos++;
					}
					
				}
			
			
			
			
			long cantidadDeMeseMorosos = 0;
			if(atleta.getFechaAdmision().getYear()==fechaHoy.getYear()){
				cantidadDeMeseMorosos += (fechaHoy.getMonth()+2) - (atleta.getFechaAdmision().getMonth()+1) - cantidadDeMesesBecados - cantidadDeMesesPagos;
			}else 
				if((fechaHoy.getYear()-atleta.getFechaAdmision().getYear())==1){
				cantidadDeMeseMorosos += ((13-(atleta.getFechaAdmision().getMonth()+1)) +
										 (fechaHoy.getMonth()+1)) -
										 cantidadDeMesesBecados -
										 cantidadDeMesesPagos;
			}else{
				cantidadDeMeseMorosos += ((13-(atleta.getFechaAdmision().getMonth()+1)) +
										 (((fechaHoy.getYear()-atleta.getFechaAdmision().getYear())-1)*12) +
										 (fechaHoy.getMonth()+1)) -
										 cantidadDeMesesBecados -
										 cantidadDeMesesPagos;
			}
			if(cantidadDeMeseMorosos==0){
				atleta.setSolvente("SOLVENTE");
			}else{
				atleta.setSolvente("MOROSO");
			}
		}
		return atletas;
	}
	
	public Atleta buscarAtletaPorCedula(String cedula){
		return daoAtletaI.findByCedula(cedula);
	}

	
//  29-01-15 por  Henri
//****************************************
	
	//  atletas sin beca
	public List<Atleta> ListarAtletasSinBeca(int numeroPagina, int tamanoPagina) {
	Session session = sessionFactory.openSession();
	Query query=session.createQuery("select DISTINCT A from Atleta A left join A.historiaAyudas HA where (HA is null or HA.fechaFin <= current_date) and (A.id in (select X.id from Atleta X left join X.historiaAtleta HX where (HX.estatus = 1 and HX.estatusAtleta.id = 1 or HX.estatusAtleta.id = 2))) and (A.id not in (select Y.id from Atleta Y left join Y.historiaAyudas HY where(HY.fechaFin >= current_date)))");
	query.setMaxResults(tamanoPagina);
	query.setFirstResult(numeroPagina * tamanoPagina);
	List<Atleta> atletas = query.list();
		for(Atleta atleta:atletas){
			atleta.getHistoriaAyudas().size();
		}
	session.close();
	return atletas;
	}
	
	//  atletas sin beca filtrados
	public List<Atleta> ListarAtletasFiltradosSinBeca(String hql, int numeroPagina, int tamanoPagina) {
	Session session = sessionFactory.openSession();
	Query query=session.createQuery("select DISTINCT A from Atleta A left join A.historiaAyudas HA where (HA is null or HA.fechaFin <= current_date) and (A.id in (select X.id from Atleta X left join X.historiaAtleta HX where (HX.estatus = 1 and HX.estatusAtleta.id = 1 or HX.estatusAtleta.id = 2))) and (A.id not in (select Y.id from Atleta Y left join Y.historiaAyudas HY where(HY.fechaFin >= current_date))) and " + hql);
	query.setMaxResults(tamanoPagina);
	query.setFirstResult(numeroPagina * tamanoPagina);
	List<Atleta> atletas = query.list();
		for(Atleta atleta:atletas){
			atleta.getHistoriaAyudas().size();
		}
	session.close();
	return atletas;
	}
	
	//  nro. de atletas sin beca
	public long AtletasSinBeca(){
	Session session = sessionFactory.openSession();
	Query query=session.createQuery("select count(DISTINCT A) from Atleta A left join A.historiaAyudas HA where (HA is null or HA.fechaFin <= current_date) and (A.id in (select X.id from Atleta X left join X.historiaAtleta HX where (HX.estatus = 1 and HX.estatusAtleta.id = 1 or HX.estatusAtleta.id = 2))) and (A.id not in (select Y.id from Atleta Y left join Y.historiaAyudas HY where(HY.fechaFin >= current_date)))");
	long total= (Long) query.uniqueResult();
	session.close();
	return total;
	}
	
	//  nro. de atletas filtrados sin beca
	public long AtletasFiltradosSinBeca(String hql){
	Session session = sessionFactory.openSession();
	Query query=session.createQuery("select count(DISTINCT A) from Atleta A left join A.historiaAyudas HA where (HA is null or HA.fechaFin <= current_date) and (A.id in (select X.id from Atleta X left join X.historiaAtleta HX where (HX.estatus = 1 and HX.estatusAtleta.id = 1 or HX.estatusAtleta.id = 2))) and (A.id not in (select Y.id from Atleta Y left join Y.historiaAyudas HY where(HY.fechaFin >= current_date))) and " + hql);
	long total= (Long) query.uniqueResult();
	session.close();
	return total;
	}
	
	public void Guardar(Atleta a){
	daoAtletaI.save(a);
	}

	
	
	
	// asignarun  atleta a un equipo
	public void estatusAsignarAtletaEquipo(Atleta atleta) {
		HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);
		for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){
			if(historiaAtleta.getId().equals(historiaAtletaActual.getId())){
				historiaAtleta.setEstatus((short) 0);
		    }
		}		
				
		HistoriaAtleta historiaAtleta = new HistoriaAtleta(new Date(), (short) 1, servicioEstatusAtleta.buscarEstatusAtleta(1));
		atleta.addHistoriasAtletla(historiaAtleta);
		daoAtletaI.save(atleta);

		
	}
	
	// retirar un atleta de un equipo
	public void estatusRetirarAtletaEquipo(Atleta atleta) {	
		HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);
		for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){
			if(historiaAtleta.getId().equals(historiaAtletaActual.getId())){
				historiaAtleta.setEstatus((short) 0);
		     }
		}		
				
		HistoriaAtleta historiaAtleta = new HistoriaAtleta(new Date(), (short) 1, servicioEstatusAtleta.buscarEstatusAtleta(2));
		atleta.addHistoriasAtletla(historiaAtleta);
		daoAtletaI.save(atleta);

	}

	public Atleta buscarAtletaPorID(Integer id) {
		// TODO Auto-generated method stub
		return daoAtletaI.findOne(id);
	}
	
}
