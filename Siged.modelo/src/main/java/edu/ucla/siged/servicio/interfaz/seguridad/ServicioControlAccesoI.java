package edu.ucla.siged.servicio.interfaz.seguridad;

import java.util.List;
import java.util.Set;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Rol;

public interface ServicioControlAccesoI {
	
	public static final int TAMANO_PAGINA = 4;
	public List<ControlAcceso> buscarTodos();
	public void eliminar(ControlAcceso funcionalidadRolSeleccionado);
	public void eliminarVarios(List<ControlAcceso> funcionalidadesRol);
	public void guardarListaFuncionalidadRol(List<ControlAcceso> funcionalidades);
	public List<ControlAcceso> getFuncionalidadesRol(Rol rol);
}
