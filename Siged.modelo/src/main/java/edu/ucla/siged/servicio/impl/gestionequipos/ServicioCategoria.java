package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoCategoriaI;
import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.utilidades.Persona;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;

@Service("servicioCategoria")
public class ServicioCategoria implements ServicioCategoriaI {

	@Autowired
	private DaoCategoriaI daoCategoria;
	@Autowired
	private DaoEquipoI daoEquipo;
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Categoria> buscarTodos() {	
		return daoCategoria.findAll(); 
	}
	
	public void guardar(Categoria categoria) {
		if (categoria.getId() ==null){
			daoCategoria.save(categoria);
		}
		else{
			categoria.setId(categoria.getId());
			daoCategoria.save(categoria);
		}
	}
	
	public void eliminarCategoria(Categoria categoria) {
		categoria.setEstatus( Short.parseShort("0"));
		daoCategoria.delete( categoria ); 
	}
	
	public void eliminarCategorias(List<Categoria> categorias) {
		for(Categoria categoria:categorias){
			categoria.setEstatus(Short.parseShort("0") );
		}
		daoCategoria.delete(categorias);
	}
	
	public Page<Categoria> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoCategoria.findAll(pagina);
	}
	
	public Categoria buscarPorId(Integer Id) {
		return daoCategoria.findOne(Id);
	}
	
	public List<Categoria> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Categoria where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Categoria> lista =query.list();
		session.close();
		return lista;
		}

	 public long totalCategorias() {
		 return daoCategoria.count();
	 }

	 public long totalCategoriasFiltradas(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Categoria where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	 }
	 
	//Agregado Por Jesus
		public Categoria obtenerCategoria(Persona persona){
			
			
			Calendar fechaActual= Calendar.getInstance();
			
			Calendar fechaNacimiento= Calendar.getInstance();
			
			fechaNacimiento.setTime(persona.getFechaNacimiento());
			
			Integer anios= fechaActual.get(Calendar.YEAR) - fechaNacimiento.get(Calendar.YEAR);
			
			if(fechaActual.get(Calendar.MONTH)<fechaNacimiento.get(Calendar.MONTH)){
		       --anios;		
			}else if((fechaActual.get(Calendar.MONTH)==fechaNacimiento.get(Calendar.MONTH)) &&
					(fechaActual.get(Calendar.DAY_OF_MONTH)<fechaNacimiento.get(Calendar.DAY_OF_MONTH))){
				--anios;
			}
			
			
			Categoria categoria= daoCategoria.buscarCategoriaPorEdad(anios);
			
			
			return categoria;
		}

		public List<Categoria> buscarCategoriasActivas() {
			List<Categoria> lista= daoCategoria.findByEstatus(Short.parseShort("1"));
			
			return lista;
		}
}