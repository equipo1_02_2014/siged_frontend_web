package edu.ucla.siged.servicio.interfaz.gestioneventos;

import java.util.List;
import java.util.Set;
import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;

public interface ServicioAdjuntoNoticiaI {
	
	public void Guardar(AdjuntoNoticia an);
	public void Actualizar(AdjuntoNoticia an);
	public void Eliminar(Integer id);
	public void EliminarVarios(Set<AdjuntoNoticia> adjuntos);
	public List<AdjuntoNoticia> ListarAdjuntosNoticia();
}