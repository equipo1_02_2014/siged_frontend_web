package edu.ucla.siged.servicio.interfaz.gestiondeportiva;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.domain.gestionequipo.Equipo;

public interface ServicioCompetenciaI {
	
    public void guardar(Competencia competencia);
    public void editar(Competencia competencia);
    public List<Competencia> buscarTodos();
    public List<Competencia> buscarCompetenciasVigentes();
    public List<Competencia> buscarCompetenciasFuturasYVigentes();
    public List<Competencia> buscarCompetenciasFuturasYVigentes(int nroPagina);
    public long obtenerCantidadCompetenciasFuturasYVigentes();
    public int getTamanioPagina();
    public Set<Juego> buscarJuegosPorCompetencia(Competencia competencia);
	public Page<Competencia> buscarTodos(int numeroPagina);
	public Competencia buscarPorId(Integer Id);
	public List<Competencia> buscarFiltrado(String hql,int numeroPagina);
	public long totalCompetencias();
	public long totalCompetenciasFiltrados(String hql);
	List<TipoCompetencia> buscarTodosTipoCompetencia();
	public Page<Competencia> buscarCompetenciasActivas(int numeroPagina);
	public long totalCompetenciasActivas();
	public void Desactivar(Competencia competencia);
	public boolean existeCompetenciaPorEquipo(Equipo equipo);
}