package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.ucla.siged.dao.DaoAtletaEquipoI;
import edu.ucla.siged.dao.DaoEstatusAtletaI;
import edu.ucla.siged.dao.DaoHistoriaAtletaI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;

@Service("servicioAtletaEquipo")
public class ServicioAtletaEquipo implements ServicioAtletaEquipoI {

	@Autowired DaoAtletaEquipoI daoAtletaEquipo;
	@Autowired DaoEstatusAtletaI daoEstatusAtleta;
	@Autowired DaoHistoriaAtletaI daoHistoriaAtleta;
	@Autowired ServicioHistoriaAtletaI servicioHistoriaAtleta;

	public List<AtletaEquipo> obtenerListaAtletaEquipo(Equipo equipo) { 
		return daoAtletaEquipo.findByEquipo(equipo); 
	}

	public void guardarAtletaEquipo(AtletaEquipo atletaEquipo) {
		daoAtletaEquipo.save(atletaEquipo);

	}

	public void eliminarAtletaEquipo(Atleta atleta){
		Integer idAtletaEquipo = daoAtletaEquipo.obtenerIdAtletaEquipo(atleta);
		daoAtletaEquipo.delete(idAtletaEquipo);
	}

	@Transactional
	public void revocarAtletasEquipoCerrado(Equipo equipo){		
		List <Atleta> listaAtletaEquipo =  new ArrayList<Atleta>();
				
		// me traigo los atletas que est�n en el equipo donde voy a cerrar
		listaAtletaEquipo = daoAtletaEquipo.obtenerAtletasEquipo(equipo);
		
		
		// cambio el estatus de la historiaAtleta a 0
		for(Atleta atleta : listaAtletaEquipo){								
			
			//coloco en 0 la historia vieja
			if (atleta.getHistoriaAtleta().size() != 0) {
				HistoriaAtleta historiaAtletaActual = servicioHistoriaAtleta.buscarUltimaHistoriaAtleta(atleta);				
				for(HistoriaAtleta historiaAtleta: atleta.getHistoriaAtleta()){										
					if(historiaAtleta.getId()==historiaAtletaActual.getId()){
						historiaAtleta.setEstatus((short) 0);
					}
				}
			}
		
			
			
			// creamos una nueva historia con el estatus en 2 (Sin equipo)
			HistoriaAtleta historiaAtleta = new HistoriaAtleta();
			Date fecha = new Date();
			EstatusAtleta estatusAtleta = daoEstatusAtleta.findOne(2); // hace lo mismo que -> servicioEstatusAtleta.buscarEstatusAtleta(2)
			historiaAtleta.setEstatus(Short.parseShort("1"));
			historiaAtleta.setFecha(fecha);
			historiaAtleta.setAtleta(atleta);
			historiaAtleta.setEstatusAtleta(estatusAtleta);
			daoHistoriaAtleta.save(historiaAtleta);


			// elimino al atleta fisicamente de atletaequipo	
			AtletaEquipo atletaEquipo = daoAtletaEquipo.obtenerAtletaEquipo(atleta, equipo);
			if(atletaEquipo != null){
				try{
					System.out.println("------------------------------------------------ atletaEquipo: " + atletaEquipo.getId());
					//daoAtletaEquipo.delete(atletaEquipo);
					daoAtletaEquipo.delete(atletaEquipo.getId());
				}catch(Exception e){
					e.printStackTrace();
				}				
			}
			
			
		}
	}
	
	
	
}
