 package edu.ucla.siged.servicio.impl.seguridad;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoControlAccesoI;
import edu.ucla.siged.dao.DaoRolI;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;


@Service("servicioRol")
public class ServicioRol implements ServicioRolI{

	@Autowired private DaoRolI daoRol;
	@Autowired private DaoControlAccesoI daoControlAcceso;
	
	@Autowired private SessionFactory sessionFactory;
	
	public List<Rol> buscarTodos() {
		return daoRol.findAll();
	
	}
	public List<Rol> buscarActivos() {
		return daoRol.findByEstatus((short)1);
	
	}
	
	public void guardar(Rol rol) {
		// TODO Auto-generated method stub
		daoRol.save(rol);
	}
	
	public void eliminar(Rol rol) throws Exception {
		// TODO Auto-generated method stub
		
		List<ControlAcceso> list=daoControlAcceso.findByRol(rol);
		Session session = sessionFactory.openSession(); 		
		session.beginTransaction();
		try{
			Query query1 = session.createQuery("select count(*) from RolUsuario where idrol="+rol.getId());
			int count = query1.executeUpdate();
			if(count!=0){
				throw new Exception("Existen Usuarios con ese Rol");
			}
			Query q;
			for (ControlAcceso control : list) {
				//session.delete(control);
				q=session.createQuery("delete from ControlAcceso where id="+control.getId()); 	
				q.executeUpdate();
			}
			
			//session.delete(rol);
			Query query = session.createQuery("delete from Rol where id="+rol.getId());
			int deletedCount = query.executeUpdate();
			session.getTransaction().commit();
	
		}
		catch(Exception e){
			session.getTransaction().rollback();	
			System.out.println("Errooooooooooooooooor:"+e.getCause());
			throw new Exception("No se pudo eliminar el rol");
			
		}
		
		
		session.close();
		
	}

	public void eliminarVarios(List<Rol> listaRoles) throws Exception {
		Session session = sessionFactory.openSession(); 
		session.beginTransaction();
		try{
		Query q;
		for (Rol rol : listaRoles) {
			
			List<ControlAcceso> list=daoControlAcceso.findByRol(rol);
			for (ControlAcceso control : list) {
				//session.delete(control);
				q=session.createQuery("delete from ControlAcceso where id="+control.getId()); 	
				q.executeUpdate();
			}
			
			//session.delete(rol);
			Query query = session.createQuery("delete from Rol where id="+rol.getId());
			int deletedCount = query.executeUpdate();
			session.getTransaction().commit();
		}
		}
		catch(Exception e){
			session.getTransaction().rollback();	
			throw new Exception("No se pudo eliminar el rol");
		}
		
		session.close();
	}
	
	
	public void eliminarLogico(Rol rol) {
		// TODO Auto-generated method stub
		rol.setEstatus((short) 0);
		daoRol.save(rol);
	}

	public void eliminarVariosLogico(List<Rol> roles) {
		// TODO Auto-generated method stub
		for (int i=0;i<roles.size();i++)
			roles.get(i).setEstatus(Short.parseShort("0"));
			
		daoRol.save(roles);
	}
	
	public Page<Rol> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoRol.findAll(pagina);
	}
	
	public List<Rol> buscarFiltrado(String hql, int numeroPagina) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Rol where "+hql); 
		System.out.println("from Rol where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Rol> lista =query.list(); 
		session.close();
		return lista;
		
		
		}

	public long totalRoles() {
		// TODO Auto-generated method stub
		return daoRol.count();
	}

	public long totalRolesFiltrados(String hql) {
		
			// TODO Auto-generated method stub
			Session session = sessionFactory.openSession(); 
			Query query=session.createQuery("select count(*) from Rol where "+hql); 
			long total= (Long) query.uniqueResult(); 
			session.close();
			return total;

		

	}
	
	public Rol buscarRolByNombre(String nombre){
		return daoRol.findByNombre(nombre);
	}


}