package edu.ucla.siged.servicio.interfaz.gestionatletas;

import java.util.Date;
import java.util.List;

import edu.ucla.siged.domain.gestionatleta.Pago;

public interface ServicioPagoI {

	public static final int TAMANO_PAGINA = 9;
	public List<Pago> obtenerListaPago(int numeroPagina);
	public long totalPago();
	public List<Pago> obtenerListaPago(int numeroPagina,Date fechaPago, String numeroRecibo, String nombreAtleta, String apellidoAtleta);
	public long totalpago(int numeroPagina,Date fechaPago, String numeroRecibo, String nombreAtleta, String apellidoAtleta);
	public void eliminarPago(Pago pago);
	public void eliminarPagos(List<Pago> pagos);
	
	public void guardarPago(Pago pago);
	public boolean buscarPago(String numRecibo);
	
	public boolean buscarPagoPorMesYAnno(int id, Integer mes, Long anno);
}
