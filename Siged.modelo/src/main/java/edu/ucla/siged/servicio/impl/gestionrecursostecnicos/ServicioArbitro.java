/** 
 * 	ServicioArbitro
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.servicio.impl.gestionrecursostecnicos;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.ucla.siged.dao.DaoArbitroI;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioArbitroI;

@Service("servicioArbitro")
public class ServicioArbitro implements ServicioArbitroI{
	@Autowired
	private DaoArbitroI daoArbitro;
	@Autowired
	private SessionFactory sessionFactory;
	@Transactional
	public void guardar(Arbitro arbitro) {
		
		if (arbitro.getId()==null){	
			arbitro.setEstatus((short) 1);
			daoArbitro.save(arbitro);
		}
		else{
			arbitro.setId(arbitro.getId());
			daoArbitro.save(arbitro);
		}
	}

	public Page<Arbitro> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoArbitro.findAll(pagina);
	}

	public Arbitro buscarPorId(Integer Id) {
		return daoArbitro.findOne(Id);
	}

	public List<Arbitro> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from Arbitro where "+hql); 
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<Arbitro> lista =query.list();
		session.close();
		return lista;
	}
	
	public void eliminar(Arbitro arbitro) {
		daoArbitro.delete(arbitro);
	}
	
	public int eliminarVarios(List<Arbitro> arbitros) {
		int contador=0;
		
		for (int i=0;i<arbitros.size();i++)	
			if (!existeEnJuego(arbitros.get(i))){
			daoArbitro.delete(arbitros.get(i));
			contador++;
			}
		return contador;
	}

	public long totalArbitros() {
		return daoArbitro.count();
	}

	public long totalArbitrosFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from Arbitro where "+hql); 
		long total= (Long) query.uniqueResult(); 
		session.close();
		return total;		
	}
	
	public boolean existeArbitro(String cedula, Integer idArbitro) {	
		
		if (idArbitro == null){
			idArbitro = 0; 
		}

		Long cuantos = daoArbitro.countByCedula(cedula, idArbitro);
		if (cuantos > 0){
		   return true;
		}
		else {
			return false;
		}
	}

	public boolean existeEnJuego(Arbitro arbitro) {
		Long cuantos = daoArbitro.countByArbitroEnJuego(arbitro);
		if (cuantos > 0){
		   return true;
		}
		else {
			return false;
		}		
	}
}