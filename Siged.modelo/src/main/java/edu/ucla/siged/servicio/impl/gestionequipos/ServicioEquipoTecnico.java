package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.dao.DaoEquipoTecnicoI;
import edu.ucla.siged.dao.DaoTecnicoI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoTecnicoI;

@Service("servicioEquipoTecnico")
public class ServicioEquipoTecnico implements ServicioEquipoTecnicoI {
	
	@Autowired private DaoEquipoI daoEquipo;
	@Autowired private DaoTecnicoI daoTecnico;
	@Autowired private DaoEquipoTecnicoI daoEquipoTecnico;
	
	@Autowired
	ServicioAtletaEquipoI servicioAtletaEquipoI;
	private SessionFactory sessionFactory;
	
	
	public void guardarEquipo( Equipo equipo ) { daoEquipo.save( equipo ); }
	
	public List<Equipo> buscarTodos() { return daoEquipo.findAll();	 }
	
	public void eliminarEquipo( Equipo equipo ) {
		equipo.setEstatus( Short.parseShort("0"));		
		daoEquipo.save(equipo);
	}
	
	public void eliminarEquipos( List<Equipo> equipos ){
		for(Equipo equipo:equipos){
			equipo.setEstatus( Short.parseShort("0"));
		}
		daoEquipo.save( equipos );
	}
	
	public List<AtletaEquipo> atletaPorEquipo(Equipo equipo){
		return servicioAtletaEquipoI.obtenerListaAtletaEquipo( equipo );
	}

	public List<Atleta> obtenerAtletasPorEquipo(Equipo equipo) {
		List<AtletaEquipo> set= servicioAtletaEquipoI.obtenerListaAtletaEquipo(equipo);
		List<Atleta> listaAtleta= new ArrayList<Atleta>();
		for (AtletaEquipo aq: set){
			listaAtleta.add(aq.getAtleta());
		}
		return listaAtleta;
	}

	
	
	
	public Page<EquipoTecnico> buscarEquipoTecnicosTrue(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANIO_PAGINA, Sort.Direction.DESC, "id" );
		return daoEquipoTecnico.buscarEquipoTecnicosTrue(Short.parseShort("1"), pagina);
	}

	public Tecnico obtenerTecnicoPrincipal(Integer id) {
		Tecnico tecnico = daoEquipoTecnico.obtenerTecnicoDao(id, Short.parseShort("1"));
		if (tecnico == null){
			return null;
		}else{
			return tecnico;	
		}
	}
	
	 public Tecnico obtenerTecnicoAsistente(Integer id){
		Tecnico tecnico = daoEquipoTecnico.obtenerTecnicoDao(id, Short.parseShort("2"));
		if(tecnico == null){
			return null;
		}else{
			return tecnico;
		}
	 }
	
	 public void guardarEquipoTecnico(EquipoTecnico equipoTecnico){ daoEquipoTecnico.save(equipoTecnico); }
	 
	 public List<Tecnico> buscarTecnicosDisponibles(){  return daoTecnico.buscarTecnicosDisponiblesDao(); }
	 
//	 public EquipoTecnico buscarTecnicoEquipo(Equipo equipo){ 
//		 //return daoEquipoTecnico.buscarTecnicoEquipo(equipo);
//		 return null;
//		 
//	 }
	 
	 public void revocarTecnicosEquipoCerrado(Equipo equipo){
		 List <Tecnico> listaTecnicoEquipo = new ArrayList<Tecnico>();

		 listaTecnicoEquipo = daoEquipoTecnico.obtenerTecnicosEquipo(equipo);		 		 
		 
		 for(Tecnico tecnico : listaTecnicoEquipo){			 
			 EquipoTecnico equipoTecnico = daoEquipoTecnico.obtenerTecnicoEquipo(tecnico, equipo);			 
			 daoEquipoTecnico.delete(equipoTecnico);
		 }		 
	 }
	 
}