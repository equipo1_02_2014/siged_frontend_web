package edu.ucla.siged.servicio.impl.gestionequipos;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.dao.DaoTipoEquipoI;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioTipoEquipoI;

@Service("servicioTipoEquipo")
public class ServicioTipoEquipo implements ServicioTipoEquipoI {

	@Autowired 
	private DaoTipoEquipoI daoTipoEquipo;
	@Autowired 
	private SessionFactory sessionFactory;
	@Autowired 
	private DaoEquipoI daoEquipo;
	
	public List<TipoEquipo> buscarTodos(){	
		return daoTipoEquipo.findAll(); 
	}
	
	public void guardar(TipoEquipo tipoequipo){
		if (tipoequipo.getId()==null){
			daoTipoEquipo.save(tipoequipo);
		}
		else{
			tipoequipo.setId(tipoequipo.getId());
			daoTipoEquipo.save(tipoequipo);
		}
	}

	public void eliminarTipoEquipo(TipoEquipo tipoequipo) {
		tipoequipo.setEstatus( Short.parseShort("0"));
		daoTipoEquipo.delete( tipoequipo ); 
	}

	public void eliminarTipoEquipos(List<TipoEquipo> tipoequipos) {
		for(TipoEquipo tipoequipo:tipoequipos){
			tipoequipo.setEstatus( Short.parseShort("0") );
		}
		daoTipoEquipo.delete(tipoequipos);
	}

	public Page<TipoEquipo> buscarTodos(int numeroPagina) {
		PageRequest pagina = new PageRequest(numeroPagina, TAMANO_PAGINA, Sort.Direction.ASC,"id");
		return daoTipoEquipo.findAll(pagina);
	}

	public TipoEquipo buscarPorId(Integer Id) {
		return daoTipoEquipo.findOne(Id);
	}

	public List<TipoEquipo> buscarFiltrado(String hql, int numeroPagina) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("from TipoEquipo where "+hql);
		query.setMaxResults(TAMANO_PAGINA);
		query.setFirstResult(numeroPagina * TAMANO_PAGINA);
		List<TipoEquipo> lista =query.list();
		session.close();
		return lista;
	}

	public long totalTipoEquipos() {
		return daoTipoEquipo.count();
	}

	public long totalTipoEquiposFiltrados(String hql) {
		Session session = sessionFactory.openSession(); 
		Query query=session.createQuery("select count(*) from TipoEquipo where "+hql);
		long total= (Long) query.uniqueResult();
		session.close();
		return total;
	}
}