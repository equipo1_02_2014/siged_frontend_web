package edu.ucla.siged.test;

//import modelo.impl.IEquipoDao;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucla.siged.dao.DaoAtletaI;
import edu.ucla.siged.dao.DaoEstadoCivilI;
import edu.ucla.siged.dao.DaoEstatusAtletaI;
import edu.ucla.siged.dao.DaoRepresentanteI;
import edu.ucla.siged.servicio.impl.gestionatletas.ServicioAtleta;









public class ApplicationContextProvider implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/BDConfig/application-context.xml");
  
	public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {ApplicationContextProvider.applicationContext = applicationContext;
    }
  
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
    
    public static ServicioAtleta getServicioAtleta(){
    	return applicationContext.getBean(ServicioAtleta.class);
    }
    public static DaoAtletaI getDaoAtletaI(){
    	return applicationContext.getBean(DaoAtletaI.class);
    }
    public static DaoEstatusAtletaI getDaoEstatusAtletaI(){
    	return applicationContext.getBean(DaoEstatusAtletaI.class);
    }
    public static DaoRepresentanteI getDaoRepresentanteI(){
    	return applicationContext.getBean(DaoRepresentanteI.class);
    }
    public static DaoEstadoCivilI getDaoEstadoCivil(){
    	return applicationContext.getBean(DaoEstadoCivilI.class);
    }
    
    /*
    public static IEquipoDao getIEquipoDao(){
    	return applicationContext.getBean(IEquipoDao.class);
    }
    
    public static IEntrenadorDao getIEntrenadorDao(){
    	return applicationContext.getBean(IEntrenadorDao.class);
    }*/
    /*
     public static ServicioEquipo getServicioEquipo() {
   		return applicationContext.getBean(ServicioEquipo.class);
   	}
     
     public static ServicioEntrenador getServicioEntrenador() {
    		return applicationContext.getBean(ServicioEntrenador.class);
     }*/


}