package edu.ucla.siged.servicio.impl.gestionequipos.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.ucla.siged.dao.DaoPracticaI;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.utilidades.Paginacion;

@Controller
@RequestMapping(value="/practicas")
public class ServicioPracticasRest {
	
	@Autowired
	private DaoPracticaI iPractica;
	
	MutableSortDefinition sort = new MutableSortDefinition("fecha",true,true);
	MutableSortDefinition sort_desc = new MutableSortDefinition("fecha",true,false);
	
	@RequestMapping(value="/todos")
	public @ResponseBody List<Object> todos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
										    @RequestParam(value="limit", required=true) Integer limit){
	
		return Paginacion.paginar(Practica.class, iPractica.findAll(), sort, page, limit);
		
	}
	
	@RequestMapping(value="/porRango")
	public @ResponseBody List<Object> porRango(@RequestParam(value="desde", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date desde,
											    @RequestParam(value="hasta", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date hasta,
											    @RequestParam(value="page", required=true, defaultValue="0") Integer page,
				   								@RequestParam(value="limit", required=true) Integer limit) throws ParseException 
	{
		return Paginacion.paginar(Practica.class, iPractica.porRango(desde, hasta), sort, page, limit);
		
	}
	
	
	@RequestMapping(value="/proximas/todos")
	public @ResponseBody List<Object> Proximas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
											   @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Practica.class, iPractica.practicasProximas(fechasistema), sort, page, limit);
		
	} 
	 
	 @RequestMapping(value="/pasadas/todos")
	 public @ResponseBody List<Object> Pasadas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
											   @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
			
			Date today = Calendar.getInstance().getTime();
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String today_str = formatter.format(today);
			Date fechasistema = formatter.parse(today_str);
		
	        return Paginacion.paginar(Practica.class, iPractica.practicasPasadas(fechasistema), sort_desc, page, limit);
			
		} 
	 

}
