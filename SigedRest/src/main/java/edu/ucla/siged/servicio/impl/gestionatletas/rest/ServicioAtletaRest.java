package edu.ucla.siged.servicio.impl.gestionatletas.rest;
 
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
 
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
 
import edu.ucla.siged.dao.DaoAtletaI;
import edu.ucla.siged.dao.DaoReposoI;
import edu.ucla.siged.dao.DaoTipoDocumentoI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.AtletaRepresentante;
import edu.ucla.siged.domain.gestionatleta.Documento;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAyuda;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.domain.gestionatleta.Reposo;
import edu.ucla.siged.domain.gestionatleta.TipoDocumento;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.Paginacion;
 
 
@Controller
@RequestMapping(value="/atleta")
public class ServicioAtletaRest {
 
	@Autowired
	private DaoAtletaI iAtleta;
	
	@Autowired
	private DaoReposoI iReposo;
	
	@Autowired
	private DaoTipoDocumentoI iTipoDocumento;
	
	MutableSortDefinition sort = new MutableSortDefinition("id", true, true);
	
	@RequestMapping(value="/todos", method = RequestMethod.GET)
	public @ResponseBody List<Object> todos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
											@RequestParam(value="limit", required=true) Integer limit)
	{
		return Paginacion.paginar(Atleta.class,iAtleta.findAll(), sort, page, limit);
		
	}
	
	
	@RequestMapping(value="/porCedula/{cedula}", method = RequestMethod.GET)
	public @ResponseBody Atleta ObtenerAtleta(@PathVariable(value="cedula") String cedula)
	{
		Atleta atleta = iAtleta.findByCedula(cedula);
		return atleta;
		
	}
	
	@RequestMapping(value="/porCedula/{cedula}/historiasAyuda", method = RequestMethod.GET)
	public @ResponseBody List<Object> getHistorias(@PathVariable(value="cedula") String cedula,
												   @RequestParam(value="page", required=true, defaultValue="0") Integer page,
												   @RequestParam(value="limit", required=true) Integer limit)
	{
		Set<HistoriaAyuda> historias =  iAtleta.findByCedula(cedula).getHistoriaAyudas();
		return  Paginacion.paginar_para_set(Atleta.class, historias , sort, page, limit);
		
	}
	
	@RequestMapping(value="/porCedula/{cedula}/pagos", method = RequestMethod.GET)
	public @ResponseBody List<Object> getPagos(@PathVariable(value="cedula") String cedula,
				 							   @RequestParam(value="page", required=true, defaultValue="0") Integer page,
				 							   @RequestParam(value="limit", required=true) Integer limit)
	{
		Set<Pago> pagos =  iAtleta.findByCedula(cedula).getPagos();
		return  Paginacion.paginar_para_set(Atleta.class, pagos , sort, page, limit);
		
	}
	
	@RequestMapping(value="/porCedula/{cedula}/atletaRepresentantes", method = RequestMethod.GET)
	public @ResponseBody List<Object> getAtletaRepresentantes(@PathVariable(value="cedula") String cedula,
															  @RequestParam(value="page", required=true, defaultValue="0") Integer page,
															  @RequestParam(value="limit", required=true) Integer limit)
	{
		Set<AtletaRepresentante> atletaRepresentantes =  iAtleta.findByCedula(cedula).getAtletaRepresentantes();
		return  Paginacion.paginar_para_set(Atleta.class, atletaRepresentantes , sort, page, limit);
		
	}
	
	@RequestMapping(value="/porCedula/{cedula}/documentos", method = RequestMethod.GET)
	public @ResponseBody List<Object> getDocumento(@PathVariable(value="cedula") String cedula,
												   @RequestParam(value="page", required=true, defaultValue="0") Integer page,
												   @RequestParam(value="limit", required=true) Integer limit)
	{
		Set<Documento> documentos =  iAtleta.findByCedula(cedula).getDocumentos();
		return  Paginacion.paginar_para_set(Atleta.class, documentos , sort, page, limit);
		
	}
	
	@RequestMapping(value = "/porCedula/{cedula}/atletaEquipos", method = RequestMethod.GET)
	public @ResponseBody List<Object> getAtletaEquipos(@PathVariable(value = "cedula") String cedula,
			 										   @RequestParam(value="page", required=true, defaultValue="0") Integer page,
			 										   @RequestParam(value="limit", required=true) Integer limit) 
	{
		Set<AtletaEquipo> atletaEquipos = iAtleta.findByCedula(cedula).getAtletaEquipos();
		return  Paginacion.paginar_para_set(Atleta.class, atletaEquipos , sort, page, limit);
 
	}
	
	@RequestMapping(value="/porCedula/{cedula}/historiaAtletas", method = RequestMethod.GET)
	public @ResponseBody List<Object> getHistoriaAtletas(@PathVariable(value="cedula") String cedula,
														 @RequestParam(value="page", required=true, defaultValue="0") Integer page,
														 @RequestParam(value="limit", required=true) Integer limit)
	{
		Set<HistoriaAtleta> historiaAtletas =  iAtleta.findByCedula(cedula).getHistoriaAtleta();
		return  Paginacion.paginar_para_set(Atleta.class,  historiaAtletas , sort, page, limit);
		
	}
	
	@RequestMapping(value = "/porCedula/{cedula}/reposos", method = RequestMethod.GET)
	public @ResponseBody List<Object> getReposos(@PathVariable(value = "cedula") String cedula,
			 									 @RequestParam(value="page", required=true, defaultValue="0") Integer page,
			 									 @RequestParam(value="limit", required=true) Integer limit) {
		
//		Atleta atleta = iAtleta.findByCedula(cedula);
		Set<Reposo> reposos = iAtleta.findByCedula(cedula).getReposos(); //getRepososPoAtleta(atleta.getId());
		return  Paginacion.paginar_para_set(Atleta.class,  reposos , sort, page, limit);
		
	}
	
	//usando Documento.java - http://pastie.org/9823321
	@RequestMapping(value="/reposo/documento-upload", method=RequestMethod.POST, produces = "application/json")
	public @ResponseBody Reposo handleFileUpload(@RequestParam("idatleta") Integer idatleta, 
			  									 @RequestParam("descripcion") String descripcion,
			  									 @RequestParam(value="fechainicio", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date fechainicio,
			  									 @RequestParam(value="fechafin", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date fechafin,
			                                     @RequestParam(value="file") MultipartFile file,
			                                     @RequestParam("idTipoDocumento") Integer idTipoDocumento
			                                                 ) throws IOException{
	
		 
		  Map<String,Object> result = new HashMap<String,Object>();
		  
		  Archivo archivo = new Archivo(file);
		  Atleta atleta = iAtleta.findOne(idatleta);
		  TipoDocumento tipoDocuento = iTipoDocumento.findOne(idTipoDocumento);
		  Documento documento =  new Documento(atleta, tipoDocuento, archivo);
		  
		  Reposo reposo =  new Reposo(descripcion, fechainicio,fechafin,documento);
		  
		 return iReposo.saveAndFlush(reposo);
	}
	
}