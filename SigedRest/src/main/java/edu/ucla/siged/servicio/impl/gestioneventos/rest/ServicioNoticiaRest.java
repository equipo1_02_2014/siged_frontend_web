package edu.ucla.siged.servicio.impl.gestioneventos.rest;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Calendar;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.zkoss.util.logging.Log;

import edu.ucla.siged.dao.DaoNoticiaI;
import edu.ucla.siged.dao.DaoRolUsuarioI;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.Paginacion;
import edu.ucla.siged.domain.utilidades.RespuestaServidor;

@Controller
@RequestMapping(value="/noticia")

public class ServicioNoticiaRest {

	@Autowired
	private DaoNoticiaI iNoticia;
	
	@Autowired
	private DaoRolUsuarioI iRolUsuario;
	
	MutableSortDefinition sort = new MutableSortDefinition("fecha", true, false);
	
	@RequestMapping(value="/todas")
	public @ResponseBody List<Object> todas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		return Paginacion.paginar(Noticia.class, iNoticia.findAll(), sort, page, limit);
	
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public @ResponseBody Noticia obtenerUnaNoticia(@PathVariable(value="id") Integer id)
	{
		Noticia noticia = iNoticia.findOne(id);
		return noticia;
		
	}
	
	@RequestMapping(value="/{id}/adjuntos", method = RequestMethod.GET)
	public @ResponseBody java.util.Set<AdjuntoNoticia> getAdjuntos(@PathVariable(value="id") Integer id)
	{
		java.util.Set<AdjuntoNoticia> adjuntos = iNoticia.findOne(id).getAdjuntoNoticias();
		return adjuntos;
		
	}
	
	@RequestMapping(value="/porRango")
	public @ResponseBody List<Object> porRango(@RequestParam(value="desde", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date desde,
											    @RequestParam(value="hasta", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date hasta,
											    @RequestParam(value="page", required=true, defaultValue="0") Integer page,
				   								@RequestParam(value="limit", required=true) Integer limit) throws ParseException 
	{
		return Paginacion.paginar(Noticia.class, iNoticia.porRango(desde, hasta), sort, page, limit);
		
	}
	
	@RequestMapping("/insertar")
	public @ResponseBody Object insertar(@RequestParam(value="titulo", required=true) String titulo,
										  @RequestParam(value="descripcion", required=true) String descripcion,
										  @RequestParam(value="enlace", required=false) String enlace,
									      @RequestParam(value="autor", required=true) String autor,
										  @RequestParam(value="file") MultipartFile file,
										  HttpSession session) throws IOException, ParseException
	{	
		
		List<String> funcionalidades = (List<String>) session.getAttribute("funcionalidades"); 
		//boolean accesoRegistrarNoticia = funcionalidades.contains("Noticias");
		RespuestaServidor response = new RespuestaServidor();
		
		boolean accesoRegistrarNoticia = true;
		if(accesoRegistrarNoticia==false)
		{
			response.setOk(false);
			response.setMensaje("Acceso denegado a esta funcionalidad!");
		}
		else
		{
			Archivo archivo =  new Archivo(file);
			archivo.setTamano((long) 10);
			
			AdjuntoNoticia adjunto = new AdjuntoNoticia(archivo);
			HashSet<AdjuntoNoticia> adjuntos = new HashSet<AdjuntoNoticia>();
			adjuntos.add(adjunto);
			
			Date today = Calendar.getInstance().getTime();
			DateFormat formatter = new SimpleDateFormat("hh:mm:ss");
			String today_str = formatter.format(today);
			Date hora = formatter.parse(today_str);
			
			Date today_fecha = Calendar.getInstance().getTime();
			DateFormat formatter_fecha = new SimpleDateFormat("yyyy-MM-dd");
			String today_str_fecha = formatter_fecha.format(today_fecha);
			Date fecha = formatter_fecha.parse(today_str_fecha);
			
			Noticia noticia = new Noticia(titulo,descripcion,fecha,enlace,autor,hora,adjuntos);
			
			Noticia noticiaInsertada = iNoticia.saveAndFlush(noticia);
			
			if(noticiaInsertada!=null)
			{
				response.setOk(true);
				response.setMensaje("Noticia Registrada Satisfactoriamente!");
				
			}
			
			
		}
		
		return  response;
		
	}
}
