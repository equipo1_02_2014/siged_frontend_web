package edu.ucla.siged.servicio.impl.seguridad.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig.Feature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.ucla.siged.dao.DaoControlAccesoI;
import edu.ucla.siged.dao.DaoFuncionalidadI;
import edu.ucla.siged.dao.DaoRolI;
import edu.ucla.siged.dao.DaoRolUsuarioI;
import edu.ucla.siged.dao.DaoUsuarioI;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.RespuestaServidor;
import edu.ucla.siged.domain.utilidades.SessionWrapper;


////email imports
/*
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
*/

@Controller
@RequestMapping(value="/acceso")
public class ServicioControlAccesoRest {

	@Autowired
	private DaoUsuarioI iUsuario;
	
	@Autowired
	private DaoRolUsuarioI iRolUsuario;
	
	@Autowired
	private DaoRolI iRol;
	
	@Autowired
	private DaoControlAccesoI iControlAcceso;
	
	@Autowired
	private DaoFuncionalidadI iFuncionalidad;
	
	
	
	@RequestMapping(value="/login",  method = RequestMethod.POST)
	public @ResponseBody Object login(@RequestParam(value="nombreUsuario", required=true) String nombreUsuario,
			   						  @RequestParam(value="password", required=true) String password,
			   						  HttpSession session)
	{
		Usuario usuario = iUsuario.login(nombreUsuario, password);
		
		if (usuario==null) 
		{
			RespuestaServidor response = new RespuestaServidor(false, "Nombre de usuario o Password incorrecto.");
			return response;
		}
		
		List<RolUsuario> rolesUsuario = iRolUsuario.buscarListaPorUsuarioId(usuario.getId());
		List<Rol> roles = (List<Rol>) new ArrayList<Rol>();
		List<String> nombreRoles = (List<String>) new ArrayList<String>();
		List<String> nombresFuncionalidades = new ArrayList<String>();
		
		for (RolUsuario rolUsuario : rolesUsuario) 
		{
			roles.add(rolUsuario.getRol());
			nombreRoles.add(rolUsuario.getRol().getNombre());
			
			List<ControlAcceso> controlAccesos = iControlAcceso.obtenerPorRolId(rolUsuario.getRol().getId());
			
			for (ControlAcceso controlAcceso : controlAccesos) 
			{
				if(!nombresFuncionalidades.contains(iFuncionalidad.findById(controlAcceso.getFuncionalidad().getId()).getNombre())) //evita insertar funcionalidades duplicadas
				{
					nombresFuncionalidades.add(iFuncionalidad.findById(controlAcceso.getFuncionalidad().getId()).getNombre());
				}
				
			}
		}
		
		session.setAttribute("id", usuario.getId());
		session.setAttribute("cedula", usuario.getCedula());
		session.setAttribute("nombre", usuario.getNombre() );
		session.setAttribute("nombreUsuario", usuario.getNombreUsuario());
		session.setAttribute("apellido", usuario.getApellido());
		session.setAttribute("email", usuario.getEmail());
		session.setAttribute("password", usuario.getPassword());
		session.setAttribute("celular", usuario.getCelular());
		session.setAttribute("telefono", usuario.getTelefono());
		session.setAttribute("fechaNacimiento", usuario.getFechaNacimiento());
		session.setAttribute("fechaRegistro", usuario.getFechaRegistro());
		session.setAttribute("preguntaSecreta", usuario.getPreguntaSecreta());
		session.setAttribute("respuestaSecreta", usuario.getRespuestaSecreta());
		session.setAttribute("estatus", usuario.getEstatus());
		//Colecciones
		session.setAttribute("roles", roles);
		session.setAttribute("funcionalidades", nombresFuncionalidades);
		session.setAttribute("nombreRoles", nombreRoles);
		
		RespuestaServidor response = new RespuestaServidor(true, "Bienvenido : " + usuario.getNombre() + " " + usuario.getApellido() );
		//RespuestaServidor response = new RespuestaServidor(true, "Bienvenido : " + roles );
		
		return response;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.POST)
	public @ResponseBody Object logout(HttpSession session)
	{
		session.invalidate();
		RespuestaServidor response = new RespuestaServidor(true, "Hasta Pronto!"  );
		
		return response;
		
	}
	
	
	
	@RequestMapping(value="/session")
	public @ResponseBody SessionWrapper obtenerSession(HttpSession session) 
	{
		
		Integer idUsuario = (Integer) session.getAttribute("id");
		Usuario usuario =  iUsuario.findOne(idUsuario);
		
		List<String> funcionalidades = (List<String>) session.getAttribute("funcionalidades");
		List<String> roles = (List<String>) session.getAttribute("nombreRoles");
		
		SessionWrapper usuarioWrapper = new SessionWrapper(usuario,
														  funcionalidades,
														  roles);
		
		return usuarioWrapper;
	}
	
	@RequestMapping(value="/actualizarPassword")
	public @ResponseBody Object cambiarPassword(@RequestParam(value="passwordActual", required=true) String passwordActual,
											    @RequestParam(value="passwordNuevo", required=true) String passwordNuevo,
											    HttpSession session) 
	{
		Usuario usuario = iUsuario.login((String) session.getAttribute("nombreUsuario"), passwordActual);
		if (usuario==null) 
		{
			RespuestaServidor response = new RespuestaServidor(false, "password Incorrecto");
			return response;
		}
		
		try {
			
			usuario.setPassword(passwordNuevo);
			iUsuario.saveAndFlush(usuario);
			
		} catch (Exception e) {
			
			RespuestaServidor response = new RespuestaServidor(false, e.getMessage());
			return response;
			
		}
	
		RespuestaServidor response = new RespuestaServidor(true, "Su password ha sido actualizado satisfactoriamente! "  );
		
		return response;
	}
	
	@RequestMapping(value="/actualizarFoto")
	public @ResponseBody Object actualizarFoto(@RequestParam(value="file") MultipartFile file,
											   HttpSession session) 
	{
		Usuario usuario = iUsuario.findOne((Integer) session.getAttribute("id"));
		if (usuario==null) 
		{
			RespuestaServidor response = new RespuestaServidor(false, "no se encontro usuario con la id en objeto session");
			return response;
		}
		
		
		try {
			
			Archivo archivo =  new Archivo(file);
			usuario.setFoto(archivo);
			
			iUsuario.saveAndFlush(usuario);
			
		} catch (Exception e) {
			
			RespuestaServidor response = new RespuestaServidor(false, e.getMessage());
			return response;
		}
		
		RespuestaServidor response = new RespuestaServidor(true, "Su foto ha sido actualizada satisfactoriamente! "  );
		
		return response;
	}
	
	@RequestMapping(value="/actualizarPerfil")
	public @ResponseBody Object actualizarPerfil(@RequestParam(value="email", required=false) String email,
												 @RequestParam(value="numeroTelefono", required=false) String numeroTelefono,
												 @RequestParam(value="numeroCelular", required=false) String numeroCelular,
											     HttpSession session) 
	{
		Usuario usuario = iUsuario.findOne((Integer) session.getAttribute("id"));
		if (usuario==null) 
		{
			RespuestaServidor response = new RespuestaServidor(false, "no se encontro usuario con el idUsuario indicado en objeto session.");
			return response;
		}
		
		try {
			usuario.setEmail(email);
			usuario.setTelefono(numeroTelefono);
			usuario.setCelular(numeroCelular);
			
			iUsuario.saveAndFlush(usuario);
			
		} catch (Exception e) {
			
			RespuestaServidor response = new RespuestaServidor(false, e.getMessage());
			return response;
		}
		
		RespuestaServidor response = new RespuestaServidor(true, "Cambios realizados satisfactoriamente! "  );
		
		return response;
	}
	

	@RequestMapping(value="/reestablecerPassword/paso1")
	public @ResponseBody Object reestablecerPassword1(@RequestParam(value="email", required=false) String email,
													  @RequestParam(value="nombreUsuario", required=false) String nombreUsuario) 
													  
	{
		RespuestaServidor response = new RespuestaServidor();
		
		if(email!=null)
		{
			Usuario usuario = iUsuario.findByEmail(email);
			if (usuario==null) 
			{
				response.setOk(false);
				response.setMensaje("El email ingresado no esta asociado a ningun usuario en el sistema.");
				return response;
			}

			response.setOk(true);
			response.setMensaje(usuario.getPreguntaSecreta());
			return response;
		}
		else
		{
			Usuario usuario = iUsuario.buscarPorNombreUsuario(nombreUsuario);
			if (usuario==null) 
			{
				response.setOk(false);
				response.setMensaje("El nombre de usuario ingresado no esta asociado a ningun usuario en el sistema.");
				return response;
			}

			response.setOk(true);
			response.setMensaje(usuario.getPreguntaSecreta());
			return response;
		}
	}
	
	@RequestMapping(value="/reestablecerPassword/paso2")
	public @ResponseBody Object reestablecerPassword2(@RequestParam(value="respuestaSecreta", required=true) String respuestaSecreta,
													  @RequestParam(value="email", required=true) String email) 
	{
		Usuario usuario = iUsuario.findMatchEmailRespuestaSecreta(email, respuestaSecreta );
		if (usuario==null) 
		{
			RespuestaServidor response = new RespuestaServidor(false, "Respuesta Incorrecta.");
			return response;
		}
		
		return usuario;
	}
	
	@RequestMapping(value="/reestablecerPassword/paso3")
	public @ResponseBody Object reestablecerPassword3(@RequestParam(value="idUsuario", required=true) Integer idUsuario,
													  @RequestParam(value="passwordNuevo", required=true) String passwordNuevo) 
	{
		Usuario usuario = iUsuario.findById(idUsuario);
		if (usuario==null) 
		{
			RespuestaServidor response = new RespuestaServidor(false, "El id ingresado no esta asociado a un usuario.");
			return response;
		}
		
		try {
			
			usuario.setPassword(passwordNuevo);
			iUsuario.saveAndFlush(usuario);
			
		} catch (Exception e) {
			
			RespuestaServidor response = new RespuestaServidor(false, e.getMessage());
			return response;
		}
		
		RespuestaServidor response = new RespuestaServidor(true, "Password Reestablecido satisfactoriamente. "  );
		return response;
	}	

}
