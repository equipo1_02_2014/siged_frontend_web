package edu.ucla.siged.servicio.impl.gestionequipos.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.ucla.siged.dao.DaoRangoI;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Rango;


@Controller
@RequestMapping(value="/rango")
public class ServicioRangoRest {

	@Autowired
	private DaoRangoI iRango;
	
	@RequestMapping(value="/todos", method = RequestMethod.GET)
	public @ResponseBody List<Rango> todos()
	{
		List<Rango> rangos = iRango.findAll();
		return rangos;
		
	}
	
}
