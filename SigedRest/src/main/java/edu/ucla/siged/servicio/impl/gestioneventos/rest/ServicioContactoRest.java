package edu.ucla.siged.servicio.impl.gestioneventos.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.ucla.siged.dao.DaoSugerenciaI;
import edu.ucla.siged.domain.gestioneventos.Sugerencia;
import edu.ucla.siged.domain.utilidades.RespuestaServidor;

@Controller
@RequestMapping(value="/contacto")
public class ServicioContactoRest {
	
	@Autowired
	private DaoSugerenciaI iSugerencia;
	
	@RequestMapping("/insertar")
	public @ResponseBody Object insertar(@RequestParam(value="autor", required=true) String autor,
									     @RequestParam(value="celular", required=true) String celular,
									     @RequestParam(value="descripcion", required=true) String descripcion,
									     @RequestParam(value="email", required=true) String email) throws ParseException
	{
		Date today_fecha = Calendar.getInstance().getTime();
		DateFormat formatter_fecha = new SimpleDateFormat("yyyy-MM-dd");
		String today_str_fecha = formatter_fecha.format(today_fecha);
		Date fecha = formatter_fecha.parse(today_str_fecha);
		
		RespuestaServidor response = new RespuestaServidor();
		Sugerencia sugerencia = new Sugerencia(autor, email,celular,descripcion,fecha,0);
		
		try {
			
			iSugerencia.saveAndFlush(sugerencia);
		    response.setOk(true);
		    response.setMensaje("Registro guardado satisfactoriamente");
			
			
		} catch (Exception e) {
			
			response.setOk(false);
			response.setMensaje(e.getMessage());
		}
		
		
		return response;
	}
	
	
	
	

}
