package edu.ucla.siged.servicio.impl.gestionequipos.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.data.repository.query.Param;

import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.DelegadoEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.utilidades.Paginacion;


@Controller
@RequestMapping(value="/equipo")
public class ServicioEquipoRest {

	@Autowired
	private DaoEquipoI iEquipo;
	
	MutableSortDefinition sort = new MutableSortDefinition("fechaCreacion", true, true);
	
	private List<Date> rangoFromYear(String year) throws ParseException
	{
		DateFormat formatter = new SimpleDateFormat("yy-MM-dd");
		
		Date inicio = formatter.parse(year + "-01-01");
		Date fin = formatter.parse(year + "-12-31");
		
		List<Date> rango = Arrays.asList(inicio, fin);
		return rango;
	}
	
	@RequestMapping(value="/todos", method = RequestMethod.GET)
	public @ResponseBody List<Object> todos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		
		return Paginacion.paginar(Equipo.class, iEquipo.findAll(), sort, page, limit);
		
	}
	
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public @ResponseBody Equipo porCategoriaRango(@RequestParam(value = "idCategoria") Integer idCategoria,
												  @RequestParam(value = "idRango") Integer idRango,
												  @RequestParam(value = "idTipoEquipo") Integer idTipoEquipo,
												  @RequestParam(value = "year") String year) throws ParseException 
    {

		List<Date> rango = rangoFromYear(year);
		
		Equipo equipo = iEquipo.findByCategoriaRango(idCategoria, idRango, idTipoEquipo, rango.get(0), rango.get(1));
		return equipo;

	}
	
	
	@RequestMapping(value = "/practicas", method = RequestMethod.GET)
	public @ResponseBody List<Object> getPracticas(@RequestParam(value = "idCategoria") Integer idCategoria,
													@RequestParam(value = "idRango") Integer idRango,
													@RequestParam(value = "idTipoEquipo") Integer idTipoEquipo,
													@RequestParam(value = "year") String year,
													@RequestParam(value="page", required=true, defaultValue="0") Integer page,
					   								@RequestParam(value="limit", required=true) Integer limit) throws ParseException 
	{
		List<Date> rango = rangoFromYear(year);
		Equipo equipo = iEquipo.findByCategoriaRango(idCategoria, idRango,idTipoEquipo, rango.get(0), rango.get(1));

		return Paginacion.paginar_para_set(Equipo.class, equipo.getPracticas() , sort, page, limit);

	}
	

	@RequestMapping(value = "/delegados", method = RequestMethod.GET)
	public @ResponseBody List<Object> getDelegadoEquipos(@RequestParam(value = "idCategoria") Integer idCategoria,
															  	@RequestParam(value = "idRango") Integer idRango,
															  	@RequestParam(value = "idTipoEquipo") Integer idTipoEquipo,
															  	@RequestParam(value = "year") String year,
															  	@RequestParam(value="page", required=true, defaultValue="0") Integer page,
								   								@RequestParam(value="limit", required=true) Integer limit) throws ParseException {
		List<Date> rango = rangoFromYear(year);
		Equipo equipo = iEquipo.findByCategoriaRango(idCategoria, idRango,idTipoEquipo,rango.get(0), rango.get(1));
		
		return Paginacion.paginar_para_set(Equipo.class, equipo.getDelegadoEquipos() , sort, page, limit);
		 

	}
	
	@RequestMapping(value = "/tecnicos", method = RequestMethod.GET)
	public @ResponseBody List<Object> getEquipoTecnicos(@RequestParam(value = "idCategoria") Integer idCategoria,
															  @RequestParam(value = "idRango") Integer idRango,
															  @RequestParam(value = "idTipoEquipo") Integer idTipoEquipo,
															  @RequestParam(value = "year") String year,
															  @RequestParam(value="page", required=true, defaultValue="0") Integer page,
								   							  @RequestParam(value="limit", required=true) Integer limit) throws ParseException {
		List<Date> rango = rangoFromYear(year);
		Equipo equipo = iEquipo.findByCategoriaRango(idCategoria, idRango,idTipoEquipo, rango.get(0), rango.get(1));
		
		return Paginacion.paginar_para_set(Equipo.class, equipo.getEquipoTecnicos(), sort, page, limit);

	}

	@RequestMapping(value = "/atletas", method = RequestMethod.GET)
	public @ResponseBody List<Object> getAtletaEquipos(@RequestParam(value = "idCategoria") Integer idCategoria,
															@RequestParam(value = "idRango") Integer idRango,
															@RequestParam(value = "idTipoEquipo") Integer idTipoEquipo,
															@RequestParam(value = "year") String year,
															@RequestParam(value="page", required=true, defaultValue="0") Integer page,
								   							@RequestParam(value="limit", required=true) Integer limit) throws ParseException {
			
		List<Date> rango = rangoFromYear(year);
		Equipo equipo = iEquipo.findByCategoriaRango(idCategoria, idRango,idTipoEquipo, rango.get(0), rango.get(1));
		
		return Paginacion.paginar_para_set(Equipo.class, equipo.getAtletaEquipos() , sort, page, limit);
	
	}
	
	


	
}
