package edu.ucla.siged.servicio.impl.gestionequipos.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.ucla.siged.dao.DaoCompetenciaI;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.utilidades.Paginacion;


@Controller
@RequestMapping(value="/competencia")
public class ServicioCompetenciaRest {

	@Autowired
	private DaoCompetenciaI iCompetencia;
	
	MutableSortDefinition sort = new MutableSortDefinition("fechaInicio", true, true);
	
	@RequestMapping(value="/todos")
	public @ResponseBody List<Object> todos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		return Paginacion.paginar(Competencia.class, iCompetencia.findAll(), sort, page, limit);
		
	}
	
	@RequestMapping(value="/{id}/juegos")
	public @ResponseBody List<Object> getJuegos(@PathVariable(value="id") Integer id,
											@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		Competencia competencia = iCompetencia.findOne(id);
		return Paginacion.paginar_para_set(Competencia.class, competencia.getJuegos() , sort, page, limit);
		
	}
	
	
}
