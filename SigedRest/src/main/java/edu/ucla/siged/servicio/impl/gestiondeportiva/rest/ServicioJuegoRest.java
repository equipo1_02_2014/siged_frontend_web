package edu.ucla.siged.servicio.impl.gestiondeportiva.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
//import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import edu.ucla.siged.dao.DaoJuegoI;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.utilidades.Paginacion;


@Controller
@RequestMapping(value="/juegos")
public class ServicioJuegoRest {
	
	@Autowired
	private DaoJuegoI iJuego;
	
	MutableSortDefinition sort = new MutableSortDefinition("fecha",true,true); 
	MutableSortDefinition sort_desc = new MutableSortDefinition("fecha",true,false); 
	
	
	@RequestMapping(value="/todos")
	public @ResponseBody List<Object> todos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
										    @RequestParam(value="limit", required=true) Integer limit){
	
		return Paginacion.paginar(Juego.class, iJuego.findAll(), sort, page, limit);
		
	}
	
	//Agregado Enero 13
	@RequestMapping(value="/{id}/equipo")
	public @ResponseBody Equipo getEquipo(@PathVariable(value="id") Integer id)
	{
		return  iJuego.findOne(id).getEquipo();
	}
	
	@RequestMapping(value="/{id}/competencia")
	public @ResponseBody Competencia getCompetencia(@PathVariable(value="id") Integer id)
	{
		return  iJuego.findOne(id).getCompetencia();
	}
	
	@RequestMapping(value="/{id}/area")
	public @ResponseBody Area getArea(@PathVariable(value="id") Integer id)
	{
		return  iJuego.findOne(id).getArea();
	}
	
	@RequestMapping(value="/{id}/anotador")
	public @ResponseBody Anotador getAnotador(@PathVariable(value="id") Integer id)
	{
		return  iJuego.findOne(id).getAnotador();
	}
	
	@RequestMapping(value="/{id}/arbitroJuego")
	public @ResponseBody List<Object> getArbitroJuego(@PathVariable(value="id") Integer id,
											@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		Juego juego =  iJuego.findOne(id);
		return Paginacion.paginar_para_set(Juego.class, juego.getArbitroJuego() , sort, page, limit);
		
	}
	
	@RequestMapping(value="/jugados/todos")
	public @ResponseBody List<Object> JugadosTodos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
												   @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Juego.class, iJuego.juegosJugados(fechasistema), sort_desc, page, limit);
		
	}
	
	
	@RequestMapping(value="/jugados/porCompetencia")
	public @ResponseBody List<Object> PorCompetenciaJugados(@RequestParam(value="idcompetencia", required=true) Integer idcompetencia,
															@RequestParam(value="page", required=true, defaultValue="0") Integer page,
															@RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Juego.class, iJuego.juegosPorCompetenciaJugados(idcompetencia, fechasistema), sort_desc, page, limit);
		
	}
	
	//+ emisael
	@RequestMapping(value="/proximos/todos")
	public @ResponseBody List<Object> PorCompetenciaProximos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
															 @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Juego.class, iJuego.juegosProximos(fechasistema), sort, page, limit);
		
	}
	
	@RequestMapping(value="/proximos/porCompetencia")
	public @ResponseBody List<Object> PorCompetenciaProximos(@RequestParam(value="idcompetencia", required=true) Integer idcompetencia,
															@RequestParam(value="page", required=true, defaultValue="0") Integer page,
															@RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Juego.class, iJuego.juegosPorCompetenciaProximos(idcompetencia, fechasistema), sort, page, limit);
		
	}
	
	@RequestMapping(value="/jugados/porCategoria")
	public @ResponseBody List<Object> PorCategoriaJugados(@RequestParam(value="idcategoria", required=true) Integer idcategoria,
												  @RequestParam(value="page", required=true, defaultValue="0") Integer page,
												  @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
		
		return Paginacion.paginar(Juego.class, iJuego.juegosPorCategoriaJugados(idcategoria, fechasistema), sort_desc, page, limit);
		
	}
	
	@RequestMapping(value="/proximos/porCategoria")
	public @ResponseBody List<Object> PorCategoriaProximos(@RequestParam(value="idcategoria", required=true) Integer idcategoria,
												  @RequestParam(value="page", required=true, defaultValue="0") Integer page,
												  @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
		
		return Paginacion.paginar(Juego.class, iJuego.juegosPorCategoriaProximos(idcategoria, fechasistema), sort, page, limit);
		
	}
	
	@RequestMapping(value="/jugados/porCategoria&Competencia")
	public @ResponseBody List<Object> PorCategoriaCompetenciaJugados(@RequestParam(value="idcategoria", required=true) Integer idcategoria,	
															 @RequestParam(value="idcompetencia", required=true) Integer idcompetencia,
															 @RequestParam(value="page", required=true, defaultValue="0") Integer page,
															 @RequestParam(value="limit", required=true) Integer limit) throws ParseException
	{
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Juego.class, iJuego.juegosPorCategoriaCompetenciaJugados(idcategoria, idcompetencia, fechasistema), sort, page, limit);
	}
	
	@RequestMapping(value="/proximos/porCategoria&Competencia")
	public @ResponseBody List<Object> PorCategoriaCompetenciaProximos(@RequestParam(value="idcategoria", required=true) Integer idcategoria,	
															 @RequestParam(value="idcompetencia", required=true) Integer idcompetencia,
															 @RequestParam(value="page", required=true, defaultValue="0") Integer page,
															 @RequestParam(value="limit", required=true) Integer limit) throws ParseException
	{
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Juego.class, iJuego.juegosPorCategoriaCompetenciaProximos(idcategoria, idcompetencia, fechasistema), sort, page, limit);
	}
	
	
	@RequestMapping(value="/porJugar")
	public @ResponseBody List<Object> porJugar(@RequestParam(value="idEquipo", required=true) Integer idEquipo,
											  @RequestParam(value="page", required=true, defaultValue="0") Integer page,
											  @RequestParam(value="limit", required=true) Integer limit) throws ParseException 
	{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
		
		return Paginacion.paginar(Juego.class, iJuego.juegosPendientes(date, idEquipo), sort, page, limit);
	}
	
	//+ 10/12/2014
	@RequestMapping(value="/porRango")
	public @ResponseBody List<Object> porRango(@RequestParam(value="desde", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date desde,
											   @RequestParam(value="hasta", required= true ) @DateTimeFormat(pattern="yyyy-MM-dd") Date hasta,
											  @RequestParam(value="idEquipo", required=true) Integer idEquipo,
											  @RequestParam(value="page", required=true, defaultValue="0") Integer page,
											  @RequestParam(value="limit", required=true) Integer limit) throws ParseException 
	{
			
		return Paginacion.paginar(Juego.class, iJuego.porRango(desde, hasta, idEquipo), sort, page, limit);
		
	}
	
	
}
