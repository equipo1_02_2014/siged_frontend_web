package edu.ucla.siged.servicio.impl.gestionequipos.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.ucla.siged.dao.DaoCategoriaI;
import edu.ucla.siged.dao.DaoEquipoI;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.utilidades.CategoriaWrapper;


@Controller
@RequestMapping(value="/categoria")
public class ServicioCategoriaRest {
	
	@Autowired
	private DaoCategoriaI iCategoria;
	
	@Autowired
	private DaoEquipoI iEquipo;
	
	@RequestMapping(value="/todos", method = RequestMethod.GET)
	public @ResponseBody List<Categoria> todos()
	{
		List<Categoria> categoria = iCategoria.findAll();
		return categoria;
		
	}
	
	@RequestMapping(value="/rangos", method = RequestMethod.GET)
	public @ResponseBody List<CategoriaWrapper> rangos()
	{
		
		List<Equipo> equiposTodos = iEquipo.findAll();
		List<CategoriaWrapper> response = new ArrayList<CategoriaWrapper>();
		
		for (Equipo equipo : equiposTodos) 
		{	
			response.add(new CategoriaWrapper(equipo.getCategoria().getId() ,equipo.getCategoria().getNombre(), 
											  equipo.getRango().getId(), equipo.getRango().getDescripcion()));
		}
		 		
		return response;
		
	}
}
