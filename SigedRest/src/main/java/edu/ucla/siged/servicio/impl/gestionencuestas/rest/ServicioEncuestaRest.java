package edu.ucla.siged.servicio.impl.gestionencuestas.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;



import javax.servlet.http.HttpSession;

import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.ucla.siged.dao.DaoAtletaI;
import edu.ucla.siged.dao.DaoEncuestaI;
import edu.ucla.siged.dao.DaoOpcionPreguntaI;
import edu.ucla.siged.dao.DaoPreguntaI;
import edu.ucla.siged.dao.DaoPublicoI;
import edu.ucla.siged.dao.DaoRespuestaI;
import edu.ucla.siged.dao.DaoRolI;
import edu.ucla.siged.dao.DaoRolUsuarioI;
import edu.ucla.siged.dao.DaoTecnicoI;
import edu.ucla.siged.dao.DaoUsuarioI;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.gestioneventos.Respuesta;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.domain.utilidades.EncuestaWrapper;
import edu.ucla.siged.domain.utilidades.Paginacion;
import edu.ucla.siged.domain.utilidades.PreguntaWrapper;
import edu.ucla.siged.domain.utilidades.RespuestaServidor;

@Controller
@RequestMapping(value="/encuestas")
public class ServicioEncuestaRest {
	
	@Autowired
	private DaoEncuestaI iEncuesta;
	
	@Autowired
	private DaoPreguntaI iPregunta;
	
	@Autowired
	private DaoOpcionPreguntaI iOpcionPregunta;
	
	@Autowired
	private DaoRespuestaI iRespuesta;
	
	@Autowired
	private DaoUsuarioI iUsuario;
	
	@Autowired
	private DaoRolI iRol;
	
	@Autowired
	private DaoPublicoI iPublico;
	
	MutableSortDefinition sort = new MutableSortDefinition("fechaFin", true, true);
	MutableSortDefinition sort_publicos_rolid = new MutableSortDefinition("id", true, true);
	
	public Boolean respuestaDuplicada(Encuesta encuesta, Integer idUsuario) throws ParseException
	{
		java.util.Set<Pregunta> preguntas = encuesta.getPreguntas();
		for (Pregunta pregunta : preguntas) 
		{
			for (OpcionPregunta opcionPreguntas : pregunta.getOpcionPreguntas()) 
			{
				Respuesta respuesta = iRespuesta.verificarRespuestaEncuesta(idUsuario, opcionPreguntas.getId());
				if(respuesta!=null)
				{
					return true;
				}
				
			}
			
		}
		
		return false;
		
	}
	
//	public Encuesta obtenerEncuestaDesdeOpcionPregunta(Integer idOpcionPregunta) throws ParseException
//	{
//		Date today = Calendar.getInstance().getTime();
//		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//		String today_str = formatter.format(today);
//		Date date = formatter.parse(today_str);
//		
//		OpcionPregunta opcionP = iOpcionPregunta.findOne(idOpcionPregunta);
//		List<Encuesta> encuestas = iEncuesta.encuestasAbiertas(date);
//		
//		for (Encuesta encuesta : encuestas) 
//		{
//			for (Pregunta pregunta : encuesta.getPreguntas()) 
//			{
//				for (Encuesta e : iEncuesta.findAll()) 
//				{
//					if (e.getPreguntas().contains(pregunta)) 
//					{
//						if(pregunta.getOpcionPreguntas().contains(opcionP))
//						{
//							return e;
//						}
//						
//					}
//				}
//
//			}
//			
//		}
//	
//		return null;
//	}

	
	/*
	 * Dado un id de Encuesta retorna todas sus preguntas.
	 * 
	 */
	@RequestMapping(value="/{id}/preguntas")
	public @ResponseBody java.util.Set<PreguntaWrapper> obtenerPreguntasPorIdEncuesta(@PathVariable(value="id") Integer id)   
	{
		
		java.util.Set<PreguntaWrapper> preguntasWrapper = new HashSet<PreguntaWrapper>();
		java.util.Set<Pregunta> preguntas = new HashSet<Pregunta>();
		preguntas = iEncuesta.findOne(id).getPreguntas();
				
		for (Pregunta pregunta : preguntas ) 
		{
			PreguntaWrapper p = new PreguntaWrapper(iEncuesta.findOne(id).getId() ,pregunta, iEncuesta.findOne(id).getFechaRegistro());
			preguntasWrapper.add(p);
		}
		
		return preguntasWrapper;
		
	}
	
	/*
	 * Dado un id de encuesta retorna todos sus permisos (Publicos)
	 * Publico se refiere a que idroles pueden accesar y/o responder la encuesta.
	 */
	@RequestMapping(value="/{id}/publicos")
	public @ResponseBody List<Object> obtenerPublicos(@PathVariable(value="id") Integer id) throws ParseException  
	{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
			
		return Paginacion.paginar_para_set(Encuesta.class, iEncuesta.findOne(id).getPublicos(), sort_publicos_rolid, 0, 10);
		
	}
	
	/*
	 * Retorna todas las encuestas (Abiertas). 
	 * Abiertas son las encuestas que aun se pueden responder y no ha llegado su fecha de cierre.
	 */
	@RequestMapping(value="/abiertas/todas")
	public @ResponseBody List<Object> abiertas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
											   @RequestParam(value="limit", required=true) Integer limit) throws ParseException  
	{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
			
		return Paginacion.paginar(Encuesta.class, iEncuesta.encuestasAbiertas(date), sort, page, limit);
		
	}
	
	/*
	 * Retorna todas las preguntas de las encuestas (Abiertas). 
	 * Abiertas son las encuestas que aun se pueden responder y no ha llegado su fecha de cierre.
	 * 
	 * ENERO 31
	 */
	@RequestMapping(value="/abiertas/preguntas")
	public @ResponseBody List<Object> obtenerPreguntasTodas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			  												@RequestParam(value="limit", required=true) Integer limit) throws ParseException
															    
	{
				
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
		
		List<Encuesta> encuestas = iEncuesta.encuestasAbiertas(date);
		java.util.Set<PreguntaWrapper> preguntas = new HashSet<PreguntaWrapper>();
		Integer idEncuesta = 0;
		
		for (Encuesta encuesta : encuestas) 
		{
			for (Pregunta pregunta : encuesta.getPreguntas()) 
			{
				for (Encuesta e : iEncuesta.findAll()) //buscar a que Encuesta pertenece una Pregunta
				{
					if (e.getPreguntas().contains(pregunta)) 
					{
						idEncuesta =  e.getId();
						break;
					}
				}

				PreguntaWrapper preguntaWrapper = new PreguntaWrapper( idEncuesta,pregunta, encuesta.getFechaRegistro());
				preguntas.add(preguntaWrapper);
			}
			
		}
			
		return Paginacion.paginar_para_set(Encuesta.class, preguntas,sort, page, limit);
			
	}
		
	/*
	 * Dado un idPregunta ,retorna todas las OpcionPregunta de la Pregunta perteneciente a la ENCUESTA (Abiertas). 
	 * Abiertas son las encuestas que aun se pueden responder y no ha llegado su fecha de cierre.
	 * 
	 * ENERO 31
	 */
	@RequestMapping(value="/pregunta/{idPregunta}/opciones")
	public @ResponseBody List<Object> obtenerOpcionRespuestasPorPregunta(@PathVariable(value="idPregunta") Integer idPregunta,
																		 @RequestParam(value="page", required=true, defaultValue="0") Integer page,
						  												 @RequestParam(value="limit", required=true) Integer limit) throws ParseException
																		    
	{
				
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
		
		Pregunta pregunta = iPregunta.findOne(idPregunta);
		java.util.Set<OpcionPregunta> opcionesPreguntas = new HashSet<OpcionPregunta>();
		
		opcionesPreguntas =  pregunta.getOpcionPreguntas();
			
		return Paginacion.paginar_para_set(Encuesta.class, opcionesPreguntas , sort, page, limit);
			
	}
		
	/*
	 * Dado un idRol especifico , retorna todas las encuestas (Abiertas) para dicho id. 
	 * Abiertas son las encuestas que aun se pueden responder y no ha llegado su fecha de cierre.
	 */
	@RequestMapping(value="/abiertas/porRol")
	public @ResponseBody List<EncuestaWrapper> abiertasPorRol(HttpSession session) throws ParseException  
	{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
		
		List<Rol> rolesUsuario = (List<Rol>) session.getAttribute("roles");
		List<EncuestaWrapper>  encuestasPorRol = new ArrayList<EncuestaWrapper>();
		List<Encuesta> encuestasAbiertas = iEncuesta.encuestasAbiertas(date);
		
		for (Rol rol : rolesUsuario)
		{
			for (Encuesta encuesta : encuestasAbiertas) 
			{
				for (Publico publico : encuesta.getPublicos()) 
				{
					if((rol.getId()==publico.getRol().getId()))
					{
						EncuestaWrapper encuestaWrapper =  new EncuestaWrapper(this.respuestaDuplicada(encuesta, (Integer) session.getAttribute("id")), encuesta);
						encuestasPorRol.add(encuestaWrapper);
					}
				}
			}
		}

		return encuestasPorRol;
	}
	
	/*
	 * Retorna todas las encuestas (Abiertas) y (Publicas). 
	 * Abiertas son las encuestas que aun se pueden responder y no ha llegado su fecha de cierre.
	 * Publicas son las encuestas que puede responder cualquier usuario no registrado o visitante.
	 */
	@RequestMapping(value="/abiertas/publicas")
	public @ResponseBody List<Object> abiertasPublicas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
											           @RequestParam(value="limit", required=true) Integer limit) throws ParseException  
	{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date date = formatter.parse(today_str);
			
		List<Object> encuestas_por_rol = new ArrayList<Object>();
		
		List<Encuesta> encuestas_abiertas = iEncuesta.encuestasAbiertas(date);
		for (Encuesta encuesta : encuestas_abiertas) {
			
			for (Publico publico :encuesta.getPublicos()) {
				if(0==publico.getRol().getId())
				{
					if(!encuestas_por_rol.contains(encuesta)){
						encuestas_por_rol.add(encuesta);
					}
				}
			}
		}
		
		return Paginacion.paginar(Encuesta.class, encuestas_por_rol, sort, page, limit);
	}
	
	/*
	 * Dado un idEncuesta especifico ,permite responder las preguntas de la misma. 
	 * se envian los id de las OpcionPreguntas (resuestas) en caso de que la encuesta tenga varias preguntas se envia de la siguente manera 
	 * 1,2,3 --cda respuesta u OpcionPregunta separado por coma(,)
	 * 
	 */
	@RequestMapping(value="/{idEncuesta}/responder")
	public @ResponseBody Object responder(@PathVariable(value="idEncuesta") Integer idEncuesta,
										  @RequestParam(value="idOpcion", required=true) Integer idOpcion,
										  HttpSession session) throws ParseException
	{
		RespuestaServidor response = new RespuestaServidor();
		
		Usuario usuario = iUsuario.findOne((Integer) session.getAttribute("id"));
		if (usuario==null) 
		{
			response.setOk(false);
			response.setMensaje("usuario null, objeto session no creado");
			return response;
		}
		
		OpcionPregunta opcionPregunta = iOpcionPregunta.findOne(idOpcion);
		short valor = (short) 1;
		Respuesta respuesta = new Respuesta(valor,opcionPregunta, usuario);
		
		if(iRespuesta.verificarRespuestaEncuesta(usuario.getId(), idOpcion)!=null)//Valida que el mismo usuario no responda la misma encuesta mas de una vez
		{
			response.setOk(false);
			response.setMensaje("ya ha respondido esta encuesta.");//Lado cliente llama a /resultado con este idEncuesta.
			return response;
		}
		
		iRespuesta.saveAndFlush(respuesta);
		
		response.setOk(true);
		response.setMensaje("Respuesta registrada satisfactoriamente");
		return response;
	}
	
	
	@RequestMapping(value="/{idEncuesta}/responderPublico")
	public @ResponseBody Object responderUsuarioPublico(@PathVariable(value="idEncuesta") Integer idEncuesta,
														@RequestParam(value="OpcionPreguntas", required=true) String OpcionPreguntas)
	{
		
		Usuario usuario = iUsuario.findOne(0); //usuario publico
		List<String> items = Arrays.asList(OpcionPreguntas.split("\\s*,\\s*"));
		for (String string : items) 
		{
			OpcionPregunta opcionPregunta = iOpcionPregunta.findOne(Integer.parseInt(string));
			short valor = (short) 1;
			Respuesta respuesta = new Respuesta(valor,opcionPregunta, usuario);
			iRespuesta.saveAndFlush(respuesta);
		}
		
		RespuestaServidor response = new RespuestaServidor(true, "Respuesta registrada Satisfactoriamente.");
			
		return response;
	}
	
	/*
	 * Dado un idRol y idEncuesta se verifica si un idRol ya ha respondido un idEncuesta 
	 * evitando que un mismo idRol responda mas de una vez un idEncuesta
	 */
	@RequestMapping(value="/verificarRespuesta")
	public @ResponseBody Object verificarRespuesta(@RequestParam(value="idEncuesta", required=true) Integer idEncuesta,
												   HttpSession session) throws ParseException  
	{
		RespuestaServidor respuesta =  new RespuestaServidor();
		
		Encuesta encuesta = iEncuesta.findOne(idEncuesta);
		if(encuesta==null)
		{
			RespuestaServidor response = new RespuestaServidor(false, "el id enviado por parametro no esta asociado a una Encuesta");
			return response;
		}
		
		
		for (Pregunta pregunta : encuesta.getPreguntas() )
		{
			for (OpcionPregunta opcionPregunta : pregunta.getOpcionPreguntas()) 
			{
				if(null!=iRespuesta.verificarRespuestaEncuesta((Integer) session.getAttribute("id") , opcionPregunta.getId()))
				{
					respuesta.setOk(true);
					respuesta.setMensaje("Ya ha respondido esta encuesta.");
					return resultado(idEncuesta);
				}
			}
			
		}
		
		respuesta.setOk(false);
		respuesta.setMensaje("No ha respondido esta encuesta (Activa hasta: "+ iEncuesta.findOne(idEncuesta).getFechaFin() + " )");
		return respuesta;
	}
	
	@RequestMapping(value="/verificarRespuestaPublica")
	public @ResponseBody Object verificarPublica(@RequestParam(value="idEncuesta", required=true) Integer idEncuesta) throws ParseException  
	{
		RespuestaServidor respuesta =  new RespuestaServidor();
		
		Encuesta encuesta = iEncuesta.findOne(idEncuesta);
		if(encuesta==null)
		{
			RespuestaServidor response = new RespuestaServidor(false, "el id enviado por parametro no esta asociado a una Encuesta");
			return response;
		}
		
		
		for (Pregunta pregunta : encuesta.getPreguntas() )
		{
			for (OpcionPregunta opcionPregunta : pregunta.getOpcionPreguntas()) 
			{
				if(null!=iRespuesta.verificarRespuestaEncuestaPublica(opcionPregunta.getId()))
				{
					respuesta.setOk(true);
					respuesta.setMensaje("Ya ha respondido esta encuesta.");
					return resultado(idEncuesta);
				}
			}
			
		}
		
		respuesta.setOk(false);
		respuesta.setMensaje("No ha respondido esta encuesta (Activa hasta: "+ iEncuesta.findOne(idEncuesta).getFechaFin() + " )");
		return respuesta;
	}
	
	@RequestMapping(value="/pregunta/{idPregunta}/verificarRespuesta")
	public @ResponseBody Object verificarRespuestaParaPregunta(@PathVariable(value="idPregunta") Integer idPregunta,
															   HttpSession session) throws ParseException  
	{
		RespuestaServidor response =  new RespuestaServidor();
		
		Pregunta pregunta = iPregunta.findOne(idPregunta);
		if (pregunta==null) 
		{
			response.setOk(false);
			response.setMensaje("no se encontro pregunta asociada con la id ");
			return response;
		}
		
		for (OpcionPregunta opcionPregunta : pregunta.getOpcionPreguntas()) 
		{
			if(null!=iRespuesta.verificarRespuestaPregunta((Integer) session.getAttribute("id"), opcionPregunta.getId()))
			{
				response.setOk(false);
				response.setMensaje("Ya ha respondido esta pregunta.");
				return resultadoPregunta(pregunta.getId());
			}
		}
		
		response.setOk(true);
		response.setMensaje("Esta disponible para responder.");
		
		return response;
	}
	
	/*
	 *  Dado un idRol y idEncuesta se verifica si un idRol ya ha respondido un idEncuesta
	 *  retorna el resultado de cada encuesta con la cantidad de votos de cada OpcionPregunta
	 */
	@RequestMapping(value="/resultado")
	public @ResponseBody Object resultado(@RequestParam(value="idEncuesta", required=true) Integer idEncuesta) throws ParseException  
	{
		Map<String, Object> mapaRespuestas = new HashMap<String,Object>();
		for (Pregunta pregunta : iEncuesta.findOne(idEncuesta).getPreguntas()) {
							
			for (OpcionPregunta opcionPregunta : iPregunta.findOne(pregunta.getId()).getOpcionPreguntas()) 
			{
				
				mapaRespuestas.put(opcionPregunta.getDescripcion(), opcionPregunta.getRespuestas().size());
			}
		}
			
		return mapaRespuestas;
	}

	
	@RequestMapping(value="/pregunta/{id}/resultado")
	public @ResponseBody Object resultadoPregunta(@PathVariable(value="id") Integer id) throws ParseException  
	{
		RespuestaServidor response =  new RespuestaServidor();
		Map<String, Object> mapaRespuestas = new HashMap<String,Object>();
		
		Pregunta pregunta = iPregunta.findOne(id);
		if (pregunta==null) 
		{
			response.setOk(false);
			response.setMensaje("no se encontro pregunta asociada con la id ");
			return response;
		}
		
		for (OpcionPregunta opcionPregunta : pregunta.getOpcionPreguntas()) 
		{
			
			mapaRespuestas.put(opcionPregunta.getDescripcion(), opcionPregunta.getRespuestas().size());
		}
		
	    return mapaRespuestas;
	}
}

