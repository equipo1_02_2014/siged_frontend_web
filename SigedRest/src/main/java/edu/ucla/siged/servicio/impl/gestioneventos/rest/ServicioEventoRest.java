package edu.ucla.siged.servicio.impl.gestioneventos.rest;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.ucla.siged.dao.DaoEventoI;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;
import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.Paginacion;


@Controller
@RequestMapping(value="/evento")
public class ServicioEventoRest {
	
	@Autowired
	private DaoEventoI iEvento;
	
	MutableSortDefinition sort = new MutableSortDefinition("fecha", true, true);
	
	@RequestMapping(value="/todos")
	public @ResponseBody List<Object> todas(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		return Paginacion.paginar(Evento.class, iEvento.findAll(), sort, page, limit);
	
	}
	
	@RequestMapping(value="/proximos/todos")
	public @ResponseBody List<Object> proximosTodos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
													@RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Evento.class, iEvento.eventosProximos(fechasistema), sort, page, limit);
		
	}
	
	@RequestMapping(value="/pasados/todos")
	public @ResponseBody List<Object> pasadosTodos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
												   @RequestParam(value="limit", required=true) Integer limit) throws ParseException{
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String today_str = formatter.format(today);
		Date fechasistema = formatter.parse(today_str);
	
        return Paginacion.paginar(Evento.class, iEvento.eventosPasados(fechasistema), sort, page, limit);
		
	}
	
	@RequestMapping(value="/{id}/adjuntos", method = RequestMethod.GET)
	public @ResponseBody java.util.Set<AdjuntoEvento> getAdjuntosEvento(@PathVariable(value="id") Integer id)
	{
		java.util.Set<AdjuntoEvento> adjuntos = iEvento.findOne(id).getAdjuntoEventos();
		return adjuntos;
		
	}
	
	@RequestMapping("/insertarAdjunto/{idEvento}")
    public @ResponseBody Evento insertar(@PathVariable(value="idEvento") Integer idEvento,
    									 @RequestParam(value="file") MultipartFile file) throws ParseException 
    {
		
		Date today_fecha = Calendar.getInstance().getTime();
		DateFormat formatter_fecha = new SimpleDateFormat("yyyy-MM-dd");
		String today_str_fecha = formatter_fecha.format(today_fecha);
		Date fecha = formatter_fecha.parse(today_str_fecha);
		
		Archivo archivo =  new Archivo(file);
		archivo.setTamano((long) 10);
		AdjuntoEvento adjunto = new AdjuntoEvento("caimanera 2015",fecha ,archivo);
		HashSet<AdjuntoEvento> adjuntos = new HashSet<AdjuntoEvento>();
		adjuntos.add(adjunto);
		
		Evento evento = iEvento.findOne(idEvento);
		evento.setAdjuntoEventos(adjuntos);
		iEvento.saveAndFlush(evento);
		
		
		return evento;
		
    }
		
	
}