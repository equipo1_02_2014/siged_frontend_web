package edu.ucla.siged.servicio.impl.gestionatletas.rest;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.ucla.siged.dao.DaoPostulanteI;
import edu.ucla.siged.domain.gestionatleta.Postulante;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.Paginacion;
import edu.ucla.siged.domain.utilidades.RespuestaServidor;

@Controller
@RequestMapping(value="/postulante")
public class ServicioPostulanteRest {

	@Autowired
	private DaoPostulanteI iPostulante;
	
	MutableSortDefinition sort = new MutableSortDefinition("fechaPostulacion", true, true);
	
	@RequestMapping(value="/todos")
	public @ResponseBody List<Object> todos(@RequestParam(value="page", required=true, defaultValue="0") Integer page,
			   								@RequestParam(value="limit", required=true) Integer limit)
	{
		return Paginacion.paginar(Postulante.class, iPostulante.findAll(), sort, page, limit);
		
	}
	
	@RequestMapping(value="/insertar")
	public @ResponseBody Object insertar(@RequestParam(value="asmatico", required=true) Boolean asmatico,
			@RequestParam(value="retardo", required=true) Boolean retardo,
			@RequestParam(value="sordoMudo", required=true) Boolean sordoMudo,
			@RequestParam(value="intervencionQuirurgica", required=true) Boolean intervencionQuirurgica,
			@RequestParam(value="descripcionIntervencion", required=true) String descripcionIntervencion,
			@RequestParam(value="fechaPostulacion", required=true) @DateTimeFormat(pattern="yyyy-MM-dd") Date fechaPostulacion,
			@RequestParam(value="fechaRespuesta", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") Date fechaRespuesta,
			@RequestParam(value="observacion", required=false) String observacion,
			@RequestParam(value="autorRespuesta", required=false) String autorRespuesta,
			@RequestParam(value="respuesta", required=false) String respuesta,
			@RequestParam(value="aprobacion", required=false) Boolean aprobacion,
			@RequestParam(value="cedula", required=true) String cedula,
			@RequestParam(value="nombre", required=true) String nombre,
			@RequestParam(value="apellido", required=true) String apellido,
			@RequestParam(value="telefono", required=true) String telefono,
			@RequestParam(value="celular", required=true) String celular,
			@RequestParam(value="direccion", required=true) String direccion,
			@RequestParam(value="fechaNacimiento", required=true) @DateTimeFormat(pattern="yyyy-MM-dd") Date fechaNacimiento,
			@RequestParam(value="lugarNacimiento", required=true) String lugarNacimiento,
			@RequestParam(value="email", required=true) String email) 
			
	{
		
		short estatus = (short) 1;

		Postulante postulante = new Postulante(cedula,nombre,apellido,
											   fechaNacimiento, lugarNacimiento, direccion,
											   telefono,celular,email, null, asmatico,retardo,sordoMudo,intervencionQuirurgica,
											   descripcionIntervencion, observacion,
											   fechaPostulacion,estatus);
		
		RespuestaServidor response = new RespuestaServidor();
		
		try {
			
			iPostulante.saveAndFlush(postulante);
			response.setOk(true);
			response.setMensaje("Postulante Registrado Satisfactoriamente.");
			
		} catch (Exception e) {
			
			response.setOk(false);
			response.setMensaje(e.getMessage());
		}
				
		return response;
	}
	
	

}
