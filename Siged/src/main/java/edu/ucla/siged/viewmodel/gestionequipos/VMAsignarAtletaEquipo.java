package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstatusAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;

public class VMAsignarAtletaEquipo {
	
	@WireVariable ServicioEquipoI servicioEquipo;
	@WireVariable ServicioAtletaI servicioAtleta;
	@WireVariable ServicioAtletaEquipoI servicioAtletaEquipo;
	@WireVariable ServicioEstatusAtletaI servicioEstatusAtleta;
	@WireVariable Equipo equipoSeleccionado;
	@WireVariable List<Equipo> listaEquipos;
	@WireVariable private Atleta atleta;
	@WireVariable private Equipo equipo;
	@WireVariable private int cuposTotales;
	
	Set<Atleta>		selection1;
	List<Atleta>	atletasDisponibles;
	Set<Atleta>		selection2;
	List<Atleta>	atletasAsignados;
	List<Atleta> 	atletasOriginal;
	
	private EstatusAtleta estatusAtleta;
	private boolean registroExitoso;
	

	//	--------------------------------------------------------------------------------------------------------- Inicio de Métodos Getters and Setters	
	public Equipo getEquipoSeleccionado() { return equipoSeleccionado; }
	public void setEquipoSeleccionado(Equipo equipoSeleccionado) { this.equipoSeleccionado = equipoSeleccionado; }

	public List<Equipo> getListaEquipos() { return listaEquipos; }
	public void setListaEquipos(List<Equipo> listaEquipos) { this.listaEquipos = listaEquipos; }
	
	public Equipo getEquipo() {	return equipo; }
	public void setEquipo(Equipo equipo) { this.equipo = equipo; }	
	
	public Set<Atleta> getSelection1() { return selection1; }
	public void setSelection1(Set<Atleta> selection1) { this.selection1 = selection1; }
	
	public List<Atleta> getAtletasDisponibles() { return atletasDisponibles; }
	public void setAtletasDisponibles(List<Atleta> atletasDisponibles) { this.atletasDisponibles = atletasDisponibles; }
	
	public Set<Atleta> getSelection2() { return selection2; }
	public void setSelection2(Set<Atleta> selection2) { this.selection2 = selection2; }
	
	public List<Atleta> getAtletasAsignados() {	return atletasAsignados; }
	public void setAtletasAsignados(List<Atleta> atletasAsignados) { this.atletasAsignados = atletasAsignados; }
	
	public Atleta getAtleta() { return atleta; }
	public void setAtleta(Atleta atleta) { this.atleta = atleta; }
	
	public EstatusAtleta getEstatusAtleta() { return estatusAtleta; }
	public void setEstatusAtleta(EstatusAtleta estatusAtleta) { this.estatusAtleta = estatusAtleta; }
	
	public List<Atleta> getAtletasOriginal() { return atletasOriginal; }
	public void setAtletasOriginal(List<Atleta> atletasOriginal) { this.atletasOriginal = atletasOriginal; }
		
	public int getCuposTotales() { return cuposTotales; }
	public void setCuposTotales(int cuposTotales) { this.cuposTotales = cuposTotales; }
	
	public boolean isRegistroExitoso() { return registroExitoso; }
	public void setRegistroExitoso(boolean registroExitoso) { this.registroExitoso = registroExitoso; }
// --------------------------------------------------------------------------------------------------------- Fin de Métodos Getters and Setters	
	
	
	
	@Init
	public void inicializar(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("equipo") Equipo equipoSeleccionado){
		if(equipoSeleccionado != null){
			this.equipo = equipoSeleccionado;
			cuposTotales = equipo.getCuposTotales();
			atletasDisponibles = servicioAtleta.obtenerAtletasValidos(equipo.getCategoria());
			atletasAsignados = servicioEquipo.obtenerAtletasPorEquipo(equipo);
			selection1 = new HashSet<Atleta>();
			selection2 = new HashSet<Atleta>();
			
			if(atletasAsignados != null){
				atletasOriginal = new ArrayList<Atleta>();
				for(Atleta atleta:atletasAsignados){
					atletasOriginal.add(atleta);
				}
			}
			
		}else{
			Messagebox.show( "Debe Seleccionar un Equipo para asignar atletas!","", Messagebox.OK, Messagebox.ERROR );
		}
	}
	
	
	

	@Command
	@NotifyChange({"atletasDisponibles","atletasAsignados", "selection1", "selection2"})
	public void asignarAtleta(){
		if( selection1 != null && selection1.size() >0 ){
			atletasAsignados.addAll(selection1);
			atletasDisponibles.removeAll(selection1);
			selection2.addAll(selection1);
			selection1.clear();
		}
		else
		{
			Messagebox.show( "Debe Seleccionar un Atleta para asignar!","", Messagebox.OK, Messagebox.ERROR );
		}
	}
	
	
	@Command
	@NotifyChange({"atletasDisponibles","atletasAsignados", "selection1", "selection2"})
	public void removerAtleta(){
		if(selection2 != null && selection2.size() >0 ){
			atletasDisponibles.addAll(selection2);
			atletasAsignados.removeAll(selection2);
			selection1.addAll(selection2);
			selection2.clear();				
		}else{
			Messagebox.show( "Debe Seleccionar un Atleta para removerlo!","", Messagebox.OK, Messagebox.ERROR );
		}				
	}
	
	
	@Command
	@NotifyChange({"atletasAsignados"})
	public void guardar(){
		
		Collection<Atleta> collectionAsignados = atletasAsignados;	
		Collection<Atleta> collectionOriginal = atletasOriginal;
		Collection<Atleta> collectionDisponibles = atletasDisponibles;						
		Collection<Atleta> atletasAGuardar = new HashSet<Atleta>();
		Collection<Atleta> atletasAEliminar = new HashSet<Atleta>();
		
		atletasAGuardar.addAll(collectionAsignados);
		atletasAGuardar.removeAll(collectionOriginal);		
		atletasAEliminar.addAll(collectionOriginal);
		atletasAEliminar.retainAll(collectionDisponibles);

		//System.out.printf("Asignados:%s%nOriginal:%s%nEliminar:%s%nGuardar:%s%n", collectionAsignados, collectionOriginal, atletasAEliminar, atletasAGuardar);
				
		Date fechaActual = new Date();
		
		for(Atleta atleta:atletasAGuardar){						
			try{
				if (this.cuposTotales > 0){
					servicioAtleta.estatusAsignarAtletaEquipo(atleta);
				
					AtletaEquipo atletaEquipo = new AtletaEquipo(atleta, fechaActual, this.equipo);
					servicioAtletaEquipo.guardarAtletaEquipo(atletaEquipo);	
				
				
					this.cuposTotales -= 1;
					equipo.setCuposTotales(cuposTotales);
				
					servicioEquipo.guardarEquipo(this.equipo);
				
					this.registroExitoso = true;
				}else{
					Messagebox.show("No hay cupos disponibles en este equipo", "Error", Messagebox.OK, Messagebox.ERROR);	
				}
				
			}catch(Exception e){
				Messagebox.show("Error al actualizar atleta", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		}
		atletasAGuardar.clear();
		
		for(Atleta atleta:atletasAEliminar){
			try{									
				servicioAtleta.estatusRetirarAtletaEquipo(atleta);			

				servicioAtletaEquipo.eliminarAtletaEquipo(atleta);
			    
				this.cuposTotales += 1;
				equipo.setCuposTotales(cuposTotales);

				servicioEquipo.guardarEquipo(this.equipo);
				
				this.registroExitoso = true;

			}catch(Exception e){
				Messagebox.show("Error al actualizar atleta", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		}
		
		atletasAEliminar.clear();
			
		if(registroExitoso==true){
			
			Messagebox.show("Asignación de Atletas Actualizada", "Información", Messagebox.OK, Messagebox.INFORMATION);
			
			Messagebox.show("Desea Asignar otro Atleta?","Confirmacion",
				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				         	new org.zkoss.zk.ui.event.EventListener<Event>() {
						public void onEvent(
								Event evt)
								throws Exception {
							
							if (evt.getName().equalsIgnoreCase("onYes")) {
								BindUtils.postGlobalCommand(null, null, "limpiarCampos", null);
							}else{
								 BindUtils.postGlobalCommand(null, null, "paginar", null);
	        		        	 Window myWindow = (Window) Path.getComponent("/wndGestionEquipoAsignarAtletaEquipo");
	        		        	 myWindow.detach();
							}
						}
	           });

		}
		
	} // fin del método
	
	
	

	@GlobalCommand("limpiarCampos")
	@NotifyChange({"atletasAsignados","atletasDisponibles","atletasOriginal","selection1", "selection2","cuposTotales","registroExitoso","equipo"})
	public void limpiarCampos(){
		this.registroExitoso = false;
		atletasAsignados = null;
		atletasDisponibles = null;
		selection1 = null;
		selection2 = null;
		System.out.println("--------------------------------------------------- if limpiando");
		
		atletasDisponibles = servicioAtleta.obtenerAtletasValidos(equipo.getCategoria());
		atletasAsignados = servicioEquipo.obtenerAtletasPorEquipo(equipo);
		selection1 = new HashSet<Atleta>();
		selection2 = new HashSet<Atleta>();
	
		if(atletasAsignados != null){
			atletasOriginal = new ArrayList<Atleta>();
			for(Atleta atleta:atletasAsignados){
				atletasOriginal.add(atleta);
			}
		}

		
		
	}
	
	
	
	
	@Command
	public Integer verEdad(Date fn){
		Calendar calendarActual = Calendar.getInstance();
		Calendar calendarFechaNacimiento = Calendar.getInstance();
		calendarFechaNacimiento.setTime(fn);
		
		int edadAtleta = calendarActual.get(Calendar.YEAR) - calendarFechaNacimiento.get(Calendar.YEAR);
		if( calendarActual.get(Calendar.MONTH) < calendarFechaNacimiento.get(Calendar.MONTH) ){ --edadAtleta; }
		else if(calendarActual.get(Calendar.MONTH) == calendarFechaNacimiento.get(Calendar.MONTH) ){
			if (calendarActual.get(Calendar.DAY_OF_MONTH) < calendarFechaNacimiento.get(Calendar.DAY_OF_MONTH)){ --edadAtleta; }
		}
		return edadAtleta;
	}
	
	@Command
	public void cerrarVentana(){
		registroExitoso = false;
		BindUtils.postGlobalCommand(null, null, "paginar", null);
	}
}