package edu.ucla.siged.viewmodel.portalprincipal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import edu.ucla.siged.domain.gestioneventos.Sugerencia;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioSugerenciaI;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

public class VMListaContacto { 
	
		@WireVariable
		ServicioSugerenciaI servicioSugerencia;
		@WireVariable 
		Sugerencia sugerencia;
		List<Sugerencia> listaSugerencias;
		List<Sugerencia> sugerenciasSeleccionadas;
		Window window;
		Long registrosTotales;
		int paginaActual;
		int tamanoPagina;

		public Sugerencia getSugerencia() {
			return sugerencia;
		}

		public void setSugerencia(Sugerencia sugerencia) {
			this.sugerencia = sugerencia;
		}

		public List<Sugerencia> getListaSugerencias() {
			return listaSugerencias;
		}

		public void setListaSugerencias(List<Sugerencia> listaSugerencias) {
			this.listaSugerencias = listaSugerencias;
		}

		public List<Sugerencia> getSugerenciasSeleccionadas() {
			return sugerenciasSeleccionadas;
		}

		public void setSugerenciasSeleccionadas(
				List<Sugerencia> sugerenciasSeleccionadas) {
			this.sugerenciasSeleccionadas = sugerenciasSeleccionadas;
		}
		
		public int getPaginaActual() {
			return paginaActual;
		}

		public void setPaginaActual(int paginaActual) {
			this.paginaActual = paginaActual;
		}

		public int getTamanoPagina() {
			this.tamanoPagina=servicioSugerencia.TAMANO_PAGINA;
			return tamanoPagina;
		}

		public void setTamanoPagina(int tamanoPagina) {
			this.tamanoPagina = tamanoPagina;
		}
		
		public Long getRegistrosTotales() {
			return registrosTotales;
		}

		public void setRegistrosTotales(Long registrosTotales) {
			this.registrosTotales = registrosTotales;
		}
		
		@Init
	    public void init() {
			paginaActual = 0;
			this.listaSugerencias = servicioSugerencia.buscarSugerenciaPendiente(paginaActual);
			this.registrosTotales = servicioSugerencia.totalSugerencias();
	    }
			
		@Command
		@NotifyChange({"sugerencia"})
		public void responderSugerencia(@BindingParam("sugerenciaSeleccionada") Sugerencia sugerenciaSeleccionada) {
			this.sugerencia = sugerenciaSeleccionada;
			Map<String, Sugerencia> mapa = new HashMap<String, Sugerencia>();
	        mapa.put("sugerencia", sugerenciaSeleccionada);
			window = (Window)Executions.createComponents("/vistas/gestionencuesta/responderContacto.zul", null, mapa);
			window.doModal();
		}
		
		public String formatearFecha(Date fecha){
			String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
			return fechaFormateada;
		}
		
		
		@GlobalCommand
		@NotifyChange({"listaSugerencias","registrosTotales","paginaActual","sugerenciasSeleccionadas"})
		public void paginar(){
			this.registrosTotales = servicioSugerencia.totalSugerenciasPendientes();
			this.listaSugerencias = servicioSugerencia.buscarSugerenciaPendiente(this.paginaActual);
		}
		
		@GlobalCommand
		@NotifyChange({"listaSugerencias","registrosTotales","paginaActual"})
	    public void actualizarLista(){
			int ultimaPagina=0;
			this.getRegistrosTotales();
			if ((this.registrosTotales+1)%this.tamanoPagina==0)
				ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			else
				ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
			this.paginaActual=ultimaPagina;
			paginar();
		}
}