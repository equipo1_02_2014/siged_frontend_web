package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.reporte.CausaInasistencia;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteAsistenciaI;

public class VMCausaInasistenciaPractica{
		
		@WireVariable
	    private ServicioCategoriaI servicioCategoria;
	    
	    @WireVariable
	    private ServicioRangoI servicioRango;
	    
	    @WireVariable
	    private ServicioReporteAsistenciaI servicioReporteAsistencia;
	    
	    @WireVariable
	    private ServicioEquipoI servicioEquipo;
	    
	    private List<Equipo> listaEquiposFundacion;
	    private Equipo equipoSeleccionado;
		private Rango rangoSeleccionado;
		private Categoria categoriaSeleccionada;
		private boolean todos;
		private boolean becados;
		private boolean noBecados;
		private Date fechaDesde;
		private Date fechaHasta;
		private String pathProyecto;
		private List<Rango> listaRango;
		private List<Categoria> listaCategoria;
		private List<Integer> listaCantidadEquipos;
		private Window window;
					
		@Init
		public void init(){
		
			this.setWindow(null);
			this.listaEquiposFundacion= servicioEquipo.buscarEquiposTrue();
			this.listaCategoria= servicioCategoria.buscarTodos();
			this.listaRango= servicioRango.buscarTodos();
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMCausaInasistenciaPractica.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			this.inicializarCampos();
			
		}
		
		@NotifyChange({"todos", "becados","noBecados","fechaDesde","fechaHasta","categoriaSeleccionada",
			           "rangoSeleccionado","cantidadEquipoSeleccionada"})
		public void inicializarCampos(){
			
			this.setTodos(true);
			this.setBecados(false);
			this.setNoBecados(false);
			this.fechaDesde=null;
			this.fechaHasta=null;
			this.equipoSeleccionado=null;
			
		}
		
		@Command
		@NotifyChange({"todos","becados","noBecados", "fechaDesde","fechaHasta","equipoSeleccionado"})
		public void cancelar(){
			this.inicializarCampos();
		}
				
		
		@Command
		public void imprimir(){		
			
			String restricciones="";
			String filtros="";
			String filtroBecados="";

			if (this.fechaDesde!=null && this.fechaHasta!=null){
				
				if(this.fechaDesde.compareTo(this.fechaHasta) == 0 || this.fechaDesde.compareTo(this.fechaHasta) == -1){
				
					if((this.fechaDesde!=null && this.fechaHasta!=null) || this.equipoSeleccionado!=null || this.rangoSeleccionado!=null || this.categoriaSeleccionada!=null){	
						
							if (this.fechaDesde!=null && this.fechaHasta!=null){
								
								String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
								String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
								restricciones += " AND p1.fecha BETWEEN '" + desde + "' AND '" + hasta + "'";
								
								desde= new SimpleDateFormat("dd/MM/YYYY").format(this.fechaDesde);
								hasta= new SimpleDateFormat("dd/MM/YYYY").format(this.fechaHasta);
								filtros += "Fecha desde " + desde.toString() + " hasta " + hasta.toString();
								
							}
																			
							if (this.equipoSeleccionado!=null){
								restricciones += " AND e1.id=" + this.equipoSeleccionado.getId();
								filtros += " Equipo: " + this.equipoSeleccionado.getNombre();
							}
							

							if (this.categoriaSeleccionada!=null){
								restricciones += " AND e1.idcategoria=" + this.categoriaSeleccionada.getId();
								filtros += " Categoria: " + this.categoriaSeleccionada.getNombre();
							}
							
							if (this.rangoSeleccionado!=null){
								restricciones += " AND e1.idrango=" + this.rangoSeleccionado.getId();
								filtros += " Rango: " + this.rangoSeleccionado.getDescripcion();
							}
							
							if(todos){
								filtros += " Atletas: Todos ";
							}
							
							if(becados){								
								filtroBecados = " AND aa2.idatleta IN (SELECT ha.idatleta FROM historiaayuda ha WHERE ha.fechafin >= current_date) ";
								filtros += " Atletas: Becados ";
							}
							
							if(noBecados){								
								filtroBecados = " AND " + 
								" aa2.idatleta NOT IN (SELECT ha.idatleta FROM historiaayuda ha WHERE ha.fechafin >= current_date) " + 
								" or aa2.idatleta NOT IN (SELECT ha.idatleta FROM historiaayuda ha WHERE ha.idatleta = aa2.idatleta) ";
								filtros += " Atletas: No becados ";
							}
							
					}
					
					//restricciones="";
					List<CausaInasistencia> lista= this.servicioReporteAsistencia.buscarCausaInasistencia(restricciones, filtroBecados, "" );
								
					
					if (!lista.isEmpty()){
						
						Map<String,Object> parameterMap = new HashMap<String,Object>();
						
						JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
						
						parameterMap.put("datasource", JRdataSource);
						
						parameterMap.put("filtros",filtros);
						
						parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
						
						parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
						
						parameterMap.put("titulo","Causas de inasistencia de atletas a prácticas");
						
						
						try {
							
							JasperDesign jasDesign=null;
							
							jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "reporteCausaInasistenciaTorta.jrxml");
							
							JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
					        
					        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
					        
					        JasperViewer.viewReport(jasperPrint,false);
							
						} catch (JRException e) {
							e.printStackTrace();
							Messagebox.show(e.getMessage());
						}
					
					
	
					}else{
						Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
				                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
					}
					
				}else{
					
					Messagebox.show("La fecha 'desde'debe ser menor o igual a la fecha 'hasta'", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
					
				}
				
			}else{
				
				Messagebox.show("Debe seleccionar el rango de fechas", 
		                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				
			}
		
		}
		
		@Command
		@NotifyChange({"asistente","noAsistente"})
		public void seleccionarVista(@BindingParam("vista")String vista){
			
			this.setTodos(false);
			this.setBecados(false);
			this.setNoBecados(false);
			
			if (vista.equalsIgnoreCase("Todos")){
				this.setTodos(true);
			}
			else if(vista.equalsIgnoreCase("Becados")){
				this.setBecados(true);
			}
			else if(vista.equalsIgnoreCase("No becados")){
				this.setNoBecados(true);
			}
			
		}
		
		
		public Date getFechaDesde() {
			return fechaDesde;
		}

		public void setFechaDesde(Date fechaDesde) {
			this.fechaDesde = fechaDesde;
		}

		public Date getFechaHasta() {
			return fechaHasta;
		}

		public void setFechaHasta(Date fechaHasta) {
			this.fechaHasta = fechaHasta;
		}

		public Rango getRangoSeleccionado() {
			return rangoSeleccionado;
		}

		public void setRangoSeleccionado(Rango rangoSeleccionado) {
			this.rangoSeleccionado = rangoSeleccionado;
		}

		public Categoria getCategoriaSeleccionada() {
			return categoriaSeleccionada;
		}

		public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) {
			this.categoriaSeleccionada = categoriaSeleccionada;
		}
		
		public List<Rango> getListaRango() {
			return listaRango;
		}

		public List<Categoria> getListaCategoria() {
			return listaCategoria;
		}

		public List<Integer> getListaCantidadEquipos() {
			return listaCantidadEquipos;
		}

		public Window getWindow() {
			return window;
		}

		public void setWindow(Window window) {
			this.window = window;
		}

		public List<Equipo> getListaEquiposFundacion() {
			return listaEquiposFundacion;
		}

		public void setListaEquiposFundacion(List<Equipo> listaEquiposFundacion) {
			this.listaEquiposFundacion = listaEquiposFundacion;
		}

		public Equipo getEquipoSeleccionado() {
			return equipoSeleccionado;
		}

		public void setEquipoSeleccionado(Equipo equipoSeleccionado) {
			this.equipoSeleccionado = equipoSeleccionado;
		}

		public boolean isTodos() {
			return todos;
		}

		public void setTodos(boolean todos) {
			this.todos = todos;
		}

		public boolean isBecados() {
			return becados;
		}

		public void setBecados(boolean becados) {
			this.becados = becados;
		}

		public boolean isNoBecados() {
			return noBecados;
		}

		public void setNoBecados(boolean noBecados) {
			this.noBecados = noBecados;
		}
		
}
