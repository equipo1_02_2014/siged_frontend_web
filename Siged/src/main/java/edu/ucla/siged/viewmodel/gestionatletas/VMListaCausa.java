/** 
 * 	VMListaCausa
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionatletas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioCausaI;

public class VMListaCausa {
	
	@WireVariable
	ServicioCausaI servicioCausa;
	@WireVariable
	Causa causa;
	List<Causa> listaCausas;
	List<Causa> causasSeleccionadas;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String descripcion=""; 
	String nombre="";
	Integer tipoCombo=0;
	boolean estatusFiltro;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	
	public Causa getCausa() {
		return causa;
	}
	
	public void setCausa(Causa causa) {
		this.causa = causa;
	}
	
	public List<Causa> getCausasSeleccionados() {
		return causasSeleccionadas;
	}
	
	public void setCausasSeleccionados(List<Causa> causasSeleccionados) {
		this.causasSeleccionadas = causasSeleccionados;
	}
	
	public List<Causa> getListaCausas() {
		return listaCausas;
	}
	
	public void setListaCausa(List<Causa> listaCausas) {
		this.listaCausas = listaCausas;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioCausa.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Integer getTipoCombo() {
		return tipoCombo;
	}
	public void setTipoCombo(Integer tipoCombo) {
		this.tipoCombo = tipoCombo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	@Init
	public void init(){
			this.listaCausas = servicioCausa.buscarTodos(0).getContent();
			this.registrosTotales= servicioCausa.totalCausas();
		}

	////////////////////////////////////////AGREGAR CAUSA///////////////////////////////////////////////////////////
	
	@Command
	public void agregarCausa() {
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestionatleta/causa.zul", null, null);
		window.doModal();
	}
	
	/////////////////////////////////////////ELIMINAR CAUSA////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaCausas"})
	public void eliminarCausa(@BindingParam("causaSeleccionada") final Causa causaSeleccionada){
		this.tipoOperacion=3;
		Messagebox.show("�Est� seguro que desea eliminar este registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						if(Messagebox.ON_YES.equals(event.getName())){
							servicioCausa.eliminar(causaSeleccionada);
							BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
							Messagebox.show("El registro se ha eliminado exitosamente.","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
							actualizarLista();
						}
					}
				   });
	actualizarLista();
	}
	
	///////////////////////////////////////////ELIMINAR VARIAS CAUSAS//////////////////////////////////////////////
	@Command
	@NotifyChange("listaCausas")
	public void eliminarCausas(@BindingParam("lista") Listbox lista){
		this.tipoOperacion=4;
		boolean ocupados=false;
		
		if(lista.getSelectedItems().size()>0){
		
			for (Causa causa:causasSeleccionadas){
			  if(servicioCausa.verificarEliminar(causa)){
				ocupados=true;
					Messagebox.show("La causa " + causa.getNombre() + "  esta asociada a otros registros y no puede ser eliminada","Precauci�n", Messagebox.OK, Messagebox.ERROR);
					break;
				}
			}
		
			if(!ocupados){
				Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
						new EventListener<Event>() {
							public void onEvent(Event event) throws Exception {
								if(Messagebox.ON_YES.equals(event.getName())){
									servicioCausa.eliminarVarios(causasSeleccionadas);
									BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
									Messagebox.show("Los registros se han eliminado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
									causasSeleccionadas=null;
								}
							}
						});
					
				
				}
			
		}
		else Messagebox.show("Debe seleccionar los registros que desea eliminar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
		}   
	
	/////////////////////////////////////////////EDITAR CAUSA/////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"causa"})
	public void editarCausa(@BindingParam("causaSeleccionado") Causa causaSeleccionado ) {
		this.causa=causaSeleccionado;
		this.tipoOperacion=2;
		//esta es la manera de pasar a la otra vista el tecnico seleccionado
		Map<String, Causa> mapa = new HashMap<String, Causa>();
        mapa.put("causa", causaSeleccionado);
		
		window = (Window)Executions.createComponents("/vistas/gestionatleta/causa.zul", null, mapa);
		window.doModal();
	}
	//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaCausas","registrosTotales","paginaActual","causasSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaCausas = servicioCausa.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioCausa.totalCausas();
		
		}else{
			ejecutarFiltro();
		     }

	}
	
	////////////////////////////////////////ACTUALIZAR LISTA/////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaCausas","registrosTotales","paginaActual"})
    public void actualizarLista(){
		int ultimaPagina=0;
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   
		   if ((this.registrosTotales+1)%this.tamanoPagina==0)
		      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
		   else
			  ultimaPagina =((int) (this.registrosTotales+1)/this.tamanoPagina);
		   this.paginaActual=ultimaPagina; // Se coloca en la �ltima p�gina para que quede visible el que se acaba de ingresar.
		}
		   paginar();
		 
		   if(this.tipoOperacion==3 || this.tipoOperacion==4){
			   this.getRegistrosTotales();
			   
			   if ((this.registrosTotales+1)%this.tamanoPagina==0)
			      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			   else
				  ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina;
			 paginar();
			}	
	}
	
    //////////////////////////////////////////////VER ESTATUS///////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaTecnicos"})
	public String verTipoEnLista(Integer tipoEnLista){
		String tip="POSTULACION";
		switch (tipoEnLista){
		case 1:
		tip="PRACTICA";
		break;
		default:
		tip="POSTULACON";
	}
		return tip;
	}
	
    ///////////////////////////////////////////////EJECUTAR FILTRO/////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaCausas","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroTipo;
		
		this.estatusFiltro=true;  //el filtro se ha activado
		if(tipoCombo==0)
			filtroTipo="(1,2)";
		else
			filtroTipo="("+(tipoCombo)+")";
		
		if (descripcion == null)
			jpql = " tipo in "+filtroTipo+" and  nombre like '%"+nombre+"%'";
		else
		   jpql = "descripcion like '%"+descripcion+"%' and tipo in "+filtroTipo+" and nombre like '%"+nombre+"%'";
			
		this.listaCausas=servicioCausa.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioCausa.totalCausasFiltrados(jpql);
		
	}
	
	@Command
	@NotifyChange({"listaCausas","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
    /////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	
	
	@Command
	@NotifyChange({"listaCausas","registrosTotales","paginaActual","causasSeleccionados",
				   "descripcion", "tipoCombo", "nombre"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		descripcion=""; //  variables del filtro
		tipoCombo=0;
		nombre="";
	}

}