package edu.ucla.siged.viewmodel.gestionatletas;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestionatleta.Documento;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.domain.gestionatleta.Reposo;
import edu.ucla.siged.domain.utilidades.Archivo;

public class VMReposo {
	
	private String descripcion;
	private Date fechaInicio = new Date();
	private Date fechaFin = new Date();
	private Documento documento;
	private @WireVariable Reposo reposo;
	private AMedia media;
	private boolean instanciado = false;
	private Window ventanaReposo;
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("reposo") Reposo reposoSeleccionado){
		Selectors.wireComponents(view, this, false);
		if(reposoSeleccionado!=null){
			this.reposo = reposoSeleccionado;
			this.descripcion = reposoSeleccionado.getDescripcion();
			this.fechaInicio = reposoSeleccionado.getFechaInicio();
			this.fechaFin = reposoSeleccionado.getFechaFin();
			if(reposoSeleccionado.getDocumento().getDocumento().getTamano() > 0){
				this.documento = reposoSeleccionado.getDocumento();
				String tipoDocumento = this.documento.getDocumento().getTipo().split("/")[1];
				
				media = new AMedia(this.documento.getDocumento().getNombreArchivo(),
								   this.documento.getDocumento().getTipo(),
								   tipoDocumento, 
								   this.documento.getDocumento().getContenido());
			}
			instanciado = true;
		}
	}
	
	@Command
	@NotifyChange({"descripcion","fechaInicio","fechaFin","documento","media"})
	public void cargarDocumento(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media = event.getMedia();
		
		
		if (media!=null){
			if(media.getContentType().equals("image/jpeg") ||
			   media.getContentType().equals("image/png") ||
			   media.getContentType().equals("application/pdf")){
			
				Archivo archivoDocumento = new Archivo();
				archivoDocumento.setNombreArchivo(media.getName());
				archivoDocumento.setTipo(media.getContentType());
				archivoDocumento.setContenido(media.getByteData());
				this.documento = new Documento();
				this.documento.setDocumento(archivoDocumento);

				String tipoDocumento = media.getContentType().split("/")[1];

				this.media = new AMedia(media.getName(), media.getContentType(), tipoDocumento, media.getByteData());
				//this.media = (AMedia) media;
			}else{
				Messagebox.show("Solo se permite Adjuntar los formatos PNG,JPEG Y PDF","Error",Messagebox.OK, Messagebox.ERROR);
			}
		}
	}
	
	@Command
	@NotifyChange({"reposo","descripcion","fechaInicio","fechaFin","documento","media"})
	public void cargarReposo(@BindingParam("ventanaReposo") Window ventanaReposo,@SelectorParam("textbox,datebox") LinkedList<InputElement> inputs){
        boolean camposValidos=true;
		
		for(InputElement input:inputs){
			
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
			
		}
		if(camposValidos){
			if(this.documento != null){
				this.ventanaReposo = ventanaReposo;
				if(!instanciado){
					reposo = new Reposo(this.descripcion, this.fechaInicio, this.fechaFin, this.documento);
				}else{
					reposo.setDescripcion(this.descripcion);
					reposo.setFechaInicio(this.fechaInicio);
					reposo.setFechaFin(this.fechaFin);
					reposo.setDocumento(this.documento);
					instanciado = false;
				}
	//			Map<String, Reposo> mapReposo = new HashMap<String, Reposo>();
	//			mapReposo.put("reposo", reposo);
	//			BindUtils.postGlobalCommand(null, null, "agregarReposo", mapReposo);
				
				limpiar();
				
				Messagebox.show("La Historia Mediaca o Reposo se ha guardado, desea agregar otro?", "Pregunata", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
						new EventListener<Event>() {
							
							public void onEvent(Event event) throws Exception {
								// TODO Auto-generated method stub
								if(Messagebox.ON_NO.equals(event.getName())){
									cerrarVentanaReposo();
								}
							}
						}
				);
			}else{
				Messagebox.show("Seleccione algun Archivo", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		}else{
			Messagebox.show("Campos No Validos", "Error", Messagebox.OK, Messagebox.ERROR);
		}
		
	}
	public void cerrarVentanaReposo(){
		ventanaReposo.detach();
	}
	@Command
	@NotifyChange({"descripcion","fechaInicio","fechaFin","documento","media"})
	public void limpiar(){
		
		this.descripcion ="";
		this.documento = null;
		this.fechaInicio = new Date();
		this.fechaFin = new Date();
		media = null;
	}
	
	@Command
	public void descargarDocumento(){
		Filedownload.save(this.documento.getDocumento().getContenido(),this.documento.getDocumento().getTipo(),this.getDocumento().getDocumento().getNombreArchivo());
	}
	

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public AMedia getMedia() {
		return media;
	}

	public void setMedia(AMedia media) {
		this.media = media;
	}

	public Reposo getReposo() {
		return reposo;
	}

	public void setReposo(Reposo reposo) {
		this.reposo = reposo;
	}
	
	
	
	
}
