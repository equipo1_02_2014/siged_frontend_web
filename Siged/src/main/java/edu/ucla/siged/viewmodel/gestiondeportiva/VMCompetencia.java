package edu.ucla.siged.viewmodel.gestiondeportiva;

import org.zkoss.bind.annotation.BindingParam;
import java.util.List;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;

public class VMCompetencia {
	@WireVariable
	ServicioCompetenciaI servicioCompetencia;
	@WireVariable
	Competencia competencia;
	@WireVariable
	TipoCompetencia tipoCompetenciaSeleccionado;
	List<TipoCompetencia> listaTipoCompetencia;
	Window window;
	
//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////
	
	public Competencia getCompetencia() {
		return competencia;
	}
	
	public void setCompetencia(Competencia competencia) {
		this.competencia = competencia;
	}

	public List<TipoCompetencia> getListaTipoCompetencia() {
		return listaTipoCompetencia;
	}

	public void setListaTipoCompetencia(List<TipoCompetencia> listaTipoCompetencia) {
		this.listaTipoCompetencia = listaTipoCompetencia;
	}
	
	public TipoCompetencia getTipoCompetenciaSeleccionado() {
		return tipoCompetenciaSeleccionado;
	}

	public void setTipoCompetenciaSeleccionado(TipoCompetencia tipoCompetenciaSeleccionado) {
		this.tipoCompetenciaSeleccionado = tipoCompetenciaSeleccionado;
	}
	
	
/////////////////////////////////////// INIT////////////////////////////////////////////////////////
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("competencia") Competencia competenciaSeleccionado) {
            listaTipoCompetencia = servicioCompetencia.buscarTodosTipoCompetencia();
            Selectors.wireComponents(view, this, false);
        if (competenciaSeleccionado!=null){
            this.competencia = competenciaSeleccionado;  
        }
        else{
        	competencia = new Competencia();
        	//this.tipoCompetenciaSeleccionado= getListaTipoCompetencia().get(0);
        }
    }
	
////////////////////////////////////////////GUARDAR/////////////////////////////////////////////////////////////
	@Command
	@NotifyChange({"competencia"})   
	public void guardar(){
		if( tipoCompetenciaSeleccionado != null ){
			competencia.setTipoCompetencia(tipoCompetenciaSeleccionado);
		}
		
		if(competencia.getNombre()==null || competencia.getEdicion()==null || competencia.getFechaInicio()==null || competencia.getFechaFin()==null){
			Messagebox.show("Por favor introduzca todos los datos","", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			if(competencia.getFechaInicio().compareTo(competencia.getFechaFin())>0 || competencia.getFechaInicio().equals(competencia.getFechaFin())){
				Messagebox.show("La fecha de finalizacion debe ser mayor a la fecha de inicio","", Messagebox.OK, Messagebox.INFORMATION);
			}
			else{
				servicioCompetencia.guardar(competencia);
				Messagebox.show("Competencia guardada exitosamente","", Messagebox.OK, Messagebox.INFORMATION);
				Window myWindow = (Window) Path.getComponent("/vistaCompetencia");
				myWindow.detach();
			}	
		}
	}
	
////////////////////////////////////////////////LIMPIAR///////////////////////////////////////////////
	@Command
	@NotifyChange({"competencia"})
	public void limpiar(){
	competencia.setNombre("");
	competencia.setTipoCompetencia(null);
	competencia.setFechaInicio(null);
	competencia.setFechaFin(null);
	competencia.setEdicion(null);	
	}
	
/////////////////////////////////////////////ABRIR FORMULARIO//////////////////////////////////////////////
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("competenciaSeleccionado") Competencia competenciaSeleccionado ) {
		Window window = (Window)Executions.createComponents("competencia.zul", null, null);
		this.competencia = competenciaSeleccionado;
		window.doModal();
	}
	
}