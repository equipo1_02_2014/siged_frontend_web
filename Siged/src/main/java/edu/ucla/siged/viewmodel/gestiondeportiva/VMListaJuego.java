package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;

public class VMListaJuego {
	
	@WireVariable
	private ServicioJuegoI servicioJuego;
	
	private Window window;
	
	private int paginaActual;
	
	private boolean filtrar;
	
	private List<Juego> listaJuegos;

	private List<Integer> listaResultados;
	
	private long registrosTotales;
	
	/* Variables Usadas para Filtrar */
	private String nombreCompetencia;
	
	private Date fechaCompetencia;
	
	private String participantes;
	
	private Integer resultado;

	@Init
	public void init(){
		
		this.paginaActual= 0;
		this.listaJuegos= servicioJuego.buscarTodosCompetenciasActivas(0);
		this.registrosTotales= servicioJuego.obtenerCantidadJuegosCompetenciasActivas();
		
		this.listaResultados= new ArrayList<Integer>();
		
		this.listaResultados.add(-10);
		
		this.listaResultados.add(-1);
		
		this.listaResultados.add(1);
		
		this.listaResultados.add(0);
		
		this.listaResultados.add(2);
		
		this.cancelarFiltro();
	}
	
	@Command
	public void agregarJuego(){
		 window = (Window)Executions.createComponents(
		            "/vistas/gestiondeportiva/juego.zul", null, null);	   
	     window.doModal();
	}
	
	@Command
	public void editarJuego(@BindingParam("juegoEditar")Juego juego){
		
		Sessions.getCurrent().setAttribute("JuegoEditar", juego);
		
		window = (Window)Executions.createComponents(
		            "/vistas/gestiondeportiva/editarjuego.zul", null, null);
		
	    window.doModal();
	    
	}
	
	
	@Command
	@NotifyChange({"filtrar","nombreCompetencia",
		           "fechaCompetencia","participantes",
		           "listaJuegos","registrosTotales","resultado"})
	public void cancelarFiltro(){
		
		this.filtrar=false;
		
		this.nombreCompetencia="";
		
		this.fechaCompetencia=null;
		
		this.participantes="";
		
		this.resultado=-10;
		
		this.paginarJuego();
		
	}
	
	 @GlobalCommand("paginarJuego")
	 @NotifyChange({"listaJuegos","registrosTotales"})
	 public void paginarJuego(){
			if (filtrar==false){
			   this.listaJuegos = servicioJuego.buscarTodosCompetenciasActivas(this.paginaActual);
			   this.registrosTotales= servicioJuego.obtenerCantidadJuegosCompetenciasActivas();
			
			}else{
				
				ejecutarFiltro();
			}

	 }
	 
	 @Command
	 @NotifyChange({"listaJuegos","registrosTotales"})
	 public void ejecutarFiltro(){
		 
		if ((this.nombreCompetencia!=null && !this.nombreCompetencia.isEmpty()) ||
			(this.participantes!=null && !this.participantes.isEmpty()) || 
			(this.resultado!=null && !this.resultado.equals(-10)) ||
			this.fechaCompetencia!=null){
				String tablas="FROM Juego j";
				String condiciones=" WHERE ";
				String jpql="";
				this.filtrar=true;
				
				if (!this.nombreCompetencia.isEmpty()){
					
					tablas += ",Competencia c";
					
					condiciones += "c.estatus=1 AND c.id=j.competencia.id AND c.nombre LIKE '%" + this.nombreCompetencia + "%' AND";
				}
				
				if (!this.participantes.isEmpty()){
					tablas += ",Equipo e";
				    
					
					condiciones += " e.id=j.equipo.id AND ((e.nombre LIKE '%" + this.participantes + "%') OR " + 
					               "(j.equipoContrario LIKE '%" + this.participantes + "%')) AND";
				}
				
				if (this.fechaCompetencia!=null){
					
					String fechaFormateada= new SimpleDateFormat("yyyy-MM-dd").format(this.fechaCompetencia);
					
					condiciones += " j.fecha='" + fechaFormateada + "' AND";
				}
			    
				if (!this.resultado.equals(-10)){
					condiciones += " j.resultado=" + this.resultado.intValue() + " AND";
				}
				
				condiciones= condiciones.substring(0, condiciones.length()-3);
				
				jpql += tablas + condiciones;
				
				//System.out.println("-------------------CONSULTA JPQL-------------------------------: ");
				 // System.out.println(jpql); 
				 // System.out.println("----------------------------------------------------------------: ");				
				
				this.listaJuegos=servicioJuego.buscarFiltrado(jpql,this.paginaActual);
				this.registrosTotales=servicioJuego.obtenerTotalFiltrados(jpql);
				
				
		}
		else{
			 this.listaJuegos = servicioJuego.buscarTodosCompetenciasActivas(this.paginaActual);
			 this.registrosTotales= servicioJuego.obtenerCantidadJuegosCompetenciasActivas();
		}
		 
		 
	 }
	
	public String formatearFechaJuego(Juego juego){
		
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(juego.getFecha());
		
		fechaFormateada += new SimpleDateFormat(" hh:mm a").format(juego.getHoraInicio());;
		
		return fechaFormateada;
	}
	
	public String formatearParticipantes(Juego juego){
		
		String participantes= juego.getEquipo().getNombre() + " VS " + juego.getEquipoContrario();
		
		return participantes;
	}
	
	public String formatearResultado(Integer resultado){
		String stringResultado="";
		
		switch  (resultado){
		    case -1: 
		    	stringResultado="Sin Resultado";
		    	break;
			case 1:
				stringResultado="Ganamos";
				break;
		    case 0:
   			    stringResultado="Perdimos";
			    break;
			case 2:
			    stringResultado="Empatamos";
				break;
		     default:
				stringResultado="Todos";

		}

		return stringResultado;

	  }

			

	public String formatearEstiloResultado(Integer resultado){

		String estiloResultado="color:black";

		switch  (resultado){
		case 1:
		    estiloResultado="color:green";
		    break;
		case 0:
			estiloResultado="color:red";
		    break;
		case 2:
		    estiloResultado="color:blue";
		    break;
		default:
			estiloResultado="color:black";

		}

		return estiloResultado;

	}
	
	
	public boolean visualizarEditar(Juego juego){
		
		boolean visualizar= juego.getResultado().equals(-1);
		
		return visualizar;
	}
	
	public int getTamanioPagina(){
		return servicioJuego.obtenerTamanioPagina();
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}


	public List<Juego> getListaJuegos() {
		return listaJuegos;
	}
	
	public List<Integer> getListaResultados() {
		return listaResultados;
	}

	public void setListaResultados(List<Integer> listaResultados) {
		this.listaResultados = listaResultados;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public String getNombreCompetencia() {
		return nombreCompetencia;
	}

	public void setNombreCompetencia(String nombreCompetencia) {
		this.nombreCompetencia = nombreCompetencia;
	}

	public Date getFechaCompetencia() {
		return fechaCompetencia;
	}

	public void setFechaCompetencia(Date fechaCompetencia) {
		this.fechaCompetencia = fechaCompetencia;
	}

	public String getParticipantes() {
		return participantes;
	}

	public void setParticipantes(String participantes) {
		this.participantes = participantes;
	}

	public Integer getResultado() {
		return resultado;
	}

	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}
	
	
	
}
