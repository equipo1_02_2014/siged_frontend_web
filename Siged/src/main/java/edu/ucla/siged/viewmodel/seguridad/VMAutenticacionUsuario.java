package edu.ucla.siged.viewmodel.seguridad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Messagebox;




import org.zkoss.zul.Window;

import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioUsuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;


public class VMAutenticacionUsuario  {


@WireVariable
ServicioUsuarioI servicioUsuario; // Instancia la interface servicio	

private String account;
private String password;
private AImage fotoSesion;

public VMAutenticacionUsuario() {
	super();
	// TODO Auto-generated constructor stub
}

@Init
public void init() {
	this.account="";
	this.password="";
	 refrescar();	
    
}


public Usuario getUserCredential(){
    Session sess = Sessions.getCurrent();
    Usuario cre = (Usuario)sess.getAttribute("userCredential");
    if(cre==null){
        cre = new Usuario();
        sess.setAttribute("userCredential",cre);
    }
    return cre;
}



public boolean login(String nomb, String pd) {
System.out.println("++++++++++"+nomb+"------pd:"+pd);
nomb="h";
Usuario user = servicioUsuario.buscarByNombreUsuario(nomb);
//a simple plan text password verification
if(user==null || !user.getPassword().equals(pd)){
return false;
}
Session sess = Sessions.getCurrent();
sess.setAttribute("userCredential",user);
//TODO handle the role here for authorization
return true;
}

public void logout() {
Session sess = Sessions.getCurrent();
sess.removeAttribute("userCredential");
}


@Command
public void doLogin(){
    String nm = account;
    String pd = password;
   
    Usuario user = servicioUsuario.buscarByNombreUsuario(nm);
    //a simple plan text password verification
    if(user==null || !user.getPassword().equals(pd)){
    	Messagebox.show("Usuario o clave incorrectos", "Error", Messagebox.OK, Messagebox.ERROR);
        return;
    }
    Session sess = Sessions.getCurrent();
    sess.setAttribute("userCredential",user);
   // Messagebox.show("Bienvenido a SIGED, "+user.getNombre()+"!", "Error", Messagebox.OK, Messagebox.INFORMATION);     
    Executions.sendRedirect("/");
     
}

//@Listen("onClick=#logout")
@Command
public void doLogout(){
	Session sess = Sessions.getCurrent();
	sess.removeAttribute("userCredential");
	Executions.sendRedirect("/portalPrincipal.zul");
}

@Command
public void configurarPerfil() {
	
	try{
		Borderlayout borderlayout = (Borderlayout) Path.getComponent("/borderLayauttree");
		Center center = borderlayout.getCenter();
		center.getChildren().clear();
		Map<String,Usuario> mapUsuario = new HashMap<String, Usuario>();
		Session sess = Sessions.getCurrent();
		Usuario user= (Usuario)sess.getAttribute("userCredential");
		mapUsuario.put("usuarioSesion", user);
		Executions.createComponents("/vistas/seguridad/cambiopassword.zul", center, mapUsuario);
	}catch(Exception e){
		Messagebox.show(e.toString());
	}
	
}

public String getAccount() {
	return account;
}


public void setAccount(String account) {
	this.account = account;
}


public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}


public AImage getFotoSesion(){

	return fotoSesion;
}

public void setFotoSesion(AImage img)
{
	this.fotoSesion=img;
}

@GlobalCommand
@NotifyChange("fotoSesion")
public void refrescar(){
	System.out.println("refrescarrrrrrrrrrrrrrrrrrrrrrrrrrrr");
	Session sess = Sessions.getCurrent();
	AImage img= (AImage) sess.getAttribute("foto");
	fotoSesion=img;
}
}