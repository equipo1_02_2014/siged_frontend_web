/** 
 * 	VMArbitro
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.util.List;
import java.util.Date;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioArbitroI;

public class VMArbitro {
	@WireVariable
	ServicioArbitroI servicioArbitro;
	@WireVariable
	Arbitro arbitro;
	List<Short> listaEstatus;	
	@WireVariable
	Short estatusSeleccionado;
	Short tipoOperacion;
	Window my;
	
	public Arbitro getArbitro() {
		return arbitro;
	}
	
	public void setAbitro(Arbitro arbitro) {
		this.arbitro = arbitro;
	}
				
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}
	
	public List<Short> getListaEstatus() {
		return listaEstatus;
	}

	//esta es la manera de recibir el arbitro seleccionado, que se le pasa desde la vista
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("arbitro") Arbitro arbitroSeleccionado) {
   
        if (arbitroSeleccionado!=null){
            this.arbitro = arbitroSeleccionado;
            this.tipoOperacion = 2;

        }else{
        	arbitro = new Arbitro();
        	arbitro.setFechaInicio(new Date());
        	this.tipoOperacion = 1;
        }
    }
	
	@Command
	@NotifyChange({"arbitro","listaArbitros"})   
	public void guardar(){		
		if (arbitro.getCedula()=="" || arbitro.getCedula()==null || arbitro.getNombre()=="" || arbitro.getNombre()==null || 
			arbitro.getApellido()==""  || arbitro.getApellido()==null ||  arbitro.getTelefono()==null || 
			arbitro.getTelefono()=="" || arbitro.getCelular()=="" || arbitro.getCelular()==null || arbitro.getFechaInicio()==null){
				Messagebox.show("Debe completar todos los campos para continuar", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		else{
			
		try{
			Messagebox.show("�Desea agregar el registro?","Confirmacion",
			         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
			         new org.zkoss.zk.ui.event.EventListener<Event>() {
	                    
		              public void onEvent(Event evt) throws InterruptedException {
		            	  
	       		        if (evt.getName().equalsIgnoreCase("onYes")) {
	       		        	servicioArbitro.guardar(arbitro);
	       		        	Messagebox.show("El registro se ha guardado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
	       		        	if (tipoOperacion==1) {	       		        	       		        
		       		        	Messagebox.show("�Desea agregar otro registro?","Pregunta",
							         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
							         new org.zkoss.zk.ui.event.EventListener<Event>() {
										public void onEvent(Event evt)
											throws Exception {
												if (evt.getName().equalsIgnoreCase("onYes")) {
													BindUtils.postGlobalCommand(null, null, "limpiarArbitro", null);
													
												}else{
													
													BindUtils.postGlobalCommand(null, null, "asignarTipoOperacionGuardarArbitro", null);
													BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
													Window myWindow = (Window) Path.getComponent("/vistaArbitro");
													myWindow.detach();
						        		    	}
											}
		       		        	});
		       		        	} else {
		       		        	Window myWindow = (Window) Path.getComponent("/vistaArbitro");
								myWindow.detach();
		       		        	}
				        }
		              }
			});
	}catch(Exception ex){
		Messagebox.show("Error al agregar el registro","Error", Messagebox.OK, Messagebox.ERROR);
	}
		
	}
	}

	@GlobalCommand("limpiarArbitro")
	@NotifyChange({"arbitro","estatusSeleccionado"})
	public void limpiar(){
	arbitro.setId(null);
	arbitro.setCedula("");
	arbitro.setNombre("");
	arbitro.setApellido("");
	arbitro.setTelefono("");
	arbitro.setCelular("");
	arbitro.setFechaInicio(new Date());
	}
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("arbitroSeleccionado") Arbitro arbitroSeleccionado ) {
		Window window = (Window)Executions.createComponents("arbitro.zul", null, null);
		this.arbitro = arbitroSeleccionado;
		window.doModal();
	}
	
	@Command
    @NotifyChange({"arbitro"})
    public String verStatus(Short estatus){
        String est="";
        
        switch (estatus){
            case 0:
                est="Inactivo";
                break;
            case 1:
                est="Activo";
                break;
            default:
                est="Seleccione Estatus";
        }
    
        return est;
    }
	
	@Command
	@NotifyChange({"arbitro"})
	public void validarCedula(){
		boolean aux=false;
		if (arbitro.getCedula()!="" && arbitro.getCedula()!=null){
		aux=servicioArbitro.existeArbitro(arbitro.getCedula(), arbitro.getId());
		if (aux==true)
		{
			Messagebox.show("La cedula ya existe", "Error", Messagebox.OK, Messagebox.ERROR);
			arbitro.setCedula("");
		}
		}
	}
		
	@Command
	@NotifyChange({"arbitro"})
	public boolean validarFecha(){
		Date fechaSeleccionada= arbitro.getFechaInicio();
		Date fechaHoy = new Date();
			
		if (!fechaSeleccionada.before(fechaHoy)){
			Messagebox.show("Fecha no v�lida","Error",Messagebox.OK,Messagebox.ERROR);
			arbitro.setFechaInicio(null);
		return true;
		
		}	
		return false;
		
	}
	
}
