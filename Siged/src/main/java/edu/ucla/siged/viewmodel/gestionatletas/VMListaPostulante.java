package edu.ucla.siged.viewmodel.gestionatletas;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import edu.ucla.siged.domain.gestionatleta.Postulante;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPostulanteI;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

public class VMListaPostulante { 
	
		@WireVariable 
		ServicioPostulanteI servicioPostulante;
		@WireVariable 
		Postulante postulante;
		List<Postulante> listaPostulantes;
		List<Postulante> postulantesSeleccionados;
		Window window;
		String cedula="";
		String nombre="";
		String apellido="";
		Date fechaNacimiento;
		Date fechaPostulacion;
		String telefono="";
		Short estatus;
		Long registrosTotales;
		int paginaActual;
		int tamanoPagina;

		public int getPaginaActual() {
			return paginaActual;
		}

		public void setPaginaActual(int paginaActual) {
			this.paginaActual = paginaActual;
		}

		public int getTamanoPagina() {
			this.tamanoPagina=servicioPostulante.TAMANO_PAGINA;
			return tamanoPagina;
		}

		public void setTamanoPagina(int tamanoPagina) {
			this.tamanoPagina = tamanoPagina;
		}

		public Postulante getPostulante() {
			return postulante;
		}

		public void setPostulante(Postulante postulante) {
			this.postulante = postulante;
		}

		public String getCedula() {
			return cedula;
		}

		public void setCedula(String cedula) {
			this.cedula = cedula;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getApellido() {
			return apellido;
		}

		public void setApellido(String apellido) {
			this.apellido = apellido;
		}

		public Date getFechaNacimiento() {
			return fechaNacimiento;
		}

		public void setFechaNacimiento(Date fechaNacimiento) {
			this.fechaNacimiento = fechaNacimiento;
		}

		public Date getFechaPostulacion() {
			return fechaPostulacion;
		}

		public void setFechaPostulacion(Date fechaPostulacion) {
			this.fechaPostulacion = fechaPostulacion;
		}

		public String getTelefono() {
			return telefono;
		}

		public void setTelefono(String telefono) {
			this.telefono = telefono;
		}

		public Short getEstatus() {
			return estatus;
		}

		public void setEstatus(Short estatus) {
			this.estatus = estatus;
		}
		
		public List<Postulante> getListaPostulantes() {
			return listaPostulantes;
		}

		public void setListaPostulantes(List<Postulante> listaPostulantes) {
			this.listaPostulantes = listaPostulantes;
		}

		public List<Postulante> getPostulantesSeleccionados() {
			return postulantesSeleccionados;
		}

		public void setPostulantesSeleccionados(List<Postulante> postulantesSeleccionados) {
			this.postulantesSeleccionados = postulantesSeleccionados;
		}
		
		public Long getRegistrosTotales() {
			return registrosTotales;
		}

		public void setRegistrosTotales(Long registrosTotales) {
			this.registrosTotales = registrosTotales;
		}
		
		@Init
	    public void init() {
			paginaActual = 0;
			this.listaPostulantes = servicioPostulante.buscarPostulantesPendientes(paginaActual);
			this.registrosTotales = servicioPostulante.totalPostulantesPendientes();
	    }
			
		@Command
		@NotifyChange({"postulante"})
		public void responderPostulante(@BindingParam("postulanteSeleccionado") Postulante postulanteSeleccionado) {
			this.postulante = postulanteSeleccionado;
			Map<String, Postulante> mapa = new HashMap<String, Postulante>();
	        mapa.put("postulante", postulanteSeleccionado);
			
			window = (Window)Executions.createComponents("/vistas/gestionatleta/responderPostulante.zul", null, mapa);
			window.doModal();
		}
		
		@Command
		public String cambiarFormatoFecha(Date fechaNacimiento){
			String fechaFormateada="";
			
			if(fechaNacimiento!=null){
			   fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fechaNacimiento);
			}
				return fechaFormateada;	
		}
		
		@Command
		@NotifyChange({"listaPostulantes"})
		public String verStatusEnLista(Integer estatusEnLista){
			String est="Pendiente";
			switch (estatusEnLista){
				case 0:
					est="Eliminado";
					break;
				case 1:
					est="Pendiente";
					break;
				case 2:
					est="Aprobado";
				    break;
				case 3:
					est="Rechazado";
					break;
			}
			return est;
		}
		
		@GlobalCommand
		@NotifyChange({"listaPostulantes","registrosTotales","paginaActual","postulantesSelecionados"})
		public void paginar(){
			this.registrosTotales = servicioPostulante.totalPostulantesPendientes();
			this.listaPostulantes = servicioPostulante.buscarPostulantesPendientes(this.paginaActual);
		}
		
		@GlobalCommand
		@NotifyChange({"listaPostulantes","registrosTotales","paginaActual"})
	    public void actualizarLista(){
			int ultimaPagina=0;
			this.getRegistrosTotales();
			if ((this.registrosTotales+1)%this.tamanoPagina==0)
				ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			else
				ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
			this.paginaActual=ultimaPagina;
			paginar();
		}
}