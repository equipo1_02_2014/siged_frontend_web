package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.reporte.Asistencia;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteAsistenciaI;

public class VMAsistencias{
		
		@WireVariable
	    private ServicioCategoriaI servicioCategoria;
	    
	    @WireVariable
	    private ServicioRangoI servicioRango;
	    
	    @WireVariable
	    private ServicioReporteAsistenciaI servicioReporteAsistencia;
	    
	    @WireVariable
	    private ServicioEquipoI servicioEquipo;
	    
	    private List<Equipo> listaEquiposFundacion;
	    private Equipo equipoSeleccionado;
		private Rango rangoSeleccionado;
		private Categoria categoriaSeleccionada;
		private boolean asistente;
		private boolean noAsistente;
		private Date fechaDesde;
		private Date fechaHasta;
		private String pathProyecto;
		private List<Rango> listaRango;
		private List<Categoria> listaCategoria;
		private List<Integer> listaCantidadEquipos;
		
		private Window window;
					
		@Init
		public void init(){
		
			this.setWindow(null);
			this.listaEquiposFundacion= servicioEquipo.buscarEquiposTrue();
			
			this.listaCategoria= servicioCategoria.buscarTodos();
			
			this.listaRango= servicioRango.buscarTodos();
			
			
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMRelacionJugadosGanados.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			
			this.inicializarCampos();
		}
		
		@NotifyChange({"fechaDesde","fechaHasta","categoriaSeleccionada",
			           "rangoSeleccionado","cantidadEquipoSeleccionada"})
		public void inicializarCampos(){
			
			this.asistente=true;
			this.noAsistente=false;
			this.fechaDesde=null;
			this.fechaHasta=null;
			this.equipoSeleccionado=null;
			this.rangoSeleccionado=null;
			this.categoriaSeleccionada=null;
			
			
		}
		
		@Command
		@NotifyChange({"asistente","noAsistente", "fechaDesde","fechaHasta","equipoSeleccionado", "categoriaSeleccionada",
	           "rangoSeleccionado"})
		public void cancelar(){
			this.inicializarCampos();
		}
				
		
		@Command
		public void imprimir(){		
			
			
			String restricciones="";
			String filtros="";

			if (this.fechaDesde!=null && this.fechaHasta!=null){
				
				if(this.fechaDesde.compareTo(this.fechaHasta) == 0 || this.fechaDesde.compareTo(this.fechaHasta) == -1){
				
					if((this.fechaDesde!=null && this.fechaHasta!=null) || this.equipoSeleccionado!=null || this.rangoSeleccionado!=null || this.categoriaSeleccionada!=null){	
						
							if (this.fechaDesde!=null && this.fechaHasta!=null){
								
								String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
								String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
								restricciones += " AND p.fecha BETWEEN '" + desde + "' AND '" + hasta + "'";
								
								desde= new SimpleDateFormat("dd/MM/YYYY").format(this.fechaDesde);
								hasta= new SimpleDateFormat("dd/MM/YYYY").format(this.fechaHasta);
								filtros += "Fecha desde " + desde.toString() + " hasta " + hasta.toString();
							}
																			
							if (this.equipoSeleccionado!=null){
								restricciones += " AND p.idequipo=" + this.equipoSeleccionado.getId();
								filtros += " Equipo: " + this.equipoSeleccionado.getNombre();
							}
							

							if (this.categoriaSeleccionada!=null){
								restricciones += " AND e.idcategoria=" + this.categoriaSeleccionada.getId();
								filtros += " Categoria: " + this.categoriaSeleccionada.getNombre();
							}
							
							if (this.rangoSeleccionado!=null){
								restricciones += " AND e.idrango=" + this.rangoSeleccionado.getId();
								filtros += " Rango: " + this.rangoSeleccionado.getDescripcion();
							}
							
							
							
					}
					
					
					
					if(asistente){
						filtros += " Atletas asistentes" ;
					}else{
						filtros += " Atletas inasistentes" ;
					}
					
					List<Asistencia> lista= this.servicioReporteAsistencia.buscarAsistencia(restricciones,  Boolean.toString(asistente) );
								
					
					if (!lista.isEmpty()){
						
						Map<String,Object> parameterMap = new HashMap<String,Object>();
						
						JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
						
						parameterMap.put("datasource", JRdataSource);
						
						parameterMap.put("filtros",filtros);
						
						parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
						
						parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
						
						//Messagebox.show(Boolean.toString(asistente));
						
						if(asistente){
							parameterMap.put("tipo","Atletas asistentes");
							parameterMap.put("titulo","Asistencia de atletas por prácticas");
						}else{
							parameterMap.put("tipo","Atletas inasistentes");
							parameterMap.put("titulo","Inasistencia de atletas por prácticas");
						}
						
						try {
							
							JasperDesign jasDesign=null;
							
							jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "reporteAsistenciaTorta.jrxml");
							
							JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
					        
					        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
					        
					        JasperViewer.viewReport(jasperPrint,false);
							
						} catch (JRException e) {
							e.printStackTrace();
							Messagebox.show(e.getMessage());
						}
					
					
	
					}else{
						Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
				                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
					}
					
				}else{
					
					Messagebox.show("La fecha 'desde'debe ser menor o igual a la fecha 'hasta'", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
					
				}
				
			}else{
				
				Messagebox.show("Debe seleccionar el rango de fechas", 
		                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				
			}
		
		}
		
		@Command
		@NotifyChange({"asistente","noAsistente"})
		public void seleccionarVista(@BindingParam("vista")String vista){
			
			this.asistente=false;
			this.noAsistente=false;
			
			if (vista.equalsIgnoreCase("Asistencia")){
				this.asistente=true;
			}
			else if(vista.equalsIgnoreCase("No asistencia")){
				this.noAsistente=true;
			}
			
		}
		
		
		public Date getFechaDesde() {
			return fechaDesde;
		}

		public void setFechaDesde(Date fechaDesde) {
			this.fechaDesde = fechaDesde;
		}

		public Date getFechaHasta() {
			return fechaHasta;
		}

		public void setFechaHasta(Date fechaHasta) {
			this.fechaHasta = fechaHasta;
		}

		public Rango getRangoSeleccionado() {
			return rangoSeleccionado;
		}

		public void setRangoSeleccionado(Rango rangoSeleccionado) {
			this.rangoSeleccionado = rangoSeleccionado;
		}

		public Categoria getCategoriaSeleccionada() {
			return categoriaSeleccionada;
		}

		public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) {
			this.categoriaSeleccionada = categoriaSeleccionada;
		}
		
		public List<Rango> getListaRango() {
			return listaRango;
		}

		public List<Categoria> getListaCategoria() {
			return listaCategoria;
		}

		public List<Integer> getListaCantidadEquipos() {
			return listaCantidadEquipos;
		}

		public Window getWindow() {
			return window;
		}

		public void setWindow(Window window) {
			this.window = window;
		}

		public boolean isAsistente() {
			return asistente;
		}

		public void setAsistente(boolean asistente) {
			this.asistente = asistente;
		}

		public boolean isNoAsistente() {
			return noAsistente;
		}

		public void setNoAsistente(boolean noAsistente) {
			this.noAsistente = noAsistente;
		}

		public List<Equipo> getListaEquiposFundacion() {
			return listaEquiposFundacion;
		}

		public void setListaEquiposFundacion(List<Equipo> listaEquiposFundacion) {
			this.listaEquiposFundacion = listaEquiposFundacion;
		}

		public Equipo getEquipoSeleccionado() {
			return equipoSeleccionado;
		}

		public void setEquipoSeleccionado(Equipo equipoSeleccionado) {
			this.equipoSeleccionado = equipoSeleccionado;
		}
		
}
