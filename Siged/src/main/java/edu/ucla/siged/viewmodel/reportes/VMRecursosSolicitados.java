package edu.ucla.siged.viewmodel.reportes;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.http.SimpleWebApp;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.reporte.PromedioCantidadBase;
import edu.ucla.siged.domain.reporte.RecursosSolicitados;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioDonacionI;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioRecursoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteRecursosSolicitadosI;


public class VMRecursosSolicitados {
	
	 @WireVariable
	    private ServicioDonacionI servicioDonacion;
	    
	    @WireVariable
	    private ServicioRecursoI servicioRecurso;
	    
	    @WireVariable
	    private ServicioReporteRecursosSolicitadosI servicioReporteRecursosSolicitados;
	
	    private boolean vistaGraficaTorta;
		
		private boolean vistaGraficaTorta2;
		
		private Date fechaDesde;
		
		private Date fechaHasta;
		
		//private Recurso recursoSeleccionado;
		
		private String pathProyecto;
		
		//private List<Recurso> listaRecurso;
		
		
		private Window window;
		
		@Init
		public void init(){
		
			this.window= null;
			
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMRecursosSolicitados.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			
			
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			this.inicializarCampos();
			
		}
		
		
		@NotifyChange({"vistaGraficaTorta","vistaGraficaTorta2","fechaDesde","fechaHasta","recursoSeleccionado",})
        public void inicializarCampos(){
	
			this.vistaGraficaTorta=true;
			this.vistaGraficaTorta2=false;
			this.fechaDesde=null;
			this.fechaHasta=null;
			//this.recursoSeleccionado= null;
			
		}
		
		
            
		@Command
		@NotifyChange({"vistaGraficaTorta","vistaGraficaTorta2","fechaDesde","fechaHasta", "recursoSeleccionado"})
		public void cancelar(){
			
			this.inicializarCampos();
		}
		
		@Command
		@NotifyChange({"vistaGraficaTorta","vistaGraficaTorta2"})
		public void seleccionarVista(@BindingParam("vista")String vista){
			
			this.vistaGraficaTorta=false;
			this.vistaGraficaTorta2=false;
			
			if (vista.equalsIgnoreCase("Ver en porcentaje")){
				this.vistaGraficaTorta=true;
			}
			else if(vista.equalsIgnoreCase("Ver en cantidad")){
				this.vistaGraficaTorta2=true;
			}
			
		}
		
		
		public boolean validarPeriodoApertura(){
			boolean valido=true;
			
			
            if (this.fechaDesde==null && this.fechaHasta!=null){
				
            	valido= false;
				
            	Messagebox.show("El Campo Desde es Invalido" + System.lineSeparator() + "Coloque una Fecha Valida en el Campo Desde o deje el campo Desde y Hasta Vacios", 
                        "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				
				
				
			}else if(this.fechaHasta==null && this.fechaDesde!=null){
				
				valido=false;
				
				Messagebox.show("El Campo Hasta es Invalido" + System.lineSeparator() + "Coloque una Fecha Valida en el Campo Hasta o deje el campo Desde y Hasta Vacios", 
                        "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				
			}else if ((this.fechaDesde!=null && this.fechaHasta!=null) && this.fechaDesde.after(this.fechaHasta)){
				
				valido=false;
				
				Messagebox.show("El Rango de las Fechas es Invalido", 
		                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
	        }
					
			
			return valido;
		}
		
		
		@Command
		public void imprimir(){		
			
			if (this.validarPeriodoApertura()){ 
			
				String restricciones="";
				
				if(this.fechaDesde!=null && this.fechaHasta!=null){
					
					
						//if (this.fechaDesde!=null && this.fechaHasta!=null){
							
							String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
							String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
							
							restricciones += "AND donacion.fechasolicitud BETWEEN '" + desde + "' AND '" + hasta + "'";
							
						//}
						
						List<RecursosSolicitados> lista;
						
						
						 lista= this.servicioReporteRecursosSolicitados.buscarRecursosSolicitados(restricciones);
						
						if (!lista.isEmpty()){
							
							//String desde= new SimpleDateFormat("dd-MM-YYYY").format(this.fechaDesde);
							//String hasta= new SimpleDateFormat("dd-MM-YYYY").format(this.fechaHasta);
							Map<String,Object> parameterMap = new HashMap<String,Object>();
							
							JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
							
							parameterMap.put("datasource", JRdataSource);
							
							parameterMap.put("titulo","Recursos Solicitados");
							
							parameterMap.put("fechadesde","Desde: "+desde+" Hasta: "+hasta);
							
							
							parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
							
							parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
								
							try {			
								
								JasperDesign jasDesign=null;
								
								if(this.vistaGraficaTorta){
								   jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "recursos.jrxml");
								}else{
									jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "recursos2.jrxml");
								}
								
								JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
						        
						        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
						        
						        JasperViewer.viewReport(jasperPrint,false);
								
							} catch (JRException e) {
								e.printStackTrace();
								Messagebox.show(e.getMessage());
							}
							
				    }else{
							Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
					                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
						}

				}else{
					Messagebox.show("Por favor escoja un rango de fechas", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			
			}
			
			     
		}
		
		
		public boolean isVistaGraficaTorta() {
			return vistaGraficaTorta;
		}

		public void setVistaGraficaTorta(boolean vistaGraficaTorta) {
			this.vistaGraficaTorta = vistaGraficaTorta;
		}

		public boolean isVistaGraficaTorta2() {
			return vistaGraficaTorta2;
		}

		public void setVistaGraficaTorta2(boolean vistaGraficaTorta2) {
			this.vistaGraficaTorta2 = vistaGraficaTorta2;
		}

		
		public Date getFechaDesde() {
			return fechaDesde;
		}

		public void setFechaDesde(Date fechaDesde) {
			this.fechaDesde = fechaDesde;
		}

		public Date getFechaHasta() {
			return fechaHasta;
		}

		public void setFechaHasta(Date fechaHasta) {
			this.fechaHasta = fechaHasta;
		}

		/*public Recurso getRecursoSeleccionado() {
			return recursoSeleccionado;
		}

		public void setRecursoSeleccionado(Recurso recursoSeleccionado) {
			this.recursoSeleccionado = recursoSeleccionado;
		}

		

		/*public List<Recurso> getListaRecurso() {
			return listaRecurso;
		}
*/
		

}
