package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioTipoEquipoI;

public class VMListaTipoEquipo {
	@WireVariable
	ServicioTipoEquipoI servicioTipoEquipo;
	@WireVariable
	TipoEquipo tipoequipo;
	List<TipoEquipo> listaTipoEquipos;
	List<TipoEquipo> tipoequiposSeleccionados;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String descripcion="";
	short estatusCombo=0;
	boolean estatusFiltro;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	
	public TipoEquipo getTipoequipo() {
		return tipoequipo;
	}

	public void setTipoequipo(TipoEquipo tipoequipo) {
		this.tipoequipo = tipoequipo;
	}

	public List<TipoEquipo> getListaTipoEquipos() {
		return listaTipoEquipos;
	}

	public void setListaTipoEquipos(List<TipoEquipo> listaTipoEquipos) {
		this.listaTipoEquipos = listaTipoEquipos;
	}

	public List<TipoEquipo> getTipoequiposSeleccionados() {
		return tipoequiposSeleccionados;
	}

	public void setTipoequiposSeleccionados(
			List<TipoEquipo> tipoequiposSeleccionados) {
		this.tipoequiposSeleccionados = tipoequiposSeleccionados;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioTipoEquipo.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}
	
	////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////

	@Init
	public void init(){
			this.listaTipoEquipos = servicioTipoEquipo.buscarTodos(0).getContent();
			this.registrosTotales= servicioTipoEquipo.totalTipoEquipos();
	}
	
	////////////////////////////////////////AGREGAR TIPOEQUIPO///////////////////////////////////////////////////////////
	
	@Command
	public void agregarTipoEquipo() {
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestionequipo/tipoequipo.zul", null, null);
		window.doModal();
	}
	
	/////////////////////////////////////////ELIMINAR TIPOEQUIPO////////////////////////////////////////////////////
	
	@Command
	@NotifyChange("listaTipoEquipos")
	public void eliminarTipoEquipo(@BindingParam("tipoequipoSeleccionado") TipoEquipo tipoequipoSeleccionado){
		servicioTipoEquipo.eliminarTipoEquipo(tipoequipoSeleccionado);
		Messagebox.show("Datos eliminados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		actualizarLista(tipoequipo);
	}

	///////////////////////////////////////////ELIMINAR VARIOS TIPOSEQUIPO//////////////////////////////////////////////
	@Command
	@NotifyChange("listaTipoEquipos")
	public void eliminarTipoEquipos(@BindingParam("tipoequiposSeleccionados") TipoEquipo tipoequiposSeleccionados){
		servicioTipoEquipo.eliminarTipoEquipos(listaTipoEquipos);
		Messagebox.show("Lista eliminada correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		actualizarLista(tipoequipo);	
	}
	
	/////////////////////////////////////////////EDITAR TIPOEQUIPO/////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"tipoequipo"})
	public void editarTipoEquipo(@BindingParam("tipoequipoSeleccionado") TipoEquipo tipoequipoSeleccionado ) {
		
		this.tipoequipo=tipoequipoSeleccionado;
		this.tipoOperacion=2;
		//esta es la manera de pasar a la otra vista el tecnico seleccionado
		Map<String, TipoEquipo> mapa = new HashMap<String, TipoEquipo>();
        mapa.put("tipoequipo", tipoequipoSeleccionado);
		
		window = (Window)Executions.createComponents("/vistas/gestionequipo/tipoequipo.zul", null, mapa);
		window.doModal();
	}
	
	//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaTipoEquipos","registrosTotales","paginaActual","tipoequiposSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaTipoEquipos = servicioTipoEquipo.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioTipoEquipo.totalTipoEquipos();
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	////////////////////////////////////////ACTUALIZAR LISTA/////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaTipoEquipos","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevoTipoEquipo") TipoEquipo nuevoTipoEquipo){
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina);
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina;
		} else if(this.tipoOperacion==2){
		
		}
		paginar();
	}
	
	////////////////////////////////////////////// VER ESTATUS///////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaTipoEquipos"})
	public String verStatusEnLista(Integer estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	//////////////////////////////////////////////EJECUTAR FILTRO/////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaTipoEquipos","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		if (descripcion == null)
			jpql = " descripcion like '%"+descripcion+"%' and estatus in "+filtroEstatus+"";
		else
		   jpql = " descripcion like '%"+descripcion+"%' and estatus in "+filtroEstatus+"";
			
		System.out.println("----------------------------------"+jpql);
		this.listaTipoEquipos=servicioTipoEquipo.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioTipoEquipo.totalTipoEquiposFiltrados(jpql);
	}
	
/////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	
	
	@Command
	@NotifyChange({"listaTipoEquipos","registrosTotales","paginaActual","tipoequiposSeleccionados",
				   "descripcion","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar(); 
		descripcion="";     //  variables del filtro
		estatusCombo=0;
	}

}