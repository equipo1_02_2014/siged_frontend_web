package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.reporte.ResultadosJuegos;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;

public class VMResultadosJuegos {

	@WireVariable
	private ServicioCategoriaI servicioCategoria;

	@WireVariable
	private ServicioRangoI servicioRango;

	@WireVariable
	private ServicioCompetenciaI servicioCompetencia;

	@WireVariable
	private ServicioEquipoI servicioEquipo;

	@WireVariable
	private ServicioReporteEstadisticoI servicioReporteEstadistico;

	private List<Equipo> listaEquiposFundacion;

	private Equipo equipoSeleccionado;

	private Date fechaDesde;

	private Date fechaHasta;

	private Rango rangoSeleccionado;

	private Categoria categoriaSeleccionada;

	private Integer cantidadEquipoSeleccionada;

	private Competencia competenciaSeleccionada;

	private String pathProyecto;

	private List<Rango> listaRango;

	private List<Categoria> listaCategoria;

	private List<Integer> listaCantidadEquipos;

	private List<Competencia> listaCompetencia;

	private List<Juego> listaResultados;
	
	private int resultadoJuegoSeleccionado;

	private Window window;
	
	private String arregloResultados[];
	private String resultadoFiltro;

	@Init
	public void init() {

		arregloResultados = new String[] {"Perdido","Ganado","Empatado"};
		
		this.setWindow(null);
		this.listaEquiposFundacion = servicioEquipo.buscarEquiposTrue();
		this.listaCategoria = servicioCategoria.buscarTodos();
		this.listaCompetencia = servicioCompetencia.buscarTodos();
		this.listaRango = servicioRango.buscarTodos();

		/********* Para obtener el path del proyecto ******/

		File file = new File(VMResultadosJuegos.class.getProtectionDomain()
				.getCodeSource().getLocation().getPath());

		this.pathProyecto = file.getParentFile().getParentFile()
				.getParentFile().getParentFile().getParentFile()
				.getParentFile().getParentFile().getParentFile().getPath();
		// Sustitucion de caracteres codificados (Para servidores con Windows)
		this.pathProyecto = this.pathProyecto.replaceAll("%20", " ");

		/***********************************/

		this.inicializarCampos();
	}

	@NotifyChange({ "fechaDesde", "fechaHasta", "equipoSeleccionado",
			"competenciaSeleccionada", "resultadoFiltro",
			"categoriaSeleccionada", "rangoSeleccionado" })
	public void inicializarCampos() {

		this.fechaDesde = null;
		this.fechaHasta = null;
		this.equipoSeleccionado = null;
		this.competenciaSeleccionada = null;
		this.setResultadoJuegoSeleccionado(-1);
		this.setResultadoFiltro(null);
		this.categoriaSeleccionada = null;
		this.rangoSeleccionado = null;
	}

	@Command
	@NotifyChange({ "fechaDesde", "fechaHasta", "equipoSeleccionado",
			"competenciaSeleccionada", "resultadoFiltro",
			"categoriaSeleccionada", "rangoSeleccionado" })
	public void cancelar() {
		this.inicializarCampos();
	}
	
	
	
	@Command("seleccionarResultado")
	public void seleccionarResultado() {
		this.setResultadoJuegoSeleccionado(this.traducirResultado(this.getResultadoFiltro()));
		System.out.println(" Resultado en INT: " + this.getResultadoJuegoSeleccionado());
	}

	@Command
	public void imprimir(){	
		
			String restricciones="";
			String filtros="";
			
			if((((((this.fechaDesde!=null && this.fechaHasta!=null) || this.equipoSeleccionado!=null) || this.competenciaSeleccionada!=null) || this.resultadoJuegoSeleccionado != -1) ||this.categoriaSeleccionada!=null) || this.rangoSeleccionado!=null){	
				
					if (this.fechaDesde!=null && this.fechaHasta!=null){
						
						String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
						String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
						restricciones += " AND j.fecha BETWEEN '" + desde + "' AND '" + hasta + "'";
						//para mostrar filtro en reporte
						desde= new SimpleDateFormat("dd/MM/YYYY").format(this.fechaDesde);
						hasta= new SimpleDateFormat("dd/MM/YYYY").format(this.fechaHasta);
						filtros += "Fecha desde " + desde.toString() + " hasta " + hasta.toString();
						
					}
																	
					if (this.equipoSeleccionado!=null){
						restricciones += " AND j.idequipo=" + this.equipoSeleccionado.getId();
						filtros += " Equipo: " + this.equipoSeleccionado.getNombre();
					}
					
					if (this.competenciaSeleccionada!=null) {
						restricciones += " AND j.idcompetencia=" + this.competenciaSeleccionada.getId();
						filtros += " Competencia: " + this.competenciaSeleccionada.getNombre();
					}
					
					
					if (this.resultadoJuegoSeleccionado != -1){
						restricciones += " AND j.resultado=" + this.getResultadoJuegoSeleccionado();
						filtros += " Resultado: " + this.getResultadoFiltro();
					}
					
					if (this.categoriaSeleccionada!=null){
						restricciones += " AND e.idcategoria=" + this.categoriaSeleccionada.getId();
						filtros += " Categoria: " + this.categoriaSeleccionada.getNombre();
					}
					
					if(this.rangoSeleccionado!=null){
						restricciones += " AND e.idrango=" + this.rangoSeleccionado.getId();
						filtros += " Rango: " + this.rangoSeleccionado.getDescripcion();
						
					}
					
			}
			
			//restricciones="";
			List<ResultadosJuegos> lista= this.servicioReporteEstadistico.listarResultadosJuegos(restricciones);
			
			if (!lista.isEmpty()){
				
				Map<String,Object> parameterMap = new HashMap<String,Object>();
				JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
				
				parameterMap.put("datasource", JRdataSource);
				parameterMap.put("titulo","Resultados de los Juegos");
				parameterMap.put("filtros",filtros);
				parameterMap.put("encabezado1", "Republica Bolivariana de Venezuela");
		        parameterMap.put("encabezado2", "Fundacion Luis Sojo");
		        parameterMap.put("encabezado3", "Cabudare - Lara");
		        parameterMap.put("rutaLogo", "http://localhost:8080/Siged/source/images/logo_fundacion_luis_sojo.png");
		        parameterMap.put("registrosTotales",lista.size());
				
				try {
					
					JasperDesign jasDesign=null;
					
					jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "reporteResultadosJuegos.jrxml");
					
					JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
			        
			        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
			        
			        JasperViewer.viewReport(jasperPrint,false);
					
				} catch (JRException e) {
					e.printStackTrace();
					Messagebox.show(e.getMessage());
				}

			}else{
				Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
		                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			
	}

	public int traducirResultado(String resultado) {
		if (resultado.equalsIgnoreCase("perdido")) {
			return 0;
		} else if (resultado.equalsIgnoreCase("ganado")) {
			return 1;
		} else if (resultado.equalsIgnoreCase("empatado")) {
			return 2;
		}
		return -1;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public Rango getRangoSeleccionado() {
		return rangoSeleccionado;
	}

	public void setRangoSeleccionado(Rango rangoSeleccionado) {
		this.rangoSeleccionado = rangoSeleccionado;
	}

	public Categoria getCategoriaSeleccionada() {
		return categoriaSeleccionada;
	}

	public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) {
		this.categoriaSeleccionada = categoriaSeleccionada;
	}

	public Integer getCantidadEquipoSeleccionada() {
		return cantidadEquipoSeleccionada;
	}

	public void setCantidadEquipoSeleccionada(Integer cantidadEquipoSeleccionada) {
		this.cantidadEquipoSeleccionada = cantidadEquipoSeleccionada;
	}

	public Competencia getCompetenciaSeleccionada() {
		return competenciaSeleccionada;
	}

	public void setCompetenciaSeleccionada(Competencia competenciaSeleccionada) {
		this.competenciaSeleccionada = competenciaSeleccionada;
	}

	public List<Rango> getListaRango() {
		return listaRango;
	}

	public List<Categoria> getListaCategoria() {
		return listaCategoria;
	}

	public List<Integer> getListaCantidadEquipos() {
		return listaCantidadEquipos;
	}

	public List<Competencia> getListaCompetencia() {
		return listaCompetencia;
	}

	public void setListaCompetencia(List<Competencia> listaCompetencia) {
		this.listaCompetencia = listaCompetencia;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(Window window) {
		this.window = window;
	}

	public List<Equipo> getListaEquiposFundacion() {
		return listaEquiposFundacion;
	}

	public void setListaEquiposFundacion(List<Equipo> listaEquiposFundacion) {
		this.listaEquiposFundacion = listaEquiposFundacion;
	}

	public Equipo getEquipoSeleccionado() {
		return equipoSeleccionado;
	}

	public void setEquipoSeleccionado(Equipo equipoSeleccionado) {
		this.equipoSeleccionado = equipoSeleccionado;
	}

	public List<Juego> getListaResultados() {
		return listaResultados;
	}

	public void setListaResultados(List<Juego> listaResultados) {
		this.listaResultados = listaResultados;
	}

	public int getResultadoJuegoSeleccionado() {
		return resultadoJuegoSeleccionado;
	}

	public void setResultadoJuegoSeleccionado(int resultadoJuegoSeleccionado) {
		this.resultadoJuegoSeleccionado = resultadoJuegoSeleccionado;
	}

	public String[] getAregloResultados() {
		if (arregloResultados == null) {
			arregloResultados = new String[] {"Perdido","Ganado","Empatado"};
		}
		return arregloResultados;
	}

	public void setAregloResultados(String[] aregloResultados) {
		this.arregloResultados = aregloResultados;
	}

	public String getResultadoFiltro() {
		return resultadoFiltro;
	}

	public void setResultadoFiltro(String resultadoFiltro) {
		this.resultadoFiltro = resultadoFiltro;
	}

}
