package edu.ucla.siged.viewmodel.seguridad;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioEmailsI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMRecuperarPassword {
	@WireVariable
	ServicioUsuarioI servicioUsuario; // Instancia la interface servicio	
	@WireVariable
	ServicioEmailsI servicioEmails;
	@WireVariable
	Usuario usuario;
	private String password;
	private String respuestaSecreta;
	private String email;
	private boolean activaDatos;
	public VMRecuperarPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            		 @ExecutionArgParam("nombreUsuario") String nombreUsuario) {
		 if (nombreUsuario!=null){
			this.usuario=servicioUsuario.buscarByNombreUsuario(nombreUsuario);	
	         Selectors.wireComponents(view, this, false);
	        if (usuario==null){
	        	Messagebox.show("Ese usuario no existe!", "Información", Messagebox.OK, Messagebox.INFORMATION);}
		}
		 else
			 usuario=new Usuario();
		 this.activaDatos=false;
       }      
    
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRespuestaSecreta() {
		return respuestaSecreta;
	}

	public void setRespuestaSecreta(String respuestaSecreta) {
		this.respuestaSecreta = respuestaSecreta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void generarPassword() {
		String base="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$";
		int longitud = base.length();
		final int LARGO_PASWWORD=8;
		password="";
		for(int i=0; i<LARGO_PASWWORD;i++){ 
		    int numero = (int)(Math.random()*(longitud)); 
		    String caracter=base.substring(numero, numero+1); 
		    password+=caracter; 
		}
		System.out.println("PASSWORD: "+password);
		usuario.setPassword(password); 
	}
	
	@Command
	@NotifyChange({"usuario","email","respuestaSecreta", "activaDatos"})
	public void continuarRecuperacion()
	{
		if(usuario.getNombreUsuario()!=""){
		Usuario usuarioNew=servicioUsuario.buscarByNombreUsuario(usuario.getNombreUsuario());
		System.out.println("BUSCANDO "+usuario.getNombreUsuario());
		if(usuarioNew!=null)
		{
			this.usuario=usuarioNew;
			this.activaDatos=true;
			Window myWindow = (Window) Path.getComponent("/vistaRecuperarPassword");	
			return;
		}
		}
		Messagebox.show("Ese nombre de usuario no existe", "Información", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({"usuario"})
	public void enviarRecuperacionPassword(){
		System.out.println(usuario);
		if ( usuario.getEmail().equals("") || respuestaSecreta.equals(""))
			Messagebox.show("No pueden haber campos vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		else{
			try{
			
					if(this.respuestaSecreta.equals(usuario.getRespuestaSecreta()) && this.email.equals(usuario.getEmail()))
					{	generarPassword();
						//Funcion para enviar correo a la persona con la nueva contraseña
						usuario.setPassword(password);
						servicioUsuario.guardar(usuario);
						servicioEmails.enviarEmailUsuarioNuevo(usuario.getEmail(), usuario.getNombreUsuario(), usuario.getPassword());
						Messagebox.show("Se le ha enviado su nueva clave al email", "Información", Messagebox.OK, Messagebox.INFORMATION);
						Window myWindow = (Window) Path.getComponent("/vistaRecuperarPassword");
						myWindow.detach();
					}
					else{
					Messagebox.show("Información incorrecta!", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
					return;
					}
				}
			catch(Exception e){
			    System.out.println(e.getMessage());
			}
		}
		
	}
		
	@Command
	@NotifyChange({"usuario"})
		public void limpiarRecuperarPassword(){
			usuario=new Usuario();
			respuestaSecreta="";
			email="";
		}

	public boolean isActivaDatos() {
		return activaDatos;
	}

	public void setActivaDatos(boolean activaDatos) {
		this.activaDatos = activaDatos;
	}
}