package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.HashSet;
import java.util.Set;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMSolicitudAprobada {
	
	@WireVariable
	private ServicioDonacion servicioDonacion;
	
	@WireVariable
	private Donacion donacion = new Donacion();
	
	@WireVariable
	private Set<DonacionRecurso> recursos; // = new HashSet<DonacionRecurso>();
	
	private Window window;
	
	public VMSolicitudAprobada(){
		
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("donacion") Donacion donacionSeleccionada){
		Selectors.wireComponents(view, this, false);
		if(donacionSeleccionada!=null){
			this.donacion = donacionSeleccionada;
			this.recursos = this.donacion.getRecursosDonados();
		}
	}

	public Donacion getDonacion() {
		return donacion;
	}

	public void setDonacion(Donacion donacion) {
		this.donacion = donacion;
	}

	public Set<DonacionRecurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(Set<DonacionRecurso> recursos) {
		this.recursos = recursos;
	}

	@Command
	@NotifyChange({"donacion"})
	public void Aprobar(){
		try{
			this.donacion.setEstatus((short)3);
			servicioDonacion.Guardar(donacion);
			Messagebox.show("Donaci�n Entregada", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		Limpiar();
		Window myWindow = (Window) Path.getComponent("/vistaSolicitudAprobadah");
		myWindow.detach();
			
	}
	
	@Command
	@NotifyChange({"donacion"})
	public void Rechazar(){
		Limpiar();
		Window myWindow = (Window) Path.getComponent("/vistaSolicitudAprobada");
		myWindow.detach();
			
	}
	
	@Command
	@NotifyChange({"donacion"})
	public void Limpiar(){
		this.donacion = new Donacion();
		this.recursos = new HashSet<DonacionRecurso>();
	}
	
	@Command
	public String VerTipoDonacion(Integer aux){
		String est="Entrante";
		switch (aux){
			case 1:
				est="Saliente";
			    break;
			default:
				est="Entrante";
		}
		return est;
	}
	
	@Command
	public String VerEstatusDonacion(Integer aux){
		String est="Solicitada";
		switch (aux){
			case 1:
				est="Aprobada";
			    break;
			case 2:
				est="Rechazada";
			    break;
			case 3:
				est="Entregada";
			    break;
			case 4:
				est="Recibida";
			    break;
			default:
				est="Solicitada";
		}
		return est;
	}

}