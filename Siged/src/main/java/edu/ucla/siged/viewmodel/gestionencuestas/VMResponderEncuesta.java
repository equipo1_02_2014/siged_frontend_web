package edu.ucla.siged.viewmodel.gestionencuestas;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.util.Date;

import org.hibernate.Query;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;
import edu.ucla.siged.domain.gestioneventos.Respuesta;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioEncuestaI;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioRespuestaI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;

public class VMResponderEncuesta {

	
	@WireVariable private ServicioRespuestaI servicioRespuesta;
	@WireVariable private ServicioEncuestaI servicioEncuesta;
	
	
	private Encuesta encuesta;
	List<Encuesta> listaEncuestas;
	private int puntero;
	private List <Rol> roles;
	private Usuario usuario;
	private String opcion;
	private boolean siguiente;
	private boolean anterior;
	private List<OpcionPregunta> opcionesPregunta;
	private Pregunta pregunta;
	Window win = null;
	
	
	public VMResponderEncuesta() {
		super();
		// TODO Auto-generated constructor stub
	}



	@Init
	public void init() {
		this.opcionesPregunta= new ArrayList<OpcionPregunta>();	
		this.listaEncuestas = new ArrayList<Encuesta>();
		this.pregunta= new Pregunta();
		this.roles= new ArrayList<Rol>();
		Session sess = Sessions.getCurrent();
    	this.roles =  (List<Rol>) sess.getAttribute("roles");
    	this.usuario = (Usuario)sess.getAttribute("userCredential");
  	   	this.puntero=0;
  	   	if(usuario==null || usuario.isAnonimo())
  	   		this.listaEncuestas=servicioEncuesta.listarEncuestasPublicas();
  	   	else
  	   		this.listaEncuestas=servicioEncuesta.listarEncuestasUsuarioParaVotar(roles, usuario.getId());
		
  	   	System.out.println("-------------------------------->"+listaEncuestas.size());
		this.encuesta=new Encuesta();
		if(listaEncuestas.size()>0){
		this.encuesta=listaEncuestas.get(puntero);
		pregunta= encuesta.getFirstPregunta();
		pregunta.getOpcionPreguntas().size();
		this.opcionesPregunta=cargarOpciones();
		this.siguiente=true;}
		else{
		this.opcionesPregunta= new ArrayList<OpcionPregunta>();	
		this.siguiente=false;
		}
	}
	
	@Command
	@NotifyChange({"encuesta", "opcionesPregunta", "pregunta"})
	public void siguienteEncuesta() {
		puntero++;
		if(puntero<listaEncuestas.size()){
		this.encuesta=this.listaEncuestas.get(puntero);
		this.pregunta= encuesta.getFirstPregunta();
		this.pregunta.getOpcionPreguntas().size();
		this.opcionesPregunta=cargarOpciones();
		this.siguiente=true;
		}
		else
		this.siguiente=false;
		
		
	}
	
	 
	@Command
	@NotifyChange({"encuesta", "siguiente", "anterior",  "opcionesPregunta", "pregunta" })
	public void votarEncuesta(@BindingParam("opcionSeleccionada") OpcionPregunta opcionSeleccionada) {
		// TODO Auto-generated method stub
		if(opcionSeleccionada!=null){
		Respuesta respuesta= new Respuesta();
		respuesta.setOpcionPregunta(opcionSeleccionada);
		if(usuario==null || usuario.isAnonimo())
			respuesta.setUsuario(null);
		else
			respuesta.setUsuario(usuario);
		respuesta.setValor((short) 1);
		respuesta.setId(null);
		servicioRespuesta.guardar(respuesta);
		opcionSeleccionada=null;
		siguienteEncuesta();
		Messagebox.show("Su voto ha sido registrado exitosamente! ", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else
			Messagebox.show("Por favor seleccione una opción! ", "Información", Messagebox.OK, Messagebox.INFORMATION);
			
		
	}
	
	
	@Command
	public void verResultadosEncuesta(){
		try{
		/*	Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			*/
			if (win != null) {
				win.detach();
			}
			win = (Window) Executions.createComponents("/vistas/gestionencuesta/resultadosencuesta.zul",null, null);
			win.setMaximizable(true);
			win.doModal();
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}

	
	public Encuesta getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

	public int getPuntero() {
		return puntero;
	}

	public void setPuntero(int puntero) {
		this.puntero = puntero;
	}

	public List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}

	public boolean isSiguiente() {
		return siguiente;
	}

	public void setSiguiente(boolean siguiente) {
		this.siguiente = siguiente;
	}
	
	public boolean isAnterior(){
		
		return !siguiente;
	}
	
	
	public List<OpcionPregunta> cargarOpciones(){
		 this.pregunta.getOpcionPreguntas().size();
   	 System.out.println(this.pregunta.getOpcionPreguntas().size());
   		Object[] opcionest=pregunta.getOpcionPreguntas().toArray();
			OpcionPregunta opciont= new OpcionPregunta();
			List<OpcionPregunta> lista = new ArrayList<OpcionPregunta>();
			for(int i=0; i<opcionest.length;i++)
			{
				opciont= (OpcionPregunta) opcionest[i];
				lista.add(opciont);
				System.out.println("Opcion:"+opciont.getDescripcion());
			}
			return lista;
	}



	public List<OpcionPregunta> getOpcionesPregunta() {
		return opcionesPregunta;
	}



	public void setOpcionesPregunta(List<OpcionPregunta> opcionesPregunta) {
		this.opcionesPregunta = opcionesPregunta;
	}



	public Pregunta getPregunta() {
		return pregunta;
	}



	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}
	
	

}
