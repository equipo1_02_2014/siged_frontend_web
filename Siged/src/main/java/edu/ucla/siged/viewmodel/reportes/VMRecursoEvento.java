package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.reporte.RecursoEvento;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioEventoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;

public class VMRecursoEvento{
	
	    @WireVariable
	    private ServicioReporteEstadisticoI servicioReporteEstadistico;
	    	    	    
	    @WireVariable
	    private ServicioEventoI servicioEvento;
	    
	    private List<Evento> listaEventos;
	    
	    private Evento eventoSeleccionado;
		
	    private String pathProyecto;
		
		private Window window;
					
		@Init
		public void init(){
		
			this.setWindow(null);
			//this.listaEquiposFundacion= servicioEquipo.buscarEquiposTrue();
			
			this.listaEventos = servicioEvento.ListarEventos();
			
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMRecursoEvento.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			
			this.inicializarCampos();
		}
		
		@NotifyChange({"eventoSeleccionado"})
		public void inicializarCampos(){
			this.eventoSeleccionado=null;
		}
		
		@Command
		@NotifyChange({"eventoSeleccionado"})
		public void cancelar(){
			this.inicializarCampos();
		}
				
		
		@Command
		public void imprimir(){		
			
			
			String restricciones="";
			String filtros="";
			
				
			if(this.eventoSeleccionado!=null){	
				
				//Messagebox.show(this.eventoSeleccionado.getNombre());
				
				//restricciones += " AND e.id = " + this.eventoSeleccionado.getId().toString();
				filtros += " Evento: " + this.eventoSeleccionado.getNombre();
								
				Integer cantidadEquipos=0;
				List<RecursoEvento> lista= this.servicioReporteEstadistico.buscarRecursosEventos(restricciones, cantidadEquipos);
										
				if (!lista.isEmpty()){
					
					Map<String,Object> parameterMap = new HashMap<String,Object>();
					
					JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
					
					parameterMap.put("datasource", JRdataSource);
					
					parameterMap.put("titulo","Recursos obtenidos por evento");
					
					parameterMap.put("filtros",filtros);
					
					parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
					
					parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
					
					try {
						
						JasperDesign jasDesign=null;
						
						jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "reporteRecursoEvento.jrxml");
						
						JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
				        
				        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
				        
				        JasperViewer.viewReport(jasperPrint,false);
						
					} catch (JRException e) {
						e.printStackTrace();
						Messagebox.show(e.getMessage());
					}
				
				

				}else{
					Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				}
				
			    
			}else{
				Messagebox.show("Debe seleccionar un evento", 
		                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			}
				
		}
		
		
	
		public Window getWindow() {
			return window;
		}

		public void setWindow(Window window) {
			this.window = window;
		}

		public List<Evento> getListaEventos() {
			return listaEventos;
		}

		public void setListaEventos(List<Evento> listaEventos) {
			this.listaEventos = listaEventos;
		}

		public Evento getEventoSeleccionado() {
			return eventoSeleccionado;
		}

		public void setEventoSeleccionado(Evento eventoSeleccionado) {
			this.eventoSeleccionado = eventoSeleccionado;
		}
		
		
		
		@Command
		@NotifyChange("eventoSeleccionado")
		public void seleccionarEvento(				
		@BindingParam("eventoSeleccionado")Listitem eventoParam, @SelectorParam("#bdbReporteRecaudacionEvento")Bandbox bandbox){
			
			this.eventoSeleccionado= this.listaEventos.get(eventoParam.getIndex());
		    bandbox.close();

		}
		
		public String formatearFecha(Date fecha){
			String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
			return fechaFormateada;
		}

}
