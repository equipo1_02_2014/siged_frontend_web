package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.domain.gestionatleta.Postulante;
import edu.ucla.siged.dao.DaoPostulanteI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;
import edu.ucla.siged.domain.reporte.FrecuenciaCausa;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;


public class VMFrecuenciaCausas{
	    
	    @WireVariable
	    private ServicioReporteEstadisticoI servicioReporteEstadistico;
	    
	    @WireVariable
	    private ServicioEquipoI servicioEquipo;
	    
		@Autowired
		private DaoPostulanteI daoPostulante;
	    
	    private Causa causaSeleccionada;
		
		private Date fechaDesde;
		
		private Date fechaHasta;
			
		private Integer cantidadCausaSeleccionada;
		
		private String pathProyecto;
		
		private List<Postulante> listaPostulante;
		
		private List<Integer> listaCantidadCausas;
		
		private Window window;
					
		@Init
		public void init(){
		
			this.setWindow(null);
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMFrecuenciaCausas.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			
			this.inicializarCampos();
		}
		
		@NotifyChange({"fechaDesde","fechaHasta","causaSeleccionada"})
		public void inicializarCampos(){
			
			this.fechaDesde=null;
			this.fechaHasta=null;
			this.causaSeleccionada=null;
			
			
		}
		
		@Command
		@NotifyChange({"fechaDesde","fechaHasta","causaSeleccionada"})
		public void cancelar(){
			this.inicializarCampos();
		}
				
		
		@Command
		public void imprimir(){		
			
			
				String restricciones="";
				if (this.fechaDesde!=null && this.fechaHasta!=null){
					
					String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
					String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
					restricciones += " AND p1.fecharespuesta BETWEEN '" + desde + "' AND '" + hasta + "'";
					
				}
				cantidadCausaSeleccionada=0;
				List<FrecuenciaCausa> lista= this.servicioReporteEstadistico.buscarFrecuenciaCausa(restricciones, cantidadCausaSeleccionada);
							
				
				if (!lista.isEmpty()){
					
					Map<String,Object> parameterMap = new HashMap<String,Object>();
					
					JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
					
					parameterMap.put("datasource", JRdataSource);
					
					parameterMap.put("titulo","Causa de Rechazo a los postulantes");
					
					parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
					
					parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
					
					parameterMap.put("","Causa de Rechazo a los postulantes");

					
					try {
						
						JasperDesign jasDesign=null;
						
						jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "frecuenciaCausas.jrxml");
						
						JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
				        
				        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
				        
				        JasperViewer.viewReport(jasperPrint,false);
						
					} catch (JRException e) {
						e.printStackTrace();
						Messagebox.show(e.getMessage());
					}
				
				

				}else{
					Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				}
				
			     
		}
		
		
		public Date getFechaDesde() {
			return fechaDesde;
		}

		public void setFechaDesde(Date fechaDesde) {
			this.fechaDesde = fechaDesde;
		}

		public Date getFechaHasta() {
			return fechaHasta;
		}

		public void setFechaHasta(Date fechaHasta) {
			this.fechaHasta = fechaHasta;
		}
		
		public Integer getCantidadCausaSeleccionada() {
			return cantidadCausaSeleccionada;
		}

		public void setCantidadCausaSeleccionada(Integer cantidadCausaSeleccionada) {
			this.cantidadCausaSeleccionada = cantidadCausaSeleccionada;
		}

		public List<Postulante> getListaPostulante() {
			return listaPostulante;
		}

		public List<Integer> getListaCantidadCausas() {
			return listaCantidadCausas;
		}

		public Window getWindow() {
			return window;
		}

		public void setWindow(Window window) {
			this.window = window;
		}

		public void setListaPostulante(List<Postulante> listaPostulante) {
			this.listaPostulante = listaPostulante;
		}

		public Causa getCausaSeleccionada() {
			return causaSeleccionada;
		}

		public void setCausaSeleccionada(Causa causaSeleccionada) {
			this.causaSeleccionada = causaSeleccionada;
		}
		
}
