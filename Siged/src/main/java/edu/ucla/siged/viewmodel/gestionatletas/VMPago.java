package edu.ucla.siged.viewmodel.gestionatletas;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAyudaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPagoI;

public class VMPago {

	private @WireVariable ServicioPagoI servicioPago;
	private @WireVariable Pago pago;
	private Window window;
	private Window ventaPago;
	private boolean disabled = true;
	private boolean editar = false;
	private String mes;
	// nuevo
	private @WireVariable ServicioHistoriaAyudaI servicioHistoriaAyuda;
	private List<Pago> pagosMensuales = new ArrayList<Pago>();
	private int paginaActual=0;
	private int tamanoPagina=12;
	private int registrosTotales=0;
	private boolean listaVisible= true;
	private Atleta atleta;
	private Pago pagoSeleccionado;
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("pago") Pago pagoSeleccionado) {
		Selectors.wireComponents(view, this, false);
		if (pagoSeleccionado != null) {
			this.pago = pagoSeleccionado;
			editar = true;
			listaVisible = false;
			mes = mostrarMes(pago.getMes());
		} else {
			Date fecha = new Date();
			pago.setFecha(fecha);
			pago.setAtleta(null);
			pago.setNumRecibo("");
			this.pago.setAnno(null);
			this.pago.setMes(null);
			this.disabled = false;
			this.mes = "";
		}
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	// nuevo

	@Command
	@NotifyChange({ "pago", "disabled","pagosMensuales", "paginaActual", "registrosTotales", "mes"  })
	public void guardarPago(@BindingParam("ventanaPago") Window ventanaPago) {
		if (pago.getAtleta() != null && pago.getMes() != null
				&& pago.getAnno() != null) {
			pago.setNumRecibo(pago.getAnno() + "-" + pago.getMes() + "-"
					+ pago.getAtleta().getCedula());
			int mesAux = pago.getMes()-1;
			long annoP = pago.getAnno()-1900;
			int annoAux = (int)annoP;
			Date fecha = new Date(annoAux, mesAux, 1);
			if(!servicioHistoriaAyuda.buscarBecaPorAtletaYFecha(pago.getAtleta().getId(), fecha)){
				if (servicioPago.buscarPago(pago.getNumRecibo()) == false) {
					servicioPago.guardarPago(pago);
					limpiar();
					this.ventaPago = ventanaPago;
					Messagebox
							.show("El Pago se ha realizado con satisfaccion, desea agregar otro pago",
									"Pregunta", Messagebox.YES | Messagebox.NO,
									Messagebox.QUESTION,
									new EventListener<Event>() {
	
										public void onEvent(Event event)
												throws Exception {
											// TODO Auto-generated method stub
											if (Messagebox.ON_NO.equals(event
													.getName())) {
												BindUtils.postGlobalCommand(null,
														null, "actualizarLista",
														null);
												cerrarVentanaPago();
											}
										}
									});
				} else {
					if (editar == true) {
						servicioPago.guardarPago(pago);
						limpiar();
						this.ventaPago = ventanaPago;
						Messagebox
								.show("El Pago se ha realizado con satisfaccion, desea agregar otro pago",
										"Pregunta", Messagebox.YES | Messagebox.NO,
										Messagebox.QUESTION,
										new EventListener<Event>() {
	
											public void onEvent(Event event)
													throws Exception {
												// TODO Auto-generated method stub
												if (Messagebox.ON_NO.equals(event
														.getName())) {
													BindUtils
															.postGlobalCommand(
																	null,
																	null,
																	"actualizarLista",
																	null);
													cerrarVentanaPago();
												}
											}
										});
					} else {
						Messagebox.show("Ya este Atleta hizo el pago de este mes");
						limpiar();
					}
				}
			}else{
				Messagebox.show("Este Atleta Se Encuentra Becado Para Esta Mes");
				limpiar();
			}
		} else {
			Messagebox.show("Debe seleccionar un atleta");
		}
	}
	
	
	
	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public List<Pago> getPagosMensuales() {
		return pagosMensuales;
	}

	public void setPagosMensuales(List<Pago> pagosMensuales) {
		this.pagosMensuales = pagosMensuales;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public int getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(int registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	

	public boolean isListaVisible() {
		return listaVisible;
	}

	public void setListaVisible(boolean listaVisible) {
		this.listaVisible = listaVisible;
	}

	public Pago getPagoSeleccionado() {
		return pagoSeleccionado;
	}

	public void setPagoSeleccionado(Pago pagoSeleccionado) {
		this.pagoSeleccionado = pagoSeleccionado;
	}

	public void cerrarVentanaPago() {
		this.ventaPago.detach();
	}

	@Command
	@NotifyChange({ "pago", "disabled" })
	public void buscarAtleta() {
		try {
			window = (Window) Executions.createComponents(
					"vistas/gestionatleta/listapagoatleta.zul", null, null);
			window.doModal();
		} catch (Exception e) {
			Messagebox.show(e.toString());
		}

	}

	@GlobalCommand
	@NotifyChange({ "pago", "disabled" , "pagosMensuales", "registrosTotales", "paginaActual"})
	public void atletaSeleccionado(
			@BindingParam("atletaSeleccionado") Atleta atletaSeleccionado,
			@BindingParam("ventana") Window ventana) {
		pago.setAtleta(atletaSeleccionado);
		this.paginaActual = 0;
		pagosMensuales.clear();
		actualizarListaPagoMensuales(atletaSeleccionado);
		ventana.detach();
	}

	@Command
	@NotifyChange({ "pago", "disabled", "pagosMensuales", "paginaActual", "registrosTotales", "mes","listaVisible" })
	public void limpiar() {
		this.pago.setId(null);
		this.pago.setAtleta(null);
		Date fecha = new Date();
		this.pago.setFecha(fecha);
		this.pago.setMes(null);
		this.pago.setNumRecibo("");
		this.pago.setObservacion("");
		this.pago.setAnno(null);
		this.disabled = false;
		this.editar = false;
		paginaActual=0;
		registrosTotales = 0;
		pagosMensuales.clear();
		mes = "";
		listaVisible = true;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	// nuevo

	@Command
	@NotifyChange({ "pagosMensuales", "registrosTotales", "paginaActual" })
	public void paginarListaPagos() {
		pagosMensuales.clear();
		Date fechaActual = new Date();
		int h = 1;
		int n = 12;
		if (atleta != null) {
			if ((fechaActual.getYear() - paginaActual) == atleta
					.getFechaAdmision().getYear()) {
				h = atleta.getFechaAdmision().getMonth() + 1;
			} else if (this.paginaActual == 0) {
				n = fechaActual.getMonth() + 1;
			}

			Long annoActual = Long.valueOf((fechaActual.getYear() + 1900)
					- this.paginaActual);
			for (int i = h; i <= n; i++) {
				Integer j = Integer.valueOf(i);
				Pago pago = new Pago(null, null, null, j, annoActual, null);
				pagosMensuales.add(pago);

			}
		}
	}
	
	//en el onchange de la otra
	public void actualizarListaPagoMensuales(Atleta atleta){
		this.atleta=atleta;
		Date fechaActual = new Date();
		int h = 1;
		if (fechaActual.getYear() == atleta.getFechaAdmision().getYear()) {
			h = atleta.getFechaAdmision().getMonth() + 1;
			this.registrosTotales = 12;
		} else {
			this.registrosTotales = ((fechaActual.getYear() - atleta.getFechaAdmision().getYear()) + 1) * 12; 
		}

		Long annoActual = Long.valueOf(fechaActual.getYear() + 1900);
		for (int i = h; i <= (fechaActual.getMonth() + 1); i++) {
			Integer j = Integer.valueOf(i);
			Pago pago = new Pago(null, null, null, j, annoActual, null);
			pagosMensuales.add(pago);
		}
	}
	
	@Command
	public String mostrarMes(int mes) {

		String mesString;
		switch (mes) {
		case 1:
			mesString = "ENERO";
			break;
		case 2:
			mesString = "FEBRERO";
			break;
		case 3:
			mesString = "MARZO";
			break;
		case 4:
			mesString = "ABRIL";
			break;
		case 5:
			mesString = "MAYO";
			break;
		case 6:
			mesString = "JUNIO";
			break;
		case 7:
			mesString = "JULIO";
			break;
		case 8:
			mesString = "AGOSTO";
			break;
		case 9:
			mesString = "SEPTIEMBRE";
			break;
		case 10:
			mesString = "OCTUBRE";
			break;
		case 11:
			mesString = "NOVIEMBRE";
			break;
		case 12:
			mesString = "DICIEMBRE";
			break;		
		default:
			mesString = "";
			break;
		}
		return mesString;
	}
	
	@Command
	public String mostrarStatus(int mes, long anno) {
		String estatus = "MOROSO";
		int mesAux = mes-1;
		long annoP = anno-1900;
		int annoAux = (int)annoP;
		Date fecha = new Date(annoAux, mesAux, 1);
		if(servicioHistoriaAyuda.buscarBecaPorAtletaYFecha(this.atleta.getId(), fecha)){
			estatus = "BECADO";
		}else 
			if (servicioPago.buscarPagoPorMesYAnno(this.atleta.getId(),
				mes, anno)) {
			estatus = "SOLVENTE";
		}

		return estatus;
	}
	
	@Command
	@NotifyChange({"pago","mes"})
	public void seleccionarPago(){
		mes = mostrarMes(pagoSeleccionado.getMes());
		pago.setMes(pagoSeleccionado.getMes());
		pago.setAnno(pagoSeleccionado.getAnno());
	}
	
	

}
