package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;


public class VMListaEquipo {

	Window window;

	@WireVariable ServicioEquipoI servicioEquipo;
	@WireVariable ServicioAtletaEquipoI servicioAtletaEquipo;
	@WireVariable Equipo equipo;
	private List<Equipo> listaEquipos;
	private List<Equipo> equiposSeleccionados;
	
	//---------- variables para filtrar ----------
	private String nombreEquipoFiltro="";
	private String categoriaEquipoFiltro="";
	private String rangoEquipoFiltro="";
	private String tipoEquipoFiltro="";
	boolean estatusFiltro;


	//---------- variables la paginaci�n ----------
	int paginaActual;
	int tamanioPagina;
	long registrosTotales;
	
	// --------------------------------------------------------------------------------------------------------- Inicio de M�todos Getters and Setters
	public Equipo getEquipo() {	return equipo; }
	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public List<Equipo> getListaEquipos(){ return listaEquipos; }	
	public void setListaEquipos(List<Equipo> listaEquipos) { this.listaEquipos = listaEquipos; }
		
	public List<Equipo> getEquiposSeleccionados() { return equiposSeleccionados; }
	public void setEquiposSeleccionados(List<Equipo> equiposSeleccionados) { this.equiposSeleccionados = equiposSeleccionados; }
	
	public String getNombreEquipoFiltro() { return nombreEquipoFiltro; }
	public void setNombreEquipoFiltro(String nombreEquipoFiltro) { this.nombreEquipoFiltro = nombreEquipoFiltro; }
		
	public String getCategoriaEquipoFiltro() { return categoriaEquipoFiltro; }
	public void setCategoriaEquipoFiltro(String categoriaEquipoFiltro) { this.categoriaEquipoFiltro = categoriaEquipoFiltro; }
	
	public String getRangoEquipoFiltro() { return rangoEquipoFiltro; }
	public void setRangoEquipoFiltro(String rangoEquipoFiltro) { this.rangoEquipoFiltro = rangoEquipoFiltro; }
	
	public String getTipoEquipoFiltro() { return tipoEquipoFiltro; }
	public void setTipoEquipoFiltro(String tipoEquipoFiltro) { this.tipoEquipoFiltro = tipoEquipoFiltro; }
		
	public int getPaginaActual() { return paginaActual; }
	public void setPaginaActual(int paginaActual) {	this.paginaActual = paginaActual; }

	public int getTamanioPagina() {	
		this.tamanioPagina = servicioEquipo.TAMANIO_PAGINA;
		return tamanioPagina; 
	}
	public void setTamanioPagina(int tamanioPagina) { this.tamanioPagina = tamanioPagina; }

	public long getRegistrosTotales() {	return registrosTotales; }
	public void setRegistrosTotales(long registrosTotales) { this.registrosTotales = registrosTotales; }

	// --------------------------------------------------------------------------------------------------------- Fin de M�todos Getters and Setters	
	


	@Init
	public void inicializar(){ 
		cerrarEquiposAutomtic();
		buscarEquipos();
	}
	
	@NotifyChange({"listaEquipos","listaEquiposAux", "registrosTotales", "paginaActual", "tamanioPagina"})	
	public void cerrarEquiposAutomtic(){
		Date fechaActual = new Date();
		List<Equipo> listaEquiposAux = new ArrayList<Equipo>();
		listaEquiposAux = servicioEquipo.buscarEquiposTrue();
		for(Equipo equipo : listaEquiposAux){
			if(equipo.getFechaCierre() != null){
				int validarFecha = fechaActual.compareTo(equipo.getFechaCierre());
				
				// si la fecha de cierre es igual a la fecha actual
				if(validarFecha == 0){
					equipo.setEstatus(Short.parseShort("0"));
					servicioEquipo.guardarEquipo(equipo);
					servicioAtletaEquipo.revocarAtletasEquipoCerrado(equipo);

				// si la fecha de cierre es menor a la fecha actual
				}else if( validarFecha == 1){
					equipo.setEstatus(Short.parseShort("0"));
					servicioEquipo.guardarEquipo(equipo);
					servicioAtletaEquipo.revocarAtletasEquipoCerrado(equipo);
				}
			}			
		}
	}
	
	
	@Command
	@NotifyChange({"listaEquipos","registrosTotales", "paginaActual", "tamanioPagina"})
	public void buscarEquipos(){
		this.paginaActual= 0;
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
	}
	
	@Command
	public void agregarEquipo(){
		window = (Window)Executions.createComponents("/vistas/gestionequipo/registrarequipo.zul", null, null);
		window.doModal();
	}
    
	@Command
	@NotifyChange({"equipo"})
	public void editarEquipo(@BindingParam("equipoSeleccionado") Equipo equipoSeleccionado){
		try{
			this.equipo = equipoSeleccionado;
			Map<String, Equipo> mapEquipo = new HashMap<String, Equipo>();
			mapEquipo.put( "equipo", equipoSeleccionado );		
			window = (Window)Executions.createComponents("/vistas/gestionequipo/registrarequipo.zul", null, mapEquipo);
			window.doModal();
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
					
	@GlobalCommand("paginar")
	@NotifyChange({"listaEquipos", "registrosTotales", "paginaActual", "tamanioPagina"})
	public void paginar(){
		if(estatusFiltro==false){
			this.listaEquipos = servicioEquipo.buscarEquiposTrue(this.paginaActual).getContent();
			this.registrosTotales = servicioEquipo.totalEquipos();
		}else{
			ejecutarFiltro();
		}
	}
	
	@Command
	@NotifyChange({"listaEquipos", "registrosTotales", "paginaActual", "tamanioPagina"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
    @Command
    @NotifyChange({"listaEquipos", "registrosTotales", "paginaActual", "tamanioPagina"})
	public void ejecutarFiltro(){
		String jpql=" 1=1 ";
		if((!nombreEquipoFiltro.equals("")) || (!categoriaEquipoFiltro.equals("")) || (!rangoEquipoFiltro.equals("")) || (!tipoEquipoFiltro.equals(""))){
			this.estatusFiltro = true;
			if(!nombreEquipoFiltro.equals(""))
				jpql = jpql + " and nombre like '%"+nombreEquipoFiltro+"%'";
			if(!categoriaEquipoFiltro.equals(""))
				jpql = jpql + " and categoria.nombre like '%"+categoriaEquipoFiltro+"%'";
			if(!rangoEquipoFiltro.equals(""))
				jpql = jpql + " and rango.descripcion like '%"+rangoEquipoFiltro+"%'";
			if(!tipoEquipoFiltro.equals(""))
				jpql = jpql + " and tipoEquipo.descripcion like '%"+tipoEquipoFiltro+"%'";
		}
		this.listaEquipos = servicioEquipo.buscarFiltrado(jpql, this.paginaActual);
		this.registrosTotales = servicioEquipo.totalEquiposFiltrados(jpql);
	}
	
	

	
	@Command
	@NotifyChange({"listaEquipos", "registrosTotales", "paginaActual", "tamanioPagina",
		"nombreEquipoFiltro","categoriaEquipoFiltro","rangoEquipoFiltro","tipoEquipoFiltro"})
	public void cancelarFiltro(){
		this.estatusFiltro = false;
		paginar();
		nombreEquipoFiltro="";
		categoriaEquipoFiltro="";
		rangoEquipoFiltro="";
		tipoEquipoFiltro="";
	}
	
	@GlobalCommand
	@NotifyChange({"listaEquipos", "registrosTotales", "paginaActual", "tamanioPagina", "equipo"})
	public void actualizarListaEquipos(){
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();		
	}
	
	
}