package edu.ucla.siged.viewmodel.gestionencuestas;


import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;



import java.util.Set;

import org.hibernate.Hibernate;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.web.servlet.dsp.action.ForEach;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.gestioneventos.Pregunta;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRol;
import edu.ucla.siged.servicio.impl.seguridad.ServicioControlAcceso;
import edu.ucla.siged.servicio.impl.seguridad.ServicioFuncionalidad;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioEncuestaI;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioOpcionPreguntaI;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioPreguntaI;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioPublicoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioControlAccesoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMEncuesta {
	@WireVariable
	ServicioEncuestaI servicioEncuesta; // Instancia la interface servicio	
	@WireVariable
	ServicioPreguntaI servicioPregunta; // Instancia la interface servicio	
	@WireVariable
	ServicioRolI servicioRol; // Instancia la interface servicio	
	@WireVariable
	ServicioOpcionPreguntaI servicioOpcion; // Instancia la interface servicio	
	@WireVariable
	ServicioPublicoI servicioPublico; // Instancia la interface servicio	
	
	
	
	private Encuesta encuesta;

	private Pregunta pregunta;

	private OpcionPregunta opcionPreguntaSeleccionada;
	private OpcionPregunta opcion;
	private ArrayList<Integer> listaEstatus;
	private ArrayList<Integer> listaPermisos;
	private Short estatusSeleccionado;
	private Short permisoSeleccionado;
	

	private List<Publico> listaPublicos; 

	private List<OpcionPregunta> listaOpcionesPregunta=new ArrayList<OpcionPregunta>();
	private List<OpcionPregunta> listaOpcionesEliminadas;
	private List<Publico> listaPublicosEliminados;
	
	
	private List<Rol> listaRoles =new ArrayList<Rol>();
	private Rol rolSeleccionado;
	private Boolean verResultados;
	private Boolean responderEncuesta;
	
	
	short estatusCombo=0;
	private boolean activarEstatus;
	
	

	public VMEncuesta() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            		 @ExecutionArgParam("encuestaSeleccionada") Encuesta encuestaSeleccionada) {
		 listaEstatus= new ArrayList<Integer>();
		 listaPermisos= new ArrayList<Integer>();
         listaEstatus.add(0);
         listaEstatus.add(1);
         
         listaPermisos.add(0);
         listaPermisos.add(1);
         listaPermisos.add(2);
        
         listaRoles = servicioRol.buscarActivos();
         limpiarEncuesta();
         Selectors.wireComponents(view, this, false);
		System.out.println("------------------------------------ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        if (encuestaSeleccionada!=null){
        	  this.encuesta = encuestaSeleccionada;
        	 Set<Pregunta> list=encuesta.getPreguntas();
        	 this.pregunta=(Pregunta) list.toArray()[0];
             this.listaOpcionesPregunta =cargarOpciones();
        	 //this.listaOpcionesPregunta = servicioOpcion.buscarOpciones(this.pregunta);
        	 this.listaPublicos=servicioPublico.buscarPublicoEncuesta(this.encuesta);
        	
        	
        }else{
        	
        	System.out.println("------------------------------------vaciaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            
        	this.pregunta=new Pregunta();
        	this.listaPublicos=new ArrayList<Publico>();
        	System.out.println("publicos al iniciarrrrrrrrrrrrrr:" +this.listaPublicos.size());
        	estatusSeleccionado=1;
        	this.activarEstatus=true;

        }
       }      
    
	
	
	@Command
	
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Cerrada";
		    break;
		default:
			est="Activa";
		}
		return est;
	}
	
	@Command
	public String verPermiso(Short permiso){
		String est="Ver y Responder Encuesta";
		switch (permiso){
		case 0:
			est="Responder Encuesta";
			break;
		case 1:
			est="Ver Resultados Encuesta";
		    break;
		case 2:
			est="Ver Resultados y Responder Encuesta";
		    break;
		
		}
		return est;
	}
	
	@Command
	@NotifyChange({"listaPublicos"})
	public String verPermisoPublico(Short permiso){
		String est="Ver y Responder Encuesta";
		switch (permiso){
		case 0:
			est="No permitido";
		    break;
		case 1:
			est="Permitido";
		    break;
		
		}
		return est;
	}
	
	
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Integer> getListaEstatus() {
		return listaEstatus;
	}
	
	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public void setListaEstatus(ArrayList<Integer> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	

	@Command
	@NotifyChange({"encuesta", "listaPublicos", "listaOpcionesPregunta"})
	public void guardarEncuesta(@SelectorParam(".encuestaRestriccion") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		if(!camposValidos){
			Messagebox.show("Por Favor Introduzca Campos V�lidos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}	
		else{
			if(this.encuesta.getFechaInicio().compareTo(this.encuesta.getFechaFin()) > 0)
			{
				Messagebox.show("Fecha inicio debe ser menor a fecha fin", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			if(this.listaOpcionesPregunta.size()<2)
			{
				Messagebox.show("Por favor asigne las opciones a la pregunta", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			Encuesta encontrado=servicioEncuesta.buscarEncuestaByNombre(encuesta.getNombre());
			System.out.println("entro a grabar ");
			
			if(encontrado!=null)
			System.out.println("Id de encontrado "+encontrado.getId());
			System.out.println("Id encuesta "+this.encuesta.getId());
			
			if(encontrado==null || this.encuesta.getId().equals(encontrado.getId())){
				    encuesta.setEstatus((short) 1);
			
				try{				
					pregunta.getOpcionPreguntas().size();
					if(this.listaOpcionesEliminadas.size()>0) {
						System.out.println("Por eliminar "+this.listaOpcionesEliminadas.size());
						for(OpcionPregunta op: listaOpcionesEliminadas)
						pregunta.getOpcionPreguntas().remove(op);
					}
					
					
					for(int i=0; i<this.listaOpcionesPregunta.size(); i++){
						this.pregunta.agregarOpcion(listaOpcionesPregunta.get(i));
					}
					
					encuesta.agregarPregunta(pregunta);
					
					encuesta.getPublicos().size();
					if(this.listaPublicosEliminados.size()>0) {
						for(Publico pub: listaPublicosEliminados)
						encuesta.eliminarPublico(pub);
					}
					
					for(int i=0; i<this.listaPublicos.size(); i++)
						this.encuesta.agregarPublico(listaPublicos.get(i));
				
					servicioEncuesta.guardar(encuesta);
					
					
				}
				catch(Exception e){
				    System.out.println("Excepcion"+e.getMessage());
				}
				
				Messagebox.show("Datos almacenados correctamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				limpiarEncuesta();
				Window myWindow = (Window) Path.getComponent("/vistaEncuesta");
				myWindow.detach();
			}else
			{	Messagebox.show("Ya existe una Encuesta con ese nombre", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);}
			}
			
		
	}
		
	@Command
	@NotifyChange({"encuesta", "pregunta", "listaPublicos", "listaOpcionesPregunta", 
		"rolSeleccionado", "permisoSeleccionado", "opcion", "pregunta"})
		public void limpiarEncuesta(){
			this.encuesta= new Encuesta();	
			this.encuesta.getPublicos().size();
			this.encuesta.getPreguntas().size();
			this.encuesta.getPreguntas();
			for(Pregunta pregunta: encuesta.getPreguntas())
			pregunta.getOpcionPreguntas().size();
			this.opcionPreguntaSeleccionada=new OpcionPregunta();
			limpiarPregunta();
			limpiarPublico();
		}
	
	
	// Lista de Preguntas

	@Command
	public void limpiarPregunta(){
			this.pregunta= new Pregunta();	
			this.opcion= new OpcionPregunta();
			this.opcion.setDescripcion("");
			this.listaOpcionesEliminadas= new ArrayList<OpcionPregunta>();
			this.listaOpcionesPregunta= new ArrayList<OpcionPregunta>();
		}
	
	@Command
		public void limpiarPublico(){
		this.responderEncuesta=false;
		this.rolSeleccionado=null;
		this.permisoSeleccionado=null;
		this.verResultados=false;
		this.listaPublicos= new ArrayList<Publico>();		
		this.listaPublicosEliminados= new ArrayList<Publico>();
			
		}
	
	public int buscarOpcionPregunta(OpcionPregunta opcion) {
		for(OpcionPregunta op : listaOpcionesPregunta)
		{
			if(op.getDescripcion().equals(opcion.getDescripcion()))
			{return 1;}
			
		}
		return 2;
	}
	
	
	@Command
	@NotifyChange({"listaOpcionesPregunta", "opcion"})
	public void agregarOpcionPregunta(){		
		if(this.opcion.getDescripcion()=="")
		{Messagebox.show("Debe ingresar una opcion", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);}
		else{	
		
			if(listaOpcionesPregunta.size()<5){
				switch(buscarOpcionPregunta(this.opcion)){
				case 1: {Messagebox.show("Esa opci�n ya fu� agregada a la pregunta", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
						break;}
				case 2: {this.listaOpcionesPregunta.add(this.opcion); 
						this.opcion=new OpcionPregunta();	
						this.opcion.setDescripcion("");
						break;}
				}
			}
			else
			{Messagebox.show("Ha llegado al l�mite de opciones", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);}
			
	
		}
		this.opcion=new OpcionPregunta();	
		this.opcion.setDescripcion("");
	}
	
	@Command
	@NotifyChange({"listaOpcionesPregunta", "encuesta", "listaOpcionesEliminadas"})
	public void eliminarOpcionPregunta(@BindingParam("opcionPreguntaSeleccionada") OpcionPregunta opcionPreguntaSeleccionada){
		if(opcionPreguntaSeleccionada.getId()!=null){
			if(opcionPreguntaSeleccionada.getDescripcion()!=null)
			this.listaOpcionesEliminadas.add(opcionPreguntaSeleccionada);
			
			this.listaOpcionesPregunta.remove(opcionPreguntaSeleccionada);
			System.out.println("Agregada a la lista Por eliminar "+opcionPreguntaSeleccionada.getDescripcion());
			System.out.println("lista Por eliminar size: "+this.listaOpcionesEliminadas.size());
			
		}
		else{
		
			this.listaOpcionesPregunta.remove(opcionPreguntaSeleccionada);
		}

		//Messagebox.show("Datos eliminados correctamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	public int buscarPublicoEncuesta(Publico publico) {
		for(Publico p : listaPublicos)
		{
			if(p.getRol().getId().equals(publico.getRol().getId()))
			{return 1;}
			
		}
		return 2;
	}
	
	
	@Command
	@NotifyChange({"listaPublicos", "permisoSeleccionado", "rolSeleccionado"})
	public void agregarPublico(){		
		if(this.rolSeleccionado==null || permisoSeleccionado==null)
		{Messagebox.show("Debe seleccionar un rol y un permiso", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);}
		else{	
		
		Publico publicot=new Publico();
		publicot.setRol(rolSeleccionado);
		switch(permisoSeleccionado){
		case 0: 
			publicot.setResponder((short) 1);
			publicot.setVerResultados((short) 0);
			break;
		case 1:
			publicot.setResponder((short) 0);
			publicot.setVerResultados((short) 1);
			break;
		case 2:
			publicot.setResponder((short) 1);
			publicot.setVerResultados((short) 1);
			break;
		}
		
		switch(buscarPublicoEncuesta(publicot)){
		case 1: {Messagebox.show("Ese rol ya fue configurado", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				break;}
		case 2: {listaPublicos.add(publicot); 
				this.permisoSeleccionado=null;
				this.rolSeleccionado=null;
				break;}
		}
			
		}
		this.permisoSeleccionado=null;
		this.rolSeleccionado=null;
	}
	
	@Command
	@NotifyChange({"listaPublicos", "encuesta"})
	public void eliminarPublico(@BindingParam("publicoSeleccionado") Publico publicoSeleccionado){
		if(publicoSeleccionado.getId()!=null){
		
			if(publicoSeleccionado.getId()!=null)
			this.listaPublicosEliminados.add(publicoSeleccionado);
			
			this.listaPublicos.remove(publicoSeleccionado);
		}
		else{
		
			this.listaPublicos.remove(publicoSeleccionado);
		}

		//Messagebox.show("Datos eliminados correctamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	
	public boolean isActivarEstatus() {
		return activarEstatus;
	}

	public void setActivarEstatus(boolean activarEstatus) {
		this.activarEstatus = activarEstatus;
	}

	public Encuesta getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	public OpcionPregunta getOpcionPreguntaSeleccionada() {
		return opcionPreguntaSeleccionada;
	}

	public void setOpcionPreguntaSeleccionada(
			OpcionPregunta opcionPreguntaSeleccionada) {
		this.opcionPreguntaSeleccionada = opcionPreguntaSeleccionada;
	}

	public ArrayList<Integer> getListaPermisos() {
		return listaPermisos;
	}

	public void setListaPermisos(ArrayList<Integer> listaPermisos) {
		this.listaPermisos = listaPermisos;
	}

	public Short getPermisoSeleccionado() {
		return permisoSeleccionado;
	}

	public void setPermisoSeleccionado(Short permisoSeleccionado) {
		this.permisoSeleccionado = permisoSeleccionado;
	}

	public List<Publico> getListaPublicos() {
		return listaPublicos;
	}

	public void setListaPublicos(List<Publico> listaPublicos) {
		this.listaPublicos = listaPublicos;
	}

	public List<OpcionPregunta> getListaOpcionesPregunta() {
		return listaOpcionesPregunta;
	}

	public void setListaOpcionesPregunta(List<OpcionPregunta> listaOpcionesPregunta) {
		this.listaOpcionesPregunta = listaOpcionesPregunta;
	}

	public List<OpcionPregunta> getListaOpcionesEliminadas() {
		return listaOpcionesEliminadas;
	}

	public void setListaOpcionesEliminadas(
			List<OpcionPregunta> listaOpcionesEliminadas) {
		this.listaOpcionesEliminadas = listaOpcionesEliminadas;
	}

	public List<Publico> getListaPublicosEliminados() {
		return listaPublicosEliminados;
	}

	public void setListaPublicosEliminados(List<Publico> listaPublicosEliminados) {
		this.listaPublicosEliminados = listaPublicosEliminados;
	}

	public List<Rol> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<Rol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public Rol getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Rol rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}

	public Boolean getVerResultados() {
		return verResultados;
	}

	public void setVerResultados(Boolean verResultados) {
		this.verResultados = verResultados;
	}

	public Boolean getResponderEncuesta() {
		return responderEncuesta;
	}

	public void setResponderEncuesta(Boolean responderEncuesta) {
		this.responderEncuesta = responderEncuesta;
	}

	public OpcionPregunta getOpcion() {
		return opcion;
	}

	public void setOpcion(OpcionPregunta opcion) {
		this.opcion = opcion;
	}
	public List<OpcionPregunta> cargarOpciones(){
		 this.pregunta.getOpcionPreguntas().size();
    	 System.out.println(this.pregunta.getOpcionPreguntas().size());
    		Object[] opcionest=pregunta.getOpcionPreguntas().toArray();
			OpcionPregunta opciont= new OpcionPregunta();
			List<OpcionPregunta> lista = new ArrayList<OpcionPregunta>();
			for(int i=0; i<opcionest.length;i++)
			{
				opciont= (OpcionPregunta) opcionest[i];
				lista.add(opciont);
			}
			return lista;
	}

	
	
	
}