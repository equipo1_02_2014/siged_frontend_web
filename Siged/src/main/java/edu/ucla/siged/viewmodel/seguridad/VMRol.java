package edu.ucla.siged.viewmodel.seguridad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;














import org.zkoss.zul.impl.InputElement;






import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRol;
import edu.ucla.siged.servicio.impl.seguridad.ServicioControlAcceso;
import edu.ucla.siged.servicio.impl.seguridad.ServicioFuncionalidad;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioControlAccesoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMRol {
	@WireVariable
	ServicioUsuarioI servicioUsuario; // Instancia la interface servicio	
	@WireVariable
	ServicioRolI servicioRol; // Instancia la interface servicio	
	@WireVariable
	ServicioFuncionalidadI servicioFuncionalidad; // Instancia la interface servicio	
	@WireVariable
	ServicioControlAccesoI servicioControlAcceso; // Instancia la interface servicio	
	
	@WireVariable
	Rol rol;
	private ArrayList<Integer> listaEstatus;
	private Short estatusSeleccionado;
	private Funcionalidad funcionalidadSeleccionada;
	
	private List<Funcionalidad> listaFuncionalidades; //combo de agregar
	private List<ControlAcceso> funcionalidadesRolSeleccionados;
	
	@WireVariable
	private ControlAcceso funcionalidadRolSeleccionado=new ControlAcceso();
	private List<ControlAcceso> listaFuncionalidadesRol; 
	private List<ControlAcceso> listaFuncionalidadesEliminadas;
	
	short estatusCombo=0;
	private boolean activarEstatus;
	
	

	public VMRol() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            		 @ExecutionArgParam("rolSeleccionado") Rol rolSeleccionado) {
		 listaEstatus= new ArrayList<Integer>();
		 
         listaEstatus.add(0);
         listaEstatus.add(1);
      
         listaFuncionalidades = servicioFuncionalidad.buscarFuncionalidadesHojasActivas();
         listaFuncionalidadesEliminadas= new ArrayList<ControlAcceso>();
         
         Selectors.wireComponents(view, this, false);
		System.out.println("------------------------------------ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        if (rolSeleccionado!=null){
        	//listaFuncionalidadesRol= rolSeleccionado.getFuncionalidades();
        	this.listaFuncionalidadesRol=servicioControlAcceso.getFuncionalidadesRol(rolSeleccionado);
            this.rol = rolSeleccionado;
            System.out.println("ROL SELCCIONADO:"+ rol.getNombre());
            this.estatusSeleccionado= this.rol.getEstatus();
            this.activarEstatus=false;
        
        }else{
        	limpiarRol();
        	System.out.println("------------------------------------Esta vaciiiiiiiiiiiiiiiiiiiiaaaaaaaaaaaa");
        	this.rol= new Rol();
        	listaFuncionalidadesRol= new ArrayList<ControlAcceso>();
        	
        	estatusSeleccionado=1;
        	this.activarEstatus=true;

        }
       }      
    
	
	
	@Command
	@NotifyChange({"listaFuncionalidadesRol"})
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
	
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Integer> getListaEstatus() {
		return listaEstatus;
	}
	
	
	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	
	public List<Funcionalidad> getListaFuncionalidades() {
		return listaFuncionalidades;
	}

	public void setListaFuncionalidades (List<Funcionalidad> listaFuncionalidades) {
		this.listaFuncionalidades = listaFuncionalidades;
	}

	/*public AImage getImagenT() {
		return imagenT;
	}

	public void setImagenT(AImage imagenT) {
		this.imagenT = imagenT;
	}
*/
	public List<ControlAcceso> getFuncionalidadesRolSeleccionados() {
		return funcionalidadesRolSeleccionados;
	}

	public void setFuncionalidadesRolSeleccionados(
			List<ControlAcceso> funcionalidadesRolSeleccionados) {
		this.funcionalidadesRolSeleccionados = funcionalidadesRolSeleccionados;
	}

	public ControlAcceso getFuncionalidadRolSeleccionado() {
		return funcionalidadRolSeleccionado;
	}

	public void setFuncionalidadRolSeleccionado(ControlAcceso funcionalidadRolSeleccionado) {
		this.funcionalidadRolSeleccionado = funcionalidadRolSeleccionado;
	}

	public List<ControlAcceso> getListaFuncionalidadesRol() {
		return this.listaFuncionalidadesRol;
	}

	public void setListaFuncionalidadesRol(List<ControlAcceso> listaFuncionalidadesRol) {
		this.listaFuncionalidadesRol = listaFuncionalidadesRol;
	}

	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public void setListaEstatus(ArrayList<Integer> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	public Funcionalidad getFuncionalidadSeleccionada() {
		return funcionalidadSeleccionada;
	}

	public void setFuncionalidadSeleccionada(Funcionalidad funcionalidadSeleccionada) {
		this.funcionalidadSeleccionada = funcionalidadSeleccionada;
	}

	@Command
	@NotifyChange({"rol"})
	public void guardarRol(@SelectorParam(".rolRestriccion") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		if(!camposValidos){
			Messagebox.show("Por Favor Introduzca Campos V�lidos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		if (rol.getNombre().equals("") || rol.getDescripcion().equals("") || rol.getFechaRegistro()==null)	
			Messagebox.show("No pueden haber campos vac�os", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);

		else{
			if(this.listaFuncionalidadesRol.size()==0)
			{
				Messagebox.show("Por favor asigne al menos una funcionalidad al rol", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			Rol encontrado=servicioRol.buscarRolByNombre(rol.getNombre());
			System.out.println("entro a grabar ");
			if(encontrado==null || rol.getId().equals(encontrado.getId())){
				    rol.setEstatus((short) 1);
				try{
					for(int i=0; i<this.listaFuncionalidadesRol.size(); i++)
						listaFuncionalidadesRol.get(i).setRol(rol);
					servicioRol.guardar(rol);		
					System.out.println("guardo el rol");
					if(listaFuncionalidadesRol.size()>0)
					servicioControlAcceso.guardarListaFuncionalidadRol(listaFuncionalidadesRol);
					if(listaFuncionalidadesEliminadas.size()>0)
					{
						servicioControlAcceso.eliminarVarios(listaFuncionalidadesEliminadas);
					}
					
				}
				catch(Exception e){
				    System.out.println("Excepcion"+e.getMessage());
				}
				Messagebox.show("Datos almacenados correctamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				limpiarRol();
				Window myWindow = (Window) Path.getComponent("/vistaRol");
				myWindow.detach();
			}else
			{	Messagebox.show("Ya existe un rol con ese nombre", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);}
			}
			
		
	}
		
	@Command
	@NotifyChange({"rol", "listaFuncionalidadesRol"})
		public void limpiarRol(){
			this.rol= new Rol();	
			limpiarFuncionalidadesRol();
		}
	
	
	// Lista de Funcionalidades del rol

	@Command
	@NotifyChange({"rol","listaFuncionalidadesRol"})
		public void limpiarFuncionalidadesRol(){
			this.listaFuncionalidadesRol= new ArrayList<ControlAcceso>();		
		}
	
	public int buscarFuncionalidadRol(ControlAcceso control) {
		for(ControlAcceso ru : listaFuncionalidadesRol)
		{
			if(ru.getFuncionalidad().getNombre().equals(control.getFuncionalidad().getNombre()) && ru.getEstatus()==1)
			{return 1;}
			if(ru.getFuncionalidad().getNombre().equals(control.getFuncionalidad().getNombre()) && ru.getEstatus()==0) 
			{ru.setEstatus((short) 1);
			return 2;}
		}
		return 3;
	}
	
	
	@Command
	@NotifyChange({"rol","funcionalidadRol","listaFuncionalidadesRol"})
	public void agregarFuncionalidadRol(){		
		if(funcionalidadSeleccionada==null)
		{Messagebox.show("Debe seleccionar una funcionalidad", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);}
		else{	
		ControlAcceso funcionalidadRolt= new ControlAcceso ((short) 1, funcionalidadSeleccionada);
		switch(buscarFuncionalidadRol(funcionalidadRolt)){
		case 1: {Messagebox.show("Esa funcionalidad ya fu� agregada al rol", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				break;}
		case 2: {Messagebox.show("Esa funcionalidad ha sido reactivada", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				break;}
		case 3: {listaFuncionalidadesRol.add(funcionalidadRolt); break;}
		}
			
		}
	}
	
	@Command
	@NotifyChange({"listaFuncionalidadesRol", "rol"})
	public void eliminarFuncionalidadRol(@BindingParam("funcionalidadRolSeleccionado") ControlAcceso funcionalidadRolSeleccionado){
		if(funcionalidadRolSeleccionado.getId()!=null){
		
			/*funcionalidadRolSeleccionado.setEstatus(Short.parseShort("0"));
			System.out.println("Se cambio el estatus"+funcionalidadRolSeleccionado.getEstatus());
			*/
			this.listaFuncionalidadesEliminadas.add(funcionalidadRolSeleccionado);
			this.listaFuncionalidadesRol.remove(funcionalidadRolSeleccionado);
		}
		else{
		
		this.listaFuncionalidadesRol.remove(funcionalidadRolSeleccionado);
		}

		Messagebox.show("Datos eliminados correctamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	public void eliminarFuncionalidadesRolRegistradas(ControlAcceso control) {
		this.listaFuncionalidadesEliminadas.add(control);
		this.listaFuncionalidadesRol.remove(control);
	}
	
	@Command
	@NotifyChange({"listaFuncionalidadesRol", "rol"})
	public void eliminarFuncionalidadesRol(){
		if(funcionalidadesRolSeleccionados!=null){
			ControlAcceso controlTemp =null;
			for (int i=0;i<funcionalidadesRolSeleccionados.size();i++){

				controlTemp=funcionalidadesRolSeleccionados.get(i);	
							
				if(controlTemp.getId()!=null){
					this.eliminarFuncionalidadesRolRegistradas(controlTemp);
				} else
				{
				this.listaFuncionalidadesRol.remove(controlTemp);
				}
				System.out.println(controlTemp.getFuncionalidad().getNombre());
			}	
			this.funcionalidadesRolSeleccionados=null;

			Messagebox.show("Datos eliminados correctamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay datos seleccionados", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
		}
	}

	public boolean isActivarEstatus() {
		return activarEstatus;
	}

	public void setActivarEstatus(boolean activarEstatus) {
		this.activarEstatus = activarEstatus;
	}
	
	@Command
	
	public void iniciarSesion(){
		Usuario usuario=servicioUsuario.buscarByNombreUsuario("xx");
		if(usuario==null)
			Messagebox.show("Usuario o clave incorrectos", "Error", Messagebox.OK, Messagebox.ERROR);
		System.out.println(usuario.getNombreUsuario());
		Messagebox.show("Inicia", "Bienvenido", Messagebox.OK, Messagebox.INFORMATION);
		  Executions.sendRedirect("/");	
		
	}
	
	@Command
	public void doLogout(){
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("userCredential");
		Executions.sendRedirect("/portalPrincipal.zul");
	}
	
}