/** 
 * 	VMAnotador
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.util.Date;
import java.util.List;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAnotadorI;

public class VMAnotador {
	@WireVariable
	ServicioAnotadorI servicioAnotador;
	@WireVariable
	Anotador anotador;
	List<Short> listaEstatus;
	@WireVariable
	Short estatusSeleccionado;
	Window my;
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.


	public Anotador getAnotador() {
		return anotador;
	}
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public void setAnotador(Anotador anotador) {
		this.anotador = anotador;
	}
	
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Short> getListaEstatus() {
		return listaEstatus;
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("anotador") Anotador anotadorSeleccionado) {
           
        if (anotadorSeleccionado!=null){
            this.anotador = anotadorSeleccionado;
            this.tipoOperacion=2;

        }else{
        		this.tipoOperacion=1;
                anotador = new Anotador();
                anotador.setFechaRegistro(new Date());
              }
        }
      
 	
	@Command
	@NotifyChange({"anotador","listaAnotadores"})   
	public void guardar(){
		if (anotador.getCedula()=="" || anotador.getCedula()==null || anotador.getNombre()=="" || 
			anotador.getNombre()==null || anotador.getApellido()==""  || anotador.getApellido()==null ||  
			anotador.getTelefono()==null || anotador.getTelefono()=="" || anotador.getCelular()=="" || 
			anotador.getCelular()==null || anotador.getFechaRegistro()==null){
				Messagebox.show("Debe completar todos los campos para continuar", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		else {
		
			try{
				Messagebox.show("�Desea agregar el registro?","Pregunta",
				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				         new org.zkoss.zk.ui.event.EventListener<Event>() {
		                    
			              public void onEvent(Event evt) throws InterruptedException {
			            	  
		       		       if (evt.getName().equalsIgnoreCase("onYes")) {
		       		        	servicioAnotador.guardar(anotador);
		       		        	Messagebox.show("El registro se ha guardado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
				       		    if (tipoOperacion==1){
				       		        Messagebox.show("�Desea agregar otro registro?","Pregunta",
									Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
									new org.zkoss.zk.ui.event.EventListener<Event>() {
				       		        	public void onEvent(Event evt) throws Exception {
											if (evt.getName().equalsIgnoreCase("onYes")) {
												BindUtils.postGlobalCommand(null, null, "limpiarAnotador", null);
											}else{
												BindUtils.postGlobalCommand(null, null, "asignarTipoOperacionGuardarAnotador", null);
												BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
												Window myWindow = (Window) Path.getComponent("/vistaAnotador");
												myWindow.detach();
								        		}
										}
				       		        });
				       		     }
				       		     else{
				       		    	BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
				       				Window myWindow = (Window) Path.getComponent("/vistaAnotador");
									myWindow.detach();
				       		     }
		       		    	 }
			              }
				});
			
		}catch(Exception ex){
			Messagebox.show("Error al Registrar Anotador","Error", Messagebox.OK, Messagebox.ERROR);
		}
		
			
    
	}	
	}

	@GlobalCommand("limpiarAnotador")
	@NotifyChange({"anotador","estatusSeleccionado"})
	public void limpiar(){
	anotador.setId(null);
	anotador.setCedula("");
	anotador.setNombre("");
	anotador.setApellido("");
	anotador.setTelefono("");
	anotador.setCelular("");
	anotador.setFechaRegistro(new Date());

	}
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("anotadorSeleccionado") Anotador anotadorSeleccionado ) {
		Window window = (Window)Executions.createComponents("anotador.zul", null, null);
		this.anotador = anotadorSeleccionado;
		window.doModal();
	}
	
	@Command
    @NotifyChange({"anotador"})
    public String verStatus(Short estatus){
        String est="";
        
        switch (estatus){
            case 0:
                est="Inactivo";
                break;
            case 1:
                est="Activo";
                break;
            default:
                est="Seleccione Estatus";
        }
    
        return est;
    }

	@Command
	@NotifyChange({"anotador"})
	public void validarCedula(){
		boolean existe=false;
		if (anotador.getCedula()!="" && anotador.getCedula()!=null){
		   existe = servicioAnotador.existeAnotador(anotador.getCedula(), anotador.getId());
		   if (existe==true){
			  Messagebox.show("La cedula ya existe","Error",Messagebox.OK,Messagebox.ERROR);
			  anotador.setCedula("");
		   }
		   
		}
		
	}

	}