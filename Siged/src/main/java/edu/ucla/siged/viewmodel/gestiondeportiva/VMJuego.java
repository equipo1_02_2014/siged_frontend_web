/**
 * Clase: VMJuego
 * 
 * Version 1.0: Clase que corresponde al View Model de Juego
 * 
 * Fecha de Ultima Modificacion: 06/12/2014
 * 
 * Autor: Jesus Baute
 * 
 * */



package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.utilidades.Persona;
import edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.PosicionArbitro;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;

public class VMJuego {
   
	@WireVariable
    private ServicioCompetenciaI servicioCompetencia;
	
	@WireVariable
    private ServicioEquipoI servicioEquipo;
	
	@WireVariable
    private ServicioJuegoI servicioJuego;
	
	@WireVariable
    private ServicioPracticaI servicioPractica;
	
	private List<Competencia> listaCompetenciasVigentes;
	
	private List<Equipo> listaEquiposFundacion;
	
	private List<Area> listaAreas;
	
	private List<Anotador> listaAnotadores;
	
	private List<ArbitroJuego> listaArbitroJuego;
	
	private List<Arbitro> listaArbitros;
	
	private List<Arbitro> listaArbitrosOrig;
	
	private List<PosicionArbitro> listaPosicionArbitro;
	
	private Competencia competenciaSeleccionada;
	
    private long cantidadCompetencias;
	
	private int paginaActual;
	
	private Equipo equipoSeleccionado;
	
	private Area areaSeleccionada;
	
	private Anotador anotadorSeleccionado;
	
	private String nombreLugar;
	
	private boolean soloLecturaLugar;
	
	private String observacion;
	
	private Date fecha;
	
	private Date hora;
	
	private String escuelaContraria;
	
	private String equipoContrario;
	
	private Juego juego;
	
	private Window window;
	
	private Tab tabInformacion;
	
	@Init
	public void init(){
		
		this.listaAreas= servicioJuego.buscarAreasJuego();
		
		this.listaEquiposFundacion= servicioEquipo.buscarEquiposRosterConTecnico();
		
		Area area= new Area();
		
		area.setNombre("OTRO");
		
		this.listaAreas.add(area);
		
		this.listaPosicionArbitro= servicioJuego.buscarPosicionesArbitros();
		
		this.window=null;
		
		this.tabInformacion= null;
		
        this.inicializarCampos();
	}
	
	@Command
	@NotifyChange("competenciaSeleccionada")
	public void seleccionarCompetencia(
			@BindingParam("competenciaSeleccionada")Listitem competenciasParam,
			@SelectorParam("#bdbGestionDeportivaJuegoInformacionCompetencia")Bandbox bandbox){
		
	    this.competenciaSeleccionada= this.listaCompetenciasVigentes.get(competenciasParam.getIndex());
	    
		bandbox.close();

	}
	
	@Command
	@NotifyChange({"cantidadCompetencias","listaCompetenciasVigentes"})
	public void paginarCompetencias(){
		
		this.listaCompetenciasVigentes= servicioCompetencia.buscarCompetenciasFuturasYVigentes(this.paginaActual);
		this.cantidadCompetencias= servicioCompetencia.obtenerCantidadCompetenciasFuturasYVigentes();
		
	}
	
	public int getTamanioPagina(){
		return servicioCompetencia.getTamanioPagina();
	}
	
	@Command
	@NotifyChange({"soloLecturaLugar","nombreLugar"})
	public void habilitarLugar(){
		
		if (this.areaSeleccionada==null){
			this.soloLecturaLugar=true;
			this.nombreLugar="";
		}
		else if(!this.areaSeleccionada.getNombre().equalsIgnoreCase("OTRO")){
			this.soloLecturaLugar=true;
			this.nombreLugar= this.areaSeleccionada.getNombre();
		}
		else{
			this.soloLecturaLugar=false;
			this.nombreLugar="";
		}
		
		
		
	}
	
	@Command
	public void cerrarVentana(){
		BindUtils.postGlobalCommand(null, null, "paginarJuego", null);
	}
	
	@Command
	public void guardar(@SelectorParam(".juego_restriccion") LinkedList<InputElement> inputs,
			            @SelectorParam("#wdwGestionDeportivaJuego") Window win,
			            @SelectorParam("#tabGestionDeportivaJuegoInformacion")Tab tabInf){	
	
		boolean camposValidos=true;
		
		for(InputElement input:inputs){
			
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
			
		}
		
		/*Validando Campos*/
		if(!camposValidos){
			
			Messagebox.show("Por Favor Introduzca Campos Validos", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/* Validar que la Fecha del Juego coincida que la fecha de inicio y fin de la competencia */
		else if(this.fecha.compareTo(this.competenciaSeleccionada.getFechaInicio()) < 0 || 
		        this.fecha.compareTo(this.competenciaSeleccionada.getFechaFin()) > 0){
			
			Messagebox.show("La Fecha no esta en el Rango de Tiempo de la Competencia", 
			        "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/*Validar que el Equipo ya no Tenga un Juego Planificado*/
		else if(servicioJuego.isEquipoJuegoFechaHora(this.equipoSeleccionado, this.fecha, this.hora)){
			
			Messagebox.show("El Equipo ya tiene un Juego Planificado en ese Dia y Hora", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/*Validar que el Equipo ya no Tenga una Practica Planificada*/
        else if(servicioPractica.isEquipoPracticaFechaHora(this.equipoSeleccionado, this.fecha, this.hora)){
			
			Messagebox.show("El Equipo ya tiene una Practica Planificada en ese Dia y Hora", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/*Validar que el Area este en el horario indicado*/
        else if(!this.areaSeleccionada.getNombre().equalsIgnoreCase("OTRO") && 
        		servicioJuego.isAreaOcupadaHorario(this.areaSeleccionada, this.fecha, this.hora)){
        	
        	Messagebox.show("El Area no se encuentra disponible a esa hora", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }
		/*Validar que el Area no tenga un juego Planificado*/
        else if(!this.areaSeleccionada.getNombre().equalsIgnoreCase("OTRO") && 
        		servicioJuego.isAreaOcupadaFechaHora(this.areaSeleccionada, this.fecha, this.hora)){
        	
        	Messagebox.show("El Area ya tiene un Juego Planificado en ese Dia y Hora", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }/*Validar que el Area no tenga una practica Planificada*/
        else if(!this.areaSeleccionada.getNombre().equalsIgnoreCase("OTRO") && 
        		servicioPractica.isAreaOcupadaFechaHora(this.areaSeleccionada, this.fecha, this.hora)){
        	
        	Messagebox.show("El Area ya tiene una Practica Planificada en ese Dia y Hora", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }/*Validar que los Arbitros No se Repitan*/
        else if(servicioJuego.existeArbitrosRepetidos(this.listaArbitroJuego)){
        	
        	Messagebox.show("Los Arbitro Seleccionados No Pueden Repetirse", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }/*Inicio de Operacion*/
        else{
        	
        	juego= new Juego();
        	
        	juego.setCompetencia(this.competenciaSeleccionada);
        	
        	juego.setAnotador(this.anotadorSeleccionado);
        	
        	juego.setEquipo(this.equipoSeleccionado);
        	
        	juego.setEscuelaContraria(this.escuelaContraria);
        	
        	juego.setEquipoContrario(this.equipoContrario);
        	
        	juego.setFecha(this.fecha);
        	
        	juego.setHoraInicio(this.hora);
        	
        	juego.setLugar(this.nombreLugar);
        	
        	juego.setObservacion(this.observacion);
        	
        	juego.setResultado(-1);
        	
        	if(!this.areaSeleccionada.getNombre().equalsIgnoreCase("OTRO")){
        		
        	   juego.setArea(this.areaSeleccionada);
        	   
        	}else{
        		
        		juego.setArea(null);
        		
        	}
        	
        	for(ArbitroJuego aj:this.listaArbitroJuego){
        		aj.setJuego(juego);
        	}
        	
        	Set<ArbitroJuego> setArbitroJuego= new HashSet<ArbitroJuego>(this.listaArbitroJuego);
        	
        	juego.setArbitroJuego(setArbitroJuego);
        	
        	this.window= win;
        	
        	this.tabInformacion= tabInf;
        	
        	try{
        		
        		
        		Messagebox.show("Desea Registrar el Juego?","Confirmacion",
        				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
        				         new org.zkoss.zk.ui.event.EventListener<Event>() {
        		                     
        			              public void onEvent(Event evt) throws InterruptedException {
        			            	  
				        		        if (evt.getName().equalsIgnoreCase("onYes")) {
				        		        	
				        		           servicioJuego.guardar(juego);
				        		           
				        		           Messagebox.show("Juego Guardado con Exito", 
				        	    	                       "Information", Messagebox.OK, 
				        	    	                        Messagebox.INFORMATION);
				        		           
				        		           Messagebox.show("Desea Registrar otro Juego?","Confirmacion",
				          				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				          				         new org.zkoss.zk.ui.event.EventListener<Event>() {

													public void onEvent(
															Event evt)
															throws Exception {
														
														if (evt.getName().equalsIgnoreCase("onYes")) {
															BindUtils.postGlobalCommand(null, null, "refreshjuego", null);
									        		           
									        		        tabInformacion.setSelected(true);
														}else{
															BindUtils.postGlobalCommand(null, null, "paginarJuego", null);
															window.onClose();
														}
														
													}
				        		        	   
				        		           });
				        		           
				        		        }
        			              }
        		});
        		
        	}catch(Exception ex){
        		Messagebox.show("Error al Registrar Juego", 
    	                "Error", Messagebox.OK, Messagebox.ERROR);
        	}
        }
		
	}
	
    @Command
    @NotifyChange({"listaAnotadores","anotadorSeleccionado","listaArbitros"})
    public void cambiarArbitroAnotador(@SelectorParam("#tabGestionDeportivaJuegoInformacion")Tab tabInf,
    		                   @SelectorParam("#dtbGestionDeportivaJuegoInformacionFecha")Datebox date,
    		                   @SelectorParam("#tmbGestionDeportivaJuegoInformacionHoraInicio")Timebox time){
    	
    	
    	if (!date.isValid() || !time.isValid()){
    		Messagebox.show("Seleccione una Fecha y una Hora para el Juego");
    		tabInf.setSelected(true);
    		this.anotadorSeleccionado=null;
    	}else{
    		this.listaAnotadores= servicioJuego.buscarAnotadoresDisponibles(this.fecha,this.hora);
    		this.listaArbitros= servicioJuego.buscarArbitrosDisponibles(this.fecha, this.hora);
    		this.listaArbitrosOrig= servicioJuego.buscarArbitrosDisponibles(this.fecha, this.hora);
    		
    	}
    	
    }
    
    @Command
    @NotifyChange("listaArbitros")
    public void actualizarArbitros(){
    
    	if (this.listaArbitrosOrig!=null){
    	   this.listaArbitros= new ArrayList<Arbitro>(this.listaArbitrosOrig);	
    	}
    	
    	for (ArbitroJuego aj:this.listaArbitroJuego){
    		if (aj.getArbitro()!=null){
    			this.listaArbitros.remove(aj.getArbitro());
    		}
    	}
    	
    }
    
    @NotifyChange("listaArbitroJuego")
    private void inicialiarListaArbitroJuego(){
    	
    	if (this.listaPosicionArbitro!=null && !this.listaPosicionArbitro.isEmpty()){
 		   this.listaArbitroJuego= new ArrayList<ArbitroJuego>();
 		   
 		   for(PosicionArbitro posicion:listaPosicionArbitro){
 			   ArbitroJuego arbitroJuego= new ArbitroJuego();
 			   
 			   arbitroJuego.setPosicionarbitro(posicion); //No cumple con el estandar java
 			   
 			   this.listaArbitroJuego.add(arbitroJuego);
 		   }
 		}
    }
	
    @NotifyChange({"listaArbitroJuego","listaAnotadores","listaArbitros",
    	           "listaArbitrosOrig","soloLectura","competenciaSeleccionada",
    	           "equipoSeleccionado","areaSeleccionada","nombreLugar",
    	           "observacion","fecha","hora","anotadorSeleccionado",
    	           "escuelaContraria","equipoContrario"})
    public void inicializarCampos(){
    	
       this.inicialiarListaArbitroJuego();
       
       this.listaCompetenciasVigentes= servicioCompetencia.buscarCompetenciasFuturasYVigentes(0);
       
       this.listaCompetenciasVigentes= servicioCompetencia.buscarCompetenciasActivas(0).getContent();
       
       this.paginaActual= 0;
		
	   this.cantidadCompetencias= servicioCompetencia.obtenerCantidadCompetenciasFuturasYVigentes();
    	
       this.listaAnotadores= new ArrayList<Anotador>();
 		
 	   this.listaArbitros= new ArrayList<Arbitro>();
 		
 	   this.listaArbitrosOrig= new ArrayList<Arbitro>();
 		
 	   this.soloLecturaLugar=true;
 	
 	   this.competenciaSeleccionada=null;	
 		
 	   this.equipoSeleccionado= null;
 		
 	   this.areaSeleccionada=null;
 		
 	   this.nombreLugar="";
 		
 	   this.observacion="";
 		
 	   this.fecha=null;
 		
 	   this.hora=null;
 		
 	   this.anotadorSeleccionado= null;
 		
 	   this.escuelaContraria=null;
 		
 	   this.equipoContrario=null; 
    	
 	   this.juego= null;
    }
    
    @Command
    @NotifyChange({"listaArbitroJuego","listaAnotadores","listaArbitros",
                   "listaArbitrosOrig","soloLectura","competenciaSeleccionada",
                   "equipoSeleccionado","areaSeleccionada","nombreLugar",
                   "observacion","fecha","hora","anotadorSeleccionado",
                   "escuelaContraria","equipoContrario"})
    public void cancelar(@SelectorParam("#tabGestionDeportivaJuegoInformacion")Tab tabInf){
        
       this.inicializarCampos();	
	   tabInf.setSelected(true);
	   
    }
    
    @GlobalCommand("refreshjuego")
    @NotifyChange({"listaArbitroJuego","listaAnotadores","listaArbitros",
        "listaArbitrosOrig","soloLectura","competenciaSeleccionada",
        "equipoSeleccionado","areaSeleccionada","nombreLugar",
        "observacion","fecha","hora","anotadorSeleccionado",
        "escuelaContraria","equipoContrario"})
    public void refresh(){
    	 this.inicializarCampos();	
    }
    
    public String obtenerNombreCompleto(Persona persona){
    	String nombreCompleto="";
    	
    	if (persona!=null){
    		nombreCompleto= persona.getNombre() + " " + persona.getApellido(); 
    	}
    	
    	return nombreCompleto;
    }
	
	public String formatearFecha(Date fecha){
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
	
	public List<Competencia> getListaCompetenciasVigentes() {
		
		return listaCompetenciasVigentes;
		
	}

	public void setListaCompetenciasVigentes(
			List<Competencia> listaCompetenciasVigentes) {
		
		this.listaCompetenciasVigentes = listaCompetenciasVigentes;
		
	}

	public Competencia getCompetenciaSeleccionada() {
		
		return competenciaSeleccionada;
		
	}

	public void setCompetenciaSeleccionada(Competencia competenciaSeleccionada) {
		
		this.competenciaSeleccionada = competenciaSeleccionada;
		
	}

	public List<Equipo> getListaEquiposFundacion() {
		return listaEquiposFundacion;
	}

	public Equipo getEquipoSeleccionado() {
		return equipoSeleccionado;
	}

	public void setEquipoSeleccionado(Equipo equipoSeleccionado) {
		this.equipoSeleccionado = equipoSeleccionado;
	}

	public List<Area> getListaAreas() {
		return listaAreas;
	}

	public Area getAreaSeleccionada() {
		return areaSeleccionada;
	}


	public void setAreaSeleccionada(Area areaSeleccionada) {
		this.areaSeleccionada = areaSeleccionada;
	}

	public String getNombreLugar() {
		return nombreLugar;
	}

	public void setNombreLugar(String nombreLugar) {
		this.nombreLugar = nombreLugar;
	}

	public boolean isSoloLecturaLugar() {
		return soloLecturaLugar;
	}
	
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Date getFecha() {
		return fecha;
	}
    
	public void setFecha(Date fecha) {
		this.fecha = fecha;

	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
		
	}

	public List<Anotador> getListaAnotadores() {
		return listaAnotadores;
	}

	public Anotador getAnotadorSeleccionado() {
		return anotadorSeleccionado;
	}

	public void setAnotadorSeleccionado(Anotador anotadorSeleccionado) {
		this.anotadorSeleccionado = anotadorSeleccionado;
	}

	public List<ArbitroJuego> getListaArbitroJuego() {
		return listaArbitroJuego;
	}

	public void setListaArbitroJuego(List<ArbitroJuego> listaArbitroJuego) {
		this.listaArbitroJuego = listaArbitroJuego;
	}

	public List<Arbitro> getListaArbitros() {
		return listaArbitros;
	}

	public void setListaArbitros(List<Arbitro> listaArbitro) {
		this.listaArbitros = listaArbitro;
	}

	public String getEscuelaContraria() {
		return escuelaContraria;
	}

	public void setEscuelaContraria(String escuelaContraria) {
		this.escuelaContraria = escuelaContraria;
	}

	public String getEquipoContrario() {
		return equipoContrario;
	}

	public void setEquipoContrario(String equipoContrario) {
		this.equipoContrario = equipoContrario;
	}
    
	public long getCantidadCompetencias() {
		return cantidadCompetencias;
	}

	public void setCantidadCompetencias(long cantidadCompetencias) {
		this.cantidadCompetencias = cantidadCompetencias;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	
}
