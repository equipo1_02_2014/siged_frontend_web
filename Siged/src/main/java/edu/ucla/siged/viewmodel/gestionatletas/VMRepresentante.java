package edu.ucla.siged.viewmodel.gestionatletas;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestionatleta.AtletaRepresentante;
import edu.ucla.siged.domain.gestionatleta.Parentesco;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.utilidades.EstadoCivil;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstadoCivilI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioParentescoI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAnotadorI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMRepresentante {

	private @WireVariable ServicioEstadoCivilI servicioEstadoCivil;
	List<EstadoCivil> estadoCiviles;
	private EstadoCivil estadoCivilSeleccionado;
	private @WireVariable ServicioParentescoI servicioParentesco;
	List<Parentesco> parentescos;
	private Parentesco parentescoSeleccionado;
	
	private @WireVariable ServicioAtletaI servicioAtleta;
	private @WireVariable ServicioUsuarioI servicioUsuario;
	private @WireVariable ServicioRepresentanteI servicioRepresentante;
	private @WireVariable ServicioTecnicoI servicioTecnico;
	private @WireVariable ServicioAnotadorI servicioAnotador;
	private String cedula;
	private String nombre;
	private String apellido;
	private String telefono;
	private String celular;
	private String direccion;
	private Date fechaNacimiento;
	private String lugarNacimiento;
	private Archivo foto;
	private String email;
	private EstadoCivil estadoCivil; 
	private String lugarTrabajo;
	private String profesion;
	private String observacion;
	private Short estatus;
	 
	
	private AImage imagen;
	private boolean instanciado = false;
	private Window ventanaRepresentante;
	private @WireVariable Representante representante;
	private @WireVariable AtletaRepresentante atletaRepresentante;
	private Parentesco parentesco;
	private Boolean representanteLegal=false;
	private boolean encontrado = false;
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("atletaRepresentante") AtletaRepresentante atletaRepresentanteSeleccionado){
		this.parentesco= new Parentesco();
		this.estadoCivil= new EstadoCivil();
		setEstadoCiviles(servicioEstadoCivil.obtenerListaEstadoCivil());
		setParentescos(servicioParentesco.obtenerListaParentesco());
		Selectors.wireComponents(view, this, false);
		if(atletaRepresentanteSeleccionado!=null){
			this.atletaRepresentante = atletaRepresentanteSeleccionado;
			this.representante = atletaRepresentanteSeleccionado.getRepresentante();
			this.cedula = atletaRepresentanteSeleccionado.getRepresentante().getCedula();
			this.nombre = atletaRepresentanteSeleccionado.getRepresentante().getNombre();
			this.apellido = atletaRepresentanteSeleccionado.getRepresentante().getApellido();
			this.telefono = atletaRepresentanteSeleccionado.getRepresentante().getTelefono();
			this.celular = atletaRepresentanteSeleccionado.getRepresentante().getCelular();
			this.direccion = atletaRepresentanteSeleccionado.getRepresentante().getDireccion();
			this.fechaNacimiento = atletaRepresentanteSeleccionado.getRepresentante().getFechaNacimiento();
			this.lugarNacimiento = atletaRepresentanteSeleccionado.getRepresentante().getLugarNacimiento();
			this.estadoCivil = atletaRepresentanteSeleccionado.getRepresentante().getEstadoCivil();
			this.lugarTrabajo = atletaRepresentanteSeleccionado.getRepresentante().getLugarTrabajo();
			this.profesion = atletaRepresentanteSeleccionado.getRepresentante().getProfesion();
			this.observacion = atletaRepresentanteSeleccionado.getRepresentante().getObservacion();
			this.estatus = atletaRepresentanteSeleccionado.getRepresentante().getEstatus();
			this.parentesco = atletaRepresentanteSeleccionado.getParentesco();
			this.email = atletaRepresentanteSeleccionado.getRepresentante().getEmail();
			this.representanteLegal = atletaRepresentanteSeleccionado.getRepresentanteLegal();
			this.estadoCivilSeleccionado = atletaRepresentanteSeleccionado.getRepresentante().getEstadoCivil();
			this.parentescoSeleccionado = atletaRepresentanteSeleccionado.getParentesco();
			if(atletaRepresentanteSeleccionado.getRepresentante().getFoto() != null){
				this.foto = atletaRepresentanteSeleccionado.getRepresentante().getFoto();
				try {
					imagen = new AImage(atletaRepresentanteSeleccionado.getRepresentante().getFoto().getNombreArchivo(), atletaRepresentanteSeleccionado.getRepresentante().getFoto().getContenido());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			instanciado = true;
		}
	}
	@Command
	@NotifyChange({"cedula","nombre","apellido","telefono","celular","direccion","fechaNacimiento","lugarNacimiento","foto","email","estadoCivil","lugarTrabajo","profesion","observacion","estatus","imagen","parentesco","representanteLegal"})
	public void  cargarRepresentante(@BindingParam("ventanaRepresentante") Window ventanaRepresentante,@SelectorParam("textbox,datebox") LinkedList<InputElement> inputs){
        boolean camposValidos=true;
		
		for(InputElement input:inputs){
			
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
			
		}
		
		/*Validando Campos*/
		if(!camposValidos || parentescoSeleccionado==null || estadoCivilSeleccionado==null){
			
			Messagebox.show("Campos no Validos", "ERROR", Messagebox.OK, Messagebox.ERROR);
			
		}else{
			
			this.ventanaRepresentante = ventanaRepresentante;
			if(!instanciado){
				if(this.encontrado){
					this.atletaRepresentante = new AtletaRepresentante();
					this.atletaRepresentante.setRepresentante(representante);
					this.atletaRepresentante.setParentesco(parentescoSeleccionado);
					this.atletaRepresentante.setRepresentanteLegal(this.representanteLegal);
				}
				else{
					this.parentesco = parentescoSeleccionado;
					this.estadoCivil = estadoCivilSeleccionado;
					representante = new Representante(this.cedula, this.nombre, this.apellido, this.telefono, this.celular, this.direccion, this.fechaNacimiento, this.lugarNacimiento, this.email, this.foto, this.estadoCivil, this.lugarTrabajo, this.profesion, this.observacion, (short) 1);
					atletaRepresentante = new AtletaRepresentante(this.representante, this.parentesco, this.representanteLegal);
				}
			}else{
				
					if(this.parentescoSeleccionado!=null){ 
						this.parentesco=this.parentescoSeleccionado; 
					}
					if(this.estadoCivilSeleccionado!=null){ 
						this.estadoCivil=this.estadoCivilSeleccionado; 
					}
					atletaRepresentante.getRepresentante().setCedula(this.cedula);
					atletaRepresentante.getRepresentante().setNombre(this.nombre);
					atletaRepresentante.getRepresentante().setApellido(this.apellido);
					atletaRepresentante.getRepresentante().setTelefono(this.telefono);
					atletaRepresentante.getRepresentante().setCelular(this.celular);
					atletaRepresentante.getRepresentante().setDireccion(this.direccion);
					atletaRepresentante.getRepresentante().setFechaNacimiento(this.fechaNacimiento);
					atletaRepresentante.getRepresentante().setLugarNacimiento(this.lugarNacimiento);
					atletaRepresentante.getRepresentante().setFoto(this.foto);
					atletaRepresentante.getRepresentante().setEmail(this.email);
					atletaRepresentante.getRepresentante().setLugarTrabajo(this.lugarTrabajo);
					atletaRepresentante.getRepresentante().setProfesion(this.profesion);
					atletaRepresentante.getRepresentante().setObservacion(this.observacion);
					atletaRepresentante.getRepresentante().setEstatus(this.estatus);
					atletaRepresentante.getRepresentante().setEstadoCivil(this.estadoCivil);
					atletaRepresentante.setParentesco(this.parentesco);
					atletaRepresentante.setRepresentante(this.atletaRepresentante.getRepresentante());
					atletaRepresentante.setRepresentanteLegal(this.representanteLegal);
				
			}
			limpiar();
			Messagebox.show("El Representante se ha guardado, desea agregar otro", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
						
						public void onEvent(Event event) throws Exception {
							// TODO Auto-generated method stub
							if(Messagebox.ON_NO.equals(event.getName())){
								cerrarVentanaReposo();
							}
						}
					});
		}
	}
	public void cerrarVentanaReposo(){
		ventanaRepresentante.detach();
	}
	
	@Command
	@NotifyChange({"cedula","nombre","apellido","telefono","celular","direccion","fechaNacimiento","lugarNacimiento","foto","email","estadoCivil","lugarTrabajo","profesion","observacion","estatus","imagen","parentesco","representanteLegal"})
	public void limpiar(){
		this.cedula = "";
		this.nombre = "";
		this.apellido = "";
		this.telefono = "";
		this.celular = "";
		this.direccion = "";
		this.fechaNacimiento = null;
		this.lugarNacimiento = "";
		this.foto = null;
		this.email = "";
		this.estadoCivil = null; 
		this.lugarTrabajo = "";
		this.profesion = "";
		this.observacion = "";
		this.estatus = null;
		this.imagen = null;
		this.estadoCivilSeleccionado = null;
		this.parentescoSeleccionado = null;
		this.instanciado = false;
		this.parentesco = new Parentesco();
		this.estadoCivil = new EstadoCivil();
		this.representanteLegal = false;
		encontrado = false;
	}
	
	@Command
	@NotifyChange({"foto","imagen"})
	public void cargarImagen(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media =event.getMedia();
		if(media != null){
			if(media instanceof org.zkoss.image.Image){
				this.foto = new Archivo();
				this.foto.setNombreArchivo(media.getName());
				this.foto.setTipo(media.getContentType());
				this.foto.setContenido(media.getByteData());
				this.imagen = (AImage) media;
			}
		}
	}
	public List<EstadoCivil> getEstadoCiviles() {
		return estadoCiviles;
	}
	public void setEstadoCiviles(List<EstadoCivil> estadoCiviles) {
		this.estadoCiviles = estadoCiviles;
	}
	public List<Parentesco> getParentescos() {
		return parentescos;
	}
	public void setParentescos(List<Parentesco> parentescos) {
		this.parentescos = parentescos;
	}
	public Parentesco getParentescoSeleccionado() {
		return parentescoSeleccionado;
	}
	public void setParentescoSeleccionado(Parentesco parentescoSeleccionado) {
		this.parentescoSeleccionado = parentescoSeleccionado;
	}

	public EstadoCivil getEstadoCivilSeleccionado() {
		return estadoCivilSeleccionado;
	}

	public void setEstadoCivilSeleccionado(EstadoCivil estadoCivilSeleccionado) {
		this.estadoCivilSeleccionado = estadoCivilSeleccionado;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getLugarTrabajo() {
		return lugarTrabajo;
	}

	public void setLugarTrabajo(String lugarTrabajo) {
		this.lugarTrabajo = lugarTrabajo;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public AImage getImagen() {
		return imagen;
	}

	public void setImagen(AImage imagen) {
		this.imagen = imagen;
	}

	public Archivo getFoto() {
		return foto;
	}
	public void setFoto(Archivo foto) {
		this.foto = foto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public AtletaRepresentante getAtletaRepresentante() {
		return atletaRepresentante;
	}
	public void setAtletaRepresentante(AtletaRepresentante atletaRepresentante) {
		this.atletaRepresentante = atletaRepresentante;
	}
	
	public Boolean getRepresentanteLegal() {
		return representanteLegal;
	}
	public void setRepresentanteLegal(Boolean representanteLegal) {
		this.representanteLegal = representanteLegal;
	}
	public Parentesco getParentesco() {
		return parentesco;
	}
	public void setParentesco(Parentesco parentesco) {
		this.parentesco = parentesco;
	}
	
	@Command
	@NotifyChange({"cedula","nombre","apellido","telefono","celular","direccion","fechaNacimiento","lugarNacimiento","foto","email","estadoCivil","lugarTrabajo","profesion","observacion","estatus","imagen","parentesco","representanteLegal"})
	public void validarCedula(){
//		validar que distinto de null y vacio
		
		if(!instanciado){
			
			representante = servicioRepresentante.buscarPorCedula(this.cedula);
			if(representante!=null){
				Messagebox.show("Representante Encontrado");
				representante.setId(representante.getId());
				this.cedula = representante.getCedula();
				this.nombre = representante.getNombre();
				this.apellido = representante.getApellido();
				this.telefono = representante.getTelefono();
				this.celular = representante.getCelular();
				this.direccion = representante.getDireccion();
				this.fechaNacimiento = representante.getFechaNacimiento();
				this.lugarNacimiento = representante.getLugarNacimiento();
				this.estadoCivil = representante.getEstadoCivil();
				this.lugarTrabajo = representante.getLugarTrabajo();
				this.profesion = representante.getProfesion();
				this.observacion = representante.getObservacion();
				this.estatus = representante.getEstatus();
				this.email = representante.getEmail();
				this.estadoCivilSeleccionado = representante.getEstadoCivil();
				if(representante.getFoto() != null){
					this.foto = representante.getFoto();
					try {
						imagen = new AImage(representante.getFoto().getNombreArchivo(), representante.getFoto().getContenido());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				this.encontrado = true;
			}
		}
		
	}
	
	
	
	
}
