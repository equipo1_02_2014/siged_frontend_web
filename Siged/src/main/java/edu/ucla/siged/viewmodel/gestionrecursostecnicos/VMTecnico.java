package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import org.zkoss.bind.annotation.BindingParam;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.utilidades.EstadoCivil;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;

public class VMTecnico {
	
	@WireVariable
	ServicioTecnicoI servicioTecnico;
	@WireVariable
	Tecnico tecnico;
	@WireVariable
	EstadoCivil estadoCivilSeleccionado;
	List<Integer> listaEstatus;
	List<EstadoCivil> listaEstadosCivil;
	Short estatusSeleccionado;
	Window window;
	private AImage imagenT;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////
	
	public Tecnico getTecnico() {
		return tecnico;
	}
	
	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Integer> getListaEstatus() {
		return listaEstatus;
	}
	
	public List<EstadoCivil> getListaEstadosCivil() {
		return listaEstadosCivil;
	}

	public void setListaEstadosCivil(List<EstadoCivil> listaEstadosCivil) {
		this.listaEstadosCivil = listaEstadosCivil;
	}
	
	public EstadoCivil getEstadoCivilSeleccionado() {
		return estadoCivilSeleccionado;
	}

	public void setEstadoCivilSeleccionado(EstadoCivil estadoCivilSeleccionado) {
		this.estadoCivilSeleccionado = estadoCivilSeleccionado;
	}
	
	public AImage getImagenT() {
		return imagenT;
	}
	
	public void setImagenT(AImage imagenT) {
		this.imagenT = imagenT;
	}

	/////////////////////////////////////// INIT////////////////////////////////////////////////////////
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("tecnico") Tecnico tecnicoSeleccionado) {
            listaEstatus= new ArrayList<Integer>();
            listaEstatus.add(0);            
            listaEstatus.add(1);
            
            listaEstadosCivil = servicioTecnico.buscarEstadoCivilTecnico();
            
         
            Selectors.wireComponents(view, this, false);
        if (tecnicoSeleccionado!=null){
            this.tecnico = tecnicoSeleccionado;
            this.estatusSeleccionado= this.tecnico.getEstatus();
            if(tecnicoSeleccionado.getFoto().getTamano() > 0){
				try{
					imagenT = new AImage(tecnico.getFoto().getNombreArchivo(), tecnico.getFoto().getContenido());
					
				}catch(IOException e){
				}
			}
            
        }else{
        	try{
        		tecnico = new Tecnico();
        		tecnico.setEstatus(Short.parseShort("1"));
        		this.estatusSeleccionado= Short.parseShort("1");
        		this.estadoCivilSeleccionado= getListaEstadosCivil().get(0);
        	}catch(Exception e){
        		
        	}
        }
    }
	////////////////////////////////////////////GUARDAR/////////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"tecnico","imagenT"})   
	public void guardar(){
		if( estatusSeleccionado != null ){
			tecnico.setEstatus(estatusSeleccionado); 
			}
		
		if( estadoCivilSeleccionado != null ){
			tecnico.setEstadoCivil(estadoCivilSeleccionado);
			}
		
		if (tecnico.getNombre()==null || tecnico.getApellido()==null ||tecnico.getCedula()==null || tecnico.getCelular()==null
				|| tecnico.getDireccion()==null || tecnico.getEmail()==null || tecnico.getFechaAdmision()==null ||
				tecnico.getFechaNacimiento()==null || tecnico.getFoto()==null || tecnico.getLugarNacimiento()==null ||
				tecnico.getRif()==null || tecnico.getTelefono()==null)
                 {
			Messagebox.show("Por favor introduzca todos los datos","", Messagebox.OK, Messagebox.INFORMATION);}
		else{    
		   tecnico.setEstatus(Short.parseShort("1"));
		servicioTecnico.guardar(tecnico);
		Messagebox.show(
                "Tecnico guardado exitosamente","", Messagebox.OK, Messagebox.INFORMATION
            );
		Window myWindow = (Window) Path.getComponent("/vistaTecnico");
		myWindow.detach();
	}
	}
	
	////////////////////////////////////////////////LIMPIAR///////////////////////////////////////////////
	@Command
	@NotifyChange({"tecnico","imagenT"})
	public void limpiar(){
	tecnico.setCedula(null);
	tecnico.setNombre("");
	tecnico.setApellido("");
	tecnico.setTelefono(null);
	tecnico.setCelular(null);
	tecnico.setDireccion("");
	tecnico.setRif("");
	tecnico.setFechaNacimiento(null);
	tecnico.setLugarNacimiento("");
	tecnico.setFechaAdmision(null);
	tecnico.setEstadoCivil(null);
	tecnico.setFoto(null);
	tecnico.setEstatus(null);
	tecnico.setEmail("");
	imagenT = null;
	}
	
	/////////////////////////////////////////////ABRIR FORMULARIO//////////////////////////////////////////////
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("tecnicoSeleccionado") Tecnico tecnicoSeleccionado ) {
		Window window = (Window)Executions.createComponents("tecnico.zul", null, null);
		this.tecnico = tecnicoSeleccionado;
		window.doModal();
	}
	
	///////////////////////////////////////////VER ESTATUS////////////////////////////////////////////////////////
	
	@Command
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
	///////////////////////////////// VER VALOR////////////////////////////////////////////////////////////////////
	
	@Command
	public void verValor(){
		Messagebox.show(estatusSeleccionado.toString());
	}
	
	//////////////////////////////////////SUBIR IMAGEN////////////////////////////////////////////////////////////////
	@Command
	@NotifyChange({"tecnico","imagenT"})
	public void subirImagen(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media= event.getMedia();
		if(media != null){
			if(media instanceof org.zkoss.image.Image){
				Archivo foto = new Archivo();
				foto.setNombreArchivo(media.getName());
				foto.setTipo(media.getContentType());
				foto.setContenido(media.getByteData());
				tecnico.setFoto(foto);
				imagenT = (AImage) media;
			}else{
			}
		}
	}
}