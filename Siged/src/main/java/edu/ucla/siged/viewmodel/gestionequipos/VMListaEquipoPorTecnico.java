package edu.ucla.siged.viewmodel.gestionequipos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;

public class VMListaEquipoPorTecnico {

	private @WireVariable ServicioEquipoI servicioEquipo;
	private @WireVariable ServicioTecnicoI servicioTecnico;
	private @WireVariable Tecnico tecnico;
	private List<Equipo> equipos;
	private int paginaActual;
	private int tamanoPagina;
	private long registrosTotales;
	private Usuario usuario;
	private boolean listaVisible = false;
	private boolean mensajeVisible = true;
	private String titulo = "Lista de Equipos del Tecnico ";

	@Init
	public void init() {
		this.paginaActual = 0;
		this.tamanoPagina = 10;
		this.registrosTotales = 0;
		Session session = Sessions.getCurrent();
		Usuario usuario = (Usuario) session.getAttribute("userCredential");
		if (usuario != null) {
			this.tecnico = servicioTecnico.buscarTecnicoPorCedula(usuario
					.getCedula());
			if (this.tecnico != null) {
				this.listaVisible = true;
				this.mensajeVisible = false;
				this.equipos = servicioEquipo.buscarEquiposPorTecnico(
						this.tecnico.getId(), this.paginaActual,
						this.tamanoPagina);
				this.registrosTotales = (long) servicioEquipo
						.totalEquipoPorTecnico(this.tecnico.getId());
				titulo += tecnico.getNombre() + " " + tecnico.getApellido();
			}
		}
	}

	@Command
	@NotifyChange({ "equipos", "paginaActual" })
	public void paginarEquipos() {
		this.equipos = servicioEquipo.buscarEquiposPorTecnico(
				this.tecnico.getId(), this.paginaActual, tamanoPagina);
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}

	public Tecnico getTecnico() {
		return tecnico;
	}

	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isListaVisible() {
		return listaVisible;
	}

	public void setListaVisible(boolean listaVisible) {
		this.listaVisible = listaVisible;
	}

	public boolean isMensajeVisible() {
		return mensajeVisible;
	}

	public void setMensajeVisible(boolean mensajeVisible) {
		this.mensajeVisible = mensajeVisible;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}
	

}
