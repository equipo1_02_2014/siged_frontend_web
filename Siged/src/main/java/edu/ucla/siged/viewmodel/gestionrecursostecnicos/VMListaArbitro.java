/** 
 * 	VMListaArbitro
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioArbitroI;

public class VMListaArbitro {

	@WireVariable
	ServicioArbitroI servicioArbitro;
	@WireVariable
	Arbitro arbitro;
	List<Arbitro> listaArbitros;
	List<Arbitro> arbitrosSeleccionados;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String cedula=""; 
	String nombre="";
	Date fechaInicio;
	String apellido="";
	String telefono="";
	String celular="";
	Short estatusCombo=0;
	boolean estatusFiltro;
	
	public Arbitro getArbitro() {
		return arbitro;
	}	
	
	public void setArbitro(Arbitro arbitro) {
		this.arbitro = arbitro;
	}	
	
	public List<Arbitro> getArbitrosSeleccionados() {
		return arbitrosSeleccionados;
	}
	
	public void setArbitrosSeleccionados(List<Arbitro> arbitrosSeleccionados) {
		this.arbitrosSeleccionados = arbitrosSeleccionados;
	}
	
	public List<Arbitro> getListaArbitros() {
		return listaArbitros;
	}
	
	public void setListaArbitros(List<Arbitro> listaArbitros) {
		this.listaArbitros = listaArbitros;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioArbitro.TAMANO_PAGINA;
		return tamanoPagina;
	}
		
	public long getRegistrosTotales() {
		return registrosTotales;
	}
		
	public String getCedula() {
		return cedula;
	}
	
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getCelular() {
		return celular;
	}
	
	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Short getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}
	
	@Init
	public void init(){			
		this.listaArbitros = servicioArbitro.buscarTodos(0).getContent();
		this.registrosTotales= servicioArbitro.totalArbitros();
		this.paginaActual=0;
	}
	
	@Command
	public void agregarArbitro() {
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/arbitro.zul", null, null);
		window.doModal();
	}	
	
	@Command
	@NotifyChange({"listaArbitros","registrosTotales"})
	public void eliminarArbitro(@BindingParam("arbitroSeleccionado") final Arbitro arbitroSeleccionado){
		if(!servicioArbitro.existeEnJuego(arbitroSeleccionado)){
			Messagebox.show("�Est� seguro que desea eliminar este registro?","Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>(){
						public void onEvent(Event event) throws Exception{
							if(Messagebox.ON_YES.equals(event.getName())){
								servicioArbitro.eliminar(arbitroSeleccionado);
								paginar();
								Messagebox.show("El registro se ha eliminado exitosamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
								actualizarLista();
							}
						}
			});
		}else {
			Messagebox.show("No puede eliminar el Arbitro porque forma parte de un juego","Error", Messagebox.OK, Messagebox.ERROR);
		}
		//el siguiente paso se realiza cuando existe un �nico elemento en la �ltima p�gina y se ha eliminado.
		if ((listaArbitros.size()==0) && (registrosTotales>0)){
			this.listaArbitros = servicioArbitro.buscarTodos(this.paginaActual-1).getContent();							
		}
	}
		
	@Command
	@NotifyChange({"listaArbitros","registrosTotales"})
	public void eliminarArbitros(){
		
		int cuantos= servicioArbitro.eliminarVarios(arbitrosSeleccionados);
		if (cuantos>0){
			Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							if(Messagebox.ON_YES.equals(event.getName())){
								Messagebox.show("Los registros se han eliminado exitosamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
								arbitrosSeleccionados.clear();
							}
						}
					});		
		}
		else
			Messagebox.show("S�lo se eliminan Arbitros no asignados a juego.", "Error", Messagebox.OK, Messagebox.ERROR);
		paginar();
		if ((listaArbitros.size()==0) && (registrosTotales>0)){
			this.listaArbitros = servicioArbitro.buscarTodos(this.paginaActual-1).getContent();							
		}									
	}	
		
	@Command
	@NotifyChange({"arbitro"})
	public void editarArbitro(@BindingParam("arbitroSeleccionado") Arbitro arbitroSeleccionado ) {
		
		this.arbitro=arbitroSeleccionado;
		this.tipoOperacion=2;
		//esta es la manera de pasar a la otra vista el arbitro seleccionado
		Map<String, Arbitro> mapa = new HashMap<String, Arbitro>();
        mapa.put("arbitro", arbitroSeleccionado);		
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/arbitro.zul", null, mapa);
		window.doModal();
	}
	
	@GlobalCommand
	@NotifyChange({"listaArbitros","registrosTotales","paginaActual","arbitrosSeleccionados"})
	public void paginar(){
		
		if (estatusFiltro==false){
			 
			this.listaArbitros = servicioArbitro.buscarTodos(this.paginaActual).getContent();
			this.registrosTotales=servicioArbitro.totalArbitros();				
		}else{		
			ejecutarFiltro();
		}        			
	}
	
	@GlobalCommand
	@NotifyChange({"listaArbitros","registrosTotales","paginaActual"})
	public void actualizarLista(){
		
		this.registrosTotales=servicioArbitro.totalArbitros();
		if (registrosTotales>0) {
			if (this.tipoOperacion==1){			   
			   int ultimaPagina=0;
			   if ((this.registrosTotales)%this.tamanoPagina==0)
				  ultimaPagina= ((int) (this.registrosTotales)/this.tamanoPagina)-1;
			   else		
				  ultimaPagina =((int) (this.registrosTotales)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
			} 
			else if(this.tipoOperacion==2){		
			}
			paginar();
		}
	}
	
	@Command
	@NotifyChange({"listaArbitros","arbitro"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		switch (estatusEnLista) {
			case 0:
				est="Inactivo";
				break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
	}

	@Command
	@NotifyChange({"listaArbitros","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado		
		if (fechaInicio == null)
			jpql = " cedula like '%"+cedula+"%' and nombre like '%"+nombre+"%' and apellido like '%"+apellido+"%' and telefono like '%"+telefono+"%' and celular like '%"+celular+"%'";
		else
		   jpql = " cedula like '%"+cedula+"%' and nombre like '%"+nombre+"%' and apellido like '%"+apellido+"%' and telefono like '%"+telefono+"%' and celular like '%"+celular+"%'and fechaInicio = '"+fechaInicio+"'";
		
		this.listaArbitros=servicioArbitro.buscarFiltrado(jpql,this.paginaActual);  //el filtro comienza a mostrar desde la 1ra pagina
		this.registrosTotales=servicioArbitro.totalArbitrosFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"listaArbitros","registrosTotales","paginaActual","arbitrosSeleccionados",
				   "cedula", "nombre","fechaInicio","apellido","telefono","celular","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		cedula=""; // las variables siguientes son del filtro
		nombre="";
		fechaInicio=null;
		apellido="";
		telefono="";
		celular="";
	}
	
	@GlobalCommand("asignarTipoOperacionGuardarArbitro")
    @NotifyChange({"tipoOperacion"})
    public void asignarTipoOperacionGuardar(){
        this.tipoOperacion= 1;
    }
}