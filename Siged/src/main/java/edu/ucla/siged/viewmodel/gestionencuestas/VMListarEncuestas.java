package edu.ucla.siged.viewmodel.gestionencuestas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.util.Date;

import org.hibernate.Query;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioEncuestaI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;


public class VMListarEncuestas {
	@WireVariable
	private ServicioEncuestaI servicioEncuesta;
	
	
	
	@WireVariable
	Encuesta encuesta;

	List<Encuesta> listaEncuestas;
	Encuesta encuestaSeleccionada;
	
	List<Encuesta> encuestasSeleccionadas;
	
	private Window window;
	short estatusCombo=0;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	boolean estatusFiltro;
	String fechaInicio;
	String fechaFin;
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	


	String nombre;

	public VMListarEncuestas() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
	public void init() {
		
		nombre="";
		fechaInicio="";
		fechaFin="";
		estatusCombo=0;
	/*	List <Rol> roles= new ArrayList<Rol>();
		Session sess = Sessions.getCurrent();
    	roles =  (List<Rol>) sess.getAttribute("roles");
    	Usuario usuario = (Usuario)sess.getAttribute("userCredential");
  	   
		this.listaEncuestas=servicioEncuesta.listarEncuestasUsuarioParaVotar(roles, usuario.getId());*/
	
		this.listaEncuestas=servicioEncuesta.buscarTodos(0);
		
		this.registrosTotales= servicioEncuesta.totalEncuestas();
	}

	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina=servicioEncuesta.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public boolean isEstatusFiltro() {
		return estatusFiltro;
	}

	public void setEstatusFiltro(boolean estatusFiltro) {
		this.estatusFiltro = estatusFiltro;
	}	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Encuesta getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

	public List<Encuesta> getListaEncuestas() {
		return listaEncuestas;
	}

	public void setListaEncuestas(List<Encuesta> listaEncuestas) {
		this.listaEncuestas = listaEncuestas;
	}

	public Encuesta getEncuestaSeleccionada() {
		return encuestaSeleccionada;
	}

	public void setEncuestaSeleccionada(Encuesta encuestaSeleccionada) {
		this.encuestaSeleccionada = encuestaSeleccionada;
	}

	public List<Encuesta> getEncuestasSeleccionadas() {
		return encuestasSeleccionadas;
	}

	public void setEncuestasSeleccionadas(List<Encuesta> encuestasSeleccionadas) {
		this.encuestasSeleccionadas = encuestasSeleccionadas;
	}

	@Command
	@NotifyChange({"listaEncuestas"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		System.out.println("estatus:"+estatusEnLista.toString());
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	@NotifyChange("listaEncuestas")
	public void agregarEncuesta(){
		/*Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
		Center center = borderl.getCenter();
		center.getChildren().clear();*/
		window = (Window)Executions.createComponents("/vistas/gestionencuesta/encuesta.zul",null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange("listaEncuestas")
	public void editarEncuesta(@BindingParam("encuestaSeleccionada") Encuesta encuestaSeleccionada){
		Map<String,Encuesta> mapRol = new HashMap<String, Encuesta>();
		mapRol.put("encuestaSeleccionada", encuestaSeleccionada);
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestionencuesta/encuesta.zul", null, mapRol);
		window.doModal();	
	
	
	}
	
	@Command
	@NotifyChange("listaEncuestas")
	public void eliminarEncuesta(@BindingParam("encuestaSeleccionada") Encuesta encuestaSeleccionada){
		try{
			servicioEncuesta.eliminar(encuestaSeleccionada);
			actualizarLista(encuestaSeleccionada);
		}
		catch(Exception e){
			Messagebox.show("No se ha podido eliminar la encuesta!", "Error", Messagebox.OK, Messagebox.ERROR);
			
		}
		
		//cambiarEstatusRolesUsuario();
	}
	
	@Command
	@NotifyChange("listaEncuestas")
	public void eliminarEncuestas(){
		System.out.println("Entro en eliminar 1");
		try {
			servicioEncuesta.eliminarVarios(encuestasSeleccionadas);
			actualizarLista(encuestasSeleccionadas.get(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//cambiarEstatusRolesUsuario();
		
	}
	
	@GlobalCommand
	@NotifyChange({"listaEncuestas","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevaEncuesta") Encuesta nuevaEncuesta){
		
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} 
		paginar();
	}
	
	@GlobalCommand
	@NotifyChange({"listaEncuestas","registrosTotales","paginaActual","rolesSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaEncuestas = servicioEncuesta.buscarTodos(this.paginaActual);
		this.registrosTotales=servicioEncuesta.totalEncuestas();
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@Command
	@NotifyChange({"listaEncuestas","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		System.out.println("ESTATUS SELECCIONADO"+estatusCombo);
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		List<String> condiciones = new ArrayList<String>();
		
		if(!nombre.trim().isEmpty())	
		condiciones.add(" nombre like '%"+nombre+"%' ");
		if(!fechaInicio.trim().isEmpty())
		condiciones.add(" to_char(fechaInicio, 'DD/MM/YYYY') like'%"+fechaInicio+"%' ");
		if(!fechaFin.trim().isEmpty())
			condiciones.add(" to_char(fechaFin, 'DD/MM/YYYY') like'%"+fechaFin+"%' ");
		if(!filtroEstatus.isEmpty())
		condiciones.add(" estatus in "+filtroEstatus);
		
		
		jpql="";	
		for(int i=0; i<condiciones.size(); i++){
			jpql+= condiciones.get(i);
			if(i+1<condiciones.size())
			jpql+= " and ";
		}
		
		System.out.println("----------------------------------"+jpql.toString());
		this.listaEncuestas=servicioEncuesta.buscarFiltrado(jpql,this.paginaActual);
		
		this.registrosTotales=servicioEncuesta.totalEncuestasFiltradas(jpql);
	}
	
	@Command
	@NotifyChange({"listaEncuestas","registrosTotales","paginaActual","rolesSeleccionados",
				   "nombre","fechaInicio", "estatusCombo", "fechaFin"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		nombre="";
		fechaInicio="";
		fechaFin="";
		estatusCombo=0;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		if(fecha!=null){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
		}
		return "-";
	}
	

	
	
}