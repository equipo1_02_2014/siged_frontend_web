/** 
 * 	VMActividad
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.util.LinkedList;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.servicio.impl.gestiondeportiva.ServicioActividad;

public class VMActividad {
	@WireVariable
	ServicioActividad servicioActividad;
	@WireVariable
	Actividad actividad;
	private Window ventanaActividad;
	Short estatusSeleccionado;
	short tipoOperacion;
	
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public ServicioActividad getServicioActividad() {
		return servicioActividad;
	}

	public void setServicioActividad(ServicioActividad servicioActividad) {
		this.servicioActividad = servicioActividad;
	}

	public Actividad getActividad() {
		return actividad;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}
	
	public Window getVentanaActividad() {
		return ventanaActividad;
	}

	public void setVentanaActividad(Window ventanaActividad) {
		this.ventanaActividad = ventanaActividad;
	}

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("actividad") Actividad actividadSeleccionada){
		if(actividadSeleccionada!=null){
			this.actividad = actividadSeleccionada;
			this.tipoOperacion=2;
            this.estatusSeleccionado= actividad.getEstatus();
		}
		else{
			this.tipoOperacion=1;
			actividad = new Actividad();
		}
	}
	
	@Command
	 @NotifyChange({"actividad","limpiar","estatusSeleccionado","actualizarLista"})
	 public void guardarActividad(@SelectorParam(".actividad_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaActividad") Window ventanaActividad){
		this.ventanaActividad = ventanaActividad;
		if (this.estatusSeleccionado!=null)
			actividad.setEstatus(estatusSeleccionado);
		boolean camposValidos=true;
		
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
			break;
			}
		}
		
		if(!camposValidos){
			Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
			
		}
		else{
			if(this.tipoOperacion==1)
			{
				actividad.setEstatus(Short.parseShort("1"));
				servicioActividad.guardar(actividad);
				Messagebox.show("El registro se ha guardado exitosamente","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				Messagebox.show("�Desea agregar otro registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
      					new EventListener<Event>() {
      				
      						public void onEvent(Event event) throws Exception {
      							if(Messagebox.ON_NO.equals(event.getName())){
      								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
      								cerrarVentanaActividad();
      							}
      							else BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
      						}
      					});
				    limpiar();
			}
			
			if(this.tipoOperacion==2){
				actividad.setEstatus(Short.parseShort("1"));	
				servicioActividad.guardar(actividad);
				Messagebox.show("Los cambios se han guardado exitosamente","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				cerrarVentanaActividad();
				}
		    }
	 }
	
	public void cerrarVentanaActividad(){
		ventanaActividad.detach();
	}

	@Command
	@NotifyChange({"actividad"})
	public void limpiar(){
		actividad.setId(null);
		actividad.setDescripcion("");	
	}		
}