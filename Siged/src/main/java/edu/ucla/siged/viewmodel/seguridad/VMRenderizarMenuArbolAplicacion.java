package edu.ucla.siged.viewmodel.seguridad;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.UtilidadArbol;
import edu.ucla.siged.servicio.impl.seguridad.ServicioFuncionalidad;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;





public class VMRenderizarMenuArbolAplicacion implements TreeitemRenderer<VMNodoMenuArbol> {
	//-----------------Variables Objeto---------------------
	Window w = null;
	UtilidadArbol utilidadArbol = new UtilidadArbol();
	

	/** Metodo render permite a ciertos nodos del arbol renderizar una pantalla en especifico.
	 * @param articuloArbol, nodoArbol, indice 
	 * @return ninguno.
	 * @throws No dispara ninguna excepci�n. 
	 */
	
	//@Override
	public void render(final Treeitem articuloArbol, VMNodoMenuArbol nodoArbol,
			int indice) throws Exception {
		VMNodoMenuArbol ctn = nodoArbol;
		Funcionalidad nodo = (Funcionalidad) ctn.getData();

		Treerow filaArbol = new Treerow();
		filaArbol.setParent(articuloArbol);
		articuloArbol.setValue(ctn);
		Hlayout hl = new Hlayout();
		Treecell celdaArbol = new Treecell();
		celdaArbol.setLabel(nodo.getNombre()); // 1
		celdaArbol.appendChild(hl);
		/*if (nodo.isHijo()==1) {
			celdaArbol.setImage("/imagenes/iconos/funcion-tree.png");
		}*/
		filaArbol.appendChild(celdaArbol);
		filaArbol.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
			//@Override
			public void onEvent(Event event) throws Exception {
				VMNodoMenuArbol valorNodoClickeado = (VMNodoMenuArbol) ((Treeitem) event
						.getTarget().getParent()).getValue();

				if (valorNodoClickeado.getData().getVinculo()!= null && valorNodoClickeado.getData().getVinculo().trim()!=""
						&& !valorNodoClickeado.getData().getVinculo().trim().isEmpty()) {
					if (w != null) {
						w.detach();
					}
					utilidadArbol.onClickMenu(valorNodoClickeado.getData()
							.getVinculo(), valorNodoClickeado.getData()
							.getVinculo());
				} else {
					if (articuloArbol.isOpen()) {
						articuloArbol.setOpen(false);
					} else {
						articuloArbol.setOpen(true);
					}
				}
			}
		});
	}
} //fin VMRenderizarMenuArbolAplicacion