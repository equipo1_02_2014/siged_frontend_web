package edu.ucla.siged.viewmodel.seguridad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import edu.ucla.siged.domain.seguridad.ControlAcceso;
import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioFuncionalidad;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRolUsuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioUsuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioControlAccesoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;


@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMMenuAplicacion{
		
	
	@WireVariable 
	private ServicioFuncionalidadI servicioFuncionalidad;
	@WireVariable
	private  ServicioRolI servicioRol;
	@WireVariable 
	private	ServicioUsuario servicioUsuario;
	@WireVariable
	ServicioControlAccesoI servicioControlAcceso; // Instancia la interface servicio	
	//---------Variables de control------------------------
	private String nombreUsuario;
	//-----------------Variables Objeto--------------------
	private VMModeloArbolAvanzado modeloMenuArbol;
	private static VMNodoMenuArbol  raizPortalInicial;
	private AImage imagen;
	private VMRenderizarMenuArbolAplicacion renderizarPortalAplicacion=new VMRenderizarMenuArbolAplicacion();
	
	
	// M�todos Set y Get 
	public VMRenderizarMenuArbolAplicacion getRendererPortalAplicacion() {
		return renderizarPortalAplicacion;
	}

	public void setRendererPortalAplicacion(
			VMRenderizarMenuArbolAplicacion rendererPortalAplicacion) {
		this.renderizarPortalAplicacion = rendererPortalAplicacion;
	}
	
	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public AImage getImagen() {
		return imagen;
	}

	public void setImagen(AImage imagen) {
		this.imagen = imagen;
	}

    public VMModeloArbolAvanzado getModeloMenuArbol() {
		return modeloMenuArbol;
	}

	public void setModeloMenuArbol(VMModeloArbolAvanzado modeloMenuArbol) {
		this.modeloMenuArbol = modeloMenuArbol;
	}
	public static VMNodoMenuArbol getRaizPortalInicial() {
		return raizPortalInicial;
	}

	public static void setRaizPortalInicial(VMNodoMenuArbol raizPortalInicial) {
		VMMenuAplicacion.raizPortalInicial = raizPortalInicial;
	}
	//Fin M�todos Set y Get
	
	/**
	* Init. C�digo de inicializaci�n del arbol.
	* @param @ContextParam(ContextType.COMPONENT) Component windowindex,
	* @ContextParam(ContextType.VIEW) Component view
	* @return Objetos inicializados, el command 
	* indica a las variables el cambio que se har� en el objeto.
	* @throws No dispara ninguna excepci�n.
	*
	*/
	
	@AfterCompose
	@Command
	@GlobalCommand
	@NotifyChange({"modeloMenuArbol"})
	public void Init(@ContextParam(ContextType.COMPONENT) Component windowindex,@ContextParam(ContextType.VIEW) Component view) {
		raizPortalInicial = new VMNodoMenuArbol(null,null);	
		VMNodoMenuArbol aux = null;
		Session sess = Sessions.getCurrent();
 	    Usuario usuario = (Usuario)sess.getAttribute("userCredential");
		List<RolUsuario> rolesUsuario = new ArrayList<RolUsuario>(servicioUsuario.getRolesActivosUsuario(usuario));
		System.out.println("USER"+usuario.getId()+":"+usuario.getNombreUsuario());
		List<Funcionalidad> nodosOrdenados = new ArrayList<Funcionalidad>();
		List<ControlAcceso> funcionalidadesRol=new ArrayList<ControlAcceso>();
		List<Rol> rolesSesion= new ArrayList<Rol>();
		Funcionalidad ca=new Funcionalidad();
		for(RolUsuario rolUsuario : rolesUsuario){
			System.out.println("ROLUsuario "+rolUsuario.getId()+":"+rolUsuario.getRol().getNombre());
			rolesSesion.add(rolUsuario.getRol());
			funcionalidadesRol=servicioControlAcceso.getFuncionalidadesRol(rolUsuario.getRol());
			for (ControlAcceso controlAcceso : funcionalidadesRol) {
				ca=controlAcceso.getFuncionalidad();
				//ca.setHijo(servicioFuncionalidad.esHijo(ca));
				System.out.println("Funcionalidad ROl : "+controlAcceso.getId()+": Funcionalidad: "+ca.getNombre()+": id "+ca.getId());
				nodosOrdenados.add(ca);
			}	
		}	
		
		//se agregan los roles al objeto session
		sess.setAttribute("roles", rolesSesion);
		
		Collections.sort(nodosOrdenados, new Funcionalidad());
		for (Funcionalidad func : nodosOrdenados) {
			aux = new VMNodoMenuArbol(func);
			VMNodoMenuArbol ctreenodo = this.cargarPadre(aux);
			Integer j = raizPortalInicial.getChildCount();
			System.out.println("aux="+func.getNombre()+"/id:"+func.getId()+"/Numero de hijos de la raiz"+j);
			if (!(j.compareTo(0) == 0)) {
				System.out.println("Cargando Nodos del padre de :"+func.getNombre()+"/el padre es........"+ctreenodo.getData().getNombre()+"idPadre="+func.getIdPadre());
				this.cargarNodos(ctreenodo, raizPortalInicial);
			} else {
				System.out.println("el nodo:"+func.getNombre()+"/no tiene hijos");
				
				raizPortalInicial.add(ctreenodo);
			}
		}		
		modeloMenuArbol = new VMModeloArbolAvanzado(raizPortalInicial);
	}

	/**
	* Carga recursiva del nodo padre dado un objeto nodo en particular
	* @param @ContextParam(ContextType.COMPONENT) Component windowindex,
	* @ContextParam(ContextType.VIEW) Component view
	* @return Objeto VMNodoMenuArbol.
	* @throws No dispara ninguna excepci�n.
	*
	*/
	
	public VMNodoMenuArbol cargarPadre(VMNodoMenuArbol nodo) {
		VMNodoMenuArbol padre=null;
			if(nodo.getData().getIdPadre()!=0){
				Funcionalidad npadre=servicioFuncionalidad.buscarById(nodo.getData().getIdPadre());
				padre=new VMNodoMenuArbol(npadre,null);
				padre.add(nodo);
				nodo=cargarPadre(padre);		
			}
			return nodo;
	}

	/**
	* Carga recursiva de los nodos asociados a un nodo raiz en particular
	* @param nodo, raiz
	* @return No devuelve ningun valor.
	* @throws No dispara ninguna excepci�n.
	*
	*/
	
	private void cargarNodos(VMNodoMenuArbol nodo,VMNodoMenuArbol raiz) { 
		System.out.println("+Entrooo en cargar nodos. / raiz:");
		boolean encontro=false;
		for(int j=0;j< raiz.getChildCount();j++){
			System.out.println("---Buscando hijosss. / Numero de hijos="+ nodo.getChildCount()+"/ hijo:"+raiz.getChildAt(j).getData().getId());
			
			  if(raiz.getChildAt(j).getData().getId().compareTo(nodo.getData().getId())==0){
				  for(int i=0;i< nodo.getChildCount();i++){
				    if(nodo.getChildCount()==1)
					cargarNodos((VMNodoMenuArbol) nodo.getChildAt(0),(VMNodoMenuArbol) raiz.getChildAt(j));  
				    else{
				    	 VMNodoMenuArbol aux = new VMNodoMenuArbol(nodo.getChildAt(i).getData(),null);
				         cargarNodos(aux,(VMNodoMenuArbol) raiz.getChildAt(j));
				    }
				  }
				  encontro=true;
			  }
		}
		if(!encontro)
			raiz.add(nodo);
	}	
} // fin VMMenuAplicacion.
