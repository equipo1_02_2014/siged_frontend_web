package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.reporte.CumplimientoActividad;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioActividadI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAreaI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;

public class VMCumplimientoActividades {

	@WireVariable
	ServicioReporteEstadisticoI servicioReporteEstadistico;
	@WireVariable
	ServicioActividadI servicioActividad;
	@WireVariable
	ServicioCategoriaI servicioCategoria;
	@WireVariable
	ServicioRangoI servicioRango;
	@WireVariable
	ServicioAreaI servicioArea;
	@WireVariable
	ServicioTecnicoI servicioTecnico;
	String pathProyecto;
	String restricciones;
	Date fechaDesde;
	Date fechaHasta;
	
	public class Atributo{
		String nombreAMostrar;
		String nombreReal;
		int tipo;//1:string 2: numero
		
		public Atributo(String nombreAMostrar, String nombreReal, int tipo) {
			super();
			this.nombreAMostrar = nombreAMostrar;
			this.nombreReal = nombreReal;
			this.tipo = tipo;
		}
		public String getNombreAMostrar() {
			return nombreAMostrar;
		}
		public void setNombreAMostrar(String nombreAMostrar) {
			this.nombreAMostrar = nombreAMostrar;
		}
		public String getNombreReal() {
			return nombreReal;
		}
		public void setNombreReal(String nombreReal) {
			this.nombreReal = nombreReal;
		}
		public int getTipo() {
			return tipo;
		}
		public void setTipo(int tipo) {
			this.tipo = tipo;
		}
		
		
	}
	Atributo atributoAFiltrar;
	List<Atributo> listaAtributos;
	List<String> operadores;
	String operadorSeleccionado="Seleccione";
	Date valorFiltroHora;
	boolean visibleComboValor=true;
	String tipoReporte="Por Actividades";
	
	public class Criterio{
		Atributo columna;
		String operador;
		String valor;
	public Criterio(Atributo columna, String operador, String valor) {
		super();
		this.columna = columna;
		this.operador = operador;
		this.valor = valor;
	}
	public Atributo getColumna() {
		return columna;
	}
	public String getOperador() {
		return operador;
	}
	public String getValor() {
		return valor;
	}
	public void setColumna(Atributo columna) {
		this.columna = columna;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	}
	List<Criterio> listaCriterios;
	String mensajeComboAtributo = "Seleccione";
	List<String> listaValoresFiltro;
	String valorSeleccionadoFiltro;
	String presentacionCriterios;
	
	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	

	public Atributo getAtributoAFiltrar() {
		return atributoAFiltrar;
	}

	public void setAtributoAFiltrar(Atributo atributoAFiltrar) {
		this.atributoAFiltrar = atributoAFiltrar;
	}

	public List<Atributo> getListaAtributos() {
		return listaAtributos;
	}

	public void setListaAtributos(List<Atributo> listaAtributos) {
		this.listaAtributos = listaAtributos;
	}
	

	public String getOperadorSeleccionado() {
		return operadorSeleccionado;
	}

	public void setOperadorSeleccionado(String operadorSeleccionado) {
		this.operadorSeleccionado = operadorSeleccionado;
	}
	

	public List<String> getOperadores() {
		return operadores;
	}

	public void setOperadores(List<String> operadores) {
		this.operadores = operadores;
	}

	public Date getValorFiltroHora() {
		return valorFiltroHora;
	}

	public void setValorFiltroHora(Date valorFiltroHora) {
		this.valorFiltroHora = valorFiltroHora;
	}

	public List<Criterio> getListaCriterios() {
		return listaCriterios;
	}

	public void setListaCriterios(List<Criterio> listaCriterios) {
		this.listaCriterios = listaCriterios;
	}

	public String getMensajeComboAtributo() {
		return mensajeComboAtributo;
	}

	public void setMensajeComboAtributo(String mensajeComboAtributo) {
		this.mensajeComboAtributo = mensajeComboAtributo;
	}
	
	public boolean isVisibleComboValor() {
		return visibleComboValor;
	}

	public void setVisibleComboValor(boolean visibleComboValor) {
		this.visibleComboValor = visibleComboValor;
	}

	public List<String> getListaValoresFiltro() {
		return listaValoresFiltro;
	}

	public void setListaValoresFiltro(List<String> listaValoresFitro) {
		this.listaValoresFiltro = listaValoresFitro;
	}

	public String getValorSeleccionadoFiltro() {
		return valorSeleccionadoFiltro;
	}

	public void setValorSeleccionadoFiltro(String valorSeleccionadoFiltro) {
		this.valorSeleccionadoFiltro = valorSeleccionadoFiltro;
	}

	public String getPresentacionCriterios() {
		return presentacionCriterios;
	}

	public void setPresentacionCriterios(String presentacionCriterios) {
		this.presentacionCriterios = presentacionCriterios;
	}

	public String getTipoReporte() {
		return tipoReporte;
	}
	@Command
	public void setTipoReporte(@BindingParam("tipo") String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	@Init
	public void init(){
		File file= new File(VMCumplimientoActividades.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		
		this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
		
		this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
		fechaDesde = new Date();
		fechaHasta = new Date();
		listaAtributos = new ArrayList<Atributo>();
		listaAtributos.add(new Atributo("Actividad","a.descripcion",1));
		listaAtributos.add(new Atributo("Tecnico","concat(et.tecnico.nombre,' ',et.tecnico.apellido)",1));
		listaAtributos.add(new Atributo("Categoria","e.categoria.nombre",1));
		listaAtributos.add(new Atributo("Rango","e.rango.descripcion",1));
		listaAtributos.add(new Atributo("Area","ap.area.nombre",1));
		listaAtributos.add(new Atributo("Hora de Inicio","ap.horaInicio",3));
		listaAtributos.add(new Atributo("Hora Fin","ap.horaFin",3));
		operadores = new ArrayList<String>();
		operadores.add("=");
		operadores.add(">");
		operadores.add(">=");
		operadores.add("<");
		operadores.add("<=");
		listaCriterios = new ArrayList<Criterio>();
		listaValoresFiltro = new ArrayList<String>();
	}
	
	
	private void llenarComboCriterio(){
		listaValoresFiltro.clear();
		if (atributoAFiltrar.nombreAMostrar.equalsIgnoreCase("Actividad")){
			List <Actividad> lista = servicioActividad.buscarActivos();
			for(int i=0; i< lista.size();i++){
				this.listaValoresFiltro.add(lista.get(i).getDescripcion());
			}
		}else if (atributoAFiltrar.nombreAMostrar.equalsIgnoreCase("Categoria")) {
			List <Categoria> lista = servicioCategoria.buscarTodos();
			for(int i=0; i< lista.size();i++){
				this.listaValoresFiltro.add(lista.get(i).getNombre());
			}
		}else if (atributoAFiltrar.nombreAMostrar.equalsIgnoreCase("Rango")) {
			List <Rango> lista = servicioRango.buscarTodos();
			for(int i=0; i< lista.size();i++){
				this.listaValoresFiltro.add(lista.get(i).getDescripcion());
			}
		}else if (atributoAFiltrar.nombreAMostrar.equalsIgnoreCase("Area")) {
			List <Area> lista = servicioArea.buscarTodos();
			for(int i=0; i< lista.size();i++){
				this.listaValoresFiltro.add(lista.get(i).getNombre());
			}
		}else if (atributoAFiltrar.nombreAMostrar.equalsIgnoreCase("Tecnico")) {
			List <Tecnico> lista = servicioTecnico.buscarTodos();
			for(int i=0; i< lista.size();i++){
				this.listaValoresFiltro.add(lista.get(i).getNombre()+" "+lista.get(i).getApellido());
			}
		}
		if (listaValoresFiltro.size()>0){
		   this.valorSeleccionadoFiltro = listaValoresFiltro.get(0);
		}
		
	}
	
	@Command
	@NotifyChange({"visibleComboValor","valorFiltroHora","listaValoresFiltro","valorSeleccionadoFiltro"})
	public void seleccionarAtributoFiltrar(){
		if (atributoAFiltrar.getTipo()==3){
			this.visibleComboValor=false;
			this.valorFiltroHora=new Date();
		}else{
			this.visibleComboValor=true;
			llenarComboCriterio();
		}
	}
	
	@Command
	@NotifyChange({"listaCriterios","mensajeComboAtributo","valorFiltroHora","operadorSeleccionado","listaValoresFiltro"})
	public void agregarCriterio(){
		if (!mensajeComboAtributo.equalsIgnoreCase("Seleccione") && !operadorSeleccionado.equalsIgnoreCase("Seleccione") && !valorSeleccionadoFiltro.equalsIgnoreCase("")){
		   if (visibleComboValor){
		      listaCriterios.add(new Criterio(atributoAFiltrar,operadorSeleccionado,valorSeleccionadoFiltro));	
		   }else{
		      listaCriterios.add(new Criterio(atributoAFiltrar,operadorSeleccionado,formatearHora(valorFiltroHora)));
		   }
		   mensajeComboAtributo = "Seleccione";
		   valorFiltroHora=new Date();
		   operadorSeleccionado="Seleccione";
		   listaValoresFiltro.clear();
		}
	}
	
	@Command
	@NotifyChange({"listaCriterios"})
	public void eliminarCriterio(@BindingParam("criterio")Criterio criterio){
		listaCriterios.remove(criterio);
	}
	
	private boolean validarFechas(){
		boolean valido = true;
		if (fechaDesde.after(fechaHasta) || fechaHasta.before(fechaDesde)){
		   Messagebox.show("El Rango de las Fechas es Invalido", "Error", Messagebox.OK, Messagebox.ERROR);
		   valido = false;
		}
		return valido;
	}
	
	@Command
	@NotifyChange({"fechaDesde","fechaHasta","listaCriterios","mensajeComboAtributo","visibleComboValo","operadorSeleccionado","valorSeleccionadoFiltro"})
	public void limpiar(){
		fechaDesde = new Date();
		fechaHasta = new Date();
		this.listaCriterios.clear();
		mensajeComboAtributo = "Seleccione";
		this.visibleComboValor=true;
		operadorSeleccionado="Seleccione";
		this.valorSeleccionadoFiltro="";
		
	}
	
	private void imprimirTipo(String titulo, String nombre, int tipo, String tituloColumna){
		List<CumplimientoActividad> lista = servicioReporteEstadistico.listaCumplimiento(restricciones, tipo);
		Map<String,Object> parameterMap = new HashMap<String,Object>();
		
		JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
		parameterMap.put("datasource", JRdataSource);
		parameterMap.put("titulo",titulo);
		parameterMap.put("fechaDesde",formatearFecha(fechaDesde,"dd/MM/YYYY"));
		parameterMap.put("fechaHasta",formatearFecha(fechaHasta,"dd/MM/YYYY"));
		parameterMap.put("filtros", presentacionCriterios);
		parameterMap.put("tituloColumna", tituloColumna);
		parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
		
		parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
			
		try {			
			
			JasperDesign jasDesign=null;
			
			jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + nombre);
			
			
			JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
	        
	        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
	      
	        JasperViewer jv = new JasperViewer(jasperPrint,false);
	        jv.setTitle(titulo);
	        jv.setVisible(true);
	        //JasperViewer.viewReport(jasperPrint,false);
			
		} catch (JRException e) {
			e.printStackTrace();
			Messagebox.show(e.getMessage());
		}
		
	}
	
	private void agregarFiltros(){
		String valorFiltro="";
		presentacionCriterios="";
	   for(int i=0; i < listaCriterios.size();i++){
		   //si es cadena o hora lleva comillas
	       if ((listaCriterios.get(i).getColumna().getTipo() == 1) || (listaCriterios.get(i).getColumna().getTipo() == 3)){
	    	   valorFiltro = "'"+ listaCriterios.get(i).getValor() +"'";
	       }
		   restricciones = restricciones + " and "+listaCriterios.get(i).getColumna().getNombreReal()+" "
			             +listaCriterios.get(i).getOperador()+valorFiltro;
		   presentacionCriterios = presentacionCriterios + listaCriterios.get(i).getColumna().getNombreAMostrar()+ ": "+ listaCriterios.get(i).getValor()+" "; 
		}
		
	}
	
	@Command
	public void imprimir(){

		restricciones="";
		restricciones = restricciones + "p.fecha between '"+formatearFecha(fechaDesde,"YYYY-MM-dd")+"' and '"+formatearFecha(fechaHasta,"YYYY-MM-dd")+"' ";
		agregarFiltros();
		
		if (validarFechas()){
			if (tipoReporte.equalsIgnoreCase("Por Equipos")){
		       imprimirTipo("Cumplimiento de Actividades por Equipos","reporteCumplimientoActividades.jrxml",3,"Equipos");
			}else if (tipoReporte.equalsIgnoreCase("Por Tecnicos")){
			   imprimirTipo("Cumplimiento de Actividades por Tecnicos","reporteCumplimientoActividades.jrxml",2,"Tecnicos");	
			}else{
			   imprimirTipo("Cumplimiento de Actividades","reporteCumplimientoActividades.jrxml",1,"Actividades");
			}
		}
	}
	
	private String formatearFecha(Date fecha, String formato){
		String fechaFormateada= new SimpleDateFormat(formato).format(fecha);
		return fechaFormateada;
	}
	
	public String formatearHora(Date fecha){
		String fechaFormateada= new SimpleDateFormat("HH:mm").format(fecha);
		return fechaFormateada; 
	}
		
		
}
