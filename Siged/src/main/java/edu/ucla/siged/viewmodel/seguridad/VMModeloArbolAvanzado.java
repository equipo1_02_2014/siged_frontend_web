package edu.ucla.siged.viewmodel.seguridad;

import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.DefaultTreeNode;
import edu.ucla.siged.domain.seguridad.Funcionalidad;



public class VMModeloArbolAvanzado extends DefaultTreeModel<Funcionalidad> {
	private static final long serialVersionUID = -5513180500300189445L;
	//-----------------Variables Objeto--------------------
	DefaultTreeNode<Funcionalidad> _root;

	/**
	* Constructor de la clase VMModeloArbolAvanzado
	*
	* @param nodoMenuArbol
	*/
	
	public VMModeloArbolAvanzado(VMNodoMenuArbol nodoMenuArbol) {
		super(nodoMenuArbol);
		_root = nodoMenuArbol;
	}

	/**
	 * Eliminar los nodos cuyo padre es <code>padre</code> con indices
	 * <code>indices</code>
	 * 
	 * @param padre
	 *            El padre de los nodos que ser�n removidos
	 * @param indiceDesde
	 *            El �ndice m�s bajo del rango de cambio
	 * @param indiceA
	 *            El �ndice superior del rango de cambio
	 * @return ninguno. 
	 * @throws ocurre una excepci�n IndexOutOfBoundsException cuando 
	 * - indiceDesde < 0 o el indiceA > N�mero de hijos de los padres
	 */
	public void remover(DefaultTreeNode<Funcionalidad> padre, int indiceDesde, int indiceA) throws IndexOutOfBoundsException {
		DefaultTreeNode<Funcionalidad> nodoPadre = padre;
		for (int i = indiceA; i >= indiceDesde; i--)
			try {
				nodoPadre.getChildren().remove(i);
			} catch (Exception exp) {
				exp.printStackTrace();
			}
	}

	/**
	 * Eliminar los nodos hijos cuyo padre tiene como hijo a objetivo
	 * <code>indices</code>
	 * 
	 * @param objetivo
	 * @return ninguno. 
	 * @throws No dispara ninguna excepcion. 
	 */
	
	public void remover(DefaultTreeNode<Funcionalidad> objetivo) throws IndexOutOfBoundsException {
		int indice = 0;
		DefaultTreeNode<Funcionalidad> padre = null;
		// encontrar el padre y el �ndice del objetivo
		padre = dfbuscarPadre(_root, objetivo);
		for (indice = 0; indice < padre.getChildCount(); indice++) {
			if (padre.getChildAt(indice).equals(objetivo)) {
				break;
			}
		}
		remover(padre, indice, indice);
	}

	/**
	 * Insertar nuevos nodos cuyo padre es <code>padre</code> con indices
	 * <code>indices</code> con nuevos nodos <code>nuevosNodos</code>
	 * 
	 * @param padre
	 *            El padre de los nodos que seran insertados.
	 * @param indiceDesde
	 *            El �ndice m�s bajo del rango de cambio
	 * @param indiceA
	 *            El �ndice superior del rango de cambio
	 * @param nuevosNodos
	 *            Los nodos nuevos que se insertan
	 * @return ninguno.
	 * @throws ocurre una excepci�n IndexOutOfBoundsException cuando 
	 * - indiceDesde < 0 o el indiceA > N�mero de hijos de los padres
	 */
	public void insertar(DefaultTreeNode<Funcionalidad> padre, int indiceDesde, int indiceA, DefaultTreeNode<Funcionalidad>[] nuevosNodos)
			throws IndexOutOfBoundsException {
		DefaultTreeNode<Funcionalidad> nodoPadre = padre;
		for (int i = indiceDesde; i <= indiceA; i++) {
			try {
				nodoPadre.getChildren().add(i, nuevosNodos[i - indiceDesde]);
			} catch (Exception exp) {
				throw new IndexOutOfBoundsException("Fuera de l�mite: " + i + " mientras tama�o=" + nodoPadre.getChildren().size());
			}
		}
	}

	/**
	 * agregar nuevos nodos cuyo padre es <code>padre</code> con nuevos nodos
	 * <code>nuevosNodos</code>
	 * 
	 * @param padre
	 *            El padre de los nodos que se adjuntan
	 * @param nuevosNodos
	 *            Nuevos nodos que se adjuntan
	* @return ninguno.
	* @throws No dispara ninguna excepcion.
	 */
	public void agregar(DefaultTreeNode<Funcionalidad> padre, DefaultTreeNode<Funcionalidad>[] nuevosNodos) {
		DefaultTreeNode<Funcionalidad> nodoPadre = (DefaultTreeNode<Funcionalidad>) padre;

		for (int i = 0; i < nuevosNodos.length; i++)
			nodoPadre.getChildren().add(nuevosNodos[i]);
	}

	/**
	 * Buscar el DefaultTreeNode<Nodo> padre dado un nodo objetivo y 
	 * el n�mero de hijos de un nodo determinado.
	 * 
	 * @param nodo, objetivo
	 * @return Objeto DefaultTreeNode<Nodo>. 
	 * @throws No dispara ninguna excepcion. 
	 */	
	
	private DefaultTreeNode<Funcionalidad> dfbuscarPadre(DefaultTreeNode<Funcionalidad> nodo, DefaultTreeNode<Funcionalidad> objetivo) {
		if (nodo.getChildren() != null && nodo.getChildren().contains(objetivo)) {
			return nodo;
		} else {
			int tamanho = getChildCount(nodo);
			for (int i = 0; i < tamanho; i++) {
				DefaultTreeNode<Funcionalidad> padre = dfbuscarPadre((DefaultTreeNode<Funcionalidad>) getChild(nodo, i), objetivo);
				if (padre != null) {
					return padre;
				}
			}
		}
		return null;
	}
} //fin VMModeloArbolAvanzado.
