package edu.ucla.siged.viewmodel.seguridad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;




import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.servicio.impl.seguridad.ServicioFuncionalidad;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;

public class VMListarFuncionalidades {
	@WireVariable
	private ServicioFuncionalidad servicioFuncionalidad;
	@WireVariable
	Funcionalidad funcionalidad;

	List<Funcionalidad> listaFuncionalidades;
	
	Funcionalidad funcionalidadSeleccionada;
	
	List<Funcionalidad> funcionalidadesSeleccionadas;
	
	private Window window;
	short estatusCombo=0;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	boolean estatusFiltro;
	String idPadre;
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	


	String nombre;

	public VMListarFuncionalidades() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
	public void init() {
		//listaFuncionalidades();
		cancelarFiltro();
		this.listaFuncionalidades=servicioFuncionalidad.buscarTodos(0).getContent();
		this.registrosTotales= servicioFuncionalidad.totalFuncionalidades();
	}

	public Funcionalidad getFuncionalidad() {
		return funcionalidad;
	}

	public void setFuncionalidad(Funcionalidad funcionalidad) {
		this.funcionalidad = funcionalidad;
	}

	public List<Funcionalidad> getListaFuncionalidades() {
		return listaFuncionalidades;
	}

	public void setListaFuncionalidades(List<Funcionalidad> listaFuncionalidades) {
		this.listaFuncionalidades = listaFuncionalidades;
	}

	public Funcionalidad getFuncionalidadSeleccionada() {
		return funcionalidadSeleccionada;
	}

	public void setFuncionalidadSeleccionada(Funcionalidad funcionalidadSeleccionada) {
		this.funcionalidadSeleccionada = funcionalidadSeleccionada;
	}
	
	public void listaFuncionalidades(){
		listaFuncionalidades = servicioFuncionalidad.buscarTodos();	
	}
	
	
	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina=ServicioFuncionalidad.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public boolean isEstatusFiltro() {
		return estatusFiltro;
	}

	public void setEstatusFiltro(boolean estatusFiltro) {
		this.estatusFiltro = estatusFiltro;
	}
	
	public String getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	

	public List<Funcionalidad> getFuncionalidadesSeleccionadas() {
		return funcionalidadesSeleccionadas;
	}

	public void setFuncionalidadesSeleccionadas(
			List<Funcionalidad> funcionalidadesSeleccionadas) {
		this.funcionalidadesSeleccionadas = funcionalidadesSeleccionadas;
	}

	@Command
	@NotifyChange({"listaFuncionalidades"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		System.out.println("estatus:"+estatusEnLista.toString());
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	@NotifyChange("listaFuncionalidad")
	public void agregarFuncionalidad(){
		window = (Window)Executions.createComponents("/vistas/seguridad/funcionalidad.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange("listaFuncionalidades")
	public void editarFuncionalidad(@BindingParam("funcionalidadSeleccionada") Funcionalidad funcionalidadSeleccionada){
		Map<String,Funcionalidad> mapFuncionalidad = new HashMap<String, Funcionalidad>();
		mapFuncionalidad.put("funcionalidadSeleccionada", funcionalidadSeleccionada);
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/seguridad/funcionalidad.zul", null, mapFuncionalidad);
		window.doModal();
		
	
	
	
	}
	
	@Command
	@NotifyChange("listaFuncionalidades")
	public void eliminarFuncionalidad(@BindingParam("funcionalidadSeleccionada") Funcionalidad funcionalidadSeleccionada){
		try{
		servicioFuncionalidad.eliminar(funcionalidadSeleccionada);
		actualizarLista(funcionalidadSeleccionada);
	
		}
		catch(Exception e){
			Messagebox.show("No se ha podido eliminar la funcionalidad!", "Error", Messagebox.OK, Messagebox.ERROR);
		}
		
	}
	
	@Command
	@NotifyChange("listaFuncionalidades")
	public void eliminarFuncionalidades(){
		try{	
		servicioFuncionalidad.eliminarVarios(funcionalidadesSeleccionadas);
		actualizarLista(funcionalidadesSeleccionadas.get(0));
		}
		catch(Exception e){
			Messagebox.show("No se ha podido eliminar la funcionalidad!", "Error", Messagebox.OK, Messagebox.ERROR);
		}
	}
	
	@GlobalCommand
	@NotifyChange({"listaFuncionalidades","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevoFuncionalidad") Funcionalidad nuevoFuncionalidad){
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} 
		paginar();
	}
	
	@GlobalCommand
	@NotifyChange({"listaFuncionalidades","registrosTotales","paginaActual","funcionalidadesSeleccionadas"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaFuncionalidades = servicioFuncionalidad.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioFuncionalidad.totalFuncionalidades();
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@Command
	@NotifyChange({"listaFuncionalidades","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		List<String> condiciones = new ArrayList<String>();
		
		if(nombre!=null && !nombre.trim().isEmpty())	
		condiciones.add(" nombre like '%"+nombre+"%' ");
		if(!idPadre.trim().isEmpty())
		condiciones.add(" to_char(idpadre, '99999') like'%"+idPadre+"%' ");
		if(!filtroEstatus.isEmpty())
		condiciones.add(" estatus in "+filtroEstatus);
		
		
		jpql="";	
		for(int i=0; i<condiciones.size(); i++){
			jpql+= condiciones.get(i);
			if(i+1<condiciones.size())
			jpql+= " and ";
		}
		
		System.out.println("----------------------------------"+jpql);
		this.listaFuncionalidades=servicioFuncionalidad.buscarFiltrado(jpql,this.paginaActual);
		
		this.registrosTotales=servicioFuncionalidad.totalFuncionalidadesFiltradas(jpql);
	}
	
	@Command
	@NotifyChange({"listaFuncionalidades","registrosTotales","paginaActual","funcionalidadesSeleccionadas",
				   "nombre","idPadre","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		nombre="";
		idPadre="";
		estatusCombo=0;
	}

	
}
