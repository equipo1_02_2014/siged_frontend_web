package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioRecurso;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMSolicitarDonacionPortal {
	
	@WireVariable
	private List<Recurso> comboxRecursoSolicitado;
	@WireVariable
	private ServicioRecurso servicioRecurso;
	@WireVariable
	private ServicioDonacion servicioDonacion;
	@WireVariable
	private Donacion donacion = new Donacion();
	@WireVariable
	private DonacionRecurso donacionRecurso;
	@WireVariable
	private Set<DonacionRecurso> recursos;
	@WireVariable
	private Set<DonacionRecurso> recursosPorEliminar = new HashSet<DonacionRecurso>();
	@WireVariable
	private Set<DonacionRecurso> recursosSeleccionados = new HashSet<DonacionRecurso>();
	private Window ventanaSolicitarDonacionPortal;
	
	public VMSolicitarDonacionPortal(){
		
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("donacion") Donacion donacionSeleccionada){
		Selectors.wireComponents(view, this, false);

		this.donacionRecurso = new DonacionRecurso();
		this.comboxRecursoSolicitado = servicioRecurso.ListarRecursos();
		
		if(donacionSeleccionada!=null){
			this.donacion = donacionSeleccionada;
			this.recursos = this.donacion.getRecursosDonados();
		}
		else{
			this.donacion = new Donacion();
			this.recursos = new HashSet<DonacionRecurso>();
			this.donacion.setFechaSolicitud(new Date());
		}
	}

	public Donacion getDonacion() {
		return donacion;
	}

	public void setDonacion(Donacion donacion) {
		this.donacion = donacion;
	}

	public Set<DonacionRecurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(Set<DonacionRecurso> recursos) {
		this.recursos = recursos;
	}
	
	public DonacionRecurso getDonacionRecurso() {
		return donacionRecurso;
	}

	public void setDonacionRecurso(DonacionRecurso donacionRecurso) {
		this.donacionRecurso = donacionRecurso;
	}
	
	public List<Recurso> getComboxRecursoSolicitado() {
		return comboxRecursoSolicitado;
	}

	public void setComboxRecursos(List<Recurso> comboxRecursos) {
		this.comboxRecursoSolicitado = comboxRecursos;
	}
	
	public Set<DonacionRecurso> getRecursosSeleccionados() {
		return recursosSeleccionados;
	}

	public void setRecursosSeleccionados(Set<DonacionRecurso> recursosSeleccionados) {
		this.recursosSeleccionados = recursosSeleccionados;
	}
	
	@Command
	@NotifyChange({"donacion","recursos","donacionRecurso"})
	public void CargarRecursos(){
		if(donacionRecurso.getCantidadSolicitada()==null || donacionRecurso.getCantidadSolicitada().equals("0")){
			Messagebox.show("Debe especificar una cantidad mayor a 0", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else{
			if(donacionRecurso.getRecurso() != null){
				donacionRecurso.setCantidad(0);
				if(!RecursoContenido(donacionRecurso)){
					this.recursos.add(donacionRecurso);
				}else{
					Messagebox.show("El recurso ya fue seleccionado", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			}else{
				Messagebox.show("No ha seleccionado un recursos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			LimpiarRecurso();
		}
	}
	
	//retorna true si el recurso ya existe de lo contrario retorna false
		private boolean RecursoContenido(DonacionRecurso dr){
			for(DonacionRecurso aux:recursos){
				if(aux.getRecurso().getId()==dr.getRecurso().getId())
					return true;
			}
			return false;
		}
	
	@Command
	@NotifyChange({"donacion","recursos"})
	public void enviarSolicitud(@BindingParam("ventanaSolicitarDonacionPortal") Window ventanaSolicitarDonacionPortal){
		this.ventanaSolicitarDonacionPortal = ventanaSolicitarDonacionPortal;
		if(donacion.getRif()==null || donacion.getRif().isEmpty() || 
		   donacion.getNombre()==null || donacion.getNombre().isEmpty() || 
		   donacion.getEmail()==null || donacion.getEmail().isEmpty() || 
		   donacion.getCelular()==null || donacion.getCelular().isEmpty() || 
		   donacion.getTelefono()==null || donacion.getTelefono().isEmpty() || 
		   donacion.getFechaSolicitud()==null){
			Messagebox.show("Debe completar todos los campos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else {
			if(!this.recursos.isEmpty()){
				this.donacion.setEstatus(Short.parseShort("0"));
				this.donacion.setTipo((short)1);
				this.donacion.setFecha(new Date());
				this.donacion.setRecursosDonados(recursos);
				this.donacion.setFechaSolicitud(new Date());
				servicioDonacion.Guardar(donacion);
				Messagebox.show("Solicitud Enviada exitosamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
				limpiar();
				cerrarVentanaSolicitarDonacionPortal();
			}else{
				Messagebox.show("No se han cargado recursos a la solicitud", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}
	}
	
	private void cerrarVentanaSolicitarDonacionPortal(){
		ventanaSolicitarDonacionPortal.detach();
	}
	
	@NotifyChange({"donacion","donacionRecurso"})
	public void LimpiarRecurso() {
		donacionRecurso = new DonacionRecurso();
	}
	
	@Command
	@NotifyChange({"recursos"})
	public void eliminarRecurso(@BindingParam("recursoSeleccionado") DonacionRecurso recursoSeleccionado){
		recursosPorEliminar.add(recursoSeleccionado);
		getRecursos().remove(recursoSeleccionado);
		Messagebox.show("Recurso eliminado correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({"donacion","recursos"})
	public void limpiar(){
		this.donacion = new Donacion();
		this.donacion.setFechaSolicitud(new Date());
		this.recursos = new HashSet<DonacionRecurso>();
		this.donacionRecurso = new DonacionRecurso();
		this.recursosPorEliminar = new HashSet<DonacionRecurso>();
	}
}