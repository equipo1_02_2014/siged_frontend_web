package edu.ucla.siged.viewmodel.gestiondeportiva;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCambioTecnicoI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;
//import edu.ucla.siged.viewmodel.util.messages.*;

@Controller
public class VMlistarpractica {
	
    private Window window;
    
    @WireVariable @Autowired
    private ServicioPracticaI servicioPractica;
    @WireVariable
    private ServicioCambioTecnicoI servicioCambioTecnico;
	private List<Practica> listaPractica;
	private List<Practica> practicasSeleccionadas= new ArrayList<Practica>();
    private int opcion=0; //1 la lista se llamo por menu d planificacion, 2 se llamo por menu de control
    boolean visiblesPlanificar=true;
    boolean visiblesControl=true;
   
    
    //paginacion
	 int paginaActual;
	 int tamanoPagina;
	 long registrosTotales;
	 short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	 
	// las variables siguientes son del filtro
	 boolean estatusFiltro;
	 String nombreEquipo=""; 
	 Date fechaPractica;
	 Date horaInicio;
	 Date horaFin;
	 int estatus;
	
    @Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {
    	if (view.getId().equalsIgnoreCase("listaPracticas")){
    	   this.opcion=1;
    	}else{
    		this.opcion=2;
    	}
		if (opcion==1) {
			this.visiblesPlanificar=true;
			this.visiblesControl=false;
			this.estatus=0;
			listaPractica = servicioPractica.buscarTodos(0).getContent();
			registrosTotales = servicioPractica.totalPracticas();
		}else{
			listaPractica=servicioPractica.buscarFiltrado(" 1=1 and estatus=0 ",0);
			registrosTotales = servicioPractica.totalPracticasFiltradas(" 1=1 and estatus=0 ");
			this.visiblesPlanificar=false;
			this.visiblesControl=true;
			this.estatus=2;
		}
	}

	
	public List<Practica> getListaPractica(){
		return listaPractica;
	}
	
	public void setListaPractica(List<Practica> listapractica){
		this.listaPractica = listapractica;
	}
	
	public List<Practica> getPracticasSeleccionadas() {
		return practicasSeleccionadas;
	}


	public void setPracticasSeleccionadas(List<Practica> practicasSeleccionadas) {
		this.practicasSeleccionadas = practicasSeleccionadas;
	}


	public boolean isVisiblesPlanificar() {
		return visiblesPlanificar;
	}


	public void setVisibles(boolean visibles) {
		this.visiblesPlanificar = visibles;
	}
	
	
	public boolean isVisiblesControl() {
		return visiblesControl;
	}


	public void setVisiblesControl(boolean visiblesControl) {
		this.visiblesControl = visiblesControl;
	}
	

	public String getNombreEquipo() {
		return nombreEquipo;
	}


	public void setNombreEquipo(String nombreEquipo) {
		this.nombreEquipo = nombreEquipo;
	}


	public Date getFechaPractica() {
		return fechaPractica;
	}


	public void setFechaPractica(Date fechaPractica) {
		this.fechaPractica = fechaPractica;
	}


	public Date getHoraInicio() {
		return horaInicio;
	}


	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}


	public Date getHoraFin() {
		return horaFin;
	}


	public void setHoraFin(Date horaFin) {
		this.horaFin = horaFin;
	}
	
	public int getEstatus() {
		return estatus;
	}


	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}


	public String formatearFecha(Date fecha){
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
	
	public String formatearHora(Date fecha){
		String fechaFormateada= new SimpleDateFormat("hh:mm a").format(fecha);
		return fechaFormateada;
	}
	
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioPractica.TAMANO_PAGINA;
		return tamanoPagina;
	}


	public long getRegistrosTotales() {
		return registrosTotales;
	}


	public int getPaginaActual() {
		return paginaActual;
	}


	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}


	@Command
	public void agregarPractica(){
	   this.tipoOperacion=1;
	   if (opcion==1)
	   window = (Window)Executions.createComponents(
                "/vistas/gestiondeportiva/planificacionpractica.zul", null, null);
	   else
	   window = (Window)Executions.createComponents(
	            "/vistas/gestiondeportiva/controlpractica.zul", null, null);	   
       window.doModal();

	}
	
	@GlobalCommand
    public void verVentanaEdicion(@BindingParam("id") Integer idPractica){
    	  Map mapa = new HashMap<String,Integer>();
		   mapa.put("id",idPractica);	
		   window = (Window)Executions.createComponents(
		             "/vistas/gestiondeportiva/planificacionpractica.zul", null, mapa);
		   window.doModal();
    }
	
	@Command
    @NotifyChange("listaPractica")
    public void editar(@BindingParam("practicaEditar")final Practica practicaEditar){  
		this.tipoOperacion=2;
		if (opcion==1){
			if (practicaEditar.getEstatus()==1){
		    	   Messagebox.show("Si edita una practica evaluada, perdera la informacion de la evaluacion realizada. Seguro de continuar?", "Confirmacion", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
		 		      public void onEvent(Event evt) {
		 		         if (evt.getName().equalsIgnoreCase("onYes")){
		 		           Map mapa = new HashMap<String,Integer>();
		 				   mapa.put("id",practicaEditar.getId());
		 				   BindUtils.postGlobalCommand(null, null, "verVentanaEdicion",mapa);
		 		         }
		 		      }
		 		     });
		    } else{
		    	verVentanaEdicion(practicaEditar.getId());	
		    }
		}
		
		
    }
	
	    
    @Command
    @NotifyChange({"listaPractica","registrosTotales","paginaActual"})
    public void eliminar(@BindingParam("practicaEliminar") final Practica practicaEliminar){  	 
    	Messagebox.show("Si elimina la planificacion de la practica, se eliminara tambien cualquier evaluacion asociada. Seguro de eliminar?", "Confirmacion", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
		    public void onEvent(Event evt) {
		       if (evt.getName().equalsIgnoreCase("onYes")){
		    	   servicioPractica.eliminar(practicaEliminar);
		    	   BindUtils.postGlobalCommand(null, null, "paginar", null);
				   Messagebox.show("El registro ha sido eliminado exitosamente.","",Messagebox.OK, Messagebox.INFORMATION);
					
		       }
		       
		    }
		});
    }
    
    @Command
    public void eliminarVarios(@BindingParam("lista") final List<Practica> practicasEliminar){
    	if (practicasEliminar.size() > 0){
    	   Messagebox.show("Si elimina las "+practicasEliminar.size()+" planificaciones seleccionadas, se eliminara tambien cualquier evaluacion asociada a estas. Seguro de eliminar?", "Confirmacion", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
		      public void onEvent(Event evt) {
		         if (evt.getName().equalsIgnoreCase("onYes")){
		    	    servicioPractica.eliminarVarios(practicasEliminar);
		    	    BindUtils.postGlobalCommand(null, null, "paginar", null);
				    Messagebox.show("Los registros seleccionados han sido eliminados exitosamente.","",Messagebox.OK, Messagebox.INFORMATION);
					
		         }
		       
		      }
		   });
    	}
    }
    
    @Command
    public void evaluar(@BindingParam("practicaEvaluar")final Practica practicaEvaluar){
    	
       Map mapa = new HashMap<String,Integer>();
	   mapa.put("id",practicaEvaluar.getId());
   	   window = (Window)Executions.createComponents(
			            "/vistas/gestiondeportiva/controlpractica.zul", null, mapa);
       window.doModal();
    	
    }
    
    @Command
    public String descripcionEstatus(short estatus){
    	if (estatus==1){
    		return "Evaluada";
    	}else{ 
    		return "No Evaluada";
    	}
    	
    }
    
    
    @GlobalCommand
    @NotifyChange({"listaPractica","registrosTotales","paginaActual"})
    public void actualizarLista(){
    	
    	if (this.tipoOperacion==1){
 		   this.getRegistrosTotales();
 		   this.registrosTotales++;
 		   this.paginaActual=0;
 		 
 		   /*this.getRegistrosTotales();
 		   this.registrosTotales++; 
 		   int ultimaPagina=0;
 		   if (this.registrosTotales%this.tamanoPagina==0)
 		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
 		   else
 		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
 		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar */
 		} else if(this.tipoOperacion==2){
 		
 		}
 		paginar();
    }
    
    private void casoEspecial(){
    	//solamente hay un elemento en la lista y es ese elemento el que se elimina
    	if ((listaPractica.size()<=0) && registrosTotales >0){
    		this.paginaActual--;
    		this.listaPractica = servicioPractica.buscarTodos(this.paginaActual).getContent();
		}
    }
    
    @GlobalCommand
	@NotifyChange({"listaPractica","registrosTotales","paginaActual"})
	public void paginar(){
		if (estatusFiltro==false){
			if (opcion==1){
			   this.listaPractica = servicioPractica.buscarTodos(this.paginaActual).getContent();
			   this.registrosTotales=servicioPractica.totalPracticas();
			   casoEspecial();
			}else{
			   listaPractica=servicioPractica.buscarFiltrado(" 1=1 and estatus=0 ",this.paginaActual);
			   registrosTotales = servicioPractica.totalPracticasFiltradas(" 1=1 and estatus=0 ");
			   casoEspecial();
			}
			
		
		}else{
			
			ejecutarFiltro();
		}

	}
    
    @Command
	@NotifyChange({"listaPractica","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}

    
    @Command
	@NotifyChange({"listaPractica","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql=" 1=1 ";
		//if ((!nombreEquipo.equals("")) || (fechaPractica != null) || (horaInicio!=null) || (horaFin!=null) || (estatus>=0) ){
		   this.estatusFiltro=true;  //el filtro se ha activado
		   if (!nombreEquipo.equals(""))
		      jpql = jpql + " and equipo.categoria.nombre like '%"+nombreEquipo+"%'";
		   if (fechaPractica != null)
		      jpql = jpql + " and fecha = '"+fechaPractica+"'";
		   if (horaInicio!=null)
		      jpql = jpql + " and horaInicio = '"+formatearHora(horaInicio)+"'";
		   if (horaFin!=null)
		      jpql = jpql + " and horaFin = '"+formatearHora(horaFin)+"'";
		   
		   switch (estatus) {
		   case 1: jpql = jpql + " and estatus = 1 ";
		   break;
		   case 2: jpql = jpql + " and estatus = 0";
		   break;
		   default: jpql = jpql + " and (estatus = 0 or estatus = 1) " ; //todos
		  // }
		}
		System.out.println(estatus+"----------------------------------"+jpql);
		this.listaPractica=servicioPractica.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioPractica.totalPracticasFiltradas(jpql);
		if ((listaPractica.size()<=0) && registrosTotales >0){
			this.paginaActual--;
			this.listaPractica=servicioPractica.buscarFiltrado(jpql,this.paginaActual);
		}
	}
	
	@Command
	@NotifyChange({"listaPractica","registrosTotales","paginaActual","nombreEquipo","fechaPractica","horaInicio","horaFin","estatus"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		nombreEquipo="";
		fechaPractica=null;
		horaInicio=null;
		horaFin=null;
		if (opcion== 1){
			estatus=0;
		}else{
			estatus=2;
		}
	}
	
	public String concatenar(String cadena1, String cadena2){
		return cadena1+" "+cadena2;
	}
   

}

