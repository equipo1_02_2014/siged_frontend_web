package edu.ucla.siged.viewmodel.gestionequipos;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioAtletaEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioDelegadoEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoTecnicoI;


public class VMCierreEquipo {
	@WireVariable
	ServicioEquipoI servicioEquipo;
	@WireVariable
	ServicioAtletaEquipoI servicioAtletaEquipo;
	@WireVariable
	ServicioEquipoTecnicoI servicioEquipoTecnico;
	@WireVariable
	ServicioDelegadoEquipoI servicioDelegadoEquipo;
	
	@WireVariable
	List<Equipo> equiposSeleccionados;
	@WireVariable
	Equipo equipo;
	@WireVariable
	List<Equipo> listaEquiposActivos;
	@WireVariable
	List<Categoria> listaCategoria;
	@WireVariable
	List<Rango> listaRango;
	@WireVariable
	List<TipoEquipo> listaTipoEquipo;
	
	
	Categoria categoriaSeleccionada;
	Integer rangoSeleccionado;
	Integer tipoEquipoSeleccionado;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String nombre="";
	Date fechaCreacion;
	boolean estatusFiltro;
	
//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////

	public List<Equipo> getEquiposSeleccionados() {
		return equiposSeleccionados;
	}
	public void setEquiposSeleccionados(List<Equipo> equiposSeleccionados) {
		this.equiposSeleccionados = equiposSeleccionados;
	}
	public Equipo getEquipo() {
		return equipo;
	}
	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	public List<Equipo> getListaEquiposActivos() {
		return listaEquiposActivos;
	}
	public void setListaEquiposActivos(List<Equipo> listaEquiposActivos) {
		this.listaEquiposActivos = listaEquiposActivos;
	}
	public List<Categoria> getListaCategoria() {
		return listaCategoria;
	}
	public void setListaCategoria(List<Categoria> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}
	public List<Rango> getListaRango() {
		return listaRango;
	}
	public void setListaRango(List<Rango> listaRango) {
		this.listaRango = listaRango;
	}
	public List<TipoEquipo> getListaTipoEquipo() {
		return listaTipoEquipo;
	}
	public void setListaTipoEquipo(List<TipoEquipo> listaTipoEquipo) {
		this.listaTipoEquipo = listaTipoEquipo;
	}
	public Categoria getCategoriaSeleccionada() {
		return categoriaSeleccionada;
	}
	public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) {
		this.categoriaSeleccionada = categoriaSeleccionada;
	}
	public Integer getRangoSeleccionado() {
		return rangoSeleccionado;
	}
	public void setRangoSeleccionado(Integer rangoSeleccionado) {
		this.rangoSeleccionado = rangoSeleccionado;
	}
	public Integer getTipoEquipoSeleccionado() {
		return tipoEquipoSeleccionado;
	}
	public void setTipoEquipoSeleccionado(Integer tipoEquipoSeleccionado) {
		this.tipoEquipoSeleccionado = tipoEquipoSeleccionado;
	}
	public int getPaginaActual() {
		return paginaActual;
	}
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	public int getTamanoPagina() {
		return tamanoPagina;
	}
	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public boolean isEstatusFiltro() {
		return estatusFiltro;
	}
	public void setEstatusFiltro(boolean estatusFiltro) {
		this.estatusFiltro = estatusFiltro;
	}

////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	@Init
	public void init(){
	
	this.listaEquiposActivos = servicioEquipo.buscarEquiposTrue(0).getContent();
	this.registrosTotales= servicioEquipo.totalEquiposActivos();
	this.listaTipoEquipo = servicioEquipo.buscarTodosTipoEquipo();
	this.listaCategoria = servicioEquipo.buscarTodasCategoria();
	this.listaRango = servicioEquipo.buscarTodosRango();
	
	this.tamanoPagina=5;

}


//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////
	
	
	@GlobalCommand
	@NotifyChange({"listaEquiposActivos","registrosTotales","paginaActual","equiposSeleccionados"})
	public void paginar(){
	if (estatusFiltro==false){
	this.listaEquiposActivos = servicioEquipo.buscarEquiposTrue(this.paginaActual).getContent();
	this.registrosTotales=servicioEquipo.totalEquiposActivos();
	
	}else{
		ejecutarFiltro();
	}

}
//////////////////////////////////////////////EJECUTAR FILTRO/////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaEquiposActivos","registrosTotales", "listaTipoEquipo", "listaCategoria", "listaRango"})
	public void ejecutarFiltro(){
	String jpql="";
	String filtroTipoEquipo="";
	String filtroCategoria="";
	String filtroRango="";
	this.estatusFiltro=true;  //el filtro se ha activado
//	this.estatusFiltro=true;  //el filtro se ha activado
	
	if(tipoEquipoSeleccionado!=null) {
	filtroTipoEquipo="("+(tipoEquipoSeleccionado+1)+")" ;
	}else{
	
	
	filtroTipoEquipo="(";
	
	int i;
	
	for(i=0; i< (this.listaTipoEquipo.size()-1) ; ++i){
	
	filtroTipoEquipo +=	this.listaTipoEquipo.get(i).getId() + ",";
	
	}
	
	filtroTipoEquipo+= this.listaTipoEquipo.get(i).getId() + ")";
	
	}
	
	if(categoriaSeleccionada!=null) {
		filtroCategoria="("+(categoriaSeleccionada.getId())+")" ;
		}else{
		
		
		filtroCategoria="(";
		
		int i;
		
		for(i=0; i< (this.listaCategoria.size()-1) ; ++i){
		
		filtroCategoria +=	this.listaCategoria.get(i).getId() + ",";
		
		}
		
		filtroCategoria+= this.listaCategoria.get(i).getId() + ")";
		
		}
	
	if(rangoSeleccionado!=null) {
		filtroRango="("+(rangoSeleccionado+1)+")" ;
		}else{
		
		
		filtroRango="(";
		
		int i;
		
		for(i=0; i< (this.listaRango.size()-1) ; ++i){
		
		filtroRango +=	this.listaRango.get(i).getId() + ",";
		
		}
		
		filtroRango+= this.listaRango.get(i).getId() + ")";
		
		}
	
	if (fechaCreacion == null) 
	jpql = "nombre like '%"+nombre+"%' and idTipoEquipo in "+filtroTipoEquipo+" and idCategoria in "+filtroCategoria+" and idRango in "+filtroRango+" and estatus = 1";
	else if (fechaCreacion!=null)
	jpql = "nombre like '%"+nombre+"%' and idTipoEquipo in "+filtroTipoEquipo+" and idCategoria in "+filtroCategoria+" and idRango in "+filtroRango+" and fechaCreacion = '"+fechaCreacion+"' and estatus = 1";
	
	this.listaEquiposActivos=servicioEquipo.buscarFiltrado(jpql,this.paginaActual);
	this.registrosTotales=servicioEquipo.totalEquiposFiltrados(jpql);

	}


////////////////////////////////////////ACTUALIZAR LISTA/////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaEquiposActivos","registrosTotales","paginaActual"})
	public void actualizarLista(){
		if (this.tipoOperacion==1){
			this.getRegistrosTotales();
			int ultimaPagina=0;
			if (this.registrosTotales%this.tamanoPagina==0)
				ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina);
			else
				ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
			this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} else if(this.tipoOperacion==2){
			
		}
			paginar();
	}
	
	////////////////////////////////////////////CAMBIAR FORMATO DE FECHA/////////////////////////////////////////////

	@Command
	public String cambiarFormatoFecha(Date fecha){
	DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
	return df.format(fecha);
	}
	
/////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	

	@Command
	@NotifyChange({"listaEquiposActivos","registrosTotales","paginaActual","equiposSeleccionados",
	"nombre","fechaCreacion","categoriaSeleccionada","rangoSeleccionado","tipoCompetenciaSeleccionado"})
	public void cancelarFiltro(){
	this.estatusFiltro=false;
	paginar();
	nombre="";
	fechaCreacion=null;
	categoriaSeleccionada=null;
	rangoSeleccionado=null;
	tipoEquipoSeleccionado=null;
	}

/////////////////////////////////////////METODO DESACTIVAR///////////////////////////////////////////////////////////	

	
	@Command
	@NotifyChange({"listaEquiposActivos","registrosTotales","paginaActual","equiposSeleccionados","equipo"})
	public void DesactivarEquipo(@BindingParam("equipoSeleccionado") Equipo equipoSeleccionado){
	    
		this.equipo= equipoSeleccionado;
		
		if(servicioEquipo.verificarCerrar(equipoSeleccionado)){
			Messagebox.show(
	                "El equipo esta asociado a otros registros y no puede ser eliminado","", Messagebox.OK, Messagebox.ERROR
	            );
		}
		else{
			Messagebox.show("�Est� seguro que desea cerrar este registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
	                    
							if(Messagebox.ON_YES.equals(event.getName())){
								
								try{
									servicioEquipo.Desactivar(equipo);
									servicioAtletaEquipo.revocarAtletasEquipoCerrado(equipo); // no borra en el atletaequipo					
									servicioEquipoTecnico.revocarTecnicosEquipoCerrado(equipo);
									servicioDelegadoEquipo.revocarDelegadosEquipoCerrado(equipo);
								
								
								Messagebox.show(
						                "El equipo ha sido cerrado satisfactoriamente","", Messagebox.OK, Messagebox.INFORMATION);
								
								BindUtils.postGlobalCommand(null, null, "paginar", null);
								
								}catch(Exception e){
									
								}								
							}
						}
					});
			
	    }
   }
	
}