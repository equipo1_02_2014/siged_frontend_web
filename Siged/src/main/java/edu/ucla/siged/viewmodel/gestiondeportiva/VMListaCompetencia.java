package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Competencia;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;

public class VMListaCompetencia {
    
	@WireVariable
	ServicioCompetenciaI servicioCompetencia;
	@WireVariable
	Competencia competencia;
	@WireVariable
	List<Competencia> listaCompetencias;
	@WireVariable
	List<Competencia> competenciasSeleccionados;
	@WireVariable
	List<TipoCompetencia> listaTipoCompetencia;
	
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String nombre="";
	Date fechaInicio;
	Date fechaFin;
	String edicion="";
	boolean estatusFiltro;
	Integer tipoCompetenciaSeleccionado;
	
//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	
	public Competencia getCompetencia() {
		return competencia;
	}
	
	public void setCompetencia(Competencia competencia) {
		this.competencia = competencia;
	}
	
	public List<Competencia> getCompetenciasSeleccionados() {
		return competenciasSeleccionados;
	}
	
	public void setCompetenciasSeleccionados(List<Competencia> competenciasSeleccionados) {
		this.competenciasSeleccionados = competenciasSeleccionados;
	}
	
	public List<Competencia> getListaCompetencias() {
		return listaCompetencias;
	}
	
	public void setListaCompetencia(List<Competencia> listaCompetencias) {
		this.listaCompetencias = listaCompetencias;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioCompetencia.getTamanioPagina();
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public Date getFechaFin() {
		return fechaFin;
	}
	
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public String getEdicion() {
		return edicion;
	}
	
	public void setEdicion(String edicion) {
		this.edicion = edicion;
	}
	
	public List<TipoCompetencia> getListaTipoCompetencia() {
		return listaTipoCompetencia;
	}

	public void setListaTipoCompetencia(List<TipoCompetencia> listaTipoCompetencia) {
		this.listaTipoCompetencia = listaTipoCompetencia;
	}
	
	public Integer getTipoCompetenciaSeleccionado() {
		return tipoCompetenciaSeleccionado;
	}
	
	public void setTipoCompetenciaSeleccionado(Integer tipoCompetenciaSeleccionado) {
		this.tipoCompetenciaSeleccionado = tipoCompetenciaSeleccionado;
	}
	
////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	
	@Init
	public void init(){
		this.listaCompetencias = servicioCompetencia.buscarTodos(0).getContent();
		this.registrosTotales= servicioCompetencia.totalCompetencias();
		this.listaTipoCompetencia = servicioCompetencia.buscarTodosTipoCompetencia();
	}
////////////////////////////////////////AGREGAR COMPETENCIA///////////////////////////////////////////////////////////
	
	@Command
	public void agregarCompetencia() {
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestiondeportiva/competencia.zul", null, null);
		window.doModal();
	}
	
/////////////////////////////////////////////EDITAR TECNICO/////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"competencia"})
	public void editarCompetencia(@BindingParam("competenciaSeleccionado") Competencia competenciaSeleccionado ) {
		this.competencia=competenciaSeleccionado;
		this.tipoOperacion=2;
		//esta es la manera de pasar a la otra vista la competencia seleccionada
		Map<String, Competencia> mapa = new HashMap<String, Competencia>();
        mapa.put("competencia", competenciaSeleccionado);
		window = (Window)Executions.createComponents("/vistas/gestiondeportiva/competencia.zul", null, mapa);
		window.doModal();
		Messagebox.show("Competencia guardada exitosamente","", Messagebox.OK, Messagebox.INFORMATION);
	}
	
//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaCompetencias","registrosTotales","paginaActual","competenciasSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaCompetencias = servicioCompetencia.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioCompetencia.totalCompetencias();
		}else{
			ejecutarFiltro();
		}
	}
////////////////////////////////////////ACTUALIZAR LISTA/////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaCompetencias","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevoCompetencia") Competencia nuevoCompetencia){
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina);
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} else if(this.tipoOperacion==2){
		
		}
		paginar();
	}
////////////////////////////////////////////CAMBIAR FORMATO DE FECHA/////////////////////////////////////////////
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
//////////////////////////////////////////////EJECUTAR FILTRO/////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaCompetencias","registrosTotales", "listaTipoCompetencia"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		String filtroTipoCompetencia="";
		this.estatusFiltro=true;  //el filtro se ha activado
		this.estatusFiltro=true;  //el filtro se ha activado
		if(tipoCompetenciaSeleccionado!=null) {
			filtroTipoCompetencia="("+(tipoCompetenciaSeleccionado+1)+")" ;
		}
		else{
			filtroTipoCompetencia="(";
			int i;
			for(i=0; i< (this.listaTipoCompetencia.size()-1) ; ++i){
			   filtroTipoCompetencia +=	this.listaTipoCompetencia.get(i).getId() + ",";
			}
			filtroTipoCompetencia+= this.listaTipoCompetencia.get(i).getId() + ")";
		}
			
		if (fechaInicio == null && fechaFin==null) 
			jpql = " nombre like '%"+nombre+"%'  and edicion like '%"+edicion+"%'  and idTipoCompetencia in "+filtroTipoCompetencia+"";
		else  if (fechaInicio==null && fechaFin!=null)
			 jpql = "nombre like '%"+nombre+"%'  and edicion like '%"+edicion+"%'  and idTipoCompetencia in "+filtroTipoCompetencia+" and fechaFin = '"+fechaFin+"'";
		else if (fechaInicio!=null && fechaFin==null)
			jpql = "nombre like '%"+nombre+"%'  and edicion like '%"+edicion+"%'  and idTipoCompetencia in "+filtroTipoCompetencia+" and fechaInicio = '"+fechaInicio+"' ";
		else
		   jpql = "nombre like '%"+nombre+"%'  and edicion like '%"+edicion+"%'  and idTipoCompetencia in "+filtroTipoCompetencia+" and fechaFin = '"+fechaFin+"' and fechaInicio = '"+fechaInicio+"' ";

		this.listaCompetencias=servicioCompetencia.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioCompetencia.totalCompetenciasFiltrados(jpql);
	}
	
/////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	

	@Command
	@NotifyChange({"listaCompetencias","registrosTotales","paginaActual","competenciasSeleccionados",
				    "nombre","fechaInicio","fechaFin","edicion","tipoCompetenciaSeleccionado"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		nombre="";
		fechaInicio=null;
		fechaFin=null;
		edicion="";
		tipoCompetenciaSeleccionado=null;
	}

}