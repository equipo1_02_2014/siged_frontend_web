package edu.ucla.siged.viewmodel.gestionequipos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoHorarioI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;

public class VMEquipoHorario {
	
	@WireVariable
	ServicioEquipoI servicioEquipo; // Instancia la interface servicio
	@WireVariable
	ServicioEquipoHorarioI servicioEquipoHorario; // Instancia la interface servicio
	@WireVariable
	private HorarioPractica horarioPracticaSeleccionado = new HorarioPractica();
	@WireVariable
	private List<HorarioPractica> equiposHorariosSeleccionados;
	private List<HorarioPractica> listaEquiposPracticas;
	private List<HorarioPractica> listasEquiposPracticasSeleccionados;  
	private Equipo equipo;
	private List<String> listaDias = new ArrayList<String>(); 
	@WireVariable
	private  Integer dia=0;
	private  String diaTexto="";
	private  Date horaInicio;
	private  Date horaFin;
	short estatusCombo=0;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	private Window ventanaEquipoHorario;
	
	
	public VMEquipoHorario() {
		super();
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("equipoSeleccionado") Equipo equipoSeleccionado) {
		
		this.listaDias.add("Lunes");
		this.listaDias.add("Martes");
		this.listaDias.add("Miercoles");
		this.listaDias.add("Jueves");
		this.listaDias.add("Viernes");
		this.listaDias.add("Sabado");
		this.listaDias.add("Domingo");
		this.equipo= (Equipo) Sessions.getCurrent().getAttribute("equipoSeleccionadoHorario");
		
        if (this.equipo!=null){
        	this.listaEquiposPracticas=servicioEquipoHorario.getHorariosEquipo(this.equipo);       	
        }	
	        
      }      
    
	public Window getVentanaEquipoHorario() {
		return ventanaEquipoHorario;
	}

	public void setVentanaEquipoHorario(Window ventanaEquipoHorario) {
		this.ventanaEquipoHorario = ventanaEquipoHorario;
	}

	
	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	
	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public ServicioEquipoI getServicioEquipo() {
		return servicioEquipo;
	}

	public void setServicioEquipo(ServicioEquipoI servicioEquipo) {
		this.servicioEquipo = servicioEquipo;
	}

	public ServicioEquipoHorarioI getServicioEquipoHorario() {
		return servicioEquipoHorario;
	}

	public void setServicioEquipoHorario(ServicioEquipoHorarioI servicioEquipoHorario) {
		this.servicioEquipoHorario = servicioEquipoHorario;
	}
	
	public HorarioPractica getHorarioPracticaSeleccionado() {
		return horarioPracticaSeleccionado;
	}

	public void setHorarioPracticaSeleccionado(HorarioPractica horarioPracticaSeleccionado) {
		this.horarioPracticaSeleccionado = horarioPracticaSeleccionado;
	}

	public List<HorarioPractica> getEquiposHorariosSeleccionados() {
		return equiposHorariosSeleccionados;
	}

	public void setEquiposHorariosSeleccionados(List<HorarioPractica> equiposHorariosSeleccionados) {
		this.equiposHorariosSeleccionados = equiposHorariosSeleccionados;
	}

	public List<HorarioPractica> getListaEquiposPracticas() {
		return listaEquiposPracticas;
	}

	public void setListaEquiposPracticas(List<HorarioPractica> listaEquiposPracticas) {
		this.listaEquiposPracticas = listaEquiposPracticas;
	}
		
	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Date getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(Date horaFin) {
		this.horaFin = horaFin;
	}
	
	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	

	public List<HorarioPractica> getListasEquiposPracticasSeleccionados() {
		return listasEquiposPracticasSeleccionados;
	}

	public void setListasEquiposPracticasSeleccionados(List<HorarioPractica> listasEquiposPracticasSeleccionados) {
		this.listasEquiposPracticasSeleccionados = listasEquiposPracticasSeleccionados;
	}
	
	public String getDiaTexto() {
		return diaTexto;
	}

	public void setDiaTexto(String diaTexto) {
		this.diaTexto = diaTexto;
	}

	public List<String> getListaDias() {
		return listaDias;
	}

	public void setListaDias(List<String> listaDias) {
		this.listaDias = listaDias;
	}

	@Command
	@NotifyChange({"equipo", "listaEquiposPracticas", "horarioPracticaSeleccionado", "diaTexto","dia", "horaInicio", "horaFin"})
	public void agregarEquipoHorario(){
		
		horarioPracticaSeleccionado = new HorarioPractica();
		this.horarioPracticaSeleccionado.setEquipo(equipo);
		this.dia = verDiaNumero(diaTexto);
		this.horarioPracticaSeleccionado.setDia(dia);
		this.horarioPracticaSeleccionado.setHoraInicio(horaInicio);
		this.horarioPracticaSeleccionado.setHoraFin(horaFin);
		
		if(this.dia!= null && this.horaInicio!=null  && this.horaFin!=null){
			if(!this.buscarDiaHorario(horarioPracticaSeleccionado)){
				if(this.horaInicio.before(this.horaFin) ){
					listaEquiposPracticas.add(horarioPracticaSeleccionado);
					limpiarHorario();
				}else{
					Messagebox.show("La hora inicio debe ser menor a la hora fin", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			}else{
				Messagebox.show("El dia de práctica que intenta asignar, ya fue elegido", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}else{
			Messagebox.show("No pueden haber campos vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		
	}

	public boolean buscarDiaHorario(HorarioPractica horario) {
		for(HorarioPractica hp : listaEquiposPracticas)
		{
			if(hp.getDia() == horario.getDia())	
			return true;
		}
		return false;
	}
	
	@Command
	@NotifyChange({"equipo", "listaEquiposPracticas"})
	public void guardarHorario(@BindingParam("ventanaEquipoHorario") Window ventanaEquipoHorario){
		this.ventanaEquipoHorario=ventanaEquipoHorario;
		if(listaEquiposPracticas.size()>0){
			servicioEquipoHorario.guardarListaHorarioEquipo(listaEquiposPracticas);
			//this.limpiarHorario();
			Messagebox.show("Datos almacenados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
			cerrarVentanaEquipoHorario();		
		}
	}
	
	public void cerrarVentanaEquipoHorario(){
		ventanaEquipoHorario.detach();
	}

	//metodo que limpia el formulario
	@Command
	@NotifyChange({"diaTexto","dia", "horaInicio", "horaFin"})
	public void limpiarHorario(){
			this.dia= null;
			this.diaTexto= null;
			this.horaInicio=null;
			this.horaFin=null;
	}
	
	@Command
	@NotifyChange({"equipo", "listaEquiposPracticas"})
	public void eliminarHorarioEquipo(@BindingParam("equipoHorarioSeleccionado") HorarioPractica equipoHorarioSeleccionado){
		if(equipoHorarioSeleccionado.getId()!=null){
			servicioEquipoHorario.eliminar(equipoHorarioSeleccionado);
			this.listaEquiposPracticas.remove(equipoHorarioSeleccionado);
			Messagebox.show("Datos eliminados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			this.listaEquiposPracticas.remove(equipoHorarioSeleccionado);
			Messagebox.show("Datos eliminados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	//metodo que elimina roles de un usuario en lote, modifica horarioPracticaTemsu estatus a Inactivo
	@Command
	@NotifyChange({"equipo", "listaEquiposPracticas"})
	public void eliminarHorarios(){
		if(listasEquiposPracticasSeleccionados!=null){
			HorarioPractica horarioPracticaTem =null;
			for (int i=0;i<listasEquiposPracticasSeleccionados.size();i++){
				horarioPracticaTem=listasEquiposPracticasSeleccionados.get(i);			
				if(horarioPracticaTem.getId()!=null){
					servicioEquipoHorario.eliminar(horarioPracticaTem);
					this.listaEquiposPracticas.remove(horarioPracticaTem);
				}
				else{
					this.listaEquiposPracticas.remove(horarioPracticaTem);
				}
			}	
			this.listasEquiposPracticasSeleccionados=null;

			Messagebox.show("Datos eliminados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay datos seleccionados", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	
	public String verDiaTexto(Integer dia){
		String diaT="";
        switch (dia) {
        	case 0:  diaT = "Domingo";
        			break;
            case 1:  diaT = "Lunes";
                     break;
            case 2:  diaT = "Martes";
                     break;
            case 3:  diaT = "Miercoles";
                     break;
            case 4:  diaT = "Jueves";
                     break;
            case 5:  diaT = "Viernes";
                     break;
            case 6:  diaT = "Sabado";
                     break;
            case 7:  diaT = "Domingo";
				     break;
        }
		return diaT;
	}
		
	public Integer verDiaNumero(String diaText){
		
		Integer diaNum=0;
		if(diaText=="Domingo"){
			diaNum = 0;
		}else if(diaText=="Lunes"){
			diaNum = 1;
		}else if(diaText=="Martes"){
			diaNum = 2;
		}else if(diaText=="Miercoles"){
			diaNum = 3;
		}else if(diaText=="Jueves"){
			diaNum = 4;
		}else if(diaText=="Viernes"){
			diaNum = 5;
		}else if(diaText=="Sabado"){
			diaNum = 6;
		}	
		return diaNum;
	}

	public String formatearHora(Date fecha){
		String fechaFormateada= new SimpleDateFormat("hh:mm a").format(fecha);
		return fechaFormateada; 
	}
}