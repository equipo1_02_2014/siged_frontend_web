/** 
 * 	VMArea
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;
import edu.ucla.siged.domain.gestionrecursostecnicios.TipoArea;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAreaI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTipoAreaI;

public class VMArea {
	
	short tipoOperacion;
	Window window;
	private @WireVariable ServicioTipoAreaI servicioTipoArea;
	List<TipoArea> tipoAreas; 
	private List<HorarioArea> horarioAreaSeleccionados = new ArrayList<HorarioArea>();
	Integer diasSeleccionados;
	private Integer dia;	
	@WireVariable
	ServicioAreaI servicioArea;
	@WireVariable
	Area area;
	
	public List<TipoArea> getTipoAreas(){
		return tipoAreas;
	}
		
	public void setTipoAreas(List<TipoArea> tipoAreas){
		this.tipoAreas = tipoAreas;
	}
	
	public List<HorarioArea> getHorarioAreaSeleccionados() {
		return horarioAreaSeleccionados;
	}

	public void setHorarioAreaSeleccionados(
			List<HorarioArea> horarioAreaSeleccionados) {
		this.horarioAreaSeleccionados = horarioAreaSeleccionados;
	}

	public Integer getDiasSeleccionados() {
		return diasSeleccionados;
	}

	public void setDiasSeleccionados(Integer diasSeleccionados){
		this.diasSeleccionados = diasSeleccionados;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setArea(Area area) {
		this.area = area;
	}
	
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,@ExecutionArgParam("idArea") Integer idArea) {
		setTipoAreas(servicioTipoArea.obtenerListaTipoArea());
        if ((idArea!=null)){
        	this.area = servicioArea.buscarPorId(idArea);
        	this.tipoOperacion=2;
        }else{
        	area = new Area();
        	this.tipoOperacion=1;
        	}
    	}
	
	@Command
	@NotifyChange({"area","horarioarea"})   
	public void guardar(){
		if (area.getNombre()=="" || area.getNombre()==null || area.getUbicacion()=="" || area.getUbicacion()==null || 
			area.getTipoArea()==null){
				Messagebox.show("Debe llenar todos los campos para continuar", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
       	}
		else{
			try{
				Messagebox.show("�Desea agregar el registro?","Pregunta",
				Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener<Event>() {
		          public void onEvent(Event evt) throws InterruptedException {
			           if (evt.getName().equalsIgnoreCase("onYes")) {
		       		     if (area.getNombre()=="" || area.getNombre()==null || area.getUbicacion()=="" || area.getUbicacion()==null || area.getTipoArea()==null)
		       		     {
		       		 		Messagebox.show("Debe llenar todos los campos para continuar", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		       		 	 }
		       		        servicioArea.guardar(area);
		       		        Messagebox.show("El registro se ha guardado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
		       		     if (tipoOperacion==1){
		       		        Messagebox.show("�Desea agregar otro registro?","Pregunta",
							Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
							   new org.zkoss.zk.ui.event.EventListener<Event>() {
										public void onEvent(Event evt)
											throws Exception {
												if (evt.getName().equalsIgnoreCase("onYes")) {
													BindUtils.postGlobalCommand(null, null, "limpiar", null);
													area=new Area();
												}else{
													BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
													Window myWindow = (Window) Path.getComponent("/panelarea");
													myWindow.detach();
						        		    	}
											}
		       		        	});
		       		     }else{
		       		    	BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
		       		    	Window myWindow = (Window) Path.getComponent("/panelarea");
							myWindow.detach();
		       		     }
					     }
			        }
				});
			
		}catch(Exception ex){
			Messagebox.show("Error al Registrar Area","Error", Messagebox.OK, Messagebox.ERROR);
		}
		}
  }

	
	@GlobalCommand("limpiar")
	@NotifyChange({"area"})
	public void limpiar(){
		area.setNombre("");
		area.setUbicacion("");
		area.setTipoArea(null);
		area.getHorariosArea().clear();
	}
	
	@Command
	@NotifyChange({"area"})
	public void limpiarArea(){
		area.setNombre("");
		area.setUbicacion("");
		area.setTipoArea(null);
		area.getHorariosArea().clear();
	}
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("areaSeleccionado") Area areaSeleccionado ) {
		Window window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/area.zul", null, null);
		this.area = areaSeleccionado;
		window.doModal();
	}

	@GlobalCommand
	@NotifyChange({"area","horariosArea","horarioArea"})
	public void agregarHorarioArea(@BindingParam("horarioArea") HorarioArea horarioArea) { 
		if(!area.getHorariosArea().contains(horarioArea)){
			area.addHorariosArea(new HorarioArea(horarioArea.getId(),horarioArea.getHoraInicio(),horarioArea.getHoraFin(),horarioArea.getDia(),horarioArea.getObservacion(),horarioArea.getDisponible()));
		
		}
	}
	
	@Command
	@NotifyChange({"area","horarioArea","horariosArea"})
	public void abrirHorario(@BindingParam("horarioAreaSeleccionado") HorarioArea horarioAreaSeleccionado) { 
			Map<String, HorarioArea> mapHorario = new HashMap<String, HorarioArea>();
			mapHorario.put("horarioArea", horarioAreaSeleccionado);
			window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/horarioarea.zul", null, mapHorario);
			window.doModal();
	}
	
	@Command
	@NotifyChange({"horarioArea","horarios"})
	public String verDiasEnLista(Integer dia){
		String di;
		switch (dia){
		case 1:
			di="Domingo";
		    break;
		case 2:
			di="Lunes";
		    break;
		case 3:
			di="Martes";
		    break;
		case 4:
			di="Miercoles";
		    break; 
		case 5:
			di="Jueves";
		    break;
		case 6:
			di="Viernes";
		    break;	
		default:
			di="Sabado";
		}
		return di;
	}
	
	@Command
	@NotifyChange({"horarioArea","horarios"})
	public String verDisponibilidadEnLista(Boolean disponible){
		String disponibilidad;
		if (disponible==true)
			disponibilidad="SI";
		else 
			disponibilidad="NO";
		return disponibilidad;
	}
	
	@Command
	@NotifyChange("area")
	public void eliminarHorario(@BindingParam("horarioAreaSeleccionado") HorarioArea horarioAreaSeleccionado){
		area.getHorariosArea().remove(horarioAreaSeleccionado);
		Messagebox.show("El registro se ha eliminado exitosamente", "Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@GlobalCommand("eliminarVariosHorarios")
	@NotifyChange("area")
	public void eliminarHorarios(){
		area.getHorariosArea().removeAll(horarioAreaSeleccionados);
	}
	
	@Command
	@NotifyChange("listaTipoCompetencias")
	public void eliminarHorarios(@BindingParam("listaHor") Listbox listaHor){
		if(listaHor.getSelectedItems().size()>0)
		{
		
			Messagebox.show("�Desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
                        
							if(Messagebox.ON_YES.equals(event.getName())){
								
								BindUtils.postGlobalCommand(null, null, "eliminarVariosHorarios", null);
						
								Messagebox.show("Los registros se han eliminado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
							
							}
						}
					});
			
		
		}
		else Messagebox.show("Debe seleccionar los registros que desea eliminar.","",Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	public String  formatearHora(Date hora){
		String horaFormateada= new SimpleDateFormat("hh:mm a").format(hora);
		return horaFormateada;
	}
}