package edu.ucla.siged.viewmodel.gestionatletas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.domain.utilidades.EstadoCivil;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAyudaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPagoI;

public class VMListarPagosAtleta {

	private @WireVariable ServicioPagoI servicioPago;
	private @WireVariable ServicioAtletaI servicioAtleta;
	private @WireVariable ServicioHistoriaAyudaI servicioHistoriaAyuda;
	private List<Pago> pagosMensuales = new ArrayList<Pago>();
	private int paginaActual;
	private int tamanoPagina;
	private int registrosTotales;
	private Usuario usuario;
	private boolean listaVisible = false;
	private boolean mensajeVisible = true;
	private String titulo = "Estado de Cuenta Del Atleta ";
	private int max = 5;
	private @WireVariable Atleta atleta;

	@Init
	public void init() {
		this.paginaActual = 0;
		this.tamanoPagina = 12;
		Session session = Sessions.getCurrent();
		this.usuario = (Usuario) session.getAttribute("userCredential");
		if (usuario != null) {
			this.atleta = servicioAtleta.buscarAtletaPorCedula(usuario
					.getCedula());
			if(atleta!=null){
				listaVisible = true;
				mensajeVisible = false;
				Date fechaActual = new Date();
				this.titulo = "Estado de Cuenta Del Atleta "
						+ this.atleta.getNombre() + " " + this.atleta.getApellido()
						+ ", en el A�o " + (fechaActual.getYear() + 1900);
				int h = 1;
				if (fechaActual.getYear() == atleta.getFechaAdmision().getYear()) {
					h = atleta.getFechaAdmision().getMonth() + 1;
					this.registrosTotales = 12;
				} else {
					if (((fechaActual.getYear() - atleta.getFechaAdmision()
							.getYear()) + 1) <= this.max) {
						this.registrosTotales = ((fechaActual.getYear() - atleta
								.getFechaAdmision().getYear()) + 1) * 12;
					} else {
						this.registrosTotales = this.max * 12;
					}
				}
	
				Long annoActual = Long.valueOf(fechaActual.getYear() + 1900);
				for (int i = h; i <= (fechaActual.getMonth() + 1); i++) {
					Integer j = Integer.valueOf(i);
					Pago pago = new Pago(null, null, null, j, annoActual, null);
					pagosMensuales.add(pago);
				}
			}
		}
	}

	@Command
	@NotifyChange({ "pagosMensuales", "titulo" })
	public void paginar() {
		pagosMensuales.clear();
		Date fechaActual = new Date();
		this.titulo = "Estado de Cuenta Del Atleta " + this.atleta.getNombre()
				+ " " + this.atleta.getApellido() + ", en el A�o "
				+ ((fechaActual.getYear() + 1900) - this.paginaActual);
		int h = 1;
		int n = 12;
		if ((fechaActual.getYear() - paginaActual) == atleta.getFechaAdmision()
				.getYear()) {
			h = atleta.getFechaAdmision().getMonth() + 1;
		} else if (this.paginaActual == 0) {
			n=fechaActual.getMonth()+1;
		}

		Long annoActual = Long.valueOf((fechaActual.getYear() + 1900)
				- this.paginaActual);
		for (int i = h; i <= n; i++) {
			Integer j = Integer.valueOf(i);
			Pago pago = new Pago(null, null, null, j, annoActual, null);
			pagosMensuales.add(pago);
		
		}

	}

	public List<Pago> getPagosMensuales() {
		return pagosMensuales;
	}

	public void setPagosMensuales(List<Pago> pagosMensuales) {
		this.pagosMensuales = pagosMensuales;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public int getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(int registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public boolean isListaVisible() {
		return listaVisible;
	}

	public void setListaVisible(boolean listaVisible) {
		this.listaVisible = listaVisible;
	}

	public boolean isMensajeVisible() {
		return mensajeVisible;
	}

	public void setMensajeVisible(boolean mensajeVisible) {
		this.mensajeVisible = mensajeVisible;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Command
	public String mostrarMes(int mes) {

		String mesString;
		switch (mes) {
		case 1:
			mesString = "ENERO";
			break;
		case 2:
			mesString = "FEBRERO";
			break;
		case 3:
			mesString = "MARZO";
			break;
		case 4:
			mesString = "ABRIL";
			break;
		case 5:
			mesString = "MAYO";
			break;
		case 6:
			mesString = "JUNIO";
			break;
		case 7:
			mesString = "JULIO";
			break;
		case 8:
			mesString = "AGOSTO";
			break;
		case 9:
			mesString = "SEPTIEMBRE";
			break;
		case 10:
			mesString = "OCTUBRE";
			break;
		case 11:
			mesString = "NOVIEMBRE";
			break;
		default:
			mesString = "DICIEMBRE";
			break;
		}
		return mesString;
	}

	@Command
	public String mostrarStatus(int mes, long anno) {
		String estatus = "MOROSO";
		int mesAux = mes-1;
		long annoP = anno-1900;
		int annoAux = (int)annoP;
		Date fecha = new Date(annoAux, mesAux, 1);
		if(servicioHistoriaAyuda.buscarBecaPorAtletaYFecha(this.atleta.getId(), fecha)){
			estatus = "BECADO";
		}else 
			if (servicioPago.buscarPagoPorMesYAnno(this.atleta.getId(),
				mes, anno)) {
			estatus = "SOLVENTE";
		}

		return estatus;
	}

}
