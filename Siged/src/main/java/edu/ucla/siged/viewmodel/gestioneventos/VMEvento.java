/** 
 * 	VMListaEvento
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ucla.siged.servicio.impl.gestionencuestas.ServicioPublico;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioAdjuntoEvento;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioEvento;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRol;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.utilidades.Archivo;

import org.springframework.stereotype.Controller;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

@Controller
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMEvento {

	@WireVariable
	private ServicioEvento servicioEvento;
	@WireVariable
	private ServicioAdjuntoEvento servicioAdjuntoEvento;
	@WireVariable
	private Evento evento = new Evento();
	@WireVariable
	private AdjuntoEvento adjuntoEvento = new AdjuntoEvento();
	@WireVariable
	private ServicioRol servicioRol;
	@WireVariable
	private ServicioPublico servicioPublico;
	@WireVariable
	private List<Rol> comboRol = new ArrayList<Rol>();
	
	private Archivo arch = new Archivo();
	private Media media;
	
	private Set<AdjuntoEvento> adjuntos = new HashSet<AdjuntoEvento>();
	private Set<AdjuntoEvento> adjuntosSeleccionados = new HashSet<AdjuntoEvento>();
	private Set<AdjuntoEvento> adjuntosPorEliminar = new HashSet<AdjuntoEvento>();
	private Window ventanaEvento;


	public VMEvento() {super();}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("evento") Evento eventoSeleccionado) throws ParseException{
		Selectors.wireComponents(view, this, false);
		if(eventoSeleccionado!=null){
			this.evento = eventoSeleccionado;
			this.adjuntos = evento.getAdjuntoEventos();
		}
		else{
			this.evento = new Evento();
			this.evento.setFecha(new Date());
			this.adjuntoEvento = new AdjuntoEvento();
			this.adjuntos = new HashSet<AdjuntoEvento>();
		}
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("hh:mm:ss");
		String today_str = formatter.format(today);
		this.evento.setHora(formatter.parse(today_str));
		this.comboRol = servicioRol.buscarActivos();
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public AdjuntoEvento getAdjuntoEvento() {
		return adjuntoEvento;
	}

	public void setAdjuntoEvento(AdjuntoEvento adjuntoEvento) {
		this.adjuntoEvento = adjuntoEvento;
	}

	public List<Rol> getComboRol() {
		return comboRol;
	}

	public void setComboRol(List<Rol> comboRol) {
		this.comboRol = comboRol;
	}

	public Archivo getArch() {
		return arch;
	}

	public void setArch(Archivo arch) {
		this.arch = arch;
	}

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

	public Set<AdjuntoEvento> getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(Set<AdjuntoEvento> adjuntos) {
		this.adjuntos = adjuntos;
	}

	public Set<AdjuntoEvento> getAdjuntosSeleccionados() {
		return adjuntosSeleccionados;
	}

	public void setAdjuntosSeleccionados(Set<AdjuntoEvento> adjuntosSeleccionados) {
		this.adjuntosSeleccionados = adjuntosSeleccionados;
	}

	public Set<AdjuntoEvento> getAdjuntosPorEliminar() {
		return adjuntosPorEliminar;
	}

	public void setAdjuntosPorEliminar(Set<AdjuntoEvento> adjuntosPorEliminar) {
		this.adjuntosPorEliminar = adjuntosPorEliminar;
	}

	public Window getWindow() {
		return ventanaEvento;
	}

	public void setWindow(Window ventanaEvento) {
		this.ventanaEvento = ventanaEvento;
	}
	
	@Command
	@NotifyChange({"evento","adjuntoEvento","adjuntos","limpiar","actualizarLista"})
	public void guardarEvento(@SelectorParam(".evento_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaEvento") Window ventanaEvento){
		this.ventanaEvento = ventanaEvento;
		boolean camposValidos = true;
		
		for(InputElement input:inputs){
			if(!input.isValid()){
				camposValidos = false;
				break;
			}
		}
		
		if(!camposValidos){
			Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK,Messagebox.EXCLAMATION);
		}
		else{
			if(evento.getNombre()==null || evento.getNombre().isEmpty() || evento.getFecha()==null ||
					evento.getHora()==null || evento.getDireccion()==null || evento.getDireccion().isEmpty() ||
					evento.getObservacion()==null || evento.getObservacion().isEmpty() || evento.getResponsable()==null ||
					evento.getResponsable().isEmpty() || evento.getPatrocinantes()==null || evento.getPatrocinantes().isEmpty() ||
					evento.getContactoCorreo()==null || evento.getContactoCorreo().isEmpty() || evento.getContactoTelefono()==null ||
					evento.getContactoTelefono().isEmpty()){
				Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK,Messagebox.EXCLAMATION);

			}else{
				adjuntoEvento.setFecha(new Date());
				evento.setFechaRegistro(new Date());
				evento.setResultadosEsperados(null);
				evento.setResultadosObtenidos(null);
				servicioEvento.Guardar(evento);
				Messagebox.show("El registro se ha guardado exitosamente","Información",Messagebox.OK,Messagebox.INFORMATION);
				limpiar();
				cerrarVentanaEvento();
				if(!adjuntosPorEliminar.isEmpty()){
					servicioAdjuntoEvento.EliminarVarios(adjuntosPorEliminar);
				}
			}
		}
	}
	
	public void cerrarVentanaEvento(){
		ventanaEvento.detach();
	}
	
	@Command
	@NotifyChange({"evento","adjuntoEvento","adjuntos"})
	public void limpiar(){
		evento = null;
		adjuntos = null;
		evento = new Evento();
		adjuntoEvento = new AdjuntoEvento();
		adjuntos = new HashSet<AdjuntoEvento>();
		adjuntosPorEliminar = new HashSet<AdjuntoEvento>();
	}
	
	@Command
	@NotifyChange({"noticia","adjuntoEvento","adjuntos"})
	public void agregarAdjunto(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		media = event.getMedia();
		adjuntoEvento = new AdjuntoEvento();
		arch = new Archivo();
		if(media != null){
			if(media instanceof org.zkoss.image.Image){
				arch.setNombreArchivo(media.getName());
				arch.setTipo(media.getContentType());
				arch.setContenido(media.getByteData());
				adjuntoEvento.setArchivo(arch);
				evento.getAdjuntoEventos().add(adjuntoEvento);
				adjuntos = evento.getAdjuntoEventos();
			}
			else{
				Messagebox.show("El archivo: "+media.getName()+" no es un archivo valido","Error",Messagebox.OK,Messagebox.ERROR);
			}
		}
	}
	
	@Command
	@NotifyChange({"adjuntos","evento"})
	public void eliminarAdjunto(@BindingParam("adjuntoSeleccionado") AdjuntoEvento adjuntoSeleccionado){
		adjuntosPorEliminar.add(adjuntoSeleccionado);
		getAdjuntos().remove(adjuntoSeleccionado);
		Messagebox.show("Adjunto eliminado exitosamente","Información",Messagebox.OK,Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({"adjuntos"})
	public void eliminarAdjuntos(){
		if(adjuntosSeleccionados != null){
			for(AdjuntoEvento ae : adjuntosSeleccionados){
				adjuntosPorEliminar.add(ae);
			}
			getAdjuntos().removeAll(adjuntosSeleccionados);
			Messagebox.show("Los registros se han eliminado exitosamente","Información",Messagebox.OK,Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay registros seleccionados","Advertencia",Messagebox.OK,Messagebox.EXCLAMATION);
		}
	}
	
	@Command
	public void verAdjunto(@BindingParam("image") AdjuntoEvento aux){
		Map<String,AdjuntoEvento> mapAux = new HashMap<String,AdjuntoEvento>();
		mapAux.put("adjuntoaux", aux);
		ventanaEvento = (Window) Executions.createComponents("/vistas/gestionevento/verAdjuntoEvento.zul", null, mapAux);
		ventanaEvento.doOverlapped();
	}

	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;	
	}

}
