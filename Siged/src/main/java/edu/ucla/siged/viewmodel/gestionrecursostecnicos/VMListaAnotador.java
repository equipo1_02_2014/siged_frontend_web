/** 
 * 	VMListaAnotador
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAnotadorI;

public class VMListaAnotador {
	
	@WireVariable
	ServicioAnotadorI servicioAnotador;
	@WireVariable
	Anotador anotador;
	List<Anotador> listaAnotadores;
	List<Anotador> anotadoresSeleccionados;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String cedula=""; 
	String nombre="";
	Date fechaRegistro;
	String apellido="";
	String telefono="";
	String celular="";
	Short estatusCombo=0;
	boolean estatusFiltro;
	
	public Anotador getAnotador() {
		return anotador;
	}
	
	public void setAnotador(Anotador anotador) {
		this.anotador = anotador;
	}
	
	public List<Anotador> getAnotadoresSeleccionados() {
		return anotadoresSeleccionados;
	}
	
	public void setAnotadoresSeleccionados(List<Anotador> anotadoresSeleccionados) {
		this.anotadoresSeleccionados = anotadoresSeleccionados;
	}
	
	public List<Anotador> getListaAnotadores() {
		return listaAnotadores;
	}
	
	public void setListaAnotadores(List<Anotador> listaAnotadores) {
		this.listaAnotadores = listaAnotadores;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioAnotador.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public String getCedula() {
		return cedula;
	}
	
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getCelular() {
		return celular;
	}
	
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	public Short getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Short estatus) {
		this.estatusCombo = estatus;
	}
	
	@Init
	public void init(){
		this.listaAnotadores = servicioAnotador.buscarTodos(0).getContent();
		this.registrosTotales= servicioAnotador.totalAnotadores();
		this.paginaActual=0;
	}
	
	@Command
	public void agregarAnotador() { 
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/anotador.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange({"listaAnotadores","registrosTotales"})
	public void eliminarAnotador(@BindingParam("anotadorSeleccionado") Anotador anotadorSeleccionado){
		if (!servicioAnotador.existeEnJuego(anotadorSeleccionado)){
			servicioAnotador.eliminar(anotadorSeleccionado);
			paginar();
			Messagebox.show("Se ha eliminado el registro exitosamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else {
			Messagebox.show("No puede eliminar ese Anotador porque forma parte de un juego","Advertencia", Messagebox.OK, Messagebox.ERROR);
		}
		if ((listaAnotadores.size()==0) && (this.registrosTotales>0)){
			this.listaAnotadores = servicioAnotador.buscarTodos(this.paginaActual-1).getContent();
		}
	}

	
	
	
	@Command
	@NotifyChange({"listaAnotadores","registrosTotales"})
	public void eliminarAnotadores(){
		int cuantos= servicioAnotador.eliminarVarios(anotadoresSeleccionados);
		if (cuantos>0){
			Messagebox.show("Se han eliminado los registros seleccionados exitosamente","Información", Messagebox.OK, Messagebox.INFORMATION);
		anotadoresSeleccionados.clear();
		}else
			Messagebox.show("No se eliminaron los anotadores por formar parte de un Juego", "Error", Messagebox.OK, Messagebox.ERROR);
		paginar();
		if ((listaAnotadores.size()==0) && (this.registrosTotales>0)){
			this.listaAnotadores = servicioAnotador.buscarTodos(this.paginaActual-1).getContent();
		}
	}
	
	@Command
	@NotifyChange({"anotador"})
	public void editarAnotador(@BindingParam("anotadorSeleccionado") Anotador anotadorSeleccionado ) {
		
		this.anotador=anotadorSeleccionado;
		this.tipoOperacion=2;
		//esta es la manera de pasar a la otra vista el anotador seleccionado
		Map<String, Anotador> mapa = new HashMap<String, Anotador>();
        mapa.put("anotador", anotadorSeleccionado);
    	window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/anotador.zul", null, mapa);
		window.doModal();
	}
	
	@GlobalCommand
	@NotifyChange({"listaAnotadores","registrosTotales","paginaActual","anotadoresSeleccionados"})
	public void paginar(){
		
			if (estatusFiltro==false){
				this.listaAnotadores = servicioAnotador.buscarTodos(this.paginaActual).getContent();
				this.registrosTotales=servicioAnotador.totalAnotadores();
			
			}else{
				
				ejecutarFiltro();
			}
		
	}
	
	@GlobalCommand
	@NotifyChange({"listaAnotadores","registrosTotales","paginaActual"})
	public void actualizarLista(){
		 	this.registrosTotales=servicioAnotador.totalAnotadores();
		 	if (registrosTotales>0){
				if (this.tipoOperacion==1) {
				   int ultimaPagina=0;
				   if ((this.registrosTotales)%this.tamanoPagina==0)
				      ultimaPagina= ((int) (this.registrosTotales)/this.tamanoPagina)-1;
				   else
					  ultimaPagina =((int) (this.registrosTotales)/this.tamanoPagina);
				   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
				}
				else if(this.tipoOperacion==2){
				}	
				 paginar();
		}
	}
	@Command
	@NotifyChange({"listaAnotadores","anotador"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
	}
	
	@Command
	@NotifyChange({"listaAnotadores","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		
		if (fechaRegistro == null)
			jpql = " cedula like '%"+cedula+"%' and nombre like '%"+nombre+"%' and apellido like '%"+apellido+"%' and telefono like '%"+telefono+"%' and celular like '%"+celular+"%'";
		else
			jpql = " cedula like '%"+cedula+"%' and nombre like '%"+nombre+"%' and apellido like '%"+apellido+"%' and telefono like '%"+telefono+"%' and celular like '%"+celular+"%' and fechaRegistro = '"+fechaRegistro+"'";
			
		
		this.listaAnotadores=servicioAnotador.buscarFiltrado(jpql,this.paginaActual);  //el filtro comienza a mostrar desde la 1ra pagina
		this.registrosTotales=servicioAnotador.totalAnotadoresFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"listaAnotadores","registrosTotales","paginaActual","anotadoresSeleccionados",
				   "cedula", "nombre","fechaRegistro","apellido","telefono","celular","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		cedula=""; // las variables siguientes son del filtro
		nombre="";
		fechaRegistro=null;
		apellido="";
		telefono="";
		celular="";
	}
	
	@GlobalCommand("asignarTipoOperacionGuardarAnotador")
    @NotifyChange({"tipoOperacion"})
    public void asignarTipoOperacionGuardar(){
        this.tipoOperacion= 1;

    }
	
}