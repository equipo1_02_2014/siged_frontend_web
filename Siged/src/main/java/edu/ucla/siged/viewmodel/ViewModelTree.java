package edu.ucla.siged.viewmodel;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zul.Center;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Borderlayout;

public class ViewModelTree {

	/* ---------------------------------- MENU REGISTROS BASICOS ------------------------------------ */
	
	@Command
	public void listaEquipo(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listaequipo.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listaTipoEquipo(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listatipoequipo.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarActividades(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestiondeportiva/listaactividad.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarCompetencias(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestiondeportiva/listacompetencia.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarTecnicos(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionrecursotecnico/listatecnico.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarArbitros(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionrecursotecnico/listararbitro.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarRecursos(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionevento/listarecurso.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarAnotadores(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionrecursotecnico/listaanotador.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarAreas(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionrecursotecnico/listaarea.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarCategorias(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listacategoria.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarRangos(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listarango.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listaCausa(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionatleta/listacausa.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	
	/* ----------------------------- MENU PARAMETROS DE CONFIGURACION ------------------------------- */	
	
	@Command
	public void ListarNoticias(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionevento/listarnoticias.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	
	/* ----------------------------------- MENU GESTION ATLETAS ------------------------------------- */
	@Command
	public void responderPostulantes(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionatleta/listapostulante.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	@Command
	public void inscribirAtletas(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionatleta/listainscribiratleta.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	@Command
	public void registrarAtleta(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionatleta/listaratleta.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void registrarpago(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionatleta/listarpago.zul", center, null);
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void asignarAtletas(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listaConformarEquipo.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void asignarBecas(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionatleta/listarbeca.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	/* ---------------------------------- MENU GESTION DEPORTIVA ------------------------------------ */	
	
	@Command
	public void asignarTecnicos(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listaAsignarTecnico.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void asignarDelegados(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionequipo/listaasignardelegado.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	
	@Command
	public void redirectListarPracticas(){
		try{
			Borderlayout borderlayout = (Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderlayout.getCenter();
			center.getChildren().clear();
			Map<String, Object> mapa = new HashMap<String, Object>();
	        mapa.put("titulo", "Lista de Planificacion de Practicas");
	        mapa.put("opcion", 1);
			Executions.createComponents("vistas/gestiondeportiva/listarpractica.zul", center, mapa);
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void redirectListarControlPracticas(){
		try{
			Borderlayout borderlayout = (Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderlayout.getCenter();
			center.getChildren().clear();
			Map<String, Object> mapa = new HashMap<String, Object>();
	        mapa.put("titulo", "Lista de Control y Seguimiento de Practicas");
	        mapa.put("opcion", 2);
			Executions.createComponents("vistas/gestiondeportiva/listarpractica.zul", center, mapa);
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void listarTipoCompetencias(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestiondeportiva/listatipocompetencia.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void redirectRegistrarJuego(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestiondeportiva/listajuego.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void redirectRegistrarJuegoResultado(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestiondeportiva/listajuegoresultado.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	/* ----------------------------- MENU GESTION DONACION Y EVENTOS -------------------------------- */
	
	@Command
	public void registrarEventos(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionevento/catalogoeventos.zul", center, null);
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	
	
	/* ----------------------------- MENU GESTION DONACION Y EVENTOS -------------------------------- */	

	@Command 
	public void ListarDonacion(){//donaciones recibidas
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionevento/listarDonacion.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void ListarSolicitudTercero(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionevento/listarSolicitudTercero.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void ListarSolicitudesAprobadas(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/gestionevento/listarSolicitudesAprobadas.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	/* ----------------------------------- MENU SEGURIDAD FUNCIONAL ------------------------------------ */
	@Command
	public void registrarFuncionalidad(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/seguridad/listafuncionalidades.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void registrarRol(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/seguridad/listaroles.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void activarUsuario(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/seguridad/listausuariosinactivos.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void registrarUsuario(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/seguridad/listausuarios.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	/*------------------------------------------MENU REPORTES---------------------------------------*/
	@Command
	public void reportesListas(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/general/reportelista.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void configurarRelacionJuegosJugadosGanados(){
		try{
			Borderlayout borderl =(Borderlayout) Path.getComponent("/borderLayauttree");
			Center center = borderl.getCenter();
			center.getChildren().clear();
			Executions.createComponents("vistas/reporteestadistico/vistaconfigurarrelacionjugadosganados.zul", center, null);	
		}catch (Exception e){
			Messagebox.show(e.toString());
		}
	}
	
}