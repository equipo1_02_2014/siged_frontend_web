/** 
 * 	VMRecurso
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestioneventos;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioRecursoI;

    //Declaraci�n de la clase VMRecurso con sus atributos:
    public class VMRecurso {
	@WireVariable
	ServicioRecursoI servicioRecurso;
	@WireVariable
	Recurso recurso;
	private Window ventanaRecurso;
	List<Short> listaEstatus;
	Short estatusSeleccionado;
	short tipoOperacion; //1: No existe ning�n objeto, 2: Ya existe un objeto.
	
	//M�todos Get y Set de la clase:
	public Recurso getRecurso() {
		return recurso;
	}
	
	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}

	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Short> getListaEstatus() {
		return listaEstatus;
	}

	//M�todo para inicializar el objeto recurso:
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("recurso") Recurso recursoSeleccionado) {
		listaEstatus= new ArrayList<Short>();
        listaEstatus.add((short) 0);
        listaEstatus.add((short) 1);
        estatusSeleccionado=1;

        if (recursoSeleccionado!=null){
        	this.tipoOperacion=2; //En el caso de que ya exista un objeto, tipoOperacion=2.
            this.recurso = recursoSeleccionado;
            this.estatusSeleccionado= recursoSeleccionado.getEstatus();
        }else
             {
        	    this.tipoOperacion=1; //En el caso de que haya que crear un nuevo objeto, tipoOperacion=1.
        	    recurso = new Recurso();
             }
    }
	
	//M�todo para guardar uno o varios objetos del tipo Recurso:
	@Command
	@NotifyChange({"recurso","limpiar","actualizarLista","estatusSeleccionado"})
	public void guardar(@SelectorParam(".recurso_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaRecurso") Window ventanaRecurso){
        this.ventanaRecurso = ventanaRecurso;
         
		if (this.estatusSeleccionado!=null)
			recurso.setEstatus(estatusSeleccionado);
		
        boolean camposValidos=true;
		
        //M�todo para validar si los campos est�n vac�os o no:
        for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
			break;
			}
			
		}
		
        //M�todo para preguntar si los campos est�n vac�os o no:
		if(!camposValidos || recurso.getDescripcion()==null || recurso.getDescripcion().isEmpty() || recurso.getNombreRecurso()==null || recurso.getNombreRecurso().isEmpty()){
			
			Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
			
		}
		
		else
		{
			if(this.tipoOperacion==1) //En el caso de que haya que crear un nuevo objeto.
			{
			servicioRecurso.guardar(recurso);
			
			Messagebox.show("El registro se ha guardado exitosamente.","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
			     
	       			Messagebox.show("�Desea agregar otro registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
	       					new EventListener<Event>() {
	       				
	       						public void onEvent(Event event) throws Exception {
	                                
	       							if(Messagebox.ON_NO.equals(event.getName())){
	       								
	       								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       								cerrarVentanaRecurso();
	       								
	       							}
	       							else BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       						}
	       					});

	        limpiar();
	        
			}
			
			if(this.tipoOperacion==2) //En el caso de ya exista un objeto.
			{
			servicioRecurso.guardar(recurso);
			Messagebox.show("Los cambios han sido guardados con �xito.","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
			cerrarVentanaRecurso();
			}
		}
			
	}

	//M�todo para cerrar el formulario:
	public void cerrarVentanaRecurso(){
		ventanaRecurso.detach();
	}
	
	//M�todo para limpiar los campos del formulario:
	@Command
	@NotifyChange({"recurso","estatusSeleccionado"})
	public void limpiar(){
    recurso.setId(null);
	recurso.setNombreRecurso("");
	recurso.setDescripcion("");
	estatusSeleccionado=1;
	}

}