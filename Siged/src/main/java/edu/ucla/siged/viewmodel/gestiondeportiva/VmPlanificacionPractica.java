package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.domain.gestiondeportiva.ActividadPractica;
import edu.ucla.siged.domain.gestiondeportiva.AsistenciaAtleta;
import edu.ucla.siged.domain.gestiondeportiva.CambioTecnico;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionequipo.HorarioPractica;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;

public class VmPlanificacionPractica {
	 @WireVariable
	 private ServicioPracticaI servicioPractica;
	 @WireVariable
	 Practica practica;
	 @WireVariable
	 ActividadPractica actividadPractica;
	 @WireVariable
	 List<EquipoTecnico> equipoTecnico;
	 
	 List<Equipo> listaEquipos;
	 List<Actividad> listaActividades;
	 List<Area> listaAreas;
	 List<ActividadPractica> listaActividadPracticas;
	// List<CambioTecnico> listaCambiosTecnicos;
	 List<Tecnico> listaTecnicos;
	
	 Tecnico tecnico;
	 Date fechaAnterior;
	 
	 int tipoOperacion; //1:incluir  2:editar
	 String mensajeComboArea="Seleccione";
	 String mensajeComboActividad="Seleccione";
	 String mensajeComboTecnico="Seleccione";
	 String mensajeComboEquipo="Seleccione Equipo";
	 boolean desHabilitarEncabezado=false;
	 boolean visibleTraerInformacion=false;
	 
	 
	public List<Equipo> getListaEquipos() {
		return listaEquipos;
	}

	public void setListaEquipos(List<Equipo> listaEquipos) {
		this.listaEquipos = listaEquipos;
	}

	public Practica getPractica() {
		return practica;
	}

	public void setPractica(Practica practica) {
		this.practica = practica;
	}
	
	public ActividadPractica getActividadPractica() {
		return actividadPractica;
	}

	public void setActividadPractica(ActividadPractica actividadPractica) {
		this.actividadPractica = actividadPractica;
	}

	public List<EquipoTecnico> getEquipoTecnico() {
		return equipoTecnico;
	}

	public void setEquipoTecnico(List<EquipoTecnico> equipoTecnico) {
		this.equipoTecnico = equipoTecnico;
	}

	public List<Actividad> getListaActividades() {
		return listaActividades;
	}

	public void setListaActividades(List<Actividad> listaActividades) {
		this.listaActividades = listaActividades;
	}

	public List<Area> getListaAreas() {
		return listaAreas;
	}

	public void setListaAreas(List<Area> listaAreas) {
		this.listaAreas = listaAreas;
	}
	
	public List<ActividadPractica> getListaActividadPracticas() {
		return listaActividadPracticas;
	}

	public void setListaActividadPracticas(
			List<ActividadPractica> listaActividadPracticas) {
		this.listaActividadPracticas = listaActividadPracticas;
	}
	
	public List<Tecnico> getListaTecnicos() {
		return listaTecnicos;
	}

	public void setListaTecnicos(List<Tecnico> listaTecnicos) {
		this.listaTecnicos = listaTecnicos;
	}

	public Tecnico getTecnico() {
		return tecnico;
	}

	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	public Date getFechaAnterior() {
		return fechaAnterior;
	}

	public void setFechaAnterior(Date fechaAnterior) {
		this.fechaAnterior = fechaAnterior;
	}

	public String getMensajeComboArea() {
		return mensajeComboArea;
	}

	public void setMensajeComboArea(String mensajeCombo) {
		this.mensajeComboArea = mensajeCombo;
	}
	
	public String getMensajeComboTecnico() {
		return mensajeComboTecnico;
	}

	public void setMensajeComboTecnico(String mensajeComboTecnico) {
		this.mensajeComboTecnico = mensajeComboTecnico;
	}

	public String getMensajeComboActividad() {
		return mensajeComboActividad;
	}

	public void setMensajeComboActividad(String mensajeCombo) {
		this.mensajeComboActividad = mensajeCombo;
	}
	
	public String getMensajeComboEquipo() {
		return mensajeComboEquipo;
	}

	public void setMensajeComboEquipo(String mensajeComboEquipo) {
		this.mensajeComboEquipo = mensajeComboEquipo;
	}

	public boolean isDesHabilitarEncabezado() {
		return desHabilitarEncabezado;
	}

	public void setDesHabilitarEncabezado(boolean desHabilitar) {
		this.desHabilitarEncabezado = desHabilitar;
	}

	public boolean isVisibleTraerInformacion() {
		return visibleTraerInformacion;
	}

	public void setVisibleTraerInformacion(boolean visibleTraerInformacion) {
		this.visibleTraerInformacion = visibleTraerInformacion;
	}

	@Init
	public void init(@ExecutionArgParam("id") Integer idPractica){
		
		if ((idPractica==null)||(idPractica==0)){
		   this.tipoOperacion=1;
		   practica = new Practica(new Date(),new Date(),"","",new Date(),(short)0,new Equipo(),new HashSet<ActividadPractica>(),new HashSet<AsistenciaAtleta>());
		   visibleTraerInformacion=true;
		}else{
			this.tipoOperacion=2;
			practica=servicioPractica.buscarPorId(idPractica);
			equipoTecnico = servicioPractica.buscarEquipoTecnico(practica.getEquipo());
			tecnico= tecnicoPrincipal(equipoTecnico);
			mensajeComboEquipo = practica.getEquipo().getNombre();
			if (practica.getActividadesPractica().size()<=0){
				this.desHabilitarEncabezado=false;
			}else{
				this.desHabilitarEncabezado=true;
			}
		}
		actividadPractica = new ActividadPractica(new Date(),new Date(),false,"",new Practica(),new Actividad(),new Area(),new HashSet<CambioTecnico>());
		listaEquipos= servicioPractica.buscarTodosEquipo(); 
		listaAreas = servicioPractica.buscarTodosArea();
		listaActividades = servicioPractica.buscarTodosActividad();
		listaTecnicos = servicioPractica.buscarTecnicos();
	}
	
	@Command
	@NotifyChange({"practica","actividadPractica","mensajeComboActividad","mensajeComboArea","mensajeComboTecnico"})
	public void traerInformacionAnterior(){
		if (!mensajeComboEquipo.equalsIgnoreCase("Seleccione Equipo")){
			if (!fechaAnterior.equals(practica.getFecha())){
				Set<ActividadPractica> actividadesPracticas = new HashSet<ActividadPractica>(servicioPractica.traerActividadesAnteriores(fechaAnterior, practica));
		       practica.setActividadesPractica(actividadesPracticas);
			}else{
				Messagebox.show(
		                "No se puede traer informacion de la misma fecha de esta practica","", Messagebox.OK, Messagebox.ERROR
		        );
			}
		}
		actividadPractica = new ActividadPractica(horaSiguienteValida(),practica.getHoraFin(),false,"",new Practica(),actividadPractica.getActividad(),actividadPractica.getArea(),new HashSet<CambioTecnico>());
        mensajeComboActividad = mensajeComboArea = "Seleccione";
        tecnico = tecnicoPrincipal(equipoTecnico);
		mensajeComboTecnico = concatenar(tecnico.getNombre(),tecnico.getApellido());
	}
	
	//deberia ser servicio
	private Tecnico tecnicoPrincipal(List<EquipoTecnico> equipoTecnico){
		Tecnico principal = new Tecnico();
		if (equipoTecnico.size()>0){
		   if (equipoTecnico.size()>1){
		      if (equipoTecnico.get(0).getResponsabilidad()==1){
			     principal = equipoTecnico.get(0).getTecnico();
			  }else{
				 principal = equipoTecnico.get(1).getTecnico();
			  }
		   }else{
		      principal = equipoTecnico.get(0).getTecnico();
		   }
		}
		return principal;
		
	}
	
	
	@Command
	@NotifyChange({"tecnico","practica","mensajeComboTecnico"})
	public void buscarInformacionEquipo(){
		HorarioPractica horario = servicioPractica.obtenerHorarioPracticaEquipo(practica.getEquipo(), practica.getFecha());
		if (horario!=null){
		   practica.setHoraInicio(horario.getHoraInicio());
		   practica.setHoraFin(horario.getHoraFin());
		}
		
		equipoTecnico = servicioPractica.buscarEquipoTecnico(practica.getEquipo());
		if (equipoTecnico.size()>0){
			if (equipoTecnico.size()>1){
		       tecnico = tecnicoPrincipal(equipoTecnico);
			}else{
				tecnico = equipoTecnico.get(0).getTecnico();
			}
		}else{
			tecnico = listaTecnicos.get(0);
		}
		mensajeComboTecnico = concatenar(tecnico.getNombre(),tecnico.getApellido());
		
	}
	
	@Command
	@NotifyChange({"practica"})
	public void seleccionarFecha(){
		if (!mensajeComboEquipo.equalsIgnoreCase("Seleccione Equipo")){
			HorarioPractica horario = servicioPractica.obtenerHorarioPracticaEquipo(practica.getEquipo(), practica.getFecha());
			if (horario!=null){
			   practica.setHoraInicio(horario.getHoraInicio());
			   practica.setHoraFin(horario.getHoraFin());
			}
		}
	}
	
	@Command
	@NotifyChange({"actividadPractica"})
	public void seleccionarArea(){
		
		actividadPractica.setHoraInicio(horaSiguienteValida());
		actividadPractica.setHoraFin(practica.getHoraFin());
	
	}
	
	private Date horaSiguienteValida(){
		List <ActividadPractica> actividades = new ArrayList<ActividadPractica>(practica.getActividadesPractica());
		Date mayor = practica.getHoraInicio();
		for(int i=0; i< actividades.size();i++){
			if (actividades.get(i).getHoraFin().compareTo(mayor)>0){
				mayor = actividades.get(i).getHoraFin();
			}
		}
		if (practica.getActividadesPractica().size() > 0){
			 Calendar calendar = Calendar.getInstance(); //esto es para sumar un minuto a la hora inicio 
			 calendar.setTime(mayor);    //de la siguiente actividad
			 calendar.add(Calendar.MINUTE, 1);
			 mayor = calendar.getTime();
		}
			 
		return mayor;
	}
	
	private boolean rangoValido(Date horaInicial, Date horaFinal, Date horaInicialValida, Date horaFinalValida){
	 
		boolean valido = true;
		if ((horaInicial.compareTo(horaInicialValida)<0 || horaInicial.compareTo(horaFinalValida)>0)
		|| 	(horaFinal.compareTo(horaInicialValida)<0 || horaFinal.compareTo(horaFinalValida)>0)){
			valido = false;
		}
		return valido;
	}
	
	private boolean rangoPermitido(Date horaInicial, Date horaFinal, Date horaInicialValida, Date horaFinalValida){
		 
		boolean valido = true;
		if ((horaInicial.compareTo(horaInicialValida)>=0 && horaInicial.compareTo(horaFinalValida)<=0)
		|| 	(horaFinal.compareTo(horaInicialValida)>=0 && horaFinal.compareTo(horaFinalValida)<=0)){
			valido = false;
		}
		return valido;
	}
	
	private boolean rangoActividadDisponible (Date horaInicial, Date horaFinal){
		boolean valido = true;
		int i =0;
		List <ActividadPractica> actividades = new ArrayList<ActividadPractica>(practica.getActividadesPractica());
		while (valido==true && i < actividades.size()){
			valido = rangoPermitido(horaInicial, horaFinal,actividades.get(i).getHoraInicio(),actividades.get(i).getHoraFin());
			i++;
		}
		return valido;
	}
	
	private boolean tienePracticaPrevia(Date hora){
		boolean valido = servicioPractica.isEquipoPracticaFechaHora(practica.getEquipo(), practica.getFecha(), hora, practica.getId());
		return valido;
	}
	
	private boolean tieneJuegoPrevio(Date hora){
		boolean valido = servicioPractica.isEquipoJuegoFechaHora(practica.getEquipo(), practica.getFecha(), hora);
		return valido;
	}
	
	private boolean areaOcupada(Date hora){
		boolean valido = servicioPractica.isAreaOcupadaFechaHora(actividadPractica.getArea(), practica.getFecha(), hora, practica.getId());
		return valido;
	}
	
	private boolean areaOcupadaJuego(Date hora){
		boolean valido = servicioPractica.isAreaOcupadaJuegoFechaHora(actividadPractica.getArea(), practica.getFecha(), hora);
		return valido;
	}
	
	private boolean isHorarioDisponible (ActividadPractica actividadPractica){
		int dia = diaDeLaSemana(practica.getFecha());
		return servicioPractica.isHorarioAreaDisponible(actividadPractica.getArea(), dia, actividadPractica.getHoraInicio(), 
														actividadPractica.getHoraFin());
	}
	
	private boolean validacionInterna(){
		boolean valido = true;
		 if (rangoValido(actividadPractica.getHoraInicio(),actividadPractica.getHoraFin(),
				                 practica.getHoraInicio(),practica.getHoraFin())==false){
			
			Messagebox.show(
	                "Horas de actividad no validas. La actividad debe efectuarse dentro de la hora de practica","", Messagebox.OK, Messagebox.ERROR
	            );
			return false;
		}else if (rangoActividadDisponible(actividadPractica.getHoraInicio(),actividadPractica.getHoraFin())==false){
			Messagebox.show(
	                "Horas de actividad no validas. La horas de una actividad no pueden coincidir con las horas de otra actividad","", Messagebox.OK, Messagebox.ERROR
	            );
			return false;
		}else{
			return valido;
		}
	}
	
	private boolean validacionExterna(){
		boolean valido = true;
		
		if (areaOcupada(actividadPractica.getHoraInicio())){

			Messagebox.show(
					"Horas de actividad no validas. El area que selecciono se encuentra ocupada a la hora de inicio de esta actividad","", Messagebox.OK, Messagebox.ERROR
					);
			return false;
		}else if (areaOcupada(actividadPractica.getHoraFin())){

			Messagebox.show(
					"Horas de actividad no validas. El area que selecciono se encuentra ocupada a la hora de finalizacion de esta actividad","", Messagebox.OK, Messagebox.ERROR
					);
			return false;
		}else if (areaOcupadaJuego(actividadPractica.getHoraInicio())){

			Messagebox.show(
					"Horas de actividad no validas. El area que selecciono se encuentra ocupada por un juego a la hora de inicio de esta actividad","", Messagebox.OK, Messagebox.ERROR
					);
			return false;
		}else if (isHorarioDisponible(actividadPractica)){
			Messagebox.show(
					"Horas de actividad no validas. El area que selecciono no se encuentra disponible en ese horario","", Messagebox.OK, Messagebox.ERROR
					);
			return false;
		}else{
			return valido;
		}
	}
	
	@Command
	@NotifyChange("practica")
	public void validarHorasPractica(){
		if (practica.getHoraInicio().compareTo(practica.getHoraFin())>0){
		   Messagebox.show(
	                "Horas de practica no validas. La hora fin de la practica debe ser posterior a la hora de inicio","", Messagebox.OK, Messagebox.ERROR
	            );
		   practica.setHoraInicio(practica.getHoraFin());
		}
	}
	
	@Command
	@NotifyChange("actividadPractica")
	public void validarHorasActividad(){
		if (actividadPractica.getHoraInicio().compareTo(actividadPractica.getHoraFin())>0){
		   Messagebox.show(
	                "Horas de actividad no validas. La hora fin de la actividad debe ser posterior a la hora de inicio","", Messagebox.OK, Messagebox.ERROR
	            );
		   actividadPractica.setHoraFin(practica.getHoraFin());
		   actividadPractica.setHoraInicio(horaSiguienteValida());
		}
	}
	
	@Command
	@NotifyChange("mensajeComboActividad")
	public void validarDisponibilidadEquipo(){
			if (tienePracticaPrevia(practica.getHoraInicio()) == true){
				Messagebox.show(
		                "Horas de practica no valida. El equipo tiene otra practica previa durante la hora de inicio que usted ha especificado","", Messagebox.OK, Messagebox.ERROR
		            );
				mensajeComboActividad ="Seleccione";
			}else if (tienePracticaPrevia(practica.getHoraFin()) == true){
				Messagebox.show(
		                "Horas de practica no valida. El equipo tiene otra practica previa durante la hora de fin que usted ha especificado","", Messagebox.OK, Messagebox.ERROR
		            );
				mensajeComboActividad ="Seleccione";
			}else if (tieneJuegoPrevio(practica.getHoraInicio()) == true){
				Messagebox.show(
		                "Horas de practica no valida. El equipo tiene un juego previo durante la hora de inicio que usted ha especificado","", Messagebox.OK, Messagebox.ERROR
		            );
				mensajeComboActividad ="Seleccione";
			}else if (tieneJuegoPrevio(practica.getHoraFin()) == true){
				Messagebox.show(
		                "Horas de practica no valida. El equipo tiene un juego previo durante la hora de fin que usted ha especificado","", Messagebox.OK, Messagebox.ERROR
		            );
				mensajeComboActividad ="Seleccione";
			
			}else{
				
			}
	}
	//deberia ser servicio
	public String tecnicoEjecutor(ActividadPractica actividadPractica){
		String responsable="";
		List<CambioTecnico> cambios = new ArrayList<CambioTecnico>(actividadPractica.getCambioTecnicos());
		
		if (cambios.size()>0){
			responsable = concatenar(cambios.get(0).getTecnicoSuplente().getNombre(),cambios.get(0).getTecnicoSuplente().getApellido());
		}else{
			List<EquipoTecnico> equiposTec = servicioPractica.buscarEquipoTecnico(practica.getEquipo());
			Tecnico elResponsable = tecnicoPrincipal(equiposTec);
			responsable = concatenar(elResponsable.getNombre(),elResponsable.getApellido());
		}
		return responsable;
	}
	
	@Command
	@NotifyChange({"practica","actividadPractica","mensajeComboActividad", "mensajeComboArea", "desHabilitarEncabezado","mensajeComboTecnico"})
	public void agregarActividadPractica(){
		if (!mensajeComboActividad.equals("Seleccione") && !mensajeComboArea.equals("Seleccione") && !mensajeComboEquipo.equals("Seleccione Equipo")){
		   if (validacionExterna()){
		      if (validacionInterna()){
		            ActividadPractica nuevaActividad = new ActividadPractica(actividadPractica.getHoraInicio(),actividadPractica.getHoraFin(),
                   							   true,actividadPractica.getObservacion(),practica,
                							   actividadPractica.getActividad(),actividadPractica.getArea(),actividadPractica.getCambioTecnicos());
	
		         if (!tecnicoPrincipal(equipoTecnico).equals(tecnico)){
		        	if (equipoTecnico.size()>0){
		        	   for(int i=0;i<equipoTecnico.size();i++){
			              nuevaActividad.getCambioTecnicos().add(new CambioTecnico(nuevaActividad,equipoTecnico.get(i),tecnico));
		               }
		        	}else{
		        		//si no tiene equipo tecnico
		        		   nuevaActividad.getCambioTecnicos().add(new CambioTecnico(nuevaActividad,null,tecnico));
		        	}
		         }
		
		         practica.getActividadesPractica().add(nuevaActividad);
		         this.desHabilitarEncabezado=true;
		
		         actividadPractica = new ActividadPractica(horaSiguienteValida(),practica.getHoraFin(),false,"",new Practica(),actividadPractica.getActividad(),actividadPractica.getArea(),new HashSet<CambioTecnico>());
		         mensajeComboActividad = mensajeComboArea = "Seleccione";
		         tecnico = tecnicoPrincipal(equipoTecnico);
		         //si no tiene tecnico principal, agarre al suplente, si tampoco tiene, coloque uno de la lista
		         if (tecnico.getNombre()==null){
		        	 
		        	 if (equipoTecnico.size()>0){
		        		 tecnico = equipoTecnico.get(0).getTecnico();
		        	 }else{
		        	    tecnico = listaTecnicos.get(0);
		        	 }
		         }
		 		 mensajeComboTecnico = concatenar(tecnico.getNombre(),tecnico.getApellido());
		        // tecnico = new Tecnico();
		      } // if validacion interna
		   } // if validacion externa 
		} 
	
	}
	
	@Command 
	@NotifyChange({"practica","desHabilitarEncabezado","tecnico"})
	public void removerActividadPractica(@BindingParam("actividadPractica") ActividadPractica actividadPractica){
		practica.getActividadesPractica().remove(actividadPractica);
		tecnico = tecnicoPrincipal(equipoTecnico);
		if (practica.getActividadesPractica().size()<=0){
			this.desHabilitarEncabezado=false;
		}
	}
	
	@Command
	public void guardar(){
		if (!mensajeComboEquipo.equals("Seleccione Equipo") && practica.getActividadesPractica().size()>0){
		   servicioPractica.guardar(practica,this.tipoOperacion);
		   Messagebox.show(
                "Planificacion guardada exitosamente","", Messagebox.OK, Messagebox.INFORMATION
            );
		   //para cerrar la ventana despues que guarda
		   Window myWindow = (Window) Path.getComponent("/planificacionpractica");
		   myWindow.detach();
		}else{
			Messagebox.show(
	                "No se puede guardar. Debe llenar correctamente los campos y planificar al menos una actividad","", Messagebox.OK, Messagebox.ERROR
	        );
		}
		
	}
	
	@Command
	@NotifyChange({"desHabilitarEncabezado","mensajeComboEquipo","practica","listaEquipos","listaAreas","listaActividades","mensajeComboTecnico"})
	public void cancelar(){
		desHabilitarEncabezado=false;
		mensajeComboEquipo="Seleccione Equipo";
		mensajeComboTecnico="Seleccione";
		init(practica.getId());
	}
	
	public String concatenar(String cadena1, String cadena2){
		return cadena1+" "+cadena2;
	}
	
	public String formatearHora(Date fecha){
		String fechaFormateada= new SimpleDateFormat("hh:mm a").format(fecha);
		return fechaFormateada; 
	}
	
	public String formatearFecha(Date fecha){
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
	
	private static int diaDeLaSemana(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);		
	}


	
	
}
