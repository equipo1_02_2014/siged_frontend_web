package edu.ucla.siged.viewmodel.seguridad;

import java.io.IOException;
import java.util.ArrayList;





import java.util.LinkedList;

import org.hibernate.Hibernate;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Image;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;





import org.zkoss.zul.impl.InputElement;










import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMCambioPassword {
	@WireVariable
	ServicioUsuarioI servicioUsuario; // Instancia la interface servicio	
	
	@WireVariable
	Usuario usuario;
	Usuario usuarioSesion;
	@WireVariable
	private ArrayList<String> listaPreguntas;
	private AImage imagenT;
	private String passwordAnterior;
	private String passwordNuevo;
	private String confirmacionPasswordNuevo;
	private String password;
	public VMCambioPassword() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            		 @ExecutionArgParam("usuarioSesion") Usuario usuarioSesion) {
		passwordAnterior="";
		passwordNuevo="";
		confirmacionPasswordNuevo="";
		
		this.usuarioSesion=usuarioSesion;
		 listaPreguntas= new ArrayList<String>();
         listaPreguntas.add("Nombre de tu primera mascota");
         listaPreguntas.add("Nombre de tu papá");
         listaPreguntas.add("Nombre de tu mamá");
         listaPreguntas.add("Tu color favorito");
         listaPreguntas.add("Nombre de tu primer juguete");
                 
         Selectors.wireComponents(view, this, false);
		System.out.println("------------------------------------ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        if (usuarioSesion!=null){
        	
        	
            this.usuario = usuarioSesion;
            //this.preguntaSeleccionada= this.usuario.getPreguntaSecreta();
            if(usuarioSesion.getFoto()!=null){
				try{
					imagenT = new AImage(usuario.getFoto().getNombreArchivo(), usuario.getFoto().getContenido());
					
				}catch(IOException e){
				}
            }
        }else{
        	Messagebox.show("Ha ocurrido un error!", "Información", Messagebox.OK, Messagebox.INFORMATION);}

        
       }      
    
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public AImage getImagenT() {
		return imagenT;
	}

	public void setImagenT(AImage imagenT) {
		this.imagenT = imagenT;
	}
		

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	/*@Command
	public void generarPassword() {
		String base="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$";
		int longitud = base.length();
		final int LARGO_PASWWORD=8;
		password="";
		for(int i=0; i<LARGO_PASWWORD;i++){ 
		    int numero = (int)(Math.random()*(longitud)); 
		    String caracter=base.substring(numero, numero+1); 
		    password+=caracter; 
		}
		System.out.println("PASSWORD: "+password);
		usuario.setPassword(password);
	}*/
	
	public ArrayList<String> getListaPreguntas() {
		return listaPreguntas;
	}

	public void setListaPreguntas(ArrayList<String> listaPreguntas) {
		this.listaPreguntas = listaPreguntas;
	}

	public String getPasswordAnterior() {
		return passwordAnterior;
	}

	public void setPasswordAnterior(String passwordAnterior) {
		this.passwordAnterior = passwordAnterior;
	}

	public String getPasswordNuevo() {
		return passwordNuevo;
	}

	public void setPasswordNuevo(String passwordNuevo) {
		this.passwordNuevo = passwordNuevo;
	}

	public String getConfirmacionPasswordNuevo() {
		return confirmacionPasswordNuevo;
	}

	public void setConfirmacionPasswordNuevo(String confirmacionPasswordNuevo) {
		this.confirmacionPasswordNuevo = confirmacionPasswordNuevo;
	}

	@Command
	@NotifyChange({"usuario", "imagenT"})
	public void guardarPerfilUsuario(@SelectorParam(".usuarioRestriccion") LinkedList<InputElement> inputs) throws IOException{
		boolean camposValidos=true;
		for(InputElement input:inputs){
			if (!input.isValid()){
				System.out.println("+++++++++++++"+input.getId());
				camposValidos=false;
				break;
			}
		}
		if(!camposValidos){
			Messagebox.show("Por Favor Introduzca Campos Válidos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		System.out.println(usuario);
		if ( usuario.getEmail().equals("") || usuario.getPreguntaSecreta().equals("") || usuario.getRespuestaSecreta().equals("")
			||	usuario.getCelular().equals("") || usuario.getTelefono().equals(""))
	
			Messagebox.show("No pueden haber campos vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		
		else{
		
		
			
				if(!this.passwordAnterior.trim().isEmpty() && !this.passwordNuevo.trim().isEmpty()
						&& !this.confirmacionPasswordNuevo.trim().isEmpty())
				{
					if(this.confirmacionPasswordNuevo.equals(this.passwordNuevo) && this.passwordAnterior.equals(usuario.getPassword()))
					{	usuario.setPassword(this.passwordNuevo);
						servicioUsuario.guardar(usuario);	
					}
					else{
					if(!this.confirmacionPasswordNuevo.equals(this.passwordNuevo))
						Messagebox.show("No coinciden el password nuevo con la confirmación del password", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
					else{
					if(!this.passwordAnterior.trim().equals(usuario.getPassword().trim()))
						Messagebox.show(passwordAnterior+"No coincide el password anterior/" +usuario.getPassword() , "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				
					}
					return;
					}
				}
			
				servicioUsuario.guardar(usuario);		
				Messagebox.show("Datos almacenados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				Window myWindow = (Window) Path.getComponent("/vistaCambioPassword");
				myWindow.detach();
				//myWindow = (Window) Path.getComponent("/vistaLogin");
				
				//Events.sendEvent(new Event("onClick", (Button)((Window)myWindow.getParent()).getFellow("btnrefresh") ));
				System.out.println("guardo el usuario");
				Session sess = Sessions.getCurrent();
			    sess.setAttribute("userCredential",usuario);
				 if(usuario.getFoto()!=null){
					    try {
					    	 AImage imagenT;
							imagenT = new AImage(usuario.getFoto().getNombreArchivo(), usuario.getFoto().getContenido());
							sess.setAttribute("foto",imagenT);
							sess.setAttribute("tieneFoto",true);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
							sess.setAttribute("tieneFoto",false);
						}
				}
			    else
			    	sess.setAttribute("tieneFoto",false);
				
				
			
			}
		
	}
		
	@Command
	@NotifyChange({"usuario", "passwordNuevo", "passwordAnterior", "confirmacionPasswordNuevo"})
		public void limpiarPerfilUsuario(){
	
			this.usuario= servicioUsuario.findById(usuario.getId());
			passwordAnterior="";
			passwordNuevo="";
			confirmacionPasswordNuevo="";
			
			
		}
	
	@Command
	@NotifyChange({"usuario","imagenT"})
	public void subirImagen(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media= event.getMedia();
		if(media != null){
			if(media instanceof org.zkoss.image.Image){
				Archivo foto = new Archivo();
				foto.setNombreArchivo(media.getName());
				foto.setTipo(media.getContentType());
				foto.setContenido(media.getByteData());
				usuario.setFoto(foto);
				imagenT = (AImage) media;
			}else{
				
			}
		}
	}
	

	
}