package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacionRecurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioEvento;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioRecurso;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMDonacion {
	
	@WireVariable
	private ServicioDonacion servicioDonacion;
	
//	@WireVariable
//	private ServicioEvento servicioEvento;
	
	@WireVariable
	private Donacion donacion = new Donacion();
	
//	@WireVariable
//	private List<Evento> eventos = new ArrayList<Evento>();
//	
//	@WireVariable
//	private Evento eventoSeleccionado;
	
	@WireVariable
	private Set<DonacionRecurso> recursos; // = new HashSet<DonacionRecurso>();

	
	/*********************************/
	@WireVariable
	private ServicioDonacionRecurso servicioDonacionRecurso;
	
	@WireVariable
	private ServicioRecurso servicioRecurso;
	
	@WireVariable
	private DonacionRecurso donacionRecurso; // = new DonacionRecurso();
	
	@WireVariable
	private List<Recurso> comboxRecursos;
	
	@WireVariable
	private Set<DonacionRecurso> recursosPorEliminar = new HashSet<DonacionRecurso>();
	
	@WireVariable
	private Set<DonacionRecurso> recursosSeleccionados = new HashSet<DonacionRecurso>();
	/*********************************/
	
	public VMDonacion(){
		
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("donacion") Donacion donacionSeleccionada){
		Selectors.wireComponents(view, this, false);
		
		/*********************************/
		this.donacionRecurso = new DonacionRecurso();
		this.comboxRecursos = servicioRecurso.ListarRecursos();
		/*********************************/
		
		if(donacionSeleccionada!=null){
			this.donacion = donacionSeleccionada;
//			this.eventoSeleccionado = donacion.getEvento();
			this.recursos = this.donacion.getRecursosDonados();
		}
		else{
			this.donacion = new Donacion();
			this.recursos = new HashSet<DonacionRecurso>();
		}
		
//		this.eventos = servicioEvento.ListarEventos();
	}

	public Donacion getDonacion() {
		return donacion;
	}

	public void setDonacion(Donacion donacion) {
		this.donacion = donacion;
	}
	
//	public List<Evento> getEventos() {
//		return eventos;
//	}
//
//	public void setEventos(List<Evento> eventos) {
//		this.eventos = eventos;
//	}
//
//	public Evento getEventoSeleccionado() {
//		return eventoSeleccionado;
//	}
//
//	public void setEventoSeleccionado(Evento eventoSeleccionado) {
//		this.eventoSeleccionado = eventoSeleccionado;
//	}

	public Set<DonacionRecurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(Set<DonacionRecurso> recursos) {
		this.recursos = recursos;
	}
	
	/*********************************/
	public DonacionRecurso getDonacionRecurso() {
		return donacionRecurso;
	}

	public void setDonacionRecurso(DonacionRecurso donacionRecurso) {
		this.donacionRecurso = donacionRecurso;
	}

	public List<Recurso> getComboxRecursos() {
		return comboxRecursos;
	}

	public void setComboxRecursos(List<Recurso> comboxRecursos) {
		this.comboxRecursos = comboxRecursos;
	}
	
	public Set<DonacionRecurso> getRecursosSeleccionados() {
		return recursosSeleccionados;
	}

	public void setRecursosSeleccionados(Set<DonacionRecurso> recursosSeleccionados) {
		this.recursosSeleccionados = recursosSeleccionados;
	}

	/*********************************/

	@Command
	@NotifyChange({"donacion","recursos"})
	public void GuardarDonacion(@SelectorParam("textbox,datebox,timebox") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		
		for(InputElement input:inputs){
			
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
			
		}
		
		if(!camposValidos){
			Messagebox.show("Los campos con (*) no pueden estar vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else {
			if(!this.recursos.isEmpty()){
				this.donacion.setRecursosDonados(recursos);
				try{
//					if(this.eventoSeleccionado != null){
//						this.donacion.setEvento(eventoSeleccionado);
//					}
//					if(donacion.getEstatus() == null){
						donacion.setEstatus((short)4);
//					}
//					if(donacion.getTipo() == null){
						donacion.setTipo((short)0);
//					}
					
					servicioDonacion.Guardar(donacion);
					System.out.println("******************");
					System.out.println(donacion.getEstatus());
					System.out.println("******************");
					Messagebox.show("Datos almacenados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
					/*****************************/
					if(!recursosPorEliminar.isEmpty()){
						servicioDonacionRecurso.EliminarVarios(recursosPorEliminar);
					}
					/*****************************/
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				Limpiar();
				Window myWindow = (Window) Path.getComponent("/id_donacionh");
				myWindow.detach();
			}else{
				Messagebox.show("No se han cargado recursos a la donaci�n", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}
	}
	
	@Command
	@NotifyChange({"donacion","recursos"})
	public void Limpiar(){
		this.donacion = new Donacion();
//		this.eventoSeleccionado = null;
		this.recursos = new HashSet<DonacionRecurso>();
		this.donacionRecurso = new DonacionRecurso();
		this.recursosPorEliminar = new HashSet<DonacionRecurso>();
	}
	

	@Command
	@NotifyChange({"donacion","recursos","donacionRecurso"})
	public void CargarRecursos(){
		if(donacionRecurso.getRecurso() != null){
			if(!RecursoContenido(donacionRecurso)){
				this.recursos.add(donacionRecurso);
			}else{
				Messagebox.show("El recurso ya fue seleccionado", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}else{
			Messagebox.show("No ha seleccionado un recursos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		LimpiarRecurso();
	}
	
	//terorna true si el recurso ya existe de lo contrario retorna false
	private boolean RecursoContenido(DonacionRecurso dr){
		for(DonacionRecurso aux:recursos){
			if(aux.getRecurso().getId()==dr.getRecurso().getId())
				return true;
		}
		return false;
	}
	
	@NotifyChange({"donacion","donacionRecurso"})
	public void LimpiarRecurso() {
		donacionRecurso = new DonacionRecurso();
	}
	
	@Command
	public String VerTipoDonacion(Integer aux){
		String est="Entrante";
		switch (aux){
			case 1:
				est="Saliente";
			    break;
			default:
				est="Entrante";
		}
		return est;
	}
	
	@Command
	public String VerEstatusDonacion(Integer aux){
		String est="Solicitada";
		switch (aux){
			case 1:
				est="Aprobada";
			    break;
			case 2:
				est="Rechazada";
			    break;
			case 3:
				est="Entregada";
			    break;
			case 4:
				est="Recibida";
			    break;
			default:
				est="Solicitada";
		}
		return est;
	}
	
	@Command
	@NotifyChange({"recursos"})
	public void EliminarRecurso(@BindingParam("recursoSeleccionado") DonacionRecurso recursoSeleccionado){
		/******************/
		recursosPorEliminar.add(recursoSeleccionado);
		/*****************/
		getRecursos().remove(recursoSeleccionado);
		Messagebox.show("Recurso eliminado correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({"recursos"})
	public void EliminarRecursos(){
		if(recursosSeleccionados != null){
			for(DonacionRecurso dr : recursosSeleccionados)
			{
				recursosPorEliminar.add(dr);
			}
			getRecursos().removeAll((recursosSeleccionados));
			Messagebox.show("Recursos eliminados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay recursos seleccionados", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
	}

}
