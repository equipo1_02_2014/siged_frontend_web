/** 
 * 	VMTipoCompetencia
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestiondeportiva;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioTipoCompetenciaI;

//Declaraci�n de la clase VMTipoCompetencia con sus atributos:
public class VMTipoCompetencia {
	@WireVariable
	ServicioTipoCompetenciaI servicioTipoCompetencia;
	@WireVariable
	TipoCompetencia tipoCompetencia;
	private Window ventanaTipoCompetencia;
	List<Short> listaEstatus;
	Short estatusSeleccionado;
	short tipoOperacion;//1: No existe ning�n objeto, 2: Ya existe un objeto.
	
	//M�todos Get y Set de la clase:
	public TipoCompetencia getTipoCompetencia() {
		return tipoCompetencia;
	}
	
	public void setTipoCompetencia(TipoCompetencia tipoCompetencia) {
		this.tipoCompetencia = tipoCompetencia;
	}

	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Short> getListaEstatus() {
		return listaEstatus;
	}
    
	//M�todo para inicializar el objeto tipoCompetencia:
	@Autowired
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("tipoCompetencia") TipoCompetencia tipoCompetenciaSeleccionada) {
            listaEstatus= new ArrayList<Short>();
            listaEstatus.add((short) 0);
            listaEstatus.add((short) 1);
            estatusSeleccionado=1;
            if (tipoCompetenciaSeleccionada!=null){
            this.tipoOperacion=2; //En el caso de que ya exista un objeto, tipoOperacion=2.
            this.tipoCompetencia = tipoCompetenciaSeleccionada;
            this.estatusSeleccionado= tipoCompetenciaSeleccionada.getEstatus();
        }
        else{
        	this.tipoOperacion=1; //En el caso de que haya que crear un nuevo objeto, tipoOperacion=1.
        	tipoCompetencia = new TipoCompetencia();
            }
        }
	
	//M�todo para guardar uno o varios objetos del tipo TipoCompetencia:
	@Command
	@NotifyChange({"tipoCompetencia","limpiar","actualizarLista","estatusSeleccionado"})
	public void guardar(@SelectorParam(".tipo_competencia_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaTipoCompetencia") Window ventanaTipoCompetencia){
        this.ventanaTipoCompetencia = ventanaTipoCompetencia;
         
		if (this.estatusSeleccionado!=null)
			tipoCompetencia.setEstatus(estatusSeleccionado);
		
        boolean camposValidos=true;
		
		//M�todo para validar si los campos est�n vac�os o no:
        for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
			break;
			}
		}
      //M�todo para preguntar si los campos est�n vac�os o no:
		if(!camposValidos || tipoCompetencia.getDescripcion()==null || tipoCompetencia.getDescripcion().isEmpty()){
			Messagebox.show("Debe completar todos los datos para continuar.","",Messagebox.OK, Messagebox.EXCLAMATION);
		}
		
		else
		{
			if(this.tipoOperacion==1) //En el caso de que haya que crear un nuevo objeto.
			{
			servicioTipoCompetencia.guardar(tipoCompetencia);
			
			Messagebox.show("El registro se ha guardado exitosamente.","",Messagebox.OK, Messagebox.INFORMATION);
			     
	       			Messagebox.show("�Desea agregar otro registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
	       					new EventListener<Event>() {
	       				
	       						public void onEvent(Event event) throws Exception {
	                                
	       							if(Messagebox.ON_NO.equals(event.getName())){
	       								
	       								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       								cerrarVentanaTipoCompetencia();
	       								
	       							}
	       							else BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       						}
	       					});
	            limpiar();
			}
			
			if(this.tipoOperacion==2) //En el caso de ya exista un objeto.
			{
			servicioTipoCompetencia.guardar(tipoCompetencia);
			Messagebox.show("Los cambios han sido guardados con �xito.","",Messagebox.OK, Messagebox.INFORMATION);
			cerrarVentanaTipoCompetencia();
			}
			
			}

	}
	
	//M�todo para cerrar el formulario:
	public void cerrarVentanaTipoCompetencia(){
		ventanaTipoCompetencia.detach();
	}
	
	//M�todo para limpiar los campos del formulario:
	@Command
	@NotifyChange({"tipoCompetencia","estatusSeleccionado"})
	public void limpiar(){
	tipoCompetencia.setId(null);
    tipoCompetencia.setDescripcion("");
    estatusSeleccionado=1;
	}
	
}