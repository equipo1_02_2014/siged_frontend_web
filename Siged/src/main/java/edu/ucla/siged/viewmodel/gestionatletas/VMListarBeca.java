package edu.ucla.siged.viewmodel.gestionatletas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.servicio.impl.gestionatletas.ServicioAtleta;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMListarBeca {
	
	@WireVariable
	private ServicioAtleta servicioAtleta;
	
	private List<Atleta> atletas = new ArrayList<Atleta>();
	
	public Window window;
	
	private int paginaActual=0;
	private int tamanoPagina =4;
	private long registrosTotales;
	
	// las variables siguientes son del filtro
	private String nombre="";
	private String ci="";
	
	private boolean estatusFiltro;
	
	public VMListarBeca() {
		
	}

	public List<Atleta> getAtletas() {
		return atletas;
	}
	
	public void setAtletas(List<Atleta> atletas) {
		this.atletas = atletas;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	@Init
	public void init() {
		this.atletas = servicioAtleta.ListarAtletasSinBeca(this.paginaActual,4);
		this.registrosTotales= servicioAtleta.AtletasSinBeca();
	}
	
	@Command
	@GlobalCommand
	@NotifyChange({"atletas","paginaActual","registrosTotales"})
	public void paginarBecas(){
		if(estatusFiltro==false){
			this.atletas = servicioAtleta.ListarAtletasSinBeca(this.paginaActual,4);
			this.registrosTotales= servicioAtleta.AtletasSinBeca();
		}else{
			ejecutarFiltro();
		}
		
	}
	
	@Command
	@NotifyChange({"atletas","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		this.estatusFiltro=true;  //el filtro se ha activado
		
		jpql = " cedula like '%"+ci+"%' and nombre like '%"+nombre+"%'";
		this.atletas=servicioAtleta.ListarAtletasFiltradosSinBeca(jpql,this.paginaActual, 4);
		this.registrosTotales=servicioAtleta.AtletasFiltradosSinBeca(jpql);
	}
	
	@Command
	@NotifyChange({"atletas","registrosTotales","paginaActual","ci","nombre"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginarBecas();
		ci="";
		nombre="";
	}

	@Command
	public void abrirVentanaBeca(@BindingParam("atletaSeleccionado") Atleta atletaSeleccionado){
		Map<String,Atleta> mapAtleta = new HashMap<String, Atleta>();
		mapAtleta.put("atleta", atletaSeleccionado);
		window = (Window)Executions.createComponents("/vistas/gestionatleta/beca.zul", null, mapAtleta);
		window.doModal();
	}
}
