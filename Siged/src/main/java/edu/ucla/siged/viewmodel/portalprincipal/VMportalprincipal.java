package edu.ucla.siged.viewmodel.portalprincipal;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.image.AImage;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMportalprincipal {
	Window win = null;
	@WireVariable
	ServicioUsuarioI servicioUsuario; // Instancia la interface servicio		

	private String account="";
	private String password="";
	
	@Init
	public void init() {
		this.account="";
		this.password="";
	    	
	    
	}
	
	// ---------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------- MENU HORIZONTAL --------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------------
	@Command
	public void quienesSomos() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/quienesSomos.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void requisitosAtleta() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/requisitosAtleta.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void ubicacionMapa() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/ubicacionMapa.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void galeriaFundacion() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/galeriaFundacion.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void contactanos() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/contactanos.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	
	// ---------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------- COLUMNA IZQUIERDA ------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------------
	@Command
	public void verCalendarioJuegos() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/gestiondeportiva/listajuegosfuturos.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void verCalendarioEventos() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/gestionevento/listaEventosFuturos.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	
	// ---------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------- CUERPO CENTRAL ------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------------------------
	@Command
	public void solicitarDonacion() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/solicitarDonacion.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void mostrarNoticias() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/mostrarNoticias.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	@Command
	public void postularAtleta() {
		if (win != null) {
			win.detach();
		}
		win = (Window) Executions.createComponents("/vistas/principal/portalPrincipal/ventanasModal/postulante.zul",null, null);
		win.setMaximizable(true);
		win.doModal();
	}
	
	//Metodos del iniciar sesion
		@Command
		public void abrirRecuperarPassword() {
		Window window;	
		Map<String,String> mapUsuario = new HashMap<String, String>();
		window = (Window)Executions.createComponents("/vistas/seguridad/recuperapassword.zul", null, mapUsuario);
		window.doModal();
		}
		
		@Command
		public void iniciarSesion() {
			String nm = "h";
			  String pd = "123";
			    Usuario user = servicioUsuario.buscarByNombreUsuario("xx");
				if(user==null || !user.getPassword().equals(pd)){
				Messagebox.show("Usuario o clave incorrectos", "Error", Messagebox.OK, Messagebox.ERROR);
				return;
				}     
			    Executions.sendRedirect("/");	
		 
		}
		
		@Command
		public void doLogin(){
		    String nm = account;
		    String pd = password;
		   
		    Usuario user = servicioUsuario.buscarByNombreUsuario(nm);
		    //a simple plan text password verification
		    if(user==null || !user.getPassword().equals(pd) || user.getEstatus()==0){
		    	if(user!=null && user.getEstatus()==0)
		    		Messagebox.show("Usuario Inactivo", "Error", Messagebox.OK, Messagebox.ERROR);
		    	else
		    		Messagebox.show("Usuario o clave incorrectos", "Error", Messagebox.OK, Messagebox.ERROR);
		        return;
		    }
		    Session sess = Sessions.getCurrent();
		    sess.setAttribute("userCredential",user);
		    String name="";
		    if( user.getNombre().indexOf(' ')!=-1)
		    	name=user.getNombre().substring(0, user.getNombre().indexOf(' '));
		    else
		    	name=user.getNombre();	
		    
		    sess.setAttribute("name",name);
		    if(user.getFoto()!=null){
		    try {
		    	 AImage imagenT;
				imagenT = new AImage(user.getFoto().getNombreArchivo(), user.getFoto().getContenido());
				sess.setAttribute("foto",imagenT);
				sess.setAttribute("tieneFoto",true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				sess.setAttribute("tieneFoto",false);
			}
		    }
		    else
		    	sess.setAttribute("tieneFoto",false);
		    Executions.sendRedirect("/");
		     
		}


		@Command
		public void doLogout(){
			Session sess = Sessions.getCurrent();
			sess.removeAttribute("userCredential");
			sess.removeAttribute("roles");
			Executions.sendRedirect("/portalPrincipal.zul");
		}

		
		public String getAccount() {
			return account;
		}
		public void setAccount(String account) {
			this.account = account;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
		@Command
		public void recuperarPassword() {
			try{
				Borderlayout borderlayout = (Borderlayout) Path.getComponent("/borderLayauttree");
				Center center = borderlayout.getCenter();
				center.getChildren().clear();
				Map<String,String> mapUsuario = new HashMap<String, String>();
				mapUsuario.put("nombreUsuario", this.account);
				Executions.createComponents("/vistas/seguridad/recuperapassword.zul", center, mapUsuario);
			}catch(Exception e){
				Messagebox.show(e.toString());
			}		
		}
}