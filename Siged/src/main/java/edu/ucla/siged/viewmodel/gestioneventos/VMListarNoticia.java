package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioNoticia;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMListarNoticia {
	
	@WireVariable
	private ServicioNoticia servicioNoticia;

	List<Noticia> listaNoticias = new ArrayList<Noticia>();
	
	private List<Noticia> noticiasSeleccionadas  = new ArrayList<Noticia>();
	
	private Window window;
	
	private int paginaActual;//nuevo 09-12-14
	
	private int tamanoPagina;//nuevo 09-12-14
	
	private long registrosTotales;//nuevo 09-12-14
	
	private short tipoOperacion;//nuevo 09-12-14
	
	//para los filtros
	
	private String titulo = "";//nuevo 09-12-14
	private String autor = "";//nuevo 09-12-14
	private Date fecha;//nuevo 09-12-14
	private boolean estatusFiltro;//nuevo 09-12-14
	
	public VMListarNoticia(){}
	
	@Init
	public void init() {
		this.listaNoticias = servicioNoticia.buscarTodas(0);//nuevo 09-12-14
		this.registrosTotales = servicioNoticia.totalNoticias();//nuevo 09-12-14
	}
	
	public List<Noticia> getListaNoticias() {
		return listaNoticias;
	}

	public void setListaNoticias(List<Noticia> listanoticias) {
		this.listaNoticias = listanoticias;
	}
	
	public List<Noticia> getNoticiasSeleccionadas() {
		return noticiasSeleccionadas;
	}

	public void setNoticiasSeleccionadas(List<Noticia> noticiasSeleccionadas) {
		this.noticiasSeleccionadas = noticiasSeleccionadas;
	}

	//nuevo 09-12-14
	public int getPaginaActual() {
		return paginaActual;
	}

	//nuevo 09-12-14
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	//nuevo 09-12-14
	public int getTamanoPagina() {
		this.tamanoPagina = servicioNoticia.TAMANO_PAGINA;
		return tamanoPagina;
	}

	//nuevo 09-12-14
	public void setTamanoPagina(int tamanoPagina) {
	this.tamanoPagina = tamanoPagina;
	}

	//nuevo 09-12-14
	public long getRegistrosTotales() {
		return registrosTotales;
	}

	//nuevo 09-12-14
	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	
	//nuevo 09-12-14
	public String getTitulo() {
		return titulo;
	}

	//nuevo 09-12-14
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	//nuevo 09-12-14
	public String getAutor() {
		return autor;
	}

	//nuevo 09-12-14
	public void setAutor(String autor) {
		this.autor = autor;
	}

	//nuevo 09-12-14
	public Date getFecha() {
		return fecha;
	}

	//nuevo 09-12-14
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	//nuevo 09-12-14
	@Command
	@NotifyChange({"listaNoticias","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		this.estatusFiltro=true;  //el filtro se ha activado
		
		if (fecha == null)
			jpql = " autor like '%"+autor+"%' and titulo like '%"+titulo+"%'";
		else
		    jpql = " autor like '%"+autor+"%' and titulo like '%"+titulo+"%' and fecha = '"+fecha+"'";
			
		this.listaNoticias = servicioNoticia.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales = servicioNoticia.totalNoticiasFiltradas(jpql);
	}
	
	//nuevo 09-12-14
	@Command
	@NotifyChange({"listaNoticias","registrosTotales","paginaActual","autor","titulo","fecha"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginarNoticia();
		// las variables siguientes son del filtro
		autor=""; 
		titulo="";
		fecha=null;
	}
	
	//nuevo 09-12-14
	@GlobalCommand
	@NotifyChange({"listaNoticias","registrosTotales","paginaActual"})
	public void paginarNoticia(){
		if (estatusFiltro==false){
			this.listaNoticias = servicioNoticia.buscarTodas(this.paginaActual);
			this.registrosTotales=servicioNoticia.totalNoticias();
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@GlobalCommand
	@NotifyChange({"listaNoticias","registrosTotales","paginaActual","noticiasSeleccionadas"})
	public void ActualizarListaNoticias(){
		this.registrosTotales=servicioNoticia.totalNoticias();
		if(this.registrosTotales > 0){
			if (this.tipoOperacion==1){
				int ultimaPagina=0;
				
				if (this.registrosTotales%this.tamanoPagina==0)
					ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
				else
					ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);
				
				this.paginaActual=ultimaPagina;
				
			} else if(this.tipoOperacion==2){
				
			}
			
			paginarNoticia();
		}
	}
	
	 @Command
	public void EditarNoticia(@BindingParam("noticiaSeleccionada") Noticia noticiaSeleccionada) {
		 this.tipoOperacion=2;
		 Map<String,Noticia> mapNoticia = new HashMap<String, Noticia>();
		 mapNoticia.put("noticia", noticiaSeleccionada);
		 window = (Window) Executions.createComponents("/vistas/gestionevento/noticia.zul", null, mapNoticia);
		 window.doModal();
	}
		
	@Command
	public void AgregarNoticia() {
		this.tipoOperacion=1;
		window = (Window) Executions.createComponents("/vistas/gestionevento/noticia.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange("listaNoticias")
	public void EliminarNoticia(@BindingParam("noticiaSeleccionada") Noticia noticiaSeleccionada){
		servicioNoticia.Eliminar(noticiaSeleccionada.getId());
		//ActualizarListaNoticias();
		paginarNoticia();
		Messagebox.show("Datos eliminados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange("listaNoticias")
	public void EliminarNoticias(){
		if(noticiasSeleccionadas != null){
			servicioNoticia.EliminarVarios(noticiasSeleccionadas);
			//ActualizarListaNoticias();
			paginarNoticia();
			noticiasSeleccionadas = new ArrayList<Noticia>();
			Messagebox.show("Datos eliminados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay datos seleccionados", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}

}
