package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioRecurso;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMDonacionRecurso {
	
	@WireVariable
	private ServicioRecurso servicioRecurso;
	
	@WireVariable
	private DonacionRecurso donacionRecurso; // = new DonacionRecurso();
	
	@WireVariable
	private List<Recurso> recursos;
	
	@WireVariable
	private Recurso recursoSeleccionado;
	
	public VMDonacionRecurso(){
		
	}
	
	@Init
	public void init(){
		this.donacionRecurso = new DonacionRecurso();
		this.recursoSeleccionado = new Recurso();
		//this.recursos = servicioRecurso.ListarRecursos();
	}

	public DonacionRecurso getDonacionRecurso() {
		return donacionRecurso;
	}

	public void setDonacionRecurso(DonacionRecurso donacionRecurso) {
		this.donacionRecurso = donacionRecurso;
	}

	public List<Recurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(List<Recurso> recursos) {
		this.recursos = recursos;
	}

	public Recurso getRecursoSeleccionado() {
		return recursoSeleccionado;
	}

	public void setRecursoSeleccionado(Recurso recursoSeleccionado) {
		this.recursoSeleccionado = recursoSeleccionado;
	}
	
	@Command
	@NotifyChange({"donacionRecurso"})
	public void GuardarDonacionRecurso(){
		this.donacionRecurso.setRecurso(recursoSeleccionado);
		Messagebox.show("Recurso agregado correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		
		//para cerrar la ventana despues que guarda
				Window myWindow = (Window) Path.getComponent("/VistaRecursos");
				myWindow.detach();
	}
	
	@Command
	public void CancelarDonacionRecurso(){
		//para cerrar la ventana despues que guarda
		Window myWindow = (Window) Path.getComponent("/VistaRecursos");
		myWindow.detach();
	}

	
}
