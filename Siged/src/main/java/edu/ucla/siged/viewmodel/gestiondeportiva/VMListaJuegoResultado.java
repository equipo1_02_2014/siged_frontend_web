package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioCompetenciaI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;

public class VMListaJuegoResultado {
   
	@WireVariable
    private ServicioCompetenciaI servicioCompetencia;
	
	@WireVariable
	private ServicioJuegoI servicioJuego;
	
	private Window window;
	
	private int paginaActual;
	
	private boolean filtrar;
	
	private List<Juego> listaJuegos;

	
	private long registrosTotales;
	
	/* Variables Usadas para Filtrar */
	private String nombreCompetencia;
	
	private Date fechaCompetencia;
	
	private String participantes;
	

	
	@Init
	public void init(){
		
		this.paginaActual= 0;
		
		this.listaJuegos= servicioJuego.buscarJuegosPasadosSinResultado(0);
		
		this.registrosTotales= servicioJuego.obtenerCantidadJuegosPasadosSinResultado();
		
		
		this.cancelarFiltro();
	}
	
	@Command
	@NotifyChange({"filtrar","nombreCompetencia",
		           "fechaCompetencia","participantes",
		           "listaJuegos","registrosTotales","resultado"})
	public void cancelarFiltro(){
		
		this.filtrar=false;
		
		this.nombreCompetencia="";
		
		this.fechaCompetencia=null;
		
		this.participantes="";
		
		
		this.paginarJuego();
		
	}
	
	@GlobalCommand("paginarJuegoResultado")
	 @NotifyChange({"listaJuegos","registrosTotales"})
	 public void paginarJuego(){
			if (filtrar==false){
			   this.listaJuegos = servicioJuego.buscarJuegosPasadosSinResultado(this.paginaActual);
			   this.registrosTotales= servicioJuego.obtenerCantidadJuegosPasadosSinResultado();
			
			}else{
				
				ejecutarFiltro();
			}

	 }
	
	 @Command
	 @NotifyChange({"listaJuegos","registrosTotales"})
	 public void ejecutarFiltro(){
		 
		if ((this.nombreCompetencia!=null && !this.nombreCompetencia.isEmpty()) ||
			(this.participantes!=null && !this.participantes.isEmpty()) ||
			this.fechaCompetencia!=null){
			
			
				String tablas="FROM Juego j";
				String condiciones=" WHERE ";
				String jpql="";
				this.filtrar=true;
				
				if (!this.nombreCompetencia.isEmpty()){
					
					tablas += ",Competencia c";
					
					condiciones += "c.id=j.competencia.id AND c.nombre LIKE '%" + this.nombreCompetencia + "%' AND";
				}
				
				if (!this.participantes.isEmpty()){
					tablas += ",Equipo e";
				    
					
					condiciones += " e.id=j.equipo.id AND ((e.nombre LIKE '%" + this.participantes + "%') OR " + 
					               "(j.equipoContrario LIKE '%" + this.participantes + "%')) AND";
				}
				
				if (this.fechaCompetencia!=null){
					
					String fechaFormateada= new SimpleDateFormat("yyyy-MM-dd").format(this.fechaCompetencia);
					
					condiciones += " j.fecha='" + fechaFormateada + "' AND";
				}
				
			
				condiciones += " j.resultado=-1 AND j.id IN (SELECT g.id FROM Juego g WHERE g.fecha < CURRENT_DATE "
				                    + "OR g.id IN (SELECT h.id FROM Juego h WHERE h.fecha = CURRENT_DATE AND h.horaInicio < CURRENT_TIME))";
				
				
				jpql += tablas + condiciones;
				
				//System.out.println("-------------------CONSULTA JPQL-------------------------------: ");
				 // System.out.println(jpql); 
				 // System.out.println("----------------------------------------------------------------: ");				
				
				this.listaJuegos=servicioJuego.buscarFiltrado(jpql,this.paginaActual);
				this.registrosTotales=servicioJuego.obtenerTotalFiltrados(jpql);
				
				
		}
		else{
			 this.listaJuegos = servicioJuego.buscarJuegosPasadosSinResultado(this.paginaActual);
			 this.registrosTotales= servicioJuego.obtenerCantidadJuegosPasadosSinResultado();
		}
		 
		 
	 }
	 
	 @Command
	 public void editarJuego(@BindingParam("juegoEditarResultado")Juego juego){
			
			Sessions.getCurrent().setAttribute("JuegoEditarResultado", juego);
			
			window = (Window)Executions.createComponents(
			            "/vistas/gestiondeportiva/juegoresultado.zul", null, null);
			
		    window.doModal();
		    
	 }
	 
	 
	 public String formatearFechaJuego(Juego juego){
			
			String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(juego.getFecha());
			
			fechaFormateada += new SimpleDateFormat(" hh:mm a").format(juego.getHoraInicio());;
			
			return fechaFormateada;
	 }
		
	 public String formatearParticipantes(Juego juego){
			
			String participantes= juego.getEquipo().getNombre() + " VS " + juego.getEquipoContrario();
			
			return participantes;
	 }
	 
	 public int getTamanioPagina(){
			return servicioJuego.obtenerTamanioPagina();
	 }

	 public int getPaginaActual() {
			return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
			this.paginaActual = paginaActual;
	}


	public List<Juego> getListaJuegos() {
			return listaJuegos;
	}

	public long getRegistrosTotales() {
			return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public String getNombreCompetencia() {
			return nombreCompetencia;
	}

	public void setNombreCompetencia(String nombreCompetencia) {
			this.nombreCompetencia = nombreCompetencia;
	}

	public Date getFechaCompetencia() {
			return fechaCompetencia;
	}

	public void setFechaCompetencia(Date fechaCompetencia) {
			this.fechaCompetencia = fechaCompetencia;
	}

    public String getParticipantes() {
			return participantes;
	}

	public void setParticipantes(String participantes) {
			this.participantes = participantes;
	}
	
	
		
	
}
