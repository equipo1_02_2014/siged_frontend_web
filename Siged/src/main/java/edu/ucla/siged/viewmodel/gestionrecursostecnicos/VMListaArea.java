/** 
 * 	VMListaArea
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAreaI;

public class VMListaArea {
	
	@WireVariable
	ServicioAreaI servicioArea;
	Area area;
	List<Area> listaArea; 
	List<Area> areasSeleccionadas;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	String ubicacion=""; 
	String nombre="";
	Integer id=0;
	Integer estatusCombo=0;
	boolean estatusFiltro;

	public String getUbicacion() {
		return ubicacion;
	}
	
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setArea(Area area) {
		this.area = area;
	}

	public List<Area> getListaArea() {
		return listaArea;
	}
	
	public void setListasarea(List<Area> listaArea) {
		this.listaArea = listaArea;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioArea.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public List<Area> getAreasSeleccionadas() {
		return areasSeleccionadas;
	}
	
	public void setAreasSeleccionadas(List<Area> areasSeleccionadas) {
		this.areasSeleccionadas = areasSeleccionadas;
	}
	
	public long getRegistrosTotales() {
		this.registrosTotales= servicioArea.totalArea();
		return registrosTotales;
	}
	
	public Integer getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Integer estatusCombo) {
		this.estatusCombo = estatusCombo;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Init
	public void init(){
		this.listaArea = servicioArea.buscarTodos(0).getContent();
	}
	
	@Command
	public void agregarArea() { 
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/panelarea.zul", null, null);
		window.doModal();
	}	

	@Command
	@NotifyChange({"listaArea","registrosTotales"})
	public void eliminarArea(@BindingParam("areaSeleccionada") final Area areaSeleccionada){
		this.area= areaSeleccionada;
		Messagebox.show("�Est� seguro que desea eliminar este registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new EventListener<Event>() {
			
					public void onEvent(Event event) throws Exception {
                    
						if(Messagebox.ON_YES.equals(event.getName())){
							if (servicioArea.buscarArea(area))
							{
								servicioArea.eliminar(area);
								BindUtils.postGlobalCommand(null, null, "actualizar", null);
								Messagebox.show("El registro ha sido eliminado exitosamente.","",Messagebox.OK, Messagebox.INFORMATION);
							}
							else
								Messagebox.show("El area no puede ser eliminada porque ya se encuentra asignada.","",Messagebox.OK, Messagebox.INFORMATION);
								
						}
					}
				});
		
		
	}
	
	@GlobalCommand("actualizar")
	@NotifyChange({"listaArea","registrosTotales"})
	public void actualizar(){
		paginar();
		if ((listaArea.size()==0) && (this.registrosTotales>0)){
			this.listaArea = servicioArea.buscarTodos(this.paginaActual-1).getContent();
		}
	}
	
	@Command
	@NotifyChange({"listaArea","registrosTotales","areasSeleccionadas"})
	public void eliminarAreas(@BindingParam("lista") Listbox lista){
		if(lista.getSelectedItems().size()>0){
			Messagebox.show("�Est� seguro que desea eliminar las areas seleccionadas?", "Confirmacion",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
						public void onEvent(Event event) throws Exception {
							if(Messagebox.ON_YES.equals(event.getName())){
								if (servicioArea.eliminarVarios(areasSeleccionadas)>0){	
									Messagebox.show("Las areas seleccionadas han sido eliminadas exitosamente","",Messagebox.OK, Messagebox.INFORMATION);
									areasSeleccionadas.clear();
									BindUtils.postGlobalCommand(null, null, "actualizar", null);	
								}
								else
									Messagebox.show("No puede ser eliminada porque el area ya se encuentra asignada.","",Messagebox.OK, Messagebox.INFORMATION);
							}
						}
					});
			
		
		}
		else Messagebox.show("Debe seleccionar las areas que desea eliminar.","",Messagebox.OK, Messagebox.EXCLAMATION);
	}
	


	@Command
	@NotifyChange({"area"})
	public void editarArea(@BindingParam("areaSeleccionada") Area areaSeleccionada ) {
		this.area=areaSeleccionada;
		this.tipoOperacion=2;
		Map<String, Integer> mapa = new HashMap<String, Integer>();
        mapa.put("idArea", areaSeleccionada.getId());
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/panelarea.zul", null, mapa);
		window.doModal();
	}
	
	@GlobalCommand
	@NotifyChange({"listaArea","registrosTotales","paginaActual","areaseleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaArea = servicioArea.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioArea.totalArea();
		}else{
			ejecutarFiltro();
		}

	}
	
	@GlobalCommand("actualizarLista")
	@NotifyChange({"listaArea","registrosTotales","paginaActual"})
	public void actualizarLista(){
	 	this.registrosTotales=servicioArea.totalArea();
	 	if (registrosTotales>0){
			if (this.tipoOperacion==1) {
			   int ultimaPagina=0;
			   if ((this.registrosTotales)%this.tamanoPagina==0)
			      ultimaPagina= ((int) (this.registrosTotales)/this.tamanoPagina)-1;
			   else
				  ultimaPagina =((int) (this.registrosTotales)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
			}
			else if(this.tipoOperacion==2){
			}	
			 paginar();
	 	}
	}
	
	@Command
	@NotifyChange({"listaArea","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	@Command
	@NotifyChange({"listaArea"})
	public String verStatusEnLista(Integer estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
	
	public void ejecutarFiltro(){
		String jpql="";
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		if (id != null)
			
			jpql = " nombre like '%"+nombre+"%' and ubicacion like '%"+ubicacion+"%'";
		
	
		this.listaArea=servicioArea.buscarFiltrado(jpql,this.paginaActual);  //el filtro comienza a mostrar desde la 1ra pagina
		this.registrosTotales=servicioArea.totalAreaFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"listaArea","registrosTotales","paginaActual","areaSeleccionados",
				   "ubicacion", "nombre","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		id=0;
		ubicacion=""; // las variables siguientes son del filtro
		nombre="";		
		estatusCombo=0;
	}	
}