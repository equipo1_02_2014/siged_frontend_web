package edu.ucla.siged.viewmodel.seguridad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;






import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioUsuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolUsuarioI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMActivarUsuarios {
	@WireVariable
	private ServicioUsuario servicioUsuario;
	@WireVariable
	Usuario usuario;

	List<Usuario> listaUsuarios;
	
	Usuario usuarioSeleccionado;
	
	List<Usuario> usuariosSeleccionados;
	
	private Window window;
	short estatusCombo=0;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	boolean estatusFiltro;
	String apellido;
	String nombreUsuario;
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	


	String nombre;

	public VMActivarUsuarios() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
	public void init() {
		
		cancelarFiltro();
		
		this.listaUsuarios=servicioUsuario.buscarTodosEstatus((short) 0,0);
		this.registrosTotales= servicioUsuario.totalUsuariosEstatus((short) 0);
		this.estatusCombo=1;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Usuario getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}

	public void setUsuarioSeleccionado(Usuario usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}
	
	public void listaUsuarios(){
		listaUsuarios = servicioUsuario.buscarTodos();	
	}
	
	
	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina=ServicioUsuario.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public boolean isEstatusFiltro() {
		return estatusFiltro;
	}

	public void setEstatusFiltro(boolean estatusFiltro) {
		this.estatusFiltro = estatusFiltro;
	}	
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	

	public List<Usuario> getUsuariosSeleccionados() {
		return usuariosSeleccionados;
	}

	public void setUsuariosSeleccionados(
			List<Usuario> usuariosSeleccionados) {
		this.usuariosSeleccionados = usuariosSeleccionados;
	}

	@Command
	@NotifyChange({"listaUsuarios"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		System.out.println("estatus:"+estatusEnLista.toString());
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	@NotifyChange("listaUsuario")
	public void agregarUsuario(){
		window = (Window)Executions.createComponents("/vistas/seguridad/usuario.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange("listaUsuarios")
	public void editarUsuario(@BindingParam("usuarioSeleccionado") Usuario usuarioSeleccionado){
		Map<String,Usuario> mapUsuario = new HashMap<String, Usuario>();
		mapUsuario.put("usuarioSeleccionado", usuarioSeleccionado);
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/seguridad/usuario.zul", null, mapUsuario);
		window.doModal();
		
	
	
	
	}
	
	@Command
	@NotifyChange("listaUsuarios")
	public void eliminarUsuario(@BindingParam("usuarioSeleccionado") Usuario usuarioSeleccionado){

		servicioUsuario.eliminar(usuarioSeleccionado);
	}
	
	@Command
	@NotifyChange("listaUsuarios")
	public void eliminarUsuarios(){
		
		servicioUsuario.eliminarVarios(usuariosSeleccionados);
	}
	
	@GlobalCommand
	@NotifyChange({"listaUsuarios","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevoUsuario") Usuario nuevoUsuario){
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} 
		paginar();
	}
	
	@GlobalCommand
	@NotifyChange({"listaUsuarios","registrosTotales","paginaActual","usuariosSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaUsuarios = servicioUsuario.buscarTodosEstatus((short) 0, this.paginaActual);
		this.registrosTotales=servicioUsuario.totalUsuariosEstatus((short) 0);
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@Command
	@NotifyChange({"listaUsuarios","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		List<String> condiciones = new ArrayList<String>();
		
		if(!nombre.trim().isEmpty())	
		condiciones.add(" nombre like '%"+nombre+"%' ");
		if(!apellido.trim().isEmpty())
		condiciones.add(" apellido like'%"+apellido+"%' ");
		if(!nombreUsuario.trim().isEmpty())
		condiciones.add(" nombreUsuario like'%"+nombreUsuario+"%' ");
		if(!filtroEstatus.isEmpty())
		condiciones.add(" estatus in "+filtroEstatus);
		
		
		jpql="";	
		for(int i=0; i<condiciones.size(); i++){
			jpql+= condiciones.get(i);
			if(i+1<condiciones.size())
			jpql+= " and ";
		}
		
		System.out.println("----------------------------------"+jpql.toString());
		this.listaUsuarios=servicioUsuario.buscarFiltrado(jpql,this.paginaActual);
		
		this.registrosTotales=servicioUsuario.totalUsuariosFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"listaUsuarios","registrosTotales","paginaActual","usuariosSeleccionados",
				   "nombre","apellido", "nombreUsuario", "estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		nombre="";
		apellido="";
		nombreUsuario="";
		estatusCombo=1;
	}

	@Command
	@NotifyChange("listaUsuarios")
	public void eliminarFisicoUsuario(@BindingParam("usuarioSeleccionado") Usuario usuarioSeleccionado){
		System.out.println("EliminarFisicoUsuario...vm");
		try {
			servicioUsuario.eliminarFisico(usuarioSeleccionado);
			actualizarLista(usuarioSeleccionado);
		} catch (Exception e) {
			Messagebox.show("No se ha podido eliminar el usuario!", "Error", Messagebox.OK, Messagebox.ERROR);
			
		}
		
	}
	
	@Command
	@NotifyChange("listaUsuarios")
	public void eliminarFisicoUsuarios(){
		try{
			servicioUsuario.eliminarVariosFisico(usuariosSeleccionados);
			actualizarLista(usuariosSeleccionados.get(0));
		}catch(Exception e){
			Messagebox.show("No se ha podido eliminar los usuarios!", "Error", Messagebox.OK, Messagebox.ERROR);
			
		}
	}
	
	@Command
	@NotifyChange("listaUsuarios")
	public void activarUsuario(@BindingParam("usuarioSeleccionado") Usuario usuarioSeleccionado){

		servicioUsuario.activar(usuarioSeleccionado);
		actualizarLista(usuarioSeleccionado);
	}
	
	@Command
	@NotifyChange("listaUsuarios")
	public void activarUsuarios(){
		servicioUsuario.activarVarios(usuariosSeleccionados);
		actualizarLista(usuariosSeleccionados.get(0));
	}
	
}