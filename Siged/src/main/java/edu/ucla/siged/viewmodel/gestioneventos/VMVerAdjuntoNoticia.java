package edu.ucla.siged.viewmodel.gestioneventos;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;

public class VMVerAdjuntoNoticia {
	
	private AdjuntoNoticia adjunto = new AdjuntoNoticia();
		
	public VMVerAdjuntoNoticia() {}

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("adjuntoaux") AdjuntoNoticia aux) {
		Selectors.wireComponents(view, this, false);
		if(aux!=null){
			this.adjunto = aux;
		}
	}

	public AdjuntoNoticia getAdjunto() {
		return adjunto;
	}

	public void setAdjunto(AdjuntoNoticia adjunto) {
		this.adjunto = adjunto;
	}		
}