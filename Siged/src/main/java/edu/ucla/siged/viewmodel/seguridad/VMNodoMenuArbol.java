package edu.ucla.siged.viewmodel.seguridad;

import org.zkoss.zul.DefaultTreeNode;

import edu.ucla.siged.domain.seguridad.Funcionalidad;




public class VMNodoMenuArbol extends DefaultTreeNode<Funcionalidad> {
	private static final long serialVersionUID = -7012663776755277499L;
	//---------Variables de control------------------------
	private boolean open = false;

	/**
	* Constructor de la clase VMNodoMenuArbol para un nodo del arbol con hijos
	*
	* @param datos, ninos
	*/
	public VMNodoMenuArbol(Funcionalidad datos, DefaultTreeNode<Funcionalidad>[] hijos) {
		super(datos, hijos);
	}

	/**
	* Constructor de la clase VMNodoMenuArbol para un nodo del arbol con hijos y abierto.
	*
	* @param datos, ninos, abierto
	*/
	
	public VMNodoMenuArbol(Funcionalidad datos, DefaultTreeNode<Funcionalidad>[] ninos, boolean abierto) {
		super(datos, ninos);
		setOpen(abierto);
	}
	
	/**
	* Constructor de la clase VMNodoMenuArbol para un nodo del arbol sin hijos
	*
	* @param datos
	*/

	public VMNodoMenuArbol(Funcionalidad datos) {
		super(datos);
	}

	/**
	* Permite conocer si el item del arbol est� abierto o cerrado.
	*
	* @param ninguno.
	* @return true si el item del arbol est� abierto y false en caso contrario.
	* @throws No dispara ninguna excepci�n.
	*/
	
	public boolean isOpen() {
		return open;
	}

	/**
	* Permite abrir o cerrar el item del arbol.
	*
	* @param open.
	* @return si open es tree abre el item del arbol y si es false lo cierra.
	* @throws No dispara ninguna excepci�n.
	*/
	
	public void setOpen(boolean open) {
		this.open = open;
	}
} //fin VMNodoMenuArbol