package edu.ucla.siged.viewmodel.gestionequipos;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

public class VMlistarRepresentante {

	Window window;


	@Command
	public void agregarRepresentante(){
		window = (Window)Executions.createComponents("/vistas/gestionequipo/registrarequipo.zul", null, null);
		window.doModal();
		//window.doOverlapped();
	}
	
	@Command
	public void editarRepresentante(){
		window = (Window)Executions.createComponents("/vistas/gestionequipo/registrarequipo.zul", null, null);
		window.doModal();
		//window.doOverlapped();
	}
}
