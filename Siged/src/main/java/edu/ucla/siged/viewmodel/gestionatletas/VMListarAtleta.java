package edu.ucla.siged.viewmodel.gestionatletas;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstatusAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioHistoriaAtletaI;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
//@SuppressWarnings("serial")

//@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMListarAtleta {
	
	private @WireVariable ServicioAtletaI servicioAtleta;
	private List<Atleta> listaAtletas;
	private Window window;
	private List<Atleta> atletasSelecionados = new ArrayList<Atleta>();
	private @WireVariable ServicioEstatusAtletaI servicioEstatusAtleta;
	private @WireVariable ServicioHistoriaAtletaI servicioHistoriaAtleta;
	
	private String filtroCedula = "";
	private String filtroNombre = "";
	private String filtroApellido = "";
	private String filtroEquipoActual = "";
	private List<EstatusAtleta> estatusAtletas = new ArrayList<EstatusAtleta>();
	private EstatusAtleta estatusAtletaSeleccionado = null;
	private String filtroBecado = "";
	private String filtroSolvencia = "";
	private List<String> comboBecado = new ArrayList<String>();
	private List<String> comboSolvencia = new ArrayList<String>();
	
	
	private @WireVariable Atleta atletaSeleccionadoPago;
//	Inicio de variables, set y get para Paginación
	private int paginaActual;
	private int tamanoPagina;
	private long registrosTotales;
	
	
	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		tamanoPagina = this.servicioAtleta.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	
	public String getFiltroCedula() {
		return filtroCedula;
	}

	public void setFiltroCedula(String filtroCedula) {
		this.filtroCedula = filtroCedula;
	}

	public String getFiltroNombre() {
		return filtroNombre;
	}

	public void setFiltroNombre(String filtroNombre) {
		this.filtroNombre = filtroNombre;
	}

	public String getFiltroApellido() {
		return filtroApellido;
	}

	public void setFiltroApellido(String filtroApellido) {
		this.filtroApellido = filtroApellido;
	}

	public List<EstatusAtleta> getEstatusAtletas() {
		return estatusAtletas;
	}

	public void setEstatusAtletas(List<EstatusAtleta> estatusAtletas) {
		this.estatusAtletas = estatusAtletas;
	}

	public EstatusAtleta getEstatusAtletaSeleccionado() {
		return estatusAtletaSeleccionado;
	}

	public void setEstatusAtletaSeleccionado(EstatusAtleta estatusAtletaSeleccionado) {
		this.estatusAtletaSeleccionado = estatusAtletaSeleccionado;
	}

	public String getFiltroEquipoActual() {
		return filtroEquipoActual;
	}

	public void setFiltroEquipoActual(String filtroEquipoActual) {
		this.filtroEquipoActual = filtroEquipoActual;
	}

	public String getFiltroBecado() {
		return filtroBecado;
	}

	public void setFiltroBecado(String filtroBecado) {
		this.filtroBecado = filtroBecado;
	}

	public String getFiltroSolvencia() {
		return filtroSolvencia;
	}

	public void setFiltroSolvencia(String filtroSolvencia) {
		this.filtroSolvencia = filtroSolvencia;
	}

	@GlobalCommand
	@NotifyChange({"listaAtletas","paginaActual","registrosTotales","tamanoPagina"})
	public void paginar(){
		this.listaAtletas = servicioAtleta.obtenerListaAtleta(this.paginaActual,this.filtroCedula, this.filtroNombre, this.filtroApellido, this.filtroEquipoActual,this.estatusAtletaSeleccionado);
		this.registrosTotales = servicioAtleta.totalAtletasFiltrados(this.filtroCedula, this.filtroNombre, this.filtroApellido, this.filtroEquipoActual, this.estatusAtletaSeleccionado);
	}
	
	@Command
	@NotifyChange({"listaAtletas","paginaActual","registrosTotales","tamanoPagina"})
	public void filtrar(){
		this.paginaActual=0;
		this.listaAtletas = servicioAtleta.obtenerListaAtleta(this.paginaActual,this.filtroCedula, this.filtroNombre, this.filtroApellido,this.filtroEquipoActual , this.estatusAtletaSeleccionado);
		this.registrosTotales = servicioAtleta.totalAtletasFiltrados(this.filtroCedula, this.filtroNombre, this.filtroApellido, filtroEquipoActual,this.estatusAtletaSeleccionado);	
	}
	
// fin de los get y set para paginación
	@Init
	public void init(){
		this.comboBecado.add("Becado");
		this.comboBecado.add("N/A");
		this.comboSolvencia.add("Solvente");
		this.comboSolvencia.add("Moroso");
		this.comboSolvencia.add("N/A");
		estatusAtletas = servicioEstatusAtleta.listaEstatusAtletaNoEliminado();
		listaAtletas = servicioAtleta.obtenerListaAtleta(0);//Para buscar Todos con paginacion
		this.registrosTotales = servicioAtleta.totalAtletas();//Para la paginacion
	}
	
	public List<Atleta> getListaAtletas() {
		return listaAtletas;
	}

	public void setListaAtletas(List<Atleta> listaAtletas) {
		this.listaAtletas = listaAtletas;
	}
	
	public List<Atleta> getAtletasSelecionados() {
		return atletasSelecionados;
	}

	public void setAtletasSelecionados(List<Atleta> atletasSelecionados) {
		this.atletasSelecionados = atletasSelecionados;
	}
	
	public List<String> getComboBecado() {
		return comboBecado;
	}

	public void setComboBecado(List<String> comboBecado) {
		this.comboBecado = comboBecado;
	}

	public List<String> getComboSolvencia() {
		return comboSolvencia;
	}

	public void setComboSolvencia(List<String> comboSolvencia) {
		this.comboSolvencia = comboSolvencia;
	}

	@Command
	@NotifyChange({"listaAtletas"})
	public void abrirVentanaAtleta(@BindingParam("atletaSeleccionado") Atleta atletaSeleccionado){
		try{
			Map<String, Atleta> mapAtleta = new HashMap<String, Atleta>();
			mapAtleta.put("atleta", atletaSeleccionado);
			window = (Window)Executions.createComponents("vistas/gestionatleta/tabatleta.zul", null, mapAtleta);
			window.doModal();
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@GlobalCommand
	@NotifyChange({"listaAtletas","paginaActual","registrosTotales","tamanoPagina"})
	public void actualizarListaAtletas(){
		//this.paginaActual=0;
		this.listaAtletas = servicioAtleta.obtenerListaAtleta(this.paginaActual,this.filtroCedula, this.filtroNombre, this.filtroApellido,this.filtroEquipoActual ,this.estatusAtletaSeleccionado);
		this.registrosTotales = servicioAtleta.totalAtletasFiltrados(this.filtroCedula, this.filtroNombre, this.filtroApellido,this.filtroEquipoActual,this.estatusAtletaSeleccionado);
	}
	
	@Command
	@NotifyChange({"atletasSelecionados", "listaAtletas"})
	public void eliminarAtletas(){
		List<Atleta> atletasAuxiliar= new ArrayList<Atleta>();
		for(Atleta atleta: atletasSelecionados){
			if(atleta.getEstatusActual().getId()!=0){
				HistoriaAtleta historiaAtleta = new HistoriaAtleta(new Date(),(short) 1, servicioEstatusAtleta.buscarEstatusAtleta(0));
				atleta.addHistoriasAtletla(historiaAtleta);
				atletasAuxiliar.add(atleta);
			}
		}
		servicioAtleta.eliminarAtletasLogicamente(atletasAuxiliar);
		atletasSelecionados.clear();
		actualizarListaAtletas();
	}
	@Command
	@NotifyChange("listaAtletas")
	public void eliminarAtleta(@BindingParam("atletaSeleccionado") Atleta atleta){
		if(atleta.getEstatusActual().getId()!=0)
			servicioAtleta.eliminarAtletaLogicamente(atleta);
		
		actualizarListaAtletas();
	}

	public Atleta getAtletaSeleccionadoPago() {
		return atletaSeleccionadoPago;
	}

	public void setAtletaSeleccionadoPago(Atleta atletaSeleccionadoPago) {
		this.atletaSeleccionadoPago = atletaSeleccionadoPago;
	}
	
	@Command
	@NotifyChange({"listaAtletas","registrosTotales","filtroCedula","filtroNombre","filtroApellido","estatusAtletaSeleccionado", "filtroEquipoActual", "filtroBecado", "filtroSolvencia"})
	public void  cancelarFiltro(){
		this.paginaActual=0;
		listaAtletas = servicioAtleta.obtenerListaAtleta(this.paginaActual);
		this.registrosTotales = servicioAtleta.totalAtletas();
		this.filtroCedula = "";
		this.filtroNombre = "";
		this.filtroApellido = "";
		this.estatusAtletaSeleccionado = null;
		this.filtroEquipoActual = "";
	}
	

}
