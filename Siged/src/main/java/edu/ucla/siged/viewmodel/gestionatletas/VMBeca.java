package edu.ucla.siged.viewmodel.gestionatletas;

import java.util.Calendar;
import java.util.LinkedList;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zul.impl.InputElement;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAyuda;
import edu.ucla.siged.servicio.impl.gestionatletas.ServicioAtleta;
import edu.ucla.siged.servicio.impl.gestionatletas.ServicioPago;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMBeca {
	
	@WireVariable
	private ServicioAtleta servicioAtleta;
	
	@WireVariable
	private Atleta atleta = new Atleta();
	
	@WireVariable
	private HistoriaAyuda beca = new HistoriaAyuda();
	
	/********************************/
	/********************************/
	@WireVariable
	private ServicioPago servicioPago;
	/********************************/
	/********************************/

	public Atleta getAtleta() {
		return atleta;
	}

	public void setAtleta(Atleta atleta) {
		this.atleta = atleta;
	}

	public HistoriaAyuda getBeca() {
		return beca;
	}

	public void setBeca(HistoriaAyuda beca) {
		this.beca = beca;
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("atleta") Atleta atletaSeleccionado){
		Selectors.wireComponents(view, this, false);
		
		this.atleta = atletaSeleccionado;
		this.beca = new HistoriaAyuda();
	}
	
	@Command
	@NotifyChange({"beca","atleta"})
	public void GuardarBeca(@SelectorParam("textbox,datebox,timebox") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}			
		}
		if(!camposValidos){
			Messagebox.show("Los campos no pueden estar vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{
			Calendar cInicio = Calendar.getInstance();
			Calendar cFin = Calendar.getInstance();
			cInicio.setTime(this.beca.getFechaInicio());
			cFin.setTime(this.beca.getFechaFin());
			if(servicioPago.buscarPagoPorMesYAnno(this.atleta.getId(), (Integer)(cInicio.get(Calendar.MONTH)+1), (long)cInicio.get(Calendar.YEAR))){
				Messagebox.show("Escoja otro mes de inicio.\n Este mes fue pagado.!!", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
			}else{
				if(!camposValidos){
					Messagebox.show("Los campos no pueden estar vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				}else{
					if(this.beca.getFechaInicio().after(this.beca.getFechaFin())){
						Messagebox.show("La fecha de fin debe ser mayor a la fecha de inicio", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
					}else{
						cInicio.set(Calendar.DATE, 1);
						beca.setFechaInicio(cInicio.getTime());
						cFin.set(Calendar.DATE, 1);
						cFin.add(Calendar.MONTH, 1);
						beca.setFechaFin(cFin.getTime());
						this.atleta.getHistoriaAyudas().add(beca);
						servicioAtleta.Guardar(atleta);
						Messagebox.show("Datos almacenados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
						Limpiar();
						Window myWindow = (Window) Path.getComponent("/id_becah");
						myWindow.detach();
					}
				}
			}
		}
	}
	
	@Command
	@NotifyChange({"beca","atleta"})
	public void Limpiar(){
		this.atleta = new Atleta();
		this.beca = new HistoriaAyuda();
	}

}
