/**
 * Clase: VMEditarJuego
 * 
 * Version 1.0: Clase que corresponde al View Model de EditarJuego
 * 
 * Fecha de Ultima Modificacion: 11/01/2015
 * 
 * Autor: Jesus Baute
 * 
 * */

package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Constraint;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestiondeportiva.ArbitroJuego;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestionrecursostecnicios.Anotador;
import edu.ucla.siged.domain.gestionrecursostecnicios.Arbitro;
import edu.ucla.siged.domain.gestionrecursostecnicios.Area;
import edu.ucla.siged.domain.utilidades.Persona;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;

public class VMEditarJuego {

	@WireVariable
    private ServicioEquipoI servicioEquipo;
	
	@WireVariable
    private ServicioJuegoI servicioJuego;
	
	@WireVariable
    private ServicioPracticaI servicioPractica;
	
	private List<Area> listaAreas;
	
	private List<Anotador> listaAnotadores;
	
	private List<Arbitro> listaArbitros;
	
	private List<Arbitro> listaArbitrosOrig;
	
	private List<ArbitroJuego> listaArbitroJuego;
	
    private boolean soloLecturaLugar;
	
	private Anotador anotador;
	
	private Juego juego;
	
	private Juego juegoOrig;
	
	private Date fecha;
	
	private Date hora;
	
    private String escuelaContraria;
	
	private String equipoContrario;
	
	private String observacion;
	
	private Area area;
	
	private String lugar;
	
    private Window window;
    
    private Tab tabInformacion;
    
    private boolean tabCambiado;
	
	
	@Init
	public void init(){
		
		this.juego= (Juego) Sessions.getCurrent().getAttribute("JuegoEditar");
		
		this.juegoOrig= new Juego();
		
		Set<ArbitroJuego> arbitrosJuego= this.servicioJuego.obtenerArbitrosJuego(juego);
		
		this.juego.setArbitroJuego(arbitrosJuego);
		
		/*Valores Juego Original*/
        this.juegoOrig.setFecha(this.juego.getFecha());
		
		this.juegoOrig.setHoraInicio(this.juego.getHoraInicio());
    	
    	this.juegoOrig.setEquipoContrario(this.juego.getEquipoContrario());
    	
    	this.juegoOrig.setEscuelaContraria(this.juego.getEscuelaContraria());
    	
    	this.juegoOrig.setObservacion(this.juego.getObservacion());
    	
     	this.juegoOrig.setArea(this.juego.getArea());
     	
    	this.juegoOrig.setLugar(this.juego.getLugar());
    	
    	this.juegoOrig.setAnotador(this.juego.getAnotador());
   
    	this.juegoOrig.setArbitroJuego(this.juego.getArbitroJuego());
		/**/
		
		this.listaAreas= servicioJuego.buscarAreasJuego();
		
        Area area= new Area();
		
		area.setNombre("OTRO");
		
		this.listaAreas.add(area);
		
        this.inicializarCampos();
        
        
	}
	

	
	@NotifyChange({"fecha","hora","escuelaContraria",
		           "equipoContrario","observacion",
		           "area","lugar","soloLecturaLugar",
		           "nombreAreaInicio","anotador",
		           "listaArbitroJuego","listaAnotadores",
		           "listaArbitros","listaArbitrosOrig","tabCambiado"})
	public void inicializarCampos(){
		
		
		/*Reiniciar Juego*/
		this.juego.setFecha(this.juegoOrig.getFecha());
		
		this.juego.setHoraInicio(this.juegoOrig.getHoraInicio());
    	
    	this.juego.setEquipoContrario(this.juegoOrig.getEquipoContrario());
    	
    	this.juego.setEscuelaContraria(this.juegoOrig.getEscuelaContraria());
    	
    	this.juego.setObservacion(this.juegoOrig.getObservacion());
    	
     	this.juego.setArea(this.juegoOrig.getArea());
     	
    	this.juego.setLugar(this.juegoOrig.getLugar());
    	
    	this.juego.setAnotador(this.juegoOrig.getAnotador());
   
    	this.juego.setArbitroJuego(this.juegoOrig.getArbitroJuego());
		
		/**/
		
		
		this.fecha= this.juego.getFecha();
		
		this.hora= this.juego.getHoraInicio();
		
		this.escuelaContraria= this.juego.getEscuelaContraria();
		
		this.equipoContrario= this.juego.getEquipoContrario();
		
		this.observacion= this.juego.getObservacion();
		
		this.area= this.juego.getArea();
		
	    this.lugar= this.juego.getLugar();
	    
	    this.anotador= this.juego.getAnotador();
	    
	    this.listaArbitroJuego= this.servicioJuego.obtenerListaArbitrosJuego(juego);
	    
	    this.listaAnotadores= new ArrayList<Anotador>();
 		
	 	this.listaArbitros= new ArrayList<Arbitro>();
	 		
	 	this.listaArbitrosOrig= new ArrayList<Arbitro>();
	    
	    this.soloLecturaLugar=(this.area!=null);
	    
	    this.tabCambiado= false;
	    
	}
	
    @Command
    @NotifyChange({"fecha","hora","escuelaContraria",
                   "equipoContrario","observacion",
                   "area","lugar","soloLecturaLugar","anotador",
                   "listaArbitroJuego","listaAnotadores",
                   "listaArbitros","listaArbitrosOrig","tabCambiado"})
    public void cancelar(@SelectorParam("#tabGestionDeportivaEditarJuegoInformacion")Tab tabInf){
        
       this.inicializarCampos();	
	   tabInf.setSelected(true);
	   
    }
    
    @GlobalCommand("refreshEditarJuego")
    @NotifyChange({"fecha","hora","escuelaContraria",
                   "equipoContrario","observacion",
                   "area","lugar","soloLecturaLugar","anotador",
                   "listaArbitroJuego","listaAnotadores",
                   "listaArbitros","listaArbitrosOrig","tabCambiado"})
    public void refresh(){
    	 this.inicializarCampos();	
    }
	
	
	@Command
	@NotifyChange({"soloLecturaLugar","lugar"})
	public void habilitarLugar(){
		
		if (this.area==null){
			this.soloLecturaLugar=true;
			this.lugar="";
			
		}
		else if(!this.area.getNombre().equalsIgnoreCase("OTRO")){
			this.soloLecturaLugar=true;
			this.lugar= this.area.getNombre();
		
		}
		else{
			this.soloLecturaLugar=false;
			this.lugar="";
			
		}
		
		
	}
	
	@Command
	public void guardar(@SelectorParam(".juegoEditar") LinkedList<InputElement> inputsEdicion,
			            @SelectorParam(".juegoEditarAnotador") LinkedList<InputElement> inputAnotador,
			            @SelectorParam(".juegoEditarArbitro") LinkedList<InputElement> inputArbitro,
			            @SelectorParam("#wdwGestionDeportivaEditarJuego") Window win,
			            @SelectorParam("#tabGestionDeportivaEditarJuegoInformacion")Tab tabInf){
		
		
        boolean camposValidos=true;
		
		for(InputElement input:inputsEdicion){
			
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
			
		}
		
		if (camposValidos && this.tabCambiado){
			
			for(InputElement input:inputAnotador){
				
				if (!input.isValid()){
					camposValidos=false;
					break;
				}
				
			}
			
			if (camposValidos){
				for(InputElement input:inputArbitro){
					
					if (!input.isValid()){
						camposValidos=false;
						break;
					}
					
				}
			}
		}
		
		/*Validando Campos*/
		if(!camposValidos){
			
			Messagebox.show("Por Favor Introduzca Campos Validos", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/*Validar que el Equipo ya no Tenga un Juego Planificado*/
		else if(servicioJuego.existeOtroJuegoDeEquipoPorFechaYHora(this.juego, this.juego.getEquipo(), this.fecha, this.hora)){
		 
			Messagebox.show("El Equipo ya tiene un Juego Planificado en ese Dia y Hora", 
	                       "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/*Validar que el Equipo ya no Tenga una Practica Planificada*/
        else if(servicioPractica.isEquipoPracticaFechaHora(this.juego.getEquipo(), this.fecha, this.hora)){
			
			Messagebox.show("El Equipo ya tiene una Practica Planificada en ese Dia y Hora", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}/*Validar que el Area este en el horario indicado*/
        else if(this.area!=null && !this.area.getNombre().equalsIgnoreCase("OTRO") && 
        		servicioJuego.isAreaOcupadaHorario(this.area, this.fecha, this.hora)){
        	
        	Messagebox.show("El Area no se encuentra disponible a esa hora", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }
		/*Validar que el Area no tenga un juego Planificado*/
        else if(this.area!=null && !this.area.getNombre().equalsIgnoreCase("OTRO") &&
                (servicioJuego.existeOtroJuegoDeAreaPorFechaYHora(this.juego, this.area, this.fecha, this.hora))){
        	
        	Messagebox.show("El Area ya tiene un Juego Planificado en ese Dia y Hora", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }/*Validar que el Area no tenga una practica Planificada*/
        else if(this.area!=null && !this.area.getNombre().equalsIgnoreCase("OTRO") && 
        		servicioPractica.isAreaOcupadaFechaHora(this.area, this.fecha, this.hora)){
        	
        	Messagebox.show("El Area ya tiene una Practica Planificada en ese Dia y Hora", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }/*Valida que los Arbitros No se Repitan*/
        else if(servicioJuego.existeArbitrosRepetidos(this.listaArbitroJuego)){
        	
        	Messagebox.show("Los Arbitro Seleccionados No Pueden Repetirse", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        	
        }/*Inicio de Operacion*/
        else{
        	
        	this.juego.setFecha(this.fecha);
        	
        	this.juego.setHoraInicio(this.hora);
        	
        	this.juego.setEquipoContrario(this.equipoContrario);
        	
        	this.juego.setEscuelaContraria(this.escuelaContraria);
        	
        	this.juego.setObservacion(this.observacion);
        	
        	if(this.area!=null && !this.area.getNombre().equalsIgnoreCase("OTRO")){
        		
         	   juego.setArea(this.area);
         	   
         	}else{
         		
         		juego.setArea(null);
         		
         	}
        	
        	this.juego.setLugar(this.lugar);
        	
        	this.juego.setAnotador(this.anotador);
        	
        	Set<ArbitroJuego> arbitrosJuego= new HashSet<ArbitroJuego>(this.listaArbitroJuego);
        	
        	this.juego.setArbitroJuego(arbitrosJuego);
        	
            this.window= win;
        	
            this.tabInformacion= tabInf;
        	
             try{	
        		
        		Messagebox.show("Desea Guardar la Informacion del Juego?","Confirmacion",
        				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
        				         new org.zkoss.zk.ui.event.EventListener<Event>() {
        		                     
        			              public void onEvent(Event evt) throws InterruptedException {
        			            	  
				        		        if (evt.getName().equalsIgnoreCase("onYes")) {
				        		        	
				        		           servicioJuego.editar(juego);
				        		           
				        		           Messagebox.show("Informacion Guardada con Exito", 
				        	    	                       "Information", Messagebox.OK, 
				        	    	                        Messagebox.INFORMATION);
				        		           
				        		           BindUtils.postGlobalCommand(null, null, "paginarJuego", null);
										   window.onClose();
				        		       
				        		        }else{
				        		        	tabInformacion.setSelected(true);
				        		        	BindUtils.postGlobalCommand(null, null, "refreshEditarJuego", null);
				        		        }
        			              }
        		});
        		
        	}catch(Exception ex){
        		Messagebox.show("Error Guardar la Informacion Juego", 
    	                "Error", Messagebox.OK, Messagebox.ERROR);
        	}
        	
        }
		
	}
	
	public String formatearAreaCombo(Area area){
		String nombreArea= "OTRO";
		if (area!=null){
			nombreArea= area.getNombre();
		}
		return nombreArea;
	}
	
	public String obtenerNombreCompleto(Persona persona){
    	String nombreCompleto="";
    	
    	if (persona!=null){
    		nombreCompleto= persona.getNombre() + " " + persona.getApellido(); 
    	}
    	
    	return nombreCompleto;
    }


	@Command
    @NotifyChange({"listaAnotadores","anotador","listaArbitros","listaArbitroJuego","tabCambiado"})
    public void cambiarArbitroAnotador(@SelectorParam("#tabGestionDeportivaEditarJuegoInformacion")Tab tabInf,
    		                   @SelectorParam("#dtb_gestiondeportiva_editarjuego_informacion_fecha")Datebox date,
    		                   @SelectorParam("#tmb_gestiondeportiva_editarjuego_informacion_horainicio")Timebox time){
    	
    	this.tabCambiado=true;
    	
    	if (!date.isValid() || !time.isValid()){
    		Messagebox.show("Seleccione una Fecha y una Hora Valida para el Juego");
    		tabInf.setSelected(true);
    		this.anotador=null;
    	}else{
    		this.listaAnotadores= servicioJuego.buscarAnotadoresDisponibles(this.fecha,this.hora);
    		this.listaArbitros= servicioJuego.buscarArbitrosDisponibles(this.fecha, this.hora);
    		this.listaArbitrosOrig= servicioJuego.buscarArbitrosDisponibles(this.fecha, this.hora);
    		 
    		
    		if(this.juego.getFecha().equals(this.fecha)){
	    	
    			if (this.anotador!=null && (!servicioJuego.existeOtroJuegoDeAnotadorPorFechaYHora(this.juego,this.anotador, this.fecha, this.hora) ||
    					this.juego.getHoraInicio().equals(this.hora))){
    				
    				/**************Para insertar o quitar el Anotador del Juego en la lista del Combo ***************/
    				
    				boolean contieneAnotador= false;
    				
    				for (Anotador anotador: this.listaAnotadores){
    					
    					if (anotador.getId().equals(this.juego.getAnotador().getId())){
    						contieneAnotador= true;
    						break;
    					}
    				}
    				
    				if (!contieneAnotador){
    					this.listaAnotadores.add(this.juego.getAnotador());
    					this.anotador=this.juego.getAnotador();
    				}
    				
    			
    				/***********************************************************************************/
    				
	    			
	    		}else{
	    			this.anotador=null;
	    		}
    			
    			
    			
    			for(ArbitroJuego aj:this.listaArbitroJuego){
    				if (aj.getArbitro()!=null && (!servicioJuego.existeOtroJuegoDeArbitroPorFechaYHora(juego, aj.getArbitro(), this.fecha, this.hora) ||
    						this.juego.getHoraInicio().equals(this.hora))){
    					
    					boolean contieneArbitro= false;
        				Arbitro arbitroTmp= null;
    					
        				for (Arbitro arbitro: this.listaArbitros){
        					
        					arbitroTmp= aj.getArbitro();
        					
        					contieneArbitro= false;
        					
        					if (arbitro.getId().equals(aj.getArbitro().getId())){
        						contieneArbitro= true;
        						break;
        					}
        					
        					
        				}
        				
        				if (!contieneArbitro){
        					this.listaArbitros.add(arbitroTmp);
        				    this.listaArbitrosOrig.add(arbitroTmp);
        				    aj.setArbitro(arbitroTmp);
        				}
    					
    					
    					
    				}
    				else{
    					aj.setArbitro(null);
    				}
    			}
    			
    		}
    		
    	}
    	
    }
	
	
	@Command
    @NotifyChange("listaArbitros")
    public void actualizarArbitros(){
    
    	if (this.listaArbitrosOrig!=null){
    	   this.listaArbitros= new ArrayList<Arbitro>(this.listaArbitrosOrig);	
    	}
    	
    	for (ArbitroJuego aj:this.listaArbitroJuego){
    		if (aj.getArbitro()!=null){
    			this.listaArbitros.remove(aj.getArbitro());
    		}
    	}
    	
    }
	
	public Constraint obtenerRestriccionFechaCompetencia(){
		
		Constraint restriccion= null;
		
		if (this.juego!=null){
			restriccion= new RestriccionFechaCompetencia(this.juego.getCompetencia().getFechaInicio(),
                                                         this.juego.getCompetencia().getFechaFin());
		}
			
		
		return restriccion;
		    
	}
	
	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Date getHora() {
		return hora;
	}


	public void setHora(Date hora) {
		this.hora = hora;
	}


	public Juego getJuego() {
		return juego;
	}


	public String getEscuelaContraria() {
		return escuelaContraria;
	}


	public void setEscuelaContraria(String escuelaContraria) {
		this.escuelaContraria = escuelaContraria;
	}


	public String getEquipoContrario() {
		return equipoContrario;
	}


	public void setEquipoContrario(String equipoContrario) {
		this.equipoContrario = equipoContrario;
	}


	public String getObservacion() {
		return observacion;
	}


	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}


	public List<Area> getListaAreas() {
		return listaAreas;
	}


	public Area getArea() {
		return area;
	}


	public void setArea(Area area) {
		this.area = area;
	}


	public String getLugar() {
		return lugar;
	}


	public void setLugar(String lugar) {
		this.lugar = lugar;
	}


	public boolean isSoloLecturaLugar() {
		return soloLecturaLugar;
	}
	
	
	public Anotador getAnotador() {
		return anotador;
	}


	public void setAnotador(Anotador anotador) {
		this.anotador = anotador;
	}


	public List<Anotador> getListaAnotadores() {
		return listaAnotadores;
	}


	public List<Arbitro> getListaArbitros() {
		return listaArbitros;
	}


	public List<Arbitro> getListaArbitrosOrig() {
		return listaArbitrosOrig;
	}


	public List<ArbitroJuego> getListaArbitroJuego() {
		return listaArbitroJuego;
	}


	public void setListaArbitroJuego(List<ArbitroJuego> listaArbitroJuego) {
		this.listaArbitroJuego = listaArbitroJuego;
	}




	/************************Clases Constraint Privadas************************/
	private class RestriccionFechaCompetencia implements Constraint {

		private Date fechaInicio;
		private Date fechaFin;
		
		public RestriccionFechaCompetencia(Date fechaInicio,Date fechaFin){
			
			this.fechaInicio= fechaInicio;
			
			this.fechaFin= fechaFin;
			
		}
		
		
		public void validate(Component comp, Object valor) throws WrongValueException {
			
				if (valor==null){
					throw new WrongValueException(comp, "Este Campo No puede ser Vacio");  
				}
				else if(!(valor instanceof Date)){
					throw new WrongValueException(comp, "Fecha Invalida");
				}
				else if(valor instanceof Date){
					
					Date fechaEntrada= (Date) valor;
					
					if (fechaEntrada.compareTo(fechaInicio)< 0 || 
					    fechaEntrada.compareTo(fechaFin)>0){
						throw new WrongValueException(comp, "La Fecha No se encuentra en el Rango de Tiempo de la Competencia");  
					}
					
				}
			
		}

    }
	
	/***********************************************************************************/
	

	
	
	/************************Clases Constraint Privadas************************
	private class RestriccionEquipoOcupado implements Constraint {

		public void validate(Component comp, Object arg1)throws WrongValueException {
		    Equipo equipo= getEquipoSeleccionado();
		    
			if (equipo!=null && getFecha()!=null && getHora()!=null){
				if (servicioJuego.isEquipoJuegoFechaHora(equipo, fecha, hora)){
					throw new WrongValueException(comp, "El Equipo ya tiene un Juego en ese Rango de Tiempo");  
				}
				else if (servicioPractica.isEquipoPracticaFechaHora(equipo, fecha, hora)){
					throw new WrongValueException(comp, "El Equipo ya tiene una Practica en ese Rango de Tiempo");  
				}
			}
			else if(equipo==null){
				throw new WrongValueException(comp, "Por Favor Elija un Equipo");
			}
			else if(getFecha()==null){
				throw new WrongValueException(comp, "Por Favor Elija una Fecha");
			}
			else if(getHora()==null){
				throw new WrongValueException(comp, "Por Favor Elija una Hora");
			}
		}
    }
	
	private class RestriccionAreaOcupada implements Constraint {
        
		
		
		public void validate(Component comp, Object arg1)throws WrongValueException {
		    Area area= getAreaSeleccionada();
		    
			
			if (area!=null && getFecha()!=null && getHora()!=null){
				
				if (servicioJuego.isAreaOcupadaFechaHora(area, fecha, hora)){
					throw new WrongValueException(comp, "El Area ya esta Ocupada con un Juego en ese Dia y Hora");  
				}
				else if (servicioPractica.isAreaOcupadaFechaHora(area, fecha, hora)){
					throw new WrongValueException(comp, "El Area ya esta Ocupada con una Practica en ese Dia y Hora");  
				}
				
			}
			else if(area==null){
				throw new WrongValueException(comp, "Por Favor Elija un Area");
			}
			else if(getFecha()==null){
				throw new WrongValueException(comp, "Por Favor Elija una Fecha");
			}
			else if(getHora()==null){
				throw new WrongValueException(comp, "Por Favor Elija una Hora");
			}
			
		}
    }
	/**************************************************************************/
	
	
	
	
}
