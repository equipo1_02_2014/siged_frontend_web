package edu.ucla.siged.viewmodel.gestiondeportiva;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;


public class VMListaJuegosFuturos {
	
	@WireVariable
	ServicioJuegoI servicioJuego;
	
	@WireVariable
	Juego juego;
	@WireVariable
	List<Juego> listaJuegos;
	
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	String equipoContrario="";
	Date fecha;
	String lugar="";
	boolean estatusFiltro;
	
	
//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	
	
	public Juego getJuego() {
		return juego;
	}
	public void setJuego(Juego juego) {
		this.juego = juego;
	}
	
	public List<Juego> getListaJuegos() {
		
		return listaJuegos;
	}
	
	public void setListaJuego(List<Juego> listaJuegos) {
		this.listaJuegos = listaJuegos;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioJuego.obtenerTamanioPagina();
		return tamanoPagina;
	}
	
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	public String getEquipoContrario() {
		return equipoContrario;
	}
	public void setEquipoContrario(String equipoContrario) {
		this.equipoContrario = equipoContrario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getLugar() {
		return lugar;
	}
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	
	
////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	
@Init
public void init(){

  this.listaJuegos = servicioJuego.buscarJuegosFuturos(0).getContent();
  this.registrosTotales= servicioJuego.obtenerCantidadJuegosFuturos();
  


}

//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////


@GlobalCommand
@NotifyChange({"listaJuegos","registrosTotales","paginaActual"})
public void paginar(){
//if (estatusFiltro==false){
  this.listaJuegos = servicioJuego.buscarJuegosFuturos(this.paginaActual).getContent();
  this.registrosTotales=servicioJuego.obtenerCantidadJuegosFuturos();

//}else{

//ejecutarFiltro();
}


public String formatearFecha(Date fecha){
	String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
	return fechaFormateada;
}

public String obtenerEquiposJuego(Juego juego){
	String nombreEquipos="";
	
	if (juego!=null){
		nombreEquipos= juego.getEquipoContrario()  + " & " + juego.getEquipo().getNombre(); 
	}
	
	return nombreEquipos;
}


}
