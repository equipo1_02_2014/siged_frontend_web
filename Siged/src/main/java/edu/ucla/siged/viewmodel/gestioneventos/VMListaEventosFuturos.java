package edu.ucla.siged.viewmodel.gestioneventos;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioEventoI;

public class VMListaEventosFuturos {
	
	@WireVariable
	ServicioEventoI servicioEvento;
	
	@WireVariable
	Evento evento;
	@WireVariable
	List<Evento> listaEventos;
	
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	String nombre="";
	Date fecha;
	String direccion="";
	boolean estatusFiltro;
	
	
//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	
	
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	public List<Evento> getListaEventos() {
		
		return listaEventos;
	}
	
	public void setListaEvento(List<Evento> listaEventos) {
		this.listaEventos = listaEventos;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioEvento.obtenerTamanioPagina();
		return tamanoPagina;
	}
	
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	

@Init
public void init(){

  this.listaEventos = servicioEvento.buscarEventosFuturos(0).getContent();
  this.registrosTotales= servicioEvento.obtenerCantidadEventosFuturos();
  


}

//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////


@GlobalCommand
@NotifyChange({"listaEventos","registrosTotales","paginaActual"})
public void paginar(){
//if (estatusFiltro==false){
  this.listaEventos = servicioEvento.buscarEventosFuturos(this.paginaActual).getContent();
  this.registrosTotales=servicioEvento.obtenerCantidadEventosFuturos();

//}else{

//ejecutarFiltro();
}


public String formatearFecha(Date fecha){
	String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
	return fechaFormateada;
}




}

