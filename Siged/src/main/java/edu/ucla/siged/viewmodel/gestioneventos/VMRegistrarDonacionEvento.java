package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioEvento;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMRegistrarDonacionEvento {
	@WireVariable
	private ServicioEvento servicioEvento;
	
	List<Evento> listaEventos = new ArrayList<Evento>();
	
	private Window window;
	
	private int paginaActual;
	
	private int tamanoPagina = servicioEvento.TAMANIO_PAGINA;
	
	private long registrosTotales;
	
	//para el filtro
	private String nombre = "";
	private Date fecha;
	private boolean estatusFiltro;
	
	public VMRegistrarDonacionEvento(){
			
	}
	
	public List<Evento> getListaEventos() {
		return listaEventos;
	}
	
	public void setListaEventos(List<Evento> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Init
	public void init(){
		this.listaEventos = servicioEvento.EventosRealizadosSinDonacion(0, 5);
		this.registrosTotales = servicioEvento.TotalEventosRealizadosSinDonacion();
	}
	
	@Command
	public void addDonacion(@BindingParam("eventoSeleccionada") Evento eventoSeleccionada) {
		 Map<String,Evento> mapEvento = new HashMap<String, Evento>();
		 mapEvento.put("evento", eventoSeleccionada);
		 window = (Window) Executions.createComponents("/vistas/gestionevento/donacionevento.zul", null, mapEvento);
		 window.doModal();
	}
	
	@GlobalCommand
	@NotifyChange({"listaEventos","registrosTotales","paginaActual"})
	public void paginarEventos(){
		if (estatusFiltro==false){
			this.listaEventos = servicioEvento.EventosRealizadosSinDonacion(this.paginaActual, 5);
			this.registrosTotales = servicioEvento.TotalEventosRealizadosSinDonacion();
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@Command
	@NotifyChange({"listaEventos","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		this.estatusFiltro=true;  //el filtro se ha activado
		
		if (fecha == null)
			jpql = " nombre like '%"+nombre+"%'";
		else
		    jpql = " nombre like '%"+nombre+"%' and fecha = '"+fecha+"'";
			
		this.listaEventos = servicioEvento.buscarEventosRealizadosSinDonacion(jpql, 0, 5);
		this.registrosTotales = servicioEvento.totalEventosRealizadosSinDonacion(jpql);
	}
	
	@Command
	@NotifyChange({"listaEventos","registrosTotales","paginaActual","nombre","fecha"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginarEventos();
		// las variables siguientes son del filtro
		nombre="";
		fecha=null;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
}
