package edu.ucla.siged.viewmodel.reportes;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.http.SimpleWebApp;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.reporte.PromedioCantidadBase;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;

public class VMRelacionJugadosGanados {
	
	    @WireVariable
	    private ServicioCategoriaI servicioCategoria;
	    
	    @WireVariable
	    private ServicioRangoI servicioRango;
	    
	    @WireVariable
	    private ServicioReporteEstadisticoI servicioReporteEstadistico;
	
	    private boolean vistaGraficaTorta;
		
		private boolean vistaGraficaBarra;
		
		private boolean vistaGraficaLinea;
		
		private boolean vistaLista;
		
		private boolean formatoPdf;
		
		private boolean formatoXls;
		
		private Date fechaDesde;
		
		private Date fechaHasta;
		
		private Rango rangoSeleccionado;
		
		private Categoria categoriaSeleccionada;
		
		private Integer cantidadEquipoSeleccionada;
		
		private String pathProyecto;
		
		private List<Rango> listaRango;
		
		private List<Categoria> listaCategoria;
		
		private List<Integer> listaCantidadEquipos;
		
		private Window window;
		
		
		
		@Init
		public void init(){
		
			this.window= null;
			
			this.listaCategoria= servicioCategoria.buscarTodos();
			
			this.listaRango= servicioRango.buscarTodos();
			
			this.listaCantidadEquipos= new ArrayList<Integer>();
			
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMRelacionJugadosGanados.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			
			//this.pathProyecto="C:\Users\Veronica\workspace 2801\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\Siged";
			
			
			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			
			for (int i=1; i<=5;++i){
				this.listaCantidadEquipos.add(i);
			}
			
			this.inicializarCampos();
		}
		
		

		@NotifyChange({"vistaGraficaTorta","vistaGraficaBarra","vistaGraficaLinea","vistaLista",
			           "formatoPdf","formatoXls","fechaDesde","fechaHasta","categoriaSeleccionada",
			           "rangoSeleccionado","cantidadEquipoSeleccionada"})
		public void inicializarCampos(){
			
			this.vistaGraficaTorta=true;
			this.vistaGraficaBarra=false;
			this.vistaGraficaLinea= false;
			this.vistaLista=false;
			
			this.formatoPdf=true;
			this.formatoXls=false;
			
			this.fechaDesde=null;
			this.fechaHasta=null;
			
			this.cantidadEquipoSeleccionada= 3;
			
			this.categoriaSeleccionada=null;
			
			this.rangoSeleccionado= null;
		}
		
		@Command
		@NotifyChange({"vistaGraficaTorta","vistaGraficaBarra","vistaGraficaLinea","vistaLista",
	           "formatoPdf","formatoXls","fechaDesde","fechaHasta","categoriaSeleccionada",
	           "rangoSeleccionado","cantidadEquipoSeleccionada"})
		public void cancelar(){
			
			this.inicializarCampos();
		}
		
		
		@Command
		@NotifyChange({"vistaGraficaTorta","vistaGraficaBarra","vistaGraficaLinea","vistaLista"})
		public void seleccionarVista(@BindingParam("vista")String vista){
			
			this.vistaGraficaTorta=false;
			this.vistaGraficaBarra=false;
			this.vistaGraficaLinea= false;
			this.vistaLista=false;
			
			if (vista.equalsIgnoreCase("Promedio de Juegos Ganados")){
				this.vistaGraficaTorta=true;
			}
			else if(vista.equalsIgnoreCase("Relacion Juegos Jugados/Ganados")){
				this.vistaGraficaBarra=true;
			}
			else if(vista.equalsIgnoreCase("Grafica de Lineas")){
				this.vistaGraficaLinea=true;
			}
			else if(vista.equalsIgnoreCase("Lista")){
				this.vistaLista=true;
			}
			
		}
		
		@Command
		@NotifyChange({"formatoPdf","formatoXls"})
		public void seleccionarFormato(@BindingParam("formato")String formato){
			
			this.formatoPdf=false;
			this.formatoXls=false;
			
			if (formato.equalsIgnoreCase("PDF")){
				this.formatoPdf=true;
			}
			else if(formato.equalsIgnoreCase("XLS")){
				this.formatoXls=true;
			}
			
		}
		
		public boolean validarPeriodoApertura(){
			boolean valido=true;
			
			
            if (this.fechaDesde==null && this.fechaHasta!=null){
				
            	valido= false;
				
            	Messagebox.show("El Campo Desde es Invalido" + System.lineSeparator() + "Coloque una Fecha Valida en el Campo Desde o deje el campo Desde y Hasta Vacios", 
                        "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				
				
				
			}else if(this.fechaHasta==null && this.fechaDesde!=null){
				
				valido=false;
				
				Messagebox.show("El Campo Hasta es Invalido" + System.lineSeparator() + "Coloque una Fecha Valida en el Campo Hasta o deje el campo Desde y Hasta Vacios", 
                        "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				
			}else if ((this.fechaDesde!=null && this.fechaHasta!=null) && this.fechaDesde.after(this.fechaHasta)){
				
				valido=false;
				
				Messagebox.show("El Rango de las Fechas es Invalido", 
		                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
	        }
					
			
			return valido;
		}
		
	
		
		@Command
		public void imprimir(){		
			
			if (this.validarPeriodoApertura()){ 
			
				String restricciones="";
				
				if((this.fechaDesde!=null && this.fechaHasta!=null) ||
					 this.rangoSeleccionado!=null || this.categoriaSeleccionada!=null){
					
					
						if (this.fechaDesde!=null && this.fechaHasta!=null){
							
							String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
							String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
							
							restricciones += "AND equipo.fechacreacion BETWEEN '" + desde + "' AND '" + hasta + "'";
							
						}
						
						if (this.categoriaSeleccionada!=null){
							restricciones += "AND equipo.idcategoria=" + this.categoriaSeleccionada.getId();
						}
						
						if (this.rangoSeleccionado!=null){
							restricciones += "AND equipo.idrango=" + this.getRangoSeleccionado().getId();
						}
					
				}
				
				
				List<PromedioCantidadBase> lista= this.servicioReporteEstadistico.buscarEquiposMayorPromedioJuegosGanados(restricciones,this.cantidadEquipoSeleccionada);
			
				
				
				if (!lista.isEmpty()){
					
					Map<String,Object> parameterMap = new HashMap<String,Object>();
					
					JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
					
					parameterMap.put("datasource", JRdataSource);
					
					parameterMap.put("titulo","Promedio de Juegos Ganados por Equipo");
					
					parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
					
					parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
						
					try {			
						
						JasperDesign jasDesign=null;
						
						if(this.vistaGraficaTorta){
						   jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "equipoCantidadPromedioJuegosGanadosTorta.jrxml");
						}else{
							jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "equipoCantidadPromedioJuegosGanadosBarra.jrxml");
						}
						
						JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
				        
				        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
				        
				        JasperViewer.viewReport(jasperPrint,false);
						
					} catch (JRException e) {
						e.printStackTrace();
						Messagebox.show(e.getMessage());
					}
				
				

				}else{
					Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			
			}
			
			     
		}
		

		public boolean isVistaGraficaTorta() {
			return vistaGraficaTorta;
		}

		public void setVistaGraficaTorta(boolean vistaGraficaTorta) {
			this.vistaGraficaTorta = vistaGraficaTorta;
		}

		public boolean isVistaGraficaBarra() {
			return vistaGraficaBarra;
		}

		public void setVistaGraficaBarra(boolean vistaGraficaBarra) {
			this.vistaGraficaBarra = vistaGraficaBarra;
		}

		public boolean isVistaGraficaLinea() {
			return vistaGraficaLinea;
		}

		public void setVistaGraficaLinea(boolean vistaGraficaLinea) {
			this.vistaGraficaLinea = vistaGraficaLinea;
		}

		public boolean isVistaLista() {
			return vistaLista;
		}

		public void setVistaLista(boolean vistaLista) {
			this.vistaLista = vistaLista;
		}

		public boolean isFormatoPdf() {
			return formatoPdf;
		}

		public void setFormatoPdf(boolean formatoPdf) {
			this.formatoPdf = formatoPdf;
		}

		public boolean isFormatoXls() {
			return formatoXls;
		}

		public void setFormatoXls(boolean formatoXls) {
			this.formatoXls = formatoXls;
		}

		public Date getFechaDesde() {
			return fechaDesde;
		}

		public void setFechaDesde(Date fechaDesde) {
			this.fechaDesde = fechaDesde;
		}

		public Date getFechaHasta() {
			return fechaHasta;
		}

		public void setFechaHasta(Date fechaHasta) {
			this.fechaHasta = fechaHasta;
		}

		public Rango getRangoSeleccionado() {
			return rangoSeleccionado;
		}

		public void setRangoSeleccionado(Rango rangoSeleccionado) {
			this.rangoSeleccionado = rangoSeleccionado;
		}

		public Categoria getCategoriaSeleccionada() {
			return categoriaSeleccionada;
		}

		public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) {
			this.categoriaSeleccionada = categoriaSeleccionada;
		}
		
		public Integer getCantidadEquipoSeleccionada() {
			return cantidadEquipoSeleccionada;
		}

		public void setCantidadEquipoSeleccionada(Integer cantidadEquipoSeleccionada) {
			this.cantidadEquipoSeleccionada = cantidadEquipoSeleccionada;
		}

		public List<Rango> getListaRango() {
			return listaRango;
		}

		public List<Categoria> getListaCategoria() {
			return listaCategoria;
		}

		public List<Integer> getListaCantidadEquipos() {
			return listaCantidadEquipos;
		}
		
		
        
		
}
