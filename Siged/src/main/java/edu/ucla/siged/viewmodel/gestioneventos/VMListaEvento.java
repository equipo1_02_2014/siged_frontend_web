/** 
 * 	VMListaEvento
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioAdjuntoEvento;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioEvento;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMListaEvento {
	
	@WireVariable
	private ServicioEvento servicioEvento;
	@WireVariable
	private ServicioAdjuntoEvento servicioAdjuntoEvento;
	@WireVariable
	Evento evento;
	private List<Evento> eventosSeleccionados = new ArrayList<Evento>();
	private List<AdjuntoEvento> adjuntoEventosSeleccionados = new ArrayList<AdjuntoEvento>();
	private Window window;
	private int paginaActual;
	private int tamanoPagina;
	private long registrosTotales;
	private short tipoOperacion;
	List<Evento> listaEventos = new ArrayList<Evento>();
	private Date fecha;
	private boolean estatusFiltro;
	
	public VMListaEvento(){}
	
	@Init
	public void init() {
		this.listaEventos = servicioEvento.buscarTodosProximos(0);
		this.registrosTotales = servicioEvento.totalEventosProximos();
	}
	
	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	public List<Evento> getEventosSeleccionados() {
		return eventosSeleccionados;
	}

	public void setEventosSeleccionados(List<Evento> eventosSeleccionados) {
		this.eventosSeleccionados = eventosSeleccionados;
	}

	public List<AdjuntoEvento> getAdjuntoEventosSeleccionados() {
		return adjuntoEventosSeleccionados;
	}

	public void setAdjuntoEventosSeleccionados(
			List<AdjuntoEvento> adjuntoEventosSeleccionados) {
		this.adjuntoEventosSeleccionados = adjuntoEventosSeleccionados;
	}
	
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina = servicioEvento.TAMANIO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public List<Evento> getListaEventos() {
		return listaEventos;
	}

	public void setListaEventos(List<Evento> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public boolean isEstatusFiltro() {
		return estatusFiltro;
	}

	public void setEstatusFiltro(boolean estatusFiltro) {
		this.estatusFiltro = estatusFiltro;
	}

	@Command
	@NotifyChange({"listaEventos","registrosTotales"})
	public void ejecutarFiltro(){
//		String jpql;
//		this.estatusFiltro=true;
//		if (fecha == null)
//			jpql = "";//" autor like '%"+autor+"%' and titulo like '%"+titulo+"%'";
//		else
//		    jpql = "";//" autor like '%"+autor+"%' and titulo like '%"+titulo+"%' and fecha = '"+fecha+"'";
//		this.listaEventos = servicioEvento.buscarFiltrado(jpql, paginaActual);
//		this.registrosTotales = servicioEvento.totalEventosFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"listaEventos","registrosTotales","paginaActual"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		//autor=""; 
		//titulo="";
		fecha=null;
	}
	
	@GlobalCommand
	@NotifyChange({"listaEventos","registrosTotales","paginaActual"})
	public void paginar(){
//		if (estatusFiltro==false){
			this.listaEventos = servicioEvento.buscarTodosProximos(paginaActual);
			this.registrosTotales = servicioEvento.totalEventosProximos();
//		}else{
//			ejecutarFiltro();
//		}
	}
	
	@GlobalCommand
	@NotifyChange({"listaEventos","registrosTotales","paginaActual","eventosSeleccionados"})
	public void actualizarLista(){
//		int ultimaPagina=0;
//		if(this.tipoOperacion==1){
//			this.getRegistrosTotales();
//			if((this.registrosTotales+1)%this.tamanoPagina==0)
//				ultimaPagina = ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
//			else
//				ultimaPagina = ((int) (this.registrosTotales+1)/this.tamanoPagina);
//			this.paginaActual=ultimaPagina;
//		}
//		paginar();
//		if(this.tipoOperacion==3 || this.tipoOperacion==4){
//			this.getRegistrosTotales();
//			if((this.registrosTotales+1)%this.tamanoPagina==0)
//				ultimaPagina = ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
//			else
//				ultimaPagina = ((int) (this.registrosTotales-1)/this.tamanoPagina);
//			this.paginaActual=ultimaPagina;
//			paginar();
//		}
		paginaActual=0;
		this.listaEventos = servicioEvento.buscarTodosProximos(paginaActual);
		this.registrosTotales = servicioEvento.totalEventosProximos();
		
	}
	
	@Command
	@NotifyChange({"evento"})
	public void editarEvento(@BindingParam("eventoSeleccionado") Evento eventoSeleccionado) {
		this.evento = eventoSeleccionado;
		this.tipoOperacion=2;
		 Map<String,Evento> mapEvento = new HashMap<String,Evento>();
		 mapEvento.put("evento", this.evento);
		 window = (Window) Executions.createComponents("/vistas/gestionevento/evento.zul", null, mapEvento);
		 window.doModal();
	}
		
	@Command
	public void agregarEvento() {
		this.tipoOperacion=1;
		window = (Window) Executions.createComponents("/vistas/gestionevento/evento.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange({"listaEventos"})
	public void eliminarEvento(@BindingParam("eventoSeleccionado") final Evento eventoSeleccionado){
		this.tipoOperacion=3;
		Messagebox.show("�Est� seguro que desea eliminar este registro?","Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new EventListener<Event>(){
					public void onEvent(Event event) throws Exception{
						if(Messagebox.ON_YES.equals(event.getName())){
							servicioEvento.Eliminar(eventoSeleccionado);
							BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
							Messagebox.show("El registro ha sido eliminado exitosamente","Informaci�n",Messagebox.OK,Messagebox.INFORMATION);
						}
					}
		});
		actualizarLista();
	}
	
	@Command
	@NotifyChange("listaEventos")
	public void eliminarEventos(@BindingParam("lista") Listbox lista){
		this.tipoOperacion=4;
		if(lista.getSelectedItems().size()>0){
			Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?","Pregunta",Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				new EventListener<Event>(){
				public void onEvent(Event event) throws Exception{
					if(Messagebox.ON_YES.equals(event.getName())){
						servicioEvento.EliminarVarios(eventosSeleccionados);
						BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
						Messagebox.show("Los registros seleccionados se han eliminado exitosamente","Informaci�n",Messagebox.OK,Messagebox.INFORMATION);
						eventosSeleccionados=null;
					}
				}
			});
		}
		else{
			Messagebox.show("Debe seleccionar los registros que desea eliminar", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}

	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
	
	@Command
	public String cambiarFormatoHora(Date hora){
		String horaFormateada="";
		horaFormateada= new SimpleDateFormat("hh:mm a").format(hora);
		return horaFormateada;
	}

}
