/** 
 * 	VMHorarioArea
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioHorarioAreaI;

public class VMHorarioArea {
	
	short tipoOperacion;
	Window window;
	@WireVariable
	ServicioHorarioAreaI servicioHorarioArea;
	@WireVariable HorarioArea horarioArea;
	List<Integer> listadias;
	List<Boolean> listaDisponibilidad;
	private boolean instanciado = false;
	
	public List<Integer> getListaDias(){
		return listadias;
	}
	
	public HorarioArea getHorarioArea() {
		return horarioArea;
	}

	public void setHorarioArea(HorarioArea horarioArea) {
		this.horarioArea = horarioArea;
	}

	public List<Boolean> getListaDisponibilidad() {
		return listaDisponibilidad;
	}

	public void setListaDisponibilidad(List<Boolean> listaDisponibilidad) {
		this.listaDisponibilidad = listaDisponibilidad;
	}
	

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("horarioArea") HorarioArea horarioAreaSeleccionado){
		 listadias= new ArrayList<Integer>();	     
	     listadias.add(1);
	     listadias.add(2);
	     listadias.add(3);
	     listadias.add(4);
	     listadias.add(5);
	     listadias.add(6);
	     listadias.add(7);
	     listaDisponibilidad= new ArrayList<Boolean>();
	     listaDisponibilidad.add(true);
	     listaDisponibilidad.add(false);
	     
		if(horarioAreaSeleccionado!=null){
			this.horarioArea = horarioAreaSeleccionado;
			instanciado = true;
		}else{
			this.horarioArea= new HorarioArea(new Date(), new Date(), 0,"",null);
		}
			
		}
		
	@Command
	@NotifyChange({"horarioArea","horainicio","horafin","dia","observacion","disponible"})
	public void  cargarHorarioArea(){
			Messagebox.show("El Horario se ha guardado, desea agregar otro", 
				"Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new org.zkoss.zk.ui.event.EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						// TODO Auto-generated method stub
						if(event.getName().equalsIgnoreCase("onYes")){
							BindUtils.postGlobalCommand(null, null, "limpiarHorario", null);
						
						}else{
							Window myWindow = (Window) Path.getComponent("/vistaHorario");
							myWindow.detach();
							
						}
					}
		});
	}

						
	
	public void cerrarVentana(){
		Window myWindow = (Window) Path.getComponent("/vistaHorario");
		myWindow.detach();
	}

	
	@GlobalCommand("limpiarHorario")
	@NotifyChange({"horarioArea"})
	public void limpiarHorario(){
		horarioArea.setId(null);
		horarioArea.setObservacion("");
		horarioArea.setHoraInicio(null);
		horarioArea.setHoraFin(null);
		horarioArea.setDisponible(null);
		horarioArea.setDia(null);
		
	}
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("horarioAreaSeleccionado") HorarioArea horarioAreaSeleccionado ) {
		Window window = (Window)Executions.createComponents("area.zul", null, null);
		this.horarioArea = horarioAreaSeleccionado;
		window.doModal();
	}
	
	@Command
	@NotifyChange({"horarioArea"})
	public String verDias(Integer dia){
		String di;
		switch (dia){
		case 1:
			di="Domingo";
		    break;
		case 2:
			di="Lunes";
		    break;
		case 3:
			di="Martes";
		    break;
		case 4:
			di="Miercoles";
		    break; 
		case 5:
			di="Jueves";
		    break;
		case 6:
			di="Viernes";
		    break;	
		default:
			di="Sabado";
		}
		return di;
		
	}
	
	@Command
	public String verDisponibilidad(Boolean disponible){
		String disponibilidad="";
		if (disponible==true)
			disponibilidad="SI";
		else if (disponible==false)
			disponibilidad="NO";
		return disponibilidad;
	}
	

}
