/** 
 * 	VMCategoria
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.LinkedList;
import java.util.List;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;

public class VMCategoria {
	@WireVariable
	ServicioCategoriaI servicioCategoria;
	@WireVariable
	Categoria categoria;
	@WireVariable
	List<Short> listaEstatus;
	Short estatusSeleccionado;
	private Window ventanaCategoria;
	short tipoOperacion;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////
	
	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public Window getVentanaCategoria() {
		return ventanaCategoria;
	}

	public void setVentanaCategoria(Window ventanaCategoria) {
		this.ventanaCategoria = ventanaCategoria;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Short> getListaEstatus() {
		return listaEstatus;
	}

	/////////////////////////////////////// INIT////////////////////////////////////////////////////////
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("categoria") Categoria categoriaSeleccionada) {
        if (categoriaSeleccionada!=null){
        	this.tipoOperacion=2;
            this.categoria = categoriaSeleccionada;
            this.estatusSeleccionado= this.categoria.getEstatus();
        }else{
        	categoria = new Categoria();
        	this.tipoOperacion=1;
        }
	}
	
	////////////////////////////////////////////GUARDAR/////////////////////////////////////////////////////////////
	@Command
	@NotifyChange({"categoria","limpiar","estatusSeleccionado","actualizarLista"})
	public void guardarCategoria(@SelectorParam(".categoria_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaCategoria") Window ventanaCategoria){
		this.ventanaCategoria = ventanaCategoria;
	
		if (this.estatusSeleccionado!=null)
			categoria.setEstatus(estatusSeleccionado);
		
        boolean camposValidos=true;
		
        for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
			break;
			}
		}
		if(!camposValidos || categoria.getNombre()==null || categoria.getNombre().isEmpty() ||
				categoria.getEdadMaxima()==null || categoria.getEdadMinima()==null){
			Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
		}
		else{
			if(categoria.getEdadMaxima()<0 || categoria.getEdadMinima()<0){
				Messagebox.show("Las edades no pueden contener valores negativos","Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			else{
				if(categoria.getEdadMaxima() <= categoria.getEdadMinima()){
					Messagebox.show("La edad m�xima no puede ser menor o igual que la edad m�nima","Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				}
			else{
				if(categoria.getEdadMaxima()==0 || categoria.getEdadMinima()==0){
					Messagebox.show("Las edades no pueden contener el valor 0.","Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				}
				else{
					if(tipoOperacion==1){
						categoria.setEstatus(Short.parseShort("1"));
						servicioCategoria.guardar(categoria);
						Messagebox.show("El registro se ha guardado exitosamente","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
						
						Messagebox.show("�Desea agregar otro registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
	       					new EventListener<Event>() {
	       				
	       						public void onEvent(Event event) throws Exception {
	                                
	       							if(Messagebox.ON_NO.equals(event.getName())){
	       								
	       								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       								cerrarVentanaCategoria();
	       								
	       							}
	       							else BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       						}
	       					});
					
					limpiar();
					
					}
					
					if(tipoOperacion==2){
					categoria.setEstatus(Short.parseShort("1"));
					servicioCategoria.guardar(categoria);
					Messagebox.show("Los cambios se han guardado exitosamente","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
					cerrarVentanaCategoria();
					}
				 }
			}
		}
	  }
    }

	////////////////////////////////////////////////LIMPIAR///////////////////////////////////////////////
	@Command
	@NotifyChange({"categoria","estatusSeleccionado"})
	public void limpiar(){
		categoria.setId(null);
		categoria.setNombre("");
		categoria.setEdadMinima(0);
		categoria.setEdadMaxima(0);
		estatusSeleccionado=1;
	}
	
	public void cerrarVentanaCategoria(){
		ventanaCategoria.detach();
	}
	
	///////////////////////////////////////////VER ESTATUS////////////////////////////////////////////////////////
	@Command
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
}