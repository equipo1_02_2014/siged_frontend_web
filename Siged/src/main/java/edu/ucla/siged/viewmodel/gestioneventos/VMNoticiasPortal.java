package edu.ucla.siged.viewmodel.gestioneventos;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.gestioneventos.OpcionPregunta;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioNoticia;

public class VMNoticiasPortal {
	
	@WireVariable
	private ServicioNoticia servicioNoticia;

	List<Noticia> listaNoticias = new ArrayList<Noticia>();
	
	private int paginaActual = 0;
	
	private int tamanoPagina=2;
	
	private long registrosTotales;
	
	public VMNoticiasPortal(){
		
	}
	
	@Init
	public void init() {
		List <Rol> roles= new ArrayList<Rol>();
		Session sess = Sessions.getCurrent();
    	roles =  (List<Rol>) sess.getAttribute("roles");
    	Usuario usuario = (Usuario)sess.getAttribute("userCredential");
    	
    	if(usuario==null || usuario.isAnonimo()){
    		this.listaNoticias = servicioNoticia.buscarTodasPublicas(this.paginaActual,2);
    		this.registrosTotales = servicioNoticia.TodasPublicas();
    	}else{
    		this.listaNoticias = servicioNoticia.buscarTodasPorRol(roles,this.paginaActual,2);
    		this.registrosTotales = servicioNoticia.TodasPorRol(roles);
    	}
	}
	
	public List<Noticia> getListaNoticias() {
		return listaNoticias;
	}

	public void setListaNoticias(List<Noticia> listanoticias) {
		this.listaNoticias = listanoticias;
	}

	
	public int getPaginaActual() {
		return paginaActual;
	}

	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	
	public int getTamanoPagina() {
		return tamanoPagina;
	}

	
	public void setTamanoPagina(int tamanoPagina) {
	this.tamanoPagina = tamanoPagina;
	}

	
	public long getRegistrosTotales() {
		return registrosTotales;
	}

	
	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}
	
	
	@GlobalCommand
	@NotifyChange({"listaNoticias","registrosTotales","paginaActual"})
	public void paginarNoticia(){
		List <Rol> roles= new ArrayList<Rol>();
		Session sess = Sessions.getCurrent();
    	roles =  (List<Rol>) sess.getAttribute("roles");
    	Usuario usuario = (Usuario)sess.getAttribute("userCredential");
    	
    	if(usuario==null || usuario.isAnonimo()){
    		this.listaNoticias = servicioNoticia.buscarTodasPublicas(this.paginaActual,2);
    		this.registrosTotales = servicioNoticia.TodasPublicas();
    	}else{
    		this.listaNoticias = servicioNoticia.buscarTodasPorRol(roles,this.paginaActual,2);
    		this.registrosTotales = servicioNoticia.TodasPorRol(roles);
    	}

	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
	}

	public boolean mostrarEnlace(Noticia noticia) {
		if(noticia==null || noticia.getEnlace()==null || noticia.getEnlace().trim().isEmpty())
			return false;
		else
			return true;
	}
	
	
	public String obtenerEnlace(Noticia noticia) {
		if(noticia==null || noticia.getEnlace()==null || noticia.getEnlace().trim().isEmpty())
			return "";
		else
			return noticia.getEnlace();
	}

	
	@Command
	public void redireccionarEnlace(@BindingParam("noticiaSeleccionada") Noticia noticia){
		 try {

			Executions.getCurrent().sendRedirect("http://"+noticia.getEnlace(), "New Window");
		} catch (Exception e) {
			Messagebox.show("Lo sentimos, el link no est� disponible.", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}
	}

}
