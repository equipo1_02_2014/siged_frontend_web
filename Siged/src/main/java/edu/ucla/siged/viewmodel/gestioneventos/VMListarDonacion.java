package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMListarDonacion {
	
	@WireVariable
	private ServicioDonacion servicioDonacion;
	private List<Donacion> donaciones = new ArrayList<Donacion>();
	private List<Donacion> donacionesSeleccionadas = new ArrayList<Donacion>();
	private Window window;
	private int paginaActual;
	private int tamanoPagina;
	private long registrosTotales; 
	private short tipoOperacion; 
	private String rif=""; 
	private String nombre="";
	private Date fecha;
	private Integer comboEstatus=0;
	private Integer comboTipo=0;
	
	private boolean estatusFiltro;

	public VMListarDonacion() {
		
	}

	public List<Donacion> getDonaciones() {
		return donaciones;
	}

	public void setDonaciones(List<Donacion> donaciones) {
		this.donaciones = donaciones;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina = servicioDonacion.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public String getRif() {
		return rif;
	}

	public void setRif(String rif) {
		this.rif = rif;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getComboEstatus() {
		return comboEstatus;
	}

	public void setComboEstatus(Integer comboEstatus) {
		this.comboEstatus = comboEstatus;
	}

	public Integer getComboTipo() {
		return comboTipo;
	}

	public void setComboTipo(Integer comboTipo) {
		this.comboTipo = comboTipo;
	}

	public List<Donacion> getDonacionesSeleccionadas() {
		return donacionesSeleccionadas;
	}

	public void setDonacionesSeleccionadas(List<Donacion> donacionesSeleccionadas) {
		this.donacionesSeleccionadas = donacionesSeleccionadas;
	}

	@Init
	public void init() {
		this.donaciones = servicioDonacion.ListarDonacionesNE(0);
		this.registrosTotales= servicioDonacion.totalDonacionesFiltrados("tipo in (0)");
	}
	
	@Command
	public void AgregarDonacion() {
		this.tipoOperacion=1;
		window = (Window) Executions.createComponents("/vistas/gestionevento/Donacion.zul", null, null);
		window.doModal();
	}
	
	@Command
	public void EditarDonacion(@BindingParam("donacionSeleccionada") Donacion donacionSeleccionada) {
		this.tipoOperacion=2;
		Map<String,Donacion> mapDonacion = new HashMap<String, Donacion>();
		mapDonacion.put("donacion", donacionSeleccionada);
		window = (Window) Executions.createComponents("/vistas/gestionevento/Donacion.zul", null, mapDonacion);
		window.doModal();
	}
	
	@GlobalCommand
	@NotifyChange({"donaciones","registrosTotales","paginaActual"})
	public void PaginarDonacion(){
		if (estatusFiltro==false){
			this.donaciones = servicioDonacion.ListarDonacionesNE(paginaActual);
			this.registrosTotales=servicioDonacion.totalDonacionesFiltrados("tipo in (0)");
		}else{
			ejecutarFiltro();
		}
	}
	
	@GlobalCommand
	@NotifyChange({"donaciones","registrosTotales","paginaActual"})
	public void ActualizarListaDonaciones(){
		this.registrosTotales = servicioDonacion.totalDonacionesFiltrados("tipo in (0)");
		if(this.registrosTotales > 0){
			if (this.tipoOperacion==1){
			   int ultimaPagina=0;
			   if (this.registrosTotales%this.tamanoPagina==0)
			      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
			   else
			      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
			   this.paginaActual=ultimaPagina; 
			} else if(this.tipoOperacion==2){
			
			}
			PaginarDonacion();
		}
	}
	
	@Command
	@NotifyChange({"donaciones","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String tipo="(0)";
		String estatus="(4)";
		this.estatusFiltro=true;
		
		if (fecha == null)
			jpql = " rif like '%"+rif+"%' and nombre like '%"+nombre+"%' and tipo in "+tipo+" and estatus in "+estatus;
		else
		    jpql = " rif like '%"+rif+"%' and nombre like '%"+nombre+"%' and tipo in "+tipo+" and estatus in "+estatus+" and fecha = '"+fecha+"'";
			
		this.donaciones=servicioDonacion.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioDonacion.totalDonacionesFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"donaciones","registrosTotales","paginaActual","rif", "nombre","fecha"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		PaginarDonacion();
		rif="";
		nombre="";
		fecha=null;
		comboEstatus=0;
		comboTipo=0;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
	
	@Command
	public String VerTipoDonacion(Integer aux){
		String est="Entrante";
		switch (aux){
			case 1:
				est="Saliente";
			    break;
			default:
				est="Entrante";
		}
		return est;
	}
	
	@Command
	public String VerEstatusDonacion(Integer aux){
		String est="Solicitada";
		switch (aux){
			case 1:
				est="Aprobada";
			    break;
			case 2:
				est="Rechazada";
			    break;
			case 3:
				est="Entregada";
			    break;
			case 4:
				est="Recibida";
			    break;
			default:
				est="Solicitada";
		}
		return est;
	}
	
	@Command
	@NotifyChange("donaciones")
	public void EliminarDonacion(@BindingParam("donacionSeleccionada") Donacion donacionSeleccionada){
		servicioDonacion.Eliminar(donacionSeleccionada.getId());
		PaginarDonacion();
		Messagebox.show("Datos eliminados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange("donaciones")
	public void EliminarDonaciones(){
		if(donacionesSeleccionadas != null){
			servicioDonacion.EliminarVarios(donacionesSeleccionadas);
			PaginarDonacion();
			donacionesSeleccionadas = new ArrayList<Donacion>();
			Messagebox.show("Datos eliminados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay datos seleccionados", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
}
