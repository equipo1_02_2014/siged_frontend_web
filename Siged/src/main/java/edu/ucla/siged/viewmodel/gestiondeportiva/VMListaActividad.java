/** 
 * 	VMListaActividad
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestiondeportiva.Actividad;
import edu.ucla.siged.servicio.impl.gestiondeportiva.ServicioActividad;

public class VMListaActividad {
	@WireVariable
	ServicioActividad servicioActividad;
	@WireVariable
	Actividad actividad;
	
	int paginaActual;
	int tamanoPagina;
	long registrosTotales;
	boolean estatusFiltro;
	short tipoOperacion;
	List<Actividad> listaActividad;
	List<Actividad> actividadesSeleccionadas;
	Window window;
	Integer id=0;
	String descripcion="";
	Integer estatusCombo=0;
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	public Actividad getActividad() {
		return actividad;
	}

	public List<Actividad> getActividadesSeleccionadas() {
		return actividadesSeleccionadas;
	}

	public void setActividadesSeleccionadas(List<Actividad> actividadesSeleccionadas) {
		this.actividadesSeleccionadas = actividadesSeleccionadas;
	}

	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina = servicioActividad.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(Window window) {
		this.window = window;
	}
	
	public List<Actividad> getListaActividad() {
		return listaActividad;
	}

	public void setListaActividad(List<Actividad> listaActividad) {
		this.listaActividad = listaActividad;
	}
	
	public Integer getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Integer estatus) {
		this.estatusCombo = estatus;
	}
	
////////////////////////////////////////////////////////////////////////////////
	
	@Init
	public void init() {
		this.listaActividad = servicioActividad.buscarTodos(0).getContent();
		this.registrosTotales = servicioActividad.totalActividades();
	}
	
	@Command
	public void agregarActividad(){
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestiondeportiva/actividad.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange({"actividad"})
	public void editarActividad(@BindingParam("actividadSeleccionada") Actividad actividadSeleccionada){
		this.actividad = actividadSeleccionada;
		this.tipoOperacion = 2;
		Map<String,Actividad> mapActividad = new HashMap<String, Actividad>();
		mapActividad.put("actividad", actividadSeleccionada);
		window = (Window)Executions.createComponents("/vistas/gestiondeportiva/actividad.zul", null, mapActividad);
		window.doModal();
	}
	
	@Command
	@NotifyChange("listaActividad")
	public void eliminarActividad(@BindingParam("actividadSeleccionada") final Actividad actividadSeleccionada){
		tipoOperacion = 3;
		Messagebox.show("�Est� seguro que desea eliminar este registro?","Pregunta",Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				new EventListener<Event>(){
					public void onEvent(Event event) throws Exception{
						if(Messagebox.ON_YES.equals(event.getName())){
							servicioActividad.eliminar(actividadSeleccionada);
							BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
							Messagebox.show("El registro se ha eliminado exitosamente","Informaci�n",Messagebox.OK,Messagebox.INFORMATION);
							actualizarLista();
						}
					}
		});
		actualizarLista();
	}
	
	@GlobalCommand
	@NotifyChange({"listaActividad","registrosTotales","paginaActual"})
    public void actualizarLista(){
		int ultimaPagina=0;
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   
		   if ((this.registrosTotales+1)%this.tamanoPagina==0)
		      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
		   else
			  ultimaPagina =((int) (this.registrosTotales+1)/this.tamanoPagina);
		   this.paginaActual=ultimaPagina; // Se coloca en la �ltima p�gina para que quede visible el que se acaba de ingresar.
		}
		   paginar();
		 
		   if(this.tipoOperacion==3 || this.tipoOperacion==4){
			   this.getRegistrosTotales();
			   
			   if ((this.registrosTotales+1)%this.tamanoPagina==0)
			      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			   else
				  ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina;
			 paginar();
			}	
	}
	
	@Command
	@NotifyChange({"listaActividad","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql="";
		String filtroEstatus;
		this.estatusFiltro=true;
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		if (id != null)
			
			jpql = " descripcion like '%"+descripcion+"%' and estatus in "+filtroEstatus+"";
		
		this.listaActividad = servicioActividad.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales = servicioActividad.totalActividadesFiltrados(jpql);
	} 
	
	@Command
	@NotifyChange({"listaActividad","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	@GlobalCommand
	@NotifyChange({"listaActividad","registrosTotales","paginaActual","actividadSeleccionada"})
	public void paginar(){
		if (estatusFiltro==false){
			this.listaActividad = servicioActividad.buscarTodos(this.paginaActual).getContent();
			this.registrosTotales = servicioActividad.totalActividades();
		}
		else{
			ejecutarFiltro();
		}
	}
	
	@Command
	@NotifyChange("listaActividad")
	public void eliminarActividades(@BindingParam("lista") Listbox lista){
		this.tipoOperacion=4; //Como se eliminar�n varios registros, tipoOperacion=4.
		if(lista.getSelectedItems().size()>0)
		{
			Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
                        
							if(Messagebox.ON_YES.equals(event.getName())){
								
								servicioActividad.eliminarVarios(actividadesSeleccionadas);
								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
								Messagebox.show("Los registros seleccionados se han eliminado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
								actividadesSeleccionadas=null;
							}
						}
					});
			
		}
		else Messagebox.show("Debe seleccionar los registros que desea eliminar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
/////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	
	
  @Command
  @NotifyChange({"listaActividad","registrosTotales","paginaActual","actividadSeleccionada","descripcion"})
     public void cancelarFiltro(){
	  	this.estatusFiltro = false;
	  	id=0;
	  	paginar();
	  	descripcion="";
	  	estatusCombo=0;
  }
}