package edu.ucla.siged.viewmodel.seguridad;
import java.util.Map;







import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Initiator;

import edu.ucla.siged.domain.seguridad.Usuario;



public class AuthenticationInit implements Initiator {
 
  
     
    public void doInit(Page page, Map<String, Object> args) throws Exception {
    	System.out.println("Autenticando init");
    	  Session sess = Sessions.getCurrent();
    	    Usuario cre = (Usuario)sess.getAttribute("userCredential");
    	   
    		
    	    if(cre==null || cre.isAnonimo()){
    	    	cre = new Usuario();
    	        sess.setAttribute("userCredential",cre);
    	    	Executions.sendRedirect("/portalPrincipal.zul");
    	       return;
    	        
    	    }
    	  
    	    System.out.println("Usuario: "+cre.getNombreUsuario());
    	
    }
}