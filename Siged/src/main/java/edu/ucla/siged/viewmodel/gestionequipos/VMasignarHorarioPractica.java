package edu.ucla.siged.viewmodel.gestionequipos;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

public class VMasignarHorarioPractica {
	Window window;


	@Command
	public void listarEquipo(){
		window = (Window)Executions.createComponents("/vistas/gestiondeequipos/listarequipo.zul", null, null);
		window.doModal();
		//window.doOverlapped();
	}
	

}
