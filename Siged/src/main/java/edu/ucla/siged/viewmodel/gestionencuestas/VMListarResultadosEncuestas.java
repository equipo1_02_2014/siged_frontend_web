package edu.ucla.siged.viewmodel.gestionencuestas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.util.Date;

import org.hibernate.Query;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestioneventos.Encuesta;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioEncuestaI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;


public class VMListarResultadosEncuestas {
	@WireVariable
	private ServicioEncuestaI servicioEncuesta;
	
	
	
	@WireVariable
	Encuesta encuesta;

	List<Encuesta> listaEncuestas;
	
	private Window window;
	short estatusCombo=0;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	boolean estatusFiltro;
	String fechaRegistro;
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	private List <Rol> roles;
	private Usuario usuario;
	


	String nombre;

	public VMListarResultadosEncuestas() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
	public void init() {
		
		this.roles= new ArrayList<Rol>();
		Session sess = Sessions.getCurrent();
    	this.roles =  (List<Rol>) sess.getAttribute("roles");
    	this.usuario = (Usuario)sess.getAttribute("userCredential");
  	   	if(usuario==null || usuario.isAnonimo()){
  	   		this.listaEncuestas=servicioEncuesta.listarEncuestasCerradasPublicasPaginada(0);
  	   		this.registrosTotales= servicioEncuesta.totalEncuestasCerradasPublicas();
  	   	}
  	   	else
  	   	{
  	   		this.listaEncuestas=servicioEncuesta.listarEncuestasCerradasParaVerResultados(roles, 0);
  	   		this.registrosTotales= servicioEncuesta.totalEncuestasCerradasParaVerResultados(roles);
  	   	}
	}



	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina=servicioEncuesta.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}


	public List<Encuesta> getListaEncuestas() {
		return listaEncuestas;
	}

	public void setListaEncuestas(List<Encuesta> listaEncuestas) {
		this.listaEncuestas = listaEncuestas;
	}

	

	@Command
	@NotifyChange({"listaEncuestas"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		System.out.println("estatus:"+estatusEnLista.toString());
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@GlobalCommand
	@NotifyChange({"listaEncuestas","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevaEncuesta") Encuesta nuevaEncuesta){
		
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} 
		paginar();
	}
	
	@GlobalCommand
	@NotifyChange({"listaEncuestas","registrosTotales","paginaActual","rolesSeleccionados"})
	public void paginar(){
		
	 	if(usuario==null || usuario.isAnonimo()){
  	   		this.listaEncuestas=servicioEncuesta.listarEncuestasCerradasPublicasPaginada(this.paginaActual);
  	   		this.registrosTotales= servicioEncuesta.totalEncuestasCerradasPublicas();
  	   	}
  	   	else
  	   	{
  	   		this.listaEncuestas=servicioEncuesta.listarEncuestasCerradasParaVerResultados(roles, this.paginaActual);
  	   		this.registrosTotales= servicioEncuesta.totalEncuestasCerradasParaVerResultados(roles);
  	   	}

	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		if(fecha!=null){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
		}
		return "-";
	}
	
	@Command
	public String cargarVotosOpcion(Integer idOpcion){
	List<String> numeros= servicioEncuesta.getResultadoOpcionPregunta(idOpcion);
	String votosOpcion="0";
	if(numeros.size()>0){
		votosOpcion=numeros.get(0);
		
	}
	return votosOpcion;
	}
	
	
	@Command
	public String cargarPorcOpcion(Integer idOpcion){
		List<String> numeros= servicioEncuesta.getResultadoOpcionPregunta(idOpcion);
		String votosOpcion="0";
		if(numeros.size()>0){
			votosOpcion=numeros.get(1);
			
		}
		return votosOpcion;
	}
	
	
	@Command
	public String cargarTotalPregunta(Integer idPregunta){
		return servicioEncuesta.getTotalVotosPregunta(idPregunta);
	}
	
	
}