package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.gestionatleta.Postulante;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.reporte.PromedioCantidadBase;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPostulanteI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;

public class VMPromedioCategoriaSolicitada {

	@WireVariable
    private ServicioPostulanteI servicioPostulante;
	
	@WireVariable
    private ServicioCategoriaI servicioCategoria;
	
	private List<Postulante> listaPostulantes;
	
	private List<Categoria> listaCategorias;
	
	private List<PromedioCantidadBase> listaPromedioCategorias;
	
	private Date fechaInicio;
	
	private Date fechaFin;
	
	private String pathProyecto;
	
	@Init
	public void init(){
		this.listaPostulantes= new ArrayList<Postulante>();
		
		this.listaPromedioCategorias= new ArrayList<PromedioCantidadBase>();
		
		this.listaCategorias= servicioCategoria.buscarCategoriasActivas();
		
		/*********Para obtener el path del proyecto******/
		
		File file= new File(VMPromedioCategoriaSolicitada.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		
		this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
		
		
		//Sustitucion de caracteres codificados (Para servidores con Windows)
		this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
		/***********************************/
	}
	
	@NotifyChange("listaPostulantes")
	public void cargarPostulantes(){
		
		if (validarRangoFechaPostulacion()){
		   
			if (fechaInicio==null && fechaFin==null){
				listaPostulantes= servicioPostulante.buscarTodos();
			}
			else{
				listaPostulantes= servicioPostulante.buscarPostulantesPorRangoFechaPostulacion(this.fechaInicio, this.fechaFin);
			}
			
		}
		
	}
	
	@NotifyChange({"listaPromedioCategorias",
		           "listaPostulantes"})
	public void cargarPromedioCategorias(){
		this.cargarPostulantes();
	   	
		if (!listaPostulantes.isEmpty()){
			Map<String,Integer> map= new HashMap<String,Integer>();
			Integer totalPostulantes=0;
			
			for (Categoria categoria:this.listaCategorias){
			   map.put(categoria.getNombre(),0);	
			}
			
			for (Postulante postulante:this.listaPostulantes){
				Categoria categoriaPostulante= servicioCategoria.obtenerCategoria(postulante);
				Integer cantidadPostulacionCategoria=0;
				
				if (categoriaPostulante!=null){
				   ++totalPostulantes;
				   cantidadPostulacionCategoria=map.get(categoriaPostulante.getNombre())+1;   
				   map.replace(categoriaPostulante.getNombre(), cantidadPostulacionCategoria);	
				}
			}
			
			Iterator iteradorMap= map.keySet().iterator();
			PromedioCantidadBase promedioCantidadBase= new PromedioCantidadBase();
			
			while(iteradorMap.hasNext()){
			   String descripcion = (String)iteradorMap.next();
			   Integer cantidad   = map.get(descripcion);
			   Double porcentaje= (cantidad*100)/(totalPostulantes*1.0);
			   Double promedio= cantidad/(totalPostulantes*1.0);
			   promedioCantidadBase= new PromedioCantidadBase();
			   
			   promedioCantidadBase.setDescripcion(descripcion);
			   promedioCantidadBase.setCantidadConsiderada(cantidad);
			   promedioCantidadBase.setCantidadTotal(totalPostulantes);
			   promedioCantidadBase.setPromedio(Math.round((promedio)*Math.pow(10,2))/Math.pow(10, 2));
			   promedioCantidadBase.setPorcentaje(Math.round((porcentaje)*Math.pow(10,2))/Math.pow(10, 2));
			   this.listaPromedioCategorias.add(promedioCantidadBase);
			}
		}
		
	}
	
	@Command
	@NotifyChange({"listaPromedioCategorias","listaPostulantes","fechaInicio","fechaFin"})
	public void cancelar(){
        this.listaPostulantes= new ArrayList<Postulante>();
		this.listaPromedioCategorias= new ArrayList<PromedioCantidadBase>();
		this.fechaInicio= null;
		this.fechaFin= null;
	}
	
	@Command
	public void imprimir(){	
		this.cargarPromedioCategorias();
		
		if (!this.listaPromedioCategorias.isEmpty()){
			
			Map<String,Object> parameterMap = new HashMap<String,Object>();
			
			JRDataSource JRdataSource = new JRBeanCollectionDataSource(this.listaPromedioCategorias);
			
			parameterMap.put("datasource", JRdataSource);
			
			parameterMap.put("titulo","Porcentaje de Categorias Demandadas");
			
			parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
			
			parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
				
			try {			
				
				JasperDesign jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "categoriaSolicitadas.jrxml");
				
				JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
		        
		        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
		        
		        JasperViewer.viewReport(jasperPrint,false);
				
			} catch (JRException e) {
				Messagebox.show(e.getMessage());
			}
			
		}else{
			Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
	                        "Information", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		
		this.listaPromedioCategorias.clear();
		
	}
	
	
	public boolean validarRangoFechaPostulacion(){
		boolean valido=true;
	
        if (this.fechaInicio==null && this.fechaFin!=null){
			
        	valido= false;
			
        	Messagebox.show("El Campo Desde es Invalido" + System.lineSeparator() + "Coloque una Fecha Valida en el Campo Desde o deje el campo Desde y Hasta Vacios", 
                    "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
			
		}else if(this.fechaFin==null && this.fechaInicio!=null){
			
			valido=false;
			
			Messagebox.show("El Campo Hasta es Invalido" + System.lineSeparator() + "Coloque una Fecha Valida en el Campo Hasta o deje el campo Desde y Hasta Vacios", 
                    "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			
		}else if ((this.fechaInicio!=null && this.fechaFin!=null) && this.fechaInicio.after(this.fechaFin)){
			
			valido=false;
			
			Messagebox.show("El Rango de las Fechas es Invalido", 
	                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
        }
				
		
		return valido;
	}

	public List<Postulante> getListaPostulantes() {
		return listaPostulantes;
	}

	public void setListaPostulantes(List<Postulante> listaPostulantes) {
		this.listaPostulantes = listaPostulantes;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<Categoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<Categoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public List<PromedioCantidadBase> getListaPromedioCategorias() {
		return listaPromedioCategorias;
	}

	public void setListaPromedioCategorias(
			List<PromedioCantidadBase> listaPromedioCategorias) {
		this.listaPromedioCategorias = listaPromedioCategorias;
	}
	
	
}
