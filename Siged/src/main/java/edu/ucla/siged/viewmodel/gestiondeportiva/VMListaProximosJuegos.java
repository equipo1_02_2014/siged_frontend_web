package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.SimpleDateFormat;
import java.util.List;

import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.domain.utilidades.Persona;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;

public class VMListaProximosJuegos {
     
	@WireVariable
	private ServicioJuegoI servicioJuego;
	
	@WireVariable
	private ServicioAtletaI servicioAtleta;
	
	@WireVariable
	private ServicioTecnicoI servicioTecnico;
	
	@WireVariable
	private ServicioRepresentanteI servicioRepresentante;
	
	private List<Juego> listaJuegos;
	
	
	private long registrosTotales;
	
	private int tamanioPagina;
	
	private int paginaActual;
	
	private Persona persona;
	
	@Init
	public void init(){
		
		  this.tamanioPagina=5;
		  this.paginaActual=0;
		  
		  Session sess = Sessions.getCurrent();
		  Usuario usuario = (Usuario)sess.getAttribute("userCredential");
		  persona=null;
		  
		  if (usuario==null){
			  
			  Messagebox.show("Usuario no Autenticado", "Error", Messagebox.OK, Messagebox.ERROR);
			  Executions.sendRedirect("/portalPrincipal.zul");  
			  
		  }else if(usuario.getCedula()==null){
			  
			  Messagebox.show("Usuario no Encontrado", "Error", Messagebox.OK, Messagebox.ERROR);
			  Executions.sendRedirect("/");
			  
		  }else if( (persona=servicioAtleta.buscarAtletaPorCedula(usuario.getCedula()))==null &&
				    (persona=servicioTecnico.buscarPorCedula(usuario.getCedula()))==null &&
				    (persona=servicioRepresentante.buscarPorCedula(usuario.getCedula()))==null  ){
			  
			  this.listaJuegos= servicioJuego.buscarTodos();
			  
		  }else{
			  
			  this.paginarProximosJuegos();
		  }
		
	}
	
	@GlobalCommand("paginarProximosJuegos")
	 @NotifyChange({"listaJuegos","registrosTotales"})
	 public void paginarProximosJuegos(){
		    
		if (persona instanceof Representante){
			  Representante representante= (Representante)persona;
			  this.listaJuegos= servicioJuego.buscarJuegosPorRepresentante(this.paginaActual, representante);
			  this.registrosTotales= servicioJuego.obtenerCantidadJuegosPorRepresentante(representante);
		}else if (persona instanceof Tecnico){
			  Tecnico tecnico= (Tecnico)persona;
			  this.listaJuegos= servicioJuego.buscarJuegosPorTecnico(this.paginaActual, tecnico);
			  this.registrosTotales= servicioJuego.obtenerCantidadJuegosPorTecnico(tecnico);
		}else if (persona instanceof Atleta){
			  Atleta atleta= (Atleta) persona;
			  this.listaJuegos= servicioJuego.buscarJuegosPorAtleta(this.paginaActual, atleta);
			  this.registrosTotales= servicioJuego.obtenerCantidadJuegosPorAtleta(atleta);  
		}
		
		
			//this.listaJuegos = servicioJuego.buscarTodosCompetenciasActivas(this.paginaActual);
		   // this.registrosTotales= servicioJuego.obtenerCantidadJuegosCompetenciasActivas();
	 }

    public String formatearFechaJuego(Juego juego){
		
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(juego.getFecha());
		
		fechaFormateada += new SimpleDateFormat(" hh:mm a").format(juego.getHoraInicio());;
		
		return fechaFormateada;
	}
	
	public String formatearParticipantes(Juego juego){
		
		String participantes= juego.getEquipo().getNombre() + " VS " + juego.getEquipoContrario();
		
		return participantes;
	}

	public List<Juego> getListaJuegos() {
		return listaJuegos;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public int getTamanioPagina() {
		return tamanioPagina;
	}

	public void setTamanioPagina(int tamanioPagina) {
		this.tamanioPagina = tamanioPagina;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	
}
