package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.ListModels;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.utilidades.Persona;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoTecnicoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;

public class VMAsignarTecnicoEquipo {
	
	@WireVariable ServicioEquipoTecnicoI servicioEquipoTecnico;
	@WireVariable ServicioTecnicoI servicioTecnico;	
	@WireVariable Tecnico tecnicoPrincipalSeleccionado;
	@WireVariable Tecnico tecnicoAsistenteSeleccionado;
	@WireVariable Tecnico tecnicoPrincipal;
	@WireVariable Tecnico tecnicoAsistente;
	@WireVariable List<Tecnico> listaTecnicos;
	@WireVariable List<Tecnico> listaTecnicosAux;
	@WireVariable EquipoTecnico equipoTecnicoSeleccionado;	
	@WireVariable private Equipo equipo;	
	@WireVariable private Tecnico tecnico;
	

	
	private EquipoTecnico equipoTecnico;
	List<Integer> listaResponsabilidad;
	Integer responsabilidadSeleccionada;
	private Window window;
	
	

//	--------------------------------------------------------------------------------------------------------- Inicio de M�todos Getters and Setters	

	public Integer getResponsabilidadSeleccionada() { return responsabilidadSeleccionada; }

	public void setResponsabilidadSeleccionada(Integer responsabilidadSeleccionada) { this.responsabilidadSeleccionada = responsabilidadSeleccionada; }

	public List<Integer> getListaResponsabilidad() { return listaResponsabilidad; }

	public void setListaResponsabilidad(List<Integer> listaResponsabilidad) { this.listaResponsabilidad = listaResponsabilidad; }

	public List<Tecnico> getListaTecnicos() { return listaTecnicos; }

	public void setListaTecnicos(List<Tecnico> listaTecnicos) { this.listaTecnicos = listaTecnicos; }

	public Tecnico getTecnicoPrincipal() { return tecnicoPrincipal; }

	public void setTecnicoPrincipal(Tecnico tecnicoPrincipal) { this.tecnicoPrincipal = tecnicoPrincipal; }

	public Tecnico getTecnicoAsistente() { return tecnicoAsistente; }

	public void setTecnicoAsistente(Tecnico tecnicoAsistente) { this.tecnicoAsistente = tecnicoAsistente; }

	public EquipoTecnico getEquipoTecnico() { return equipoTecnico; }

	public void setEquipoTecnico(EquipoTecnico equipoTecnico) { this.equipoTecnico = equipoTecnico; }

	public Tecnico getTecnicoPrincipalSeleccionado() { return tecnicoPrincipalSeleccionado; }

	public void setTecnicoPrincipalSeleccionado(Tecnico tecnicoPrincipalSeleccionado) { this.tecnicoPrincipalSeleccionado = tecnicoPrincipalSeleccionado; }

	public Tecnico getTecnicoAsistenteSeleccionado() { return tecnicoAsistenteSeleccionado; }

	public void setTecnicoAsistenteSeleccionado(Tecnico tecnicoAsistenteSeleccionado) { this.tecnicoAsistenteSeleccionado = tecnicoAsistenteSeleccionado; }

	public Equipo getEquipo() { return equipo; }

	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public EquipoTecnico getEquipoTecnicoSeleccionado() { return equipoTecnicoSeleccionado; }

	public void setEquipoTecnicoSeleccionado(EquipoTecnico equipoTecnicoSeleccionado) { this.equipoTecnicoSeleccionado = equipoTecnicoSeleccionado; }	
	
	public List<Tecnico> getListaTecnicosAux() { return listaTecnicosAux; }

	public void setListaTecnicosAux(List<Tecnico> listaTecnicosAux) { this.listaTecnicosAux = listaTecnicosAux; }
	
//	--------------------------------------------------------------------------------------------------------- Fin de M�todos Getters and Setters

	@Init
	public void inicializar(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("equipo") Equipo equipoSeleccionado){
			
		if(equipoSeleccionado != null){
			this.equipo = equipoSeleccionado;

			tecnicoPrincipal = servicioEquipoTecnico.obtenerTecnicoPrincipal(equipo.getId());
			if(tecnicoPrincipal != null){ this.tecnicoPrincipalSeleccionado = tecnicoPrincipal; }
			else{ this.tecnicoPrincipalSeleccionado = null; }
			
			tecnicoAsistente = servicioEquipoTecnico.obtenerTecnicoAsistente(equipo.getId());
			if(tecnicoAsistente != null){ this.tecnicoAsistenteSeleccionado = tecnicoAsistente; }
			else{ this.tecnicoAsistenteSeleccionado = null;}
				
			listaTecnicos = servicioEquipoTecnico.buscarTecnicosDisponibles(); 
			listaTecnicosAux= servicioEquipoTecnico.buscarTecnicosDisponibles();
			
			if(tecnicoPrincipal != null){
				listaTecnicos.remove(tecnicoPrincipal);
			}
			if(tecnicoAsistente != null){
				listaTecnicos.remove(tecnicoAsistente);
			}
			
			this.window = null;
						
		}else{
			Messagebox.show( "Debe Seleccionar un Equipo para asignar t�cnicos!","", Messagebox.OK, Messagebox.ERROR );
		}
	}
	
	@NotifyChange({"listaTecnicos","listaTecnicosAux"})
	public void inicializarListaTecnicos(){
		listaTecnicos = servicioEquipoTecnico.buscarTecnicosDisponibles(); 
		listaTecnicosAux= servicioEquipoTecnico.buscarTecnicosDisponibles();
	}
	
	@Command
	public String verResponsabilidad( Integer responsabilidad ){
		String res="Principal";
		switch (responsabilidad){
		case 1:
			res="Principal";
		    break;
		case 2:
			res="Asistente";
			break;		
		}		
		return res;
	}
	
	
	@Command
	@NotifyChange({"tecnicoPrincipalSeleccionado","tecnicoAsistenteSeleccionado","equipo"})
	public void guardar(@SelectorParam(".equipotecnico_restriccion") LinkedList<InputElement> inputs,
						@SelectorParam("#wndGestionDeportivaAsignarTecnicoEquipo") Window win,
						@SelectorParam("#cmbTecnicoPrincipalGestionDeportivaAsignarTecnico") Combobox cmbTecnicoPrincipal,
    					@SelectorParam("#cmbTecnicoAsistenteGestionDeportivaAsignarTecnico") Combobox cmbTecnicoAsistente){

		
		this.window = win;
		
		boolean camposValidos=true;
		
		if(cmbTecnicoPrincipal.getText().equalsIgnoreCase("")){
			camposValidos=false;
		}		
		
//		for(InputElement input:inputs){
//			if (!input.isValid()){
//				camposValidos=false;
//				break;
//			}
//		}
		
		/*Validando Campos*/
		if(!camposValidos){
			Messagebox.show("Por Favor Seleccione Un Tecnico Principal", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
		}/*Inicio de Operacion*/
		else{
			final EquipoTecnico equipoTecnicoPrincipal = new EquipoTecnico();
			
			final EquipoTecnico equipoTecnicoAsistente= new EquipoTecnico();
			
			Date fechaActual = new Date();

			if(tecnicoPrincipalSeleccionado != null){
				Short responsabilidad = 1; 
				
				equipoTecnicoPrincipal.setTecnico(tecnicoPrincipalSeleccionado);
				
				equipoTecnicoPrincipal.setEquipo(this.equipo);
				
				equipoTecnicoPrincipal.setFechaAsignacion(fechaActual);
				
				equipoTecnicoPrincipal.setResponsabilidad(responsabilidad);
				
				equipoTecnicoPrincipal.setEstatus(Short.parseShort("1"));			
			}
			
			if(tecnicoAsistenteSeleccionado != null){
				Short responsabilidad = 2; 
				
				equipoTecnicoAsistente.setTecnico(tecnicoAsistenteSeleccionado);
				
				equipoTecnicoAsistente.setEquipo(this.equipo);
				
				equipoTecnicoAsistente.setFechaAsignacion(fechaActual);
				
				equipoTecnicoAsistente.setResponsabilidad(responsabilidad);
				
				equipoTecnicoAsistente.setEstatus(Short.parseShort("1"));			
			}
			
			
        	try{
        		
        		
        		Messagebox.show("Desea Guardar los cambios realizados?","Confirmacion",
        				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
        				         new org.zkoss.zk.ui.event.EventListener<Event>() {
        		                     
        			              public void onEvent(Event evt) throws InterruptedException {
        			            	  
				        		        if (evt.getName().equalsIgnoreCase("onYes")) {
				        		        	
				        		        	if(tecnicoPrincipalSeleccionado != null){
				        		        		servicioEquipoTecnico.guardarEquipoTecnico(equipoTecnicoPrincipal);
				        		        	}
				        		        	if(tecnicoAsistenteSeleccionado!=null){
				        		        	   servicioEquipoTecnico.guardarEquipoTecnico(equipoTecnicoAsistente);
				        		        	}
				        		           
				        		           Messagebox.show("T�cnicos asignados con �xito", 
				        	    	                       "Information", Messagebox.OK, 
				        	    	                        Messagebox.INFORMATION);	
											 
				        		           BindUtils.postGlobalCommand(null, null, "paginar", null);
				        		           window.onClose();
											
										}else{
											
										}
									}
				        		        	   
				        		 });				        		         
	        	   
        	}catch(Exception ex){
        		Messagebox.show("Error al asignar t�cnicos", 
    	                "Error", Messagebox.OK, Messagebox.ERROR);
        	}
			
		}		
	}	

    @Command
    @NotifyChange({"listaTecnicos", "listaTecnicosAux","tecnicoPrincipalSeleccionado","tecnicoAsistenteSeleccionado"})
    public void actualizarTecnicos(){
    	if(this.listaTecnicosAux!=null){
    		this.listaTecnicos = new ArrayList<Tecnico>(this.listaTecnicosAux);
    	}
    	
    	if(tecnicoPrincipalSeleccionado != null){
    		this.listaTecnicos.remove(tecnicoPrincipalSeleccionado);
    	}    	
    	if(tecnicoAsistenteSeleccionado !=null){
    		this.listaTecnicos.remove(tecnicoAsistenteSeleccionado);
    	}
    }
           
    @GlobalCommand("limpiar")
	@NotifyChange({"listaTecnicos","listaTecnicosAux","tecnicoPrincipalSeleccionado","tecnicoAsistenteSeleccionado"})
    public void limpiar(){   	
		this.tecnicoAsistenteSeleccionado = null;
		this.tecnicoPrincipalSeleccionado = null;
		actualizarTecnicos();
    }


    public String obtenerNombreCompleto(Persona persona){
    	String nombreCompleto="";
    	
    	if (persona!=null){
    		nombreCompleto= persona.getNombre() + " " + persona.getApellido(); 
    	}

    	return nombreCompleto;
    }
    
    
	@Command
	public void cerrarVentana(){
		BindUtils.postGlobalCommand(null, null, "paginar", null);
	}
    
	
	
}