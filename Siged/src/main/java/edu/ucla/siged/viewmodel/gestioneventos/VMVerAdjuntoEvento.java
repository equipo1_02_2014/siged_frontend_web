package edu.ucla.siged.viewmodel.gestioneventos;

import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;

public class VMVerAdjuntoEvento {
	
	private AdjuntoEvento adjuntoEvento = new AdjuntoEvento();

	public VMVerAdjuntoEvento() {}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("adjuntoaux") AdjuntoEvento aux){
		Selectors.wireComponents(view, this, false);
		if(aux!=null){
			this.adjuntoEvento = aux;
		}
	}

	public AdjuntoEvento getAdjuntoEvento() {
		return adjuntoEvento;
	}

	public void setAdjuntoEvento(AdjuntoEvento adjuntoEvento) {
		this.adjuntoEvento = adjuntoEvento;
	}
}