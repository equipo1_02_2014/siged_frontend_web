package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.reporte.AceptadosPostuladosPorMes;
//import edu.ucla.siged.servicio.interfaz.reportes.ServicioPostuladosAceptadosI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;



public class VMRelacionPostuladosAceptados {

			
		private Date anioDesde;
		
		private Date anioHasta;
		
		private String pathProyecto;
		
		private List<Integer> listaCantidadAceptados;
		
		private List<Integer> listaCantidadPostulados;
		
		@WireVariable
	    private ServicioReporteEstadisticoI servicioReporteEstadistico;
		
		private Window window;
					
		@Init
		public void init(){
		
			this.setWindow(null);
		//this.listaEquiposFundacion= servicioEquipo.buscarEquiposTrue();
			
			
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMRelacionPostuladosAceptados.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			
			this.inicializarCampos();
		}
		
		@NotifyChange({"vistaGraficaTorta","vistaGraficaBarra","vistaGraficaLinea","vistaLista",
			           "formatoPdf","formatoXls","anioDesde","anioHasta"})
		public void inicializarCampos(){
			
		
			this.anioDesde=null;
			this.anioHasta=null;
			
			
			
		}
		
		@Command
		@NotifyChange({ "anioDesde","anioHasta"})
		public void cancelar(){
			this.inicializarCampos();
		}
				
		
		@Command
		public void imprimir(){		
			
			
				String restricciones="";
				
				//Messagebox.show(this.equipoSeleccionado.getNombre());
				
				if((this.anioDesde!=null && this.anioHasta!=null) ){	
					
						if (this.anioDesde!=null && this.anioHasta!=null){
							
							String desde= new SimpleDateFormat("YYYY").format(this.anioDesde);
							String hasta= new SimpleDateFormat("YYYY").format(this.anioHasta);
							restricciones = "where  to_char(fechapostulacion, 'yyyy') BETWEEN '" + desde + "' AND '" + hasta + "'";
							
						}
																		
						
						
						
				}
				//servicioPostuladosAceptados = new ServicioReporteEstadisticoI();
				
				List<AceptadosPostuladosPorMes> lista= this.servicioReporteEstadistico.buscarAceptadosPostulados(restricciones);
							
				
				if (!lista.isEmpty()){
					
					Map<String,Object> parameterMap = new HashMap<String,Object>();
					
					JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
					
					parameterMap.put("datasource", JRdataSource);
					
					parameterMap.put("titulo","Postulados y Aceptados por mes");
					
					parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
					
					parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
					
					//Messagebox.show(Boolean.toString(asistente));
					
				
					try {
						
						JasperDesign jasDesign=null;
						
						jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "reporteAceptadosPostuladosBarra.jrxml");
						
						JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
				        
				        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
				        
				        JasperViewer.viewReport(jasperPrint,false);
						
					} catch (JRException e) {
						e.printStackTrace();
						Messagebox.show(e.getMessage());
					}
				
				

				}else{
					Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				}
				
			     
		}

		public Date getAnioDesde() {
			return anioDesde;
		}

		public void setAnioDesde(Date anioDesde) {
			this.anioDesde = anioDesde;
		}

		public Date getAnioHasta() {
			return anioHasta;
		}

		public void setAnioHasta(Date anioHasta) {
			this.anioHasta = anioHasta;
		}

		public String getPathProyecto() {
			return pathProyecto;
		}

		public void setPathProyecto(String pathProyecto) {
			this.pathProyecto = pathProyecto;
		}

		public List<Integer> getListaCantidadAceptados() {
			return listaCantidadAceptados;
		}

		public void setListaCantidadAceptados(List<Integer> listaCantidadAceptados) {
			this.listaCantidadAceptados = listaCantidadAceptados;
		}

		public List<Integer> getListaCantidadPostulados() {
			return listaCantidadPostulados;
		}

		public void setListaCantidadPostulados(List<Integer> listaCantidadPostulados) {
			this.listaCantidadPostulados = listaCantidadPostulados;
		}

		public ServicioReporteEstadisticoI getServicioPostuladosAceptados() {
			return servicioReporteEstadistico;
		}

		public void setServicioPostuladosAceptados(
				ServicioReporteEstadisticoI servicioPostuladosAceptados) {
			this.servicioReporteEstadistico = servicioPostuladosAceptados;
		}

		public Window getWindow() {
			return window;
		}

		public void setWindow(Window window) {
			this.window = window;
		}
		
		
		
		
	
	}

