package edu.ucla.siged.viewmodel.gestionatletas;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.Pago;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPagoI;

public class VMListarPago {
	
	private @WireVariable ServicioPagoI servicioPago;
	private Window window;
	
	private @WireVariable Pago pagoSeleccionado;
	private List<Pago> listaPagos;
	private List<Pago> pagosSeleccionados;
	
	private int paginaActual;
	private int tamanoPagina;
	private long registrosTotales;
	
	private Date fechaPago = null;
	private String numeroRecibo = "";
	private String nombreAtleta = "";
	private String apellidoAtleta = "";

	
	private boolean estatusFiltro = false;
	
	@Init
	public void Init(){
		this.listaPagos= servicioPago.obtenerListaPago(0);
		this.registrosTotales = servicioPago.totalPago();
	}
		
	public Pago getPagoSeleccionado() {
		return pagoSeleccionado;
	}

	public void setPagoSeleccionado(Pago pagoSeleccionado) {
		this.pagoSeleccionado = pagoSeleccionado;
	}

	public List<Pago> getListaPagos() {
		return listaPagos;
	}

	public void setListaPagos(List<Pago> listaPagos) {
		this.listaPagos = listaPagos;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina = servicioPago.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public Date getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getNumeroRecibo() {
		return numeroRecibo;
	}

	public void setNumeroRecibo(String numeroRecibo) {
		this.numeroRecibo = numeroRecibo;
	}
	
	public String getNombreAtleta() {
		return nombreAtleta;
	}

	public void setNombreAtleta(String nombreAtleta) {
		this.nombreAtleta = nombreAtleta;
	}
	
	public String getApellidoAtleta() {
		return apellidoAtleta;
	}

	public void setApellidoAtleta(String apellidoAtleta) {
		this.apellidoAtleta = apellidoAtleta;
	}

	public List<Pago> getPagosSeleccionados() {
		return pagosSeleccionados;
	}

	public void setPagosSeleccionados(List<Pago> pagosSeleccionados) {
		this.pagosSeleccionados = pagosSeleccionados;
	}

	@GlobalCommand
	@NotifyChange({"listaPagos","registrosTotales","paginaActual"})
	public void paginar(){
		if(estatusFiltro==false){
			this.listaPagos = servicioPago.obtenerListaPago(paginaActual);
			this.registrosTotales=servicioPago.totalPago();
		}else{
			ejecutarFiltro();
		}
		
	}
	
	@Command
	@NotifyChange({"listaPagos","registrosTotales", "paginaActual","nombreAtleta","fechaPago","numeroRecibo","apellidoAtleta"})
	public void filtrar(){
		this.estatusFiltro = true;
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	public void ejecutarFiltro(){
		this.listaPagos = servicioPago.obtenerListaPago(paginaActual, fechaPago, numeroRecibo, nombreAtleta, apellidoAtleta);
		this.registrosTotales=servicioPago.totalpago(paginaActual, fechaPago, numeroRecibo, nombreAtleta, apellidoAtleta);
	}
	@Command
	@NotifyChange({"listaPagos","registrosTotales","paginaActual","pagoSeleccionado","fechaPago","nombreAtleta","numeroRecibo","apellidoAtleta"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		this.fechaPago=null;
		this.numeroRecibo = "";
		this.nombreAtleta = "";
		this.apellidoAtleta ="";
	}
	
	@Command
	@NotifyChange("listaPagos")
	public void eliminarPago(@BindingParam("pagoSeleccionado") Pago pagoSeleccionado){
		servicioPago.eliminarPago(pagoSeleccionado);
		actualizarLista();
	}
	
	@Command
	@NotifyChange("listaPagos")
	public void eliminarPagos(){
		servicioPago.eliminarPagos(pagosSeleccionados);
		actualizarLista();
	}
	
	@GlobalCommand
	@NotifyChange({"listaPagos", "registrosTotales", "paginaActual"})
	public void actualizarLista(){
		paginar();
	}

	@Command
	@NotifyChange({"listaPagos"})
	public void abrirVentanaPago(@BindingParam("pagoSeleccionado") Pago pagoSeleccionado){
		try{
			Map<String, Pago> mapPago = new HashMap<String, Pago>();
			mapPago.put("pago", pagoSeleccionado);
			window = (Window)Executions.createComponents("vistas/gestionatleta/pago.zul", null, mapPago);
			window.doModal();
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
}
