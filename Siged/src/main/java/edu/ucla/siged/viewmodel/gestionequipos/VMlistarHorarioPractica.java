package edu.ucla.siged.viewmodel.gestionequipos;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Window;

public class VMlistarHorarioPractica {
	Window window;


	@Command
	public void agregarHorarioPractica(){
		window = (Window)Executions.createComponents("/vistas/gestionequipo/asignarhorariopractica.zul", null, null);
		window.doModal();
		//window.doOverlapped();
	}
	
	@Command
	public void editarHorarioPractica(){
		window = (Window)Executions.createComponents("/vistas/gestionequipo/asignarhorariopractica.zul", null, null);
		window.doModal();
		//window.doOverlapped();
	}

}
