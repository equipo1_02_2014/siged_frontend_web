/** 
 * 	VMNoticia
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Controller;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestioneventos.AdjuntoNoticia;
import edu.ucla.siged.domain.gestioneventos.Noticia;
import edu.ucla.siged.domain.gestioneventos.Publico;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.servicio.impl.gestionencuestas.ServicioPublico;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioAdjuntoNoticia;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioNoticia;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRol;

@Controller
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMNoticia {
	
	@WireVariable
	private ServicioNoticia servicioNoticia;
	@WireVariable
	private ServicioAdjuntoNoticia servicioAdjuntoNoticia;
	@WireVariable
	private Noticia noticia = new Noticia();
	@WireVariable
	private AdjuntoNoticia adjuntoNoticia = new AdjuntoNoticia();
	private Media media;

	private Set<AdjuntoNoticia> adjuntos = new HashSet<AdjuntoNoticia>();
	
	private Set<AdjuntoNoticia> adjuntosSeleccionados = new HashSet<AdjuntoNoticia>();
	
	private Set<AdjuntoNoticia> adjuntosPorEliminar = new HashSet<AdjuntoNoticia>();
	
	private Window window;
	
	@WireVariable
	private ServicioRol servicioRol;
	
	@WireVariable
	private ServicioPublico servicioPublico;
	
	@WireVariable
	private List<Rol> comboxRoles = new ArrayList<Rol>();
	
	@WireVariable
	private Publico publico = new Publico();
	
	@WireVariable
	private Set<Publico> publicos = new HashSet<Publico>();
	
	@WireVariable
	private  List<Publico> publicosPorEliminar = new ArrayList<Publico>();

	private Archivo arch = new Archivo();

	public VMNoticia() {
		
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("noticia") Noticia noticiaSeleccionada) throws ParseException {
		Selectors.wireComponents(view, this, false);
		
		if(noticiaSeleccionada!=null){
			this.noticia = noticiaSeleccionada;
			this.adjuntos = noticia.getAdjuntoNoticias();
			this.publicos = noticia.getPublicos();
		}
		else{
			this.noticia = new Noticia();
			this.noticia.setFecha(new Date());
			this.adjuntoNoticia = new AdjuntoNoticia();
			this.adjuntos = new HashSet<AdjuntoNoticia>();
			this.publicos = new HashSet<Publico>();
		}
		
		Date today = Calendar.getInstance().getTime();
		DateFormat formatter = new SimpleDateFormat("hh:mm:ss");
		String today_str = formatter.format(today);
		this.noticia.setHora(formatter.parse(today_str));
		this.comboxRoles = servicioRol.buscarActivos();
	}

	public Noticia getNoticia() {
		return noticia;
	}

	public void setNoticia(Noticia noticia) {
		this.noticia = noticia;
	}
	
	public AdjuntoNoticia getAdjuntoNoticia() {
		return adjuntoNoticia;
	}

	public void setAdjuntoNoticia(AdjuntoNoticia adjuntoNoticia) {
		this.adjuntoNoticia = adjuntoNoticia;
	}

	public Set<AdjuntoNoticia> getAdjuntos() {
		return adjuntos;
	}

	public void setAdjuntos(Set<AdjuntoNoticia> adjuntos) {
		this.adjuntos = adjuntos;
	}
	
	public Set<AdjuntoNoticia> getAdjuntosSeleccionados() {
		return adjuntosSeleccionados;
	}

	public void setAdjuntosSeleccionados(Set<AdjuntoNoticia> adjuntosSeleccionados) {
		this.adjuntosSeleccionados = adjuntosSeleccionados;
	}

	public List<Rol> getComboxRoles() {
		return comboxRoles;
	}

	public void setComboxRoles(List<Rol> comboxRoles) {
		this.comboxRoles = comboxRoles;
	}

	public Set<Publico> getPublicos() {
		return publicos;
	}

	public void setPublicos(Set<Publico> publicos) {
		this.publicos = publicos;
	}

	public Publico getPublico() {
		return publico;
	}

	public void setPublico(Publico publico) {
		this.publico = publico;
	}

	@Command
	@NotifyChange({"noticia","adjuntoNoticia","adjuntos"})
	public void GuardarNoticia(@SelectorParam("textbox,datebox,timebox") LinkedList<InputElement> inputs){
		
		boolean camposValidos=true;
		
		for(InputElement input:inputs){			
			if (!input.isValid()){
				camposValidos=false;
				break;
			}			
		}
		
		if(!camposValidos){
			Messagebox.show("Debe completar todos los campos para continuar", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}else {	
			try{
				servicioNoticia.Guardar(noticia);
				Messagebox.show("El registro se ha guardado exitosamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
				
				if(!adjuntosPorEliminar.isEmpty())
				{
					servicioAdjuntoNoticia.EliminarVarios(adjuntosPorEliminar);
				}
				if(!publicosPorEliminar.isEmpty()){
					servicioPublico.eliminarVarios( publicosPorEliminar);
				}
				
			}
			catch(Exception e){}
			Limpiar();
			Window myWindow = (Window) Path.getComponent("/id_noticiah");
			myWindow.detach();
		}
	}
	
	@Command
	@NotifyChange({"noticia","adjuntoNoticia","adjuntos","publicos","publico"})
	public void Limpiar(){
		noticia = null;
		adjuntos = null;
		noticia = new Noticia();
		adjuntoNoticia = new AdjuntoNoticia();
		adjuntos = new HashSet<AdjuntoNoticia>();
		adjuntosPorEliminar = new HashSet<AdjuntoNoticia>();
		this.publico = new Publico();
		this.publicos = new HashSet<Publico>();
	}
	
	@Command
	@NotifyChange({"noticia","adjuntoNoticia","adjuntos"})
	public void AgregarAdjunto(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		media = event.getMedia();
		adjuntoNoticia = new AdjuntoNoticia();
		arch = new Archivo();
		if (media != null) {
			if (media instanceof org.zkoss.image.Image){
				arch.setNombreArchivo(media.getName());
				arch.setTipo(media.getContentType());
				arch.setContenido(media.getByteData());
				
				adjuntoNoticia.setFoto(arch);
				
				noticia.agregarAdjunto(adjuntoNoticia);
				
				adjuntos = noticia.getAdjuntoNoticias();
								
			}
			else{
				Messagebox.show("El archivo: "+media.getName()+" no es una imagen valida", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		}
	}
	
	@Command
	@NotifyChange({"adjuntos", "noticia", "publicos"})
	public void EliminarAdjunto(@BindingParam("adjuntoSeleccionado") AdjuntoNoticia adjuntoSeleccionado){
		adjuntosPorEliminar.add(adjuntoSeleccionado);
		getAdjuntos().remove(adjuntoSeleccionado);
		Messagebox.show("Adjunto eliminado exitosamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({"adjuntos"})
	public void EliminarAdjuntos(){
		if(adjuntosSeleccionados != null){
			for(AdjuntoNoticia an : adjuntosSeleccionados)
			{
				adjuntosPorEliminar.add(an);
			}
			getAdjuntos().removeAll((adjuntosSeleccionados));
			Messagebox.show("Los registros se han eliminado exitosamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay registros seleccionados", "Advertencia", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	@Command
	public void VerAdjunto(@BindingParam("imagen") AdjuntoNoticia aux) {
		 Map<String,AdjuntoNoticia> mapAux = new HashMap<String, AdjuntoNoticia>();
		 mapAux.put("adjuntoaux", aux);
		 window = (Window) Executions.createComponents("/vistas/gestionevento/verAdjuntoNoticia.zul", null, mapAux);
		 window.doOverlapped(); //doPopup();//doModal();
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;	
	}
	
	/***************************************************/
	/*****************para publico**********************/
	
	@Command
	@NotifyChange({"noticia","publicos","publico"})
	public void CargarRol(){
		if(publico.getRol() != null){
			publico.setResponder((short)-1);
			publico.setVerResultados((short)-1);
			if(!RolContenido(publico)){
				this.publicos.add(publico);
				this.noticia.getPublicos().addAll(publicos);
			}else{
				Messagebox.show("El rol ya fue seleccionado", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}else{
			Messagebox.show("No ha seleccionado un rol", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
		LimpiarRol();
	}
	
	//terorna true si el rol ya existe de lo contrario retorna false
	private boolean RolContenido(Publico p){
		for(Publico aux:publicos){
			if(aux.getRol().getId()==p.getRol().getId())
				return true;
		}
		return false;
	}
		
	@NotifyChange({"noticia","publico"})
	public void LimpiarRol() {
		publico = new Publico();
	}
	
	@Command
	@NotifyChange({"publicos"})
	public void EliminarPublico(@BindingParam("publicoSeleccionado") Publico publicoSeleccionado){
		publicosPorEliminar.add(publicoSeleccionado);
		getPublicos().remove(publicoSeleccionado);
	}

}
