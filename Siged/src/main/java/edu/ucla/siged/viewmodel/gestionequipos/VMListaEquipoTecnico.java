package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.List;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoTecnicoI;

public class VMListaEquipoTecnico {

	Window window;

	@WireVariable ServicioEquipoTecnicoI servicioEquipoTecnico;
	@WireVariable Equipo equipo;
	private List<Equipo> listaEquipos;
	private List<Equipo> equiposSeleccionados;
	
// --------------------------------------------------------------------------------------------------------- Inicio de M�todos Getters and Setters
	public Equipo getEquipo() {	return equipo; }
	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public List<Equipo> getListaEquipos(){ return listaEquipos; }	
	public void setListaEquipos(List<Equipo> listaEquipos) { this.listaEquipos = listaEquipos; }
		
	public List<Equipo> getEquiposSeleccionados() { return equiposSeleccionados; }
	public void setEquiposSeleccionados(List<Equipo> equiposSeleccionados) { this.equiposSeleccionados = equiposSeleccionados; }
// --------------------------------------------------------------------------------------------------------- Fin de M�todos Getters and Setters	
	
	@Init
	public void inicializar(){ 
		this.listaEquipos = servicioEquipoTecnico.buscarTodos();
		
		System.out.println(  listaEquipos.get(0).getEquipoTecnicos().size()  );
		
	}
	
	@Command
	public void agregarEquipo(){
		Sessions.getCurrent().setAttribute("equiposeleccionado", null);
		window = (Window)Executions.createComponents("/vistas/gestionequipo/registrarequipotecnico.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange("equipo")
	public void editarEquipo( @BindingParam("equipoSeleccionado") Equipo equipoSeleccionado ){
		//Map<String, Equipo> mapEquipo = new HashMap<String, Equipo>();
		//mapEquipo.put( "equipo", equipoSeleccionado );
		this.equipo = equipoSeleccionado;	
		Sessions.getCurrent().setAttribute("equiposeleccionado", equipoSeleccionado);
		window = (Window)Executions.createComponents("/vistas/gestionequipo/gestionarequipo.zul", null, null);
		window.doModal();
	}
			
	@Command
	@NotifyChange("listaEquipos")
    public void eliminarEquipo( @BindingParam("equipoSeleccionado") Equipo equipoSeleccionado){
		servicioEquipoTecnico.eliminarEquipo(equipoSeleccionado);
//		actualizarListaEquipos();
	}
	
	@Command
	@NotifyChange("listaEquipos")
	public void eliminarEquipos(){
		servicioEquipoTecnico.eliminarEquipos( equiposSeleccionados );
	}
	
	
	@Command
	@NotifyChange({"listaEquipos"})
	public String verStatusEnLista(Integer estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@GlobalCommand
	@NotifyChange("listaEquipos")
	public void actualizarListaEquipos(){	
		this.listaEquipos = servicioEquipoTecnico.buscarTodos();
	}

}