/** 
 * 	VMListaRango
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;

public class VMListaRango {
	@WireVariable
	ServicioRangoI servicioRango;
	@WireVariable
	Rango rango;
	List<Rango> listaRangos;
	List<Rango> rangosSeleccionados;
	Window window;
	short tipoOperacion;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales;
	boolean estatusFiltro;
	Integer id=0;
	String descripcion="";
	String observacion="";
	Integer estatusCombo=0;
	
	public Rango getRango() {
		return rango;
	}
	
	public void setRango(Rango rango) {
		this.rango = rango;
	}	
	
	public List<Rango> getListaRangos() {
		return listaRangos;
	}
	
	public void setListaRangos(List<Rango> listaRangos) {
		this.listaRangos = listaRangos;
	}
	
	public List<Rango> getRangosSeleccionados() {
		return rangosSeleccionados;
	}

	public void setRangosSeleccionados(List<Rango> rangosSeleccionados) {
		this.rangosSeleccionados = rangosSeleccionados;
	}

	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina = servicioRango.TAMANO_PAGINA;
		return tamanoPagina;
	}
		
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getObservacion() {
		return observacion;
	}
	
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public Integer getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Integer estatus) {
		this.estatusCombo = estatus;
	}
		
	@Init
	public void init(){
		this.listaRangos = servicioRango.buscarTodos(0).getContent();
		this.registrosTotales= servicioRango.totalRangos();
	}
	
	@Command
	public void agregarRango(){
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestionequipo/rango.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange({"listaRangos"})
	public void eliminarRango(@BindingParam("rangoSeleccionado") final Rango rangoSeleccionado){
		this.tipoOperacion=3;
		Messagebox.show("�Est� seguro que desea eliminar este registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new EventListener<Event>() {
					public void onEvent(Event event) throws Exception {
						if(Messagebox.ON_YES.equals(event.getName())){
							servicioRango.eliminar(rangoSeleccionado);
							BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
							Messagebox.show("El registro se ha eliminado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
							actualizarLista();
						}
					}
				   });
	actualizarLista();
	}
	
		@Command
		@NotifyChange("listaRangos")
		public void eliminarRangos(@BindingParam("lista") Listbox lista){
			this.tipoOperacion=4;
			if(lista.getSelectedItems().size()>0)
			{
				Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
						new EventListener<Event>() {
					
							public void onEvent(Event event) throws Exception {
	                        
								if(Messagebox.ON_YES.equals(event.getName())){
									
									servicioRango.eliminarVarios(rangosSeleccionados);
									BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
									Messagebox.show("Los registros seleccionados se han eliminado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
									rangosSeleccionados=null;
								}
							}
						});
				
			
			}
			else Messagebox.show("Debe seleccionar los registros que desea eliminar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
		}

	@Command
	@NotifyChange({"rango"})
	public void editarRango(@BindingParam("rangoSeleccionado") Rango rangoSeleccionado) {
		this.rango=rangoSeleccionado;
		tipoOperacion = 2;
		Map<String, Rango> mapa = new HashMap<String, Rango>();
	    mapa.put("rango", rangoSeleccionado);
		window = (Window)Executions.createComponents("/vistas/gestionequipo/rango.zul", null, mapa);
		window.doModal();
	}

		@GlobalCommand
		@NotifyChange({"listaRangos","registrosTotales","paginaActual"})
	    public void actualizarLista(){
			int ultimaPagina=0;
			if (this.tipoOperacion==1){
			   this.getRegistrosTotales();
			   
			   if ((this.registrosTotales+1)%this.tamanoPagina==0)
			      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			   else
				  ultimaPagina =((int) (this.registrosTotales+1)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina; // Se coloca en la �ltima p�gina para que quede visible el que se acaba de ingresar.
			}
			   paginar();
			 
			   if(this.tipoOperacion==3 || this.tipoOperacion==4){
				   this.getRegistrosTotales();
				   
				   if ((this.registrosTotales+1)%this.tamanoPagina==0)
				      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
				   else
					  ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
				   this.paginaActual=ultimaPagina;
				 paginar();
				}	
		}

	@GlobalCommand
	@NotifyChange({"listaRangos","registrosTotales","paginaActual","rangosSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
			this.listaRangos = servicioRango.buscarTodos(this.paginaActual).getContent();
			this.registrosTotales=servicioRango.totalRangos();
		}
		else{
			ejecutarFiltro();
		    }

	}

	@Command
	@NotifyChange({"listaRangos"})
	public String verStatusEnLista(Integer estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	@NotifyChange({"listaRangos","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	@Command
	@NotifyChange({"listaRangos","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql="";
		String filtroEstatus;
		this.estatusFiltro=true;
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		if (id != null)
			
			jpql = " descripcion like '%"+descripcion+"%' and observacion like '%"+observacion+"%' and estatus in "+filtroEstatus+"";
		
		this.listaRangos = servicioRango.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales = servicioRango.totalRangosFiltrados(jpql);
	} 
	
	@Command
	@NotifyChange({"listaRangos","registrosTotales","paginaActual","rangosSeleccionados",
				    "descripcion","observacion","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		id=0;
		descripcion="";
		observacion="";
		estatusCombo=0;
	}

}