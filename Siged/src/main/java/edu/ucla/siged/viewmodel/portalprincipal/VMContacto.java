/** 
 * 	VMContacto
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.portalprincipal;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestioneventos.Sugerencia;
import edu.ucla.siged.servicio.interfaz.gestionencuestas.ServicioSugerenciaI;

public class VMContacto {
	@WireVariable
	ServicioSugerenciaI servicioSugerencia;
	@WireVariable
	List<Integer> listaEstatus;
	Integer estatusSeleccionado;
	private Window ventanaContacto;
	private Window ventanaResponderContacto;
	short tipoOperacion;
	Sugerencia sugerencia;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////
	
	public Window getVentanaResponderContacto() {
		return ventanaResponderContacto;
	}

	public void setVentanaResponderContacto(Window ventanaResponderContacto) {
		this.ventanaResponderContacto = ventanaResponderContacto;
	}
	
	public Sugerencia getSugerencia() {
		return sugerencia;
	}

	public void setSugerencia(Sugerencia sugerencia) {
		this.sugerencia = sugerencia;
	}

	public Integer getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public Window getVentanaContacto() {
		return ventanaContacto;
	}

	public void setVentanaContacto(Window ventanaContacto) {
		this.ventanaContacto = ventanaContacto;
	}

	public void setEstatusSeleccionado(Integer estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Integer> getListaEstatus() {
		return listaEstatus;
	}

	/////////////////////////////////////// INIT////////////////////////////////////////////////////////
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sugerencia") Sugerencia sugerenciaSeleccionada) {
        if (sugerenciaSeleccionada!=null){
        	this.tipoOperacion=2;
            this.sugerencia = sugerenciaSeleccionada;
            this.estatusSeleccionado= this.sugerencia.getEstatusSugerencia();
        }else{
        	this.sugerencia = new Sugerencia();
        	this.tipoOperacion=1;
        }
	}
	
	////////////////////////////////////////////GUARDAR/////////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"sugerencia","limpiar","estatusSeleccionado","actualizarLista"})
	public void enviarSugerencia(@SelectorParam(".sugerencia_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaContacto") Window ventanaContacto){
		this.ventanaContacto = ventanaContacto;
		if(this.estatusSeleccionado!=null)
			this.sugerencia.setEstatusSugerencia(estatusSeleccionado);
		boolean camposValidos=true;
		for(InputElement input:inputs){
			if(!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		
			if(!camposValidos || sugerencia.getAutor()==null || sugerencia.getAutor().isEmpty() || sugerencia.getCelular()==null || sugerencia.getCelular().isEmpty() ||
					sugerencia.getDescripcion()==null || sugerencia.getDescripcion().isEmpty() || sugerencia.getEmail()==null || sugerencia.getEmail().isEmpty()){
				Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK,Messagebox.EXCLAMATION);
			}
			else{
				sugerencia.setFechaEmision(new Date());
				sugerencia.setEstatusSugerencia(Integer.parseInt("1"));
				servicioSugerencia.guardar(sugerencia);
				Messagebox.show("El registro se ha guardado exitosamente","Informaci�n",Messagebox.OK,Messagebox.INFORMATION);
				cerrarVentanaContacto();
			}
		limpiar();		
		}

	
	@Command
	@NotifyChange({"sugerencia","paginar"})
	public void responderSugerencia(@BindingParam("ventanaResponderContacto") Window ventanaResponderContacto){
		this.ventanaResponderContacto = ventanaResponderContacto;
		
		Messagebox.show("�Desea enviar la respuesta?","Pregunta",Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
				new EventListener<Event>(){
			public void onEvent(Event event) throws Exception{
				if(Messagebox.ON_YES.equals(event.getName())){
					sugerencia.setFechaRespuesta(new Date());
					sugerencia.setEstatusSugerencia(Integer.parseInt("2"));
					servicioSugerencia.guardar(sugerencia);
					Messagebox.show("Respuesta ha sido enviada a "+sugerencia.getAutor(),"",Messagebox.OK,Messagebox.INFORMATION);
					//servicioEmails.enviarEmailAprobarPostulante(postulante.getEmail(), postulante.getNombre());
					limpiar();
					BindUtils.postGlobalCommand(null, null, "paginar", null);
					cerrarVentanaResponderContacto();
				}		
			}
		});
	}

	////////////////////////////////////////////////LIMPIAR///////////////////////////////////////////////
	@Command
	@NotifyChange({"sugerencia","estatusSeleccionado"})
	public void limpiar(){
		sugerencia.setId(null);
		sugerencia.setAutor("");
		sugerencia.setCelular("");
		sugerencia.setDescripcion("");
		sugerencia.setEmail("");
		estatusSeleccionado=1;
	}
	
	public void cerrarVentanaContacto(){
		ventanaContacto.detach();
	}
	
	public void cerrarVentanaResponderContacto(){
		ventanaResponderContacto.detach();
	}
	
	///////////////////////////////////////////VER ESTATUS////////////////////////////////////////////////////////
	@Command
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
}