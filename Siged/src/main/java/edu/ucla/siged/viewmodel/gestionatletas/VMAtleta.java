package edu.ucla.siged.viewmodel.gestionatletas;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import com.lowagie.text.Cell;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionatleta.AtletaRepresentante;
import edu.ucla.siged.domain.gestionatleta.Documento;
import edu.ucla.siged.domain.gestionatleta.EstatusAtleta;
import edu.ucla.siged.domain.gestionatleta.HistoriaAtleta;
import edu.ucla.siged.domain.gestionatleta.Reposo;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.servicio.impl.gestionatletas.ServicioAtleta;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioEstatusAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioTipoDocumentoI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAnotadorI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMAtleta {
	
	//Para La ventana tabatleta.zul
	private @WireVariable ServicioAtletaI servicioAtleta;
	private @WireVariable ServicioEstatusAtletaI servicioEstatusAtleta;
	private @WireVariable ServicioUsuarioI servicioUsuario;
	private @WireVariable ServicioRepresentanteI servicioRepresentante;
	private @WireVariable ServicioTecnicoI servicioTecnico;
	private @WireVariable ServicioAnotadorI servicioAnotador;
	private @WireVariable Atleta atleta;
	// eliminado private List<EstatusAtleta> listaEstatusAtleta;
	private EstatusAtleta estatusAtletaSeleccionado = null;
	//Para la ventana tabatleta/Reposo
	private @WireVariable ServicioTipoDocumentoI servicioTipoDocumento;
	//Para la ventana tabatleta/Representante
	
	private Set<Documento> documentos = new HashSet<Documento>();
	private Set<AtletaRepresentante> atletaRepresentantes = new HashSet<AtletaRepresentante>();
	
	private Set<Reposo> repososSeleccionado = new HashSet<Reposo>();
	private Set<AtletaRepresentante> atletaRepresentantesSeleccionado = new HashSet<AtletaRepresentante>();
	private Set<Documento> documentosSeleccionado = new HashSet<Documento>();
	Window window;
	private AImage imagen;
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("atleta") Atleta atletaSeleccionado){
		this.estatusAtletaSeleccionado = servicioEstatusAtleta.obtenerEstatusSinAsignar();
		Selectors.wireComponents(view, this, false);

		if(atletaSeleccionado!=null){
			this.atleta = atletaSeleccionado;
			this.estatusAtletaSeleccionado=atletaSeleccionado.getEstatusActual();
			for(Documento documento: atletaSeleccionado.getDocumentos()){
				if(documento.getTipoDocumento().getId() != 1){
					this.documentos.add(documento);
				}
			}
			for(AtletaRepresentante atletaRepresentante: atletaSeleccionado.getAtletaRepresentantes()){
				if(atletaRepresentante.getRepresentante().getEstatus()!= (short)0){
					this.atletaRepresentantes.add(atletaRepresentante);
				}
			}
			if(atletaSeleccionado.getFoto() != null){
				try{
					imagen = new AImage(atleta.getFoto().getNombreArchivo(), atleta.getFoto().getContenido());
				}catch(IOException e){
				}
			}
		}
	}
	@Command
	@NotifyChange({"atleta","imagen", "estatusAtletaSeleccionado","documentos","atletaRepresentantes","listaEstatusAtleta"})
	public void guardarAtleta(@BindingParam("ventana") Window ventanaAtleta,@SelectorParam("textbox,datebox,intbox") LinkedList<InputElement> inputs){
		 boolean camposValidos=true;
			
			for(InputElement input:inputs){
				if (!input.isValid()){
					camposValidos=false;
					break;
				}
				
			}
			/*Validando Campos*/
			if(!camposValidos){
				Messagebox.show("Campos no Validos", "ERROR", Messagebox.OK, Messagebox.ERROR);
			}else{

		if(this.atletaRepresentantes.size()!=0){
			boolean representanteLegal = false;
			for(AtletaRepresentante atletaRepresentante: atleta.getAtletaRepresentantes()){
				if((atletaRepresentante.getRepresentante().getEstatus()!= ((short)0)) &&
				   (atletaRepresentante.getRepresentanteLegal()==true) &&
				   (representanteLegal!=true)){
					representanteLegal = true;
				}
			}
			if(representanteLegal==true){
//				if(estatusAtletaSeleccionado!=atleta.getEstatusActual()){
//					Date fechaHoy = new Date();
//					HistoriaAtleta historiaAtleta = new HistoriaAtleta(fechaHoy,(short) 1, estatusAtletaSeleccionado);
//					atleta.addHistoriasAtletla(historiaAtleta);
//					servicioAtleta.guardarAtletaConEstatus(atleta,this.repososSeleccionado,this.documentosSeleccionado);
//					limpiarAtleta();
//				}else{
				
						if (atleta.getId()==null){
							if(!tipoDePersona(atleta.getCedula())){
								Date fechaHoy = new Date();
								HistoriaAtleta historiaAtleta = new HistoriaAtleta(fechaHoy,(short) 1, estatusAtletaSeleccionado);
								atleta.addHistoriasAtletla(historiaAtleta);
								servicioAtleta.guardarAtletaConEstatus(atleta,this.repososSeleccionado,this.documentosSeleccionado);
								BindUtils.postGlobalCommand(null, null, "cambiarEstatusPostulante", null);
								BindUtils.postGlobalCommand(null, null, "actualizarListaPostulante", null);	
								
								Messagebox.show("Atleta Grabado Satisfactoriamente","Información", Messagebox.OK, Messagebox.INFORMATION);
								ventanaAtleta.detach();
							} else {
								Messagebox.show("Esta Cedula Ya Esta Registrada en el Sitema1", "Error", Messagebox.OK, Messagebox.ERROR);
							}
						}else{
							if(!servicioAtleta.buscarAtletaPorID(atleta.getId()).getCedula().equals(atleta.getCedula())){
								if(!tipoDePersona(atleta.getCedula())){
									servicioUsuario.guardarCedulaUsuario((servicioAtleta.buscarAtletaPorID(atleta.getId()).getCedula()),atleta.getCedula());
									servicioAtleta.guardarAtleta(atleta,this.repososSeleccionado,this.documentosSeleccionado);
									BindUtils.postGlobalCommand(null, null, "actualizarListaAtletas", null);
									limpiarAtleta();
									
									Messagebox.show("Atleta Grabado Satisfactoriamente","Información", Messagebox.OK, Messagebox.INFORMATION);
									ventanaAtleta.detach();
								}else{
									Messagebox.show("Esta Cedula Ya Esta Registrada en el Sitema2", "Error", Messagebox.OK, Messagebox.ERROR);
								}
							}else{
								servicioAtleta.guardarAtleta(atleta,this.repososSeleccionado,this.documentosSeleccionado);
								BindUtils.postGlobalCommand(null, null, "actualizarListaAtletas", null);
								limpiarAtleta();
								
								Messagebox.show("Atleta Grabado Satisfactoriamente","Información", Messagebox.OK, Messagebox.INFORMATION);
								ventanaAtleta.detach();
							}
						}
//				}

			}else{
				Messagebox.show("Debe tener al menos un Representante Legal", "Error", Messagebox.OK, Messagebox.ERROR);
			}
		}
		else{
			Messagebox.show("Debe tener al menos un Representante", "Error", Messagebox.OK, Messagebox.ERROR);
		}
	}
	}
	
	public boolean tipoDePersona(String cedulaUsuario){
//		Usuario 		0
//		Atleta			1
//		Representante	2
//		Tecnico			3
//		Anotador		4
		
		
		if(servicioAtleta.buscarAtletaPorCedula(cedulaUsuario)     != null || 
		   servicioRepresentante.buscarPorCedula(cedulaUsuario)    != null ||
		   servicioTecnico.buscarPorCedula(cedulaUsuario)		   != null ||
		   servicioAnotador.buscarAnotadorPorCedula(cedulaUsuario) != null ||
		   servicioUsuario.buscarPorCedula(cedulaUsuario)    != null){
			return true;
		}else{
			return false;
		}
	}
	
	@Command
	@NotifyChange({"atleta","imagen", "estatusAtletaSeleccionado","documentos","atletaRepresentantes","listaEstatusAtleta"})
	public void limpiarDatosAtleta(){
		Date fechaHoy = new Date();
		atleta.setFechaAdmision(fechaHoy);
		atleta.setColegio("");
		atleta.setGrado("");
		atleta.setUltimaDivisa("");
		atleta.setPosicion("");
		atleta.setTallaCamisa(null);
		atleta.setTallaPantalon(null);
		atleta.setFoto(null); 
		atleta.setHistoriaAtleta(new HashSet<HistoriaAtleta>());
		atleta.setReposos(new HashSet<Reposo>());
		atleta.setDocumentos(new HashSet<Documento>());
		atleta.setEstatusActual(new EstatusAtleta());
		imagen = null;
		estatusAtletaSeleccionado = servicioEstatusAtleta.obtenerEstatusSinAsignar();
		documentos = new HashSet<Documento>();
		this.repososSeleccionado.clear();
		this.documentosSeleccionado.clear();
		atletaRepresentantes = new HashSet<AtletaRepresentante>();
	}
	
	@Command
	@NotifyChange({"atleta","imagen", "estatusAtletaSeleccionado","documentos","atletaRepresentantes","listaEstatusAtleta"})
	public void limpiarAtleta() {
		atleta.setId(null);
		atleta.setCedula("");
		atleta.setNombre("");
		atleta.setApellido("");
		atleta.setFechaNacimiento(null);
		atleta.setLugarNacimiento("");
		atleta.setDireccion("");
		atleta.setTelefono("");
		atleta.setColegio("");
		atleta.setGrado("");
		atleta.setUltimaDivisa("");
		atleta.setPosicion("");
		atleta.setTallaCamisa(null);
		atleta.setTallaPantalon(null);
		Date fechaHoy = new Date();
		atleta.setFechaAdmision(fechaHoy);
		atleta.setSordoMudo(null);
		atleta.setAsmatico(null);
		atleta.setIntervencionQuirurgica(null);
		atleta.setRetardo(null);
		atleta.setDescripcionIntervencion(null);
		atleta.setEmail("");
		atleta.setFoto(null); 
		atleta.setHistoriaAtleta(new HashSet<HistoriaAtleta>());
		atleta.setReposos(new HashSet<Reposo>());
		atleta.setDocumentos(new HashSet<Documento>());
		atleta.setEstatusActual(new EstatusAtleta());
		imagen = null;
		estatusAtletaSeleccionado = servicioEstatusAtleta.obtenerEstatusSinAsignar();
		documentos = new HashSet<Documento>();
		this.repososSeleccionado.clear();
		this.documentosSeleccionado.clear();
		atletaRepresentantes = new HashSet<AtletaRepresentante>();
	}
	
	@Command
	public void abrirVentanaReposo(@BindingParam("reposoSeleccionado") Reposo reposoSeleccionado){
		try{
			Map<String, Reposo> mapReposo = new HashMap<String, Reposo>();
			mapReposo.put("reposo", reposoSeleccionado);
			window = (Window)Executions.createComponents("vistas/gestionatleta/reposo.zul", null, mapReposo);
			window.doModal();
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	public void abrirVentanaRepresentante(@BindingParam("atletaRepresentanteSeleccionado") AtletaRepresentante atletaRepresentanteSeleccionado){
		try{
			Map<String, AtletaRepresentante> mapRepresentante = new HashMap<String, AtletaRepresentante>();
			mapRepresentante.put("atletaRepresentante", atletaRepresentanteSeleccionado);
			window = (Window)Executions.createComponents("vistas/gestionatleta/representante.zul", null, mapRepresentante);
			window.doModal();
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
	
	@Command
	@NotifyChange({"atleta","imagen"})
	public void cargarImagen(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media= event.getMedia();
		if(media != null){
			if(media instanceof org.zkoss.image.Image){
				Archivo foto = new Archivo();
				foto.setNombreArchivo(media.getName());
				foto.setTipo(media.getContentType());
				foto.setContenido(media.getByteData());
				atleta.setFoto(foto);
				imagen = (AImage) media;
			}
		}
	}
	
	@GlobalCommand
	@NotifyChange({"atleta"})
	public void agregarReposo(@BindingParam("reposo") Reposo reposo){		
		if(reposo.getDocumento().getDocumento().getTamano()>0){
			reposo.getDocumento().setTipoDocumento(servicioTipoDocumento.tipoDocumento(1));
			reposo.getDocumento().setAtleta(atleta);
			atleta.addReposos(reposo);
		}
	}
	
	@Command
	@NotifyChange({"atleta"})
	public void eliminarReposo(@BindingParam("reposoSeleccionado") Reposo reposoSeleccionado){
		if(reposoSeleccionado.getId()!=null){
			this.repososSeleccionado.add(reposoSeleccionado);
		}
		atleta.removeReposo(reposoSeleccionado);
	}
	
	@Command
	@NotifyChange({"atleta"})
	public void eliminarReposos(){
		atleta.removeReposos(this.repososSeleccionado);
		Set<Reposo> repososAuxiliar = repososSeleccionado;
		for(Reposo reposo:repososAuxiliar){
			if(reposo.getId()==null)
				repososSeleccionado.remove(reposo);
		}
	}
	
	@GlobalCommand
	@NotifyChange({"atleta","atletaRepresentantes"})
	public void agregarAtletaRepresentante(@BindingParam("atletaRepresentante") AtletaRepresentante atletaRepresentante){
		if(atletaRepresentante.getRepresentante()!=null){
			atleta.addAtletaRepresentantes(atletaRepresentante);
			this.atletaRepresentantes.add(atletaRepresentante);
		}
	}
	
	@Command
	@NotifyChange({"atleta","atletaRepresentantes"})
	public void eliminarAtletaRepresentante(@BindingParam("atletaRepresentanteSeleccionado") AtletaRepresentante atletaRepresentanteSeleccionado){
		atletaRepresentanteSeleccionado.getRepresentante().setEstatus((short)0);
		this.atletaRepresentantes.remove(atletaRepresentanteSeleccionado);
	}
	
	@Command
	@NotifyChange({"atleta","atletaRepresentantes"})
	public void eliminarAtletaRepresentantes(){
		for(AtletaRepresentante atletaRepresentante: this.atletaRepresentantesSeleccionado){
			atletaRepresentante.getRepresentante().setEstatus((short)0);
		}
		this.atletaRepresentantes.removeAll(atletaRepresentantesSeleccionado);
	}
	
	@Command
	@NotifyChange({"atleta","documentos"})
	public void agregarDocumento(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media = event.getMedia();
		if(media != null){
			if(media.getContentType().equals("image/jpeg") ||
			   media.getContentType().equals("image/png") ||
			   media.getContentType().equals("application/pdf")){
					Documento documento = new Documento();
					Archivo archivo = new Archivo();
					documento.setAtleta(this.atleta);
					documento.setTipoDocumento(servicioTipoDocumento.tipoDocumento(2));
					archivo.setNombreArchivo(media.getName());
					archivo.setTipo(media.getContentType());
					archivo.setContenido(media.getByteData());
					documento.setDocumento(archivo);
					this.documentos.add(documento);
					this.atleta.addDocumento(documento);
			}else{
				Messagebox.show("Solo se permite Adjuntar los formatos PNG,JPEG Y PDF","Error",Messagebox.OK, Messagebox.ERROR);
			}
		}
	}
	
	@Command
	@NotifyChange({"atleta","documentos"})
	public void eliminarDocumento(@BindingParam("documentoSeleccionado") Documento documentoSeleccionado){
		if(documentoSeleccionado!=null){
			this.documentosSeleccionado.add(documentoSeleccionado);
		}
		this.documentos.remove(documentoSeleccionado);
		this.atleta.removeDocumento(documentoSeleccionado);
	}
	
	@Command
	@NotifyChange({"atleta","documentos"})
	public void eliminarDocumentos(){
		this.documentos.removeAll(documentosSeleccionado);
		this.atleta.removeDocumentos(documentosSeleccionado);
		Set<Documento> documentosAuxiliar = documentosSeleccionado;
		for(Documento documento: documentosAuxiliar){
			if(documento.getId()==null)
				documentosSeleccionado.remove(documento);
		}
	}
	
	@Command
	public void descargarReposo(@BindingParam("reposo") Reposo reposo ){
		Filedownload.save(reposo.getDocumento().getDocumento().getContenido(),
						  reposo.getDocumento().getDocumento().getTipo(),
						  reposo.getDocumento().getDocumento().getNombreArchivo());
	}
	
	@Command
	public void descargarDocumento(@BindingParam("documento") Documento documento ){
		Filedownload.save(documento.getDocumento().getContenido(),
						  documento.getDocumento().getTipo(),
						  documento.getDocumento().getNombreArchivo());
	}
	
	
	public Atleta getAtleta() {
		return atleta;
	}
	
	public void setAtleta(Atleta atleta) {
		this.atleta = atleta;
	}

	public EstatusAtleta getEstatusAtletaSeleccionado() {
		return estatusAtletaSeleccionado;
	}
	
	public void setEstatusAtletaSeleccionado(EstatusAtleta estatusAtletaSeleccionado) {
		this.estatusAtletaSeleccionado = estatusAtletaSeleccionado;
	}
	
	public AImage getImagen() {
		return imagen;
	}
	
	public void setImagen(AImage imagen) {
		this.imagen = imagen;
	}
	
	public Set<Documento> getDocumentos() {
		return documentos;
	}
	
	public void setDocumentos(Set<Documento> documentos) {
		this.documentos = documentos;
	}
	
	public Set<Reposo> getRepososSeleccionado() {
		return repososSeleccionado;
	}
	
	public void setRepososSeleccionado(Set<Reposo> repososSeleccionado) {
		this.repososSeleccionado = repososSeleccionado;
	}
	
	public Set<AtletaRepresentante> getAtletaRepresentantesSeleccionado() {
		return atletaRepresentantesSeleccionado;
	}
	
	public void setAtletaRepresentantesSeleccionado(
			Set<AtletaRepresentante> atletaRepresentantesSeleccionado) {
		this.atletaRepresentantesSeleccionado = atletaRepresentantesSeleccionado;
	}
	
	public Set<AtletaRepresentante> getAtletaRepresentantes() {
		return atletaRepresentantes;
	}
	
	public void setAtletaRepresentantes(
			Set<AtletaRepresentante> atletaRepresentantes) {
		this.atletaRepresentantes = atletaRepresentantes;
	}
	
	public Set<Documento> getDocumentosSeleccionado() {
		return documentosSeleccionado;
	}
	
	public void setDocumentosSeleccionado(Set<Documento> documentosSeleccionado) {
		this.documentosSeleccionado = documentosSeleccionado;
	}
}