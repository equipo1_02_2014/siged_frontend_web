/** 
 * 	VMListaCategoria
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;

public class VMListaCategoria {
	@WireVariable
	ServicioCategoriaI servicioCategoria;
	@WireVariable
	Categoria categoria;
	List<Categoria> listaCategorias;
	List<Categoria> categoriasSeleccionadas;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;
	String nombre="";
	short estatusCombo=0;
	boolean estatusFiltro;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public List<Categoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<Categoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public List<Categoria> getCategoriasSeleccionadas() {
		return categoriasSeleccionadas;
	}

	public void setCategoriasSeleccionadas(List<Categoria> categoriasSeleccionadas) {
		this.categoriasSeleccionadas = categoriasSeleccionadas;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioCategoria.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}
	
////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	@Init
	public void init(){
		this.listaCategorias = servicioCategoria.buscarTodos(0).getContent();
		this.registrosTotales= servicioCategoria.totalCategorias();
	}

////////////////////////////////////////AGREGAR CATEGORIA///////////////////////////////////////////////////////////

	@Command
	public void agregarCategoria() {
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestionequipo/categoria.zul", null, null);
		window.doModal();
	}

/////////////////////////////////////////////EDITAR CATEGORIA/////////////////////////////////////////////////////////
	@Command
	@NotifyChange({"categoria"})
	public void editarCategoria(@BindingParam("categoriaSeleccionada") Categoria categoriaSeleccionada ) {
		this.categoria=categoriaSeleccionada;
		this.tipoOperacion=2;
		Map<String, Categoria> mapa = new HashMap<String, Categoria>();
		mapa.put("categoria", categoriaSeleccionada);
		window = (Window)Executions.createComponents("/vistas/gestionequipo/categoria.zul", null, mapa);
		window.doModal();
	}

//////////////////////////////////////ELIMINAR//////////////////////////////////////////////////////////////////////
	@Command
	@NotifyChange({"listaCategorias"})
	public void eliminarCategoria(@BindingParam("categoriaSeleccionada") final Categoria categoriaSeleccionada){
		this.tipoOperacion=3;
		Messagebox.show("�Est� seguro que desea eliminar este registro?","Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new EventListener<Event>(){
					public void onEvent(Event event) throws Exception{
						if(Messagebox.ON_YES.equals(event.getName())){
							servicioCategoria.eliminarCategoria(categoriaSeleccionada);
							BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
							Messagebox.show("El registro ha sido eliminado exitosamente","Informaci�n",Messagebox.OK,Messagebox.INFORMATION);
						}
					}
		});
		actualizarLista();
	}

//////////////////////////////////////ELIMINAR VARIOS//////////////////////////////////////////////////////////////////////
	@Command
	@NotifyChange("listaCategorias")
	public void eliminarCategorias(@BindingParam("lista") Listbox lista){
		this.tipoOperacion=4;
		if(lista.getSelectedItems().size()>0){
			Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
							if(Messagebox.ON_YES.equals(event.getName())){
								servicioCategoria.eliminarCategorias(categoriasSeleccionadas);
								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
								Messagebox.show("Los registros seleccionados se han eliminado exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
								categoriasSeleccionadas=null;
							}
						}
					});		
		}
		else Messagebox.show("Debe seleccionar los registros que desea eliminar.","",Messagebox.OK, Messagebox.EXCLAMATION);
	}

//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////
	@GlobalCommand
	@NotifyChange({"listaCategorias","registrosTotales","paginaActual","categoriasSeleccionadas"})
	public void paginar(){
		if (estatusFiltro==false){
			this.listaCategorias = servicioCategoria.buscarTodos(this.paginaActual).getContent();
			this.registrosTotales=servicioCategoria.totalCategorias();
		}
		else{
			ejecutarFiltro();
		    }
	}
	
////////////////////////////////////////ACTUALIZAR LISTA/////////////////////////////////////////////////////////
			@GlobalCommand
			@NotifyChange({"listaCategorias","registrosTotales","paginaActual"})
		    public void actualizarLista(){
				int ultimaPagina=0;
				if (this.tipoOperacion==1){
				   this.getRegistrosTotales();
				   
				   if ((this.registrosTotales+1)%this.tamanoPagina==0)
				      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
				   else
					  ultimaPagina =((int) (this.registrosTotales+1)/this.tamanoPagina);
				   this.paginaActual=ultimaPagina; // Se coloca en la �ltima p�gina para que quede visible el que se acaba de ingresar.
				}
				   paginar();
				 
				   if(this.tipoOperacion==3 || this.tipoOperacion==4){
					   this.getRegistrosTotales();
					   
					   if ((this.registrosTotales+1)%this.tamanoPagina==0)
					      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
					   else
						  ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
					   this.paginaActual=ultimaPagina;
					 paginar();
					}	
			}

////////////////////////////////////////////// VER ESTATUS///////////////////////////////////////////////////////
	@Command
	@NotifyChange({"listaCategorias"})
	public String verStatusEnLista(Integer estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
		case 0:
			est="Inactivo";
			break;
		default:
			est="Activo";
		}
		return est;
	}

//////////////////////////////////////////////EJECUTAR FILTRO/////////////////////////////////////////////////////
	@Command
	@NotifyChange({"listaCategorias","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true; //El filtro se ha activado.
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
	
		if (nombre == null)
			jpql = " nombre like '%"+nombre+"%' and estatus in "+filtroEstatus+"";
		else
			jpql = " nombre like '%"+nombre+"%' and estatus in "+filtroEstatus+"";
	
		this.listaCategorias=servicioCategoria.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioCategoria.totalCategoriasFiltradas(jpql);
	}
	
/////////////////////////////////////////PARA INVOCAR AL EJECUTAR FILTRO///////////////////////////////////////////	
	@Command
	@NotifyChange({"listaCategorias","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}

/////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	
	@Command
	@NotifyChange({"listaCategorias","registrosTotales","paginaActual","categoriasSeleccionadas",
	"nombre","estatusCombo"})
		public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar(); 
		nombre="";
		estatusCombo=0;
	}

}