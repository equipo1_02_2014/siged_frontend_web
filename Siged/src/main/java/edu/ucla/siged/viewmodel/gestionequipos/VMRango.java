/** 
 * 	VMRango
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.LinkedList;
import java.util.List;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

public class VMRango { 
		@WireVariable 
		ServicioRangoI servicioRango;
		@WireVariable 
		Rango rango;
		private Window ventanaRango;
		List<Short> listaEstatus;
		Short estatusSeleccionado;
		short tipoOperacion;
		
		public ServicioRangoI getServicioRango() {
			return servicioRango;
		}

		public void setServicioRango(ServicioRangoI servicioRango) {
			this.servicioRango = servicioRango;
		}

		public Rango getRango() {
			return rango;
		}

		public void setRango(Rango rango) {
			this.rango = rango;
		}

		public List<Short> getListaEstatus() {
			return listaEstatus;
		}

		public Short getEstatusSeleccionado() {
			return estatusSeleccionado;
		}
		
		public void setEstatusSeleccionado(Short estatusSeleccionado) {
			this.estatusSeleccionado = estatusSeleccionado;
		}

		@Init
		 public void init(@ContextParam(ContextType.VIEW) Component view,
		            @ExecutionArgParam("rango") Rango rangoSeleccionado) {
		        if (rangoSeleccionado!=null){
		        	this.tipoOperacion=2;
		            this.rango = rangoSeleccionado;
		            this.estatusSeleccionado= rangoSeleccionado.getEstatus();
		        }
		        else{
		        	rango = new Rango();
		        	this.tipoOperacion=1;
		            }
		 }
			
		 @Command
		 @NotifyChange({"rango","limpiar","estatusSeleccionado","actualizarLista"})
		 public void guardarRango(@SelectorParam(".rango_restriccion") LinkedList<InputElement> inputs, @BindingParam("ventanaRango") Window ventanaRango){
			 this.ventanaRango = ventanaRango;
			if (this.estatusSeleccionado!=null)
				rango.setEstatus(estatusSeleccionado);
	        boolean camposValidos=true;
			
	        for(InputElement input:inputs){
				if (!input.isValid()){
					camposValidos=false;
				break;
				}
				
			}
			if(!camposValidos || rango.getDescripcion()==null || rango.getDescripcion().isEmpty() ||
					rango.getObservacion()==null || rango.getObservacion().isEmpty()){
				Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
			}
			else{
				if(this.tipoOperacion==1){
					rango.setEstatus(Short.parseShort("1"));
					servicioRango.guardar(rango);
					Messagebox.show("El registro se ha guardado exitosamente.","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
					Messagebox.show("�Desea agregar otro registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
	       					new EventListener<Event>() {
	       				
	       						public void onEvent(Event event) throws Exception {
	       							if(Messagebox.ON_NO.equals(event.getName())){
	       								
	       								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       								cerrarVentanaRango();
	       								
	       							}
	       							else BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
	       						}
	       					    });
					
					    limpiar();
					
				}
				
				if(this.tipoOperacion==2)
				    {
					rango.setEstatus(Short.parseShort("1"));	
					servicioRango.guardar(rango);
					Messagebox.show("Los cambios han sido guardados con �xito.","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
					cerrarVentanaRango();
					}
			    }
		 }
		
		 public void cerrarVentanaRango(){
			 ventanaRango.detach();
		 }
		
		 @Command
		 public String verStatus(Integer estatus){
			String est="Activo";
			switch (estatus){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
			}
			return est;
		}
        
		@Command
		@NotifyChange({"rango","estatusSeleccionado"})
		public void limpiar(){
			rango.setId(null);
			rango.setDescripcion("");
			rango.setObservacion("");
			estatusSeleccionado=1;
		}

}