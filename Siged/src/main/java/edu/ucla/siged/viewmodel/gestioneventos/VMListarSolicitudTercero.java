package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMListarSolicitudTercero{
	
	@WireVariable
	private ServicioDonacion servicioDonacion;
	
	private List<Donacion> donaciones = new ArrayList<Donacion>();
	
	private Window window;
	
	private int paginaActual;
	private int tamanoPagina;
	private long registrosTotales; 
	private short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	
	// las variables siguientes son del filtro
	private String rif=""; 
	private String nombre="";
	private Date fecha;
	private Integer comboEstatus=0;
	
	private boolean estatusFiltro;

	public VMListarSolicitudTercero() {
		
	}

	public List<Donacion> getDonaciones() {
		return donaciones;
	}

	public void setDonaciones(List<Donacion> donaciones) {
		this.donaciones = donaciones;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina = servicioDonacion.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public String getRif() {
		return rif;
	}

	public void setRif(String rif) {
		this.rif = rif;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getComboEstatus() {
		return comboEstatus;
	}

	public void setComboEstatus(Integer comboEstatus) {
		this.comboEstatus = comboEstatus;
	}

	@Init
	public void init() {
		String sql = "estatus in (0) order by fechasolicitud desc";
		String sql2 = "estatus in (0)";
		this.donaciones = servicioDonacion.buscarFiltrado(sql, 0);
		this.registrosTotales= servicioDonacion.totalDonacionesFiltrados(sql2);
	}
	
	@Command
	public void EditarDonacion(@BindingParam("donacionSeleccionada") Donacion donacionSeleccionada) {
		this.tipoOperacion=2;
		Map<String,Donacion> mapDonacion = new HashMap<String, Donacion>();
		mapDonacion.put("donacion", donacionSeleccionada);
		window = (Window) Executions.createComponents("/vistas/gestionevento/SolicitudTercero.zul", null, mapDonacion);
		window.doModal();
	}
	
	@GlobalCommand
	@NotifyChange({"donaciones","registrosTotales","paginaActual"})
	public void PaginarDonacion(){
		String sql = "estatus in (0)";
		if (estatusFiltro==false){
			this.donaciones = servicioDonacion.buscarFiltrado(sql, paginaActual);
			this.registrosTotales=servicioDonacion.totalDonacionesFiltrados(sql);
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@GlobalCommand
	@NotifyChange({"donaciones","registrosTotales","paginaActual"})
	public void ActualizarListaSolicitudes(){
		String sql = "estatus in (0)";
		this.registrosTotales = servicioDonacion.totalDonacionesFiltrados(sql);
		if(this.registrosTotales > 0){
			if (this.tipoOperacion==1){
			   int ultimaPagina=0;
			   if (this.registrosTotales%this.tamanoPagina==0)
			      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
			   else
			      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
			   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
			} else if(this.tipoOperacion==2){
			
			}
			PaginarDonacion();
		}else{
			String sql3 = "estatus in (0) order by fechasolicitud desc";
			String sql2 = "estatus in (0)";
			this.donaciones = servicioDonacion.buscarFiltrado(sql3, 0);
			this.registrosTotales= servicioDonacion.totalDonacionesFiltrados(sql2);
		}
	}
	
	@Command
	@NotifyChange({"donaciones","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String estatus = "(0)";
		this.estatusFiltro=true;  //el filtro se ha activado
		
		if (fecha == null)
			jpql = " rif like '%"+rif+"%' and nombre like '%"+nombre+"%' and estatus in "+estatus;
		else
		    jpql = " rif like '%"+rif+"%' and nombre like '%"+nombre+"%' and estatus in "+estatus+" and fechasolicitud = '"+fecha+"'";
			
		this.donaciones=servicioDonacion.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioDonacion.totalDonacionesFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"donaciones","registrosTotales","paginaActual","rif", "nombre","fecha"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		PaginarDonacion();
		rif="";
		nombre="";
		fecha=null;
		comboEstatus=0;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}
	
	@Command
	public String VerEstatusDonacion(Integer aux){
		String est="Solicitada";
		switch (aux){
			case 1:
				est="Aprobada";
			    break;
			case 2:
				est="Rechazada";
			    break;
			case 3:
				est="Entregada";
			    break;
			case 4:
				est="Recibida";
			    break;
			default:
				est="Solicitada";
		}
		return est;
	}

}
