package edu.ucla.siged.viewmodel.seguridad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;



import edu.ucla.siged.domain.seguridad.Funcionalidad;
import edu.ucla.siged.servicio.impl.seguridad.ServicioFuncionalidad;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioFuncionalidadI;

public class VMFuncionalidad {
	@WireVariable
	ServicioFuncionalidadI servicioFuncionalidad; // Instancia la interface servicio	
	@WireVariable
	Funcionalidad funcionalidad;
	private ArrayList<Integer> listaEstatus;
	private Short estatusSeleccionado;
	private boolean activarEstatus;
	
	
	
	public VMFuncionalidad() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            		 @ExecutionArgParam("funcionalidadSeleccionada") Funcionalidad funcionalidadSeleccionada) {
		 listaEstatus= new ArrayList<Integer>();
         listaEstatus.add(0);
         listaEstatus.add(1);
         Selectors.wireComponents(view, this, false);
		System.out.println("------------------------------------ENTROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
        if (funcionalidadSeleccionada!=null){
        	activarEstatus=false;
        	System.out.println(funcionalidadSeleccionada.getNombre());
            this.funcionalidad = funcionalidadSeleccionada;
            this.estatusSeleccionado= this.funcionalidad.getEstatus();
        }else{
        	activarEstatus=true;
        	System.out.println("------------------------------------Esta vaciiiiiiiiiiiiiiiiiiiiaaaaaaaaaaaa");
        	this.funcionalidad = new Funcionalidad();
        	//this.funcionalidad.setEstatus(Short.parseShort("1"));
        	
        }
    }
	
	@Command
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
	
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Integer> getListaEstatus() {
		return listaEstatus;
	}
	
	
	public Funcionalidad getFuncionalidad() {
		return funcionalidad;
	}

	public void setFuncionalidad(Funcionalidad funcionalidad) {
		this.funcionalidad = funcionalidad;
	}
	
	
	@Command
	@NotifyChange({"funcionalidad"})
	public void guardarFuncionalidad(@SelectorParam(".funcionalidadRestriccion") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		if(!camposValidos){
			Messagebox.show("Por Favor Introduzca Campos Válidos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		if (funcionalidad.getNombre().equals("") || funcionalidad.getDescripcion().equals("") || funcionalidad.getRuta().equals("")
			|| funcionalidad.getVinculo().equals("") || !camposValidos)
			Messagebox.show("No pueden haber campos vacios. Por Favor Introduzca Campos Válidos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		
		else{
			if (this.estatusSeleccionado!=null)
			    this.funcionalidad.setEstatus(estatusSeleccionado);
			
			Funcionalidad encontrado=servicioFuncionalidad.buscarByNombre(this.funcionalidad.getNombre());
			if(encontrado==null || this.funcionalidad.getId()==encontrado.getId()){
			try{
				servicioFuncionalidad.guardar(this.funcionalidad);
				System.out.println("----------Funcionalidad:----------- " + this.funcionalidad.getNombre());
			}
			catch(Exception e){
			    System.out.println(e.getMessage());
			}
			Messagebox.show("Datos almacenados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
			limpiarFuncionalidad();
			Window myWindow = (Window) Path.getComponent("/vistaFuncionalidad");
			myWindow.detach();
			}else
			{	Messagebox.show("Ya existe una funcionalidad con ese nombre", "Información", Messagebox.OK, Messagebox.INFORMATION);}
			
			}
		
	}
		
	@Command
	@NotifyChange({"funcionalidad"})
		public void limpiarFuncionalidad(){
			this.funcionalidad= new Funcionalidad();
			
			/*funcionalidad.setNombre("");
			funcionalidad.setDescripcion("");
			funcionalidad.setRuta("");
			funcionalidad.setVinculo("");
			funcionalidad.setIdPadre((short) 0);
			funcionalidad.setEstatus((short) 1);*/
			//anotador.setfechaRegistro(null);
			
		}

	public boolean isActivarEstatus() {
		return activarEstatus;
	}

	public void setActivarEstatus(boolean activarEstatus) {
		this.activarEstatus = activarEstatus;
	}
	
}
