/** 
 * 	VMListaHorario
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.util.HashMap;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.HorarioArea;

public class VMListaHorario {
	
	Window window;
	short tipoOperacion;
	private @WireVariable HorarioArea horarioArea;
	
	@Command
	public void agregarHorario() { 
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/horarioarea.zul", null, null);
		window.doModal();
	}

	
	@GlobalCommand
	@NotifyChange({"horarioarea","panelarea"})
	public void editarHorario(@BindingParam("horarioareaSeleccionado") HorarioArea horarioareaSeleccionado){
		this.horarioArea = horarioareaSeleccionado;
		this.tipoOperacion=2;
		Map<String,HorarioArea> mapa = new HashMap<String,HorarioArea>();
		mapa.put("horarioarea",horarioareaSeleccionado);
		window = (Window)Executions.createComponents("vistas/gestionrecursotecnico/horarioarea.zul", null, null);
		window.doModal();
	}
}
