/** 
 * 	VMListaRecurso
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioRecursoI;

//Declaraci�n de la la clase VMListaTipoCompetencia con sus atributos:
public class VMListaRecurso {
	
	@WireVariable
	ServicioRecursoI servicioRecurso;
	@WireVariable
	Recurso recurso;
	List<Recurso> listaRecursos;
	List<Recurso> recursosSeleccionados;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion; //1: El formulario se llama para incluir, 2: Para editar, 3: Si se elimina un solo registro, 4: Si se eliminan varios.
	// Las variables siguientes son del filtro:
	Integer id=0;
	String nombreRecurso="";
	String descripcion="";
	Integer estatusCombo=0;
	boolean estatusFiltro;
	
	//M�todos Get y Set de la clase:
	public Recurso getRecurso() {
		return recurso;
	}
	
	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}
	
	public List<Recurso> getRecursosSeleccionados() {
		return recursosSeleccionados;
	}
	
	public void setRecursosSeleccionados(List<Recurso> recursosSeleccionados) {
		this.recursosSeleccionados = recursosSeleccionados;
	}
	
	public List<Recurso> getListaRecursos() {
		
		return listaRecursos;
	}
	
	public void setListaRecursos(List<Recurso> listaRecursos) {
		this.listaRecursos = listaRecursos;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioRecurso.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombreRecurso() {
		return nombreRecurso;
	}
	
	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Integer getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Integer estatus) {
		this.estatusCombo = estatus;
	}
	
	//M�todo para inicializar la lista con los registros totales:
	@Init
	public void init(){
		this.listaRecursos = servicioRecurso.buscarTodos(0).getContent();
		this.registrosTotales= servicioRecurso.totalRecursos();
	}
	
	//M�todo que llama al formulario para agregar un nuevo registro:
	@Command
	public void agregarRecurso() { 
		this.tipoOperacion=1; //Como el formulario se llama para incluir, tipoOperacion=1.
		window = (Window)Executions.createComponents("vistas/gestionevento/recurso.zul", null, null);
		window.doModal();
	}
	
	//M�todo para eliminar un registro de la lista:
	@Command
	@NotifyChange({"listaRecursos","registrosTotales","paginaActual"})
	public void eliminarRecurso(@BindingParam("recursoSeleccionado") final Recurso recursoSeleccionado){
		this.tipoOperacion=3; //Como se eliminar� un registro, tipoOperacion=3.
		Messagebox.show("�Est� seguro que desea eliminar este registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
				new EventListener<Event>() {
			
					public void onEvent(Event event) throws Exception {
                    
						if(Messagebox.ON_YES.equals(event.getName())){
							
							servicioRecurso.eliminar(recursoSeleccionado);
							BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
							Messagebox.show("El registro se ha eliminado exitosamente.","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
							
						}
					}
				});
		
}
	
	//M�todo para eliminar varios registros de la lista:
	@Command
	@NotifyChange("listaRecursos")
	public void eliminarRecursos(@BindingParam("lista") Listbox lista){
		this.tipoOperacion=4; //Como se eliminar�n varios registros, tipoOperacion=4.
		if(lista.getSelectedItems().size()>0)
		{
			Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
                        
							if(Messagebox.ON_YES.equals(event.getName())){
								
								servicioRecurso.eliminarVarios(recursosSeleccionados);
								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
								Messagebox.show("Los registros seleccionados se han eliminados exitosamente","Informaci�n",Messagebox.OK, Messagebox.INFORMATION);
								recursosSeleccionados=null;
								
							}
						}
					});
		
		}
		else Messagebox.show("Debe seleccionar los registros que desea eliminar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	//M�todo para modificar un registro:
	@Command
	@NotifyChange({"recurso"})
	public void editarRecurso(@BindingParam("recursoSeleccionado") Recurso recursoSeleccionado ) {
		
		this.recurso=recursoSeleccionado;
		this.tipoOperacion=2; //Como el formulario se llama para editar, tipoOperacion=2.
		
		Map<String, Recurso> mapa = new HashMap<String, Recurso>();
        mapa.put("recurso", recursoSeleccionado);
		window = (Window)Executions.createComponents("vistas/gestionevento/recurso.zul", null, mapa);
		window.doModal();
	}
	
	//M�todo para paginar los registros en la lista:
	@GlobalCommand
	@NotifyChange({"listaRecursos","registrosTotales","paginaActual","recursosSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaRecursos = servicioRecurso.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioRecurso.totalRecursos();
		
		}else {
			    ejecutarFiltro();
		      }

	}
	
	//M�todo para actualizar la lista:
	@GlobalCommand
	@NotifyChange({"listaRecursos","registrosTotales","paginaActual"})
    public void actualizarLista(){
		int ultimaPagina=0;
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   if ((this.registrosTotales+1)%this.tamanoPagina==0)
		      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
		   else
			  ultimaPagina =((int) (this.registrosTotales+1)/this.tamanoPagina);
		   this.paginaActual=ultimaPagina; // Se coloca en la �ltima p�gina para que quede visible el que se acaba de ingresar.
		} 
		 paginar();
		 
		 if(this.tipoOperacion==3 || this.tipoOperacion==4){
			   this.getRegistrosTotales();
			   
			   if ((this.registrosTotales+1)%this.tamanoPagina==0)
			      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			   else
				  ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina;
			 paginar();
			}
	}
	
	//M�todo para filtrar los registros:
	@Command
	@NotifyChange({"listaRecursos","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql="";
		String filtroEstatus;
		this.estatusFiltro=true;
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		if (id != null)
			
			jpql = " nombreRecurso like '%"+nombreRecurso+"%' and descripcion like '%"+descripcion+"%' and estatus in "+filtroEstatus+"";
		
		this.listaRecursos=servicioRecurso.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioRecurso.totalRecursosFiltrados(jpql);
	}
	
	//M�todo para asignarle 0 a paginaActual e invocar al m�todo ejecutarFiltro():
	@Command
	@NotifyChange({"listaRecursos","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	//M�todo para cancelar el filtro y mostrar todos los registros:
	@Command
	@NotifyChange({"listaRecursos","registrosTotales","paginaActual","recursosSeleccionados",
				   "nombreRecurso", "descripcion","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		id=0;
		nombreRecurso="";
		descripcion="";
		estatusCombo=0;
	}

}