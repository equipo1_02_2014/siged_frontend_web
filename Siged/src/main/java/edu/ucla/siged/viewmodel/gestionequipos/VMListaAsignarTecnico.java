package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoTecnicoI;

public class VMListaAsignarTecnico {

	Window window;
	@WireVariable
	ServicioEquipoI servicioEquipo;
	@WireVariable
	ServicioEquipoTecnicoI servicioEquipoTecnico;
	@WireVariable
	Equipo equipo;
	private List<Equipo> equipoSeleccionado;
	private List<Equipo> listaEquipos;
	private List<EquipoTecnico> listaEquipoTecnicos;
	@WireVariable
	Tecnico tecnicoPrincipal;
	@WireVariable
	Tecnico tecnicoAsistente;
	short tipoOperacion; // 1: el formulario se llamo para incluir, 2:para
							// editar.

	// ---------- variables para filtrar ----------
	private String nombreEquipoFiltro = "";
	private String tecnicoPrincipalFiltro = "";
	private String tecnicoAsistenteFiltro = "";
	private String tipoEquipoFiltro = "";

	// ---------- variables la paginaci�n ----------
	int paginaActual;
	int tamanioPagina;
	long registrosTotales;

	// ---------------------------------------------------------------------------------------------------------
	// Inicio de M�todos Getters and Setters
	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public List<Equipo> getEquipoSeleccionado() {
		return equipoSeleccionado;
	}

	public void setEquipoSeleccionado(List<Equipo> equipoSeleccionado) {
		this.equipoSeleccionado = equipoSeleccionado;
	}

	public String getNombreEquipoFiltro() {
		return nombreEquipoFiltro;
	}

	public void setNombreEquipoFiltro(String nombreEquipoFiltro) {
		this.nombreEquipoFiltro = nombreEquipoFiltro;
	}

	public String getTipoEquipoFiltro() {
		return tipoEquipoFiltro;
	}

	public void setTipoEquipoFiltro(String tipoEquipoFiltro) {
		this.tipoEquipoFiltro = tipoEquipoFiltro;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanioPagina() {
		this.tamanioPagina = servicioEquipo.TAMANIO_PAGINA;
		return tamanioPagina;
	}

	public void setTamanioPagina(int tamanioPagina) {
		this.tamanioPagina = tamanioPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public String getTecnicoPrincipalFiltro() {
		return tecnicoPrincipalFiltro;
	}

	public void setTecnicoPrincipalFiltro(String tecnicoPrincipalFiltro) {
		this.tecnicoPrincipalFiltro = tecnicoPrincipalFiltro;
	}

	public String getTecnicoAsistenteFiltro() {
		return tecnicoAsistenteFiltro;
	}

	public void setTecnicoAsistenteFiltro(String tecnicoAsistenteFiltro) {
		this.tecnicoAsistenteFiltro = tecnicoAsistenteFiltro;
	}

	public List<EquipoTecnico> getListaEquipoTecnicos() {
		return listaEquipoTecnicos;
	}

	public void setListaEquipoTecnicos(List<EquipoTecnico> listaEquipoTecnicos) {
		this.listaEquipoTecnicos = listaEquipoTecnicos;
	}

	public Tecnico getTecnicoPrincipal() {
		return tecnicoPrincipal;
	}

	public void setTecnicoPrincipal(Tecnico tecnicoPrincipal) {
		this.tecnicoPrincipal = tecnicoPrincipal;
	}

	public Tecnico getTecnicoAsistente() {
		return tecnicoAsistente;
	}

	public void setTecnicoAsistente(Tecnico tecnicoAsistente) {
		this.tecnicoAsistente = tecnicoAsistente;
	}

	public List<Equipo> getListaEquipos() {
		return listaEquipos;
	}

	public void setListaEquipos(List<Equipo> listaEquipos) {
		this.listaEquipos = listaEquipos;
	}

	// ---------------------------------------------------------------------------------------------------------
	// Fin de M�todos Getters and Setters

	@Init
	public void inicializar() {
		buscarEquipos();
	}

	@Command
	@NotifyChange({ "listaEquipos" })
	public void buscarEquipos() {
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();

	}

	@GlobalCommand("paginar")
	@NotifyChange({ "listaEquipos", "registrosTotales", "paginaActual","tamanioPagina" })
	public void paginar() {
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(this.paginaActual).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
	}

	@Command
	@NotifyChange({ "listaEquipos", "registrosTotales", "paginaActual" })
	public void filtrar() {
		this.paginaActual = 0;
		ejecutarFiltro();
	}

	@Command
	@NotifyChange({ "listaEquipos", "registrosTotales","nombreEquipoFiltro","tecnicoPrincipalFiltro","tecnicoAsistenteFiltro",
		"tipoEquipoFiltro","paginaActual"})
	public void ejecutarFiltro() {
		if ((!nombreEquipoFiltro.equals("")) || (!tecnicoPrincipalFiltro.equals(""))
				|| (!tecnicoAsistenteFiltro.equals("")) || (!tipoEquipoFiltro.equals(""))) {

			String tablas = "FROM Equipo e";
			String condiciones = " WHERE ";
			String jpql = "";
			
			if (!this.nombreEquipoFiltro.isEmpty()) {
				condiciones += " e.nombre LIKE '%"+nombreEquipoFiltro+"%' AND"
							+  " e.estatus = 1 AND";
			}
			if (!this.tipoEquipoFiltro.isEmpty()) {
				tablas += ", TipoEquipo te";
				condiciones += " e.tipoEquipo.id = te.id AND"
						    +  " te.descripcion LIKE '%"+tipoEquipoFiltro+"%' AND";
			}
			if (!this.tecnicoPrincipalFiltro.isEmpty() || !this.tecnicoAsistenteFiltro.isEmpty()) {
				tablas += ", Tecnico t, EquipoTecnico et";
				
				if(!this.tecnicoPrincipalFiltro.isEmpty() ){
					condiciones += " t.estatus = 1 AND e.estatus=1 AND et.estatus=1 AND "				
						        +  " e.id = et.equipo.id AND t.id=et.tecnico.id AND"
						        +  " t.nombre LIKE '%"+tecnicoPrincipalFiltro+"%' OR t.apellido LIKE '%"+tecnicoPrincipalFiltro+"%' AND";
				}
				if(!this.tecnicoAsistenteFiltro.isEmpty()){
					condiciones += " t.estatus = 1 AND e.estatus=1 AND et.estatus=1 AND "
						        +  " e.id = et.equipo.id AND t.id=et.tecnico.id AND"
						        +  " t.nombre LIKE '%"+tecnicoAsistenteFiltro+"%' OR t.apellido LIKE '%"+tecnicoAsistenteFiltro+"%' AND";
				}
			}
			condiciones = condiciones.substring(0, condiciones.length()-3);
			jpql += tablas + condiciones;
			
		this.listaEquipos = servicioEquipo.buscarFiltradoPorTecnico(jpql,this.paginaActual);
		this.registrosTotales = servicioEquipo.totalEquiposFiltradosPorTecnico(jpql);	
		}
	}

	@Command
	@NotifyChange({ "listaEquipos", "registrosTotales", "paginaActual",
			"tamanioPagina", "nombreEquipoFiltro", "tecnicoPrincipalFiltro","tecnicoAsistenteFiltro", "tipoEquipoFiltro" })
	public void cancelarFiltro() {
		paginar();
		nombreEquipoFiltro = "";
		tipoEquipoFiltro = "";
		tecnicoPrincipalFiltro = "";
		tecnicoAsistenteFiltro = "";
	}

	@GlobalCommand
	@NotifyChange({ "listaEquipos", "registrosTotales", "paginaActual",
			"tamanioPagina" })
	public void listaEquipoTecnicos() {
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
	}

	@Command
	@NotifyChange({ "listaEquipos" })
	public void asignarTecnicos(
			@BindingParam("equipoSeleccionado") Equipo equipoSeleccionado) {
		try {
			this.equipo = equipoSeleccionado;
			Map<String, Equipo> mapEquipo = new HashMap<String, Equipo>();
			mapEquipo.put("equipo", equipoSeleccionado);
			window = (Window) Executions.createComponents(
					"/vistas/gestionequipo/asignartecnicoequipo.zul", null,
					mapEquipo);
			window.doModal();
		} catch (Exception e) {
			Messagebox.show(e.toString());
		}
	}

	@Command
	@NotifyChange({ "listaEquipoTecnicos" })
	public String obtenerTecnicoPrincipal(Integer id) {
		String sinTecnico = "No asignado";
		tecnicoPrincipal = servicioEquipoTecnico.obtenerTecnicoPrincipal(id);
		if (tecnicoPrincipal != null) {
			return (tecnicoPrincipal.getNombre() + " " + tecnicoPrincipal
					.getApellido());
		} else {
			return sinTecnico;
		}
	}

	@Command
	@NotifyChange({ "listaEquipoTecnicos" })
	public String obtenerTecnicoAsistente(Integer id) {
		String sinTecnico = "No asignado";
		tecnicoAsistente = servicioEquipoTecnico.obtenerTecnicoAsistente(id);
		if (tecnicoAsistente != null) {
			return (tecnicoAsistente.getNombre() + " " + tecnicoAsistente
					.getApellido());
		} else {
			return sinTecnico;
		}

	}

	@GlobalCommand("actualizarLista")
	@NotifyChange({ "listaEquipos", "registrosTotales" })
	public void actualizarLista() {
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
	}

}
