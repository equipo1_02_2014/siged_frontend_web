package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.DelegadoEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioDelegadoEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;

public class VMListaAsignarDelegado {
	
	Window window;
	@WireVariable ServicioEquipoI servicioEquipo;
	@WireVariable ServicioRepresentanteI servicioDelegado;
	@WireVariable ServicioDelegadoEquipoI servicioDelegadoEquipo;
	@WireVariable Equipo equipo;
	@WireVariable Representante delegado;
	@WireVariable Representante delegadoPrincipal;
	@WireVariable Representante delegadoSuplente;
	private List<Equipo> equipoSeleccionado;
	private List<Equipo> listaEquipos;
	private List<DelegadoEquipo> listaDelegadoEquipo;
	
	//---------- variables para filtrar ----------
	private String nombreEquipoFiltro="";
	private String tipoEquipoFiltro="";
	private String delegadoPrincipalFiltro="";
	private String delegadoSuplenteFiltro="";

	//---------- variables la paginación ----------
	int paginaActual;
	int tamanioPagina;
	long registrosTotales;
	
	
	
	
		
	public String getDelegadoPrincipalFiltro() { return delegadoPrincipalFiltro; }

	public void setDelegadoPrincipalFiltro(String delegadoPrincipalFiltro) { this.delegadoPrincipalFiltro = delegadoPrincipalFiltro; }

	public String getDelegadoSuplenteFiltro() { return delegadoSuplenteFiltro; }

	public void setDelegadoSuplenteFiltro(String delegadoSuplenteFiltro) { this.delegadoSuplenteFiltro = delegadoSuplenteFiltro; }

	public Representante getDelegadoPrincipal() { return delegadoPrincipal; }

	public void setDelegadoPrincipal(Representante delegadoPrincipal) { this.delegadoPrincipal = delegadoPrincipal; }

	public Representante getDelegadoSuplente() { return delegadoSuplente; }

	public void setDelegadoSuplente(Representante delegadoSuplente) { this.delegadoSuplente = delegadoSuplente; }

	public Equipo getEquipo() { return equipo; }
	
	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public Representante getDelegado() { return delegado; }
	
	public void setDelegado(Representante delegado) { this.delegado = delegado; }
	
	public List<Equipo> getEquipoSeleccionado() { return equipoSeleccionado; }
	
	public void setEquipoSeleccionado(List<Equipo> equipoSeleccionado) { this.equipoSeleccionado = equipoSeleccionado; }
	
	public List<DelegadoEquipo> getListaDelegadoEquipo() { return listaDelegadoEquipo; }
	
	public void setListaDelegadoEquipo(List<DelegadoEquipo> listaDelegadoEquipo) { this.listaDelegadoEquipo = listaDelegadoEquipo; }
	
	public String getNombreEquipoFiltro() { return nombreEquipoFiltro; }
	
	public void setNombreEquipoFiltro(String nombreEquipoFiltro) { this.nombreEquipoFiltro = nombreEquipoFiltro; }
	
	public String getTipoEquipoFiltro() { return tipoEquipoFiltro; }
	
	public void setTipoEquipoFiltro(String tipoEquipoFiltro) { this.tipoEquipoFiltro = tipoEquipoFiltro; }

	public int getPaginaActual() { return paginaActual; }
	
	public void setPaginaActual(int paginaActual) { this.paginaActual = paginaActual; }
	
	public int getTamanioPagina() { 
		this.tamanioPagina = servicioEquipo.TAMANIO_PAGINA;
		return tamanioPagina; 
	}
	
	public void setTamanioPagina(int tamanioPagina) { this.tamanioPagina = tamanioPagina; }
	
	public long getRegistrosTotales() { return registrosTotales; }
	
	public void setRegistrosTotales(long registrosTotales) { this.registrosTotales = registrosTotales; }
	
	public List<Equipo> getListaEquipos() { return listaEquipos; }

	public void setListaEquipos(List<Equipo> listaEquipos) { this.listaEquipos = listaEquipos; }	
	
	
	


	@Init
	public void inicializar(){
		buscarEquipos();
	}
	
	@Command
	@NotifyChange({"listaEquipos"})
	public void buscarEquipos(){
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
		
	}
	
	@GlobalCommand("paginar")
	@NotifyChange({"listaEquipos", "registrosTotales", "paginaActual", "tamanioPagina"})
	public void paginar(){
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(this.paginaActual).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
	}
	
	@Command
	@NotifyChange({ "listaEquipos", "registrosTotales", "paginaActual","tamanioPagina" })
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
    @Command
    @NotifyChange({"listaEquipos", "registrosTotales","nombreEquipoFiltro","delegadoPrincipalFiltro","delegadoSuplenteFiltro",
    	"tipoEquipoFiltro","paginaActual"})
	public void ejecutarFiltro(){
		if((!nombreEquipoFiltro.equals("")) || (!delegadoPrincipalFiltro.equals("")) || (!delegadoSuplenteFiltro.equals("")) || (!tipoEquipoFiltro.equals(""))){
			
			String tablas = "FROM Equipo e";
			String condiciones = " WHERE ";
			String jpql = "";
			
			if(!nombreEquipoFiltro.isEmpty()){
				condiciones += " e.nombre LIKE '%"+nombreEquipoFiltro+"%' AND"
						    +  " e.estatus = 1 AND";
			}
			if (!this.tipoEquipoFiltro.isEmpty()) {
					tablas += ", TipoEquipo te";
					condiciones += " e.tipoEquipo.id = te.id AND"
							    +  " te.descripcion LIKE '%"+tipoEquipoFiltro+"%' AND";
			}
			if (!this.delegadoPrincipalFiltro.isEmpty() || !this.delegadoSuplenteFiltro.isEmpty()) {
					tablas += ", Representante r, DelegadoEquipo de";
					condiciones += " r.estatus = 1 AND e.estatus=1 AND de.estatus=1 AND "				
					            +  " e.id = de.equipo.id AND r.id=de.representante.id AND";
					if(!this.delegadoPrincipalFiltro.isEmpty() ){
						condiciones += " r.nombre LIKE '%"+delegadoPrincipalFiltro+"%' OR r.apellido LIKE '%"+delegadoPrincipalFiltro+"%' AND";
					}
					if(!this.delegadoSuplenteFiltro.isEmpty()){
						condiciones += " r.nombre LIKE '%"+delegadoSuplenteFiltro+"%' OR r.apellido LIKE '%"+delegadoSuplenteFiltro+"%' AND";
					}
			}
			condiciones = condiciones.substring(0, condiciones.length()-3);
			jpql += tablas + condiciones;

			this.listaEquipos = servicioEquipo.buscarFiltradoPorDelegado(jpql, this.paginaActual);
			this.registrosTotales = servicioEquipo.totalEquiposFiltradosPorDelegado(jpql);
		}		
		

	}
    
	@Command
	@NotifyChange({"listaEquipos", "registrosTotales", "paginaActual",
		"tamanioPagina", "nombreEquipoFiltro","delegadoPrincipalFiltro","delegadoSuplenteFiltro", "tipoEquipoFiltro"})
	public void cancelarFiltro(){
		paginar();
		nombreEquipoFiltro="";
		tipoEquipoFiltro="";
		delegadoPrincipalFiltro="";
		delegadoSuplenteFiltro="";
	}
	
	@Command
	@NotifyChange({"listaDelegadoEquipo"})
	public String obtenerDelegadoPrincipal(Integer id){
		String  sinDelegado="No asignado";
		delegadoPrincipal = servicioDelegadoEquipo.obtenerDelegadoPrincipal(id);
		if(delegadoPrincipal != null){
			return (delegadoPrincipal.getNombre() + " " + delegadoPrincipal.getApellido());
		}else{
			return sinDelegado;
		}	
	}
	
	@Command
	@NotifyChange({"listaDelegadoEquipo"})
	public String obtenerDelegadoSuplente(Integer id){
		String  sinDelegado="No asignado";
		delegadoSuplente = servicioDelegadoEquipo.obtenerDelegadoSuplente(id);
		if(delegadoSuplente != null){
			return (delegadoSuplente.getNombre() + " " + delegadoSuplente.getApellido());
		}else{
			return sinDelegado;
		}	
	}
	
	@GlobalCommand("actualizarLista")
	@NotifyChange({"listaEquipos", "registrosTotales"})
	public void actualizarLista(){
		this.listaEquipos = servicioEquipo.buscarEquiposTrue(0).getContent();
		this.registrosTotales = servicioEquipo.totalEquipos();
	}
	
	@Command
	@NotifyChange({"listaEquipos"})
	public void asignarDelegados(@BindingParam("equipoSeleccionado") Equipo equipoSeleccionado){
		try{
			this.equipo = equipoSeleccionado;
			Map<String, Equipo> mapEquipo = new HashMap<String, Equipo>();
			mapEquipo.put( "equipo", equipoSeleccionado );		
			window = (Window)Executions.createComponents("/vistas/gestionequipo/asignardelegadoequipo.zul", null, mapEquipo);
			window.doModal();
		}catch(Exception e){
			Messagebox.show(e.toString());
		}
	}
}
