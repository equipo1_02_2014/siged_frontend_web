package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hamcrest.core.Is;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Spinner;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.servicio.impl.gestionequipos.ServicioAtletaEquipo;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioTipoEquipoI;

public class VMRegistrarEquipo {
	
	@WireVariable ServicioEquipoI	 	servicioEquipo;
	@WireVariable ServicioCategoriaI 	servicioCategoria;
	@WireVariable ServicioTipoEquipoI 	servicioTipoEquipo;
	@WireVariable ServicioRangoI		servicioRango;
	@WireVariable ServicioAtletaI 		servicioAtleta;
	@WireVariable ServicioAtletaEquipo	servicioAtletaEquipo;

	@WireVariable Categoria categoriaSeleccionada;
	@WireVariable TipoEquipo tipoEquipoSeleccionado;
	@WireVariable Rango rangoSeleccionado;
	private int cuposTotales;
	private Date fechaCreacion;
	private Date fechaCierre;
	
	Equipo	equipo;
	
	List<Categoria> 	listaCategoria;
	List<TipoEquipo> 	listaTipoEquipo;
	List<Rango> 		listaRango;

    int tipoOperacion; //1:incluir  2:editar
	private String nombreEquipo;
	Window window;
	String confirmarRegistro;
	boolean registroNuevo;
	

	// --------------------------------------------------------------------------------------------------------- Inicio de M�todos Getters and Setters	
	public TipoEquipo getTipoEquipoSeleccionado() { return tipoEquipoSeleccionado; }
	
	public void setTipoEquipoSeleccionado(TipoEquipo tipoEquipoSeleccionado) { this.tipoEquipoSeleccionado = tipoEquipoSeleccionado; }
	
	public Rango getRangoSeleccionado() { return rangoSeleccionado; }
	
	public void setRangoSeleccionado(Rango rangoSeleccionado) { this.rangoSeleccionado = rangoSeleccionado; }
	
	public Equipo getEquipo() { return equipo; }
	
	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public Categoria getCategoriaSeleccionada() { return categoriaSeleccionada; }
	
	public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) { this.categoriaSeleccionada = categoriaSeleccionada; }
	
	public List<Categoria> getListaCategoria() { return listaCategoria; }
	
	public void setListaCategoria(List<Categoria> listaCategoria) { this.listaCategoria = listaCategoria; }
	
	public List<TipoEquipo> getListaTipoEquipo() { return listaTipoEquipo; }
	
	public void setListaTipoEquipo(List<TipoEquipo> listaTipoEquipo) { this.listaTipoEquipo = listaTipoEquipo; }
	
	public List<Rango> getListaRango() { return listaRango; }
	
	public void setListaRango(List<Rango> listaRango) { this.listaRango = listaRango; }
	
	public String getNombreEquipo(){ return nombreEquipo; }
	
	public void setNombreEquipo(String nombreEquipo){ this.nombreEquipo = nombreEquipo;}
	
	public Date getFechaCreacion() { return fechaCreacion; }

	public void setFechaCreacion(Date fechaCreacion) { this.fechaCreacion = fechaCreacion; }

	public int getCuposTotales() { return cuposTotales; }
	
	public void setCuposTotales(int cuposTotales) { this.cuposTotales = cuposTotales; }

	public Date getFechaCierre() { return fechaCierre; }

	public void setFechaCierre(Date fechaCierre) { this.fechaCierre = fechaCierre; }

	public int getTipoOperacion() { return tipoOperacion; }

	public void setTipoOperacion(int tipoOperacion) { this.tipoOperacion = tipoOperacion; }

	public String getConfirmarRegistro() { return confirmarRegistro; }

	public void setConfirmarRegistro(String confirmarRegistro) { this.confirmarRegistro = confirmarRegistro; }

	public boolean isRegistroNuevo() { return registroNuevo; }

	public void setRegistroNuevo(boolean registroNuevo) { this.registroNuevo = registroNuevo; }

	// --------------------------------------------------------------------------------------------------------- Fin de M�todos Getters and Setters
	

	@Init
	public void inicializar(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("equipo") Equipo equipoSeleccionado)
	{	
		if (equipoSeleccionado != null){
			this.equipo = equipoSeleccionado;
			this.tipoEquipoSeleccionado = equipo.getTipoEquipo();
			this.categoriaSeleccionada = equipo.getCategoria();
			this.rangoSeleccionado = equipo.getRango();
			this.nombreEquipo = equipo.getNombre();
			this.fechaCreacion = equipo.getFechaCreacion();
			this.tipoOperacion=2; //editar
			
			this.window = null;
		}else{
			Date fecha = new Date();
			this.equipo = new Equipo();
			this.equipo.setFechaCreacion(fecha);
			this.tipoOperacion=1; //incluir
		}
		
		listaCategoria = servicioCategoria.buscarTodos();
		listaTipoEquipo = servicioTipoEquipo.buscarTodos();
		listaRango = servicioRango.buscarTodos();	
	}
	
	@Command
	@NotifyChange({"equipo","tipoOperacion", "equipo", "tipoEquipoSeleccionado", "categoriaSeleccionada", "rangoSeleccionado",
					"cuposTotales", "fechaCreacion", "fechaCierre","fecha"})
	public void guardarEquipo(@SelectorParam(".equipo_restriccion") LinkedList<InputElement> inputs,
							  @SelectorParam("#spnCuposTotales") Spinner spnCuposTotales,
							  @SelectorParam("#dtbFechaCreacion") Datebox dtbFechaCreacion,
							  @SelectorParam("#dtbFechaCierre") Datebox dtbFechaCierre,
							  @SelectorParam("#cmbTipoEquipo") Combobox cmbTipoEquipo,
							  @SelectorParam("#cmbCategoria") Combobox cmbCategoria,
							  @SelectorParam("#cmbRango") Combobox cmbRango,
							  @SelectorParam("#wndRegistrosBasicosRegistrarEquipo") Window win){
		
		this.window = win;
		
		boolean camposValidos = true;
		
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		
		if(cmbTipoEquipo.isValid()){ equipo.setTipoEquipo(tipoEquipoSeleccionado); }
		
		if(cmbCategoria.isValid()){ equipo.setCategoria(categoriaSeleccionada); }
		
		if(cmbRango.isValid()){ equipo.setRango(rangoSeleccionado);	}
			
		if(!camposValidos){
			Messagebox.show("Por Favor Introduzca Los Datos Faltante", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
		}else if( (Integer.parseInt(spnCuposTotales.getText()) <= 0)){
				Messagebox.show("Debe escribir un n�mero de cupos positivo", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
		}else{		
			
			equipo.setTipoEquipo(this.tipoEquipoSeleccionado);
			
			equipo.setCategoria(this.categoriaSeleccionada);
			
			equipo.setRango(this.rangoSeleccionado);
			
			cuposTotales = Integer.parseInt(spnCuposTotales.getText());
			equipo.setCuposTotales(cuposTotales);

			equipo.setEstatus(Short.parseShort("1"));
			
			equipo.setFechaCreacion(equipo.getFechaCreacion());
			
			fechaCierre = dtbFechaCierre.getValue();
			
			equipo.setFechaCierre(fechaCierre);
			
			equipo.setNombre(nombreEquipo);
			
			boolean resultado = servicioEquipo.buscarNombreEquipo(this.equipo);

			if( resultado == true ){
				Messagebox.show("El Equipo que intenta grabar, ya est� registrado", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			}else{
				try{
	
					if(tipoOperacion == 1){ 
						confirmarRegistro ="Desea Registrar el Equipo?"; 
						registroNuevo = true;
					}else{
						confirmarRegistro ="Desea Guardar los cambios en el Equipo?";
						registroNuevo = false;
					}
				
	        		Messagebox.show( confirmarRegistro,"Confirmacion",
	        				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
	        				         new org.zkoss.zk.ui.event.EventListener<Event>() {
	        		                     
	        			              public void onEvent(Event evt) throws InterruptedException {
	        			            	  
					        		        if (evt.getName().equalsIgnoreCase("onYes")) {
					        		        	
					        		        	servicioEquipo.guardarEquipo(equipo);
					        		        	
					        		        	Messagebox.show("Equipo: " +equipo.getNombre() + " Guardado con �xito!!!", 
					        	    	                       "Information", Messagebox.OK, 
					        	    	                        Messagebox.INFORMATION);
					        		           
					        		        	if(registroNuevo == true){
					        		        		Messagebox.show("Desea Registrar otro Equipo?","Confirmacion",
						          				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
						          				         	new org.zkoss.zk.ui.event.EventListener<Event>() {
															public void onEvent(
																	Event evt)
																	throws Exception {
																
																if (evt.getName().equalsIgnoreCase("onYes")) {
																	BindUtils.postGlobalCommand(null, null, "limpiarCampos", null);
																}else{
																	 BindUtils.postGlobalCommand(null, null, "paginar", null);
																	 window.onClose();
																}
															}
						        		           });
					        		           }else{
					        		        	   BindUtils.postGlobalCommand(null, null, "paginar", null);
					        		        	   window.onClose();
					        		           }
					        		           
					        		        }				        		        
	        			              }
	        		});
	        		
	        	}catch(Exception ex){
	        		Messagebox.show("Error al Registrar Equipo", 
	    	                "Error", Messagebox.OK, Messagebox.ERROR);
	        	}
			}
		}
	}
	
	@GlobalCommand("limpiarCampos")
	@NotifyChange({"equipo", "tipoEquipoSeleccionado", "categoriaSeleccionada", "rangoSeleccionado", "cuposTotales", "fechaCreacion", "fechaCierre","fecha",
		"tipoOperacion", "listaCategoria","listaTipoEquipo", "listaRango"})
	public void limpiarCampos(){
		cuposTotales = 0;
		fechaCreacion = null;
		fechaCierre = null;

		this.equipo = new Equipo();
		this.equipo.setFechaCreacion(new Date());
		this.tipoOperacion=1; //incluir

		this.listaCategoria = servicioCategoria.buscarTodos();
		this.listaTipoEquipo = servicioTipoEquipo.buscarTodos();
		this.listaRango = servicioRango.buscarTodos();	
	}	

	@Command
	@NotifyChange({"nombreEquipo"})
	public void concatenarNombreEquipo(@SelectorParam("#cmbCategoria") Combobox cmbCategoria,
										 @SelectorParam("#cmbRango") Combobox cmbRango,
										 @SelectorParam("#dtbFechaCreacion") Datebox dtbFechaCreacion,
										 @SelectorParam("#dtbFechaCierre") Datebox dtbFechaCierre){		
			boolean camposValidos = (cmbCategoria.isValid() && cmbRango.isValid() && dtbFechaCreacion.isValid() && dtbFechaCierre.isValid());
			if(camposValidos){
				this.nombreEquipo = "LUIS SOJO"+" "+cmbCategoria.getText()+" "+cmbRango.getText()+" "+dtbFechaCreacion.getText()+" "+dtbFechaCierre.getText();
			}else{
				this.nombreEquipo="";
			}
	}

	
	@Command
	public void cerrarVentana(){
		BindUtils.postGlobalCommand(null, null, "paginar", null);
	}
	
}
