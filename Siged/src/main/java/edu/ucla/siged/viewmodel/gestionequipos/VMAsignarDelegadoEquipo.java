package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;










import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestionatleta.Representante;
import edu.ucla.siged.domain.gestionequipo.DelegadoEquipo;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.EquipoTecnico;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.domain.utilidades.Persona;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioDelegadoEquipoI;

public class VMAsignarDelegadoEquipo {
	
	@WireVariable ServicioDelegadoEquipoI servicioDelegadoEquipo;
	@WireVariable ServicioRepresentanteI servicioRepresentante;
	@WireVariable private Equipo equipo;
	@WireVariable Representante delegado;
	@WireVariable Representante delegadoPrincipalSeleccionado;
	@WireVariable Representante delegadoSuplenteSeleccionado;
	@WireVariable Representante delegadoPrincipal;
	@WireVariable Representante delegadoSuplente;
	private List<Representante> listaDelegados;
	private List<Representante> listaDelegadosAux;	

	Window window;

	
	
	public List<Representante> getListaDelegadosAux() { return listaDelegadosAux; }

	public void setListaDelegadosAux(List<Representante> listaDelegadosAux) { this.listaDelegadosAux = listaDelegadosAux; }

	public Representante getDelegadoPrincipal() { return delegadoPrincipal; }

	public void setDelegadoPrincipal(Representante delegadoPrincipal) { this.delegadoPrincipal = delegadoPrincipal; }

	public Representante getDelegadoSuplente() { return delegadoSuplente; }

	public void setDelegadoSuplente(Representante delegadoSuplente) { this.delegadoSuplente = delegadoSuplente; }

	public Equipo getEquipo() { return equipo; }

	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public Representante getDelegado() { return delegado; }

	public void setDelegado(Representante delegado) { this.delegado = delegado; }

	public List<Representante> getListaDelegados() { return listaDelegados; }

	public void setListaDelegados(List<Representante> listaDelegados) { this.listaDelegados = listaDelegados; }
	
	public Representante getDelegadoPrincipalSeleccionado() { return delegadoPrincipalSeleccionado; }

	public void setDelegadoPrincipalSeleccionado( Representante delegadoPrincipalSeleccionado) { this.delegadoPrincipalSeleccionado = delegadoPrincipalSeleccionado; }

	public Representante getDelegadoSuplenteSeleccionado() { return delegadoSuplenteSeleccionado; }

	public void setDelegadoSuplenteSeleccionado( Representante delegadoSuplenteSeleccionado) { this.delegadoSuplenteSeleccionado = delegadoSuplenteSeleccionado; }

	
	
	
	@Init
	public void inicializar(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("equipo") Equipo equipoSeleccionado){
	
		if(equipoSeleccionado != null){
			this.equipo = equipoSeleccionado;
			
			delegadoPrincipal = servicioDelegadoEquipo.obtenerDelegadoPrincipal(equipo.getId());
			if(delegadoPrincipal != null){ this.delegadoPrincipalSeleccionado = delegadoPrincipal; }
			else{ this.delegadoPrincipalSeleccionado = null; }
			
			delegadoSuplente = servicioDelegadoEquipo.obtenerDelegadoSuplente(equipo.getId());
			if(delegadoSuplente != null){ this.delegadoSuplenteSeleccionado = delegadoSuplente; }
			else{ this.delegadoSuplenteSeleccionado = null; }
			
			listaDelegados = servicioDelegadoEquipo.buscarRepresentantesDisponibles();
			listaDelegadosAux = servicioDelegadoEquipo.buscarRepresentantesDisponibles();
			
			if(delegadoPrincipal != null){
				listaDelegados.remove(delegadoPrincipal);
			}
			if(delegadoSuplente != null){
				listaDelegados.remove(delegadoSuplente);
			}
			
			this.window = null;
					
		}else{
			Messagebox.show( "Debe Seleccionar un Equipo para asignar Delegados!","", Messagebox.OK, Messagebox.ERROR );
		}
	}
	
	@NotifyChange({""})
	public void inicializarListaDelegados(){
		listaDelegados = servicioDelegadoEquipo.buscarRepresentantesDisponibles();
		listaDelegadosAux = servicioDelegadoEquipo.buscarRepresentantesDisponibles();
	}

	@Command
	public String verResponsabilidad( Integer responsabilidad ){
		String res="Principal";
		switch (responsabilidad){
		case 1:
			res="Principal";
		    break;
		case 2:
			res="Suplente";
			break;		
		}		
		return res;
	}
	
	   public String obtenerNombreCompleto(Persona persona){
	    	String nombreCompleto="";
	    	
	    	if (persona!=null){
	    		nombreCompleto= persona.getNombre() + " " + persona.getApellido(); 
	    	}

	    	return nombreCompleto;
	    }
	   
		@Command
		@NotifyChange({"delegadoSeleccionado","listaDelegados","equipo"})
		public void guardar(@SelectorParam(".delegadoequipo_restriccion") LinkedList<InputElement> inputs,
							@SelectorParam("#wndGestionDeportivaAsignarDelegadoEquipos") Window win,
							@SelectorParam("#cmbDelegadoPrincipalGestionDeportivaAsignarDelegado") Combobox cmbDelegadoPrincipal,
	    					@SelectorParam("#cmbDelegadoAsistenteGestionDeportivaAsignarDelegado") Combobox cmbDelegadoSuplente){

			this.window = win;
			
			boolean camposValidos=true;
			
			if(cmbDelegadoPrincipal.getText().equalsIgnoreCase("")){
				camposValidos=false;	
			}
			
//			for(InputElement input:inputs){
//				if (!input.isValid()){
//					camposValidos=false;
//					break;
//				}
//			}
			
			/*Validando Campos*/
			if(!camposValidos){
				Messagebox.show("Por Favor Seleccione Un Tecnico Principal", "Information", Messagebox.OK, Messagebox.EXCLAMATION);
			}/*Inicio de Operacion*/
			else{
				final DelegadoEquipo delegadoPrincipalEquipo = new DelegadoEquipo();
				final DelegadoEquipo delegadoSuplenteEquipo = new DelegadoEquipo();
				
				Date fechaActual = new Date();

				if(delegadoPrincipalSeleccionado != null){
					Short responsabilidad = 1; 
					
					delegadoPrincipalEquipo.setFechaAsignacion(fechaActual);
					
					delegadoPrincipalEquipo.setResponsabilidad(responsabilidad);
					
					delegadoPrincipalEquipo.setEstatus(Short.parseShort("1"));
					
					delegadoPrincipalEquipo.setEquipo(this.equipo);
					
					delegadoPrincipalEquipo.setRepresentante(this.delegadoPrincipalSeleccionado);
					
					//delegadoEquipo.setFechaCulminacion(fechaCulminacion);
				}		
				
				if(delegadoSuplenteSeleccionado != null){
					Short responsabilidad = 2; 
					
					delegadoSuplenteEquipo.setFechaAsignacion(fechaActual);
					
					delegadoSuplenteEquipo.setResponsabilidad(responsabilidad);
					
					delegadoSuplenteEquipo.setEstatus(Short.parseShort("1"));
					
					delegadoSuplenteEquipo.setEquipo(this.equipo);
					
					delegadoSuplenteEquipo.setRepresentante(this.delegadoSuplenteSeleccionado);
					
					//delegadoEquipo.setFechaCulminacion(fechaCulminacion);
				}	
				
	        	try{
	        		
	        		
	        		Messagebox.show("Desea Registrar el Delegado?","Confirmacion",
	        				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
	        				         new org.zkoss.zk.ui.event.EventListener<Event>() {
	        		                     
	        			              public void onEvent(Event evt) throws InterruptedException {
	        			            	  
					        		        if (evt.getName().equalsIgnoreCase("onYes")) {
					        		        	
					        		        	if(delegadoPrincipalSeleccionado != null){
					        		        		servicioDelegadoEquipo.guardar(delegadoPrincipalEquipo);
					        		        	}
					        		        	if(delegadoSuplenteSeleccionado  != null){
					        		        		servicioDelegadoEquipo.guardar(delegadoSuplenteEquipo);
					        		        	}
					        		        						        		           
					        		           Messagebox.show("Delegados asignados con �xito", 
					        	    	                       "Information", Messagebox.OK, 
					        	    	                        Messagebox.INFORMATION);
					        		           		
					        		           BindUtils.postGlobalCommand(null, null, "paginar", null);
					        		           window.onClose();
					        		        	   //Window myWindow = (Window) Path.getComponent("/asignarDelegadoEquipos");
					        		        	   //myWindow.detach();
												
											}else{
											}
															
										}
					        		        	   
					        		 });
					        		           
	        	}catch(Exception ex){
	        		Messagebox.show("Error al asignar delegados", 
	    	                "Error", Messagebox.OK, Messagebox.ERROR);
	        	}
				
			}		
		}	
	   
	   
		@Command
		@NotifyChange({"listaDelegados","listaDelegadosAux","delegadoPrincipalSeleccionado","delegadoSuplenteSeleccionado"})
	    public void limpiar(){
	    	this.delegadoPrincipalSeleccionado = null;
	    	this.delegadoSuplenteSeleccionado = null;
			this.actualizarDelegados();
	    }
		
	    @Command
		@NotifyChange({"listaDelegados","listaDelegadosAux","delegadoPrincipalSeleccionado","delegadoSuplenteSeleccionado"})
	    public void actualizarDelegados(){
	    	if(this.listaDelegadosAux!=null){
	    		this.listaDelegados = new ArrayList<Representante>(this.listaDelegadosAux);
	    	}
	    	
	    	if(delegadoPrincipalSeleccionado != null)
	    		this.listaDelegados.remove(delegadoPrincipalSeleccionado);    	
	    	if(delegadoSuplenteSeleccionado !=null)
	    		this.listaDelegados.remove(delegadoSuplenteSeleccionado);        	    	
	    }
	    
		@Command
		public void cerrarVentana(){
			BindUtils.postGlobalCommand(null, null, "paginar", null);
		}

}