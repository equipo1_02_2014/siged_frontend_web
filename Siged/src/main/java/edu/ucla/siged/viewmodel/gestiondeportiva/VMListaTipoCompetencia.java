/** 
 * 	VMListaTipoCompetencia
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestiondeportiva.TipoCompetencia;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioTipoCompetenciaI;


//Declaraci�n de la la clase VMListaTipoCompetencia con sus atributos:
public class VMListaTipoCompetencia {
	
	@WireVariable
	ServicioTipoCompetenciaI servicioTipoCompetencia;
	@WireVariable
	TipoCompetencia tipoCompetencia;
	List<TipoCompetencia> listaTipoCompetencias;
	List<TipoCompetencia> tipoCompetenciasSeleccionadas;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion; //1: El formulario se llama para incluir, 2: Para editar, 3: Si se elimina un solo registro, 4: Si se eliminan varios.
	//Las variables siguientes son del filtro:
	Integer id=0;
	String descripcion="";
	Integer estatusCombo=0;
	boolean estatusFiltro;
	
	//M�todos Get y Set de la clase:
	public TipoCompetencia getTipoCompetencia() {
		return tipoCompetencia;
	}
	
	public void setTipoCompetencia(TipoCompetencia tipoCompetencia) {
		this.tipoCompetencia = tipoCompetencia;
	}
	
	public List<TipoCompetencia> getTipoCompetenciasSeleccionadas() {
		return tipoCompetenciasSeleccionadas;
	}
	
	public void setTipoCompetenciasSeleccionadas(List<TipoCompetencia> tipoCompetenciasSeleccionadas) {
		this.tipoCompetenciasSeleccionadas = tipoCompetenciasSeleccionadas;
	}
	
	public List<TipoCompetencia> getListaTipoCompetencias() {
		return listaTipoCompetencias;
	}
	
	public void setListaTipoCompetencias(List<TipoCompetencia> listaTipoCompetencias) {
		this.listaTipoCompetencias = listaTipoCompetencias;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioTipoCompetencia.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Integer getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(Integer estatus) {
		this.estatusCombo = estatus;
	}
	
	//M�todo para inicializar la lista con los registros totales:
	@Init
	public void init(){
		this.listaTipoCompetencias = servicioTipoCompetencia.buscarTodos(0).getContent();
		this.registrosTotales= servicioTipoCompetencia.totalTipoCompetencia();
	}
	
	//M�todo que llama al formulario para agregar un nuevo registro:
	@Command
	public void agregarTipoCompetencia() { 
		this.tipoOperacion=1; //Como el formulario se llama para incluir, tipoOperacion=1.
		window = (Window)Executions.createComponents("vistas/gestiondeportiva/tipocompetencia.zul", null, null);
		window.doModal();
	}
	
	//M�todo para eliminar un registro de la lista:
	@Command
	@NotifyChange({"listaTipoCompetencias","registrosTotales","paginaActual"})
	public void eliminarTipoCompetencia(@BindingParam("tipoCompetenciaSeleccionada") final TipoCompetencia tipoCompetenciaSeleccionada){
		this.tipoOperacion=3;//Como se eliminar� un registro, tipoOperacion=3.
		Messagebox.show("�Est� seguro que desea eliminar este registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
                        
							if(Messagebox.ON_YES.equals(event.getName())){
								
								servicioTipoCompetencia.eliminar(tipoCompetenciaSeleccionada);
								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
								Messagebox.show("El registro ha sido eliminado exitosamente.","",Messagebox.OK, Messagebox.INFORMATION);
								
							}
						}
					});
		
	}
	
	//M�todo para eliminar varios registros de la lista:
	@Command
	@NotifyChange("listaTipoCompetencias")
	public void eliminarTipoCompetencias(@BindingParam("lista") Listbox lista){
		this.tipoOperacion=4; //Como se eliminar�n varios registros, tipoOperacion=4.
		if(lista.getSelectedItems().size()>0)
		{
			Messagebox.show("�Est� seguro que desea eliminar los registros seleccionados?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
				
						public void onEvent(Event event) throws Exception {
                        
							if(Messagebox.ON_YES.equals(event.getName())){
								
								servicioTipoCompetencia.eliminarVarios(tipoCompetenciasSeleccionadas);
								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
								Messagebox.show("Los registros seleccionados han sido eliminados.","",Messagebox.OK, Messagebox.INFORMATION);
								tipoCompetenciasSeleccionadas=null;
							}
						}
					});
			
		
		}
		else Messagebox.show("Debe seleccionar los registros que desea eliminar.","",Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	//M�todo para modificar un registro:
	@Command
	@NotifyChange({"tipoCompetencia"})
	public void editarTipoCompetencia(@BindingParam("tipoCompetenciaSeleccionada") TipoCompetencia tipoCompetenciaSeleccionada ) {
		
		this.tipoCompetencia=tipoCompetenciaSeleccionada;
		this.tipoOperacion=2; //Como el formulario se llama para editar, tipoOperacion=2.
		
		Map<String, TipoCompetencia> mapa = new HashMap<String, TipoCompetencia>();
        mapa.put("tipoCompetencia", tipoCompetenciaSeleccionada);
		window = (Window)Executions.createComponents("vistas/gestiondeportiva/tipocompetencia.zul", null, mapa);
		window.doModal();
	}
	
	//M�todo para paginar los registros en la lista:
	@GlobalCommand
	@NotifyChange({"listaTipoCompetencias","registrosTotales","paginaActual","tipoCompetenciasSeleccionadas"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaTipoCompetencias = servicioTipoCompetencia.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioTipoCompetencia.totalTipoCompetencia();
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	//M�todo para actualizar la lista:
	@GlobalCommand
	@NotifyChange({"listaTipoCompetencias","registrosTotales","paginaActual"})
    public void actualizarLista(){
		int ultimaPagina=0;
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   
		   if ((this.registrosTotales+1)%this.tamanoPagina==0)
		      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
		   else
			  ultimaPagina =((int) (this.registrosTotales+1)/this.tamanoPagina);
		   this.paginaActual=ultimaPagina; // Se coloca en la �ltima p�gina para que quede visible el que se acaba de ingresar.
		}
		   paginar();
		 
		   if(this.tipoOperacion==3 || this.tipoOperacion==4){
			   this.getRegistrosTotales();
			   
			   if ((this.registrosTotales+1)%this.tamanoPagina==0)
			      ultimaPagina= ((int) (this.registrosTotales+1)/this.tamanoPagina)-1;
			   else
				  ultimaPagina =((int) (this.registrosTotales-1)/this.tamanoPagina);
			   this.paginaActual=ultimaPagina;
			 paginar();
			}	
	}
    
	//M�todo para filtrar los registros:
	@Command
	@NotifyChange({"listaTipoCompetencias","registrosTotales"})
	public void ejecutarFiltro(){
	
		String jpql="";
		String filtroEstatus;
		
		this.estatusFiltro=true;
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		if (id != null)
			
			jpql = " descripcion like '%"+descripcion+"%' and estatus in "+filtroEstatus+"";
		
		System.out.println("----------------------------------"+jpql);
		this.listaTipoCompetencias=servicioTipoCompetencia.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioTipoCompetencia.totalTipoCompetenciasFiltradas(jpql);
	}
	
	//M�todo para asignarle 0 a paginaActual e invocar al m�todo ejecutarFiltro():
	@Command
	@NotifyChange({"listaTipoCompetencias","registrosTotales","paginaActual"})
	public void filtrar(){
		this.paginaActual=0;
		ejecutarFiltro();
	}
	
	//M�todo para cancelar el filtro y mostrar todos los registros:
	@Command
	@NotifyChange({"listaTipoCompetencias","registrosTotales","paginaActual","tipoCompetenciasSeleccionadas",
				   "descripcion","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		id=0;
		descripcion="";
		estatusCombo=0;
	}

}