/** 
 * 	VMCausa
 *	@author: 	Equipo Challenge
 * 	@version:	1.0
 * 	@since:		01/01/2015
 * 	@last:		20/02/2015
 **/

package edu.ucla.siged.viewmodel.gestionatletas;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioCausaI;

public class VMCausa {
	@WireVariable
	ServicioCausaI servicioCausa;
	@WireVariable
	Causa causa;
	List<Integer> listaTipos;
	Integer tipoSeleccionado;
	Window window;
	short tipoOperacion;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////
	
	public Causa getCausa() {
		return causa;
	}
	
	public void setCausa(Causa causa) {
		this.causa = causa;
	}
	
	public List<Integer> getListaTipos() {
		return listaTipos;
	}

	public void setListaTipos(List<Integer> listaTipos) {
		this.listaTipos = listaTipos;
	}

	public Integer getTipoSeleccionado() {
		return tipoSeleccionado;
	}

	public void setTipoSeleccionado(Integer tipoSeleccionado) {
		this.tipoSeleccionado = tipoSeleccionado;
	}

	/////////////////////////////////////// INIT////////////////////////////////////////////////////////
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("causa") Causa causaSeleccionada) {
		    listaTipos= new ArrayList<Integer>();
            listaTipos.add(1);
            listaTipos.add(2);
            Selectors.wireComponents(view, this, false);
            
        if (causaSeleccionada!=null){
        	this.tipoOperacion=2;
            this.causa = causaSeleccionada;
            this.tipoSeleccionado= this.causa.getTipo();
        }else
              {
        	   causa = new Causa();
               this.tipoSeleccionado=1;
               this.tipoOperacion=1;
              }
    }
	////////////////////////////////////////////GUARDAR/////////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"causa","limpiar","tipoSeleccionado","actualizarLista"})
	 public void guardarCausa(@SelectorParam(".causa_restriccion") LinkedList<InputElement> inputs, @BindingParam("window") Window window){
		 this.window = window;
		if (this.tipoSeleccionado!=null)
			causa.setTipo(tipoSeleccionado);
    
		 boolean camposValidos=true;
			
	        for(InputElement input:inputs){
				if (!input.isValid()){
					camposValidos=false;
				break;
				}
			}
			
			if(!camposValidos){
				Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
			}
			
			else{
				if(causa.getNombre()==null || causa.getNombre()=="" || causa.getDescripcion()==null || causa.getDescripcion()==""){
					Messagebox.show("Debe completar todos los campos para continuar","Advertencia",Messagebox.OK, Messagebox.EXCLAMATION);
				}
				else{
					if(this.tipoOperacion==1){
						servicioCausa.guardar(causa);
						Messagebox.show("El registro se ha guardado exitosamente","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
						Messagebox.show("�Desea agregar otro registro?", "Pregunta",Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
      					new EventListener<Event>() {
      						public void onEvent(Event event) throws Exception {
      							if(Messagebox.ON_NO.equals(event.getName())){
      								BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
      								cerrarVentanaCausa();
      							}
      							else BindUtils.postGlobalCommand(null, null, "actualizarLista", null);
      						}
      					});
				    limpiar();
					}
			if(this.tipoOperacion==2)
			    {	
				servicioCausa.guardar(causa);
				Messagebox.show("Los cambios se han guardado exitosamente","Informaci�n", Messagebox.OK, Messagebox.INFORMATION);
				cerrarVentanaCausa();
				}
		    }
	    }   
	}
	
	public void cerrarVentanaCausa(){
		 window.detach();
	 }
	
	////////////////////////////////////////////////LIMPIAR///////////////////////////////////////////////
	@Command
	@NotifyChange({"causa"})
	public void limpiar(){
	causa.setId(null);
	causa.setDescripcion(null);
	causa.setTipo(null);
	causa.setNombre(null);
	}
	
	/////////////////////////////////////////////ABRIR FORMULARIO//////////////////////////////////////////////
	
	@GlobalCommand
	public void abrirFormulario(@BindingParam("causaSeleccionado") Causa causaSeleccionado ) {
		Window window = (Window)Executions.createComponents("causa.zul", null, null);
		this.causa = causaSeleccionado;
		window.doModal();
	}
	
	
   ///////////////////////////////////////////VER ESTATUS////////////////////////////////////////////////////////
	
	@Command
		public String verTipo(Integer tipo){
			String tip="POSTULACION";
			switch (tipo){
			case 1:
			tip="PRACTICA";
			break;
			default:
			tip="POSTULACION";
		}
			return tip;
		}
	///////////////////////////////// VER VALOR////////////////////////////////////////////////////////////////////
	
	@Command
	public void verValor(){
		Messagebox.show(tipoSeleccionado.toString());
	}

}