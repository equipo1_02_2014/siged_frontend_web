package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.List;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioTipoEquipoI;

public class VMTipoEquipo {
		@WireVariable
		ServicioTipoEquipoI servicioTipoEquipo;
		@WireVariable
		TipoEquipo tipoequipo;
		@WireVariable
		List<Integer> listaEstatus;
		Short estatusSeleccionado;
		private Window ventanaTipoEquipo;
		
		//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////
		
		public TipoEquipo getTipoequipo() {
			return tipoequipo;
		}

		public void setTipoequipo(TipoEquipo tipoequipo) {
			this.tipoequipo = tipoequipo;
		}

		public Short getEstatusSeleccionado() {
			return estatusSeleccionado;
		}

		public void setEstatusSeleccionado(Short estatusSeleccionado) {
			this.estatusSeleccionado = estatusSeleccionado;
		}

		public List<Integer> getListaEstatus() {
			return listaEstatus;
		}

		/////////////////////////////////////// INIT////////////////////////////////////////////////////////
		
		@Init
	    public void init(@ContextParam(ContextType.VIEW) Component view,
	            @ExecutionArgParam("tipoequipo") TipoEquipo tipoequipoSeleccionado) {
	        if (tipoequipoSeleccionado!=null){
	            this.tipoequipo = tipoequipoSeleccionado;
	            this.estatusSeleccionado= this.tipoequipo.getEstatus();
	        }else{
	        	tipoequipo = new TipoEquipo();
	        }
		}
		////////////////////////////////////////////GUARDAR/////////////////////////////////////////////////////////////
		
		@Command
		@NotifyChange({"tipoequipo","limpiar","actualizarLista"})   
		public void guardar(@BindingParam("ventanaTipoEquipo") Window ventanaTipoEquipo){
			this.ventanaTipoEquipo = ventanaTipoEquipo;
			if (tipoequipo.getDescripcion()==null || tipoequipo.getDescripcion().isEmpty()){
				Messagebox.show("Por favor introduzca todos los datos","", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			else{
					tipoequipo.setEstatus(Short.parseShort("1"));
					servicioTipoEquipo.guardar(tipoequipo);
					Messagebox.show("Tipo de Equipo guardado exitosamente","", Messagebox.OK, Messagebox.INFORMATION);
					cerrarVentanaTipoEquipo();
				}
			}
		
		////////////////////////////////////////////////LIMPIAR///////////////////////////////////////////////
		
		@Command
		@NotifyChange({"tipoequipo"})
		public void limpiar(){
		tipoequipo.setDescripcion("");
		}
		
		public void cerrarVentanaTipoEquipo(){
			ventanaTipoEquipo.detach();
		}
		
		///////////////////////////////////////////VER ESTATUS////////////////////////////////////////////////////////
		
		@Command
		public String verStatus(Short estatus){
			String est="Activo";
			switch (estatus){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
			}
			return est;
		}

	}