package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.reporte.Asistencia;
import edu.ucla.siged.domain.reporte.RecursoLlega;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.gestioneventos.ServicioDonacionRecursoI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteAsistenciaI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioReporteEstadisticoI;

public class VMRecursosLlegan{
	
	 
	    @WireVariable
	    private ServicioReporteEstadisticoI servicioReporteEstadistico;	    
	    @WireVariable
	    private ServicioDonacionRecursoI servicioDonacionRecurso;
	
		
	    private boolean evento;
		
		private boolean noEvento;
		
			
		private Date fechaDesde;
		private Date fechaHasta;
		
		private Integer cantidadRecursoSeleccionada;
		
		private String pathProyecto;
		
		
		private List<Integer> listaCantidadRecursos;
		
		private Window window;
					
		@Init
		public void init(){
		
			this.setWindow(null);
						
			/*********Para obtener el path del proyecto******/
			
			File file= new File(VMRelacionJugadosGanados.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			
			this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
    		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
			//Sustitucion de caracteres codificados (Para servidores con Windows)
			this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
			
			/***********************************/
			
			this.inicializarCampos();
		}
		
		@NotifyChange({"evento","noEvento","fechaHasta",
			           "fechaDesde","cantidadRecursoSeleccionada"})
		public void inicializarCampos(){
			
			this.evento=true;
			this.noEvento=false;
			this.fechaDesde=null;
			this.fechaHasta=null;
					
		}
		
		@Command
		@NotifyChange({"evento","noEvento", "fechaHasta",
	           "fechaDesde","cantidadRecursoSeleccionada"})
		public void cancelar(){
			this.inicializarCampos();
		}
				
		
		@Command
		public void imprimir(){		
			
			
				String restricciones="";
				String idevento = null;
						
				if((this.fechaDesde!=null && this.fechaHasta!=null)){	
					
					if (this.fechaDesde!=null && this.fechaHasta!=null){
						
						String desde= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaDesde);
						String hasta= new SimpleDateFormat("YYYY-MM-dd").format(this.fechaHasta);
						restricciones += " AND donacion.fecha BETWEEN '" + desde + "' AND '" + hasta + "'";
						
					}
																	
									
					
				}
				
				List<RecursoLlega> lista= this.servicioReporteEstadistico.buscarRecursos(restricciones,idevento);
							
				
				if (!lista.isEmpty()){
					
					Map<String,Object> parameterMap = new HashMap<String,Object>();
					
					JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
					
					parameterMap.put("datasource", JRdataSource);
					
					parameterMap.put("titulo","Recursos que llegan a la Fundacion");
					
					parameterMap.put("rutaLogoFundacion",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") + "logo_fundacion_luis_sojo.png");
					
					parameterMap.put("rutaLogoEscuela",this.pathProyecto + System.getProperty("file.separator") + "source" +  System.getProperty("file.separator") + "images" + System.getProperty("file.separator") +"logo_escuela.gif");
					
					if(evento){
						parameterMap.put("tipo","Eventos");
						
					}else{
						parameterMap.put("tipo","Sin eventos");
						idevento=null;
					}
					
					try {
						
						JasperDesign jasDesign=null;
						
						jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "reporteRecursoTorta.jrxml");
						
						JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
				        
				        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
				        
				        JasperViewer.viewReport(jasperPrint,false);
						
					} catch (JRException e) {
						e.printStackTrace();
						Messagebox.show(e.getMessage());
					}
				
				

				}else{
					Messagebox.show("El Reporte no puede ser Generado la Informacion devuelta se encuentra vacia", 
			                "Information", Messagebox.OK, Messagebox.EXCLAMATION);
				}
				
			     
		}
		
		@Command
		@NotifyChange({"evento","noEvento"})
		public void seleccionarVista(@BindingParam("vista")String vista){
			
			this.evento=false;
			this.noEvento=false;
			
			if (vista.equalsIgnoreCase("Evento")){
				this.evento=true;
			}
			else if(vista.equalsIgnoreCase("Sin eventos")){
				this.noEvento=false;
			}
			
		}
		
		
		public Date getFechaDesde() {
			return fechaDesde;
		}

		public void setFechaDesde(Date fechaDesde) {
			this.fechaDesde = fechaDesde;
		}

		public Date getFechaHasta() {
			return fechaHasta;
		}

		public void setFechaHasta(Date fechaHasta) {
			this.fechaHasta = fechaHasta;
		}

		

		public Window getWindow() {
			return window;
		}

		public void setWindow(Window window) {
			this.window = window;
		}

	

		public boolean isEvento() {
			return evento;
		}

		public void setEvento(boolean evento) {
			this.evento = evento;
		}

		public boolean isNoEvento() {
			return noEvento;
		}

		public void setNoEvento(boolean noEvento) {
			this.noEvento = noEvento;
		}

		public Integer getCantidadRecursoSeleccionada() {
			return cantidadRecursoSeleccionada;
		}

		public void setCantidadRecursoSeleccionada(Integer cantidadRecursoSeleccionada) {
			this.cantidadRecursoSeleccionada = cantidadRecursoSeleccionada;
		}

		public List<Integer> getListaCantidadRecursos() {
			return listaCantidadRecursos;
		}

		public void setListaCantidadRecursos(List<Integer> listaCantidadRecursos) {
			this.listaCantidadRecursos = listaCantidadRecursos;
		}

	
}
