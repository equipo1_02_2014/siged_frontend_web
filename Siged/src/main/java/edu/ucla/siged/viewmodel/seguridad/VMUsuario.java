package edu.ucla.siged.viewmodel.seguridad;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.image.AImage;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.domain.seguridad.Usuario;
import edu.ucla.siged.domain.utilidades.Archivo;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioEmailsI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolUsuarioI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;

public class VMUsuario {
	@WireVariable
	ServicioUsuarioI servicioUsuario; // Instancia la interface servicio	
	@WireVariable
	ServicioRolI servicioRol; // Instancia la interface servicio	
	@WireVariable
	ServicioRolUsuarioI servicioRolUsuario; // Instancia la interface servicio	
	@WireVariable
	ServicioEmailsI servicioEmails;
	@WireVariable
	Usuario usuario;
	private ArrayList<Integer> listaEstatus;
	private Short estatusSeleccionado;
	private AImage imagenT;
	private Rol rolSeleccionado;
	private List<Rol> listaRoles;
	private List<RolUsuario> rolesUsuarioSeleccionados;
	@WireVariable
	private RolUsuario rolUsuarioSeleccionado=new RolUsuario();
	private List<RolUsuario> listaRolesUsuario;  //Modelo roles
	private  String password;
	private boolean activarEstatus;
	
	short estatusCombo=0;
	boolean nuevo=false;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	private List<String> listaNacionalidad;
	private String nacionalidadSeleccionada;
	private String cedula;
	public VMUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
            		 @ExecutionArgParam("usuarioSeleccionado") Usuario usuarioSeleccionado) {
		 listaEstatus= new ArrayList<Integer>();
         listaEstatus.add(0);
         listaEstatus.add(1);
         
         listaRoles = servicioRol.buscarActivos();
         
         Selectors.wireComponents(view, this, false);
        if (usuarioSeleccionado!=null){
        	nuevo=false;
        	activarEstatus=true;
        	this.cedula=usuarioSeleccionado.getCedula().substring(1);
        	this.nacionalidadSeleccionada=usuarioSeleccionado.getCedula().substring(0, 1);
        	this.listaRolesUsuario=servicioRolUsuario.getRolesUsuario(usuarioSeleccionado);
            this.usuario = usuarioSeleccionado;
            this.estatusSeleccionado= this.usuario.getEstatus();
            if(usuarioSeleccionado.getFoto().getTamano() > 0){
				try{
					imagenT = new AImage(usuario.getFoto().getNombreArchivo(), usuario.getFoto().getContenido());
				}catch(IOException e){}
            }
        }else{
        	nuevo=true;
        	activarEstatus=true;
        	limpiarUsuario();
        	this.usuario = new Usuario();
        	listaRolesUsuario= new ArrayList<RolUsuario>();
        	nacionalidadSeleccionada="V";
        	estatusSeleccionado=1;
        	this.generarPassword();
        }
     }      
	
	@Command
	@NotifyChange({"listaRolesUsuario"})
	public String verStatus(Short estatus){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}
	
	public Short getEstatusSeleccionado() {
		return estatusSeleccionado;
	}

	public void setEstatusSeleccionado(Short estatusSeleccionado) {
		this.estatusSeleccionado = estatusSeleccionado;
	}

	public List<Integer> getListaEstatus() {
		return listaEstatus;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Rol> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<Rol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public AImage getImagenT() {
		return imagenT;
	}

	public void setImagenT(AImage imagenT) {
		this.imagenT = imagenT;
	}

	public List<RolUsuario> getRolesUsuarioSeleccionados() {
		return rolesUsuarioSeleccionados;
	}

	public void setRolesUsuarioSeleccionados(
			List<RolUsuario> rolesUsuarioSeleccionados) {
		this.rolesUsuarioSeleccionados = rolesUsuarioSeleccionados;
	}

	public RolUsuario getRolUsuarioSeleccionado() {
		return rolUsuarioSeleccionado;
	}

	public void setRolUsuarioSeleccionado(RolUsuario rolUsuarioSeleccionado) {
		this.rolUsuarioSeleccionado = rolUsuarioSeleccionado;
	}

	public List<RolUsuario> getListaRolesUsuario() {
		return this.listaRolesUsuario;
	}

	public void setListaRolesUsuario(List<RolUsuario> listaRolesUsuario) {
		this.listaRolesUsuario = listaRolesUsuario;
	}

	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public void setListaEstatus(ArrayList<Integer> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	public Rol getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Rol rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}
	
	public String getNacionalidadSeleccionada() {
		return nacionalidadSeleccionada;
	}

	public void setNacionalidadSeleccionada(String nacionalidadSeleccionada) {
		this.nacionalidadSeleccionada = nacionalidadSeleccionada;
	}
	
	public List<String> getListaNacionalidad() {
		List<String> lista= new ArrayList<String>();		 
        lista.add("V");
        lista.add("E");
        return lista;
	}
	
	public void setListaNacionalidad(List<String> listaNacionalidad) {
		this.listaNacionalidad = listaNacionalidad;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isNuevo() {
		return nuevo;
	}

	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}

	@Command
	@NotifyChange({"usuario"})
	public void generarPassword() {
	
		String base="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$";
		int longitud = base.length();
		final int LARGO_PASWWORD=8;
		password="";
		for(int i=0; i<LARGO_PASWWORD;i++){ 
		    int numero = (int)(Math.random()*(longitud)); 
		    String caracter=base.substring(numero, numero+1); 
		    password+=caracter; 
		}
		usuario.setPassword(password);
	}
	
	@Command
	public void probarCambiarPerfil() {
		Window window;	
		Map<String,Usuario> mapUsuario = new HashMap<String, Usuario>();
		mapUsuario.put("usuarioSesion", usuario);
		window = (Window)Executions.createComponents("/vistas/seguridad/cambiopassword.zul", null, mapUsuario);
		window.doModal();
	}
	
	@Command
	public void probarRecuperarPassword() {
		Window window;	
		Map<String,String> mapUsuario = new HashMap<String, String>();
		mapUsuario.put("nombreUsuario", usuario.getNombreUsuario());
		window = (Window)Executions.createComponents("/vistas/seguridad/recuperapassword.zul", null, mapUsuario);
		window.doModal();
	}
	
	public boolean buscarRolActivo() {
		for(RolUsuario ru : this.listaRolesUsuario)
		{
			if(ru.getEstatus()==1)
				return true;
		}
		return false;
	}
	
	@Command
	@NotifyChange({"usuario", "imagenT"})
	public void guardarUsuario(@SelectorParam(".usuarioRestriccion") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		if(!camposValidos){
			Messagebox.show("Por Favor Introduzca Campos Válidos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			return;
		}
		if (usuario.getNombre().equals("") || usuario.getApellido().equals("") || this.cedula.equals("")
			|| usuario.getEmail().equals("") ||this.password.equals("") || usuario.getCelular().equals("") || usuario.getTelefono().equals("")
			|| usuario.getNombreUsuario().equals("") || usuario.getFechaRegistro()==null || usuario.getFechaNacimiento()==null
			|| this.estatusSeleccionado==null)
			
	
			Messagebox.show("No pueden haber campos vacios", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		
		else{
			if(!this.buscarRolActivo() && this.estatusSeleccionado==1)
			{
				Messagebox.show("Por favor asigne al menos un rol al usuario", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				return;
			}
			Usuario encontrado=servicioUsuario.buscarByNombreUsuario(usuario.getNombreUsuario());
			if(encontrado==null || usuario.getId().equals(encontrado.getId())){
			if (this.estatusSeleccionado!=null)
			    usuario.setEstatus(estatusSeleccionado);
			try{
				for(int i=0; i<this.listaRolesUsuario.size(); i++)
					listaRolesUsuario.get(i).setUsuario(usuario);
				usuario.setCedula(this.nacionalidadSeleccionada+this.cedula);
				servicioUsuario.guardar(usuario);	
				servicioEmails.enviarEmailUsuarioNuevo(usuario.getEmail(), usuario.getNombreUsuario(), usuario.getPassword());
				if(listaRolesUsuario.size()>0)
				servicioRolUsuario.guardarListaRolUsuario(listaRolesUsuario);
			}
			catch(Exception e){}
			Messagebox.show("Datos almacenados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
			limpiarUsuario();
			Window myWindow = (Window) Path.getComponent("/vistaUsuario");
			myWindow.detach();
			}else{	
				Messagebox.show("El nombre de usuario no se encuentra disponible", "Información", Messagebox.OK, Messagebox.INFORMATION);}
			}
		
	}
		
	@Command
	@GlobalCommand("limpiarUsuario")
	@NotifyChange({"usuario", "imagenT", "listaRolesUsuario", "cedula"})
		public void limpiarUsuario(){
			this.usuario= new Usuario();	
			this.cedula="";
			this.password="";
			limpiarRolesUsuario();
		}
	
	@Command
	@GlobalCommand("asignarNuevoUsuario")
	@NotifyChange({"usuario", "imagenT", "listaRolesUsuario", "cedula"})
		public void asignarNuevoUsuario(){
	     	   usuario.setId(null);
	    	   usuario.setNombreUsuario("");
	    	   usuario.setPassword("");
	    	   usuario.setRespuestaSecreta("");
	    	   usuario.setPreguntaSecreta("");
	    	   limpiarRolesUsuario();
		}
	
	
	//-- Lista de Roles del usuario

	@Command
	@NotifyChange({"usuario", "imagenT", "listaRolesUsuario"})
	public void limpiarRolesUsuario(){
		this.listaRolesUsuario= new ArrayList<RolUsuario>();		
	}

	
	public boolean buscarRolUsuario(RolUsuario rolb) {
		for(RolUsuario ru : listaRolesUsuario)
		{
			if(ru.getRol().getDescripcion().equals(rolb.getRol().getDescripcion()) && ru.getEstatus()==1)
			return true;
		}
		return false;
	}
	
	
	@Command
	@NotifyChange({"usuario","rolUsuario","listaRolesUsuario"})
	public void agregarRolUsuario(){
		if(rolSeleccionado==null)
		{Messagebox.show("Debe seleccionar un rol", "Información", Messagebox.OK, Messagebox.INFORMATION);}
		else{	
		RolUsuario rolUsuariot= new RolUsuario(new Date(), (short) 1, rolSeleccionado);
		if(!buscarRolUsuario(rolUsuariot)){
			listaRolesUsuario.add(rolUsuariot);
		}
		else{
			Messagebox.show("Ese rol ya fué agregado al usuario", "Información", Messagebox.OK, Messagebox.INFORMATION);}
		}
	}
	
	@Command
	@NotifyChange({"listaRolesUsuario", "usuario"})
	public void eliminarRolUsuario(@BindingParam("rolUsuarioSeleccionado") RolUsuario rolUsuarioSeleccionado){
		if(rolUsuarioSeleccionado.getId()!=null){
			rolUsuarioSeleccionado.setFechaEliminacion(new Date());
			rolUsuarioSeleccionado.setEstatus(Short.parseShort("0"));
		}
		else{
			this.listaRolesUsuario.remove(rolUsuarioSeleccionado);
		}
		Messagebox.show("Datos eliminados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({"listaRolesUsuario", "usuario"})
	public void eliminarRolesUsuario(){
		if(rolesUsuarioSeleccionados!=null){
			RolUsuario rolUsuarioTemp =null;
			List<RolUsuario> rolesEliminados =new ArrayList<RolUsuario>();
			for (int i=0;i<rolesUsuarioSeleccionados.size();i++){
				rolUsuarioTemp=rolesUsuarioSeleccionados.get(i);	
				if(rolUsuarioTemp.getId()!=null){
					rolUsuarioTemp.setFechaEliminacion(new Date());
					rolUsuarioTemp.setEstatus(Short.parseShort("0"));
					rolesEliminados.add(rolUsuarioTemp);
				} else{
					this.listaRolesUsuario.remove(rolUsuarioSeleccionado);
				}				
			}	
			this.rolesUsuarioSeleccionados=null;
			if(rolesEliminados.size()>0){
				servicioRolUsuario.guardarListaRolUsuario(rolesEliminados);
			}
			Messagebox.show("Datos eliminados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay datos seleccionados", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
	}

	public void eliminarRolesUsuarioRegistrados(RolUsuario rolb) {
		for(RolUsuario ru : listaRolesUsuario)
		{
			if(ru.getRol().getId()==rolb.getRol().getId())
			{rolb.setFechaEliminacion(new Date());
			rolb.setEstatus(Short.parseShort("0"));}
		}
	}
	
	@Command
	@NotifyChange({"listaRolesUsuario", "usuario"})
	public void eliminarRolesUsuario2(){
		if(rolesUsuarioSeleccionados!=null){
			RolUsuario rolUsuarioTemp =null;
			for (int i=0;i<rolesUsuarioSeleccionados.size();i++){
				rolUsuarioTemp=rolesUsuarioSeleccionados.get(i);	
				if(rolUsuarioTemp.getId()!=null){
					this.eliminarRolesUsuarioRegistrados(rolUsuarioTemp);
				} else{
					this.listaRolesUsuario.remove(rolUsuarioTemp);
				}
			}	
			this.rolesUsuarioSeleccionados=null;
			Messagebox.show("Datos eliminados correctamente", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
		else{
			Messagebox.show("No hay datos seleccionados", "Información", Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	@Command
	@NotifyChange({"usuario","imagenT"})
	public void subirImagen(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event){
		Media media= event.getMedia();
		if(media != null){
			if(media instanceof org.zkoss.image.Image){
				Archivo foto = new Archivo();
				foto.setNombreArchivo(media.getName());
				foto.setTipo(media.getContentType());
				foto.setContenido(media.getByteData());
				usuario.setFoto(foto);
				imagenT = (AImage) media;
			}else{}
		}
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		if(fecha!=null){
			String fechaFormateada="";
			fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
			return fechaFormateada;
		}
		return "-";
	}
	
	public boolean isActivarEstatus() {
		return activarEstatus;
	}

	public void setActivarEstatus(boolean activarEstatus) {
		this.activarEstatus = activarEstatus;
	}
	
	@Command
	@NotifyChange({"usuario", "imagenT", "listaRolesUsuario", "password"})
		public void buscarUsuario(){
			Usuario usuarioTemp=servicioUsuario.buscarPersonaByCedula(this.nacionalidadSeleccionada+this.cedula);
			if(usuarioTemp!=null){
				this.usuario=usuarioTemp;
				try {
					this.imagenT = new AImage(usuario.getFoto().getNombreArchivo(), usuario.getFoto().getContenido());
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					this.imagenT=null;
				}
					
				if(usuario.getId()!=null)
				{
					this.listaRolesUsuario=servicioRolUsuario.getRolesUsuario(usuario);
					Messagebox.show("Esta persona ya tiene un usuario asociado, ¿desea crear un usuario nuevo para esta persona?","Confirmacion",
					         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
					         new org.zkoss.zk.ui.event.EventListener<Event>() {
			                    
				    public void onEvent(Event evt) throws InterruptedException {
				            	  
			       		    if (evt.getName().equalsIgnoreCase("onNo")) {
			       		    	if(usuario.getEstatus().equals((short) 0)){
			       		    		BindUtils.postGlobalCommand(null, null, "limpiarUsuario", null);
					            	 Messagebox.show("El usuario se encuentra inactivo", "Información", Messagebox.OK, Messagebox.INFORMATION);
				       		    	}
			       		    }
			       		    else{
			       		 	BindUtils.postGlobalCommand(null, null, "asignarNuevoUsuario", null);
					           
			       		       usuario.setId(null);
		       		    	   usuario.setNombreUsuario("");
		       		    	   usuario.setPassword("");
		       		    	   usuario.setRespuestaSecreta("");
		       		    	   usuario.setPreguntaSecreta("");
			       		    }
			       	}
					});
				}
			}
			else{
				Messagebox.show("No se han encontrado datos de esta persona en el sistema", "Información", Messagebox.OK, Messagebox.INFORMATION);
				limpiarUsuario();
			}
		}
}