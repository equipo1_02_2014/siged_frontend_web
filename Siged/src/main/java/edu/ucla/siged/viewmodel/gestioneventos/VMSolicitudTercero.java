package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;
import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMSolicitudTercero {
	
	@WireVariable
	private ServicioDonacion servicioDonacion;
	
	@WireVariable
	private Donacion donacion = new Donacion();
	
	@WireVariable
	private Set<DonacionRecurso> recursos; // = new HashSet<DonacionRecurso>();
	
	private Window window;
	
	public VMSolicitudTercero(){
		
	}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("donacion") Donacion donacionSeleccionada){
		Selectors.wireComponents(view, this, false);
		if(donacionSeleccionada!=null){
			this.donacion = donacionSeleccionada;
			this.recursos = this.donacion.getRecursosDonados();
		}
	}

	public Donacion getDonacion() {
		return donacion;
	}

	public void setDonacion(Donacion donacion) {
		this.donacion = donacion;
	}

	public Set<DonacionRecurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(Set<DonacionRecurso> recursos) {
		this.recursos = recursos;
	}

	@Command
	@NotifyChange({"donacion"})
	public void Aprobar(@SelectorParam("textbox,datebox,timebox") LinkedList<InputElement> inputs){
		boolean camposValidos=true;
		boolean cantidadVacia=false;
		for(InputElement input:inputs){
			if (!input.isValid()){
				camposValidos=false;
				break;
			}
		}
		
		if(!camposValidos)
			Messagebox.show("El campo observacion no puede estar vacio", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		else {
			this.donacion.setFechaRespuesta(new Date());
			this.donacion.setEstatus((short)1);
			for(DonacionRecurso aux : this.donacion.getRecursosDonados()){
				if(aux.getCantidad()>0)
					cantidadVacia=true;
			}
			if(cantidadVacia==true){
				try{
					servicioDonacion.Guardar(donacion);
					Messagebox.show("Solicitud Aprobada", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
				}catch(Exception e){
					System.out.println(e.getMessage());
				}
				Limpiar();
				Window myWindow = (Window) Path.getComponent("/vistaSolicitud");
				myWindow.detach();
			}
			else{
				Messagebox.show("Debe asignar al menos una cantidad aprobada distinta de 0", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
			
		}
	}
	
	@Command
	@NotifyChange({"donacion"})
	public void Rechazar(){
		if(donacion.getObservacion() == null || donacion.getObservacion().equals(""))
			Messagebox.show("El campo observacion no puede estar vacio", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		else {
			
			this.donacion.setFechaRespuesta(new Date());
			this.donacion.setEstatus((short)2);
			for(DonacionRecurso aux : this.donacion.getRecursosDonados()){
				aux.setCantidad(0);
			}
			try{
				servicioDonacion.Guardar(donacion);
				Messagebox.show("Solicitud rechazada", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			Limpiar();
			Window myWindow = (Window) Path.getComponent("/vistaSolicitud");
			myWindow.detach();
			
		}
	}
	
	@Command
	@NotifyChange({"donacion"})
	public void Limpiar(){
		this.donacion = new Donacion();
		this.recursos = new HashSet<DonacionRecurso>();
	}
	
	@Command
	public String VerTipoDonacion(Integer aux){
		String est="Entrante";
		switch (aux){
			case 1:
				est="Saliente";
			    break;
			default:
				est="Entrante";
		}
		return est;
	}
	
	@Command
	public String VerEstatusDonacion(Integer aux){
		String est="Solicitada";
		switch (aux){
			case 1:
				est="Aprobada";
			    break;
			case 2:
				est="Rechazada";
			    break;
			case 3:
				est="Entregada";
			    break;
			case 4:
				est="Recibida";
			    break;
			default:
				est="Solicitada";
		}
		return est;
	}

}
