package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestiondeportiva.Juego;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioJuegoI;

public class VMJuegoResultado {
    
	
	@WireVariable
    private ServicioJuegoI servicioJuego;
	
	private Juego juego;
	
	private Integer resultadoOrig;
	
    private boolean gano;
	
	private boolean perdio;
	
	private boolean empato;
	
	private Window window;

	
	@Init
	public void init(){
		
		this.juego= (Juego)Sessions.getCurrent().getAttribute("JuegoEditarResultado");
		
		this.resultadoOrig= this.juego.getResultado();
		
		this.window= null;
		
		this.inicializarCampos();
	}
	
	@NotifyChange({"gano","perdio","empato","juego"})
	public void inicializarCampos(){

		this.juego.setResultado(resultadoOrig);
		
		this.gano= this.juego.getResultado().equals(1);
		
		this.perdio= this.juego.getResultado().equals(0);
		
		this.empato=this.juego.getResultado().equals(2);
	}
	
	@Command
	@NotifyChange({"gano","perdio","empato","juego"})
	public void cancelar(){
		
		this.inicializarCampos();
	}
	
	@GlobalCommand("refreshjuegoresultado")
    @NotifyChange({"gano","perdio","empato","juego"})
    public void refresh(){
    	 this.inicializarCampos();	
    }
	
	
	@Command
	@NotifyChange({"gano","perdio","empato"})
	public void seleccionarResultado(@BindingParam("resultado")String resultado){
		
		this.gano=false;
		
		this.perdio= false;
		
		this.empato=false;
		
		if (resultado.equalsIgnoreCase("GANAMOS")){
			this.gano=true;
		}
		else if(resultado.equalsIgnoreCase("PERDIMOS")){
			this.perdio=true;
		}
        else if(resultado.equalsIgnoreCase("EMPATAMOS")){
			this.empato=true;
		}
		
	}
	
	@Command
	public void cerrarVentana(){
		BindUtils.postGlobalCommand(null, null, "paginarJuegoResultado", null);
	}
	
	@Command
	public void guardar(@SelectorParam("#wdwGestionDeportivaJuegoResultado") Window win){
		
			this.window=win;
			
            try{
        		Messagebox.show("Desea Registrar los Resultados del Juego?","Confirmacion",
        				         Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
        				         new org.zkoss.zk.ui.event.EventListener<Event>() {
        		                     
        			              public void onEvent(Event evt) throws InterruptedException {
        			            	  
				        		        if (evt.getName().equalsIgnoreCase("onYes")) {
				        		        	
				        		           if (gano){
				        		        	   juego.setResultado(1);
				        		           }else if (perdio){
				        		        	   juego.setResultado(0);
				        		           }else{
				        		        	   juego.setResultado(2);
				        		           }
		                                 
				        		           servicioJuego.editar(juego);
				        		           
				        		           Messagebox.show("Resultado del Juego Guardado con Exito", 
				        	    	                       "Information", Messagebox.OK, 
				        	    	                        Messagebox.INFORMATION);
				        		           
				        		           BindUtils.postGlobalCommand(null, null, "paginarJuegoResultado", null);
				        		           
				        		           window.onClose();
				        		          
	
				        		        }else{
				        		        	BindUtils.postGlobalCommand(null, null, "refreshjuegoresultado", null);
				        		        }
        			              }
        		});
        		
        	}catch(Exception ex){
        		Messagebox.show("Error al Registrar Resultado", 
    	                "Error", Messagebox.OK, Messagebox.ERROR);
        	}
			
		
		
	}

	
	
    public String formatearFecha(Date fecha){
		
		String fechaFormateada="";
		
		if(fecha!=null){
		  fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		}
		
		return fechaFormateada;
	}
	
    public String formatearHora(Date fecha){
        String fechaFormateada="";
		
		if(fecha!=null){
		  fechaFormateada= new SimpleDateFormat("hh:mm a").format(fecha);
		}
		
		return fechaFormateada;
	}
    
	public String formatearJuego(Juego juego){
		String juegoFormateado= "";
		
		if (juego!=null){
		   juegoFormateado += this.formatearFecha(juego.getFecha()) + " " + this.formatearHora(juego.getHoraInicio()) +
				               " " + juego.getEquipo().getNombre() + " VS " + juego.getEquipoContrario();
		}
		return juegoFormateado;
	}
	
	public boolean isGano() {
		return gano;
	}

	public void setGano(boolean gano) {
		this.gano = gano;
	}

	public boolean isPerdio() {
		return perdio;
	}

	public void setPerdio(boolean perdio) {
		this.perdio = perdio;
	}

	public boolean isEmpato() {
		return empato;
	}

	public void setEmpato(boolean empato) {
		this.empato = empato;
	}

	public Juego getJuego() {
		return juego;
	}
	
	
}
