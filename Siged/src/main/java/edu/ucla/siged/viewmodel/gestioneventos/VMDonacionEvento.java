package edu.ucla.siged.viewmodel.gestioneventos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import edu.ucla.siged.domain.gestioneventos.Donacion;
import edu.ucla.siged.domain.gestioneventos.DonacionRecurso;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.gestioneventos.Recurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacion;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioDonacionRecurso;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioEvento;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioRecurso;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMDonacionEvento {
	@WireVariable
	private ServicioEvento servicioEvento;
	@WireVariable
	private ServicioDonacion servicioDonacion;
	@WireVariable
	private ServicioRecurso servicioRecurso;
	@WireVariable
	private ServicioDonacionRecurso servicioDonacionRecurso;
	@WireVariable
	private List<Recurso> comboxRecursos;
	@WireVariable
	private DonacionRecurso donacionRecurso = new DonacionRecurso();
	@WireVariable
	private Set<DonacionRecurso> donacionRecursos = new HashSet<DonacionRecurso>();
	@WireVariable
	private Evento evento = new Evento();
	@WireVariable
	private Donacion donacion = new Donacion();
	private Set<DonacionRecurso> recursosPorEliminar = new HashSet<DonacionRecurso>();
	
	public VMDonacionEvento(){}
	
	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("evento") Evento eventoSeleccionado){
		this.evento = new Evento();
		this.donacion = new Donacion();
		
		this.evento = eventoSeleccionado;
		this.comboxRecursos = servicioRecurso.ListarRecursos();
		
		this.donacionRecurso = new DonacionRecurso();
		this.donacionRecursos = new HashSet<DonacionRecurso>();
	}

	public List<Recurso> getComboxRecursos() {
		return comboxRecursos;
	}

	public void setComboxRecursos(List<Recurso> comboxRecursos) {
		this.comboxRecursos = comboxRecursos;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Donacion getDonacio() {
		return donacion;
	}

	public void setDonacio(Donacion donacion) {
		this.donacion = donacion;
	}
	
	public DonacionRecurso getDonacionRecurso() {
		return donacionRecurso;
	}

	public void setDonacionRecurso(DonacionRecurso donacionRecurso) {
		this.donacionRecurso = donacionRecurso;
	}

	public Set<DonacionRecurso> getDonacionRecursos() {
		return donacionRecursos;
	}

	public void setDonacionRecursos(Set<DonacionRecurso> donacionRecursos) {
		this.donacionRecursos = donacionRecursos;
	}
	
	@Command
	@NotifyChange({"donacion","evento","donacionRecurso","donacionRecursos"})
	public void Limpiar(){
		this.donacion = new Donacion();
		this.donacionRecurso = new DonacionRecurso();
		this.donacionRecursos = new HashSet<DonacionRecurso>();
	}
	
	@NotifyChange({"donacion","donacionRecurso"})
	public void LimpiarRecurso(){
		donacionRecurso = new DonacionRecurso();
	}
	
	@Command
	@NotifyChange({"donacion","recursos","donacionRecurso","donacionRecursos"})
	public void CargarRecursos(){
		if(donacionRecurso.getRecurso() != null){
			if(!RecursoContenido(donacionRecurso)){
				if(donacionRecurso.getCantidad()== null || donacionRecurso.getCantidad()<=0){
					Messagebox.show("Debe ingresar una cantidad valida!!", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
				}else{
					this.donacionRecursos.add(donacionRecurso);
					LimpiarRecurso();
				}
			}else{
				Messagebox.show("El recurso ya fue seleccionado", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}else{
			Messagebox.show("No ha seleccionado un recursos", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}
	
	//terorna true si el recurso ya existe de lo contrario retorna false
	private boolean RecursoContenido(DonacionRecurso dr){
		for(DonacionRecurso aux:donacionRecursos){
			if(aux.getRecurso().getId()==dr.getRecurso().getId())
				return true;
		}
		return false;
	}
	
	@Command
	@NotifyChange({"donacionRecursos"})
	public void EliminarRecurso(@BindingParam("recursoSeleccionado") DonacionRecurso recursoSeleccionado){
		this.recursosPorEliminar.add(recursoSeleccionado);
		this.getDonacionRecursos().remove(recursoSeleccionado);
	}
	
	@Command
	@NotifyChange({"donacion","donacionRecursos"})
	public void GuardarDonacion(){
		if(!this.donacionRecursos.isEmpty()){
			this.donacion.setRecursosDonados(donacionRecursos);
			try{
				donacion.setEmail(evento.getContactoCorreo());
				donacion.setNombre(evento.getNombre());
				donacion.setRif(evento.getResponsable());
				donacion.setEvento(evento);
				donacion.setEstatus((short)5);
				donacion.setTipo((short)0);
				donacion.setFecha(evento.getFecha());
				donacion.setFechaRespuesta(evento.getFecha());
				
				
				servicioDonacion.Guardar(donacion);
				Messagebox.show("Datos almacenados correctamente", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
				if(!recursosPorEliminar.isEmpty()){
					servicioDonacionRecurso.EliminarVarios(recursosPorEliminar);
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			Limpiar();
			Window myWindow = (Window) Path.getComponent("/idDonacionEvento");
			myWindow.detach();
		}else{
			Messagebox.show("No se han cargado recursos al evento", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
		}
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		String fechaFormateada="";
		fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}

}
