package edu.ucla.siged.viewmodel.gestioneventos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.servicio.impl.gestioneventos.ServicioAdjuntoEvento;
import edu.ucla.siged.servicio.impl.gestioneventos.ServicioEvento;
import edu.ucla.siged.domain.gestioneventos.AdjuntoEvento;
import edu.ucla.siged.domain.gestioneventos.Evento;
import edu.ucla.siged.domain.utilidades.Archivo;

@Controller
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VMMostrarEvento {

	@WireVariable
	private ServicioEvento servicioEvento;
	@WireVariable
	private ServicioAdjuntoEvento servicioAdjuntoEvento;
	private Evento eventoSeleccionado;
	private HashMap<String, Evento> eventoMap = new HashMap<String, Evento>();
	private AdjuntoEvento adjuntoEvento;
	private Window window;
	private List<AdjuntoEvento> listaAdjuntosSeleccionados;

	@AfterCompose
	public void doAfterCompose(Component window) throws Exception {
		this.eventoMap = (HashMap<String, Evento>) Executions.getCurrent().getArg();
		this.setEventoSeleccionado(this.eventoMap.get("evento"));
	}

	@Command
	@NotifyChange("actualizar")
	public void actualizar() {
		servicioEvento.Actualizar(this.eventoSeleccionado);
	}

	@NotifyChange("*")
	@Command
	public void guardarEvento(@BindingParam("ventana") Window ventana) {
		try {
			this.servicioEvento.Actualizar(eventoSeleccionado);
			Messagebox.show("Datos actualizados correctamente", "Informacion",
					Messagebox.OK, Messagebox.INFORMATION);
			this.setEventoSeleccionado(new Evento());
			ventana.onClose();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			Messagebox
					.show("Ocurrio un error al actualizar los datos, por favor verifique",
							"Informacion", Messagebox.OK,
							Messagebox.INFORMATION);
		}

	}
	
	@NotifyChange("*")
	@Command
	public void limpiarEvento() {
		this.setEventoSeleccionado(new Evento());
	}
	
	@Command
	@NotifyChange("*")
	public void agregarAdjunto(
			@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {

		if (this.getEventoSeleccionado().getAdjuntoEventos().size() <= 4) {
			Media media = event.getMedia();
			if (media != null) {
				if (media instanceof org.zkoss.image.Image) {
					this.getAdjuntoEvento().setArchivo(new Archivo());
					this.getAdjuntoEvento().getArchivo()
							.setNombreArchivo(media.getName());
					this.getAdjuntoEvento().getArchivo()
							.setTipo(media.getContentType());
					this.getAdjuntoEvento().getArchivo()
							.setContenido(media.getByteData());
					this.getAdjuntoEvento().setEvento(
							this.getEventoSeleccionado());

					if (this.getEventoSeleccionado().getAdjuntoEventos() != null) {
						this.getEventoSeleccionado().getAdjuntoEventos()
								.add(getAdjuntoEvento());
					} else {
						this.getEventoSeleccionado().setAdjuntoEventos(
								new HashSet<AdjuntoEvento>());
						this.getEventoSeleccionado().getAdjuntoEventos()
								.add(getAdjuntoEvento());
					}
					this.setAdjuntoEvento(new AdjuntoEvento());
				} else {
					Messagebox.show("El archivo: " + media.getName()
							+ " no es un archivo valido", "Error",
							Messagebox.OK, Messagebox.ERROR);
				}
			}
		} else {
			Messagebox
					.show("Disculpe solo se permiten cargar maximo 4 archivos por Evento ",
							"Error", Messagebox.OK, Messagebox.ERROR);

		}
	}
	
	@Command
	@NotifyChange({ "eventoSeleccionado" })
	public void eliminarAdjuntoEvento(
			@BindingParam("adjuntoSeleccionado") AdjuntoEvento adjuntoSeleccionado) {
//		this.servicioAdjuntoEvento.Eliminar(adjuntoSeleccionado.getId());
		this.getEventoSeleccionado().getAdjuntoEventos().remove(adjuntoSeleccionado);
		Messagebox.show("Datos eliminados correctamente", "Informacion",
				Messagebox.OK, Messagebox.INFORMATION);
	}
	
	@Command
	@NotifyChange({ "eventoSeleccionado" })
	public void eliminarVariosAdjuntos() {
		if (!this.getListaAdjuntosSeleccionados().isEmpty()) {
			this.getEventoSeleccionado().getAdjuntoEventos().removeAll((this.getListaAdjuntosSeleccionados()));
//			this.servicioAdjuntoEvento.EliminarVarios((Set<AdjuntoEvento>) listaAdjuntosSeleccionados);
			Messagebox.show("Datos eliminados correctamente", "Informacion",
					Messagebox.OK, Messagebox.INFORMATION);
		} else {
			Messagebox.show("No hay datos seleccionados", "Informacion",
					Messagebox.OK, Messagebox.INFORMATION);
		}
	}
	
	@Command
	public void VerAdjuntoEvento(@BindingParam("imagen") AdjuntoEvento adjunto) {
		 Map<String, AdjuntoEvento> mapa = new HashMap<String, AdjuntoEvento>();
		 mapa.put("adjuntoVer", adjunto);
		window = (Window) Executions.createComponents("/vistas/gestionevento/verAdjuntoEvento.zul", null, mapa);
		window.doOverlapped(); 
	}
	
	public Evento getEventoSeleccionado() {
		if (this.eventoSeleccionado == null) {
			this.eventoSeleccionado = new Evento();
		}
		return eventoSeleccionado;
	}

	public void setEventoSeleccionado(Evento eventoSeleccionado) {
		this.eventoSeleccionado = eventoSeleccionado;
	}
	
	public AdjuntoEvento getAdjuntoEvento() {
		if (adjuntoEvento == null) {
			adjuntoEvento = new AdjuntoEvento();
		}
		return adjuntoEvento;
	}

	public void setAdjuntoEvento(AdjuntoEvento adjuntoEvento) {
		this.adjuntoEvento = adjuntoEvento;
	}

	public List<AdjuntoEvento> getListaAdjuntosSeleccionados() {
		if(listaAdjuntosSeleccionados == null){
			listaAdjuntosSeleccionados = new ArrayList<AdjuntoEvento>();
		}
		return listaAdjuntosSeleccionados;
	}

	public void setListaAdjuntosSeleccionados(
			List<AdjuntoEvento> listaAdjuntosSeleccionados) {
		this.listaAdjuntosSeleccionados = listaAdjuntosSeleccionados;
	}

}
