package edu.ucla.siged.viewmodel.seguridad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.util.Date;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;






import edu.ucla.siged.domain.seguridad.Rol;
import edu.ucla.siged.domain.seguridad.RolUsuario;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRol;
import edu.ucla.siged.servicio.impl.seguridad.ServicioRolUsuario;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioRolUsuarioI;

public class VMListarRoles {
	@WireVariable
	private ServicioRolI servicioRol;
	@WireVariable
	private ServicioRolUsuarioI servicioRolUsuario;
	@WireVariable
	Rol rol;

	List<Rol> listaRoles;
	
	Rol rolSeleccionado;
	
	List<Rol> rolesSeleccionados;
	
	private Window window;
	short estatusCombo=0;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	boolean estatusFiltro;
	String fechaRegistro;
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	


	String nombre;

	public VMListarRoles() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Init
	public void init() {
		//listaRoles();
		nombre="";
		fechaRegistro="";
		estatusCombo=0;
		this.listaRoles=servicioRol.buscarTodos(0).getContent();
		this.registrosTotales= servicioRol.totalRoles();
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public List<Rol> getListaRoles() {
		return listaRoles;
	}

	public void setListaRoles(List<Rol> listaRoles) {
		this.listaRoles = listaRoles;
	}

	public Rol getRolSeleccionado() {
		return rolSeleccionado;
	}

	public void setRolSeleccionado(Rol rolSeleccionado) {
		this.rolSeleccionado = rolSeleccionado;
	}
	
	public void listaRoles(){
		listaRoles = servicioRol.buscarTodos();	
	}
	
	
	public short getEstatusCombo() {
		return estatusCombo;
	}

	public void setEstatusCombo(short estatusCombo) {
		this.estatusCombo = estatusCombo;
	}

	public int getPaginaActual() {
		return paginaActual;
	}

	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}

	public int getTamanoPagina() {
		this.tamanoPagina=ServicioRol.TAMANO_PAGINA;
		return tamanoPagina;
	}

	public void setTamanoPagina(int tamanoPagina) {
		this.tamanoPagina = tamanoPagina;
	}

	public long getRegistrosTotales() {
		return registrosTotales;
	}

	public void setRegistrosTotales(long registrosTotales) {
		this.registrosTotales = registrosTotales;
	}

	public boolean isEstatusFiltro() {
		return estatusFiltro;
	}

	public void setEstatusFiltro(boolean estatusFiltro) {
		this.estatusFiltro = estatusFiltro;
	}	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public List<Rol> getRolesSeleccionados() {
		return rolesSeleccionados;
	}

	public void setRolesSeleccionados(List<Rol> rolesSeleccionados) {
		this.rolesSeleccionados = rolesSeleccionados;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}


	@Command
	@NotifyChange({"listaRoles"})
	public String verStatusEnLista(Short estatusEnLista){
		String est="Activo";
		System.out.println("estatus:"+estatusEnLista.toString());
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	
	@Command
	@NotifyChange("listaRoles")
	public void agregarRol(){
		window = (Window)Executions.createComponents("/vistas/seguridad/rol.zul", null, null);
		window.doModal();
	}
	
	@Command
	@NotifyChange("listaRoles")
	public void editarRol(@BindingParam("rolSeleccionado") Rol rolSeleccionado){
		Map<String,Rol> mapRol = new HashMap<String, Rol>();
		mapRol.put("rolSeleccionado", rolSeleccionado);
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/seguridad/rol.zul", null, mapRol);
		window.doModal();	
	
	
	}
	
	@Command
	@NotifyChange("listaRoles")
	public void eliminarRol(@BindingParam("rolSeleccionado") Rol rolSeleccionado){
		try{
			servicioRol.eliminar(rolSeleccionado);
			actualizarLista(rolSeleccionado);
		}
		catch(Exception e){
			Messagebox.show("No se ha podido eliminar el rol!", "Error", Messagebox.OK, Messagebox.ERROR);
			
		}
		
		//cambiarEstatusRolesUsuario();
	}
	
	@Command
	@NotifyChange("listaRoles")
	public void eliminarRoles(){
		System.out.println("Entro en eliminar 1");
		try {
			servicioRol.eliminarVarios(rolesSeleccionados);
			actualizarLista(rolesSeleccionados.get(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//cambiarEstatusRolesUsuario();
		
	}
	
	@GlobalCommand
	@NotifyChange({"listaRoles","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevoRol") Rol nuevoRol){
		cambiarEstatusRolesUsuario();
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina)-1;
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina; //se coloca en la ultima pagina, para que quede visible el que se acaba de ingresar
		} 
		paginar();
	}
	
	@GlobalCommand
	@NotifyChange({"listaRoles","registrosTotales","paginaActual","rolesSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaRoles = servicioRol.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioRol.totalRoles();
		
		}else{
			
			ejecutarFiltro();
		}

	}
	
	@Command
	@NotifyChange({"listaRoles","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		System.out.println("ESTATUS SELECCIONADO"+estatusCombo);
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		List<String> condiciones = new ArrayList<String>();
		
		if(!nombre.trim().isEmpty())	
		condiciones.add(" nombre like '%"+nombre+"%' ");
		if(!fechaRegistro.trim().isEmpty())
		condiciones.add(" to_char(fechaRegistro, 'DD/MM/YYYY') like'%"+fechaRegistro+"%' ");
		if(!filtroEstatus.isEmpty())
		condiciones.add(" estatus in "+filtroEstatus);
		
		
		jpql="";	
		for(int i=0; i<condiciones.size(); i++){
			jpql+= condiciones.get(i);
			if(i+1<condiciones.size())
			jpql+= " and ";
		}
		
		System.out.println("----------------------------------"+jpql.toString());
		this.listaRoles=servicioRol.buscarFiltrado(jpql,this.paginaActual);
		
		this.registrosTotales=servicioRol.totalRolesFiltrados(jpql);
	}
	
	@Command
	@NotifyChange({"listaRoles","registrosTotales","paginaActual","rolesSeleccionados",
				   "nombre","fechaRegistro", "estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		nombre="";
		fechaRegistro="";
		estatusCombo=0;
	}
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		if(fecha!=null){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
		}
		return "-";
	}
	
	@Command
	@NotifyChange("listaRoles")
	public void cambiarEstatusRolesUsuario() {
		System.out.println("Entro en cambiar estatus de roles");
		for(Rol rolt: this.listaRoles){
		List<RolUsuario> lista= servicioRolUsuario.getRolesUsuarioByRol(rolt);
		if(lista.size()>0){
			cambiarEstatusRolUsuario(lista, rolt.getEstatus());
			servicioRolUsuario.guardarListaRolUsuario(lista);
			}
		}
	}
	
	public void cambiarEstatusRolUsuario(List<RolUsuario> usuariosRol, short estatus) {
		for(RolUsuario ur: usuariosRol)
		{
			if(ur.getFechaEliminacion()==null){
			ur.setEstatus(estatus);
			System.out.println("usuario:"+ur.getUsuario().getCedula());
			System.out.println("rol"+ur.getRol().getNombre());
			}
		}
	}
}