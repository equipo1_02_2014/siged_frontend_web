package edu.ucla.siged.viewmodel.gestiondeportiva;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.domain.gestiondeportiva.AsistenciaAtleta;
import edu.ucla.siged.domain.gestiondeportiva.Practica;
import edu.ucla.siged.servicio.interfaz.gestiondeportiva.ServicioPracticaI;

public class VmControlPractica {
	
	Practica practica;
	@WireVariable
	ServicioPracticaI servicioPractica;
	List<Causa> causas;
	
	public Practica getPractica() {
		return practica;
	}
	public void setPractica(Practica practica) {
		this.practica = practica;
	}
	
	public List<Causa> getCausas() {
		return causas;
	}
	public void setCausas(List<Causa> causas) {
		this.causas = causas;
	}
	@Init
	public void init(@ExecutionArgParam("id") Integer idPractica){
		
		this.practica = servicioPractica.buscarPorId(idPractica);
		causas = servicioPractica.buscarCausasInasistencia();
	}
	
	@Command
	public void checar(@BindingParam("asistencia") AsistenciaAtleta asistencia){
		if (asistencia.getAsistencia()==false){
			asistencia.setCausa(null);
		}
		
	}
	
	
	@Command
	public void guardar(){
		servicioPractica.guardar(practica,3);	//3 porque es modificar pero para evaluar
		Messagebox.show(
                "Control y seguimiento guardado exitosamente","", Messagebox.OK, Messagebox.INFORMATION
            );
		//para cerrar la ventana despues que guarda
		Window myWindow = (Window) Path.getComponent("/vistaControl");
		myWindow.detach();
	}
	
	@Command
	@NotifyChange("practica")
	public void cancelar(){
		init(practica.getId());
	}
	
	public String concatenar(String cadena1, String cadena2){
		return cadena1+" "+cadena2;
	}
	
	public String formatearHora(Date fecha){
		String fechaFormateada= new SimpleDateFormat("hh:mm a").format(fecha);
		return fechaFormateada;
	}
	
	public String formatearFecha(Date fecha){
		String fechaFormateada= new SimpleDateFormat("dd/MM/yyyy").format(fecha);
		return fechaFormateada;
	}

}
