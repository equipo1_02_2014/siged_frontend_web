package edu.ucla.siged.viewmodel.reportes;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

import edu.ucla.siged.domain.reporte.Atributo;
import edu.ucla.siged.domain.reporte.Entidad;
import edu.ucla.siged.domain.reporte.Generica;
import edu.ucla.siged.domain.reporte.Reporte;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioEntidadI;
import edu.ucla.siged.servicio.interfaz.reportes.ServicioGenericoI;

public class ReporteListasVm {
	@WireVariable
	ServicioGenericoI servicioGenerico;
	
	List<Entidad> listaEntidades;
	List<Atributo> listaAtributos;
	Entidad entidad;
	Atributo atributoAFiltrar;
	@WireVariable
	Reporte reporte;
	@WireVariable
	ServicioEntidadI servicioEntidad;
	String hql="";
	List<String> columnas;
	List<String> operadores;
	String operadorSeleccionado;
	String valor;
	String valueEntidad="";
	String valueCriterio="";
	String pathProyecto;
	public class Criterio{
		Atributo columna;
		String operador;
		String valor;
		public Criterio(Atributo columna, String operador, String valor) {
			super();
			this.columna = columna;
			this.operador = operador;
			this.valor = valor;
		}
		public Atributo getColumna() {
			return columna;
		}
		public String getOperador() {
			return operador;
		}
		public String getValor() {
			return valor;
		}
		public void setColumna(Atributo columna) {
			this.columna = columna;
		}
		public void setOperador(String operador) {
			this.operador = operador;
		}
		public void setValor(String valor) {
			this.valor = valor;
		}
		
	}
	List<Criterio> listaCriterios;

	public List<Entidad> getListaEntidades() {
		return listaEntidades;
	}
	public void setListaEntidades(List<Entidad> listaEntidades) {
		this.listaEntidades = listaEntidades;
	}
	
	public List<Atributo> getListaAtributos() {
		return listaAtributos;
	}
	public void setListaAtributos(List<Atributo> listaAtributos) {
		this.listaAtributos = listaAtributos;
	}
	public Entidad getEntidad() {
		return entidad;
	}
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}
	public Atributo getAtributoAFiltrar() {
		return atributoAFiltrar;
	}
	public void setAtributoAFiltrar(Atributo atributoAFiltrar) {
		this.atributoAFiltrar = atributoAFiltrar;
	}
	public Reporte getReporte() {
		return reporte;
	}
	public void setReporte(Reporte reporte) {
		this.reporte = reporte;
	}
	public List<String> getOperadores() {
		return operadores;
	}
	public void setOperadores(List<String> operadores) {
		this.operadores = operadores;
	}
	public String getOperadorSeleccionado() {
		return operadorSeleccionado;
	}
	public void setOperadorSeleccionado(String operadorSeleccionado) {
		this.operadorSeleccionado = operadorSeleccionado;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public String getValueEntidad() {
		return valueEntidad;
	}
	public void setValueEntidad(String valueEntidad) {
		this.valueEntidad = valueEntidad;
	}
	public String getValueCriterio() {
		return valueCriterio;
	}
	public void setValueCriterio(String valueCriterio) {
		this.valueCriterio = valueCriterio;
	}
	public List<Criterio> getListaCriterios() {
		return listaCriterios;
	}
	public void setListaCriterios(List<Criterio> listaCriterios) {
		this.listaCriterios = listaCriterios;
	}
	@Init
	public void init(){
		listaEntidades = servicioEntidad.buscarTodas();
		entidad =new Entidad();
		reporte.inicializarLista();
		operadores = new ArrayList<String>();
		operadores.add("=");
		operadores.add(">");
		operadores.add(">=");
		operadores.add("<");
		operadores.add("<=");
		listaCriterios = new ArrayList<Criterio>();
		reporte.setTipo("pdf");
		valueEntidad  = "Seleccione";
		valueCriterio = "Seleccione";
		
		File file= new File(VMCumplimientoActividades.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		
		this.pathProyecto= file.getParentFile().getParentFile().getParentFile().getParentFile().
		                        getParentFile().getParentFile().getParentFile().getParentFile().getPath();
		
		this.pathProyecto= this.pathProyecto.replaceAll("%20", " ");
	}
	
	@Command
	@NotifyChange({"listaAtributos","listaCriterios","valueCriterio","valor","operadorSeleccionado"})
	public void buscarAtributos(){
		listaAtributos=servicioEntidad.buscarAtributos(entidad);
		listaCriterios.clear();
		valueCriterio = "Seleccione";
		valor="";
		operadorSeleccionado="Seleccione";
	}
	
	@Command
	@NotifyChange({"valueCriterio","listaCriterios"})
	public void retirarAtributo(@BindingParam("atributo")Atributo atributo){
		valueCriterio = "Seleccione";
		for (int i=0; i<listaCriterios.size();i++){
			if (listaCriterios.get(i).getColumna().equals(atributo)){
				listaCriterios.remove(i);
			}
		}
	}
	
	@Command
	@NotifyChange({"listaCriterios","valueCriterio","valor","operadorSeleccionado"})
	public void agregarCriterio(){
		listaCriterios.add(new Criterio(atributoAFiltrar,operadorSeleccionado,valor));
		valueCriterio = "Seleccione";
		valor="";
		operadorSeleccionado="Seleccione";
	}
	
	@Command
	@NotifyChange({"listaCriterios"})
	public void eliminarCriterio(@BindingParam("criterio")Criterio criterio){
		listaCriterios.remove(criterio);
	}
	
	@Command
	@NotifyChange({"listaCriterios","entidad","operadores","valueEntidad","valueCriterio","listaAtributos"})
	public void limpiar(){
		init();
		listaAtributos.clear();
	}
	
	
	private void construirQuery(){
		hql= "select new Generica(";
		columnas=new ArrayList<String>();
		
		int cantidadColumnas=listaAtributos.size();
		for (int i=0; i< cantidadColumnas;i++){
			if (listaAtributos.get(i).isMostrar()){
			   hql= hql+listaAtributos.get(i).getNombreFisico()+",";
			   columnas.add(listaAtributos.get(i).getNombreAMostrar());
			}
		}
		hql = hql.substring(0, hql.length()-1); //elimina la ultima coma
		hql= hql+ ") from " +entidad.getNombreFisico(); 
		/*Messagebox.show(
                hql,"", Messagebox.OK, Messagebox.INFORMATION
            );*/
	
	
	}
	
	private void construirRestricciones(){
		if (listaCriterios.size() > 0){
		   String restriccion =" where ";
		   String campo="";
		   for (int i=0;i< listaCriterios.size();i++){
			  if (listaCriterios.get(i).getColumna().getTipo()!=1){		//los q no son numero van entre comillas
				  campo= "'"+listaCriterios.get(i).getValor()+"'";
			  }else{
				  campo = listaCriterios.get(i).getValor();
			  }
			  restriccion = restriccion + listaCriterios.get(i).getColumna().getNombreFisico() +" "+ listaCriterios.get(i).getOperador()+" "+campo;
			  restriccion = restriccion +" and ";
		   }
		   restriccion = restriccion +" 1=1 ";
		   hql= hql + restriccion;
		}
	}
	
	@Command
	public void imprimir(){
		if (listaAtributos.size()>0){
		   construirQuery();
		   construirRestricciones();
	
		   //  Executions.getCurrent().sendRedirect("/generico?tipo="+reporte.getTipo()+"&query="+hql+"&columnas="+columnas,"New Window");
		
		     Map<String,Object> parameterMap = new HashMap<String,Object>();
		     System.out.println("*****************************************"+hql);
				List<Generica> lista = servicioGenerico.buscarTodos(hql);
				JRDataSource JRdataSource = new JRBeanCollectionDataSource(lista);
		        //Parametros
				for (int i=0; i < columnas.size();i++){
					parameterMap.put("columna"+(i+1),columnas.get(i));
				}
		        parameterMap.put("datasource", JRdataSource);
		        parameterMap.put("format", reporte.getTipo());
		        parameterMap.put("titulo", entidad.getNombreAMostrar()); //titulo es el ultimo elemento de la lista columna
		        parameterMap.put("encabezado1", "Republica Bolivariana de Venezuela");
		        parameterMap.put("encabezado2", "Fundacion Luis Sojo");
		        parameterMap.put("encabezado3", "Cabudare - Lara");
		        parameterMap.put("rutaLogo", "http://localhost:8080/Siged/source/images/logo_fundacion_luis_sojo.png");
		        parameterMap.put("registrosTotales",lista.size());
		        
		        try {			
					
					JasperDesign jasDesign=null;
					
					jasDesign = JRXmlLoader.load(this.pathProyecto + System.getProperty("file.separator") +"reportes" + System.getProperty("file.separator") + "listaGenerica.jrxml");
					
					
					JasperReport jasReport = JasperCompileManager.compileReport(jasDesign);
			        
			        JasperPrint jasperPrint= JasperFillManager.fillReport(jasReport, parameterMap,JRdataSource); 
			      
			        JasperViewer jv = new JasperViewer(jasperPrint,false);
			        jv.setTitle(entidad.getNombreAMostrar());
			        jv.setVisible(true);
			        //JasperViewer.viewReport(jasperPrint,false);
					
				} catch (JRException e) {
					e.printStackTrace();
					Messagebox.show(e.getMessage());
				}
		}
	}
	
	
}

