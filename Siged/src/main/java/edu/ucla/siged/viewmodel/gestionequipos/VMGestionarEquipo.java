package edu.ucla.siged.viewmodel.gestionequipos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.ext.Selectable;

import edu.ucla.siged.domain.gestionatleta.Atleta;
import edu.ucla.siged.domain.gestionequipo.AtletaEquipo;
import edu.ucla.siged.domain.gestionequipo.Categoria;
import edu.ucla.siged.domain.gestionequipo.Equipo;
import edu.ucla.siged.domain.gestionequipo.Rango;
import edu.ucla.siged.domain.gestionequipo.TipoEquipo;
import edu.ucla.siged.servicio.impl.gestionequipos.ServicioAtletaEquipo;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioCategoriaI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioEquipoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioRangoI;
import edu.ucla.siged.servicio.interfaz.gestionequipos.ServicioTipoEquipoI;

public class VMGestionarEquipo {
	
	@WireVariable ServicioEquipoI	 	servicioEquipo;
	@WireVariable ServicioCategoriaI 	servicioCategoria;
	@WireVariable ServicioTipoEquipoI 	servicioTipoEquipo;
	@WireVariable ServicioRangoI		servicioRango;
	@WireVariable ServicioAtletaI 		servicioAtleta;
	@WireVariable ServicioAtletaEquipo	servicioAtletaEquipo;

	@WireVariable Categoria categoriaSeleccionada;
	@WireVariable TipoEquipo tipoEquipoSeleccionado;
	@WireVariable Rango rangoSeleccionado;
	
	@Wire private Listbox ltbAtletasDisponibles;
	@Wire private Listbox ltbAtletasAsignados;
	

	Equipo	equipo;
	
	List<Categoria> 	listaCategoria;
	List<TipoEquipo> 	listaTipoEquipo;
	List<Rango> 		listaRango;
	List<Integer>		listaEstatus;
//	List<Atleta>		listaAtleta;	
//	List<Atleta>		atletaValido;
	List<AtletaEquipo>	listaAtletaEquipos;
	
	Set<Atleta>			selection1;
	List<Atleta>		atletasDisponibles;
	Set<Atleta>			selection2;
	List<Atleta>		atletasAsignados;
	
	Integer estatusSeleccionado;
	
	

// --------------------------------------------------------------------------------------------------------- Inicio de M�todos Getters and Setters	
	public TipoEquipo getTipoEquipoSeleccionado() { return tipoEquipoSeleccionado; }
	public void setTipoEquipoSeleccionado(TipoEquipo tipoEquipoSeleccionado) { this.tipoEquipoSeleccionado = tipoEquipoSeleccionado; }
	
	public Rango getRangoSeleccionado() { return rangoSeleccionado; }
	public void setRangoSeleccionado(Rango rangoSeleccionado) { this.rangoSeleccionado = rangoSeleccionado; }
	
	public Equipo getEquipo() { return equipo; }
	public void setEquipo(Equipo equipo) { this.equipo = equipo; }
	
	public Categoria getCategoriaSeleccionada() { return categoriaSeleccionada; }
	public void setCategoriaSeleccionada(Categoria categoriaSeleccionada) { this.categoriaSeleccionada = categoriaSeleccionada; }
	
	public List<Categoria> getListaCategoria() { return listaCategoria; }
	public void setListaCategoria(List<Categoria> listaCategoria) { this.listaCategoria = listaCategoria; }
	
	public List<TipoEquipo> getListaTipoEquipo() { return listaTipoEquipo; }
	public void setListaTipoEquipo(List<TipoEquipo> listaTipoEquipo) { this.listaTipoEquipo = listaTipoEquipo; }
	
	public List<Rango> getListaRango() { return listaRango; }
	public void setListaRango(List<Rango> listaRango) { this.listaRango = listaRango; }
	
	public List<Integer> getListaEstatus() { return listaEstatus; }
	public void setListaEstatus(List<Integer> listaEstatus) { this.listaEstatus = listaEstatus; }
	
//	public List<Atleta> getListaAtleta() { return listaAtleta; }
//	public void setListaAtleta(List<Atleta> listaAtleta) { this.listaAtleta = listaAtleta; }
	
	public List<Atleta> getAtletasDisponibles() { return atletasDisponibles; }
	public void setAtletasDisponibles(List<Atleta> atletasDisponibles) { this.atletasDisponibles = atletasDisponibles; }
	
//	public List<Atleta> getAtletaValido() {	return atletaValido; }
//	public void setAtletaValido(List<Atleta> atletaValido) { this.atletaValido = atletaValido; }
	
	public List<AtletaEquipo> getListaAtletaEquipos() { return listaAtletaEquipos; }
	public void setListaAtletaEquipos(List<AtletaEquipo> listaAtletaEquipos) { this.listaAtletaEquipos = listaAtletaEquipos; }
		
	public List<Atleta> getAtletasAsignados() {	return atletasAsignados; }
	public void setAtletasAsignados(List<Atleta> atletasAsignados) { this.atletasAsignados = atletasAsignados; }
	
	public Integer getEstatusSeleccionado() { return estatusSeleccionado; }
	public void setEstatusSeleccionado(Integer estatusSeleccionado) { this.estatusSeleccionado = estatusSeleccionado; }
	
	public Set<Atleta> getSelection1() { return selection1; }
	public void setSelection1(Set<Atleta> selection1) { this.selection1 = selection1; }
	
	public Set<Atleta> getSelection2() { return selection2; }
	public void setSelection2(Set<Atleta> selection2) { this.selection2 = selection2; }
	
// --------------------------------------------------------------------------------------------------------- Fin de M�todos Getters and Setters
	
	

	@Init
	public void inicializar(@ExecutionArgParam("equipo") Equipo equipoSeleccionado)
	{

		selection1 = new HashSet<Atleta>();
		selection2 = new HashSet<Atleta>();
		
		listaCategoria = servicioCategoria.buscarTodos();
		listaTipoEquipo = servicioTipoEquipo.buscarTodos();
		listaRango = servicioRango.buscarTodos();
		//listaAtleta = servicioAtleta.obtenerListaAtleta(0);		
		listaEstatus = new ArrayList<Integer>();
			listaEstatus.add(0);
			listaEstatus.add(1);		
		
		Object objEquipoSeleccionado = Sessions.getCurrent().getAttribute("equiposeleccionado");
		if ( objEquipoSeleccionado != null){
        	this.equipo = (Equipo) ( Sessions.getCurrent().getAttribute("equiposeleccionado") );
				// convirtiendo de short a Integer
				int estatusInt = equipo.getEstatus();
				Integer estatusI = (Integer) estatusInt;
				this.setEstatusSeleccionado(estatusI);
				
				// cargo Lista de Atletas y Equipos
				atletasDisponibles = servicioAtleta.obtenerAtletasValidos(equipo.getCategoria());
				atletasAsignados = servicioEquipo.obtenerAtletasPorEquipo(equipo);				
		}else{
			this.equipo = new Equipo();
		}
	}
	
	@Command
	@NotifyChange({"equipo", "atletasAsignados"})
	public void guardarEquipo(){	
		if(categoriaSeleccionada != null){ equipo.setCategoria(categoriaSeleccionada); }
		if(rangoSeleccionado != null){ equipo.setRango(rangoSeleccionado); }
		if(tipoEquipoSeleccionado != null){ equipo.setTipoEquipo(tipoEquipoSeleccionado); }
		// convirtiendo el estatus a short para asignarlo
		if( this.estatusSeleccionado != null ){
			int estatusInt = (int) estatusSeleccionado;
			short estatusShort = (short) estatusInt;
			equipo.setEstatus(estatusShort);
		}

		Date fechaActual = new Date();
		if( this.atletasAsignados != null ){
			for( Atleta atleta:atletasAsignados){				
				AtletaEquipo atletaEquipo = new AtletaEquipo(atleta, fechaActual, this.equipo);
				servicioAtletaEquipo.guardarAtletaEquipo(atletaEquipo);
				Messagebox.show( "Guardo","", Messagebox.OK, Messagebox.INFORMATION );
			}
		}else{
			Messagebox.show( "No Guardo","", Messagebox.OK, Messagebox.INFORMATION );
		}
		
		servicioEquipo.guardarEquipo(equipo);
		//Messagebox.show( "Equipo: " +equipo.getNombre() + " Guardado con �xito!!!","", Messagebox.OK, Messagebox.INFORMATION );
	}
		
	@Command
	@NotifyChange({"equipo"})
	public void limpiarEquipo(){
		equipo.setNombre("");
		equipo.getTipoEquipo().setDescripcion("");
		equipo.getCategoria().setNombre(""); 
		equipo.getRango().setDescripcion("");
		equipo.setCuposTotales(0);
		equipo.setFechaCreacion(null);
		equipo.setFechaCierre(null);
		equipo.setEstatus(new Short("0"));
	}
		
	@Command
	public String verEstatus( Integer estatus ){
		String est="Activo";
		switch (estatus){
		case 0:
			est="Inactivo";
		    break;
		default:
			est="Activo";
		}
		return est;
	}	
	

	
	@Command
	@NotifyChange({"atletasDisponibles","atletasAsignados", "selection1", "selection2"})
	public void asignarAtleta(){
		if( selection1 != null && selection1.size() >0 ){
			atletasAsignados.addAll(selection1);
			atletasDisponibles.removeAll(selection1);
			selection2.addAll(selection1);
			selection1.clear();
			Messagebox.show( (atletasAsignados.isEmpty() ?"vacia":"llena"),"", Messagebox.OK, Messagebox.INFORMATION );
//			System.out.println("------------------------------" + (atletasAsignados.isEmpty() ?"vacia":"llena"));
		}
		else
		{
			Messagebox.show( "Debe Seleccionar un Atleta para asignar!","", Messagebox.OK, Messagebox.ERROR );
		}
	}
	
	
	@Command
	@NotifyChange({"atletasDisponibles","atletasAsignados", "selection1", "selection2"})
	public void removerAtleta(){
		if(selection2 != null && selection2.size() >0 ){
			atletasDisponibles.addAll(selection2);
			atletasAsignados.removeAll(selection2);
			selection1.addAll(selection2);
			selection2.clear();				
		}else{
			Messagebox.show( "Debe Seleccionar un Atleta para removerlo!","", Messagebox.OK, Messagebox.ERROR );
		}				
	}

}
