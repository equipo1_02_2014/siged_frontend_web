package edu.ucla.siged.viewmodel.gestionrecursostecnicos;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import edu.ucla.siged.domain.gestionrecursostecnicios.Tecnico;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;


public class VMListaTecnico {
	@WireVariable
	ServicioTecnicoI servicioTecnico;
	@WireVariable
	Tecnico tecnico;
	List<Tecnico> listaTecnicos;
	List<Tecnico> tecnicosSeleccionados;
	Window window;
	int paginaActual;
	int tamanoPagina;
	long registrosTotales; 
	short tipoOperacion;  // 1: el formulario se llamo para incluir, 2:para editar.
	// las variables siguientes son del filtro
	String cedula=""; 
	String nombre="";
	Date fechaAdmision;
	short estatusCombo=0;
	boolean estatusFiltro;
	private AImage imagenT;
	
	//////////////////////////////////////////METODOS GET Y SET///////////////////////////////////////////////////////
	
	public Tecnico getTecnico() {
		return tecnico;
	}
	
	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}
	
	public List<Tecnico> getTecnicosSeleccionados() {
		return tecnicosSeleccionados;
	}
	
	public void setTecnicosSeleccionados(List<Tecnico> tecnicosSeleccionados) {
		this.tecnicosSeleccionados = tecnicosSeleccionados;
	}
	
	public List<Tecnico> getListaTecnicos() {
		return listaTecnicos;
	}
	
	public void setListaTecnico(List<Tecnico> listaTecnicos) {
		this.listaTecnicos = listaTecnicos;
	}
	
	public int getPaginaActual() {
		return paginaActual;
	}
	
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	
	public int getTamanoPagina() {
		this.tamanoPagina=servicioTecnico.TAMANO_PAGINA;
		return tamanoPagina;
	}
	
	public long getRegistrosTotales() {
		return registrosTotales;
	}
	
	public String getCedula() {
		return cedula;
	}
	
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Date getFechaAdmision() {
		return fechaAdmision;
	}
	
	public void setFechaAdmision(Date fechaAdmision) {
		this.fechaAdmision = fechaAdmision;
	}
	
	public short getEstatusCombo() {
		return estatusCombo;
	}
	
	public void setEstatusCombo(short estatus) {
		this.estatusCombo = estatus;
	}
	
	public AImage getImagenT() {
		return imagenT;
	}
	
	public void setImagenT(AImage imagenT) {
		this.imagenT = imagenT;
	}
	
	////////////////////////////////////////////////INIT///////////////////////////////////////////////////////////
	
	@Init
	public void init(){
		try{
			this.listaTecnicos = servicioTecnico.buscarTodos(0).getContent();
			this.registrosTotales= servicioTecnico.totalTecnicos();
		}catch(Exception e){
			
		}
	}
	
	////////////////////////////////////////AGREGAR TECNICO///////////////////////////////////////////////////////////
	
	@Command
	public void agregarTecnico() {
		this.tipoOperacion=1;
		window = (Window)Executions.createComponents("/vistas/gestionrecursotecnico/tecnico.zul", null, null);
		window.doModal();
	}
	
	/////////////////////////////////////////ELIMINAR TECNICO////////////////////////////////////////////////////
	
	@Command
	@NotifyChange("listaTecnicos")
	public void eliminarTecnico(@BindingParam("tecnicoSeleccionado") Tecnico tecnicoSeleccionado){
		if(servicioTecnico.verificarEliminar(tecnicoSeleccionado)){
			Messagebox.show("El tecnico esta asociado a otros registros y no puede ser eliminado","", Messagebox.OK, Messagebox.ERROR);
		}
		else{
			servicioTecnico.eliminar(tecnicoSeleccionado);
			Messagebox.show( "El tecnico ha sido eliminado de manera exitosa","", Messagebox.OK, Messagebox.INFORMATION);
			paginar();
		}	
	}

	///////////////////////////////////////////ELIMINAR VARIOS TECNICOS//////////////////////////////////////////////
	
	@Command
	@NotifyChange("listaTecnicos")
	public void eliminarTecnicos(){
		System.out.println(tecnico.getNombre());
		boolean ocupados=false;
		for (Tecnico tecnico:tecnicosSeleccionados){
		  if(servicioTecnico.verificarEliminar(tecnico)){
			ocupados=true;
				Messagebox.show( "El tecnico " + tecnico.getNombre() + " " + tecnico.getApellido() +" esta asociado a otros registros y no puede ser eliminado","", Messagebox.OK, Messagebox.ERROR);
				break;
			}
		}
		if(!ocupados)
		   servicioTecnico.eliminarVarios(tecnicosSeleccionados);
		   tecnicosSeleccionados= null;
		    paginar();
	}
	
	/////////////////////////////////////////////EDITAR TECNICO/////////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"tecnico"})
	public void editarTecnico(@BindingParam("tecnicoSeleccionado") Tecnico tecnicoSeleccionado ) {
		
		this.tecnico=tecnicoSeleccionado;
		this.tipoOperacion=2;
		//esta es la manera de pasar a la otra vista el tecnico seleccionado
		Map<String, Tecnico> mapa = new HashMap<String, Tecnico>();
        mapa.put("tecnico", tecnicoSeleccionado);
		window = (Window)Executions.createComponents("/vistas/gestionrecursotecnico/tecnico.zul", null, mapa);
		window.doModal();
	}
	//////////////////////////////////////PAGINAR//////////////////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaTecnicos","registrosTotales","paginaActual","tecnicosSeleccionados"})
	public void paginar(){
		if (estatusFiltro==false){
		this.listaTecnicos = servicioTecnico.buscarTodos(this.paginaActual).getContent();
		this.registrosTotales=servicioTecnico.totalTecnicos();
		}else{
			ejecutarFiltro();
		}
	}
	
	////////////////////////////////////////ACTUALIZAR LISTA/////////////////////////////////////////////////////////
	
	@GlobalCommand
	@NotifyChange({"listaTecnicos","registrosTotales","paginaActual"})
	public void actualizarLista(@BindingParam("nuevoTecnico") Tecnico nuevoTecnico){
		if (this.tipoOperacion==1){
		   this.getRegistrosTotales();
		   int ultimaPagina=0;
		   if (this.registrosTotales%this.tamanoPagina==0)
		      ultimaPagina= ((int) this.registrosTotales/this.tamanoPagina);
		   else
		      ultimaPagina =((int) this.registrosTotales/this.tamanoPagina);	
		   this.paginaActual=ultimaPagina;
		} 
		paginar();
	}
	
	
	////////////////////////////////////////////// VER ESTATUS///////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaTecnicos"})
	public String verStatusEnLista(Integer estatusEnLista){
		String est="Activo";
		switch (estatusEnLista){
			case 0:
				est="Inactivo";
			    break;
			default:
				est="Activo";
		}
		return est;
	}
	////////////////////////////////////////////CAMBIAR FORMATO DE FECHA/////////////////////////////////////////////
	
	@Command
	public String cambiarFormatoFecha(Date fecha){
		DateFormat df= DateFormat.getDateInstance(DateFormat.MEDIUM);
		return df.format(fecha);
	}
	//////////////////////////////////////////////EJECUTAR FILTRO/////////////////////////////////////////////////////
	
	@Command
	@NotifyChange({"listaTecnicos","registrosTotales"})
	public void ejecutarFiltro(){
		String jpql;
		String filtroEstatus;
		this.estatusFiltro=true;  //el filtro se ha activado
		if(estatusCombo==0)
			filtroEstatus="(0,1)";
		else
			filtroEstatus="("+(estatusCombo-1)+")";
		
		if (fechaAdmision == null)
			jpql = " cedula like '%"+cedula+"%' and nombre like '%"+nombre+"%'   and estatus in "+filtroEstatus+"";
		else
		   jpql = " cedula like '%"+cedula+"%' and nombre like '%"+nombre+"%' and estatus in "+filtroEstatus+" and fechaAdmision = '"+fechaAdmision+"'";
			
		System.out.println("----------------------------------"+jpql);
		this.listaTecnicos=servicioTecnico.buscarFiltrado(jpql,this.paginaActual);
		this.registrosTotales=servicioTecnico.totalTecnicosFiltrados(jpql);
	}
	
/////////////////////////////////////////CANCELAR FILTRO///////////////////////////////////////////////////////////	
	
	@Command
	@NotifyChange({"listaTecnicos","registrosTotales","paginaActual","tecnicosSeleccionados",
				   "cedula", "nombre","fechaAdmision","estatusCombo"})
	public void cancelarFiltro(){
		this.estatusFiltro=false;
		paginar();
		cedula=""; //  variables del filtro
		nombre="";
		fechaAdmision=null;
		estatusCombo=0;
	}
}