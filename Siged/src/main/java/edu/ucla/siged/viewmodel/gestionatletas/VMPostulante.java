package edu.ucla.siged.viewmodel.gestionatletas;

import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import edu.ucla.siged.domain.gestionatleta.Causa;
import edu.ucla.siged.domain.gestionatleta.Postulante;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioCausaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioAtletaI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioPostulanteI;
import edu.ucla.siged.servicio.interfaz.gestionatletas.ServicioRepresentanteI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioAnotadorI;
import edu.ucla.siged.servicio.interfaz.gestionrecursostecnicos.ServicioTecnicoI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioUsuarioI;
import edu.ucla.siged.servicio.interfaz.seguridad.ServicioEmailsI;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

public class VMPostulante { 
		
		private @WireVariable ServicioAtletaI servicioAtleta;
		private @WireVariable ServicioUsuarioI servicioUsuario;
		private @WireVariable ServicioRepresentanteI servicioRepresentante;
		private @WireVariable ServicioTecnicoI servicioTecnico;
		private @WireVariable ServicioAnotadorI servicioAnotador;
		@WireVariable 
		ServicioPostulanteI servicioPostulante;
		@WireVariable 
		Postulante postulante;
		@WireVariable
		ServicioEmailsI servicioEmails;
		@WireVariable
		ServicioCausaI servicioCausa;
		@WireVariable
		private List<Causa> comboCausas;
		private Window ventanaPostulante;
		private Window ventanaResponderPostulante;
		
		public List<Causa> getComboCausas() {
			return comboCausas;
		}

		public void setComboCausas(List<Causa> comboCausas) {
			this.comboCausas = comboCausas;
		}
		
		public Postulante getPostulante() {
			return postulante;
		}

		public void setPostulante(Postulante postulante) {
			this.postulante = postulante;
		}

		@Init
	    public void init(@ContextParam(ContextType.VIEW) Component view, 
	    		@ExecutionArgParam("postulante") Postulante postulanteSeleccionado){
			Selectors.wireComponents(view, this, false);
			this.comboCausas = servicioCausa.listaCausasPost();
	        if (postulanteSeleccionado!=null)
	           this.postulante = postulanteSeleccionado;
	        else{
	        	postulante = new Postulante();
	        }
	    }
		

		@Command
		@NotifyChange({"postulante"})
		public void guardar(@BindingParam("ventanaPostulante") Window ventanaPostulante,@SelectorParam("textbox,datebox,intbox") LinkedList<InputElement> inputs){
			this.ventanaPostulante = ventanaPostulante;
			
			boolean camposValidos=true;
			for(InputElement input:inputs){
				if (!input.isValid()){
					camposValidos=false;
					break;
				}
				
			}
			if(!camposValidos){
				Messagebox.show("Campos no Validos", "ERROR", Messagebox.OK, Messagebox.ERROR);
			}else{
				if(postulante.getNombre()==null || postulante.getNombre().isEmpty() ||
				   postulante.getApellido()==null || postulante.getApellido().isEmpty() ||
				   postulante.getCedula()==null || postulante.getCedula().isEmpty() ||
				   postulante.getFechaNacimiento()==null || 
				   postulante.getLugarNacimiento()==null || postulante.getLugarNacimiento().isEmpty() ||
				   postulante.getDireccion()==null || postulante.getDireccion().isEmpty() ||
				   postulante.getTelefono()==null || postulante.getTelefono().isEmpty()||
				   postulante.getEmail()==null || postulante.getEmail().isEmpty()){
					
					Messagebox.show("Debe completar todos los campos para continuar.","", Messagebox.OK, Messagebox.EXCLAMATION);
					
				}
				else{

					if(!tipoDePersona(postulante.getCedula())){
						postulante.setEstatus(Short.parseShort("1"));
						postulante.setFechaPostulacion(new Date());
						servicioPostulante.guardar(postulante);
						Messagebox.show(postulante.getNombre()+" "+postulante.getApellido()+
								" Ha sido Postulado con Exito","",Messagebox.OK, Messagebox.INFORMATION);
						servicioEmails.enviarEmailSolicitudPostulacion(postulante.getEmail(), postulante.getNombre());
						limpiar();
						cerrarVentanaPostulante();
					}else{
						Messagebox.show("Esta cedula esta registrada en el sistema","", Messagebox.OK, Messagebox.EXCLAMATION);
					}

				}
			}
		}
		
		public boolean tipoDePersona(String cedulaUsuario){
//			Usuario 		0
//			Atleta			1
//			Representante	2
//			Tecnico			3
//			Anotador		4
			
			
			if(servicioAtleta.buscarAtletaPorCedula(cedulaUsuario)     != null || 
			   servicioRepresentante.buscarPorCedula(cedulaUsuario)    != null ||
			   servicioTecnico.buscarPorCedula(cedulaUsuario)		   != null ||
			   servicioAnotador.buscarAnotadorPorCedula(cedulaUsuario) != null ||
			   servicioUsuario.buscarPorCedula(cedulaUsuario)    != null){
				return true;
			}else{
				return false;
			}
		}

		
		
		
		public void cerrarVentanaPostulante(){
			ventanaPostulante.detach();
		}
		
		public void cerrarVentanaResponderPostulante(){
			ventanaResponderPostulante.detach();
		}
		
		@Command
		@NotifyChange({"postulante","paginar"})
		public void aprobarPostulante(@BindingParam("ventanaResponderPostulante") Window ventanaResponderPostulante){
			this.ventanaResponderPostulante = ventanaResponderPostulante;
			
			Messagebox.show("�Desea Aprobar al Postulante?","Pregunta",Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
					new EventListener<Event>(){
				public void onEvent(Event event) throws Exception{
					if(Messagebox.ON_YES.equals(event.getName())){
						postulante.setFechaRespuesta(new Date());
						postulante.setAprobacion(true);
						postulante.setEstatus(Short.parseShort("2"));
						servicioPostulante.guardar(postulante);
						Messagebox.show("Respuesta ha sido enviada a "+postulante.getNombre()+" "+postulante.getApellido(),
								"",Messagebox.OK,Messagebox.INFORMATION);
						servicioEmails.enviarEmailAprobarPostulante(postulante.getEmail(), postulante.getNombre());
						limpiar();
						BindUtils.postGlobalCommand(null, null, "paginar", null);
						cerrarVentanaResponderPostulante();
					}		
				}
			});
		}
		
		@Command
		@NotifyChange({"postulante","paginar"})
		public void rechazarPostulante(@BindingParam("ventanaResponderPostulante") Window ventanaResponderPostulante){
			this.ventanaResponderPostulante = ventanaResponderPostulante;
			
			Messagebox.show("�Desea Rechazar al Postulante?","Pregunta",Messagebox.YES | Messagebox.NO,Messagebox.QUESTION,
					new EventListener<Event>(){
				public void onEvent(Event event) throws Exception{
					if(Messagebox.ON_YES.equals(event.getName())){
						if(postulante.getCausa()!=null){
							postulante.setFechaRespuesta(new Date());
							postulante.setAprobacion(false);
							postulante.setEstatus(Short.parseShort("3"));
							servicioPostulante.guardar(postulante);
							Messagebox.show("Respuesta ha sido enviada a "+postulante.getNombre()+" "+postulante.getApellido(),
									"",Messagebox.OK,Messagebox.INFORMATION);
							limpiar();
							BindUtils.postGlobalCommand(null, null, "paginar", null);
							cerrarVentanaResponderPostulante();
						}else{
							Messagebox.show("Debe seleccionar una causa de rechazo","Advertencia",Messagebox.OK,Messagebox.EXCLAMATION);
						}
					}		
				}
			});
		}
		
		@Command
		@NotifyChange({"postulante"})
		public void limpiar(){
			postulante.setId(null);
			postulante.setCedula("");
			postulante.setNombre("");
			postulante.setApellido("");
			postulante.setLugarNacimiento("");
			postulante.setFechaNacimiento(null);
			postulante.setEmail("");
			postulante.setDireccion("");
			postulante.setTelefono("");
			postulante.setAsmatico(null);
			postulante.setRetardo(null);
			postulante.setIntervencionQuirurgica(null);
			postulante.setSordoMudo(null);
			postulante.setDescripcionIntervencion("");
			postulante.setObservacion("");
			postulante.setFechaPostulacion(null);
			postulante.setFechaRespuesta(null);
			postulante.setRespuesta("");
			postulante.setAprobacion(null);
		}		
		
		@Command
		public String cambiarFormatoFecha(Date fecha){
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			return df.format(fecha);	
		}
}